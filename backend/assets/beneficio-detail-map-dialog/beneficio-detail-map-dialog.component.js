angular.module('beneficioDetailMapDialog').component('beneficioDetailMapDialog', {
    templateUrl: '/fgo/assets/beneficio-detail-map-dialog/beneficio-detail-map-dialog.template.html',
    bindings: {
        idBeneficio: '=',
        isEnabled: '='
    },
    controller: ['$mdDialog',
        function BeneficioDetailMapDialog($mdDialog) {

            var self = this;


            this.showAdvanced = function(ev) {
                if (self.isEnabled) {
                    $mdDialog.show({
                        controller: ['$mdDialog', '$scope',
                            function DialogController($mdDialog, $scope) {
                                $scope.idBeneficio = self.idBeneficio;
                                $scope.hide = function() {
                                    $mdDialog.hide();
                                };
                                $scope.cancel = function() {
                                    $mdDialog.cancel();
                                };
                                $scope.answer = function(answer) {
                                    $mdDialog.hide(answer);
                                };
                            }
                        ],
                        templateUrl: '/fgo/assets/beneficio-detail-map-dialog/beneficio-locales-dialog.template.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true

                    }).then(function(answer) {
                        this.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        this.status = 'You cancelled the dialog.';
                    });
                };
            }
        }
    ]

});
