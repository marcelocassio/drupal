dataBenefit.component('beneficioDetail', {
    templateUrl: '/fgo/assets/beneficio-detail/beneficio-detail.template.html',
    controller: ['Beneficio', 'Cupon', '$location', '$window', '$scope', '$mdDialog',
        function BeneficioController(Beneficio, Cupon, $location, $window, $scope, $mdDialog) {

            var self = this;
            self.idBeneficio = $location.search().id;
            self.load = false;
            if (self.idBeneficio) {
                Beneficio.detail().query({ id: self.idBeneficio }, function(res) {
                    self.beneficio = res[0];
                    if (self.beneficio) {

                        self.load = true;

                    } else {
                        $window.location.href = '/fgo/';
                    }

                });
            } else {
                $window.location.href = '/fgo/';
                //window.location = '/fgo/';
            }
            self.isValidUrl = function(url) {
                var regexp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                if(!url){
                 return false;
}
                if (regexp.test(url)) {

                     $window.open(url);
                
                    return true;
                }else{
                                         $window.open("http://"+url);
                 return true;

                }
         
            }
            self.accionBoton = function(codigoAccion) {

                switch (codigoAccion) {
                    //codigo para logear
                    case 0:
                        var dropdownEl = angular.element(document.querySelector('#dropdown-login .login'));
                        dropdownEl.trigger('click');
                        $window.scrollTo(0, 0);

                        return false;

                        break;
                    case 1:
                        $window.location.href = "subscription/";

                        break;
                    case 2:

                        $window.open('https://www.bbvafrances.com.ar/ofertasDigitales/#/?origen=FGO', '_blank');

                        return false;
                        break;
                    case 3:

                        Cupon.descargarVoucher().query({ benefit_id: self.idBeneficio }, function(res) {
                            console.log(res);
                            if (res.codigo !== 0) {
                                $scope.showAlert =
                                    // Appending dialog to document.body to cover sidenav in docs app
                                    // Modal dialogs should fully cover application
                                    // to prevent interaction outside of dialog
                                    $mdDialog.show(
                                        $mdDialog.alert()
                                        .parent(angular.element(document.querySelector('#popupContainer')))
                                        .clickOutsideToClose(true)
                                        .title('')
                                        .textContent(res.descripcion)
                                        .ariaLabel('')
                                        .ok('Cerrar')
                                    );

                                return false;

                            }
                            $window.location.href = 'voucher?voucher_id=' + res.vouchers[0].id;


                        });

                        break;
                    default:
                        break;
                }

            }

        }
    ]
});
