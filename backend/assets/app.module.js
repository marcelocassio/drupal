angular.module('cuponesApp', [
  'ngRoute',
  'ngMaterial',
  'core',
  'cuponera',
  'cupon',
  'cuponDetail',
  'beneficioDetail',
  'beneficioDetailMap',
  'beneficioDetailMapDialog',
  'ngMap',

]).config(['$locationProvider','$mdThemingProvider', function($locationProvider,$mdThemingProvider){
    $locationProvider.html5Mode({
  enabled: true,
  requireBase: false
});

 $mdThemingProvider.theme('default')
    .primaryPalette('light-blue')
    .accentPalette('orange');

}]);;
