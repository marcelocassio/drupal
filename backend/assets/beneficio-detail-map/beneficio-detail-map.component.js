angular.module('beneficioDetailMap').component('beneficioDetailMap', {
    //templateUrl: Drupal.settings.basePath+'/sites/all/modules/custom/bbva_benefit/templates/bbva_benefit.html',
    templateUrl: '/fgo/assets/beneficio-detail-map/beneficio-detail-map.template.html',
    bindings: {
        direcciones: '<',
        comercios: '<',

        idBeneficio: '='
    },
    controller: ['Beneficio', '$location', '$scope', '$window', '$timeout', 'NgMap',
        function BeneficioController(Beneficio, $location, $scope, $window, $timeout, NgMap) {
            var self = this;
            self.load = false;
            self.render = false;
            self.distanciacalculated = false
            self.distanciacalculatedPogress = false


            self.default_lat = '-34.6119033';
            self.default_lon = '-58.4019702';
            NgMap.getMap().then(function(map) {
                self.map = map;
    google.maps.event.addDomListener(map, 'resize', function() {
   map.setCenter(new google.maps.LatLng(self.default_lat,self.default_lon ));
});
                google.maps.event.trigger(map,'resize');
            });

            Beneficio.detail().query({
                pager: 0,
                cant: 10000,
                id: self.idBeneficio
            }, function(res) {
                self.beneficio = res;
                if (self.beneficio[0].shops) {
                    self.comercios = self.beneficio[0].shops;
                    self.load = true;
                    self.render = true;


                } else {
                    self.setDirreciones(res);
                    self.load = true;
                    self.calcularDistancia(res);
                self.render = true;

                }
            });
            self.setDirreciones = function(res) {
                self.direcciones = [];
                self.markers = [];
                angular.forEach(res, function(value, key) {
                        var dirrecion = {}
                        var marker = {};
                        marker = [
                            value.latitude,
                            value.longitude
                        ]
                        dirrecion.calle = value.street
                        dirrecion.marker = marker
                        dirrecion.marker_logo = {
                            url: value.rubro_logo, //
                            size: [60, 60],
                            fillColor: 'blue',
                            origin: [0, 0],
                            anchor: [0, 32]
                        };
                        dirrecion.id = key;
                        self.direcciones.push(dirrecion);
                    }

                );
            };

            self.geoOk = function(pos) {
                var currentPosition = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                self.default_lat = pos.coords.latitude;
                self.default_lon = pos.coords.longitude;
                angular.forEach(self.direcciones, function(value, key) {
                    poscionLocal = new google.maps.LatLng(value.marker[0], value.marker[1]);
                    value.distancia = parseFloat((google.maps.geometry.spherical.computeDistanceBetween(currentPosition, poscionLocal) / 1000).toFixed(2));
                    self.direcciones[key] = value;
                });
                self.distanciacalculatedPogress = true;
                self.distanciacalculated = true

            }
            self.geoError = function(err) {

            };

            self.calcularDistancia = function(res) {
                var options = options = {
                    enableHighAccuracy: true,
                    timeout: 5000,
                    maximumAge: 0
                };

                $window.navigator.geolocation.getCurrentPosition(self.geoOk, self.geoError, options);

            };
            self.setDestino = function(direccion) {;
                self.destino = direccion.marker;
            };
            self.setMapCenter = function(evt, id) {
                if (self.map) {
                    self.direcionInfo = self.direcciones[id];
                    var currentElement = document.getElementById('marker-'.id);
                    var marker = self.map.markers['#marker-' + id];
                    self.map.showInfoWindow('infoBeneficioMapa', marker);
                }
            };
        }
    ]
});
