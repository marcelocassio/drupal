angular.
  module('core.cupon').
  factory('Cupon', ['$resource',
    function($resource) {
      var CuponServ={};

      CuponServ.cuponesVigentes= function () {
         return $resource('/fgo/json/coupon/vigentes_new', {}, {
                         query: {
                            method: 'GET',
                            isArray: false,
                                                        withCredentials:true
                             }
                         });
      }
      CuponServ.cuponesUsados= function () {
         return $resource('/fgo/json/coupon/vencidos_new', {}, {
                         query: {
                            method: 'GET',
                            isArray: false,
                                                        withCredentials:true
                             }
                         });
      }
     CuponServ.voucher= function () {
         return $resource('/fgo/json/coupon/voucher', {}, {
                         query: {
                            method: 'GET',
                            isArray: false,
                                                        withCredentials:true
                             }
                         });
      }

     CuponServ.descargarVoucher= function () {
         return $resource('/fgo/json/coupon/adquirir', {}, {
                         query: {
                            method: 'GET',
                            isArray: false,
                                                        withCredentials:true
                             }
                         });
      }


  return CuponServ

    }
  ]);
