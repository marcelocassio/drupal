coreBenefit.factory('Beneficio', ['$resource', function($resource) {
  var theBenefit={};

  theBenefit.detail = function(){
    return $resource("/fgo/json/benefit", {}, {
      //return $resource('/fgo/json/benefit', {}, {
      query: {
        method: 'GET',
        isArray: true,
        withCredentials:true
      }
    });
  }
  return theBenefit;
}]);
