function init(){
	var xmlHttp = new XMLHttpRequest();
	var url = "modules/" + location.href.split("/")[5] + "/conf/ofertasDigitales.json";
	xmlHttp.open("GET", url,false);
	xmlHttp.send();
	if(xmlHttp.status == 200){
		config = JSON.parse(xmlHttp.response);
		if(typeof config.adobeTagging != "undefined"){
			appendAdobeTagging();
		}
	}else{
		console.warn("No se pudo ejecutar el script en HEAD ya que no se pudo acceder al archivo de configuracion");
	}
}


function appendAdobeTagging(){
	// Agregamos files necesarios
	var datalayer = document.createElement("script");
	var funcionesDatalayer = document.createElement("script");
	datalayer.src = "js/datalayer.js";
	funcionesDatalayer.src = "js/funciones_dataLayer.js";
	document.head.appendChild(datalayer);
	document.head.appendChild(funcionesDatalayer);
	// Adding Adobe Script
	var adobeScript = document.createElement("script");
	var scriptSrcTarget = "//assets.adobedtm.com/95bb966a4c61b200a089c37679aaf96e22114787/satelliteLib-a53dad468409d596fb1b1452aee4d3c1da1ba5ed";
	if(window.location.host == "www.bbva.com.ar"){
		scriptSrcTarget += ".js";
	}else{
		scriptSrcTarget += "-staging.js";
	}
	adobeScript.src = scriptSrcTarget;
	document.head.appendChild(adobeScript);
	// Definimos variable global
	window.isAdobeTaggingActive = true;
}

init();