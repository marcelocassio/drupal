function initPostLoad(){
	if(window.isAdobeTaggingActive){
		callAdobeTaggingPostFunctions();
	}
}

function callAdobeTaggingPostFunctions(){
	if(typeof _satellite == "undefined"){
		setTimeout(callAdobeTaggingPostFunctions, 500);
		return;
	}
	_satellite.pageBottom();
	console.log("Page Bottom ejecutado correctamente");
}