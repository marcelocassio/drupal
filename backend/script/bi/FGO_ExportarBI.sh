OFECHA=$1
JOBNAME=$2
DRUSH_ALIAS=$3
LOG=/tmp/logFGO2BI.$OFECHA.$JOBNAME
if [ ! $OFECHA ] 
then
  echo "$SCRIPTNAME YYYYMMDD (Debe ingresar el odate como primer parametro de entrada)"
  exit 2
fi
if [ ! $JOBNAME ] 
then
  echo "Debe ingresar el JOBNAME como segundo parametro de entrada"
  exit 3
fi
if [ ! $DRUSH_ALIAS ] 
then
  echo "Debe ingresar el ALIAS del drush como tercer parametro de entrada"
  exit 3
fi
echo " " > $LOG
date >> $LOG
cd /prod/apps/internet/drupal/html/fgo/script/bi
/usr/bin/drush $DRUSH_ALIAS $OFECHA $JOBNAME >> $LOG
date >> $LOG