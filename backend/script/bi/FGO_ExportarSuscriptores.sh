#! /bin/bash
OFECHA=$1
JOBNAME=$2
LOG=/tmp/logFGO2BI.$OFECHA.$JOBNAME
if [ ! $OFECHA ] 
then
  echo "$SCRIPTNAME YYYYMMDD (Debe ingresar el odate como primer parametro de entrada)"
  exit 2
fi
if [ ! $JOBNAME ] 
then
  echo "Debe ingresar el JOBNAME como segundo parametro de entrada"
  exit 3
fi

echo " " > $LOG
date >> $LOG

cd /prod/apps/internet/drupal/html/fgo/script/bi
/usr/bin/drush esus $OFECHA $JOBNAME

# Transferir archivo de suscriptores generados 

date >> $LOG