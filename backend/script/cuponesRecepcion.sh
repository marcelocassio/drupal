#!/bin/sh
historial=/prod/jp00/cupones/vuelta/historial

if [[ ! -e $historial ]]; then
    mkdir $historial
fi

drush bcpp
drush bvpnew

fecha=`date +"%Y-%m-%d-%H-%M"`

cp /prod/jp00/cupones/vuelta/TJVOUC01-PROMOCIONES-VISA.txt /prod/jp00/cupones/vuelta/historial/TJVOUC01-PROMOCIONES-VISA_${fecha}.txt
cp /prod/jp00/cupones/vuelta/TJVOUC02-VOUCHER-VISA.txt /prod/jp00/cupones/vuelta/historial/TJVOUC02-VOUCHER-VISA_${fecha}.txt

echo ""
echo " ===== FIN CUPONES RECEPCION =====  "
echo ""
