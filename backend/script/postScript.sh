#!/bin/sh
sh /prod/apps/internet/drupal/html/fgo/scripts/drush_version.sh
drush cacheapi
drush mmmff --all

chown -R dmsys:www /prod/apps/internet/drupal/html/fgo
chmod -R 750 /prod/apps/internet/drupal/html/fgo
chmod -R 770 /prod/apps/internet/drupal/html/fgo/sites/default/files
chmod -R 770 /prod/apps/internet/drupal/html/fgo/tmp

chmod 440  /prod/apps/internet/drupal/html/fgo/sites/default/settings.php

drush cc all
exit 0
