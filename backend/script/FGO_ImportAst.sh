#! /bin/bash
OFECHA=$1
JOBNAME=$2
LOG=/tmp/logFGOAST.$OFECHA
if [ ! $OFECHA ]
then
  echo "$SCRIPTNAME YYMMDD (Debe ingresar el odate como primer parametro de entrada)"
  exit 2
fi

echo " " > $LOG
cd /prod/apps/internet/drupal/html/fgo/script
date >> $LOG
echo "IMPORTANDO EMPRESAS NOMINAS"
/usr/bin/drush semento-ast >> $LOG

if [ $? -ne 0 ]
    then
    echo "---> Se ejecuto con ERRORES . Verificar!!! <---" >> $LOG
    echo `date` >> $LOG
    exit 1
fi
echo "IMPORTANDO CLIENTES NOMINAS"
/usr/bin/drush ast-nameuser >> $LOG

if [ $? -ne 0 ]
    then
    echo "---> Se ejecuto con ERRORES . Verificar!!! <---" >> $LOG
    echo `date` >> $LOG
    exit 1
fi


# Transferir archivo de suscriptores generados

date >> $LOG
