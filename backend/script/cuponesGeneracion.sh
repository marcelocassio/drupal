#!/bin/sh
historial=/prod/jp00/cupones/enviar/historial

if [[ ! -e $historial ]]; then
    mkdir $historial
fi

drush bcpnew
fecha=`date +"%Y-%m-%d-%H-%M"`

cp /prod/jp00/cupones/enviar/coupon_promo_visa.txt /prod/jp00/cupones/enviar/historial/coupon_promo_visa_${fecha}.txt
cp /prod/jp00/cupones/enviar/vouchers_promo_visa.txt /prod/jp00/cupones/enviar/historial/vouchers_promo_visa_${fecha}.txt

echo ""
echo " ===== FIN CUPONES GENERACION =====  "
echo ""
