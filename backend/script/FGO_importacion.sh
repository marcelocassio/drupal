#! /bin/bash
OFECHA=$1
LOG=/tmp/logFGOimportacion.$OFECHA
if [ ! $OFECHA ]
then
  echo "$SCRIPTNAME YYMMDD (Debe ingresar el odate como primer parametro de entrada)"
  exit 2
fi

echo " " > $LOG
date >> $LOG

cd /prod/apps/internet/drupal/html/fgo/script
/usr/bin/drush importarTodo $OFECHA >> $LOG
# Evaluacion de retornos
if [ $? -ne 0 ]
    then
    echo "---> Se ejecuto con ERRORES . Verificar!!! <---" >> $LOG
    echo `date` >> $LOG
    exit 1
fi

date >> $LOG
