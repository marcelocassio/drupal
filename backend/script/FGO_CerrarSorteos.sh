#! /bin/bash
OFECHA=$1
JOBNAME=$2
LOG=/tmp/logFGOSORTEO.$OFECHA
if [ ! $OFECHA ]
then
  echo "$SCRIPTNAME YYMMDD (Debe ingresar el odate como primer parametro de entrada)"
  exit 2
fi

echo " " > $LOG
date >> $LOG

cd /prod/apps/internet/drupal/html/fgo/script
/usr/bin/drush cerrar-sorteos $OFECHA >> $LOG

# Evaluacion de retornos
if [ $? -ne 0 ]
    then
    echo "---> Se ejecuto con ERRORES . Verificar!!! <---" >> $LOG
    echo `date` >> $LOG
    exit 1
fi

# Transferir archivo de suscriptores generados
date >> $LOG
