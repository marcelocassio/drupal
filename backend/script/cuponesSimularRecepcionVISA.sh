# 
# Uso
# 
# sh cuponesSimularRecepcionVISA.sh ID_PROMO_CUPON
# 
# ARCHIVO-ENVIAR-PROMO-VISA: /prod/jp00/cupones/enviar/vouchers_promo_visa.txt
# ARCHIVO-VUELTA-PROMO-VISA: /prod/jp00/cupones/vuelta/TJVOUC02-VOUCHER-VISA.txt
# 
# 

PROMO_NUMERO=$1

if [ -z $PROMO_NUMERO ]; then
    echo ""
    echo "PARAMETRO NESARIO: PROMO NUMERO"
    echo ""
    echo "forma de uso: sh cuponesSimularRecepcionVISA [PROMO NUMERO]"
    echo ""
    exit
fi




#!/bin/bash       
ARCHIVO_ENTRADA='/prod/jp00/cupones/enviar/vouchers_promo_visa.txt'
ARCHIVO_SALIDA='/prod/jp00/cupones/vuelta/TJVOUC02-VOUCHER-VISA.txt'

echo ""
echo " ======= GENERANDO ARCHIVOS ======  "
echo ""

echo " RECEPCION VOUCHER de VISA "
cut -d' ' -f 1 "$ARCHIVO_ENTRADA" > VOUCHER_TMP.txt

RESTO="   1620000000001000000000000000000173401718000000000000000T1712     "

sed "s/$/$RESTO/" VOUCHER_TMP.txt > $ARCHIVO_SALIDA


HEADER="HDR201712061718340000000190                                                         "
echo "$HEADER" | cat - $ARCHIVO_SALIDA > temp && mv temp $ARCHIVO_SALIDA


echo " Ver archivo: " $ARCHIVO_SALIDA
echo ""



ARCHIVO_SALIDA_CUPON='/prod/jp00/cupones/vuelta/TJVOUC01-PROMOCIONES-VISA.txt'

echo " RECEPCION CUPONES de VISA "

echo "HDR201712061718180000000190" > $ARCHIVO_SALIDA_CUPON
echo "UP11700000000${PROMO_NUMERO}ELNOBLE                                 171211171218   AR0000000003230009179175000000000100000000000" >> $ARCHIVO_SALIDA_CUPON

echo " Ver archivo: " $ARCHIVO_SALIDA_CUPON

echo ""
echo " ===== FIN GENERACION ARCHIVOS =====  "
echo ""

echo ""
echo " ===== FIN DE SIMULAR RECEPCION CUPONES VISA =====  "
echo ""

