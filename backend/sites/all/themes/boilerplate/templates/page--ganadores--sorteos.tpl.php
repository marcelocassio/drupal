<?php

  global $user;
  $listaGanadores = array();
  $user_profile = user_load($user->uid);
  $profile_name = isset($user_profile->field_profile_nombre['und']) ? $user_profile->field_profile_nombre['und']['0']['safe_value'] : "";

  foreach ($page['content']['system_main'] as $ganador) {

    if (is_array($ganador) && isset($ganador["nombre"])) {
      $listaGanadores[] = array(
        'nombre' => $ganador["nombre"],
        'apellido' => $ganador["apellido"],
        'tipo_documento' => $ganador["tipo_documento"],
        'numero_documento' => $ganador["numero_documento"]
      );
    }
  }

?>

<div id="page" class="<?=$classes?>" <?=$attributes?>>

  <div id="main" class="clearfix" role="main">

    <div id="content" style="margin-top: 50px;">
      <div id="content-inner" class="inner column center">
        <div id="content-area" class="modal-ganadores">
          <div class="titulo-ganadores-wrapper">

            <?php if ($user->uid != 0): ?>
              <div class="titulo-nombre"><?=$profile_name?>,</div>
            <?php endif ?>
            <div class="titulo">Estos son los ganadores del sorteo</div>

          </div>
          <div class="lista-ganadores">
            <ul>
              <?php foreach ($listaGanadores as $ganador): ?>
                <div class="ganador">
                  <h3 class="nombre" style="margin-bottom: 5px;">
                    <?=$ganador['nombre'].' '.$ganador['apellido']?>
                    <h4 class="documento" style="margin-bottom: 30px;"><span><?=$ganador['tipo_documento']?>: </span><?=$ganador['numero_documento']?></h4>
                  </h3>
                </div>
              <?php endforeach ?>
            </ul>
            <?php /* Se usa strip tags para eliminar los div region content  */ ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
jQuery( document ).ready(function() {
  jQuery("#smartbanner").remove();
  jQuery('head').append("<meta name='viewport' content='width=device-width, user-scalable=no'>");
});
</script>
