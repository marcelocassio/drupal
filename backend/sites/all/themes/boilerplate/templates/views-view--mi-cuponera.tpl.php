<?php

global $user;
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="mi-cuponera-tab">
<p class="tab1 <?php echo (arg(1)=='mi-cuponera')?'active':''; ?>"><a href="<?php print url('user/mi-cuponera/'.$user->uid);  ?>">MIS CUPONES VIGENTES</a></p>
<p class="tab2 <?php echo (arg(1)=='mi-cuponera-usados')?'active':''; ?>"><a href="<?php print url('user/mi-cuponera-usados/'.$user->uid); ?>">CUPONES USADOS</a></p>
</div>
<div class="<?php print $classes_array[1]; ?>">
  <div class="view-content">
      <?php if ($rows){ ?>
        <?php print $rows; ?>
      <?php }else{ ?>
        <h1>No tenés cupones para mostrar.</h1>
      <?php } ?>
  </div>
</div>
