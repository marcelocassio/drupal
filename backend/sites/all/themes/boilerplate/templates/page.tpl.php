<?php
$user_profile = user_load($user->uid);
if (isset($user_profile->field_profile_nombre['und']['0']['safe_value'])) {
  $nombre = $user_profile->field_profile_nombre['und']['0']['safe_value'];
}else{
  $nombre = '';
}
if (isset($user_profile->field_profile_apellido['und']['0']['safe_value'])) {
  $apellido = $user_profile->field_profile_apellido['und']['0']['safe_value'];
}else{
  $apellido = '';
}
$profile_name = $nombre . ' ' . $apellido;
?>
<?php if ($page['menu_responsive']): ?>
  <aside id="menu-responsive" class="column sidebar first">
   <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
      </a>
    <?php endif; ?>
    <?php if ($user->uid): ?>
      <div class="cabezal-usuario"><?php print $profile_name ?></div>
      <div class="cabezal-notificaciones"><a href="#">Tiene <span class="bbva-cant-mensajes">0</span> notificaciones</a></div>
    <?php else: ?>
    <div class="cabezal-ingresar">Inici&aacute; sesi&oacute;n</div>
    <div class="cabezal-ingresar-registro"><a href="#">¿No tenés usuario?</a></div>
    <?php endif; ?>
    <button data-target=".nav-collapse" data-toggle="collapse" class="btn-responsive trigger-margin-region-slideshow" type="button">
    </button>
    <?php print render($page['menu_responsive']); ?>
  </aside>
<?php endif; ?>


<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

<!-- ______________________ HEADER _______________________ -->
  <header id="header">
    <div class="container">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>
      <?php if ($site_slogan): ?>
        <div id="slogan">
          <?php if ($site_slogan): ?>
            <p class="site-slogan"><?php print $site_slogan; ?></p>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <?php if ($page['header']): ?>
        <?php print render($page['header']); ?>
      <?php endif; ?>
    </div>
  </header> <!-- /header -->

  <?php if ($page['remember']): ?>
    <div id="content-remember">
      <?php print render($page['remember']) ?>
    </div>
  <?php endif; ?>

  <?php print $breadcrumb; ?>


  <?php print $messages; ?>
  <div id="main" class="clearfix" role="main">

    <?php if ($page['sidebar_first']): ?>
      <aside id="sidebar-first" class="column sidebar first">
        <div id="sidebar-first-inner" class="inner">
          <?php print render($page['sidebar_first']); ?>
        </div>
      </aside>
    <?php endif; ?> <!-- /sidebar-first -->

    <div id="content">
      <div id="content-inner" class="inner column center">

        <?php if ($page['slideshow']): ?>
          <div id="slideshow-wrapper">
            <?php print render($page['slideshow']) ?>
          </div>
        <?php endif; ?>

        <?php if ($page['highlight']): ?>
          <div id="higlighted-area">
            <?php print render($page['highlight']) ?>
          </div>
        <?php endif; ?>

        <?php if ($breadcrumb || $title || $messages || $tabs || $action_links): ?>
          <div id="content-header">
            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print render($page['help']); ?>
            <?php if ($tabs): ?>
              <div class="tabs"><?php print render($tabs); ?></div>
            <?php endif; ?>
            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
          </div> <!-- /#content-header -->
        <?php endif; ?>
        <div id="content-area">
          <?php print render($page['content']) ?>
        </div>
      </div>
    </div> <!-- /content-inner /content -->

    <?php if ($page['sidebar_second']): ?>
      <aside id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </aside>
    <?php endif; ?> <!-- /sidebar-second -->
  </div> <!-- /main -->

  <?php if ($page['pre_footer']): ?>
    <aside id="pre-footer">
      <div class="inner">
        <?php print render($page['pre_footer']); ?>
      </div>
    </aside>
  <?php endif; ?>

  <?php if ($page['footer']): ?>
    <footer id="footer">
      <?php print render($page['footer']); ?>
    </footer> <!-- /footer -->
  <?php endif; ?>

  <?php if ($page['footer_copyright']): ?>
    <div id="footer-copyright">
      <?php print render($page['footer_copyright']); ?>
    </div> <!-- /footer -->
  <?php endif; ?>
</div> <!-- /page -->
