<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php print $language->language;?>" xml:lang="<?php print $language->language;?>" dir="<?php print $language->dir;?>" <?php print $rdf_namespaces;?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="<?php print $language->language;?>" xml:lang="<?php print $language->language;?>" dir="<?php print $language->dir;?>" <?php print $rdf_namespaces;?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="<?php print $language->language;?>" xml:lang="<?php print $language->language;?>" dir="<?php print $language->dir;?>" <?php print $rdf_namespaces;?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" xml:lang="<?php print $language->language;?>" lang="<?php print $language->language;?>" dir="<?php print $language->dir;?>" <?php print $rdf_namespaces;?>> <!--<![endif]-->
  <head>
    <?php print $head;?>
    <title><?php print $head_title;?></title>
    <?php print $styles;?>
    <?php print $scripts;?>
    <meta name="robots" content="noindex,nofollow">
  <script>

(function(i) {

var ts = document.createElement('script');

ts.type = 'text/javascript';

ts.async = true;

ts.src = ('https:' == document.location.protocol ? 'https://' : 'http://') +

'tags.t.tailtarget.com/t3m.js?i=' + i;

var s = document.getElementsByTagName('script')[0];

s.parentNode.insertBefore(ts, s);

})('TT-11761-7/CT-708');

</script>

<script type="text/javascript">
 var lbTrans = '[TRANSACTION ID]';
 var lbValue = '[TRANSACTION VALUE]';
 var lbData = '[Attribute/Value Pairs for Custom Data]';
 var lb_rn = new String(Math.random()); var lb_rns = lb_rn.substring(2, 12);
 var boltProtocol = ('https:' == document.location.protocol) ? 'https://' : 'http://';
 try {
 var newScript = document.createElement('script');
 var scriptElement = document.getElementsByTagName('script')[0];
 newScript.type = 'text/javascript';
 newScript.id = 'lightning_bolt_' + lb_rns;
 newScript.src = boltProtocol + 'b3.mookie1.com/2/LB/' + lb_rns + '@x96?';
 scriptElement.parentNode.insertBefore(newScript, scriptElement);
 scriptElement = null; newScript = null;
 } catch (e) { }
</script>


  </head>
  <body class="<?php print $classes;?>" <?php print $attributes;?>>
  <div id="fb-root"></div>
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '118586505158934',
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  </script>

    <!--[if lt IE 7]>
      <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <?php print $page_top;?>
    <?php print $page;?>
    <?php print $page_bottom;?>
  </body>
</html>
