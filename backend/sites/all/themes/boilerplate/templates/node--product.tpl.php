<?php if(arg(0) != 'taxonomy') :?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" style="background: url(<?php print file_create_url($node->field_product_image_bg['und'][0]['uri']) ?>) no-repeat top center; background-size: 100%; min-height: 850px;">
  <div class="node-inner">
    <div class="content">
      <?php 
        // We hide the comments and links now so that we can render them later.
        //hide($content['comments']);
        //hide($content['links']);
        //print render($content);
        $imagebutton = $node->field_product_image_button['und'][0]['uri'];
       ?>
       <div id="node-product">
         <div class="content-left">
           <div class="field-title"><?php print $node->title ?></div>
           <div class="field-description"><?php print $node->field_product_list_descriptions['und'][0]['value'] ?></div>
         </div>
       </div>
    </div>
  </div> <!-- /node-inner -->
</article> <!-- /node-->
<div class="article-bottom">
  <div class="field-tyc"><?php print l($node->field_product_tyc['und'][0]['entity']->title, 'node/'. $node->field_product_tyc['und'][0]['entity']->nid) ?></div>
  <div class="content-button">
    <div class="field-button">
      <div class="field-button"><a href="http://<?php print $node->field_product_link_button['und'][0]['url'] ?>" tarjet="_blank"><img src="<?php print file_create_url($imagebutton) ?>" /></a></div>
     </div>
  </div>
</div>
<?php endif ?>