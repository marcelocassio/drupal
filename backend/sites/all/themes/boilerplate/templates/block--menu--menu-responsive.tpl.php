<?php
$block_1 = module_invoke('bbva', 'block_view', 'search_content');
if ($user->uid != 0) {
  $uid = $user->uid;
  $user_profile = user_load($user->uid);
  $es_cliente = ''.$user_profile->field_profile_tipo_cliente['und']['0']['value'];

  $link_perfil_text = "<span>Editar perfil</span>";
  $link_perfil_link = "user/$uid/edit";

  $link_change_pass_text = "<span>Contrase&ntilde;a</span>";
  $link_change_pass_link = "user/$uid/change-password";

  $link_cuponera_text = "<span>Mis cupones</span>";
  $link_cuponera_link = "cuponera";

  $linktext = "<span>Cerrar sesi&oacute;n</span>";
  $link = '';
  // $link = 'user/logout';
  $linkclass = 'logout';
}
?>
<?php $tag = $block->subject ? 'section' : 'div'; ?>
<<?php print $tag; ?> id="block-<?php print $block->module .'-'. $block->delta ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="block-inner">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2 class="block-title"<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>
    <div class="content" <?php print $content_attributes; ?>>
      <?php print render ($block_1); ?>
      <?php print $content; ?>
      <ul class="menu-bajo">
      <?php if ($user->uid): ?>
          <li class="last leaf configuración mid-1336"><a href="javascript:void(0)">Configuración</a></li>
          <ul id="config-menu-mobile" style="display: none;">
			<li><?php  print l($link_cuponera_text, $link_cuponera_link, array('attributes' => array('class' => array('button')), 'html' => TRUE));?></li>
            <li><?php  print l($link_perfil_text, $link_perfil_link, array('attributes' => array('class' => array('button')), 'html' => TRUE));?></li>
            <?php if ($es_cliente == 0): ?>
              <li><?php  print l($link_change_pass_text, $link_change_pass_link, array('attributes' => array('class' => array('button')), 'html' => TRUE));?></li>
            <?php else: ?>
              <li><a href="https://www.bbva.com.ar">Francés Net</a></li>
            <?php endif; ?>
            <li><?php  print l($linktext, $link, array('attributes' => array('class' => array($linkclass, 'button'), 'id' => array('logoffevent')), 'html' => TRUE));?></li>
          </ul>
      <?php else: ?>
          <li class="last leaf configuración mid-1336"><a href="<?php echo url(NULL, array('absolute' => TRUE)); ?>subscription">Suscripción</a></li>
      <?php endif; ?>
          <li class="contacto"><a href="<?php echo variable_get('bbva_login_fnet_contacto_url', 'https://online.bbva.com.ar/fnet/mod/bbva/NL-contactenos.jsp?origen=fgo'); ?>">Contacto</a></li>
      </ul>
    </div>
  </div>
</<?php print $tag; ?>> <!-- /block-inner /block -->
