<?php

/**
 * @file
 * Default theme implementation to display a term.
 *
 * Available variables:
 * - $name: (deprecated) The unsanitized name of the term. Use $term_name
 *   instead.
 * - $content: An array of items for the content of the term (fields and
 *   description). Use render($content) to print them all, or print a subset
 *   such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $term_url: Direct URL of the current term.
 * - $term_name: Name of the current term.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - taxonomy-term: The current template type, i.e., "theming hook".
 *   - vocabulary-[vocabulary-name]: The vocabulary to which the term belongs to.
 *     For example, if the term is a "Tag" it would result in "vocabulary-tag".
 *
 * Other variables:
 * - $term: Full term object. Contains data that may not be safe.
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $page: Flag for the full page state.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the term. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_taxonomy_term()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div id="taxonomy-term-<?php print $term->tid; ?>" class="<?php print $classes; ?>">

  <?php if ($term->vid == 11) : // Vocabulario Experience List ?> 
    <div id="parallax">
      <?php
        $items = $term->field_experience_items['und'];
        $item = array();
        foreach ($items as $item) : ?>
          <?php 
           $fc = field_collection_field_get_entity($item);
           $logo_path = file_load($term->field_experience_logo['und'][0]['fid'])->uri;
           $image_full = image_style_url('experience_big', isset($fc->field_experience_image['und']) ? $fc->field_experience_image['und'][0]['uri'] : null);
           //$imagepath = file_load($fc->field_experience_image['und'][0]['fid'])->uri;
          ?>

         <div class="intro<?php print $fc->item_id ?> intro-content" data-speed="12" data-type="background" 
         style="background-image: url(<?php print $image_full ?>); background-repeat: no-repeat; background-position: center top; background-size: cover;"></div>


          <div class="content-node-parallax intro-unit item-content-<?php print $fc->item_id ?>" data-speed="9" data-type="background">
            <?php if (!empty($fc->field_experience_title)) : ?>
              <div class="group-title">
                <div class="field-name-field-experience-title">
                  <?php print $fc->field_experience_title['und'][0]['value']; ?>
                </div>
              </div>
            <?php endif ?>
            
            <?php if (!empty($fc->field_experience_volanta)) : ?>
              <div class="group-header">
                <div class="section">
                  <div class="icon-tarjeta" style="background-image: url(<?php print file_create_url($logo_path) ?>)">
                    <?php $volanta = $fc->field_experience_volanta['und'][0]['value']; ?>
                      <?php $type = $fc->field_experience_type_link['und'][0]['value'];?>
                      <?php if (!empty($fc->field_experience_type_exp) && $type == 0) : ?>
                          <?php print l($volanta, 'node/'.$fc->field_experience_type_exp['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_ben) && $type == 1) : ?>
                          <?php print l($volanta, 'benefit-experience', array('query' => array('field_tags_tid' => $fc->field_experience_type_ben['und'][0]['value']))); ?>
                      <?php elseif(!empty($fc->field_experience_type_sort) && $type == 2) : ?>
                          <?php print l($volanta, 'node/' . $fc->field_experience_type_sort['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_esp) && $type == 3) : ?>
                        <?php print l($volanta, $fc->field_experience_type_esp['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_pro) && $type == 4) : ?>
                          <?php print l($volanta, 'node/' . $fc->field_experience_type_pro['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_exps) && $type == 5) : ?>
                          <?php $my_path = drupal_get_path_alias($path = NULL, $path_language = NULL); ?>
                          <?php print l($volanta, $my_path .'/'. $fc->field_experience_type_exps['und'][0]['tid']); ?>
                      <?php elseif(!empty($fc->field_experience_type_prods) && $type == 6) : ?>
                          <?php print l($volanta, 'lista/productos/' . $fc->field_experience_type_prods['und'][0]['tid']); ?>
                      <?php elseif(!empty($fc->field_experience_type_tyc) && $type == 7) : ?>
                          <?php print l($volanta, 'node/' . $fc->field_experience_type_tyc['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_url) && $type == 8) : ?>
                          <a href="<?php print $fc->field_experience_type_url['und'][0]['url'] ?>" target="_blank"><?php print $volanta ?></a>
                      <?php endif ?>
                  </div>
                  <div class="field-name-field-experience-volanta" style="background: <?php print $term->field_experience_color['und'][0]['rgb'] ?>">
                      <?php $volanta = $fc->field_experience_volanta['und'][0]['value']; ?>
                      <?php $type = $fc->field_experience_type_link['und'][0]['value'];?>
                      <?php if (!empty($fc->field_experience_type_exp) && $type == 0) : ?>
                          <?php print l($volanta, 'node/'.$fc->field_experience_type_exp['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_ben) && $type == 1) : ?>
                          <?php print l($volanta, 'benefit-experience', array('query' => array('field_tags_tid' => $fc->field_experience_type_ben['und'][0]['value']))); ?>
                      <?php elseif(!empty($fc->field_experience_type_sort) && $type == 2) : ?>
                          <?php print l($volanta, 'node/' . $fc->field_experience_type_sort['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_esp) && $type == 3) : ?>
                        <?php print l($volanta, $fc->field_experience_type_esp['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_pro) && $type == 4) : ?>
                          <?php print l($volanta, 'node/' . $fc->field_experience_type_pro['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_exps) && $type == 5) : ?>
                          <?php $my_path = drupal_get_path_alias($path = NULL, $path_language = NULL); ?>
                          <?php print l($volanta, $my_path .'/'. $fc->field_experience_type_exps['und'][0]['tid']); ?>
                      <?php elseif(!empty($fc->field_experience_type_prods) && $type == 6) : ?>
                          <?php print l($volanta, 'lista/productos/' . $fc->field_experience_type_prods['und'][0]['tid']); ?>
                      <?php elseif(!empty($fc->field_experience_type_tyc) && $type == 7) : ?>
                          <?php print l($volanta, 'node/' . $fc->field_experience_type_tyc['und'][0]['target_id']); ?>
                      <?php elseif(!empty($fc->field_experience_type_url) && $type == 8) : ?>
                          <a href="<?php print $fc->field_experience_type_url['und'][0]['url'] ?>" target="_blank"><?php print $volanta ?></a>
                      <?php endif ?>
                  </div>
                </div>
              </div>
            <?php endif ?>

            <div class="parallax-inner">
              <div class="group-left">
                <div class="section">
                  <div class="field-name-field-experience-bajada">
                    <?php 
                      if (!empty($fc->field_experience_bajada)) {
                        print $fc->field_experience_bajada['und'][0]['value'];
                      }
                    ?>
                  </div>
                </div>
              </div>

              <div class="group-right">
                <div class="section">
                  <div class="field-name-field-experience-descripcion">
                    <?php 
                      if (!empty($fc->field_experience_descripcion)) {
                        print $fc->field_experience_descripcion['und'][0]['value'];
                      }
                    ?>
                  </div>
                  <div class="field-name-field-experience-link">
                    <?php $type = $fc->field_experience_type_link['und'][0]['value'];?>
                    <?php if (!empty($fc->field_experience_type_exp) && $type == 0) : ?>
                    	  <?php print l($fc->field_experience_text_link['und'][0]['value'], 'node/'.$fc->field_experience_type_exp['und'][0]['target_id']); ?>
                    <?php elseif(!empty($fc->field_experience_type_ben) && $type == 1) : ?>
                        <?php print l($fc->field_experience_text_link['und'][0]['value'], 'benefit-experience', array('query' => array('field_tags_tid' => $fc->field_experience_type_ben['und'][0]['value']))); ?>
                    <?php elseif(!empty($fc->field_experience_type_sort) && $type == 2) : ?>
                        <?php print l($fc->field_experience_text_link['und'][0]['value'], 'node/' . $fc->field_experience_type_sort['und'][0]['target_id']); ?>
                    <?php elseif(!empty($fc->field_experience_type_esp) && $type == 3) : ?>
                    	<?php print l($fc->field_experience_text_link['und'][0]['value'], $fc->field_experience_type_esp['und'][0]['target_id']); ?>
                    <?php elseif(!empty($fc->field_experience_type_pro) && $type == 4) : ?>
                    	  <?php print l($fc->field_experience_text_link['und'][0]['value'], 'node/' . $fc->field_experience_type_pro['und'][0]['target_id']); ?>
                    <?php elseif(!empty($fc->field_experience_type_exps) && $type == 5) : ?>
                        <?php $my_path = drupal_get_path_alias($path = NULL, $path_language = NULL); ?>
                        <?php print l($fc->field_experience_text_link['und'][0]['value'], $my_path .'/'. $fc->field_experience_type_exps['und'][0]['tid']); ?>
                    <?php elseif(!empty($fc->field_experience_type_prods) && $type == 6) : ?>
                        <?php print l($fc->field_experience_text_link['und'][0]['value'], 'lista/productos/' . $fc->field_experience_type_prods['und'][0]['tid']); ?>
                    <?php elseif(!empty($fc->field_experience_type_tyc) && $type == 7) : ?>
                        <?php print l($fc->field_experience_text_link['und'][0]['value'], 'node/' . $fc->field_experience_type_tyc['und'][0]['target_id']); ?>
                    <?php elseif(!empty($fc->field_experience_type_url) && $type == 8) : ?>
                        <a href="<?php print $fc->field_experience_type_url['und'][0]['url'] ?>" target="_blank"><?php print $fc->field_experience_text_link['und'][0]['value'] ?></a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
      <?php endforeach ?>
    </div>
  <?php elseif ($term->vid == 10) : // Vocabulario Experience ?>
    <div id="term-carousel">
      <?php $items_terms = $term->field_term_items['und'] ?>
      <ul class="jcarousel-20">
      <?php foreach ($items_terms as $item_term) : ?>
        <?php 
          $fc_terms = field_collection_field_get_entity($item_term); 
          if (!empty($fc_terms->field_term_item_bg['und'])){
            $imagepath_term = $fc_terms->field_term_item_bg['und'][0]['uri'];
          }
        ?>
          <?php if($fc_terms->field_term_template['und'][0]['value'] == 1) : ?>
            <li>
              <?php if ($fc_terms->field_term_format_link['und'][0]['value'] == 0) : ?>
                <div class="row only-bg" style="background: url(<?php print file_create_url($imagepath_term) ?>)"><a href="?q=taxonomy/term/<?php print $fc_terms->field_term_items_related['und'][0]['tid']?>"></a></div>
              <?php elseif($fc_terms->field_term_format_link['und'][0]['value'] == 1): ?>
                <div class="row only-bg" style="background: url(<?php print file_create_url($imagepath_term) ?>)"><a href="<?php print $fc_terms->field_term_item_extern['und'][0]['url']?>" target="_blank"></a></div>
              <?php endif ?>
            </li>  
          <?php elseif($fc_terms->field_term_template['und'][0]['value'] == 2) : ?>
            <li>
              <div class="row" style="background: url(<?php print file_create_url($imagepath_term) ?>)">
                <div class="col-rows">

                  <?php foreach ($fc_terms->field_term_items_multiple as $key => $item_multiples) :?>
                    <?php $items_count = count($item_multiples); ?>
                    <?php foreach ($item_multiples as $key => $item_multiple) :
                        $fc_multiple = field_collection_field_get_entity($item_multiple);
                        $logo_image = theme_image_style(array(
                          'style_name' => 'experience_logo', 
                          'path' => isset($fc_multiple->field_multiple_logo['und']) ? $fc_multiple->field_multiple_logo['und'][0]['uri'] : null,
                          'width' => null, 'height' => null
                        ));
                       
                        if ($items_count <= 2) {
                      	  $item_class = 'item2';
                        } elseif ($items_count == 3){
                          $item_class = 'item3';
                        } elseif ($items_count == 4){
                          $item_class = 'item4';
                        }
                      ?>
                      <div class="field-logo <?=$item_class?>">
                        <?php if (!empty($fc_multiple->field_multiple_titulo['und'])) :?>
                          <div class="field-item-title"><?=$fc_multiple->field_multiple_titulo['und'][0]['value']?></div>
                        <?php endif ?>  
                        <div class="field-item-image">

                        <?php 
                          $url_taxonomy_term = isset($fc_multiple->field_multiple_related_term2['und']) ? url('taxonomy/term/'.$fc_multiple->field_multiple_related_term2['und'][0]['target_id']) : '#';
                        ?>
                          <a href="<?=$url_taxonomy_term ?>"><?=$logo_image?></a>
                        </div>
                      </div>
                    <?php endforeach ?>
                  <?php endforeach ?>
                </div>
              </div>
            </li>
          <?php elseif($fc_terms->field_term_template['und'][0]['value'] == 3) : ?>
            <li>
              <?php if (!empty($fc_terms->field_term_item_iframe['und'])) :?>
              <?php print $fc_terms->field_term_item_iframe['und'][0]['value'] ?>
              <?php endif ?>
            </li>
          <?php endif ?>
      <?php endforeach ?>
      </ul>
    </div>
  <?php endif; ?>
</div>
