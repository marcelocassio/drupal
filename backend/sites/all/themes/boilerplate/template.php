<?php

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('clear_registry')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}

// // Add Zen Tabs styles
// if (theme_get_setting('boilerplate_tabs')) {
//   drupal_add_css( drupal_get_path('theme', 'boilerplate') . '/css/tabs.css');
// }

/**
 * Preprocesses the wrapping HTML.
 *
 * @param array &$variables
 *   Template variables.
 */
function boilerplate_preprocess_html(&$vars) {
  if (!module_exists('conditional_styles')) {
    boilerplate_add_conditional_styles();
  }
  // Setup IE meta tag to force IE rendering mode
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' =>  'IE=edge,chrome=1',
      'http-equiv' => 'X-UA-Compatible',
    )
  );
  //  Mobile viewport optimized: h5bp.com/viewport
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' =>  'width=device-width',
      'name' => 'viewport',
    )
  );



  // Add header meta tag for IE to head
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_viewport, 'meta_viewport');
  drupal_add_html_head($meta_viewport, 'viewport');


}

function boilerplate_preprocess_page(&$vars, $hook) {
  if (isset($vars['node_title'])) {
    $vars['title'] = $vars['node_title'];
  }
  // Adding a class to #page in wireframe mode
  if (theme_get_setting('wireframe_mode')) {
    $vars['classes_array'][] = 'wireframe-mode';
  }
  // Adding classes wether #navigation is here or not
  if (!empty($vars['main_menu']) or !empty($vars['sub_menu'])) {
    $vars['classes_array'][] = 'with-navigation';
  }
  if (!empty($vars['secondary_menu'])) {
    $vars['classes_array'][] = 'with-subnav';
  }

  if(isset($vars['page']['content']['system_main']['no_content'])) {
    unset($vars['page']['content']['system_main']['no_content']);
  }
  drupal_add_js(drupal_get_path('module', 'jcarousel') .'/js/jquery.jcarousel.js');
  drupal_add_js(drupal_get_path('module', 'jcarousel') .'/js/jquery.jcarousel.min.js');
}

function boilerplate_preprocess_node(&$vars) {
  // Add a striping class.
  $vars['classes_array'][] = 'node-' . $vars['zebra'];
}

function boilerplate_preprocess_block(&$vars, $hook) {
  // Add a striping class.
  $vars['classes_array'][] = 'block-' . $vars['zebra'];
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function boilerplate_breadcrumb($variables) {

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');

  // Experiencias Segundo Nivel
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {

    $tid = arg(2);
    $term = taxonomy_term_load($tid);
    if ($term->vocabulary_machine_name != 'experience') {
      $term_tid = $term->field_experience['und'][0]['tid'];
      $term_parent = taxonomy_term_load($term_tid);
      $uri = entity_uri('taxonomy_term', $term_parent);
      $breadcrumb[] = l($term_parent->name, $uri['path'], $uri['options']);
    }
    $breadcrumb[] = $term->name;
  }

  // Lista de Experiencias
  if (arg(0) == 'experiencia' && is_numeric(arg(3))) {

    $tid = arg(3);
    if ($term = taxonomy_term_load($tid)) {
      $term_tid = $term->field_experience['und'][0]['tid'];
      $term_parent = taxonomy_term_load($term_tid);
        $uri = entity_uri('taxonomy_term', $term_parent);
        $uri_level = entity_uri('taxonomy_term', $term);
        $breadcrumb[] = l($term_parent->name, $uri['path'], $uri['options']);
        $breadcrumb[] = l($term->name, $uri_level['path'], $uri_level['options']);
        $breadcrumb[] = 'Experiencias';
    }
  }


  // Sorteo & Producto
  $paths = drupal_get_path_alias();
  $path_url = array();
  $path_url = explode('/', $paths);

  if(isset($path_url[3])){
    if ($path_url[3] == 'sorteo') {

      $tid = arg(3);
      $node = node_load(arg(1));

      if ($node->type == 'sorteo') {
        $tid = $node->field_experience_category['und'][0]['tid'];
        $parent = $node->field_experience['und'][0]['tid'];

        $term = taxonomy_term_load($tid);
        $term_parent = taxonomy_term_load($parent);

          $uri = entity_uri('taxonomy_term', $term_parent);
          $uri_level = entity_uri('taxonomy_term', $term);
          $breadcrumb[] = l($term_parent->name, $uri['path'], $uri['options']);
          $breadcrumb[] = l($term->name, $uri_level['path'], $uri_level['options']);
          $breadcrumb[] = $node->title;
      }
    }

    if ($path_url[3] == 'producto') {

      $node = node_load(arg(1));

      if ($node->type == 'product') {
        $tid = $node->field_experience_category['und'][0]['tid'];
        $parent = $node->field_experience['und'][0]['tid'];

        $term = taxonomy_term_load($tid);
        $term_parent = taxonomy_term_load($parent);

          $uri = entity_uri('taxonomy_term', $term_parent);
          $uri_level = entity_uri('taxonomy_term', $term);
          $breadcrumb[] = l($term_parent->name, $uri['path'], $uri['options']);
          $breadcrumb[] = l($term->name, $uri_level['path'], $uri_level['options']);
          $breadcrumb[] = $node->title;
      }
    }

    // Experiencia Final
    $paths_url = explode('/', $paths);
    if ($path_url[3] == 'experiencia') {

      $tid = arg(3);
      $node = node_load(arg(1));

      if (isset($node->type) && $node->type == 'experience') {
        $tid = $node->field_experience_category['und'][0]['tid'];
        $parent = $node->field_experience['und'][0]['tid'];

        $term = taxonomy_term_load($tid);
        $term_parent = taxonomy_term_load($parent);

          $uri = entity_uri('taxonomy_term', $term_parent);
          $uri_level = entity_uri('taxonomy_term', $term);
          $breadcrumb[] = l($term_parent->name, $uri['path'], $uri['options']);
          $breadcrumb[] = l($term->name, $uri_level['path'], $uri_level['options']);
          $breadcrumb[] = l('Experiencias', $path_url[0].'/'.$path_url[1].'/'.$path_url[2].'/'.$term->tid);
          $breadcrumb[] = $node->title;
          //experiencia/futbol/river/151
      }
    }
  }

  $breadcrumbBeneficios = true;
  if (arg(0) == 'benefit-experience') {
    $path_tag = drupal_get_query_parameters();
    
    $tid = null;
    if(isset($path_tag['field_tags_tid'])){
        
      $field_tag_id = strtolower($path_tag['field_tags_tid']);

      $tid = $field_tag_id == 'river' ? 151 : null;
      $tid = $field_tag_id == 'boca' ? 152 : null;
      $tid = $field_tag_id == 'lan' ? 1259 : null;
      $tid = $field_tag_id == 'peugeot' ? 1671 : null;
    }

    if($tid){
      $term = taxonomy_term_load($tid);
      $term_tid = $term->field_experience['und'][0]['tid'];
      $term_parent = taxonomy_term_load($term_tid);
      $uri = entity_uri('taxonomy_term', $term_parent);
      $uri_level = entity_uri('taxonomy_term', $term);
      $breadcrumb[] = l($term_parent->name, $uri['path'], $uri['options']);
      $breadcrumb[] = l($term->name, $uri_level['path'], $uri_level['options']);
      $breadcrumb[] = 'Beneficios';
    }
    else{
      $breadcrumbBeneficios = false;
    }
  }

  return !$breadcrumbBeneficios ? '<div class="breadcrumb">'. 'Beneficios' . '</div>' : '<div class="breadcrumb">'. implode(' &gt; ', $breadcrumb) . '</div>';
}

/**
 * Adds conditional CSS from the .info file.
 *
 * Copy of conditional_styles_preprocess_html().
 */
function boilerplate_add_conditional_styles() {
  // Make a list of base themes and the current theme.
  $themes = $GLOBALS['base_theme_info'];
  $themes[] = $GLOBALS['theme_info'];
  foreach (array_keys($themes) as $key) {
    $theme_path = dirname($themes[$key]->filename) . '/';
    if (isset($themes[$key]->info['stylesheets-conditional'])) {
      foreach (array_keys($themes[$key]->info['stylesheets-conditional']) as $condition) {
        foreach (array_keys($themes[$key]->info['stylesheets-conditional'][$condition]) as $media) {
          foreach ($themes[$key]->info['stylesheets-conditional'][$condition][$media] as $stylesheet) {
            // Add each conditional stylesheet.
            drupal_add_css(
              $theme_path . $stylesheet,
              array(
                'group' => CSS_THEME,
                'browsers' => array(
                  'IE' => $condition,
                  '!IE' => FALSE,
                ),
                'every_page' => TRUE,
              )
            );
          }
        }
      }
    }
  }
}


/**
 * Generate the HTML output for a menu link and submenu.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @return
 *   A themed HTML string.
 *
 * @ingroup themeable
 */

function boilerplate_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Adding a class depending on the TITLE of the link (not constant)
  $element['#attributes']['class'][] = drupal_html_class($element['#title']);
  // Adding a class depending on the ID of the link (constant)
  if (isset($element['#original_link']['mlid']) && !empty($element['#original_link']['mlid'])) {
    $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
  }
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Override or insert variables into theme_menu_local_task().
 */
function boilerplate_preprocess_menu_local_task(&$variables) {
  $link =& $variables['element']['#link'];

  // If the link does not contain HTML already, check_plain() it now.
  // After we set 'html'=TRUE the link will not be sanitized by l().
  if (empty($link['localized_options']['html'])) {
    $link['title'] = check_plain($link['title']);
  }
  $link['localized_options']['html'] = TRUE;
  $link['title'] = '<span class="tab">' . $link['title'] . '</span>';
}

/*
 *  Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */

function boilerplate_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;

}

function boilerplate_preprocess_views_view(&$vars) {
  if ($vars['view']->name == 'benefit_filters') {
    if (empty($vars['view']->exposed_input)) {
      $vars['rows'] = array();
      $vars['empty'] = bbva_benefit_filter_category();
    }
  }
}
function boilerplate_html_head_alter(&$head_elements) {
    unset($head_elements['system_meta_generator']);
}
