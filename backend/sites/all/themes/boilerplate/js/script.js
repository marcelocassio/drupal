/* Author: Dan Linn */
(function($) {
  $(window).resize(function(){
    if(!$(".mobileselect").length) {
      createMobileMenu();
    } else if ($(window).width()>=480) {
      $('#navigation ul').show();
      $('.mobileselect').hide();
    } else {
      $('#navigation ul').hide();
      $('.mobileselect').show();
    }
  });
  function createMobileMenu(){
    $('#navigation ul').mobileSelect({
      autoHide: true, // Hide the ul automatically
      defaultOption: "Navigation", // The default select option
      deviceWidth: 480, // The select will be added for screensizes smaller than this
      appendTo: '', // Used to place the drop-down in some location other than where the primary nav exists
      className: 'mobileselect', // The class name applied to the select element
      useWindowWidth: true // Use the width of the window instead of the width of the screen
    });
  }


  Drupal.behaviors.mobileMenu = {
    attach: function (context) {
      createMobileMenu();
    }
  }

  $(document).ready(function(){
   $window = $(window);
      // $('div[data-type="background"]').each(function() {
      //     var $scroll = $(this);

      //     $(window).scroll(function() {
      //         var yPos = -($window.scrollTop() / $scroll.data('speed'));
      //         var coords = '50% ' + yPos + 'px';
      //         $scroll.css({ backgroundPosition: coords });
      //     });
      // });

      $('#bbva-search-form #edit-keyword').attr('placeholder', '¿Qué estás buscando?');
      
      $('#bbva-search-form--2 #edit-keyword--2').attr('placeholder', '¿Qué estás buscando?');

      $('#menu-responsive .btn-responsive').click(function() {
        $('.cabezal-ingresar').removeClass('active');
        jQuery('.dropdown').hide();
        $('#block-menu-menu-responsive').toggle();
        if (document.documentElement.clientWidth < 961){
          var dropdownContent = document.getElementById('dropdown-content');
          var newMarginTop = 0;
          if (dropdownContent != null) {
            var newMarginTop = dropdownContent.clientHeight;
          }
          $('.region-slideshow').css('margin-top', newMarginTop);
        } else {
          $('.region-slideshow').css('margin-top',0);
        }
      });

      /** Check all or none checkbox **/
      /*$('#all #checkAll').click(function() {
        $('.form-item-zonas input[type="checkbox"]').attr('checked', true);
      });

      $('#all #checkNone').click(function() {
        $('.form-item-zonas input[type="checkbox"]').attr('checked', false);
      });

      $('#all #checkAll2').click(function() {
        $('.form-item-rubros input[type="checkbox"]').attr('checked', true);
      });

      $('#all #checkNone2').click(function() {
        $('.form-item-rubros input[type="checkbox"]').attr('checked', false);
      });
*/
      $('.jcarousel-experience').jcarousel({
        animation: 200,
        scroll: 1,
        visible: 3,
        auto: 0,
        easing: 'linear',
        buttonNextHTML: '<div>next</div>',
        buttonPrevHTML: '<div>prev</div>',
        itemFallbackDimension: 3000,
        wrap: 'both'
      });

      $('.jcarousel-20').jcarousel({
        animation: 200,
        scroll: 1,
        visible: 1,
        auto: 0,
        easing: 'linear',
        buttonNextHTML: '<div>next</div>',
        buttonPrevHTML: '<div>prev</div>',
        itemFallbackDimension: 3000,
        wrap: 'both'
      });

      if($('.jcarousel-experience li').length == 1){
        $('.jcarousel-prev, .jcarousel-next').hide();
      }
      if($('.jcarousel-20 li').length == 1){
        $('.jcarousel-prev, .jcarousel-next').hide();
      }
	  
	  valorAlto = 0;
    $('.view-solo2 .group-fields-promotion').each(function(i,obj){
      cucarda = $(obj).html();
      if(valorAlto < cucarda) {
        valorAlto = cucarda;
      }
    });
    $('.featured-'+valorAlto).show();

    /** FILTERS BENEFIT **/
    $('.views-widget-filter-field_benefit_category_tid').hide();
    $('.views-widget-filter-field_benefit_category_tid_1 .bef-checkboxes').prepend('<div class="view-more">Ver m&aacutes</div>');
    $('.views-widget-filter-field_benefit_category_tid_1 .view-more').click(function() {
      $('.views-widget-filter-field_benefit_category_tid').toggle();
    });

    $('.views-widget-filter-field_locality_province_tid_1').hide();
    $('.views-widget-filter-field_locality_province_tid .bef-checkboxes').prepend('<div class="view-more">Ver m&aacutes</div>');
    $('.views-widget-filter-field_locality_province_tid .view-more').click(function() {
      $('.views-widget-filter-field_locality_province_tid_1').toggle();
    });

    /** Mover Elementos en Subscription **/
    /*$('.form-item-zonas .description').prependTo('.form-item-zonas');
    $('.form-item-zonas > label').prependTo('.form-item-zonas');
    $('.form-item-rubros .description').prependTo('.form-item-rubros');
    $('.form-item-rubros > label').prependTo('.form-item-rubros');*/

    /** Mover elementos del Footer **/
    $('#block-bbva-footer_mobile .go-mobile').appendTo('#block-bbva-footer_mobile h2');
    $('#block-bbva-footer_video .go-video').appendTo('#block-bbva-footer_video h2');

    /**Tabs Experiencia Full **/
    $('.tab-video').click(function() {
      $('#video-header').css('display', 'block');
      $('.tab-video').addClass('active');
      $('#experience-carrusel').css('display', 'none');
      $('.tab-gallery').removeClass('active');
    });
    $('.tab-gallery').click(function() {
      $('#video-header').css('display', 'none');
      $('.tab-video').removeClass('active');
      $('#experience-carrusel').css('display', 'block');
      $('.tab-gallery').addClass('active');
    });
  });

  
  
  
})(jQuery);





