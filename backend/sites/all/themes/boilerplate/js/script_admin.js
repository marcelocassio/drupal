/* Author: Dan Linn */
(function ($) {

  Drupal.behaviors.admin = {
    attach: function (context, settings) {
      // Al cambiar el select muestra o no el field collection interno   
      $('#edit-field-term-items .field-name-field-term-template .form-select').each(function() {

        if($(this).val() == 2){
          $(this).closest('tr').find('.field-name-field-term-items-multiple').show();
        }
        if($(this).val() != 2){
          $(this).closest('tr').find('.field-name-field-term-items-multiple').hide();
        }

        $(this).change(function() {
          if($(this).val() == 2){
            $(this).closest('tr').find('.field-name-field-term-items-multiple').show();
          }
          if($(this).val() != 2){
            $(this).closest('tr').find('.field-name-field-term-items-multiple').hide();
          }
        });
      });
    }
  };
}(jQuery));
