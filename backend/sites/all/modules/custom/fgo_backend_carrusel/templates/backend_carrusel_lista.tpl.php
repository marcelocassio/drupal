<div id="admin_container">
    <h1>Administración de Carruseles</h1>
    <div v-if="loading" class="spinner_backend" v-cloak>
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    <vue-good-table v-else
    :columns="formattedColumns"
    :rows="rows"
    style-class="vgt-table striped condensed"
    :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: true, perPage: 40, ofLabel: 'de'}"
    :search-options="{ enabled: false, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
        
        <div v-slot:emptystate>
            No hay carruseles con ese criterio
        </div>
        <template v-slot:table-row="props">
            <div v-if="props.column.field == 'botones'">
                <div v-html="props.row.botones"></div>
            </div>
            <div v-else>
                {{props.formattedRow[props.column.field]}}
            </div>
        </template>
    </vue-good-table>
    <vuedals></vuedals>
</div>

<script>
    const link = '<?php echo $base_url?>/API/v1/backend/carrusel';
    const linkAdd = '<?php echo $base_url?>/admin/v2/carrusel/agregar';
    const linkBorrar = '<?php echo $base_url?>/API/v1/backend/carrusel/borrar/';
    const FGO_BACKEND_IMG_DEFAULT = '<?php echo $base_url?>/static/media/logogo.1cb228c2.svg';

    // Contantes para el modal
    const Bus = Vuedals.Bus;
    const Component = Vuedals.Component;
    const Plugin = Vuedals.default;

      Vue.use(Plugin);

 

        new Vue({
            el: '#admin_container',
            data: {
                rows: [],
                idCarruselHome: null,
                loading: false,
                columns: [
                {
                    label: 'Nombre',
                    field: 'nombre',
                    type: 'text'
                },
                {
                    label: 'Rubro',
                    field: 'rubro',
                    type: 'text'
                },
                {
                    label: 'Editar',
                    field: 'botones',
                    width: '100px',
                    globalSearchDisabled: true,
                },
                ]
            },
            methods:{
                getCarruseles(){
                    this.loading = true;
                    axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200 && response.data.code === 0){
                            this.rows = response.data.data;
                        }else{
                            this.rows = null;
                        }
                    })
                    .catch(error => {
                        this.rows = null;
                    })
                    .finally(() => this.loading = false)
                },
                renderData(rows) {
                    if (rows){
                        rows.map(item => {
                            item.botones = ' <div class="accion_editar_carrusel"><a href="<?=$base_url;?>/admin/v2/carrusel/editar/'+item.idCarruselHome+'" class="btn btn-outline-info btn-md"> Editar </a></div>';
                        });
                    }
                },
                newItem(){
                    window.location.href = linkAdd;
                },
                openDeleteModal(idCarruselHome) {
                    this.idCarruselHome = idCarruselHome;
                    Bus.$emit('new', {
                        title: 'Borrar carrusel',
                        component: borrarCarrusel
                    });
                },
                _parseImage(img) {
                    return img.replace("public://","/fgo/sites/default/files/");
                }
            },
            computed:{
                formattedColumns(){
                    if(this.rows){
                        this.renderData(this.rows);
                        return this.columns;
                    }
                }
            },
            mounted() {
                this.getCarruseles();
            }
        });
</script>
