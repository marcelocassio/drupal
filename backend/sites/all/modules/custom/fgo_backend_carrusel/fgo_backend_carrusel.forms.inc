<?php
/**
 * Metodos para los formularios de edicion y creacion de carruseles
 */

function fgo_backend_carrusel_agregar_form()
{
    return fgo_backend_carrusel_display_form();
}

function fgo_backend_carrusel_editar_form($form_state, $build)
{
    $carrusel_id = $build["build_info"]["args"][0];
    return fgo_backend_carrusel_display_form($carrusel_id);
}

function fgo_backend_carrusel_display_form($carrusel_id = null)
{
    $_default = array(
        'idCarruselHome' => '',
        'nombre' => '',
        'idRubro' => '',
        'publicado' => false,

    );

    if (!is_null($carrusel_id)) {
        $carruselBo = new \Fgo\Bo\CarruselBo($carrusel_id);
        $_default['idCarruselHome'] = $carrusel_id;
        $_default['nombre'] = $carruselBo->nombre;
        $_default['idRubro'] = $carruselBo->idRubro;
        $_default['publicado'] = (bool) $carruselBo->publicado;
    }

    $form['redirect'] = false;

    $rubro = new \Fgo\Bo\RubroBo();
    $lista = $rubro->listar(array("nombre"=> "ASC"));
    $options = array();

    foreach ($lista as $row) {
        $options[$row->idRubro] = $row->nombre;
    }

    $form['#attributes']['autocomplete'] = "off";

    $form['#validate'][] = 'fgo_backend_carrusel_form_validate';

    $form['idCarruselHome'] = array(
        '#type' => 'hidden',
        '#default_value' => $_default['idCarruselHome']
    );

    $form['grupo_dato'] = array(
        '#type' => 'fieldset',
        '#title' => t('<strong><i>datos</i></strong>')
    );

    $form['grupo_dato']['nombre'] = array(
        '#title' => t('Nombre'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 1,
        '#default_value' => $_default['nombre']
    );

    $form['grupo_rubro'] = array(
        '#type' => 'fieldset',
        '#title' => t('<strong><i>Rubro</i></strong>')
    );

    $form['grupo_rubro']['idRubro'] = array(
        '#type' => 'select',
        '#description' => 'Elija si el rubro para el carrusel',
        '#default_value' => $_default['idRubro'],
        '#options' => $options
    );

    $form['grupo_rubro']['publicado'] = array(
      '#title' => t('Publicado (sin tilde no se visualiza).'),
      '#type' => 'checkbox',
      '#default_value' => $_default['publicado'],
    );


    $form['guardar'] = array(
        '#type' => 'submit',
        '#id' => 'guardar_slide',
        '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
        '#value' => 'Guardar',
        '#submit' => array('fgo_backend_carrusel_submit')
    );

    return $form;
}

function fgo_backend_carrusel_form_validate(&$form, &$form_state)
{
    $carruselBo = new \Fgo\Bo\CarruselBo($form_state['values']['idCarruselHome']);
    $carruselBo->idRubro = $form_state['values']['idRubro'];
    $carruselBo->nombre = $form_state['values']['nombre'];
    $carruselBo->publicado = $form_state['values']['publicado'];

    try {
        $carruselBo->guardar();
    } catch (\Fgo\Bo\DatosInvalidosException $e) {
        $errores = $e->getErrores();
        foreach ($errores as $error_campo => $error_descripcion) {
            form_set_error($error_campo, $error_descripcion);
        }
    }
}

function fgo_backend_carrusel_submit(&$form, &$form_state)
{
    $form_state['redirect'] = url('../admin/v2/carrusel/lista');
}
