<?php
function bbva_entity_shop_obtener_imgs_rubro() {

  $tree = taxonomy_get_tree(taxonomy_vocabulary_machine_name_load('category')->vid);
  $term_tree = array();

  foreach ($tree as $term) {
    $fid = array();

    $term_to_img = taxonomy_term_load($term->tid);
    $all_fids = $term_to_img->field_category_image['und'];
    if (!empty($all_fids)) {
      foreach ($all_fids as $value_fid) {
        $fid[] = $value_fid['fid'];
      }
    }

    //Si es un subrubro
    $tid_padre = null;
    if ($term->parents[0]) {
      $tid_padre = $term->parents[0];
    }

    $term_tree[$term->tid]['tid_padre'] = $tid_padre;
    $term_tree[$term->tid]['fid'] = $fid;

  }
  return $term_tree;
}

function bbva_entity_shop_imagen_from_rubro($array_rubros, $all_rubro_imgs){
  $fids_category = null;
  $tid_padre = null;
  $count_rubros = count($array_rubros);

  // - Si el primer rubro cargado es un padre
  // Si tenemos una categoria, se toma la imagen de esa.
  // - Se toman los fids del hijo
  $fids_category = $all_rubro_imgs[$array_rubros[0]]['fid'];

  // Si hay mas de una categoria, se toma el padre de la primera categoria
  if ($count_rubros >= 2 ) { // Se toman los fids del padre
    if (isset($all_rubro_imgs[$array_rubros[0]]['tid_padre'])) {
      $tid_padre = $all_rubro_imgs[$array_rubros[0]]['tid_padre'];
      $fids_category = bbva_get_imagenes_rubro_from_tid($tid_padre);
    }
  }
  return $fids_category;
}