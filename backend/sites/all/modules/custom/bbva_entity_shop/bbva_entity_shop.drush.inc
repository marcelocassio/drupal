<?php

/*
 * Implements hook_drush_command().
 */
function bbva_entity_shop_drush_command() {

  $items['vuelco-shop'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('shopsImport'),
  );
  $items['delete-shop'] = array(
    'description' => 'Elimina todos los shops de drupal',
    'aliases' => array('deleteShop'),
  );

  return $items;
}



/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_entity_shop_vuelco_shop() {
  import_shop_suip();
  drupal_set_message("Import Shop echo", 'status', FALSE);
}

function drush_bbva_entity_shop_delete_shop() {
  delete_shop();
  drupal_set_message("eliminacion hecha", 'status', FALSE);
}