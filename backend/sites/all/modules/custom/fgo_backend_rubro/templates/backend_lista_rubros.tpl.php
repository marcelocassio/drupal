<div id="admin_container">
  <h1>Administración de Rubros</h1>
  <div v-if="loading" class="spinner_backend" v-cloak>
    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
  </div>
  <vue-good-table v-else
    :columns="formattedColumns"
    :rows="rubros_ordenados"
    style-class="vgt-table striped condensed"
    :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: false, perPage: 50, ofLabel: 'de'}"
    :search-options="{ enabled: true, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
      <div v-slot:emptystate>
        No hay rubros con ese criterio
    </div>
    <template v-slot:table-row="props">
        <div v-if="props.column.field == 'botones' ">
            <div v-html="props.row.botones"></div>
        </div>
        <div v-else-if="props.column.field == 'nombre' && props.row.parent">
            <span class="rubro-nombre">{{props.row.nombre}}</span>
        </div>
        <div v-else-if="props.column.field == 'nombre'">
            <span class="subrubro-nombre">{{props.row.nombre}}</span>
        </div>
        <div v-else-if="props.column.field == 'estadoPdm'">
            <span>{{props.row.estadoPdm}}</span>
        </div>
        <div v-else>
            {{props.formattedRow[props.column.field]}}
        </div>
    </template>

  </vue-good-table>
</div>
<script>
    var link = 'https://'+window.location.host+'/fgo/API/v1/backend/rubros';

    new Vue({
        el: '#admin_container',
        data: {
            rubros: [],
            rubros_ordenados: [],
            loading: false,
            columns: [
                {
                    label: 'ID Rubro',
                    field: 'id',
                    type: 'number',
                    tdClass: 'text-center',
                    width: '100px'
                },{
                    label: 'ID Rubro Suip',
                    field: 'idSuip',
                    type: 'number',
                    tdClass: 'text-center',
                    width: '150px'
                },{
                    label: 'Estado PDM',
                    field: 'estadoPdm',
                    width: '150px'
                },{
                    label: 'Nombre de rubros y subrubros',
                    field: 'nombre',
                    tdClass: 'text-left'
                },{
                    label: 'Acciones',
                    field: 'botones',
                    filterable: false,
                    globalSearchDisabled: true
                },
            ]
        },
        methods:{
            getRubros(){
                this.loading = true;
                axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200){
                            this.rubros = Object.values(response.data.data);
                        }else{
                            this.rubros = null;
                        }
                    })
                    .catch(error => {
                        this.rubros = null;
                    })
                    .finally(() => this.loading = false)
            },
            renderData(rubros) {
                if (Array.isArray(rubros)) {
                    rubros.map(item => {
                        item.botones = '<a href="rubros/editar/' + item.id + '" class="btn btn-outline-info btn-md"> Editar </a>';
                        item.parent = true;
                        this.rubros_ordenados.push(item);

                        if (item.subrubros) {
                            var subrubros = Object.values(item.subrubros);
                            subrubros.map(itemSubrubro => {
                                itemSubrubro.botones = ' <a href="rubros/editar/' + itemSubrubro.id + '" class="btn btn-outline-info btn-md"> Editar </a>';
                                itemSubrubro.parent = false;
                                this.rubros_ordenados.push(itemSubrubro);
                            });
                        }
                    });
                }
            },
        },
        computed:{
            formattedColumns(){
                if(this.rubros) {
                    this.renderData(this.rubros);
                    return this.columns;
                }
            }
        },
        mounted() {
            this.getRubros();
        }
    });
</script>
