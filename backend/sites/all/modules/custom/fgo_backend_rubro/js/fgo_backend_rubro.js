(function($) {
  Drupal.behaviors.fgo_backend_rubro = {
    attach: function (context, settings) {
      //$('#fgo-backend-rubro-editar-form').ajaxComplete(function(event, xhr, settings) {
      $(document).ready(function() {
        var inputs_hidden = Array();
        $.each($(".fieldset-wrapper > div", this), function(key, val){
          if($("input[type='hidden']", val).val() < 1) {
            inputs_hidden.push(val);
            $(val).hide();
          } else {
            inputs_hidden = Array();
            $(val).show();
          }
        });
        $(inputs_hidden[0]).show();
      });
    }
  };
})(jQuery);
