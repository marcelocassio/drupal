<?php

function fgo_backend_rubro_editar_form($form_state, $build) {
  $id = $build["build_info"]["args"][0];

  $rubro = new \Fgo\Bo\RubroBo($id);

  $imagenes = array();

  if (!empty($rubro->imagenes->data)){
    foreach ($rubro->imagenes->data as $imagen){
      $imagenes[] = $imagen->datosImagen;
    }
  }

  // Agrego uno vacio para que aparezca el input
  $imagen = new \stdClass();
  $imagen->datosImagen->fid = 0;
  $imagenes[] = $imagen;

  // Creo varios input para tener disponibles (los oculto por js)
  $limit = 50;
  $q_imagenes = count($rubro->imagenes->data);

  if($q_imagenes < $limit) {
    $imagenes_vacias = array_fill(0, $limit - $q_imagenes, $imagen);
    $imagenes = array_merge($imagenes, $imagenes_vacias);
  }

  $form['#attributes']['enctype'] = "multipart/form-data";

  $form['#tree'] = TRUE; // Porque tengo items con el mismo nombre

  $form['idRubro'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($id) ? $id : ''
  );

  $form['grupo_imagen_rubro'] = array(
    '#type' => 'fieldset',
    '#title' => t('<i>Imagenes del rubro <strong>' . $rubro->nombre . '</strong></i>'),
  );

  foreach ($imagenes as $imagen) {
    $form['grupo_imagen_rubro']['idImagen'][] = array(
      '#type' => 'managed_file',
      '#name' => 'custom_content_block_image',
      '#size' => 40,
      '#default_value' => isset($imagen->fid) ? $imagen->fid : null,
      '#description' => t("Los archivos deben ser menores que <b>2 MB</b>. <br> Tipos de archivo permitidos: <b>png jpg jpeg</b>."),
      '#upload_location' => 'public://',
      '#theme' => 'preview_img',
      '#upload_validators' => array(
        'file_validate_is_image' => array(),
        'file_validate_extensions' => array('png jpg jpeg'),
        'file_validate_size' => array(2 * 1024 * 1024),
      ),
    );
  }

  $options = $rubro::estadosPdm();

  $form['estadoPdm'] = array(
    '#title' => t('Portal de Marcas'),
    '#type' => 'select',
    '#description' => 'Elija si el rubro es visible para el portal de marcas, si es un rubro observado o rechazado, o si no debe ser visible',
    '#options' => $options,
    '#default_value' => $rubro->estadoPdm,
  );

  global $user;

  if(!in_array('PDM', $user->roles) && !in_array('administrator', $user->roles)) {
    $form['estadoPdm']['#access'] = FALSE;
  }

  $form['guardar'] = array(
    '#type' => 'submit',
    '#id' => 'guardar_imagen',
    '#value' => 'Guardar',
    '#submit' => array('fgo_backend_rubro_editar_form_submit_now')
  );

  return $form;
}

function fgo_backend_rubro_editar_form_validate(&$form, &$form_state) {
  try {
    $rubroBo = new \Fgo\Bo\RubroBo($form_state['values']['idRubro']);
    $rubroBo->estadoPdm = $form_state['values']['estadoPdm'];
    if($rubroBo->idRubro < 1) {
      form_set_error("idRubro", "Rubro no encontrado");
    }
    $rubroImagenBo = new \Fgo\Bo\RubroImagenBo();
    $rubroImagenBo->idRubro = $rubroBo->idRubro;
    if(!empty($form_state['values']['grupo_imagen_rubro']['idImagen'])) {
      foreach ($form_state['values']['grupo_imagen_rubro']['idImagen'] as $imagen_form) {
        if($imagen_form > 0) {
          // FidBo
          $fileimagen = file_load($imagen_form);
          $fileimagen->status = FILE_STATUS_PERMANENT;
          $imagen_data = file_save($fileimagen);
          file_usage_add($fileimagen, 'fgo_backend_rubro', 'imagen_rubro', $imagen_data->fid);
          $fidBo = new \Fgo\Bo\FidBo($imagen_data->fid);
          $fidBo->guardar();
          // ImagenBo
          $imagenBo = new \Fgo\Bo\ImagenBo();
          $imagenBo->codigoTipoImagen = $imagenBo::TIPO_IMAGEN_PORTAL;
          $imagenBo->datosImagen = $fidBo;
          $imagenBo->guardar();
          // rubroImagenBo
          $rubroImagenBo->data[] = $imagenBo;
        }
      }
      $rubroImagenBo->guardar();
      $rubroBo->imagenes = $rubroImagenBo;
      $rubroBo->guardar();
    }
  } catch (Exception $e) {
    if(method_exists($e, "getErrores")) {
      $errores = $e->getErrores();
    } else {
      $errores[] = $e->getMessage();
    }
    foreach ($errores as $error_campo => $error_descripcion) {
      form_set_error($error_campo, $error_descripcion);
    }
  }
}

function fgo_backend_rubro_editar_form_submit_now(&$form, &$form_state) {
  $form_state['redirect'] = url('../admin/v2/rubros');
}
