<?php

/**
 * Admin settings page.
 */
function bbva_messages_admin($form, $form_state) {

  $form['bbva_push_cupon_vencimiento_txt_short'] = array(
  '#title' => t('Mensaje push corto'),
    '#type' => 'textfield',
    '#maxlength' => 160,
    '#size' => 100,
    '#default_value' => variable_get('bbva_push_cupon_vencimiento_txt_short'),
    '#description' => t('Es el texto que se previsualiza en la notificación que llega al dispositivo.<br/>
Comodines habilitados para la carga:<br/>Nombre del comercio del cupón "<strong>:nombre_comercio</strong>"<br/>Descuento del cupón "<strong>:descuento</strong>"<br/> '),
  );
  $form['bbva_push_cupon_vencimiento_txt_ext'] = array(
  '#title' => t('Mensaje push extendido'),
    '#type' => 'textfield',
    '#maxlength' => 200,
    '#size' => 100,
    '#default_value' => variable_get('bbva_push_cupon_vencimiento_txt_ext'),
   '#description' => t('Es el texto que se visualiza en el panel de notificaciones adentro de la aplicación a continuación del texto corto.<br/>Comodines habilitados para la carga:<br/>Nombre del comercio del cupón "<strong>:nombre_comercio</strong>"<br/>Descuento del cupón "<strong>:descuento</strong>"<br/> '),
  );
  $form['bbva_push_cupon_vencimiento_dias_antes_ven'] = array(
  '#title' => t('Momento de envió'),
    '#type' => 'textfield',
    '#maxlength' => 1,
    '#size' => 1,
    '#default_value' => variable_get('bbva_push_cupon_vencimiento_dias_antes_ven'),
   '#description' => t('Configuración del momento del envió del mensaje push respecto de los días que faltan para el vencimiento del cupón.'),
  );

  return system_settings_form($form);
}


