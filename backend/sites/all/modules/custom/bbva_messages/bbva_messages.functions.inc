<?php

  function bbva_messages_validar_imagen($field_imagen) {
    $error = FALSE;
    $file_value = $field_imagen['und'][0];
    if($file_value['fid'] > 0) {
      try {
        $file = file_load($file_value['fid']);
        $uri = $file->uri;
        $path = drupal_realpath($uri);
        // Chequeo el ratio de la imagen
        $img_data = image_get_info($uri);
        if(!empty($img_data)) {
          if($img_data['width'] / $img_data['height'] != 16/9 || $img_data['width'] / $img_data['height'] != 2) {
            $error = 'La imagen no tiene la relación de aspecto correcta (16:9 o 2:1).';
          }
          if($img_data['width'] < 800 || $img_data['width'] > 1200) {
            $error = 'El ancho de la imagen no tiene cumple los limites válidos (800px mínimo, 1200px máximo).';
          }
          if($img_data['height'] < 400 || $img_data['height'] > 600) {
            $error = 'El alto de la imagen no tiene cumple los limites válidos (400px mínimo, 600px máximo).';
          }
        }
      } catch (Exception $e) {
        $error = "Hubo un error (" . $e->getMessage() . ")";
      }
    }

    return $error;
  }