(function($) {

  Drupal.behaviors.bbva_messages = {
    attach: function (context, settings) {
      $("#edit-submit").click(function(event) {
        event.preventDefault();

        let textDeepLinking, seccionCTA;
        var mensajeCorto = $("#edit-field-message-plan-push-und-0-value").val();
        var mensajeExtendido = $("#edit-field-message-plan-long-text-und-0-value").val();
        var deepLinking = $("#edit-field-call-to-action-und-0-field-tipo-und");
        var newCTA = $("#edit-calltoactionseccion").val();

        textDeepLinking = "-<b>Sin DeepLinking</b>";

        if (deepLinking.val() != "_none") {
          seccionCTA = deepLinking.find(":selected").text();
        }

        if(!seccionCTA){
          if (newCTA != 0 && newCTA != 5) {
              seccionCTA = $("#edit-calltoactiondireccion-" + newCTA).val();
          }else if(newCTA == 5){
              seccionCTA = $("#edit-calltoactiondireccion-5").find(":selected").text();
          }
        }

        if (seccionCTA != ""){
            textDeepLinking = "-<b>DeepLinking va a</b>: " + seccionCTA;
        }

        $("#messageconfirmation").html("Querés enviar el siguiente push?: <br>-<b>Mensaje Corto</b>: " + mensajeCorto + "<br>-<b>Mensaje Extendido</b>: " + mensajeExtendido + "<br>" + textDeepLinking);

        $( "#bbva_messages_dialogo" ).dialog({
          autoOpen: false,
          title: "Confirmación",
          dialogClass: "alert",
          modal: true,
          show: "blind",
          resizable: false,
          open: function(event, ui) {
              $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
          },
          hide: "explode",
          buttons: {
            Aceptar: function() {
              $( "html body" ).css({
                "cursor": "wait",
                "background-color": "#000000",
                "opacity": "0.5"
              });
              $( "#push-on-demand-node-form" ).submit();
            },
            Cancelar: function() {
              $( this ).dialog( "close" );
            }
          }
        });

        $("#bbva_messages_dialogo").dialog("open");
      });
    }
  }

  $(document).ready(function() {
    $(".location-wrapper").remove();
  });


})(jQuery);
