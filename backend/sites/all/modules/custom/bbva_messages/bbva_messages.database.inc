<?php

function db_find_sus_push_for_date($date) {
  $query = db_select('field_data_field_message_plan_date_send', 'ds');
  $query->leftJoin('field_data_field_field_message_plan_segment','ps', 'ps.entity_id = ds.entity_id');
  $query->leftJoin('field_data_field_message_plan_segment_sc','pss', 'pss.entity_id = ds.entity_id');

  $query = $query->condition('ds.field_message_plan_date_send_value', $date);
  $query = $query->condition(
    db_or()
    ->condition('ps.field_field_message_plan_segment_value', 1)
    ->condition('pss.field_message_plan_segment_sc_value', 1)
  );

  $query = $query->fields('ds', array('entity_id'));

  $result = $query->execute()->fetchAll();

  return $result;
}

function _bbva_messages_find_push_on_demand(){
  $first_minute = mktime(0, 0, 0, date("n"), 1);
  $last_minute = mktime(23, 59, 0, date("n") + 1, 0);

  $query = db_select('node', 'n');
  $query->addField('n', 'created', 'fecha');
  $query = $query->condition('created', array($first_minute, $last_minute), 'BETWEEN');
  $query = $query->condition('type', 'push_on_demand');

  $result = $query->execute()->fetchAll();

  return $result;
}
