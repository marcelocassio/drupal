<?php

/*************  Funciones de Validacion solo para Update profile  ************/

function bbva_api_login_profile_fields_validaciones($fields, $es_cliente){

  if (!$es_cliente && empty($fields['nombre'])) {
    $response->codigo = 3;
    $response->description = 'Debe escribir un nombre.';
  } elseif (!$es_cliente && empty($fields['apellido'])) {
    $response->codigo = 4;
    $response->description = 'Debe escribir un apellido.';
  } elseif (!$es_cliente && empty($fields['sexo'])) {
    $response->codigo = 5;
    $response->description = 'Debe seleccionar un sexo.';
  } elseif (!$es_cliente && empty($fields['email'])) {
    $response->codigo = 6;
    $response->description = 'Debe escribir un email.';
  } elseif (!$es_cliente && !valid_email_address($fields['email'])) {
    $response->codigo = 6;
    $response->description = 'El email ingresado es incorrecto.';
  } elseif (!$es_cliente && empty($fields['codigo_compania']) && !$es_cliente) {
    $response->codigo = 7;
    $response->description = 'Debe seleccionar una compania.';
  } elseif (!$es_cliente && empty($fields['codigo_area'])) {
    $response->codigo = 8;
    $response->description = 'Debe seleccionar un area.';
  } elseif (!$es_cliente && empty($fields['celular'])) {
    $response->codigo = 9;
    $response->description = 'Debe escribir un celular.';
  } elseif (!$es_cliente && empty($fields['dia'])) {
    $response->codigo = 10;
    $response->description = 'Debe seleccionar un dia.';
  } elseif (!$es_cliente && empty($fields['mes'])) {
    $response->codigo = 11;
    $response->description = 'Debe seleccionar un mes.';
  } elseif (!$es_cliente && empty($fields['anio'])) {
    $response->codigo = 12;
    $response->description = 'Debe seleccionar un año.';
  } elseif(empty($fields['rubros'])){
    $response->codigo = 15;
    $response->description = 'No selecciono ningun rubro.';
  }elseif(empty($fields['zonas'])){
    $response->codigo = 16;
    $response->description = 'No selecciono ninguna zona.';
  } elseif (!$es_cliente) {
    $suscripcionResponse = bbva_profile_get_subscription();
	$suscripcionRequest = bbva_profile_update_cliente_get_request_mobile($fields, $es_cliente);

	$area_res = $suscripcionResponse->codigoDeArea;
	$celular_res = $suscripcionResponse->numeroDeCelular;
	$area_req = $suscripcionRequest->codigoDeArea;
	$celular_req = $suscripcionRequest->numeroDeCelular;

	if ($area_res != $area_req || $celular_res != $celular_req) {
		if (!bbva_subscription_validar_longitud_area_numero($area_req, $celular_req)) {
			$response->codigo = 17;
			$response->description = 'El número de celular es incorrecto. ';
		} else if (bbva_subscription_exist_celular($area_req, $celular_req)){
			$response->codigo = 18;
			$response->description = 'El celular ya existe. ';
		}
	}
  }else {
    $response->codigo = 0;
    $response->description = 'Ok.';
  }

  return $response;
}


/*************  Funciones de Validacion solo para Subscriptions  ************/

function bbva_api_login_fields_validaciones($fields, $dice_ser_cliente){

  if (!$dice_ser_cliente) {
    $pass1 = $fields['pass'];
    $pass2 = $fields['pass1'];
    $errors = bbva_profile_validate_pass_no_cliente($pass1, $pass2);
    if (!empty($errors)) {
      foreach ($errors as $key => $value) {
        $response->codigo = 14;
        $response->description = $value;
      }
      return $response;
    }
  }

  $tipo_doc = $fields['doc_type'];
  $numero_doc = $fields['doc'];
  $nationality = $fields['nationality'];

  if (!is_numeric($fields['doc'])) {
    $response->codigo = 1;
    $response->description = 'El campo Número de Documento debe contener solo números.';
  } elseif (empty($fields['doc_type'])) {
    $response->codigo = 2;
    $response->description = 'Debe seleccionar un tipo.';
  } elseif (empty($fields['nombre'])) {
    $response->codigo = 3;
    $response->description = 'Debe escribir un nombre.';
  } elseif (empty($fields['apellido'])) {
    $response->codigo = 4;
    $response->description = 'Debe escribir un apellido.';
  } elseif (!$dice_ser_cliente && !empty($nationality) && !bbva_subscription_validar_nacionalidad($tipo_doc, $numero_doc, $nationality)) {
    $response->codigo = 6;
    $response->description = 'El número de documento no es válido para la nacionalidad ingresada';
  } elseif (empty($fields['email'])) {
    $response->codigo = 6;
    $response->description = 'Debe escribir un email.';
  } elseif (!valid_email_address($fields['email'])) {
    $response->codigo = 6;
    $response->description = 'El email ingresado es incorrecto.';
  } elseif (empty($fields['compania'])) {
    $response->codigo = 7;
    $response->description = 'Debe seleccionar una compania.';
  } elseif (empty($fields['area'])) {
    $response->codigo = 8;
    $response->description = 'Debe seleccionar un area.';
  } elseif (!bbva_subscription_is_valid_code_area($fields['area'])) {
    $response->codigo = 8;
    $response->description = 'El código de área es incorrecto o verificá que el mismo no comience con 0.';
  } elseif (empty($fields['celular'])) {
    $response->codigo = 9;
    $response->description = 'Debe escribir un celular.';
  } elseif (!bbva_subscription_validar_longitud_area_numero($fields['area'], $fields['celular'])) {
    $response->codigo = 9;
    $response->description = 'El número de celular es incorrecto.';
  } elseif (!empty($fields['dia']) || !empty($fields['mes']) || !empty($fields['ano'])) {
  	if (empty($fields['dia'])) {
  		$response->codigo = 10;
  		$response->description = 'Debe seleccionar un dia para completar la fecha.';
  	} elseif (empty($fields['mes'])) {
  		$response->codigo = 11;
  		$response->description = 'Debe seleccionar un mes para completar la fecha.';
  	} elseif (empty($fields['ano'])) {
  		$response->codigo = 12;
  		$response->description = 'Debe seleccionar un año para completar la fecha.';
  	}
  } elseif (!$dice_ser_cliente && empty($fields['pass'])) {
    $response->codigo = 13;
    $response->description = 'Debe escribir una contraseña.';
  } elseif (!$dice_ser_cliente && ($fields['pass'] != $fields['pass1'])) {
    $response->codigo = 14;
    $response->description = 'Las contrasenas no coinciden.';
  } elseif(empty($fields['rubros'])){
    $response->codigo = 15;
    $response->description = 'No selecciono ningun rubro.';
  }elseif(empty($fields['zonas'])){
    $response->codigo = 16;
    $response->description = 'No selecciono ninguna zona.';
  }else {

    $response->codigo = 0;
    $response->description = 'Ok.';
  }

  return $response;
}


function bbva_api_login_recovery($doc){

  $hay_error = false;
  $error_message = '';
  if (empty($doc)) {
    $error_message = t('Debe completar el número de documento');
    form_set_error('number', $error_message);
    $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
    $hay_error = true;
  } else if (!is_numeric($form_state['values']['number'])) {
    $error_message = t('Ingrese solo números');
    form_set_error('number', $error_message);
    $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
    $hay_error = true;
  }

  if (!$hay_error) {
	try {
		$map_doc_type__altamira = bbva_login_get_document_altamira_type_list();
		$document_type = $form_state['values']['document_type'];
		$document_number = $form_state['values']['number'];

		$noClienteResponse = bbva_login_existe_no_cliente($document_type, $document_number);
		if ($noClienteResponse->numeroDeCliente != '0') {
			$suscripcionResponse = bbva_subscription_query($document_type, $document_number);
			if ($suscripcionResponse->esta_suscripto()) {
				$password = user_password(8);  // TODO: agregar validaciones de parametria clave no cliente
				_update_user_only_pass($noClienteResponse->numeroDeCliente, $password);

				$texto = "Su nueva clave es : " . $password;
				$smsRequest = new SmsRequest();
				$smsRequest->numeroDeCelular = $suscripcionResponse->codigoDeArea . $suscripcionResponse->numeroDeCelular;
				$smsRequest->codigoDeOperador = $suscripcionResponse->codigoDeOperador;
				$smsRequest->texto = $texto;

				bbva_subscription_send_sms($smsRequest);
				_require_user_change_pass($noClienteResponse->numeroDeCliente);
			}
			$mensaje = '<div id="bbva-profile-pass-recovery-form">En breve le enviaremos su password por sms.</div>';
			return $mensaje;
		} else {
			// Si pasan todas, retornar mensaje ok
			$mensaje = '<div id="bbva-profile-pass-recovery-form">En breve le enviaremos su password por sms.</div>';
			return $mensaje;
		}
	  } catch (Exception $e) {
      $error_message = t(PROFILE_ERR_GENERICO);
      form_set_error('number', $error_message);
      $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
		  return $form;
	  }
  } else {

	   return $form;
  }
}


function bbva_api_login_preference(){
  $type = array(
    array(
      'id' => 'CLIENTE',
      'name' => 'Soy cliente',
    ),
    array(
      'id' => 'NO_CLIENTE',
      'name' => 'No soy cliente',
    ),
  );
  // doc type
  $doc_type = array(
    array(
      'id' => 'DNI',
      'name' => 'DNI'),
    array(
      'id' => 'CI',
      'name' => 'CI'),
    array(
      'id' => 'LE',
      'name' => 'LE'),
    array(
      'id' => 'LC',
      'name' => 'LC'),
    array(
      'id' => 'PAS',
      'name' => 'Pasaporte'),
  );

  // doc type
  $doc_type_altamira_list = array();
  $doc_type_altamira = bbva_login_get_document_altamira_type_list_complete();
  foreach ($doc_type_altamira as $key => $value) {
    $doc_type_altamira_list[] = array(
    'id' => $key,
    'name' => $value,
    );
  }
  // Sex
  $sex = array(
    array(
      'id' => 'M',
      'name' => 'M'),
    array(
      'id' => 'F',
      'name' => 'F'),
  );

  // Day
  $day = array();
  for ($i = 1; $i<=31; $i++) {
    $day[] = array(
      'id' => $i,
      'name' => $i,
    );
  }
  // mes
  $month = array();
  for ($i = 1; $i<=12; $i++) {
    $month[] = array(
      'id' => $i,
      'name' => $i,
    );
  }
  // year
  $year = array();
  for ($i = 2005; $i>=1930; $i--) {
    $year[] = array(
      'id' => $i,
      'name' => $i,
    );
  }
  // Company
  $company = array(
    array(
      'id' => '0001',
      'name' => 'Movistar'),
    array(
      'id' => '0002',
      'name' => 'Claro'),
    array(
      'id' => '0003',
      'name' => 'Personal'),
    array(
      'id' => '0004',
      'name' => 'Nextel'),
  );
  // Rubros
  $rubros = _bbva_subscription_options(9);
  foreach ($rubros as $key => $value) {
    $rubro[] = array(
      'id' => $key,
      'name' => $value,
    );
  }

  $areas = get_code_area_type();
  foreach ($areas as $key => $value) {
    $area[] = array(
      'id' => $key,
      'name' => $value,
    );
  }

  // Zones
  $zones = _bbva_subscription_zones();
  $provinces = _bbva_subscription_provinces();

  $zonas = array_merge($zones, $provinces);

  foreach ($zonas as $key => $value) {
    $zona[] = array(
      'id' => $key,
      'name' => $value,
    );
    if ($key=='PROV-1') {
      array_unshift($zona, array_pop($zona));
    }
  }

  // Nacionalidades
  $nacionalidades_list = bbva_subscription_get_nationality_type_list();
  $nacionalidades = array();
  foreach ($nacionalidades_list as $key => $value) {
    $nacionalidades[] = array(
      'id' => $key,
      'name' => $value,
    );
  }

  // Cargo motivos de baja
  $motive = bbva_profile_get_subscription_motive_list();
  $baja = array();
  foreach ($motive as $key => $value) {
    $baja[] = array(
      'id' => $key,
      'name' => $value,
    );
  }
  global $base_url;
  $urlRegistracion=$base_url.'/?panel=suscribirNocliente';
  $urlRegistracion_cliente=$base_url.'/?panel=suscribirCliente';
  $urlRegistracion_no_cliente=$base_url.'/?panel=suscribirNocliente';
  $bbva_login_recover_cliente_url = $base_url.'/?panel=recoverpassCliente';
  $bbva_login_recover_new_cliente_url = variable_get('fnet.host.web').'/fnetcore/#firstAccess/Zxtw@8gFXNgAAFc14xwAAAQl/firstAccess';
  //$bbva_login_recover_new_cliente_url = $base_url.'/?panel=recoverpassNewCliente';
  $bbva_login_recover_no_cliente_url = $base_url.'/?panel=recoverpassNocliente';
  $bbva_login_delete_subscription_url = $base_url.'/profile/baja';
  $bbva_login_change_pass_url = $base_url.'/';

  return array(
    'guia_web'=>variable_get('bbva_api_guia_uso_guia_web', false)==1?true:false,
    'campanas' => variable_get('bbva_campanas_campanas_activas', false) == 1 ? true : false,
    'suscripcion_app'=>false,
    'refresh_time'=>'5',
    'type' => $type,
    'doc_type' => $doc_type,
    'doc_type_altamira' => $doc_type_altamira_list,
    'sex' => $sex,
    'day' => $day,
    'month' => $month,
    'year' => $year,
    'areas' => $area,
    'company' => $company,
    'rubros' => $rubro,
    'zonas' => $zona,
    'motivos_baja' => $baja,
    'nacionalidades' => $nacionalidades,
    'url_recover_cliente' => $bbva_login_recover_cliente_url,
    'url_new_cliente' => $bbva_login_recover_new_cliente_url,
    'url_recover_no_cliente' => $bbva_login_recover_no_cliente_url,
    'url_baja' => $bbva_login_delete_subscription_url,
    'url_change_password' => $bbva_login_change_pass_url,
    'urlRegistracion'=>$urlRegistracion,
    'url_registracion_cliente'=>$urlRegistracion_cliente,
    'url_registracion_no_cliente'=>$urlRegistracion_no_cliente,


  );



}
