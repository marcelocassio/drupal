<?php


function _is_apply_first_login($uid) {
  $is_apply_first_login = FALSE;
  $login_app_welcomes = db_select_login_welcome($uid);
  if (!empty($login_app_welcomes)) {
    $state = $login_app_welcomes[0]->state;
    if ($state == BBVA_API_LOGIN_FIRSTLOG_NEW_USER) {
      $is_apply_first_login = TRUE;
    }
  }
  return $is_apply_first_login;
}


function _insert_mark_first_login_app($uid) {
  $date = date_create();
  $first_login_date = date_timestamp_get($date);

  $values[] = array(
    'uid' => $uid,
    'state' => BBVA_API_LOGIN_FIRSTLOG_APP,
    'first_login_date' => $first_login_date,
  );

  db_insert_login_welcomes($values);
}

function _insert_mark_new_user($uid) {

  if (BBVA_API_HABILITAR_MARCA_LOGUEO == FALSE) {
    return FALSE;
  }
  $first_login_date = NULL;

  $values[] = array(
    'uid' => $uid,
    'state' => BBVA_API_LOGIN_FIRSTLOG_NEW_USER,
    'first_login_date' => $first_login_date ,
  );

  db_insert_login_welcomes($values);

  return 0;
}

function _update_mark_first_login_app($uid) {
  $date = date_create();
  $first_login_date = date_timestamp_get($date);

  $values = array(
    'uid' => $uid,
    'state' => BBVA_API_LOGIN_FIRSTLOG_APP,
    'first_login_date' => $first_login_date,
  );

  db_update_login_welcomes($values);
}

function _update_mark_regular_login_app($uid) {

  $first_login_date = NULL;

  $values = array(
    'uid' => $uid,
    'state' => BBVA_API_LOGIN_FIRSTLOG_REGULAR,
  );

  $query = db_update('login_app_welcomes')
            ->fields(array('state' => $values['state'] ,))
            ->condition('uid', $values['uid'], '=');
  $query->execute();
}




function db_insert_login_welcomes($values) {
  $query = db_insert('login_app_welcomes');
  $query = $query->fields(array('uid', 'state', 'first_login_date',));
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();
}

function db_update_login_welcomes($values) {
  $query = db_update('login_app_welcomes')
            ->fields(array(
              'state' => $values['state'] ,
              'first_login_date' => $values['first_login_date'],))
            ->condition('uid', $values['uid'], '=');
  $query->execute();
}

function db_select_login_welcome($uid = NULL) {
  $query = db_select('login_app_welcomes', 'lw')
            ->fields('lw', array('uid', 'state', 'first_login_date'))
            ->condition('lw.uid', $uid, '=');

  $result = $query->execute()->fetchAll();
  return $result;
}

function db_users_first_login() {
  $query = db_select('login_app_welcomes', 'lw')
            ->fields('lw', array('uid', 'state', 'first_login_date'))
            ->condition('lw.state', BBVA_API_LOGIN_FIRSTLOG_APP, '=');
  $result = $query->execute()->fetchAll();
  return $result;
}

function db_update_login_welcome_to_regular_state($users_welcome) {
  $id_users = array();

  if (!empty($users_welcome)) {
    foreach ($users_welcome as $login_app_welcome) {
      $id_users[] = $login_app_welcome->uid;
    }

    db_update('login_app_welcomes')
      ->fields(array('state' => BBVA_API_LOGIN_FIRSTLOG_REGULAR ,))
      ->condition('uid', $id_users, 'IN')
      ->execute();
  }
}
