<?php
  define('BATCH_MESSAGES_NOTIF_APP_ID_PUSH', 'FRANCESGO');
  define('BATCH_MESSAGES_NOTIF_ORIGEN_PUSH', 'FGO2');

  /**
   * @file: batch_messages_notification_process.batch.inc
   * Created: rochadm@gmail.com
   * Batch API callbacks.
   */

  /**
   * Callback to batch_messages_notification_process_visa_process using Batch API.
   */
  /**
   * The batch processor.
   */
  function batch_messages_notification_process_visa_process($data,$date, &$context) {
    switch ($data) {
      case 'messages_notification_process_visa':
        
        $fechaProceso = date('Ymd',strtotime($date.'-1 day'));
        $contRecordPush = 0;
        $contmsgAll = 0;
        $conmsgAn = 0;
        $conmsgCli = 1;

        $fp_messages_push_td = fopen("/prod/jp00/notificaciones/enviar/NotifsGO_ALL_".$date.".txt","wb");
        $fp_messages_push_an = fopen("/prod/jp00/notificaciones/enviar/ANotifsGO_ANONY_".$date.".txt","wb");
        $fp_messages_push = fopen("/prod/jp00/notificaciones/enviar/NotifsGO_CLI".$conmsgCli."_".$date.".txt","wb");
        
        $result = db_query(
          "SELECT emp.id FROM eck_bbva_message_plan emp
            JOIN field_data_field_message_plan_state fdmps on fdmps.entity_id=emp.id
            JOIN field_data_field_message_plan_date_send fdmpds on fdmpds.entity_id=emp.id
            WHERE fdmps.field_message_plan_state_value=0;"
        );

        foreach ($result as $row) {
          try {
            $id_message = $row->id;
            $entity_message = entity_load_single('bbva_message_plan', $id_message);

            $wrapper = entity_metadata_wrapper('bbva_message_plan', $entity_message);
            $msgText = $entity_message->field_message_plan_push['und'][0]['value'];
            $texto_adicional = $wrapper->field_message_plan_long_text->value();
            $call_to_action = $wrapper->field_call_to_action->value();

            $imagen = $wrapper->field_message_plan_imagen1->value();
            $nombreImagen = $imagen['filename'];

            if (variable_get('fgo_activar_cta_push') == 1) {

              $ctaRelacionBo = new Fgo\Bo\CtaRelacionBo();
              $relacionBo = $ctaRelacionBo->buscarPorIdEntidad($id_message, TIPO_RELACION_CTA_PUSH);
              $cta = $relacionBo->cta;

              if ($cta){
                $ctaDto = new \Fgo\Services\Dto\CTA();
                $call_to_action = $ctaDto->obtenerNav($cta);
              }
            }

            if($call_to_action != NULL && variable_get('fgo_activar_cta_push') == 0) {
              $call_to_action = $call_to_action->item_id;
              $call_to_actionw = entity_load_single('field_collection_item', $call_to_action);
              $call_to_action_type = $call_to_actionw->field_tipo['und'][0]['value'];

               if($call_to_action_type == '7'){
             
                $link = $call_to_actionw->field_call_to_acction_link['und'][0]['value'];
                $visible = $call_to_actionw->field_cta_link_visible['und'][0]['value'];
             
                if($visible == '1'){
                  $call_to_action = NULL;
                  $msgText = $msgText." Ver: ". $link;
                }
              }
            }

            $fechaEnvio = date('ymd', strtotime($entity_message->field_message_plan_date_send['und'][0]['value'].'-1 day'));

            if($fechaEnvio == $date){

              print_r("fecha envio".$fechaEnvio."\n");
              print_r("fecha proceso".$date."\n");
              
              //$wrapper = entity_metadata_wrapper('coupon_promo', $entity);
              /*** CONDICIONES PARA GENERAR LOS ARCHIVOS MESSAGES PUSH, SMS, EMAIL ***/
              /************************************************/
              //*******************************************************************************************//
              if (!empty($entity_message->field_message_plan_push['und'][0]['value'])) {
                /*** GENERAMOS EL ARCHIVO MESSAGES PUSH ******************************/
                /*********************************************************************/
                if ($entity_message->field_field_message_plan_segment['und'][0]['value'] == 1) {
                  if (!isset($fp_messages_push_td)) {
                    $fp_messages_push_td = fopen("/prod/jp00/notificaciones/enviar/NotifsGO_ALL_".$date.".txt","wb");
                  }
                  
                  $content_messages_push = _get_record_push($fechaProceso, $contmsgAll, '888888888888888888888888888888', '', $msgText, NULL, $texto_adicional, $call_to_action, $nombreImagen);

                  $contmsgAll++;
                  fwrite($fp_messages_push_td,$content_messages_push.PHP_EOL);
                }

                if($entity_message->field_message_plan_segment_anony['und'][0]['value'] == 1){
                  
                  if(!isset($fp_messages_push_an)){
                  
                    $fp_messages_push_an = fopen("/prod/jp00/notificaciones/enviar/NotifsGO_ANONY_".$date.".txt","wb");
                  }
                  
                  $content_messages_push = _get_record_push($fechaProceso, $conmsgAn, '999999999999999999999999999999', '', $msgText, NULL, $texto_adicional, $call_to_action, $nombreImagen);

                  //SALTO DE CARRO
                  $conmsgAn++;
                  fwrite($fp_messages_push_an,$content_messages_push.PHP_EOL);
                }

                if(!empty($entity_message->field_message_plan_list_file['und'][0]['uri'])){
                  if(!isset($fp_messages_push)){
                    $fp_messages_push = fopen("/prod/jp00/notificaciones/enviar/NotifsGO_CLI".$conmsgCli."_".$date.".txt","wb");
                  }

                  $contRecordPush = batch_messages_notification_with_file($fp_messages_push, $entity_message, $fechaProceso, $contRecordPush);
                  
                  $conmsgCli++;
                  unset($fp_messages_push);
                }
              }
              
              /*********************************************************************/
              $wrapper->field_message_plan_state->set("1"); //Estado enviado
              $wrapper->save();
              print_r("GENERO OK".PHP_EOL);
            }
          }
          catch (Exception $e) {
            
            watchdog('error_push','Error en proceso de generacion de mensajes push | Error: %error', array('%error' => $e->getMessage()));
            
            $wrapper->field_message_plan_state->set("2"); //Estado Cancelado con error
            $wrapper->save();
            print_r("GENERO CON ERROR: ".$e->getMessage().PHP_EOL);
          }
        } // FIN FOREACH
        
        if (isset($fp_messages_push)) {
          fclose($fp_messages_push);
        }
        if (isset($fp_messages_push_td)) {
          fclose($fp_messages_push_td);
        }

        if (isset($fp_messages_push_an)) {
          fclose($fp_messages_push_an);
        }
        
      break;
    }
  }

  /**
   * The batch finish handler.
   */
  function batch_messages_notification_process_finished($success, $results, $operations) {
    
    if ($success) {
      drupal_set_message(t('Archivo Notificaciones completo!'));
    }
    else {
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE)
      ));
      drupal_set_message($message, 'error');
    }
  }


  function batch_messages_notification_with_file($fp_messages_push, $entity_message, $fechaProceso, $contRecordPush) {

    $msgText = $entity_message->field_message_plan_push['und'][0]['value'];
    $id_entidad = $entity_message->id;
    $texto_adicional = $entity_message->field_message_plan_long_text['und'][0]['value'];

    if(variable_get('fgo_activar_cta_push') == 1) {
      
      $ctaRelacionBo = new Fgo\Bo\CtaRelacionBo();
      $relacionBo = $ctaRelacionBo->buscarPorIdEntidad($id_entidad, TIPO_RELACION_CTA_PUSH);
      $cta = $relacionBo->cta;

      if($cta){
        $ctaDto = new \Fgo\Services\Dto\CTA();
        $call_to_action = $ctaDto->obtenerNav($cta);
      }
    }
    else{
      $call_to_action = $entity_message->field_call_to_action['und'][0]['value'];
    }

    $imagen = $entity_message->field_message_plan_imagen1['und'][0];
    
    if(isset($imagen['filename'])){
      $nombreImagen = $imagen['filename'];
    }

    if (!isset($fp_messages_push)) {
      $fp_messages_push = fopen("/prod/jp00/notificaciones/enviar/NotifsGO_CLI_".$date.".txt","wb");
    }
    $file_listado_user_process = $entity_message->field_message_plan_list_file['und'][0]['uri'];

    $list_tipo_nro_doc = _get_list_numdoc_form_file($file_listado_user_process);
    $map_tipodocdni_ids = _get_user_number_uids($list_tipo_nro_doc);

    // Generamos archivo push
    $file_listado = fopen(drupal_realpath($file_listado_user_process), "r") or exit("Unable to open file!");

    while(!feof($file_listado)) {
      $mensaje_push = $msgText;
      $user_array = batch_messages_parsear_linea($file_listado);

      // Validar linea.
      $docType = $user_array[0];
      $numdoc = $user_array[1];
      $docAltamira = bbva_get_altamira_code($docType);
      $uid = $map_tipodocdni_ids[$numdoc];
      
      if (!empty($uid)) {
        $content_messages_push=_get_record_push($fechaProceso, $contRecordPush, $docAltamira, $numdoc, $mensaje_push, NULL, $texto_adicional, $call_to_action, $nombreImagen);

        fwrite($fp_messages_push, $content_messages_push.PHP_EOL);
        
        $contRecordPush++;
      } 
      else{
        echo "DNI sin usuario: ".$docType.$numdoc.PHP_EOL;
      }
    } //fin while feof

    return $contRecordPush;
  }

  function batch_messages_build_insert_schema($hash, $uid, $id_entidad) {
    bbva_campanias_db_insert_hash($uid, $hash, $id_entidad);
  }

  function batch_messages_parsear_linea($file_listado) {
    $line_of_text_users = fgets($file_listado);
    $line_of_text_users = str_ireplace("\x0D", "", $line_of_text_users);
    $line_of_text_users = eregi_replace("[\n|\r|\n\r]", "", $line_of_text_users);
    $user_array = explode(';', $line_of_text_users);
    
    return $user_array;
  }

  function batch_messages_generar_hash($docType, $numdoc) {
    $fecha = date_create();
    $fecha_datetime = date_timestamp_get($fecha);
    $hash = bbva_short_hash($docType.$numdoc.$fecha_datetime);
    
    return $hash;
  }
