<?php

	/**
	 * The batch finish handler.
	 */
	function batch_coupon_promotion_finished($success, $results, $operations) {
		if ($success) {
			drupal_set_message(t('File create (promo and voucher) is Complete!'.date('Y-m-d H:i:s')));
		}
		else {
			$error_operation = reset($operations);
			$message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
				'%error_operation' => $error_operation[0],
				'@arguments' => print_r($error_operation[1], TRUE)
			));
			drupal_set_message($message, 'error');
		}
	}


	function batch_coupon_usados($filename,&$context){

		//$file_name = drupal_get_path('module', 'batch_coupon') . '/tmp/francesgo.txt'; // Al actualizar el path del archivo, eliminar la carpeta /tmp de /batch_coupon
		$file_name = $filename;
		$lineas = file($file_name);
		$i==0;
		$arrayVouchersAct=array();
		$context['message'] = 'Start '.date('Y-m-d H:i:s');

		if (isset($lineas)) {
			foreach ($lineas as $row => $record) {
				$i++;
				$coupon = '';
				$coupon = explode(';', $record);
				$id = trim($coupon[0]);
				$arrayVouchersAct[]=$id;
			}
		}

		$context['message'] = sizeof($arrayVouchersAct);
		$context['message'] = 'Get all ids  '.date('Y-m-d H:i:s');
		$result =  _get_vouchers_ids($arrayVouchersAct);
		$context['message'] = sizeof($result);
		$context['message'] = 'Finish all ids  '.date('Y-m-d H:i:s');

	 	foreach ($result as $key => $vou) {
			update_cupon_usado($vou->id, $fecha_uso);
			if ($key%200 == 0) {
				$context['message'] = "Update Cupon $key ...".date('Y-m-d H:i:s');
			}
		}
	}
