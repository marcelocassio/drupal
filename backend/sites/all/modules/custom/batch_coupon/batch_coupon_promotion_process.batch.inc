<?php

/**
 * Proceso batch que toma el archivo TJVOUC01.PROMOCIONES-VISA2.txt enviado por VISA
 * Lo procesamos si tiene 117 caracteres es correcto y debemos actualizar el estado field_coupon_state_shipping a
 * APROBADO
 * Caso contrario lo ponemos en RECHAZADO
**/

  // bcpp -> CuponesRecepcion.sh
  function batch_coupon_promotion_process_visa_process($data, &$context){
    
    $rubros = _get_category();
    $archivo_cupones = file('/prod/jp00/cupones/vuelta/TJVOUC01-PROMOCIONES-VISA.txt');
    $cupones_suip = _get_cupones_suip();

    foreach($archivo_cupones as $num_línea => $línea) {
      if($num_línea == 0){
        continue;
      }
      $cupon_promo = substr($línea, 13, 4);

      if (strlen(trim($línea)) == CARACTERES_CORRECTOS_VISA) {
        $cupones_enviados_visa = Cupon::getCuponesEnviadosVisa($cupon_promo);
        
        foreach ($cupones_enviados_visa as $cupon_drupal){ 

          $id_coupon_drupal = $cupon_drupal->id; 
          $id_suip = $cupon_drupal->id_promo_suip;
          $idPromoShop = $cupon_drupal->id_promo_shop;

          $entity = entity_load_single('coupon_promo', $id_coupon_drupal);
          $wrapper = entity_metadata_wrapper('coupon_promo', $entity);
          
          $promo_numero = $wrapper->field_coupon_promo_numero->field_coupon_number_promo->value();

          // Busco el beneficio
          foreach ($cupones_suip as $cupon_suip) {

            $idPromocionSuip = $cupon_suip['idPromocion'];
            $idComunicacionSuip = $cupon_suip['idComunicacion'];
            $codigoConfirmadoSuip = $cupon_suip['codigoPromoCuponConfirmado'];


            if($idPromocionSuip != $id_suip) {
              continue;
            }

            hd($cupon_suip);
            echoline("BCPP: PROMO DRUPAL id: " .$id_coupon_drupal." - PROMO SUIP ENCONTRADA ".$id_suip, true, true);

            if(is_numeric($idComunicacionSuip) and $idComunicacionSuip > 0) {

              // v2
              try {
                bbva_coupon_relacionar_promo_cupon($id_coupon_drupal, $idComunicacionSuip);
              } 
              catch (Exception $e) {
                echo "ERROR en " . __METHOD__ . ": " . $e->getMessage();
              }

              echoline("bbva_buscar_benefit_con_idSUIP - core viejo");
              $benefit = bbva_buscar_benefit_con_idSUIP($idComunicacionSuip);


              // @DEPRECATE VIEJO CORE
              if($benefit){

                $benefitId = $benefit->entity_id;
                $entity_benefit = entity_load_single('benefit', $benefitId);
              
                if(!$entity_benefit) {
                  echoline("ENTITY BENEFICIO SUIP NO ENCONTRADO ".$idComunicacionSuip." se resulve en import suip", true, true);
                  $wrapper->field_coupon_promo_id_comunica->set($idComunicacionSuip );
                }
                else{
                  
                  echoline("ENTITY BENEFICIO SUIP ENCONTRADO ".$benefitId, true, true);
                  
                  $wrapper_benefit = entity_metadata_wrapper('benefit', $entity_benefit);

                  // Seteo legal y shop
                  $wrapper->field_coupon_promo_legal->set($wrapper_benefit->field_benefit_legal->value());
                  $shop_branch = $wrapper_benefit->field_benefit_shop_branchs->value();
                  
                  $wrapper->field_coupon_promo_shop->set($shop_branch[0]->field_bsb_shop['und'][0]['target_id']);
                  $wrapper->field_coupon_promo_benefit_asoc->set(1);
                  $wrapper->field_coupon_promo_benefit->set($entity_benefit->id);
                }
              }
              echoline("BENEFICIO SUIP ENCONTRADO DESDE EL NUEVO CORE ".$idComunicacionSuip, true, true);
            }

            // Confirmo alta de promo numero
            echoline(" Validamos si tiene que confirmar el codigo promo status: ".$codigoConfirmadoSuip, true, true);
            if ($codigoConfirmadoSuip == "false") {

              $cs = \CuponesServiceSuip::getInstance();
              echoline(" CONFIRMA ALTA EN SUIP PROMO_NUMERO: ".$promo_numero." idSuip ".$id_suip, true, true);
              $confirmar_alta = $cs->confirmarAltaBancaDigital($promo_numero, $id_suip);
              
              
              // Si fallo el alta lo logueo y escapo de la iteración
              if(!$confirmar_alta && module_exists('bbva_logs')) {
                $data_log = array(
                  "id_suip" => $id_suip,
                  "promo_numero" => $promo_numero
                );
                $log = \BBVA\Log::getInstance();
                $log->insertar("suip_error_alta_promo", $data_log);
              }
            }

            echoline('CUPON_ESTADO_APROBADO', true, true);

            $wrapper->field_coupon_state_shipping->set(CUPON_ESTADO_APROBADO); 
            $wrapper->save();

            if ($wrapper->field_coupon_promo_benefit_asoc->value() == '0') {
                
              // crea el shop branch
              $values = array(
                'type' => 'benefit_shop_branch', 
                'status' => 1, 
                'comment' => 1, 
                'promote' => 0
              );
              
              $entityBsb = entity_create('benefit_shop_branch', $values);
              $ewrapperFile = entity_metadata_wrapper('benefit_shop_branch', $entityBsb);

              $ewrapperFile->title->set($id_coupon_drupal.$cupon_promo);
              $result_branchs = Cupon::getBranchShop($idPromoShop);

              foreach ($result_branchs as $row_branch) {
                $ewrapperFile->field_bsb_branchs[] = $row_branch->entity_id;
              }

              $ewrapperFile->field_bsb_shop->set($idPromoShop);
              $ewrapperFile->save();

              // crea Entity benefit
              $entity_typeb = 'benefit';
              $entityBenefit = entity_create($entity_typeb, array('type' => $entity_typeb));
              $wrapper_benefit = entity_metadata_wrapper($entity_typeb, $entityBenefit);

              if($wrapper->field_coupon_destacado->value() == 1) {
                $wrapper_benefit->field_benefit_membership_chan[] = BENEFIT_MEMBERSHIP_CHAN;
              }

              // crea Entity Shop
              $entity_type = 'shop';
              $shop = entity_load_single($entity_type, $idPromoShop);
              $shopWarpper = entity_metadata_wrapper($entity_type, $shop);

              $wrapper_benefit->title->set($shop->title);
              $wrapper_benefit->field_benefit_description_portal->set($wrapper->field_coupon_promo_msg_web_app->value());
              $wrapper_benefit->field_benefit_date_start->set($wrapper->field_coupon_promo_date_start->value());
              $wrapper_benefit->field_benefit_date_end->set($wrapper->field_coupon_promo_date_end->value());
              $wrapper_benefit->field_benefit_legal->set($wrapper->field_coupon_promo_legal->value());
              $wrapper_benefit->field_benefit_description->set($wrapper->field_coupon_promo_subtitle->value());
              $wrapper_benefit->field_benefit_shop_branchs[] = $ewrapperFile->getIdentifier();

              $categorycup = $shopWarpper->field_shop_category->value();

              foreach ((array) $categorycup as $key => $cup) {
                $termCup = taxonomy_term_load($cup->tid);

                if (array_key_exists($termCup->field_category_parent['und'][0]['value'], $rubros)) {
                  $wrapper_benefit->field_benefit_category[] = $rubros[$termCup->field_category_parent['und'][0]['value']]->tid;
                }
              }

              // trae texto: Seas o No Cliente BBVA
              $term_by_suip = _get_term_by_suip_id(TERM_SUIP_ID);
              $wrapper_benefit->field_benefit_category[] = $term_by_suip;

              if ($wrapper->field_coupon_type->value() === CUPON_PROMO_TYPE_EVENTO_SIN_ARCHIVO) {
                $wrapper_benefit->field_published->set('0');
              }

              $wrapper_benefit->save();

              // v2
              $id_beneficio_suip_ficticio = _generar_id_suip_ficticio($wrapper_benefit->getIdentifier());

              try {
                bbva_coupon_crear_comunicacion($wrapper, $id_beneficio_suip_ficticio);
              } catch (Exception $e) {
                echoline("Error en cupon: " . $e->getMessage(), true, true);
              }

              $wrapper->field_coupon_promo_benefit->set($wrapper_benefit->getIdentifier());
              $wrapper->save();
            } 
            else {
              $term_by_suip = _get_term_by_suip_id(TERM_SUIP_ID);
              $wrapper_benefit = $wrapper->field_coupon_promo_benefit;
              $wrapper_benefit->field_benefit_category[] = $term_by_suip;
              $wrapper_benefit->save();
            }
          }
        } // end foreach cupones_enviados_visa
      }
      else{
        
        $cuponesPromoNumero = Cupon::getCuponesPromoNumero($cupon_promo);
        
        foreach ($cuponesPromoNumero as $cuponPromoNumero) {
          $entity = entity_load_single('coupon_promo', $cuponPromoNumero->id);
          $wrapper = entity_metadata_wrapper('coupon_promo', $entity);
          
          echoline('CUPON_ESTADO_RECHAZADO', true, true);
          $wrapper->field_coupon_state_shipping->set(CUPON_ESTADO_RECHAZADO);
          $wrapper->save();
        }
      }
    }
  }
  /**
   * The batch finish handler.
   */
  function batch_coupon_promotion_process_finished($success, $results, $operations){
    if ($success) {
      drupal_set_message(t('File Coupon Promo is Process Complete!'));
    } 
    else{
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], true),
      ));
      drupal_set_message($message, 'error');
    }
  }
