<?php
/**
 * @file: batch_coupon.drush.inc
 * Created: rochadm@gmail.com
 * Drush integration for node_revision_delete
 */

/**
 * Implements COMMANDFILE_drush_command().
 */
function batch_coupon_drush_command() {
  $items = array();


  $items['batch-coupon-promotion-process'] = array(
    'description' => 'Drush process info coupon promotion process for VISA.',
    'aliases' => array('bcpp'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['batch-messages-notification'] = array(
    'description' => 'Drush process messages notification process.',
    'aliases' => array('bcn'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
    ),
  );

  $items['batch-delete-voucher'] = array(
    'description' => 'Drush process messages notification process.',
    'aliases' => array('cvdelete'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['batch-update-cupones-usados'] = array(
    'description' => 'Drush process update cupones usados.',
    'aliases' => array('bucu'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'filename' => 'Nombre de archivo',
    ),
  );

  return $items;
}


/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_batch_coupon_promotion_process() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Coupon Promotion Process Visa'),
    'init_message' => t('Batch Coupon Promotion Process Visa is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'batch_coupon_promotion_process_finished',
    'file' => drupal_get_path('module', 'batch_coupon') . '/batch_coupon_promotion_process.batch.inc',
  );

  /** 
   * Necesitamos 1 operaciones:
   * 1. Procesar Voucher devueltos por Visa 
  **/
  $results = array("coupon_promotion_process_visa");
  foreach ($results as $result) {
    $batch['operations'][] = array('batch_coupon_promotion_process_visa_process', array($result));
  }
  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_batch_coupon_batch_messages_notification($odate= NULL ) {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Messages Notification Process Visa'),
    'init_message' => t('Batch Messages Notification Process Visa is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'batch_messages_notification_process_finished',
    'file' => drupal_get_path('module', 'batch_coupon') . '/batch_messages_notification_process.batch.inc',
  );

  /** Necesitamos 1 operaciones:
   ** 1. Procesar messages notificaciones por Visa 
  ***/
  $results = array("messages_notification_process_visa");


  foreach ($results as $result) {
    $batch['operations'][] = array('batch_messages_notification_process_visa_process', array($result,$odate));
  }
  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}


function drush_batch_coupon_batch_delete_voucher() {
  // // Set up the batch job.
  // $batch = array(
  //   'operations' => array(),
  //   'title' => t('Batch Messages Notification Process Visa'),
  //   'init_message' => t('Batch Messages Notification Process Visa is starting...'),
  //   'error_message' => t('An error occurred'),
  //   'finished' => 'batch_messages_notification_process_finished',
  //   'file' => drupal_get_path('module', 'batch_coupon') . '/batch_messages_notification_process.batch.inc',
  // );

  // /** Necesitamos 1 operaciones:
  //  ** 1. Procesar messages notificaciones por Visa 
  // ***/
  // $results = array("messages_notification_process_visa");


  // foreach ($results as $result) {
  //   $batch['operations'][] = array('batch_messages_notification_process_visa_process', array($result,$odate));
  // }
  // // Start the batch job.
  // batch_set($batch);
  // drush_backend_batch_process();
   _delete_voucher_not_used();
}

function drush_batch_coupon_batch_update_cupones_usados($filename){
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch update cupones usados'),
    'init_message' => t('Batch update cupones usados is starting...'),
    'error_message' => t('An error occurred'),
    'file' => drupal_get_path('module', 'batch_coupon') . '/batch_coupon.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('batch_coupon_usados', array($filename));
  
  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}
