<?php

class ComunicionServiceSuipMock {

  public static function consultarNovedadesConSucursalVirtual() {
    $jsonObject = json_decode('{ "BeneficioDrupal": {
    "result": [
    {
      "id": 10440,
      "uri": "null-10440",
      "cuotasSinInteres": 3,
      "comercios": [
        {
          "id": 632,
          "uri": "null-632",
          "nombreComercio": "A lo de guti co",
          "rubros": [
            {
              "id": 98,
              "nombre": "Concesionarias"
            }
          ]
        }
      ],
      "beneficio": "descripcion legal",
      "tipoDescripcionBeneficio": "cupon",
      "tipoBeneficio": {
        "id": 100,
        "nombre": "Promo Cupon"
      },
      "descuentoGeneralTarjetas": {
        "tarjetas": [
          {
            "id": 39,
            "nombre": "Visa Classic Nacional",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0102",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 40,
            "nombre": "Visa Classic Internacional NA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0104",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 41,
            "nombre": "Visa Gold NA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0201",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 43,
            "nombre": "Visa Signature",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "1002",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 44,
            "nombre": "Visa Recargable.",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "0001",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 48,
            "nombre": "Visa Segmentada",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 49,
            "nombre": "SEGMENTADA VISA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 52,
            "nombre": "GAFF TC VISA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          }
        ]
      },
      "lunes": true,
      "martes": true,
      "miercoles": true,
      "jueves": true,
      "viernes": true,
      "sabado": true,
      "domingo": true,
      "sucursales": [
        {
          "id": 38192,
          "nombre": "gutiweb",
          "websiteVenta": "www.gutiweb.com.ar",
          "virtual": true,
          "noAtencionPublico": false,
          "comercio": {
            "id": 632,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "barrio": {},
            "localidad": {
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {}
            },
            "provincia": {}
          },
          "shopping": {},
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "31/01/2018",
          "formattedFechaVigenciaHasta": "28/02/2018"
        },
        {
          "id": 38193,
          "nombre": "Vesuvio S.A.C.I.F.I\u000a2",
          "telefono1": "4458-1687",
          "ventaTelefonica": "01152522282 \u00092",
          "virtual": true,
          "noAtencionPublico": false,
          "comercio": {
            "id": 632,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "barrio": {},
            "localidad": {
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {}
            },
            "provincia": {}
          },
          "shopping": {},
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "31/01/2018",
          "formattedFechaVigenciaHasta": "28/02/2018"
        }
      ],
      "tope": 0,
      "legales": "LEGALES CUPONES PRUEBA",
      "idPrioridad": 1,
      "canalesPertenenciaDestacados": [
        {
          "id": 1,
          "nombre": "Frances GO Portal",
          "selected": false
        }
      ],
      "accionComercial": {
        "id": 3,
        "valor": "cupon a"
      },
      "campaniaRelacionada": [
      ],
      "denominacionPromocion": "descarga archivos",
      "localesAdheridos": [
      ],
      "fechaDesdeComunicacion": "2018-01-31",
      "fechaHastaComunicacion": "2020-02-28",
      "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
      "esShopping": false
    }
  ],
  "detalleError": "El beneficio: 10440 no tiene campanias asociadas\u000aEl beneficio: 10440 no tiene locales adheridos\u000aEl beneficio: 10443 no tiene campanias asociadas\u000aEl beneficio: 10443 no tiene locales adheridos\u000aEl beneficio: 10444 no tiene campanias asociadas\u000aEl beneficio: 10444 no tiene locales adheridos\u000aEl beneficio: 10445 no tiene campanias asociadas\u000aEl beneficio: 10445 no tiene locales adheridos\u000aEl beneficio: 10446 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene locales adheridos\u000aEl beneficio: 10447 no tiene campanias asociadas\u000aEl beneficio: 10447 no tiene locales adheridos\u000aEl beneficio: 10449 no tiene campanias asociadas\u000aEl beneficio: 10449 no tiene locales adheridos\u000aEl beneficio: 10450 no tiene campanias asociadas\u000aEl beneficio: 10450 no tiene locales adheridos\u000aEl beneficio: 10451 no tiene campanias asociadas\u000aEl beneficio: 10451 no tiene locales adheridos\u000aEl beneficio: 10452 no tiene campanias asociadas\u000aEl beneficio: 10452 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene campanias asociadas\u000aEl beneficio: 10453 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene canales de pertenencia\u000aEl beneficio: 10454 no tiene campanias asociadas\u000aEl beneficio: 10454 no tiene locales adheridos\u000aEl beneficio: 10454 no tiene canales de pertenencia\u000aEl beneficio: 10455 no tiene campanias asociadas\u000aEl beneficio: 10455 no tiene locales adheridos\u000aEl beneficio: 10456 no tiene campanias asociadas\u000aEl beneficio: 10456 no tiene locales adheridos\u000aEl beneficio: 10472 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene campanias asociadas\u000aEl beneficio: 10474 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene canales de pertenencia\u000aEl beneficio: 10475 no tiene campanias asociadas\u000aEl beneficio: 10475 no tiene locales adheridos\u000aEl beneficio: 10476 no tiene campanias asociadas\u000aEl beneficio: 10476 no tiene locales adheridos\u000aEl beneficio: 10477 no tiene campanias asociadas\u000aEl beneficio: 10477 no tiene locales adheridos\u000aEl beneficio: 10478 no tiene sucursales asociadas\u000aEl beneficio: 10478 no tiene comercios asociados\u000aEl beneficio: 10478 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene locales adheridos\u000aEl beneficio: 10482 no tiene campanias asociadas\u000aEl beneficio: 10482 no tiene locales adheridos\u000aEl beneficio: 10483 no tiene campanias asociadas\u000aEl beneficio: 10483 no tiene locales adheridos\u000aEl beneficio: 10484 no tiene campanias asociadas\u000aEl beneficio: 10484 no tiene locales adheridos\u000aEl beneficio: 10485 no tiene campanias asociadas\u000aEl beneficio: 10485 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene campanias asociadas\u000aEl beneficio: 10486 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene canales de pertenencia\u000aEl beneficio: 10487 no tiene campanias asociadas\u000aEl beneficio: 10487 no tiene locales adheridos\u000a"
}}');

    return $jsonObject->BeneficioDrupal->result;
  }

  public static function consultarNovedadesConCarteraGeneral() {
    $jsonObject = json_decode('{ "BeneficioDrupal": {
  "result": [
    {
      "id": 10443,
      "uri": "null-10443",
      "descuentoSuperior": 30,
      "cuotasSinInteres": 3,
      "comercios": [
        {
          "id": 6818,
          "uri": "null-6818",
          "nombreComercio": "Alto Avellaneda",
          "rubros": [
            {
              "id": 38,
              "nombre": "Heladerías"
            }
          ]
        }
      ],
      "beneficio": " Alto Avellaneda 3",
      "tipoDescripcionBeneficio": "cupon",
      "tipoBeneficio": {
        "id": 11,
        "nombre": "Catalogo Zonal Bs As Interior"
      },
      "descuentoGeneralTarjetas": {
        "descuentoGeneral": 35,
        "tarjetas": [
          {
            "id": 1,
            "nombre": "MasterCard Credito",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "11",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 7,
            "nombre": "Visa Débito",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 2,
              "descripcion": "Tarjetas de Debito",
              "codigo": "TD"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 18,
            "nombre": "Mastercard Black",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "43",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 23,
            "nombre": "MasterCard Gold",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "13",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 37,
            "nombre": "Visa Agrob",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 2,
              "descripcion": "Tarjetas de Debito",
              "codigo": "TD"
            },
            "codigo": "5563",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 39,
            "nombre": "Visa Classic Nacional",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0102",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 40,
            "nombre": "Visa Classic Internacional NA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0104",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 41,
            "nombre": "Visa Gold NA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0201",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 43,
            "nombre": "Visa Signature",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "1002",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 44,
            "nombre": "Visa Recargable.",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "0001",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 48,
            "nombre": "Visa Segmentada",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          }
        ]
      },
      "lunes": true,
      "martes": true,
      "miercoles": true,
      "jueves": true,
      "viernes": true,
      "sabado": true,
      "domingo": true,
      "sucursales": [
        {
          "id": 38705,
          "nombre": "Pepona´s",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38694,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38706,
          "nombre": "PEPITO2",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38695,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38707,
          "nombre": "Comercio prueba linda",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38696,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38708,
          "nombre": "Comercio Gaby",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38697,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38709,
          "nombre": "720",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38698,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38710,
          "nombre": "Xoana",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38699,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38712,
          "nombre": "Rulos",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38701,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38714,
          "nombre": "ZUCCA",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38703,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38715,
          "nombre": "BANGUI KIDS PELUQEURIA",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38704,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38716,
          "nombre": "BEDTIME UNICENTER",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38705,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38717,
          "nombre": "BENSIMON",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38706,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38718,
          "nombre": "BRAS AND PANTIES",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38707,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38719,
          "nombre": "BROOKSFIELD",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38708,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38720,
          "nombre": "BUDDIES",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38709,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38721,
          "nombre": "CASA VER",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38710,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38722,
          "nombre": "CIEL DE FRANCE",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38711,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "31/03/2018"
        },
        {
          "id": 38723,
          "nombre": "GRIMOLDI SA",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38712,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38724,
          "nombre": "VIA UNO GALERIAS PACIFICO",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38713,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38725,
          "nombre": "WRANGLER GALERIAS PACIFIC",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38714,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38726,
          "nombre": "XL FLORIDA",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38715,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38727,
          "nombre": "LEVIS",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38716,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38728,
          "nombre": "ROSSI Y CARUSO SACIF",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38717,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38729,
          "nombre": "BOATING",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38718,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38731,
          "nombre": "JANET WISE",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38720,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38732,
          "nombre": "47 STREET",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38721,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38733,
          "nombre": "MAXIME",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38722,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38734,
          "nombre": "BAMDI",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38723,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        },
        {
          "id": 38736,
          "nombre": "DANIEL HECHTER",
          "virtual": false,
          "noAtencionPublico": false,
          "comercio": {
            "id": 6818,
            "checkUltimaImportacion": false,
            "contribuyente": {
              "cliente": false,
              "adquirente": false,
              "proactivo": false
            },
            "selected": false,
            "noConvocable": false,
            "sinComunicacion": false
          },
          "localizacion": {
            "id": 38725,
            "barrio": {},
            "localidad": {
              "id": 430,
              "nombre": "Avellaneda",
              "latitud": 0.0,
              "longitud": 0.0,
              "zonaGeografica": {
                "id": 4,
                "nombre": "Zona Sur GBA",
                "idRegionOrdenable": 5
              }
            },
            "provincia": {
              "id": 24,
              "nombre": "Buenos Aires"
            },
            "coordenadaX": -34.6924055,
            "coordenadaY": -58.38732060000001,
            "calle": "Guemes",
            "numero": 897
          },
          "shopping": {
            "id": 4,
            "nombre": "Alto Avellaneda"
          },
          "franquicia": false,
          "checkUltimaImportacion": false,
          "formattedFechaVigenciaDesde": "28/02/2018",
          "formattedFechaVigenciaHasta": "30/05/2019"
        }
      ],
      "legales": "2 PROMOCIÓN VÁLIDA PARA COMPRAS CON TARJETAS EMITIDAS POR BBVA FRANCÉS. PARA COMPRAS CON TARJETA DE CRÉDITO VISA O MASTERCARD, LAS COMPRAS EN CUOTAS PUEDEN REALIZARSE ÚNICAMENTE CON TARJETA DE CRÉDITO. LA PRESENTE PROMOCIÓN ES VÁLIDA SÓLO PARA CONSUMOS DE TIPO FAMILIAR, Y NO SE ACUMULA A OTRAS PROMOCIONES. EN EL CASO DE DUDA SOBRE EL DESTINO DEL CONSUMO,LOS ACCIONISTAS DE BBVA BANCO FRANCÉS S.A. LIMITAN SU RESPONSABILIDAD A LA INTEGRACIÓN DE LAS ACCIONES SUSCRIPTAS LEY 19.550 Y LEY 25.738",
      "idPrioridad": 2,
      "canalesPertenenciaDestacados": [
        {
          "id": 20,
          "nombre": "canal 09",
          "selected": false,
          "email": "canal09@email.com"
        },
        {
          "id": 21,
          "nombre": "canale 10",
          "selected": false,
          "email": "canal10@email.com"
        },
        {
          "id": 22,
          "nombre": "canale 11",
          "selected": false,
          "email": "canal11@email.com"
        },
        {
          "id": 4,
          "nombre": "Excl. BOCA",
          "selected": false
        },
        {
          "id": 6,
          "nombre": "Excl. Plan Sueldo",
          "selected": false
        },
        {
          "id": 3,
          "nombre": "Excl. PREMIUM WORLD",
          "selected": false
        },
        {
          "id": 7,
          "nombre": "Excl. Reactivación",
          "selected": false
        },
        {
          "id": 5,
          "nombre": "Excl. RIVER",
          "selected": false
        },
        {
          "id": 19,
          "nombre": "FGO - Carrusel 2",
          "selected": false,
          "email": "ciriglianog@bbva.com.ar"
        },
        {
          "id": 1,
          "nombre": "Frances GO Portal",
          "selected": true,
          "email": "pablo.radomski@bbva.com",
          "codigo": "Frances GO Portal"
        },
        {
          "id": 23,
          "nombre": "Frances GO PUSH",
          "selected": false,
          "email": " ",
          "codigo": "Frances GO PUSH"
        },
        {
          "id": 2,
          "nombre": "Frances GO SMS",
          "selected": true,
          "email": "pablo-xoy.group@bbva.com",
          "codigo": "Frances GO SMS"
        }
      ],
      "accionComercial": {
        "id": 4,
        "valor": "San Valentín"
      },
      "campaniaRelacionada": [
      ],
      "denominacionPromocion": "tarjeta Blanca",
      "localesAdheridos": [
      ],
      "fechaDesdeComunicacion": "2018-02-28",
      "fechaHastaComunicacion": "2019-05-30",
      "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
      "esShopping": true
    }
  ],
  "detalleError": "El beneficio: 10440 no tiene campanias asociadas\u000aEl beneficio: 10440 no tiene locales adheridos\u000aEl beneficio: 10443 no tiene campanias asociadas\u000aEl beneficio: 10443 no tiene locales adheridos\u000aEl beneficio: 10444 no tiene campanias asociadas\u000aEl beneficio: 10444 no tiene locales adheridos\u000aEl beneficio: 10445 no tiene campanias asociadas\u000aEl beneficio: 10445 no tiene locales adheridos\u000aEl beneficio: 10446 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene locales adheridos\u000aEl beneficio: 10447 no tiene campanias asociadas\u000aEl beneficio: 10447 no tiene locales adheridos\u000aEl beneficio: 10449 no tiene campanias asociadas\u000aEl beneficio: 10449 no tiene locales adheridos\u000aEl beneficio: 10450 no tiene campanias asociadas\u000aEl beneficio: 10450 no tiene locales adheridos\u000aEl beneficio: 10451 no tiene campanias asociadas\u000aEl beneficio: 10451 no tiene locales adheridos\u000aEl beneficio: 10452 no tiene campanias asociadas\u000aEl beneficio: 10452 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene campanias asociadas\u000aEl beneficio: 10453 no tiene locales adheridos\u000aEl beneficio: 10454 no tiene campanias asociadas\u000aEl beneficio: 10454 no tiene locales adheridos\u000aEl beneficio: 10455 no tiene campanias asociadas\u000aEl beneficio: 10455 no tiene locales adheridos\u000aEl beneficio: 10456 no tiene campanias asociadas\u000aEl beneficio: 10456 no tiene locales adheridos\u000aEl beneficio: 10472 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene campanias asociadas\u000aEl beneficio: 10474 no tiene locales adheridos\u000aEl beneficio: 10475 no tiene locales adheridos\u000aEl beneficio: 10476 no tiene campanias asociadas\u000aEl beneficio: 10476 no tiene locales adheridos\u000aEl beneficio: 10477 no tiene campanias asociadas\u000aEl beneficio: 10477 no tiene locales adheridos\u000aEl beneficio: 10478 no tiene sucursales asociadas\u000aEl beneficio: 10478 no tiene comercios asociados\u000aEl beneficio: 10478 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene locales adheridos\u000aEl beneficio: 10482 no tiene campanias asociadas\u000aEl beneficio: 10482 no tiene locales adheridos\u000aEl beneficio: 10483 no tiene campanias asociadas\u000aEl beneficio: 10483 no tiene locales adheridos\u000aEl beneficio: 10484 no tiene campanias asociadas\u000aEl beneficio: 10484 no tiene locales adheridos\u000aEl beneficio: 10485 no tiene campanias asociadas\u000aEl beneficio: 10485 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene campanias asociadas\u000aEl beneficio: 10486 no tiene locales adheridos\u000aEl beneficio: 10487 no tiene campanias asociadas\u000aEl beneficio: 10487 no tiene locales adheridos\u000a"
}}');

    return $jsonObject->BeneficioDrupal->result;

  }

  public static function consultarNovedadesConCupon() {

      $jsonObject = json_decode('{ "BeneficioDrupal": {
    "result": [
      {
        "id": 10450,
        "uri": "null-10450",
        "cuotasSinInteres": 0,
        "comercios": [
          {
            "id": 6941,
            "uri": "null-6941",
            "nombreComercio": "resto",
            "rubros": [
              {
                "id": 39,
                "nombre": "Restaurantes"
              }
            ]
          }
        ],
        "beneficio": " restopromo resto 1",
        "tipoDescripcionBeneficio": "En el total de la compra",
        "tipoBeneficio": {
          "id": 100,
          "nombre": "Promo Cupon"
        },
        "descuentoGeneralTarjetas": {
          "tarjetas": [
            {
              "id": 39,
              "nombre": "Visa Classic Nacional",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "CR0102",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 40,
              "nombre": "Visa Classic Internacional NA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "CR0104",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 41,
              "nombre": "Visa Gold NA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "CR0201",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 43,
              "nombre": "Visa Signature",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "1002",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 44,
              "nombre": "Visa Recargable.",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "0001",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 48,
              "nombre": "Visa Segmentada",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 49,
              "nombre": "SEGMENTADA VISA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 52,
              "nombre": "GAFF TC VISA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            }
          ]
        },
        "lunes": true,
        "martes": true,
        "miercoles": false,
        "jueves": false,
        "viernes": false,
        "sabado": false,
        "domingo": false,
        "sucursales": [
          {
            "id": 38838,
            "nombre": "centro",
            "virtual": false,
            "noAtencionPublico": false,
            "comercio": {
              "id": 6941,
              "checkUltimaImportacion": false,
              "contribuyente": {
                "cliente": false,
                "adquirente": false,
                "proactivo": false
              },
              "selected": false,
              "noConvocable": false,
              "sinComunicacion": false
            },
            "localizacion": {
              "id": 38827,
              "barrio": {
                "id": 36,
                "nombre": "Retiro"
              },
              "localidad": {
                "id": 1,
                "nombre": "Capital Federal",
                "latitud": 0.0,
                "longitud": 0.0,
                "zonaGeografica": {}
              },
              "provincia": {
                "id": 1,
                "nombre": "Capital Federal"
              },
              "coordenadaX": -34.5977897644043,
              "coordenadaY": -58.3699951171875,
              "calle": "Avenida Córdoba",
              "numero": 111,
              "otrosDatos": " "
            },
            "shopping": {},
            "franquicia": false,
            "checkUltimaImportacion": false,
            "formattedFechaVigenciaDesde": "01/05/2018",
            "formattedFechaVigenciaHasta": "31/05/2018"
          }
        ],
        "tope": 0,
        "legales": "LEGALES CUPONES PRUEBA",
        "idPrioridad": 1,
        "canalesPertenenciaDestacados": [
          {
            "id": 1,
            "nombre": "Frances GO Portal",
            "selected": false
          }
        ],
        "accionComercial": {
          "id": 16,
          "valor": "Especial Restaurants"
        },
        "campaniaRelacionada": [
        ],
        "denominacionPromocion": "Promo cupon 1",
        "localesAdheridos": [
        ],
        "fechaDesdeComunicacion": "2018-05-01",
        "fechaHastaComunicacion": "2019-05-31",
        "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
        "esShopping": false
      }
    ],
    "detalleError": "El beneficio: 10472 no tiene locales adheridos\u000aEl beneficio: 10440 no tiene campanias asociadas\u000aEl beneficio: 10440 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene campanias asociadas\u000aEl beneficio: 10474 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene canales de pertenencia\u000aEl beneficio: 10475 no tiene campanias asociadas\u000aEl beneficio: 10475 no tiene locales adheridos\u000aEl beneficio: 10443 no tiene campanias asociadas\u000aEl beneficio: 10443 no tiene locales adheridos\u000aEl beneficio: 10476 no tiene campanias asociadas\u000aEl beneficio: 10476 no tiene locales adheridos\u000aEl beneficio: 10444 no tiene campanias asociadas\u000aEl beneficio: 10444 no tiene locales adheridos\u000aEl beneficio: 10477 no tiene campanias asociadas\u000aEl beneficio: 10477 no tiene locales adheridos\u000aEl beneficio: 10445 no tiene campanias asociadas\u000aEl beneficio: 10445 no tiene locales adheridos\u000aEl beneficio: 10478 no tiene sucursales asociadas\u000aEl beneficio: 10478 no tiene comercios asociados\u000aEl beneficio: 10478 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene locales adheridos\u000aEl beneficio: 10479 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene locales adheridos\u000aEl beneficio: 10447 no tiene campanias asociadas\u000aEl beneficio: 10447 no tiene locales adheridos\u000aEl beneficio: 10449 no tiene campanias asociadas\u000aEl beneficio: 10449 no tiene locales adheridos\u000aEl beneficio: 10482 no tiene campanias asociadas\u000aEl beneficio: 10482 no tiene locales adheridos\u000aEl beneficio: 10450 no tiene campanias asociadas\u000aEl beneficio: 10450 no tiene locales adheridos\u000aEl beneficio: 10483 no tiene campanias asociadas\u000aEl beneficio: 10483 no tiene locales adheridos\u000aEl beneficio: 10451 no tiene campanias asociadas\u000aEl beneficio: 10451 no tiene locales adheridos\u000aEl beneficio: 10484 no tiene campanias asociadas\u000aEl beneficio: 10484 no tiene locales adheridos\u000aEl beneficio: 10452 no tiene campanias asociadas\u000aEl beneficio: 10452 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene campanias asociadas\u000aEl beneficio: 10453 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene canales de pertenencia\u000aEl beneficio: 10454 no tiene campanias asociadas\u000aEl beneficio: 10454 no tiene locales adheridos\u000aEl beneficio: 10454 no tiene canales de pertenencia\u000aEl beneficio: 10455 no tiene campanias asociadas\u000aEl beneficio: 10455 no tiene locales adheridos\u000aEl beneficio: 10456 no tiene campanias asociadas\u000aEl beneficio: 10456 no tiene locales adheridos\u000a"
    }}');

    return $jsonObject->BeneficioDrupal->result;


  }

  public static function consultarNovedadesConEspectaculo() {
    $jsonObject = json_decode('{ "BeneficioDrupal": {
      "result": [
        {
          "id": 10476,
          "uri": "null-10476",
          "cuotasSinInteres": 3,
          "comercios": [
            {
              "id": 6993,
              "uri": "null-6993",
              "nombreComercio": "Tuentrada.com",
              "rubros": [
                {
                  "id": 145,
                  "nombre": "Entradas"
                }
              ]
            }
          ],
          "beneficio": "Disfrutá a RICARDO ARJONA en el Campo Argentino de Polo el 5 de octubre. El cantante guatemalteco cierra su gira mundial \"Circo\" con el show más grande que haya hecho en Argentina y ¡no te lo podés perder!  Adquirí tus entradas en TuEntrada.com.",
          "tipoBeneficio": {
            "id": 23,
            "nombre": "Espectáculos"
          },
          "descuentoGeneralTarjetas": {
            "tarjetas": [
              {
                "id": 1,
                "nombre": "MasterCard Credito",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "11",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 7,
                "nombre": "Visa Débito",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 18,
                "nombre": "Mastercard Black",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "43",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 23,
                "nombre": "MasterCard Gold",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "13",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 37,
                "nombre": "Visa Agrob",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "codigo": "5563",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 39,
                "nombre": "Visa Classic Nacional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0102",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 40,
                "nombre": "Visa Classic Internacional NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0104",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 41,
                "nombre": "Visa Gold NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0201",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 43,
                "nombre": "Visa Signature",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "1002",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 44,
                "nombre": "Visa Recargable.",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "0001",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 48,
                "nombre": "Visa Segmentada",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 52,
                "nombre": "GAFF TC VISA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              }
            ]
          },
          "lunes": true,
          "martes": true,
          "miercoles": true,
          "jueves": true,
          "viernes": true,
          "sabado": true,
          "domingo": true,
          "sucursales": [
            {
              "id": 38852,
              "nombre": "tuentrada.com",
              "websiteVenta": "https://tuentrada.com",
              "virtual": true,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6993,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "barrio": {},
                "localidad": {
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {}
                },
                "provincia": {}
              },
              "shopping": {},
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "10/09/2018",
              "formattedFechaVigenciaHasta": "30/06/2019"
            }
          ],
          "legales": "2 PROMOCIÓN VÁLIDA PARA COMPRAS CON TARJETAS EMITIDAS POR BBVA FRANCÉS. PARA COMPRAS CON TARJETA DE CRÉDITO VISA O MASTERCARD, LAS COMPRAS EN CUOTAS PUEDEN REALIZARSE ÚNICAMENTE CON TARJETA DE CRÉDITO. LA PRESENTE PROMOCIÓN ES VÁLIDA SÓLO PARA CONSUMOS DE TIPO FAMILIAR. LA BONIFICACIÓN SE ACREDITARA A LOS 60 DIAS DE REALIZADA LA COMPRA. , Y NO SE ACUMULA A OTRAS PROMOCIONES. EN EL CASO DE DUDA SOBRE EL DESTINO DEL CONSUMO,LOS ACCIONISTAS DE BBVA BANCO FRANCÉS S.A. LIMITAN SU RESPONSABILIDAD A LA INTEGRACIÓN DE LAS ACCIONES SUSCRIPTAS LEY 19.550 Y LEY 25.738",
          "idPrioridad": 3,
          "canalesPertenenciaDestacados": [
            {
              "id": 20,
              "nombre": "canal 09",
              "selected": false,
              "email": "canal09@email.com"
            },
            {
              "id": 21,
              "nombre": "canale 10",
              "selected": false,
              "email": "canal10@email.com"
            },
            {
              "id": 22,
              "nombre": "canale 11",
              "selected": false,
              "email": "canal11@email.com"
            },
            {
              "id": 4,
              "nombre": "Excl. BOCA",
              "selected": false
            },
            {
              "id": 6,
              "nombre": "Excl. Plan Sueldo",
              "selected": false
            },
            {
              "id": 3,
              "nombre": "Excl. PREMIUM WORLD",
              "selected": false
            },
            {
              "id": 7,
              "nombre": "Excl. Reactivación",
              "selected": false
            },
            {
              "id": 5,
              "nombre": "Excl. RIVER",
              "selected": false
            },
            {
              "id": 19,
              "nombre": "FGO - Carrusel 2",
              "selected": false,
              "email": "ciriglianog@bbva.com.ar"
            },
            {
              "id": 1,
              "nombre": "Frances GO Portal",
              "selected": true,
              "email": "pablo.radomski@bbva.com",
              "codigo": "Frances GO Portal"
            },
            {
              "id": 9,
              "nombre": "Destacado",
              "selected": true,
              "email": " ",
              "codigo": "destacado"
            },
            {
              "id": 2,
              "nombre": "Frances GO SMS",
              "selected": false,
              "email": "pablo-xoy.group@bbva.com",
              "codigo": "Frances GO SMS"
            }
          ],
          "accionComercial": {},
          "campaniaRelacionada": [
          ],
          "denominacionPromocion": "Ricardo Arjona",
          "localesAdheridos": [
          ],
          "fechaDesdeComunicacion": "2018-09-10",
          "fechaHastaComunicacion": "2019-05-30",
          "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
          "esShopping": false
        }
      ],
      "detalleError": "El beneficio: 10440 no tiene campanias asociadas\u000aEl beneficio: 10440 no tiene locales adheridos\u000aEl beneficio: 10443 no tiene campanias asociadas\u000aEl beneficio: 10443 no tiene locales adheridos\u000aEl beneficio: 10444 no tiene campanias asociadas\u000aEl beneficio: 10444 no tiene locales adheridos\u000aEl beneficio: 10445 no tiene campanias asociadas\u000aEl beneficio: 10445 no tiene locales adheridos\u000aEl beneficio: 10446 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene locales adheridos\u000aEl beneficio: 10447 no tiene campanias asociadas\u000aEl beneficio: 10447 no tiene locales adheridos\u000aEl beneficio: 10449 no tiene campanias asociadas\u000aEl beneficio: 10449 no tiene locales adheridos\u000aEl beneficio: 10450 no tiene campanias asociadas\u000aEl beneficio: 10450 no tiene locales adheridos\u000aEl beneficio: 10451 no tiene campanias asociadas\u000aEl beneficio: 10451 no tiene locales adheridos\u000aEl beneficio: 10452 no tiene campanias asociadas\u000aEl beneficio: 10452 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene campanias asociadas\u000aEl beneficio: 10453 no tiene locales adheridos\u000aEl beneficio: 10454 no tiene campanias asociadas\u000aEl beneficio: 10454 no tiene locales adheridos\u000aEl beneficio: 10455 no tiene campanias asociadas\u000aEl beneficio: 10455 no tiene locales adheridos\u000aEl beneficio: 10456 no tiene campanias asociadas\u000aEl beneficio: 10456 no tiene locales adheridos\u000aEl beneficio: 10472 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene campanias asociadas\u000aEl beneficio: 10474 no tiene locales adheridos\u000aEl beneficio: 10475 no tiene locales adheridos\u000aEl beneficio: 10476 no tiene campanias asociadas\u000aEl beneficio: 10476 no tiene locales adheridos\u000aEl beneficio: 10477 no tiene campanias asociadas\u000aEl beneficio: 10477 no tiene locales adheridos\u000aEl beneficio: 10478 no tiene sucursales asociadas\u000aEl beneficio: 10478 no tiene comercios asociados\u000aEl beneficio: 10478 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene locales adheridos\u000aEl beneficio: 10482 no tiene campanias asociadas\u000aEl beneficio: 10482 no tiene locales adheridos\u000aEl beneficio: 10483 no tiene campanias asociadas\u000aEl beneficio: 10483 no tiene locales adheridos\u000aEl beneficio: 10484 no tiene campanias asociadas\u000aEl beneficio: 10484 no tiene locales adheridos\u000aEl beneficio: 10485 no tiene campanias asociadas\u000aEl beneficio: 10485 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene campanias asociadas\u000aEl beneficio: 10486 no tiene locales adheridos\u000aEl beneficio: 10487 no tiene campanias asociadas\u000aEl beneficio: 10487 no tiene locales adheridos\u000a"
    }}');

    return $jsonObject->BeneficioDrupal->result;
  }

  public static function consultarNovedadesConCampania() {
    $jsonObject = json_decode('{ "BeneficioDrupal": {
    "result": [
      {
        "id": 10475,
        "uri": "null-10475",
        "cuotasSinInteres": 3,
        "comercios": [
          {
            "id": 6613,
            "uri": "null-6613",
            "nombreComercio": "Xoana",
            "rubros": [
              {
                "id": 35,
                "nombre": "Bombonerías y Chocolaterias"
              }
            ]
          }
        ],
        "beneficio": " Xoana 0",
        "tipoDescripcionBeneficio": "cupon",
        "tipoBeneficio": {
          "id": 100,
          "nombre": "Promo Cupon"
        },
        "descuentoGeneralTarjetas": {
          "tarjetas": [
            {
              "id": 39,
              "nombre": "Visa Classic Nacional",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "CR0102",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 40,
              "nombre": "Visa Classic Internacional NA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "CR0104",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 41,
              "nombre": "Visa Gold NA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "CR0201",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 43,
              "nombre": "Visa Signature",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "1002",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 44,
              "nombre": "Visa Recargable.",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "codigo": "0001",
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 48,
              "nombre": "Visa Segmentada",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 49,
              "nombre": "SEGMENTADA VISA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            },
            {
              "id": 52,
              "nombre": "GAFF TC VISA",
              "marcaTarjeta": {
                "id": 1,
                "descripcion": "Visa"
              },
              "grupoTarjeta": {
                "id": 1,
                "descripcion": "Tarjetas de Credito",
                "codigo": "TC"
              },
              "selected": false,
              "habilitada": true,
              "tipoTarjeta": {
                "id": 1,
                "descripcion": "Standard"
              }
            }
          ]
        },
        "lunes": true,
        "martes": true,
        "miercoles": true,
        "jueves": true,
        "viernes": true,
        "sabado": true,
        "domingo": true,
        "sucursales": [
          {
            "id": 38838,
            "nombre": "centro",
            "virtual": false,
            "noAtencionPublico": false,
            "comercio": {
              "id": 6941,
              "checkUltimaImportacion": false,
              "contribuyente": {
                "cliente": false,
                "adquirente": false,
                "proactivo": false
              },
              "selected": false,
              "noConvocable": false,
              "sinComunicacion": false
            },
            "localizacion": {
              "id": 38827,
              "barrio": {
                "id": 36,
                "nombre": "Retiro"
              },
              "localidad": {
                "id": 1,
                "nombre": "Capital Federal",
                "latitud": 0.0,
                "longitud": 0.0,
                "zonaGeografica": {}
              },
              "provincia": {
                "id": 1,
                "nombre": "Capital Federal"
              },
              "coordenadaX": -34.5977897644043,
              "coordenadaY": -58.3699951171875,
              "calle": "Avenida Córdoba",
              "numero": 111,
              "otrosDatos": " "
            },
            "shopping": {},
            "franquicia": false,
            "checkUltimaImportacion": false,
            "formattedFechaVigenciaDesde": "01/05/2018",
            "formattedFechaVigenciaHasta": "31/05/2018"
          }
        ],
        "tope": 0,
        "legales": "LEGALES CUPONES PRUEBA",
        "idPrioridad": 1,
        "canalesPertenenciaDestacados": [
          {
            "id": 20,
            "nombre": "canal 09",
            "selected": false,
            "email": "canal09@email.com"
          },
          {
            "id": 21,
            "nombre": "canale 10",
            "selected": false,
            "email": "canal10@email.com"
          },
          {
            "id": 22,
            "nombre": "canale 11",
            "selected": false,
            "email": "canal11@email.com"
          },
          {
            "id": 4,
            "nombre": "Excl. BOCA",
            "selected": false
          },
          {
            "id": 6,
            "nombre": "Excl. Plan Sueldo",
            "selected": false
          },
          {
            "id": 3,
            "nombre": "Excl. PREMIUM WORLD",
            "selected": false
          },
          {
            "id": 7,
            "nombre": "Excl. Reactivación",
            "selected": false
          },
          {
            "id": 5,
            "nombre": "Excl. RIVER",
            "selected": false
          },
          {
            "id": 19,
            "nombre": "FGO - Carrusel 2",
            "selected": false,
            "email": "ciriglianog@bbva.com.ar"
          },
          {
            "id": 1,
            "nombre": "Frances GO Portal",
            "selected": true,
            "email": "pablo.radomski@bbva.com",
            "codigo": "Frances GO Portal"
          },
          {
            "id": 23,
            "nombre": "Frances GO PUSH",
            "selected": false,
            "email": " ",
            "codigo": "Frances GO PUSH"
          },
          {
            "id": 2,
            "nombre": "Frances GO SMS",
            "selected": false,
            "email": "pablo-xoy.group@bbva.com",
            "codigo": "Frances GO SMS"
          }
        ],
        "accionComercial": {
          "id": 3,
          "valor": "cupon a"
        },
        "campaniaRelacionada": [
          {
            "id": 61,
            "nombre": "con promo",
            "descripcion": "ffgfgg",
            "fechaDesde": "2018-07-18",
            "fechaHasta": "2019-03-31"
          }
        ],
        "denominacionPromocion": "cupon Voucher",
        "localesAdheridos": [
        ],
        "fechaDesdeComunicacion": "2018-09-03",
        "fechaHastaComunicacion": "2019-05-30",
        "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
        "esShopping": false
      }
    ],
    "detalleError": "El beneficio: 10440 no tiene campanias asociadas\u000aEl beneficio: 10440 no tiene locales adheridos\u000aEl beneficio: 10443 no tiene campanias asociadas\u000aEl beneficio: 10443 no tiene locales adheridos\u000aEl beneficio: 10444 no tiene campanias asociadas\u000aEl beneficio: 10444 no tiene locales adheridos\u000aEl beneficio: 10445 no tiene campanias asociadas\u000aEl beneficio: 10445 no tiene locales adheridos\u000aEl beneficio: 10446 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene locales adheridos\u000aEl beneficio: 10447 no tiene campanias asociadas\u000aEl beneficio: 10447 no tiene locales adheridos\u000aEl beneficio: 10449 no tiene campanias asociadas\u000aEl beneficio: 10449 no tiene locales adheridos\u000aEl beneficio: 10450 no tiene campanias asociadas\u000aEl beneficio: 10450 no tiene locales adheridos\u000aEl beneficio: 10451 no tiene campanias asociadas\u000aEl beneficio: 10451 no tiene locales adheridos\u000aEl beneficio: 10452 no tiene campanias asociadas\u000aEl beneficio: 10452 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene campanias asociadas\u000aEl beneficio: 10453 no tiene locales adheridos\u000aEl beneficio: 10454 no tiene campanias asociadas\u000aEl beneficio: 10454 no tiene locales adheridos\u000aEl beneficio: 10455 no tiene campanias asociadas\u000aEl beneficio: 10455 no tiene locales adheridos\u000aEl beneficio: 10456 no tiene campanias asociadas\u000aEl beneficio: 10456 no tiene locales adheridos\u000aEl beneficio: 10472 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene campanias asociadas\u000aEl beneficio: 10474 no tiene locales adheridos\u000aEl beneficio: 10475 no tiene locales adheridos\u000aEl beneficio: 10476 no tiene campanias asociadas\u000aEl beneficio: 10476 no tiene locales adheridos\u000aEl beneficio: 10477 no tiene campanias asociadas\u000aEl beneficio: 10477 no tiene locales adheridos\u000aEl beneficio: 10478 no tiene sucursales asociadas\u000aEl beneficio: 10478 no tiene comercios asociados\u000aEl beneficio: 10478 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene locales adheridos\u000aEl beneficio: 10482 no tiene campanias asociadas\u000aEl beneficio: 10482 no tiene locales adheridos\u000aEl beneficio: 10483 no tiene campanias asociadas\u000aEl beneficio: 10483 no tiene locales adheridos\u000aEl beneficio: 10484 no tiene campanias asociadas\u000aEl beneficio: 10484 no tiene locales adheridos\u000aEl beneficio: 10485 no tiene campanias asociadas\u000aEl beneficio: 10485 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene campanias asociadas\u000aEl beneficio: 10486 no tiene locales adheridos\u000aEl beneficio: 10487 no tiene campanias asociadas\u000aEl beneficio: 10487 no tiene locales adheridos\u000a"
  }}');

    return $jsonObject->BeneficioDrupal->result;
  }


  public static function consultarNovedadesConRubro() {
    $jsonObject = json_decode('{ "BeneficioDrupal": {
  "result": [
    {
      "id": 10478,
      "uri": "null-10478",
      "cuotasSinInteres": 3,
      "comercios": [
      ],
      "beneficio": "todo el pais",
      "tipoBeneficio": {
        "id": 7,
        "nombre": "Belleza"
      },
      "descuentoGeneralTarjetas": {
        "descuentoGeneral": 30,
        "tarjetas": [
          {
            "id": 1,
            "nombre": "MasterCard Credito",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "11",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 7,
            "nombre": "Visa Débito",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 2,
              "descripcion": "Tarjetas de Debito",
              "codigo": "TD"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 18,
            "nombre": "Mastercard Black",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "43",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 23,
            "nombre": "MasterCard Gold",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "13",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 37,
            "nombre": "Visa Agrob",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 2,
              "descripcion": "Tarjetas de Debito",
              "codigo": "TD"
            },
            "codigo": "5563",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 39,
            "nombre": "Visa Classic Nacional",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0102",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 40,
            "nombre": "Visa Classic Internacional NA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0104",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 41,
            "nombre": "Visa Gold NA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "CR0201",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 43,
            "nombre": "Visa Signature",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "1002",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 44,
            "nombre": "Visa Recargable.",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "codigo": "0001",
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 48,
            "nombre": "Visa Segmentada",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 49,
            "nombre": "SEGMENTADA VISA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 50,
            "nombre": "SEGMENTADA MASTER",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 51,
            "nombre": "SEGMENTADA DEBITO",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 2,
              "descripcion": "Tarjetas de Debito",
              "codigo": "TD"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 52,
            "nombre": "GAFF TC VISA",
            "marcaTarjeta": {
              "id": 1,
              "descripcion": "Visa"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          },
          {
            "id": 53,
            "nombre": "GAFF TC MASTER",
            "marcaTarjeta": {
              "id": 2,
              "descripcion": "Mastercard"
            },
            "grupoTarjeta": {
              "id": 1,
              "descripcion": "Tarjetas de Credito",
              "codigo": "TC"
            },
            "selected": false,
            "habilitada": true,
            "tipoTarjeta": {
              "id": 1,
              "descripcion": "Standard"
            }
          }
        ]
      },
      "lunes": false,
      "martes": false,
      "miercoles": false,
      "jueves": false,
      "viernes": false,
      "sabado": false,
      "domingo": false,
      "sucursales": [
      ],
      "legales": "1 EXCLUSIVO PARA CLIENTES DEL BBVA FRANCÉS QUE SE ENCUENTREN REGISTRADOS EN FGO AL DÍA DE LA PROMOCIÓN Y ABONEN SUS COMPRAS CON TARJETAS DE CRÉDITO BBVA FRANCÉS.  CARTERA DE CONSUMO.  PARA COMPRAS REALIZADAS UNICAMENTE CON TARJETAS DE CRÉDITO VISA Y MASTERCARD EMITIDAS POR BBVA FRANCÉS.  LA PRESENTE PROMOCIÓN ES VÁLIDA SOLO PARA CONSUMO DE TIPO FAMILIAR, REALIZADOS EN LOS COMERCIOS DETALLADOS PARA LA PROMOCIÓN Y NO SE ACUMULAN A OTRAS PROMOCIONES VIGENTES.  NO PARTICIPAN TARJETAS VISA PURCHASING, DISTRIBUTION Y CORPORATE.LOS ACCIONISTAS DE BBVA BANCO FRANCÉS S.A. LIMITAN SU RESPONSABILIDAD A LA INTEGRACIÓN DE LAS ACCIONES SUSCRIPTAS A LA LEY 19.550 Y LEY 25.738.  CUIT 30-500000319-3.  RECONQUISTA 199 CABA - CP 1003",
      "idPrioridad": 1,
      "canalesPertenenciaDestacados": [
        {
          "id": 20,
          "nombre": "canal 09",
          "selected": false,
          "email": "canal09@email.com"
        },
        {
          "id": 21,
          "nombre": "canale 10",
          "selected": false,
          "email": "canal10@email.com"
        },
        {
          "id": 22,
          "nombre": "canale 11",
          "selected": false,
          "email": "canal11@email.com"
        },
        {
          "id": 4,
          "nombre": "Excl. BOCA",
          "selected": false
        },
        {
          "id": 6,
          "nombre": "Excl. Plan Sueldo",
          "selected": false
        },
        {
          "id": 3,
          "nombre": "Excl. PREMIUM WORLD",
          "selected": false
        },
        {
          "id": 7,
          "nombre": "Excl. Reactivación",
          "selected": false
        },
        {
          "id": 5,
          "nombre": "Excl. RIVER",
          "selected": false
        },
        {
          "id": 19,
          "nombre": "FGO - Carrusel 2",
          "selected": false,
          "email": "ciriglianog@bbva.com.ar"
        },
        {
          "id": 1,
          "nombre": "Frances GO Portal",
          "selected": true,
          "email": "pablo.radomski@bbva.com",
          "codigo": "Frances GO Portal"
        },
        {
          "id": 23,
          "nombre": "Frances GO PUSH",
          "selected": false,
          "email": " ",
          "codigo": "Frances GO PUSH"
        },
        {
          "id": 2,
          "nombre": "Frances GO SMS",
          "selected": false,
          "email": "pablo-xoy.group@bbva.com",
          "codigo": "Frances GO SMS"
        }
      ],
      "accionComercial": {},
      "campaniaRelacionada": [
      ],
      "denominacionPromocion": "promo rubro",
      "localesAdheridos": [
        {
          "localidad": {
            "latitud": 0.0,
            "longitud": 0.0
          },
          "provincia": {
            "nombre": "Buenos Aires"
          },
          "promocion": 0,
          "promoZonaId": 0
        },
        {
          "localidad": {
            "latitud": 0.0,
            "longitud": 0.0
          },
          "provincia": {
            "nombre": "Capital Federal"
          },
          "promocion": 0,
          "promoZonaId": 0
        },
        {
          "localidad": {
            "latitud": 0.0,
            "longitud": 0.0
          },
          "provincia": {
            "nombre": "Catamarca"
          },
          "promocion": 0,
          "promoZonaId": 0
        },
        {
          "localidad": {
            "latitud": 0.0,
            "longitud": 0.0
          },
          "provincia": {
            "nombre": "Chaco"
          },
          "promocion": 0,
          "promoZonaId": 0
        },
        {
          "localidad": {
            "latitud": 0.0,
            "longitud": 0.0
          },
          "provincia": {
            "nombre": "Chubut"
          },
          "promocion": 0,
          "promoZonaId": 0
        },
        {
          "localidad": {
            "latitud": 0.0,
            "longitud": 0.0
          },
          "provincia": {
            "nombre": "Jujuy"
          },
          "promocion": 0,
          "promoZonaId": 0
        },
        {
          "localidad": {
            "latitud": 0.0,
            "longitud": 0.0
          },
          "provincia": {
            "nombre": "Entre Ríos"
          },
          "promocion": 0,
          "promoZonaId": 0
        }
      ],
      "fechaDesdeComunicacion": "2018-09-05",
      "fechaHastaComunicacion": "2019-05-30",
      "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
      "tituloBeneficio": "Rubro",
      "esShopping": false
    }
  ],
  "detalleError": "El beneficio: 10440 no tiene campanias asociadas\u000aEl beneficio: 10440 no tiene locales adheridos\u000aEl beneficio: 10443 no tiene campanias asociadas\u000aEl beneficio: 10443 no tiene locales adheridos\u000aEl beneficio: 10444 no tiene campanias asociadas\u000aEl beneficio: 10444 no tiene locales adheridos\u000aEl beneficio: 10445 no tiene campanias asociadas\u000aEl beneficio: 10445 no tiene locales adheridos\u000aEl beneficio: 10446 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene locales adheridos\u000aEl beneficio: 10447 no tiene campanias asociadas\u000aEl beneficio: 10447 no tiene locales adheridos\u000aEl beneficio: 10449 no tiene campanias asociadas\u000aEl beneficio: 10449 no tiene locales adheridos\u000aEl beneficio: 10450 no tiene campanias asociadas\u000aEl beneficio: 10450 no tiene locales adheridos\u000aEl beneficio: 10451 no tiene campanias asociadas\u000aEl beneficio: 10451 no tiene locales adheridos\u000aEl beneficio: 10452 no tiene campanias asociadas\u000aEl beneficio: 10452 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene campanias asociadas\u000aEl beneficio: 10453 no tiene locales adheridos\u000aEl beneficio: 10454 no tiene campanias asociadas\u000aEl beneficio: 10454 no tiene locales adheridos\u000aEl beneficio: 10455 no tiene campanias asociadas\u000aEl beneficio: 10455 no tiene locales adheridos\u000aEl beneficio: 10456 no tiene campanias asociadas\u000aEl beneficio: 10456 no tiene locales adheridos\u000aEl beneficio: 10472 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene campanias asociadas\u000aEl beneficio: 10474 no tiene locales adheridos\u000aEl beneficio: 10475 no tiene locales adheridos\u000aEl beneficio: 10476 no tiene campanias asociadas\u000aEl beneficio: 10476 no tiene locales adheridos\u000aEl beneficio: 10477 no tiene campanias asociadas\u000aEl beneficio: 10477 no tiene locales adheridos\u000aEl beneficio: 10478 no tiene sucursales asociadas\u000aEl beneficio: 10478 no tiene comercios asociados\u000aEl beneficio: 10478 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene locales adheridos\u000aEl beneficio: 10482 no tiene campanias asociadas\u000aEl beneficio: 10482 no tiene locales adheridos\u000aEl beneficio: 10483 no tiene campanias asociadas\u000aEl beneficio: 10483 no tiene locales adheridos\u000aEl beneficio: 10484 no tiene campanias asociadas\u000aEl beneficio: 10484 no tiene locales adheridos\u000aEl beneficio: 10485 no tiene campanias asociadas\u000aEl beneficio: 10485 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene campanias asociadas\u000aEl beneficio: 10486 no tiene locales adheridos\u000aEl beneficio: 10487 no tiene campanias asociadas\u000aEl beneficio: 10487 no tiene locales adheridos\u000a"
}}');

    return $jsonObject->BeneficioDrupal->result;
  }

  public static function consultarNovedadesConShopping() {
    $jsonObject = json_decode('{ "BeneficioDrupal": {
      "result": [
        {
          "id": 10483,
          "uri": "null-10483",
          "descuentoSuperior": 20,
          "cuotasSinInteres": 3,
          "comercios": [
            {
              "id": 6992,
              "uri": "null-6992",
              "nombreComercio": "Prueba SH",
              "rubros": [
                {
                  "id": 1,
                  "nombre": "Shoppings"
                }
              ]
            }
          ],
          "beneficio": "hjuy",
          "tipoDescripcionBeneficio": "En el total de la compra",
          "tipoBeneficio": {
            "id": 98,
            "nombre": "Shopping Day"
          },
          "descuentoGeneralTarjetas": {
            "descuentoGeneral": 10,
            "tarjetas": [
              {
                "id": 1,
                "nombre": "MasterCard Credito",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "11",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 7,
                "nombre": "Visa Débito",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 18,
                "nombre": "Mastercard Black",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "43",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 23,
                "nombre": "MasterCard Gold",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "13",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 37,
                "nombre": "Visa Agrob",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "codigo": "5563",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 39,
                "nombre": "Visa Classic Nacional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0102",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 40,
                "nombre": "Visa Classic Internacional NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0104",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 41,
                "nombre": "Visa Gold NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0201",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 43,
                "nombre": "Visa Signature",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "1002",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 44,
                "nombre": "Visa Recargable.",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "0001",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 48,
                "nombre": "Visa Segmentada",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 49,
                "nombre": "SEGMENTADA VISA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 50,
                "nombre": "SEGMENTADA MASTER",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 51,
                "nombre": "SEGMENTADA DEBITO",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 52,
                "nombre": "GAFF TC VISA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 53,
                "nombre": "GAFF TC MASTER",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              }
            ]
          },
          "lunes": true,
          "martes": true,
          "miercoles": true,
          "jueves": true,
          "viernes": true,
          "sabado": false,
          "domingo": false,
          "sucursales": [
            {
              "id": 38850,
              "nombre": "Local X",
              "virtual": false,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6992,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "id": 38843,
                "barrio": {
                  "id": 40,
                  "nombre": "San Telmo"
                },
                "localidad": {
                  "id": 1,
                  "nombre": "Capital Federal",
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {}
                },
                "provincia": {
                  "id": 1,
                  "nombre": "Capital Federal"
                },
                "coordenadaX": -34.6173314,
                "coordenadaY": -58.3706851,
                "calle": "Independencia",
                "numero": 340,
                "otrosDatos": "8"
              },
              "shopping": {
                "id": 136,
                "nombre": "Prueba SH"
              },
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "10/09/2018",
              "formattedFechaVigenciaHasta": "30/06/2019"
            },
            {
              "id": 38851,
              "nombre": "Local Prueba",
              "virtual": false,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6992,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "id": 38844,
                "barrio": {
                  "id": 40,
                  "nombre": "San Telmo"
                },
                "localidad": {
                  "id": 1,
                  "nombre": "Capital Federal",
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {}
                },
                "provincia": {
                  "id": 1,
                  "nombre": "Capital Federal"
                },
                "coordenadaX": -34.6173314,
                "coordenadaY": -58.3706851,
                "calle": "Independencia",
                "numero": 340
              },
              "shopping": {
                "id": 136,
                "nombre": "Prueba SH"
              },
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "10/09/2018",
              "formattedFechaVigenciaHasta": "30/06/2019"
            }
          ],
          "legales": "2 PROMOCIÓN VÁLIDA PARA COMPRAS CON TARJETAS EMITIDAS POR BBVA FRANCÉS. PARA COMPRAS CON TARJETA DE CRÉDITO VISA O MASTERCARD, LAS COMPRAS EN CUOTAS PUEDEN REALIZARSE ÚNICAMENTE CON TARJETA DE CRÉDITO. LA PRESENTE PROMOCIÓN ES VÁLIDA SÓLO PARA CONSUMOS DE TIPO FAMILIAR. LA BONIFICACIÓN SE ACREDITARA A LOS 60 DIAS DE REALIZADA LA COMPRA. , Y NO SE ACUMULA A OTRAS PROMOCIONES. EN EL CASO DE DUDA SOBRE EL DESTINO DEL CONSUMO,LOS ACCIONISTAS DE BBVA BANCO FRANCÉS S.A. LIMITAN SU RESPONSABILIDAD A LA INTEGRACIÓN DE LAS ACCIONES SUSCRIPTAS LEY 19.550 Y LEY 25.738",
          "idPrioridad": 1,
          "canalesPertenenciaDestacados": [
            {
              "id": 1,
              "nombre": "Frances GO Portal",
              "selected": false
            }
          ],
          "accionComercial": {
            "id": 10,
            "valor": "Día de la madre"
          },
          "campaniaRelacionada": [
          ],
          "denominacionPromocion": "shopping",
          "localesAdheridos": [
          ],
          "fechaDesdeComunicacion": "2018-09-10",
          "fechaHastaComunicacion": "2019-06-30",
          "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
          "esShopping": true
        }
      ],
      "detalleError": "El beneficio: 10440 no tiene campanias asociadas\u000aEl beneficio: 10440 no tiene locales adheridos\u000aEl beneficio: 10443 no tiene campanias asociadas\u000aEl beneficio: 10443 no tiene locales adheridos\u000aEl beneficio: 10444 no tiene campanias asociadas\u000aEl beneficio: 10444 no tiene locales adheridos\u000aEl beneficio: 10445 no tiene campanias asociadas\u000aEl beneficio: 10445 no tiene locales adheridos\u000aEl beneficio: 10446 no tiene campanias asociadas\u000aEl beneficio: 10446 no tiene locales adheridos\u000aEl beneficio: 10447 no tiene campanias asociadas\u000aEl beneficio: 10447 no tiene locales adheridos\u000aEl beneficio: 10449 no tiene campanias asociadas\u000aEl beneficio: 10449 no tiene locales adheridos\u000aEl beneficio: 10450 no tiene campanias asociadas\u000aEl beneficio: 10450 no tiene locales adheridos\u000aEl beneficio: 10451 no tiene campanias asociadas\u000aEl beneficio: 10451 no tiene locales adheridos\u000aEl beneficio: 10452 no tiene campanias asociadas\u000aEl beneficio: 10452 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene campanias asociadas\u000aEl beneficio: 10453 no tiene locales adheridos\u000aEl beneficio: 10453 no tiene canales de pertenencia\u000aEl beneficio: 10454 no tiene campanias asociadas\u000aEl beneficio: 10454 no tiene locales adheridos\u000aEl beneficio: 10454 no tiene canales de pertenencia\u000aEl beneficio: 10455 no tiene campanias asociadas\u000aEl beneficio: 10455 no tiene locales adheridos\u000aEl beneficio: 10456 no tiene campanias asociadas\u000aEl beneficio: 10456 no tiene locales adheridos\u000aEl beneficio: 10472 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene campanias asociadas\u000aEl beneficio: 10474 no tiene locales adheridos\u000aEl beneficio: 10474 no tiene canales de pertenencia\u000aEl beneficio: 10475 no tiene locales adheridos\u000aEl beneficio: 10476 no tiene campanias asociadas\u000aEl beneficio: 10476 no tiene locales adheridos\u000aEl beneficio: 10477 no tiene campanias asociadas\u000aEl beneficio: 10477 no tiene locales adheridos\u000aEl beneficio: 10478 no tiene sucursales asociadas\u000aEl beneficio: 10478 no tiene comercios asociados\u000aEl beneficio: 10478 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene campanias asociadas\u000aEl beneficio: 10479 no tiene locales adheridos\u000aEl beneficio: 10482 no tiene campanias asociadas\u000aEl beneficio: 10482 no tiene locales adheridos\u000aEl beneficio: 10483 no tiene campanias asociadas\u000aEl beneficio: 10483 no tiene locales adheridos\u000aEl beneficio: 10484 no tiene campanias asociadas\u000aEl beneficio: 10484 no tiene locales adheridos\u000aEl beneficio: 10485 no tiene campanias asociadas\u000aEl beneficio: 10485 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene campanias asociadas\u000aEl beneficio: 10486 no tiene locales adheridos\u000aEl beneficio: 10486 no tiene canales de pertenencia\u000aEl beneficio: 10487 no tiene campanias asociadas\u000aEl beneficio: 10487 no tiene locales adheridos\u000a"
    }}');

    return $jsonObject->BeneficioDrupal->result;
  }

  public static function consultarNovedadesConCasuisticaYOtroRubro() {
    $jsonObject = json_decode('{ "BeneficioDrupal": {
      "result": [
        {
          "id": 10471,
          "uri": "null-10471",
          "comercios": [
            {
              "id": 6613,
              "uri": "null-6613",
              "nombreComercio": "Xoana",
              "rubros": [
                {
                  "id": 35,
                  "nombre": "Bombonerías y Chocolaterias",
                  "idRubroPadre": 3
                }
              ]
            }
          ],
          "beneficio": "descuento 2x1",
          "tipoDescripcionBeneficio": "Cupon 2x1",
          "tipoBeneficio": {
            "id": 24,
            "nombre": "* FRANCES GO"
          },
          "descuentoGeneralTarjetas": {
            "tarjetas": [
              {
                "id": 1,
                "nombre": "MasterCard Credito",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "11",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 7,
                "nombre": "(451770/451773/468508) Visa Débito",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 12,
                "nombre": "(416861) Visa Recargable Nac",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 13,
                "nombre": "(404022)Visa Regalo",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 16,
                "nombre": "(454074) Visa Classic Nacional NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0101",
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 17,
                "nombre": "(531441) MasterCard Prepaga",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "81",
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 18,
                "nombre": "(553633/516585) Mastercard Black",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "43",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 19,
                "nombre": "(454073) Visa Gold",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0202",
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 20,
                "nombre": "(433831) Visa Platinum",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0702",
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 23,
                "nombre": "(547059/554288) MasterCard Gold",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "13",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 24,
                "nombre": "(521373/523920) MasterCard Platinum",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "18",
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 37,
                "nombre": "Visa Agrob",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "codigo": "5563",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 38,
                "nombre": "(493715) Tarjeta Bussines",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0401",
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 67,
                "nombre": "Visa Agro",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "2002",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 2,
                  "descripcion": "Agro"
                }
              },
              {
                "id": 39,
                "nombre": "(454075) Visa Classic Internacional NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0102",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 40,
                "nombre": "(454074) Visa Classic Nacional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0104",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 52,
                "nombre": "GAFF TC VISA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 41,
                "nombre": "(454073) Visa Gold NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0201",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 49,
                "nombre": "SEGMENTADA VISA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 43,
                "nombre": "(529329/526070) Master Internacional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "1002",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 44,
                "nombre": "(530363) Mastercard Regional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "0001",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 47,
                "nombre": "Visa - Braking Bad",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "BD55",
                "selected": false,
                "habilitada": false,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 48,
                "nombre": "Visa Segmentada",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 50,
                "nombre": "SEGMENTADA MASTER",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 51,
                "nombre": "SEGMENTADA DEBITO",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 53,
                "nombre": "GAFF TC MASTER",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              }
            ]
          },
          "lunes": false,
          "martes": false,
          "miercoles": false,
          "jueves": false,
          "viernes": false,
          "sabado": false,
          "domingo": false,
          "sucursales": [
            {
              "id": 38222,
              "nombre": "RETAIL COMPANY S.A.",
              "telefono1": "4683-7115",
              "virtual": false,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6613,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "id": 38216,
                "barrio": {
                  "id": 11,
                  "nombre": "Centro"
                },
                "localidad": {
                  "id": 1,
                  "nombre": "Capital Federal",
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {}
                },
                "provincia": {
                  "id": 1,
                  "nombre": "Capital Federal"
                },
                "coordenadaX": -34.5990119,
                "coordenadaY": -58.374185899999986,
                "calle": "San Martin",
                "numero": 768
              },
              "shopping": {
                "id": 43,
                "nombre": "Galerias Pacifico"
              },
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "31/07/2018",
              "formattedFechaVigenciaHasta": "30/05/2019"
            },
            {
              "id": 38225,
              "nombre": "RETAIL COMPANY S.A.",
              "telefono1": "4683-7115",
              "virtual": false,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6613,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "id": 38219,
                "barrio": {
                  "id": 37,
                  "nombre": "Saavedra"
                },
                "localidad": {
                  "id": 1,
                  "nombre": "Capital Federal",
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {}
                },
                "provincia": {
                  "id": 1,
                  "nombre": "Capital Federal"
                },
                "coordenadaX": -34.5450579,
                "coordenadaY": -58.487633500000015,
                "calle": "Vedia",
                "numero": 3632
              },
              "shopping": {
                "id": 35,
                "nombre": "Dot Baires Shopping"
              },
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "31/07/2018",
              "formattedFechaVigenciaHasta": "30/05/2019"
            },
            {
              "id": 38226,
              "nombre": "RETAIL COMPANY S.A.",
              "telefono1": "4683-7115",
              "virtual": false,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6613,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "id": 38220,
                "barrio": {},
                "localidad": {
                  "id": 30,
                  "nombre": "Cordoba",
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {}
                },
                "provincia": {
                  "id": 6,
                  "nombre": "Córdoba"
                },
                "coordenadaX": -31.4194902,
                "coordenadaY": -64.18885160000002,
                "calle": "Velez Sarfield",
                "numero": 361
              },
              "shopping": {
                "id": 8,
                "nombre": "Patio Olmos"
              },
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "31/07/2018",
              "formattedFechaVigenciaHasta": "30/05/2019"
            },
            {
              "id": 38499,
              "nombre": "GAXSA",
              "virtual": false,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6613,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "id": 38487,
                "barrio": {},
                "localidad": {
                  "id": 5,
                  "nombre": "La Plata",
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {}
                },
                "provincia": {
                  "id": 24,
                  "nombre": "Buenos Aires"
                },
                "coordenadaX": -34.91508483886719,
                "coordenadaY": -57.95387268066406,
                "calle": "calle 9",
                "numero": 762
              },
              "shopping": {},
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "31/07/2018",
              "formattedFechaVigenciaHasta": "30/05/2019"
            },
            {
              "id": 38500,
              "nombre": "GAXSA",
              "virtual": false,
              "noAtencionPublico": false,
              "comercio": {
                "id": 6613,
                "checkUltimaImportacion": false,
                "contribuyente": {
                  "cliente": false,
                  "adquirente": false,
                  "proactivo": false
                },
                "selected": false,
                "noConvocable": false,
                "sinComunicacion": false
              },
              "localizacion": {
                "id": 38488,
                "barrio": {},
                "localidad": {
                  "id": 492,
                  "nombre": "Martinez",
                  "latitud": 0.0,
                  "longitud": 0.0,
                  "zonaGeografica": {
                    "id": 2,
                    "nombre": "Zona Norte GBA",
                    "idRegionOrdenable": 3
                  }
                },
                "provincia": {
                  "id": 24,
                  "nombre": "Buenos Aires"
                },
                "coordenadaX": -34.5087642,
                "coordenadaY": -58.52176429999997,
                "calle": "Parana",
                "numero": 3745
              },
              "shopping": {
                "id": 3,
                "nombre": "Unicenter"
              },
              "franquicia": false,
              "checkUltimaImportacion": false,
              "formattedFechaVigenciaDesde": "31/07/2018",
              "formattedFechaVigenciaHasta": "30/05/2019"
            }
          ],
          "legales": "LEGAL BENEFICIO SIN PROMO PRUEBA",
          "idPrioridad": 1,
          "canalesPertenenciaDestacados": [
            {
              "id": 20,
              "nombre": "canal 09",
              "selected": false,
              "email": "canal09@email.com"
            },
            {
              "id": 21,
              "nombre": "canale 10",
              "selected": false,
              "email": "canal10@email.com"
            },
            {
              "id": 22,
              "nombre": "canale 11",
              "selected": false,
              "email": "canal11@email.com"
            },
            {
              "id": 4,
              "nombre": "Excl. BOCA",
              "selected": false
            },
            {
              "id": 6,
              "nombre": "Excl. Plan Sueldo",
              "selected": false
            },
            {
              "id": 3,
              "nombre": "Excl. PREMIUM WORLD",
              "selected": false
            },
            {
              "id": 7,
              "nombre": "Excl. Reactivación",
              "selected": false
            },
            {
              "id": 5,
              "nombre": "Excl. RIVER",
              "selected": false
            },
            {
              "id": 19,
              "nombre": "FGO - Carrusel 2",
              "selected": false,
              "email": "ciriglianog@bbva.com.ar"
            },
            {
              "id": 1,
              "nombre": "Frances GO Portal",
              "selected": true,
              "email": "pablo.radomski@bbva.com",
              "codigo": "Frances GO Portal"
            },
            {
              "id": 23,
              "nombre": "Frances GO PUSH",
              "selected": false,
              "email": " ",
              "codigo": "Frances GO PUSH"
            },
            {
              "id": 2,
              "nombre": "Frances GO SMS",
              "selected": false,
              "email": "pablo-xoy.group@bbva.com",
              "codigo": "Frances GO SMS"
            }
          ],
          "accionComercial": {
            "id": 3,
            "valor": "cupon a"
          },
          "campaniaRelacionada": [
          ],
          "localesAdheridos": [
          ],
          "fechaDesdeComunicacion": "2018-07-31",
          "fechaHastaComunicacion": "2019-05-30",
          "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
          "casuistica": {
            "id": 1,
            "descripcion": "2X1"
          },
          "esShopping": false,
          "rubros": [
          ]
        },
        {
          "id": 10490,
          "uri": "null-10490",
          "comercios": [
          ],
          "beneficio": "ghyter",
          "tipoDescripcionBeneficio": "En el total de la compra",
          "tipoBeneficio": {
            "id": 6,
            "nombre": "RUBRO"
          },
          "descuentoGeneralTarjetas": {
            "descuentoGeneral": 30,
            "tarjetas": [
              {
                "id": 1,
                "nombre": "MasterCard Credito",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "11",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 7,
                "nombre": "(451770/451773/468508) Visa Débito",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 18,
                "nombre": "(553633/516585) Mastercard Black",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "43",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 23,
                "nombre": "(547059/554288) MasterCard Gold",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "13",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 37,
                "nombre": "Visa Agrob",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "codigo": "5563",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 39,
                "nombre": "(454075) Visa Classic Internacional NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0102",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 40,
                "nombre": "(454074) Visa Classic Nacional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0104",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 41,
                "nombre": "(454073) Visa Gold NA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "CR0201",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 43,
                "nombre": "(529329/526070) Master Internacional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "1002",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 44,
                "nombre": "(530363) Mastercard Regional",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "codigo": "0001",
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 48,
                "nombre": "Visa Segmentada",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 49,
                "nombre": "SEGMENTADA VISA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 50,
                "nombre": "SEGMENTADA MASTER",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 51,
                "nombre": "SEGMENTADA DEBITO",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 2,
                  "descripcion": "Tarjetas de Debito",
                  "codigo": "TD"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 52,
                "nombre": "GAFF TC VISA",
                "marcaTarjeta": {
                  "id": 1,
                  "descripcion": "Visa"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              },
              {
                "id": 53,
                "nombre": "GAFF TC MASTER",
                "marcaTarjeta": {
                  "id": 2,
                  "descripcion": "Mastercard"
                },
                "grupoTarjeta": {
                  "id": 1,
                  "descripcion": "Tarjetas de Credito",
                  "codigo": "TC"
                },
                "selected": false,
                "habilitada": true,
                "tipoTarjeta": {
                  "id": 1,
                  "descripcion": "Standard"
                }
              }
            ]
          },
          "lunes": false,
          "martes": false,
          "miercoles": false,
          "jueves": false,
          "viernes": false,
          "sabado": false,
          "domingo": false,
          "sucursales": [
          ],
          "legales": "1 EXCLUSIVO PARA CLIENTES DEL BBVA FRANCÉS QUE SE ENCUENTREN REGISTRADOS EN FGO AL DÍA DE LA PROMOCIÓN Y ABONEN SUS COMPRAS CON TARJETAS DE CRÉDITO BBVA FRANCÉS.  CARTERA DE CONSUMO.  PARA COMPRAS REALIZADAS UNICAMENTE CON TARJETAS DE CRÉDITO VISA Y MASTERCARD EMITIDAS POR BBVA FRANCÉS.  LA PRESENTE PROMOCIÓN ES VÁLIDA SOLO PARA CONSUMO DE TIPO FAMILIAR, REALIZADOS EN LOS COMERCIOS DETALLADOS PARA LA PROMOCIÓN Y NO SE ACUMULAN A OTRAS PROMOCIONES VIGENTES.  NO PARTICIPAN TARJETAS VISA PURCHASING, DISTRIBUTION Y CORPORATE.LOS ACCIONISTAS DE BBVA BANCO FRANCÉS S.A. LIMITAN SU RESPONSABILIDAD A LA INTEGRACIÓN DE LAS ACCIONES SUSCRIPTAS A LA LEY 19.550 Y LEY 25.738.  CUIT 30-500000319-3.  RECONQUISTA 199 CABA - CP 1003",
          "idPrioridad": 1,
          "canalesPertenenciaDestacados": [
            {
              "id": 20,
              "nombre": "canal 09",
              "selected": false,
              "email": "canal09@email.com"
            },
            {
              "id": 21,
              "nombre": "canale 10",
              "selected": false,
              "email": "canal10@email.com"
            },
            {
              "id": 22,
              "nombre": "canale 11",
              "selected": false,
              "email": "canal11@email.com"
            },
            {
              "id": 4,
              "nombre": "Excl. BOCA",
              "selected": false
            },
            {
              "id": 6,
              "nombre": "Excl. Plan Sueldo",
              "selected": false
            },
            {
              "id": 3,
              "nombre": "Excl. PREMIUM WORLD",
              "selected": false
            },
            {
              "id": 7,
              "nombre": "Excl. Reactivación",
              "selected": false
            },
            {
              "id": 5,
              "nombre": "Excl. RIVER",
              "selected": false
            },
            {
              "id": 19,
              "nombre": "FGO - Carrusel 2",
              "selected": false,
              "email": "ciriglianog@bbva.com.ar"
            },
            {
              "id": 1,
              "nombre": "Frances GO Portal",
              "selected": true,
              "email": "pablo.radomski@bbva.com",
              "codigo": "Frances GO Portal"
            },
            {
              "id": 23,
              "nombre": "Frances GO PUSH",
              "selected": false,
              "email": " ",
              "codigo": "Frances GO PUSH"
            },
            {
              "id": 2,
              "nombre": "Frances GO SMS",
              "selected": false,
              "email": "pablo-xoy.group@bbva.com",
              "codigo": "Frances GO SMS"
            }
          ],
          "accionComercial": {
            "id": 13,
            "valor": "Último minuto"
          },
          "campaniaRelacionada": [
          ],
          "denominacionPromocion": "Rubro",
          "localesAdheridos": [
            {
              "localidad": {
                "nombre": "Mar De Ajo",
                "latitud": 0.0,
                "longitud": 0.0
              },
              "provincia": {
                "nombre": "Buenos Aires"
              },
              "promocion": 0,
              "promoZonaId": 0
            },
            {
              "localidad": {
                "nombre": "Valeria del mar",
                "latitud": 0.0,
                "longitud": 0.0
              },
              "provincia": {
                "nombre": "Buenos Aires"
              },
              "promocion": 0,
              "promoZonaId": 0
            },
            {
              "localidad": {
                "nombre": "Mar de las Pampas",
                "latitud": 0.0,
                "longitud": 0.0
              },
              "provincia": {
                "nombre": "Buenos Aires"
              },
              "promocion": 0,
              "promoZonaId": 0
            },
            {
              "localidad": {
                "nombre": "Pinamar",
                "latitud": 0.0,
                "longitud": 0.0
              },
              "provincia": {
                "nombre": "Buenos Aires"
              },
              "promocion": 0,
              "promoZonaId": 0
            },
            {
              "localidad": {
                "nombre": "Mar Del Plata",
                "latitud": 0.0,
                "longitud": 0.0
              },
              "provincia": {
                "nombre": "Buenos Aires"
              },
              "promocion": 0,
              "promoZonaId": 0
            }
          ],
          "fechaDesdeComunicacion": "2019-03-29",
          "fechaHastaComunicacion": "2019-04-30",
          "descripcionCFT": "COSTO FINANCIERO TOTAL: 3.59%TASA NOMINAL ANUAL 0,00%, TASA EFECTIVA ANUAL 0,00%, SEGURO DE VIDA 0,29%.",
          "casuistica": {},
          "tituloBeneficio": "costa atlantica",
          "esShopping": false,
          "rubros": [
            {
              "id": 1,
              "nombre": "Librería",
              "idPadre": 4
            },
            {
              "id": 2,
              "nombre": "Libros",
              "idPadre": 4
            },
            {
              "id": 46,
              "nombre": "Teatros",
              "idPadre": 4
            }
          ]
        }
      ],
      "detalleError": "El beneficio: 10471 no tiene campanias asociadas\u000aEl beneficio: 10471 no tiene locales adheridos\u000aEl beneficio: 10471 no tiene rubros\u000aEl beneficio: 10490 no tiene sucursales asociadas\u000aEl beneficio: 10490 no tiene comercios asociados\u000aEl beneficio: 10490 no tiene campanias asociadas\u000a"
    }}');

    return $jsonObject->BeneficioDrupal->result;
  }

}



