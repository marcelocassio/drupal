<?php

  function fgo_importacion_get_file($fileName){
    
    $file = file_get_contents($fileName);
    if ($file === FALSE) {
      fgo_importacion_loguear("Error al abrir el archivo " . $fileName);
      return array();
    } 
    else {
      $file = str_replace("\n", "", $file);
      return array_filter(explode("\\n", $file));
    }
  }

  function fgo_importacion_finalizada($success, $results, $operations){
    if ($success) {
      drupal_set_message(t('Import all esta listo!'));
    } else {
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], true)));
      drupal_set_message($message, 'error');
    }
  }

  function fgo_importacion_loguear($texto) {
    echo date("Y-m-d H:i:s")." - ".$texto.PHP_EOL;
  }

  function fgo_importacion_calcular_cftna($cftna){
    if ($cftna) {
      $cftExt=array();
      preg_match_all('/(cftna:|CFTNA:|costo financiero total:|costo financiero total efectivo anual:|COSTO FINANCIERO TOTAL:|COSTO FINANCIERO TOTAL EFECTIVO ANUAL:)\s*(\d{1,2}[\,\.]{1}\d{1,2})(%)/', $cftna, $cftExt);
      $cftExt2= $cftExt[2];
      $cftExt3= $cftExt2[0];
      $cft = str_replace(",", ".", $cftExt3);
      return (float)$cft;
    }else{
      return null;
    }
  }
 
  function fgo_importar_encontrar_similares($id_comunicacion, $comunicaciones_similares) {
    
    $contador = 0;
    $comunicacion = $comunicaciones_similares[$id_comunicacion];
    $comunicaciones_encontradas = array();
    foreach ($comunicaciones_similares as $key_encontrar => $comunicacionParecida){
      
      if (count(array_intersect($comunicacion, $comunicacionParecida)) > 0 && $id_comunicacion != $key_encontrar) {
        $comunicaciones_encontradas[] = $key_encontrar;
        $contador++;
      }
      
      if($contador == 5){
        break;
      }

    }
    return $comunicaciones_encontradas;
  }

  function fgo_importar_convertir_utf8($texto) {
    return trim(iconv('Windows-1252','UTF-8', $texto));
  }

  function fgo_importar_convertir_fecha($fechaTexto) {
    return date( "Y-m-d", strtotime($fechaTexto));
  }

  function fgo_importar_convertir_fecha_timestamp($fechaTexto) {
    return strtotime($fechaTexto);
  }

  /**
   * Peso (ordena de mayor a menor)
   * ==============================
   */
  function fgo_importacion_ordenar() {

    fgo_importacion_db_resetear_orden();
    $comunicaciones = fgo_importacion_db_obtener_descuento_y_cuotas();
    $peso_comunicaciones = array();

    foreach ($comunicaciones as $kc => $comunicacion) {

      $pts_comunicacion = array();

      foreach ($comunicacion as $beneficio) {

        if($beneficio['cuota'] === NULL) {
          $beneficio['cuota'] = 0;
        }
        if($beneficio['prioridad'] === NULL) {
          $beneficio['prioridad'] = 5;
        }

        $formulas = array();

        // Teniendo en cuenta el máximo posible dando 16.146 y el mínimo 0
        if(!empty($beneficio['casuistica'])) {
          $formulas[] = 16147;
        }
        else{
          // Valor = Descuentos máximo 100%
          $formulas[] = (int) $beneficio['valor'] * 100;  // Máximo total = 10.000
        }

        // Si es destacado sumo lo mismo que para los especiales mas el maximo de descuento (10000) mas el maximo de cuotas (120) para que queden arriba, los especiales y los comunes que esten destacados (porque ademas de sumar por destacado suman por su descuento)
       
        if($beneficio['destacado']) {
          $formulas[] = 26267;
        }

        // Cuotas máximo 12
        $formulas[] = (int) $beneficio['cuota'] * 10;    // Máximo total = 120
        $formulas[] = (int) 5 - $beneficio['prioridad'];    // Máximo total = 4

        if($beneficio['valor'] >= 30) {
          // Sumo lo mismo que en cuotas para que las cuotas no superen a los beneficios q tengan >= 30%
          $formulas[] = 3011;
        }
        if($beneficio['cuota'] >= 12) {
          // 29% * 100 (descuento) + 11 * 10 (cuotas) = 3010 (me aseguro que quede arriba de los descuentos con hasta 29% y hasta 11 cuotas)
          $formulas[] = 3011;
        }

        $pts_comunicacion[] = array_sum($formulas);
      }
      $peso_comunicaciones[max($pts_comunicacion)][] = $kc;
    }

    if(count($peso_comunicaciones) > 0) {
      foreach ($peso_comunicaciones as $peso => $comunicaciones) {
        fgo_importacion_db_setear_orden($peso, $comunicaciones);
      }
    }

    fgo_importacion_loguear('Importar Ordenar - Se ordeno la tabla: fgo_comunicacion por causistica(switchCase)');
    eol();
    hd('Importar Ordenar - Se ordeno la tabla: fgo_comunicacion por causistica(switchCase)');

    return $peso_comunicaciones;
  }

  function fgo_importacion_adherir_imagenes_para_comercios($comunicacionBo) {
    if (!isset($comunicacionBo->comercio->idComercio) && count($comunicacionBo->rubros) > 0){
      return;
    }
    $comercioActualizado = new \Fgo\Bo\ComercioBo($comunicacionBo->comercio->idComercio);

    $id_comercio = $comercioActualizado->idComercio;
    $sin_imagen = true;
    $imagenes = null;
    $conteo_rubros = count($comunicacionBo->rubros);

    // - Si el primer rubro cargado es un PADRE, tomamos la imagen de ahi
    if(empty($comunicacionBo->rubros[0]->rubroPadre)){
      $imagenes = $comunicacionBo->rubros[0]->imagenes->data;
    }else{ // - Si el primer rubro cargado es un HIJO, tomamos la imagen de ahi
      $imagenes = $comunicacionBo->rubros[0]->rubroPadre->imagenes->data;
    }

    // Si hay mas de una categoria, se toma el padre de la primera categoria
    if ($conteo_rubros >= 2 ) { // Se toman los fids del padre
      if (!empty($comunicacionBo->rubros[0]->rubroPadre)) {
        $imagenes = $comunicacionBo->rubros[0]->rubroPadre->imagenes->data;
      }else{
        $imagenes = $comunicacionBo->rubros[0]->imagenes->data;
      }
    }

    $imagenes_comercios = $comercioActualizado->imagenes;

    if (!empty($imagenes_comercios)){
      foreach ($imagenes_comercios as $imagen_comercio){
        if ($imagen_comercio->codigoTipoImagen == 1){
          $sin_imagen = false;
          break;
        }
      }
    }

    //Si el comercio no tiene imagen se asigna una random
    if ($sin_imagen && !empty($imagenes)){
      $registro = new stdClass();
      $imagen_aleatoria = array_rand($imagenes, 1);
      $comercioImagenBo = new \Fgo\Bo\ComercioImagenBo();
      $registro->id_comercio = $id_comercio;
      $registro->id_imagen = $imagenes[$imagen_aleatoria]->idImagen;
      $comercioImagenBo->construir($registro);
      $comercioImagenBo->guardar();
    }

  }

  /**
   * Actualiza el cache de rubros_pdm
   * */
  function fgo_importacion_actualizar_cache_rubros_pdm() {
    $cache = BBVA\Cache\CacheFile::getInstance();
    $listado = \Fgo\Bo\RubroBo::listarRubrosVisiblesEnPdm();
    $data = \Fgo\Services\Dto\Rubro::contruirEnJerarquia($listado);
    if ($data) {
      $cache->set(Fgo\Services\Api::TIPO_CACHE_PARAMETROS, Fgo\Services\Api::NOMBRE_CACHE_RUBROS_PDM, $data);
    }
  }