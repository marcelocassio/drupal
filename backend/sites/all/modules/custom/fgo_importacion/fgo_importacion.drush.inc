<?php
/*
 * Implements hook_drush_command().
 */
function fgo_importacion_drush_command() {

  $items['importar-todo'] = array(
    'description' => 'Realiza la importacion de tablas de suip a drupal',
    'aliases' => array('importarTodo'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}



function drush_fgo_importacion_importar_todo() {

  $batch = array(
    'operations' => array(),
    'title' => t('Batch importar todo'),
    'init_message' => t('Importando Datos...'),
    'error_message' => t('An error occurred'),
    'finished' => 'fgo_importacion_finalizada',
    'file' => drupal_get_path('module', 'fgo_importacion') . '/fgo_importacion.batch.inc',
  );


  // Start the batch job.

  $batch['operations'][] = array('fgo_importacion_importar_todo' , array());

  batch_set($batch);
  drush_backend_batch_process();

}

