<?php

class ArchivoParser{

  public static function parsearLineaCampania($linea){
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_campania_suip' => $linea[0],
      'nombre' => fgo_importar_convertir_utf8($linea[1]),
      'fecha_desde' => strtotime($linea[3]),
      'fecha_hasta' => strtotime($linea[4]),
    );
    return (object)$dto;
  }

  public static function parsearLineaComercio($linea){
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_comercio_suip' => $linea[0],
      'nombre' => fgo_importar_convertir_utf8($linea[1]),
      'id_shopping' => $linea[9],
      'id_localidad' => $linea[11],
      'id_provincia' => $linea[12],
      'latitud' => $linea[13],
      'longitud' => $linea[14],
      'calle' => fgo_importar_convertir_utf8($linea[15]),
      'calle_numero' => $linea[16]
    );
    return (object)$dto;
  }
  public static function parsearLineaAplicacionBeneficio($linea){
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_tipo_aplicacion_beneficio_suip' => $linea[0],
      'nombre' => fgo_importar_convertir_utf8($linea[1]),
    );
    return (object)$dto;
  }

  public static function parsearLineaAccionComercial($linea){
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_tipo_accion_comercial_suip' => $linea[0],
      'nombre' => fgo_importar_convertir_utf8($linea[1]),
    );
    return (object)$dto;
  }

  public static function parsearLineaBeneficios($linea) {
    $linea = explode("\\tc", $linea);
    $id_promocion = $linea[20];
    $legales = fgo_importar_convertir_utf8($linea[9]);
    if($id_promocion && $id_promocion > 0 ){
        $legales .= " ".fgo_importar_convertir_utf8($linea[30]);
    }
    
    $dto = array(
      'id_beneficio_suip' => $linea[0],
      'id_tipo_beneficio_suip' => $linea[1],
      'id_prioridad' => $linea[2],
      'identificador' => $linea[3],
      'descripcion_extendida' => fgo_importar_convertir_utf8($linea[6]),
      'fecha_desde' => fgo_importar_convertir_fecha_timestamp($linea[7]),
      'fecha_hasta' => fgo_importar_convertir_fecha_timestamp($linea[8]),
      'legales' => $legales,
      'descripcion_cft' => fgo_importar_convertir_utf8($linea[10]),
      'cft' => fgo_importacion_calcular_cftna(fgo_importar_convertir_utf8($linea[10])),
      'id_promocion' => $linea[20],
      'zonas_adheridas' => fgo_importar_convertir_utf8($linea[23]),
      'titulo' => fgo_importar_convertir_utf8($linea[24]),
      'descuento_superior' => $linea[28],
      'descuento_cartera_general' => $linea[29],
      'cuotas' => $linea[31],
      'denominacion_promocion' => fgo_importar_convertir_utf8($linea[32]),
      'descripcion_corta' => fgo_importar_convertir_utf8($linea[33]),
      'id_aplicacion_beneficio_suip' => $linea[36],
      'id_accion_comercial_suip' => $linea[37],
      'id_comercio_suip' => (trim($linea[38]) != "")?$linea[38]:null,
      'destacado' => (isset($linea[39])?$linea[39]:0),
      'es_shopping' => (isset($linea[40])?(int)$linea[40]:0),
      'id_casuistica_suip' => (isset($linea[41])?$linea[41]:0),
    );
    return (object)$dto;
  }
  public static function parsearLineaRubro($linea){
    $linea = explode("\\tc", $linea);

    $dto = array(
      'id_rubro_padre' => $linea[1],
      'id_rubro_suip' => $linea[0],
      'nombre' => fgo_importar_convertir_utf8($linea[2])
    );
    return (object)$dto;
  }

  public static function parsearLineaProvincias($linea) {
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_provincia_suip' => $linea[0],
      'nombre' => fgo_importar_convertir_utf8($linea[2])
    );
    return (object)$dto;
  }

  public static function parsearLineaBeneficioComercio($linea) {
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_comercio_suip' => $linea[0],
      'id_beneficio_suip' => $linea[1],
    );
    return (object)$dto;
  }

  public static function parsearLineaLocalidades($linea) {
    $linea = explode("\\tc", $linea);
    if (!$linea[4]){  $codigo_postal = null;  }else{ $codigo_postal = $linea[4]; }
    $dto = array(
        'id_localidad_suip' => $linea[0],
        'nombre' => fgo_importar_convertir_utf8($linea[3]),
        'codigo_postal' => $codigo_postal
    );
    return (object)$dto;
  }

  public static function parsearLineaSucursalesVirtuales($linea) {
    $linea = explode("\\tc", $linea);
    if ($linea[4]){ // Sucursal Virtual
      $dto = array(
        'id_sucursal_virtual_suip' => $linea[0],
        'web' => fgo_importar_convertir_utf8($linea[3]),
        'telefono' => fgo_importar_convertir_utf8($linea[2]),
      );
      return (object)$dto;
    }else{
      return null;
    }
  }

  public static function parsearLineaComunicacionSucursalesVirtuales($linea) {
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_comunicacion' => $linea[0],
      'id_sucursal_virtual' => $linea[2]
    );
    return (object)$dto;
  }

  public static function parsearLineaSucursalesFisicas($linea) {
    $linea = explode("\\tc", $linea);
    if (!$linea[4]){ // No es sucursal virtual
      $dto = array(
        'id_sucursal_fisica_suip' => $linea[0],
        'nombre' => fgo_importar_convertir_utf8($linea[1]),
        'venta_telefonica' => $linea[2],
        'venta_web' => $linea[3],
        'latitud' => floatval($linea[5]),
        'longitud' => floatval($linea[6]),
        'calle' => fgo_importar_convertir_utf8($linea[7]),
        'calle_numero' => $linea[8],
        'id_localidad' => $linea[10],
        'id_provincia' => $linea[11]

      );
      return (object)$dto;
    }else{
      return null;
    }
  }

  public static function parsearLineaComunicacionSucursalesFisicas($linea) {
    $linea = explode("\\tc", $linea);
    $dto = array(
        'id_comunicacion' => $linea[0],
        'id_sucursal_fisica' => $linea[2]
    );
    return (object)$dto;
  }

  public static function parsearLineaCampaniaComunicacion($linea) {
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_campania_suip' => $linea[0],
      'id_beneficio_suip' => $linea[1],
    );
    return (object)$dto;
  }

  public static function parsearLineaComunicacionRubro($linea) {
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_beneficio_suip' => $linea[0],
      'id_rubro_suip' => $linea[1],
    );
    return (object)$dto;
  }

  public static function parsearLineaCasuistica($linea) {
    $linea = explode("\\tc", $linea);
    $dto = array(
      'id_casuistica_suip' => $linea[0],
      'descripcion' => fgo_importar_convertir_utf8($linea[1]),
    );
    return (object)$dto;
  }
}