<?php

namespace Fgo\Importacion\Dto;


class Comunicacion {

  const CANAL_PORTAL_FGO = 1;
  const CANAL_DESTACADO = 9;

  public $id_beneficio_suip;
  public $id_tipo_beneficio_suip;
  public $id_prioridad;
  public $descripcion_extendida;
  public $fecha_desde;
  public $fecha_hasta;
  public $legales;
  public $descripcion_cft;
  public $cft;
  public $zonas_adheridas;
  public $titulo;
  public $descuento_superior;
  public $descuento_cartera_general;
  public $cuotas;
  public $denominacion_promocion;
  public $descripcion_corta;
  public $id_aplicacion_beneficio_suip;
  public $id_accion_comercial_suip;
  public $id_comercio_suip;
  public $destacado;
  public $es_shopping;
  public $id_casuistica_suip;

  public $comercioDto;
  public $accionComercialDto;
  public $sucursalesFisicasDto;
  public $sucursalesVirtualesDto;
  public $campaniasDto;
  public $casuisticaDto;

  public $rubrosSuip;


  /**
   * @param $dataSuip
   */
  public function construirDesdeServicio($dataSuip)
  {
    $this->id_beneficio_suip = $dataSuip->id;
    $this->id_tipo_beneficio_suip = (isset($dataSuip->tipoBeneficio) ? (int)$dataSuip->tipoBeneficio->id : null);
    $this->id_prioridad = (int)$dataSuip->idPrioridad;
    $this->descripcion_extendida = $dataSuip->beneficio;
    $this->fecha_desde = strtotime($dataSuip->fechaDesdeComunicacion);
    $this->fecha_hasta = strtotime($dataSuip->fechaHastaComunicacion);
    $this->legales = $dataSuip->legales;
    $this->descripcion_cft = $dataSuip->descripcionCFT;
    $this->cft = $this->calcularCftna($dataSuip->descripcionCFT);
    $this->titulo = $dataSuip->tituloBeneficio;
    $this->descuento_cartera_general = $this->tomarDescuentoGeneral($dataSuip);
    $this->descuento_superior = $dataSuip->descuentoSuperior;
    $this->cuotas = isset($dataSuip->cuotasSinInteres)?$dataSuip->cuotasSinInteres:null;
    $this->denominacion_promocion = $dataSuip->denominacionPromocion;
    $this->descripcion_corta = $dataSuip->tipoDescripcionBeneficio;
    $this->id_accion_comercial_suip = (isset($dataSuip->accionComercial) ? (int)$dataSuip->accionComercial->id : null);

    $this->rubrosSuip = $this->construirRubros($dataSuip);
    $this->es_shopping = $dataSuip->esShopping;

    $this->construirCasuistica($dataSuip);
    $this->construirDestacadoPublicado($dataSuip);
    $this->construirAccionComercialDto($dataSuip);
    $this->construirSucursalesDto($dataSuip);
    $this->construirComercioDto($dataSuip);
    $this->construirCampaniaDto($dataSuip);
    $this->construirZonasAdheridas($dataSuip);
  }

  private function tomarDescuentoGeneral($dataSuip) {
    $descuentoGeneral = null;
    if (isset($dataSuip->descuentoGeneralTarjetas)
      && isset($dataSuip->descuentoGeneralTarjetas->descuentoGeneral)) {
      $descuentoGeneral = $dataSuip->descuentoGeneralTarjetas->descuentoGeneral;
    }
    return $descuentoGeneral;
  }

  private function construirCasuistica($dataSuip) {
    if (isset($dataSuip->casuistica) && $dataSuip->casuistica) {
      $this->casuisticaDto = new \stdClass();
      $this->casuisticaDto->id_casuistica_suip = $dataSuip->casuistica->id;
      $this->casuisticaDto->descripcion = $dataSuip->casuistica->descripcion;
      $this->id_casuistica_suip = $dataSuip->casuistica->id;
    }
  }

  private function construirComercioDto($dataSuip) {

    if (isset($dataSuip->comercios) && count($dataSuip->comercios) > 0) {
      $comercioSuip = $dataSuip->comercios[0];
      $comercioDto = new \stdClass();
      $comercioDto->id_comercio_suip = $comercioSuip->id;
      $comercioDto->nombre = $comercioSuip->nombreComercio;
      $comercioDto->id_shopping = null;
      $comercioDto->id_localidad = null;
      $comercioDto->id_provincia = null;
      $comercioDto->latitud = null;
      $comercioDto->longitud = null;
      $comercioDto->calle = null;
      $comercioDto->calle_numero = null;
      if ($dataSuip->esShopping && count($dataSuip->sucursales) > 0) {
        foreach ($dataSuip->sucursales as $sucursal) {
          if (!$sucursal->virtual && isset($sucursal->shopping)) {
            $comercioDto->id_shopping = $sucursal->shopping->id;
            $comercioDto->id_localidad = $sucursal->localizacion->localidad->id;
            $comercioDto->id_provincia = $sucursal->localizacion->provincia->id;
            $comercioDto->latitud = $sucursal->localizacion->coordenadaY;
            $comercioDto->longitud = $sucursal->localizacion->coordenadaX;
            $comercioDto->calle = $sucursal->localizacion->calle;
            $comercioDto->calle_numero = $sucursal->localizacion->numero;
            break;
          }
        }
      }
      $this->comercioDto = $comercioDto;
      $this->id_comercio_suip = $comercioDto->id_comercio_suip;
    }
  }

  /**
  * Construir DTO de rubros
  */
  private function construirRubros($dataSuip) {
    $rubros = array();
    if (isset($dataSuip->comercios) && count($dataSuip->comercios) > 0) {
      $comercioSuip = $dataSuip->comercios[0];
      if (isset($comercioSuip->rubros) && count($comercioSuip->rubros) > 0) {
        foreach ($comercioSuip->rubros as $rubroSuip) {
          $rubroDto = new \stdClass();
          $rubroDto->id_rubro_padre = (isset($rubroSuip->idPadre) && $rubroSuip->idPadre)?$rubroSuip->idPadre:null;
          $rubroDto->id_rubro_suip = $rubroSuip->id;
          $rubroDto->nombre = $rubroSuip->nombre;
          $rubros[] = $rubroDto;
        }
      }
    }
    return $rubros;
  }

  private function construirAccionComercialDto($dataSuip) {

    if (isset($dataSuip->accionComercial)) {
      $this->accionComercialDto = new \stdClass();
      $this->accionComercialDto->id_tipo_accion_comercial_suip = $dataSuip->accionComercial->id;
      $this->accionComercialDto->nombre = $dataSuip->accionComercial->valor;
    }
  }

  private function construirSucursalesDto($dataSuip) {

    $sucursales_virtuales = array();
    $sucursales_fisicas = array();
    if (isset($dataSuip->sucursales) && count($dataSuip->sucursales) > 0) {
      foreach ($dataSuip->sucursales as $sucursal) {
        if (isset($sucursal->virtual) && $sucursal->virtual) {
          $sucursalVirtualDto = new \stdClass();
          $sucursalVirtualDto->id_sucursal_virtual_suip = $sucursal->id;
          $sucursalVirtualDto->web = $sucursal->websiteVenta;
          $sucursalVirtualDto->telefono = $sucursal->ventaTelefonica;
          $sucursales_virtuales[] = $sucursalVirtualDto;
        } else {
          $sucursalFisicaDto = new \stdClass();

          $sucursalFisicaDto->id_sucursal_fisica_suip = $sucursal->id;
          $sucursalFisicaDto->nombre = $sucursal->nombre;
          if (isset($sucursal->localizacion) && $sucursal->localizacion !== null) {
            $sucursalFisicaDto->latitud = $sucursal->localizacion->coordenadaY;
            $sucursalFisicaDto->longitud = $sucursal->localizacion->coordenadaX;
            $sucursalFisicaDto->calle = $sucursal->localizacion->calle;
            $sucursalFisicaDto->calle_numero = $sucursal->localizacion->numero;
            if (isset($sucursal->localizacion->localidad)
               && $sucursal->localizacion->localidad !== null) {
              $sucursalFisicaDto->id_localidad = $sucursal->localizacion->localidad->id;
              $sucursalFisicaDto->localidadDto = new \stdClass();
              $sucursalFisicaDto->localidadDto->id_localidad_suip = $sucursal->localizacion->localidad->id;
              $sucursalFisicaDto->localidadDto->nombre = $sucursal->localizacion->localidad->nombre;
              $sucursalFisicaDto->localidadDto->codigo_postal = null;
            }
            if (isset($sucursal->localizacion->provincia)
               && $sucursal->localizacion->provincia !== null) {
              $sucursalFisicaDto->id_provincia = $sucursal->localizacion->provincia->id;
              $sucursalFisicaDto->provinciaDto = new \stdClass();
              $sucursalFisicaDto->provinciaDto->id_provincia_suip = $sucursal->localizacion->provincia->id;
              $sucursalFisicaDto->provinciaDto->nombre = $sucursal->localizacion->provincia->nombre;
            }
          }
          $sucursales_fisicas[] = $sucursalFisicaDto;
        }
      }
      $this->sucursalesFisicasDto = $sucursales_fisicas;
      $this->sucursalesVirtualesDto = $sucursales_virtuales;
    }
  }

  private function construirDestacadoPublicado($dataSuip) {
    $this->publicado = 0;
    $this->destacado = 0;
    foreach ($dataSuip->canalesPertenenciaDestacados as $canal) {
      if ($canal->id === self::CANAL_PORTAL_FGO && $canal->selected) {
        $this->publicado = 1;
      } else if ($canal->id === self::CANAL_DESTACADO && $canal->selected) {
        $this->destacado = 1;
      }
    }
  }

  private function construirCampaniaDto($dataSuip) {
    $this->campaniasDto = array();

    foreach ($dataSuip->campaniaRelacionada as $campaniaSuip) {
      $campaniasDto = new \stdClass();
      $campaniasDto->id_campania_suip = $campaniaSuip->id;
      $campaniasDto->nombre = $campaniaSuip->nombre;
      $campaniasDto->fecha_desde = strtotime($campaniaSuip->fechaDesde);
      $campaniasDto->fecha_hasta = strtotime($campaniaSuip->fechaHasta);
      $this->campaniasDto[] = $campaniasDto;
    }

  }

  private function construirZonasAdheridas($dataSuip) {
    $this->zonas_adheridas = null;
    if (isset($dataSuip->localesAdheridos) && count($dataSuip->localesAdheridos) > 0) {
      $listadoZonas = array();
      foreach ($dataSuip->localesAdheridos as $zona) {
        if (isset($zona->localidad->nombre) && $zona->localidad->nombre) {
          $listadoZonas[] = $zona->localidad->nombre;
        } else {
          $listadoZonas[] = $zona->provincia->nombre;
        }
      }
      $this->zonas_adheridas = implode(',', $listadoZonas);
    }
  }

  private function calcularCftna($cftna){
    if ($cftna) {
      $cftExt=array();
      preg_match_all('/(cftna:|CFTNA:|costo financiero total:|costo financiero total efectivo anual:|COSTO FINANCIERO TOTAL:|COSTO FINANCIERO TOTAL EFECTIVO ANUAL:)\s*(\d{1,2}[\,\.]{1}\d{1,2})(%)/', $cftna, $cftExt);
      $cftExt2= $cftExt[2];
      $cftExt3= $cftExt2[0];
      $cft = str_replace(",", ".", $cftExt3);
      return (float)$cft;
    }else{
      return null;
    }
  }
}
