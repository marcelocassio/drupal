<?php

function fgo_importacion_db_resetear_orden() {
  db_query("UPDATE fgo_comunicacion SET peso = 0");
}

function fgo_importacion_db_obtener_descuento_y_cuotas() {
  $query = db_select("fgo_beneficio", "b");
  $query->leftJoin('fgo_comunicacion','fc', 'b.id_comunicacion = fc.id_comunicacion');
  $query->fields('b', array("id_comunicacion", "valor", "cuota"));
  $query->fields('fc', array("prioridad", "destacado"));
  $query->addField('b', 'id_casuistica', 'casuistica');
  $query = $query->orderBy('b.id_casuistica', 'DESC');
  $query = $query->orderBy('b.valor', 'DESC');
  $query = $query->orderBy('b.cuota', 'DESC');
  $result = $query->execute()->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);

  return $result;
}

function fgo_importacion_db_setear_orden($peso, $comunicaciones) {
  db_query("UPDATE fgo_comunicacion SET peso = :peso WHERE id_comunicacion IN (:comunicaciones)", array(":peso" => $peso, ":comunicaciones" => $comunicaciones));
}