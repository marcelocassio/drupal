<?php
define('FNET_SORTEO_URI', '/fnet/mod/fgo/NL-sorteo.do');
define('FNET_SORTEO_PARTICIPAR_METHOD', 'agregarParticipacion');
define('FNET_SORTEO_CONSULTAR_METHOD', 'consultarParticipacion');


define('FGO_MSG_PARTICIPACION_OK', 'Felicitaciones. Estas participando en el sorteo.');
define('FGO_MSG_NRO_SORTEO_ERRONEO', 'El número de sorteo es incorrecto.');
define('FGO_MSG_SORTEO_NO_SUSCRIPTO', "Para poder participar tenés que estar registrado.");

//PDF 1
define('FGO_MSG_YA_PARTICIPA', 'Ya estas participando en el sorteo.');
//PDF 2
define('FGO_MSG_NO_VIGENTE', 'El sorteo no esta vigente.');

define('BBVA_SORTEO_ESTADO_NOENVIADO', '0');
define('BBVA_SORTEO_ESTADO_ENVIADO', '1');
define('BBVA_SORTEO_ID_SIN_TARJETA', 'ST');
define('BBVA_SORTEO_ID_CON_TARJETA', 'TJ');
define('BBVA_SORTEO_ID_NO_CLIENTE', 'NC');
define('BBVA_SORTEO_ID_GENERAL', '%%');
define('BBVA_SORTEO_FILE_NAME_SORTEOS', 'codsorteo');
define('BBVA_SORTEO_FILE_NAME_PARTICIPANTES', 'particip');
define('BBVA_SORTEO_NO_EXISTE', 'El sorteo no existe.');
define('BBVA_SORTEO_NO_PARTICIPA', 'El número de documento no esta participando.');
define('BBVA_SORTEO_NO_LOGEADO', 'Debe estar logueado.');
define('BBVA_SORTEO_ERROR_TOTAL', 'En este momento no podemos ejecutar la operación.');


function create_sorteos_json($data){

  global $base_url;
  //Color Default
  $color_font = '#ffffff';
  $color_default = '#999999';

  $array_sorteos_por_finalizar = array();
  $array_sorteos_finalizados = array();

  foreach ($data as $value) {
    $wrapper = entity_metadata_wrapper('node', $value->nid);
    $experience_color = $wrapper->field_experience_color->value();
    $image_path_app_3 = $wrapper->field_experience_imagen_app->value();
    $image_path_web = $wrapper->field_experience_image->value();

    // HOTFIX CAMPOS REPETIDOS
    if ($wrapper->field_experience_volanta->value()) {
      $volanta = $wrapper->field_experience_volanta->value();
    }else{
      $volanta = " ";
    }
    if ($wrapper->field_experience_descripcion->value()) {
      $descripcion = $wrapper->field_experience_descripcion->value();
    }else{
      $descripcion = " ";
    }
    if ($wrapper->field_experience_bajada->value()) {
      $bajada = $wrapper->field_experience_bajada->value();
    }else{
      $bajada = " ";
    }

    // Se pasan las variables del mismo modo que se hace en json home sin el cid
    $experiences["content"][$value->nid]['id_sorteo'] = $value->nid;

    if (!empty($experience_color)) {
      $experiences["content"][$value->nid]["color"] = $color_default;
    } else{
      $experiences["content"][$value->nid]["color"] = $experience_color['rgb'];
    }
    $experiences["content"][$value->nid]["color_font"] = $color_font;
    $experiences["content"][$value->nid]["title_header"] = null;
    $experiences["content"][$value->nid]["title"] = null;
    $experiences["content"][$value->nid]["volanta"] = $volanta;
    if (!empty($image_path_app_3)) {
      $experiences["content"][$value->nid]["image"] = file_create_url($image_path_app_3["uri"]);
    }else{
      $experiences["content"][$value->nid]["image"] = null;
    }
    if (!empty($image_path_web)) {
      $experiences["content"][$value->nid]["image_web"] = file_create_url($image_path_web["uri"]);
    }else{
      $experiences["content"][$value->nid]["image_web"] = null;
    }
    $experiences["content"][$value->nid]["bajada"] = $bajada;
    $experiences["content"][$value->nid]["description"] = $descripcion;
    $experiences["content"][$value->nid]["type"] = null;

    $array_tags_temp = $wrapper->field_tags->value();
    $array_tags_total = array();
    if (is_array($array_tags_temp)) {
      foreach ($array_tags_temp as $tids) {
        if(isset($tids->tid)){
          $array_tags_total[] = $tids->tid;
        }
      }
    }
    $experiences["content"][$value->nid]["tags"] = $array_tags_total;

    /* ---------------------------------- */

    $experiences["content"][$value->nid]["content"][$value->nid]['title'] = $wrapper->title->value();
    $experiences["content"][$value->nid]["content"][$value->nid]['title_header'] = null;
    $experiences["content"][$value->nid]["content"][$value->nid]['id'] = $value->nid;
    // Un term reference
    $term = $wrapper->field_experience_category->value();
    if (!empty($term)) {
      $experiences["content"][$value->nid]["content"][$value->nid]['category_id']  = $term->tid;
      //$name = $term->name->value();
    }
    if (!empty($experience_color)) {
      $experiences["content"][$value->nid]["content"][$value->nid]['color'] = $color_default;
    } else{
      $experiences["content"][$value->nid]["content"][$value->nid]['color'] = $experience_color['rgb'];
    }
    $experiences["content"][$value->nid]["content"][$value->nid]['color_font']  = $color_font;
    if (!empty($image_path_app_3)) {
      $experiences["content"][$value->nid]["content"][$value->nid]['image'] = file_create_url($image_path_app_3["uri"]);
    }else{
      $experiences["content"][$value->nid]["content"][$value->nid]['image'] = null;
    }
    $experiences["content"][$value->nid]["content"][$value->nid]['volanta']  = $wrapper->field_experience_bajada->value();
    $experiences["content"][$value->nid]["content"][$value->nid]['volanta_description'] = $wrapper->field_experience_descripcion->value();
    $experiences["content"][$value->nid]["content"][$value->nid]['bajada'] = $wrapper->field_experience_fija_titulo3->value();
    $experiences["content"][$value->nid]["content"][$value->nid]['bajada_description'] = $wrapper->field_experience_fija_texto3->value();
    $experiences["content"][$value->nid]["content"][$value->nid]['type'] = 9;

    $boton = $wrapper->field_experience_fija_link->value();
    if(!empty($boton)){
      $experiences["content"][$value->nid]["content"][$value->nid]['boton_url'] = $boton['url'];
      $experiences["content"][$value->nid]["content"][$value->nid]['boton_title'] = $boton['title'];
    } else {
      $experiences["content"][$value->nid]["content"][$value->nid]['boton_url'] = '';
      $experiences["content"][$value->nid]["content"][$value->nid]['boton_title'] = '';
    }
    $experiences["content"][$value->nid]["content"][$value->nid]['es_sorteo'] = true;
    $experiences["content"][$value->nid]["content"][$value->nid]['usuario_participa'] = false;

    $experiences["content"][$value->nid]["content"][$value->nid]['participacion_directa_url'] = $base_url . '/API/sorteo/participar?nroNodoSorteo='.$value->nid;

    $fecha_vigencia = $wrapper->field_experience_fechas_vigencia->value();
    $fecha_enterate = $wrapper->field_experience_fecha_enterate->value();

    if ((date('d/m/Y',strtotime($fecha_vigencia['value2'])) == '31/12/1969') || (date('d/m/Y',strtotime($fecha_vigencia['value'])) == '31/12/1969')) {
        $experiences["content"][$value->nid]["content"][$value->nid]['participacion'] = null;
        $experiences["content"][$value->nid]["content"][$value->nid]['fechas_vigencia'] = null;
    }else{
        $experiences["content"][$value->nid]["content"][$value->nid]['participacion'] = "Del ". date('d-m-Y',strtotime($fecha_vigencia['value'])). " al ". date('d-m-Y',strtotime($fecha_vigencia['value2']));
        $experiences["content"][$value->nid]["content"][$value->nid]['fechas_vigencia'] = date('Y-m-d',strtotime($fecha_vigencia['value2']));

        // Si fecha vigencia es menor a la de hoy se cambia la descripción
        if (date('Y-m-d',strtotime($fecha_vigencia['value2'])) < date("Y-m-d")) {
          $experiences["content"][$value->nid]["description"] = "Enteráte de los ganadores el ".date('d/m/Y',$fecha_enterate);
        }
    }

    $experiences["content"][$value->nid]["content"][$value->nid]['mecanica_sorteo'] = $wrapper->field_experience_mecanica_sorteo->value();

    $fecha_sortea = $wrapper->field_experience_fecha_sortea->value();
    if (date('d/m/Y',$fecha_sortea) == '31/12/1969') {
        $experiences["content"][$value->nid]["content"][$value->nid]['fecha_sortea'] = null;
        $experiences["content"][$value->nid]["content"][$value->nid]['fecha_sortea_hora'] = null;
    }else{
        $experiences["content"][$value->nid]["content"][$value->nid]['fecha_sortea'] = date('d/m/Y',$fecha_sortea);
        $experiences["content"][$value->nid]["content"][$value->nid]['fecha_sortea_hora'] = date('H:i',$fecha_sortea);
    }

    if ($fecha_enterate['value'] != '') {
        $experiences["content"][$value->nid]["content"][$value->nid]['fecha_enterate'] = date('d/m/Y',$fecha_enterate);
    }else{
        $experiences["content"][$value->nid]["content"][$value->nid]['fecha_enterate'] = null;

    }

    $experiences["content"][$value->nid]["content"][$value->nid]['tyc'] = $wrapper->field_experience_tyc->value();

    $pdf_ganador = $wrapper->field_experience_ganador_sorteo->value();
    if (empty($pdf_ganador)){
        $experiences["content"][$value->nid]["content"][$value->nid]['ganador_sorteo'] = null;
    }else{
        $experiences["content"][$value->nid]["content"][$value->nid]['ganador_sorteo'] = $base_url . '/ganadores/sorteos/'.$value->nid;

        // Si existe el archivo cargado
        $experiences["content"][$value->nid]["description"] = "Conocé a los ganadores";
    }

    $experiences["content"][$value->nid]["content"][$value->nid]['share_link'] = $base_url . '/sorteo/' .$value->nid;
   //    $experiences["content"][$value->nid]["content"][$value->nid]['share_link'] = $base_url . '/' .drupal_get_path_alias("node/".$value->nid);
    $experiences["content"][$value->nid]["content"][$value->nid]['share_text'] =  "#AlertaGo ". $wrapper->field_experience_volanta->value();

    $fecha_contra_sortea_y_vigencia = strtotime(date("Y-m-d 00:00:00"));

    $experiences["content"][$value->nid]["content"] = array_values($experiences["content"][$value->nid]["content"]);

    // Si el sorteo ya finalizo se ordena DESC en base a la fecha sortea, y si esta por finalizar se ordena ASC (default query)
    if (strtotime($fecha_vigencia['value2']) >= $fecha_contra_sortea_y_vigencia) {
      $array_sorteos_por_finalizar[] = $experiences["content"][$value->nid];
    }else{
      if ( $fecha_contra_sortea_y_vigencia <= strtotime('+15 days',$fecha_sortea)){
        $array_sorteos_finalizados[] = $experiences["content"][$value->nid];
      }
    }

  } // END Foreach
  usort($array_sorteos_finalizados, "cmp");
  $pre_order_array = array_merge($array_sorteos_por_finalizar,$array_sorteos_finalizados);
  $experiences["content"] = array_values($pre_order_array);
  return $experiences['content'];
}

function cmp($a, $b){

  $cmp = null;

  if(isset($a["content"]) && isset($b["content"])){
    $formato = 'd/m/Y';
    $fecha_a = DateTime::createFromFormat($formato, $a["content"][0]["fecha_enterate"]);
    $fecha_b = DateTime::createFromFormat($formato, $b["content"][0]["fecha_enterate"]);
    
    if ($fecha_a === $fecha_b) {
       $cmp = 0;
    }
    elseif ($fecha_a >= $fecha_b){
      $cmp = -1;
    }
    else{
      $cmp = 1;
    }
  }
  
  return $cmp;

}
function bbva_sorteo_generar_linea_sorteo($identificador, $codigo_sorteo) {
  $linea = "";
  $linea .= str_pad(trim($identificador), 2, " ", STR_PAD_LEFT);
  $linea .= str_pad(trim($codigo_sorteo), 6, "0", STR_PAD_LEFT);
  $linea .= PHP_EOL;
  return $linea;
}

function read_csv_and_save_ganadores_sorteo($fid_csv_ganador = null, $node_id){

    if ($fid_csv_ganador) {
      $file = file_load($fid_csv_ganador);
      $uri = $file->uri;

      $handle = fopen($uri, 'r');

      //Then load each row and convert the format:
      $flag = true;
      while ($row = fgetcsv($handle)) {
        foreach ($row as $i => $field) {
          if($flag) { $flag = false; continue; }
          $data = explode(";", $field);

          if ($data[0] != '') {
            $nombre = utf8_encode(trim($data[0]));
          }else{
            $nombre = '';
          }
          if ($data[1] != '') {
            $apellido = utf8_encode(trim($data[1]));
          }else{
            $apellido = '';
          }
          if ($data[2] != '') {
            $tipo_documento = utf8_encode(trim($data[2]));
          }else{
            $tipo_documento = '';
          }
          if ($data[3] != '') {
            $numero_documento = utf8_encode(trim($data[3]));
          }else{
            $numero_documento = '';
          }
          if (isset($data[4]) && $data[4] != '') {
            $sexo = utf8_encode(trim($data[4]));
          }else{
            $sexo = '';
          }
            db_insert('bbva_sorteos_ganadores')
              ->fields(array(
                'node_id' => $node_id,
                'nombre' => $nombre,
                'apellido' => $apellido,
                'tipo_documento' => $tipo_documento,
                'numero_documento' => $numero_documento,
                'sexo' => $sexo,
              ))
              ->execute();
          }
      }
      //Close the file:
      fclose($handle);
    }
}

function bbva_sorteo_read_csv_ganadores_to_push($fid_csv_ganador = null){

    if ($fid_csv_ganador) {
      $file = file_load($fid_csv_ganador);
      $uri = $file->uri;

      $handle = fopen($uri, 'r');

      //Then load each row and convert the format:
      $ganadores = array();
      $flag = true;
      while ($row = fgetcsv($handle)) {
        foreach ($row as $i => $field) {
          if($flag) { $flag = false; continue; }
          $data = explode(";", $field);

          if ($data[0] != '') {
            $tipo_documento = utf8_encode(trim($data[0]));
          }else{
            $tipo_documento = '';
          }
          if ($data[1] != '') {
            $numero_documento = utf8_encode(trim($data[1]));
          }else{
            $numero_documento = '';
          }

           $ganadores[] = $tipo_documento.";".$numero_documento;
          }
      }
      //Close the file:
      fclose($handle);
      return $ganadores;
    }
    return null;
}

function bbva_sorteo_validar_datos(&$form, &$form_state) {
  // Si es sorteo valido las fechas
  if($form_state['values']['field_experience_es_sorteo']["und"][0]["value"] == 1) {
    //Que tenga "Subtitulo" cargado para que aparezca en la APP
    $volanta = $form_state['values']['field_experience_volanta']['und'][0]['value'];
    if (empty($volanta)){
      form_set_error('field_experience_volanta', 'Completar el "Subtitulo" para que se vea en la APP de FGo');
    }

    $fecha_vigencia = $form_state['values']['field_experience_fechas_vigencia']['und'][0]['value2'];

    // No se pueden crear mas de 2 sorteos con la misma fecha de finalizacion
    if (isset($form_state['build_info']['args']->nid)){
      $nid_temp = $form_state['build_info']['args']->nid;
    }else{
      $nid_temp = 0;
    }
    if(_bbva_sorteo_vigencias($fecha_vigencia." 00:00:00", $nid_temp) > 2){
      form_set_error('field_experience_fechas_vigencia', 'No puede existir un TERCER sorteo con la misma fecha de finalización');
    }

    $fecha_vigencia = substr($fecha_vigencia, 0, 10);
    $fecha_sortea = $form_state['values']['field_experience_fecha_sortea']['und'][0]['value'];
    // Verifico que tenga la hora puesta
    if(is_array($fecha_sortea) || strlen($fecha_sortea) != 19) {
      form_set_error('field_experience_fecha_sortea', 'La fecha u hora de sortea es incorrecta.' . $fecha_sortea);
    }
    $fecha_sortea = substr($fecha_sortea, 0, 10);
    $fecha_enterate = $form_state['values']['field_experience_fecha_enterate']['und'][0]['value'];
    $fecha_enterate = substr($fecha_enterate, 0, 10);

    // Paso las fechas a timestamp
    $fecha_vigencia_timestamp = DateTime::createFromFormat('Y-m-d His', $fecha_vigencia . " 000000")->getTimestamp();
    $fecha_sortea_timestamp = DateTime::createFromFormat('Y-m-d His', $fecha_sortea . " 000000")->getTimestamp();
    $fecha_enterate_timestamp = DateTime::createFromFormat('Y-m-d His', $fecha_enterate . " 000000")->getTimestamp();

    if($fecha_vigencia_timestamp >= $fecha_sortea_timestamp) {
      form_set_error('field_experience_fecha_sortea', 'La fecha de sortea debe ser mayor a la fecha de fin de vigencia.');
    }
    if($fecha_sortea_timestamp >= $fecha_enterate_timestamp) {
      form_set_error('field_experience_fecha_enterate', 'La fecha de enterate debe ser mayor a la fecha de sortea.');
    }
  }
}

function bbva_sorteo_push_ganadores($node,$ganadores_push){
    global $base_url;
    $fecha_envio = strtotime('+1 day'); // Se envia el push un dia después de haberse cargado el excel

    $texto_corto = '¡Felicitaciones! Ganaste en el sorteo de '.$node->title;

    $texto_ext = 'Chequeá tu casilla de e-mail para conocer cómo acceder a tu premio.';

    $entity_notificacion = entity_create('bbva_message_plan', array('type' => 'bbva_message_plan', 'status' => 1, 'comment' => 1, 'promote' => 0));
    $entity_notificacion_w = entity_metadata_wrapper('bbva_message_plan', $entity_notificacion);

    // FC
    $field_collection_item = entity_create('field_collection_item', array('field_name' => 'field_call_to_action'));
    $field_collection_item->setHostEntity('bbva_message_plan', $entity_notificacion);
    $fc_wrapper = entity_metadata_wrapper('field_collection_item', $field_collection_item);
    $fc_wrapper->field_tipo->set(7);
    $fc_wrapper->field_cta_link_visible->set(1);
    $fc_wrapper->field_call_to_acction_link->set($base_url."/ganadores/sorteos/".$node->nid);
    $fc_wrapper->save(true);

    $entity_notificacion_w->field_message_plan_push->set($texto_corto);
    $entity_notificacion_w->field_message_plan_long_text->set($texto_ext);
    $entity_notificacion_w->field_message_plan_date_send->set($fecha_envio);

    $entity_notificacion_w->field_message_plan_state->set(0);

    $file = _crear_archivo_push_listado_sorteos($ganadores_push, "sorteo_ganadores_" . $node->nid . ".txt");
    $file->display = 1;
    $entity_notificacion_w->field_message_plan_list_file->set((array) $file);
    $entity_notificacion_w->save();

  if (module_exists('fgo_core') && variable_get('fgo_activar_cta_push_sorteo_ganadores') == 1) {
    //Se crea CTA
    $cta = new \Fgo\Bo\CtaBo();
    $cta->param1 = 4;
    $cta->param2 = $base_url."/ganadores/sorteos/".$node->nid;
    $cta->guardar();

    // Se guarda relacion CTA
    $ctaRelacion = new \Fgo\Bo\CtaRelacionBo();
    $ctaRelacion->cta = $cta;
    $ctaRelacion->idEntidad = $entity_notificacion_w->getIdentifier();
    $ctaRelacion->tipoEntidad = TIPO_RELACION_CTA_PUSH;

    $ctaRelacion->guardar();
  }

  return true;
}

//Individualmente
function bbva_sorteo_cache_participantes($sorteo_id, $user_uid){
  //Obtenemos el cache de ese sorteo que contiene los participantes
  $cachesorteo_temp = cache_get('jsonSorteos_'.$sorteo_id,'cache_api');
  // Si el caché existe, sólo se agrega el participante
  if (isset($cachesorteo_temp->data)) {
    $cachesorteo = $cachesorteo_temp->data;
    $cachesorteo[$user_uid] = null;
    cache_set('jsonSorteos_'.$sorteo_id, $cachesorteo,'cache_api',CACHE_PERMANENT);
  }else{
    // Si el caché nunca existió, se crea y se agrega el participante
    $data = array();
    $data[$user_uid] = null;
    cache_set('jsonSorteos_'.$sorteo_id, $data,'cache_api',CACHE_PERMANENT);
  }
}

//Grupal
function bbva_sorteo_cache_sorteos_participantes(){
  //Sorteos - Participantes
  $jsonSorteos_temp = cache_get('jsonSorteosLista','cache_api');
  if ($jsonSorteos_temp) {
    $jsonSorteos = $jsonSorteos_temp->data;
    foreach ($jsonSorteos as $value) {
      $array_participantes = array();
      $array_participantes_temp = _consultar_participantes($value["id"]);
      foreach ($array_participantes_temp as $participante_id) {
        $array_participantes[$participante_id->uid] = null;
      }
      cache_set('jsonSorteos_'.$value["id"],$array_participantes,'cache_api',CACHE_PERMANENT);
    }
  }
}

function create_sorteos_json_lista($data){

  global $base_url;

  $array_sorteos_por_finalizar = array();
  $array_sorteos_finalizados = array();
  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
  foreach ($data as $value) {
    $wrapper = entity_metadata_wrapper('node', $value->nid);

    // HOTFIX CAMPOS REPETIDOS
    if ($wrapper->field_experience_volanta->value()) { // Subtitulo de la experiencia
      $volanta = $wrapper->field_experience_volanta->value();
    }else{
      $volanta = null;
    }

    if ($wrapper->field_experience_bajada->value()) { // Subtitulo de la experiencia
      $bajada = $wrapper->field_experience_bajada->value();
    }else{
      $bajada = null;
    }

    if ($wrapper->field_experience_descripcion->value()) {
      $descripcion = $wrapper->field_experience_descripcion->value();
    } else {
      $descripcion = null;
    }

    // Se pasan las variables del mismo modo que se hace en json home sin el cid
    $experiences["content"][$value->nid]['id'] = (int)$value->nid;
    $experiences["content"][$value->nid]['user_participation'] = false;

    $experiences["content"][$value->nid]["title"] = $volanta;
    $experiences["content"][$value->nid]["subtitle"] = $bajada;

    $image_path_web = $wrapper->field_experience_image->value();
    if (!empty($image_path_web)) {
      $experiences["content"][$value->nid]["image_web"] = file_create_url($image_path_web["uri"]);
    }else{
      $experiences["content"][$value->nid]["image_web"] = null;
    }
    
    $image_path_app = $wrapper->field_experience_imagen_app->value();
    if (!empty($image_path_app)) {
      $experiences["content"][$value->nid]["image_app"] = file_create_url($image_path_app["uri"]);
    }else{
      $experiences["content"][$value->nid]["image_app"] = null;
    }
    $experiences["content"][$value->nid]["summary"] = $descripcion;

    $array_tags_temp = $wrapper->field_tags->value();
    $array_tags_total = array();
    if (is_array($array_tags_temp)) {
      foreach ($array_tags_temp as $tids) {
        if(isset($tids->tid)){
          $array_tags_total[] = $tids->tid;
        }
      }
    }
    $experiences["content"][$value->nid]["tags_search"] = $array_tags_total;
    $experiences["content"][$value->nid]["noclient"] = $wrapper->field_experience_seas_cliente->value();

    $fecha_vigencia = $wrapper->field_experience_fechas_vigencia->value();
    $timestamp_vigencia = strtotime($fecha_vigencia["value2"]);
    $fecha_enterate = $wrapper->field_experience_fecha_enterate->value();

    $experiences["content"][$value->nid]['legend_code'] = (int)99;

    if ((date('d/m/Y',strtotime($fecha_vigencia['value2'])) == '31/12/1969') || (date('d/m/Y',strtotime($fecha_vigencia['value'])) == '31/12/1969')) {
        $experiences["content"][$value->nid]['legend'] = null;
    }else{
        $experiences["content"][$value->nid]['user_participation'] = true;
        $experiences["content"][$value->nid]["legend"] = "Participá hasta el ".date('d',$timestamp_vigencia)." de ".$meses[date('n',$timestamp_vigencia)-1];
        $experiences["content"][$value->nid]['legend_code'] = (int)2;
        // Si fecha vigencia es menor a la de hoy se cambia la descripción
        if (date('Y-m-d',strtotime($fecha_vigencia['value2'])) < date("Y-m-d")) {
          $experiences["content"][$value->nid]['legend_code'] = (int)3;
          $experiences["content"][$value->nid]['user_participation'] = false;
          $experiences["content"][$value->nid]["legend"] = "Enteráte de los ganadores el ".date('d-m-Y',$fecha_enterate);
        }
    }

    /* SORTEOS WEB VIEW  * */
    $sorteo_web_view = $wrapper->field_experience_web_view->value();
    if ($sorteo_web_view == 1 && $experiences["content"][$value->nid]['legend_code'] != 3){
      $experiences["content"][$value->nid]['legend_code'] = (int)4;
    }

    $pdf_ganador = $wrapper->field_experience_ganador_sorteo->value();
    if (!empty($pdf_ganador)){
      // Si existe el archivo cargado
      $experiences["content"][$value->nid]['user_participation'] = false;
      $experiences["content"][$value->nid]["legend"] = "Ver ganadores";
    }

    $experiences["content"][$value->nid]['share_url'] = $base_url . '/sorteo/' .$value->nid;
    $experiences["content"][$value->nid]['share_text'] =  "#AlertaGo ". $volanta;

    $fecha_contra_sortea_y_vigencia = strtotime(date("Y-m-d 00:00:00"));
    $fecha_sortea = $wrapper->field_experience_fecha_sortea->value();
    // Si el sorteo ya finalizo se ordena DESC en base a la fecha sortea, y si esta por finalizar se ordena ASC (default query)
    if (strtotime($fecha_vigencia['value2']) >= $fecha_contra_sortea_y_vigencia) {
      $array_sorteos_por_finalizar[] = $experiences["content"][$value->nid];
    }else{
      if ( $fecha_contra_sortea_y_vigencia <= strtotime('+15 days',$fecha_sortea)){
        $array_sorteos_finalizados[] = $experiences["content"][$value->nid];
      }
    }

  } // END Foreach
  usort($array_sorteos_finalizados, "cmp");
  $pre_order_array = array_merge($array_sorteos_por_finalizar,$array_sorteos_finalizados);
  $experiences["content"] = array_values($pre_order_array);
  return $experiences['content'];
}

function create_sorteos_json_detalles($data){

  global $base_url;

  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

  foreach ($data as $value) {
    $wrapper = entity_metadata_wrapper('node', $value->nid);
    $image_path_app = $wrapper->field_experience_imagen_app->value();
    $image_path_web = $wrapper->field_experience_image->value();

    if ($wrapper->field_experience_volanta->value()) { // Subtitulo de la experiencia
      $volanta = $wrapper->field_experience_volanta->value();
    }else{
      $volanta = null;
    }
    if ($wrapper->field_experience_descripcion->value()) {
      $descripcion = $wrapper->field_experience_descripcion->value();
    }else{
      $descripcion = null;
    }

    $listadoSorteos[$value->nid]['id'] = (int)$value->nid;

    $listadoSorteos[$value->nid]['title'] = $wrapper->title->value();
    $listadoSorteos[$value->nid]['subtitle_web'] = $volanta;

    if (!empty($image_path_app)) {
      $listadoSorteos[$value->nid]["image_app"] = file_create_url($image_path_app["uri"]);
    }else{
      $listadoSorteos[$value->nid]["image_app"] = null;
    }

    if (!empty($image_path_web)) {
      $listadoSorteos[$value->nid]["image_web"] = file_create_url($image_path_web["uri"]);
    }else{
      $listadoSorteos[$value->nid]["image_web"] = null;
    }

    $listadoSorteos[$value->nid]["summary"] = $descripcion;
    $listadoSorteos[$value->nid]['legend_code'] = (int)99;
    $listadoSorteos[$value->nid]["legend"] = null;
    $listadoSorteos[$value->nid]['mechanic_raffle'] = $wrapper->field_experience_mecanica_sorteo->value();

    $fecha_vigencia = $wrapper->field_experience_fechas_vigencia->value();
    $timestamp_vigencia = strtotime($fecha_vigencia["value2"]);
    $fecha_enterate = $wrapper->field_experience_fecha_enterate->value();

    if ((date('d/m/Y',strtotime($fecha_vigencia['value2'])) == '31/12/1969') || (date('d/m/Y',strtotime($fecha_vigencia['value'])) == '31/12/1969')) {
        $listadoSorteos[$value->nid]['date_expiration_raffle_web'] = null;
        $listadoSorteos[$value->nid]['date_expiration_raffle_app'] = null;
    }else{
        $listadoSorteos[$value->nid]['legend_code'] = (int)2;
        $listadoSorteos[$value->nid]['date_expiration_raffle_web'] = "Del ". date('d-m-Y',strtotime($fecha_vigencia['value'])). " al ". date('d-m-Y',strtotime($fecha_vigencia['value2']));
        $listadoSorteos[$value->nid]['date_expiration_raffle_app'] = date('d',$timestamp_vigencia)." de ".$meses[date('n',$timestamp_vigencia)-1];

        // Si fecha vigencia es menor a la de hoy se cambia la descripción
        if (date('Y-m-d',strtotime($fecha_vigencia['value2'])) < date("Y-m-d")) {
          $listadoSorteos[$value->nid]['legend_code'] = (int)3;
          $listadoSorteos[$value->nid]["legend"] = "Enteráte de los ganadores el ".date('d-m-Y',$fecha_enterate);
        }
    }

    $fecha_sortea = $wrapper->field_experience_fecha_sortea->value();
    if (date('d/m/Y',$fecha_sortea) == '31/12/1969') {
        $listadoSorteos[$value->nid]['date_raffle'] = null;
    }else{
        $listadoSorteos[$value->nid]['date_raffle'] = date('d',$fecha_sortea)." de ".$meses[date('n',$fecha_sortea)-1]." ".date('H:i',$fecha_sortea)." hs";
    }

    $listadoSorteos[$value->nid]['tac'] = $wrapper->field_experience_tyc->value();
    $listadoSorteos[$value->nid]['raffle_winners_url'] = null;

    /* SORTEOS WEB VIEW  * */
    $sorteo_web_view = $wrapper->field_experience_web_view->value();
    $sorteo_web_view_datos = $wrapper->field_experience_fija_link->value();

    if ($sorteo_web_view == 1 && $listadoSorteos[$value->nid]['legend_code'] != 3){
      $listadoSorteos[$value->nid]['legend_code'] = (int)4;
      $listadoSorteos[$value->nid]["legend"] = $sorteo_web_view_datos["title"];
      $listadoSorteos[$value->nid]['raffle_winners_url'] = $sorteo_web_view_datos["url"];
    }

    $pdf_ganador = $wrapper->field_experience_ganador_sorteo->value();

    if (!empty($pdf_ganador)){ // Si existe el archivo cargado
      $listadoSorteos[$value->nid]['raffle_winners_url'] = $base_url . '/ganadores/sorteos/'.$value->nid;
      $listadoSorteos[$value->nid]['legend_code'] = (int)4;
      $listadoSorteos[$value->nid]["legend"] = "Ver ganadores";
    }

    $listadoSorteos[$value->nid]['share_url'] = $base_url . '/sorteo/' .$value->nid;
    $listadoSorteos[$value->nid]['share_text'] =  "#AlertaGo ". $volanta;

  } // END Foreach

  return $listadoSorteos;
}

function bbva_sorteos_vigentes_tags(){
  $resultSorteosVigentes = array();
  $resultSorteosAll = _tags_sorteos_vigentes_search();
  $fecha_contra_sortea_y_vigencia = REQUEST_TIME;
  foreach ($resultSorteosAll as $value) {
    $fecha_vigencia = strtotime($value->fecha_final_sorteo);
    if(!empty($value->fecha_sortea_sorteo) && is_numeric($value->fecha_sortea_sorteo)){
     $fecha_sortea = strtotime('+15 days', $value->fecha_sortea_sorteo);
    }
    else{
      $fecha_sortea = 0;
    }

    if ($fecha_vigencia >= $fecha_contra_sortea_y_vigencia) {
      $resultSorteosVigentes[] = $value;
    }else{
      if ( $fecha_contra_sortea_y_vigencia <= $fecha_sortea && $fecha_sortea != 0){
        $resultSorteosVigentes[] = $value;
      }
    }
  }
  return $resultSorteosVigentes;
}

function bbva_sorteos_vigentes(){
  $resultSorteosVigentes = array();
  $resultSorteosAll = _bbva_sorteo_listado();
  $fecha_contra_sortea_y_vigencia = strtotime(date("Y-m-d 00:00:00"));
  foreach ($resultSorteosAll as $value) {
    $fecha_vigencia = $value->fecha_vigencia_hasta;

    if (strtotime($fecha_vigencia) >= $fecha_contra_sortea_y_vigencia) {
      $resultSorteosVigentes[] = $value;
    }
  }
  return $resultSorteosVigentes;
}