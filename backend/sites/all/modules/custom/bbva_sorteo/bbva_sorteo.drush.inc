<?php

function bbva_sorteo_drush_command() {

  $items['export-participaciones'] = array(
    'description' => 'Exporta archivo con las relaciones usuario y las participaciones.',
    'aliases' => array('epus'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['cerrar-sorteos'] = array(
    'description' => 'Exporta archivos de sorteos para calculo de ganadores',
    'aliases' => array('cerrar-sorteos'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  return $items;
}


function drush_bbva_sorteo_cerrar_sorteos($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(
        array('bbva_sorteo_batch_cerrar_sorteo_finalizados',array($odate, $job))
    ),
    'title' => t('Batch Export Cerrar Sorteos'),
    'init_message' => t('Batch Export Cerrar Sorteos en sorteo esta empezando...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_sorteo_batch_finished',
    'file' => drupal_get_path('module', 'bbva_sorteo') . '/bbva_sorteo.batch.inc',
  );

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

function drush_bbva_sorteo_export_participaciones($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(
        array('bbva_sorteo_batch_participaciones',array($odate, $job))
    ),
    'title' => t('Batch Export Participaciones'),
    'init_message' => t('Batch Export Participaciones en sorteo esta empezando...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_sorteo_batch_finished',
    'file' => drupal_get_path('module', 'bbva_sorteo') . '/bbva_sorteo.batch.inc',
  );

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}


function bbva_sorteo_batch_finished($success, $results, $operations){
  if (!$results["failed"] && $success){
    drupal_set_message(t('Proceso finalizo correctamente.'));
  }else{
    reset($operations);
    drush_log(dt('Error en batch "Cerrar sorteos" o "Export participantes"'), 'error');
  }
}

