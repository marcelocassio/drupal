<?php

define('BBVA_SORTEO_PARTICIPACIONES_FILE_NAME', '/prod/jp00/vuelcoDrupal/participaciones_sorteos');


function bbva_sorteo_batch_cerrar_sorteo_finalizados($odate, $job, &$context) {
  echo "== Comienzo Export Cierre Sorteos ==" . PHP_EOL;
  echo "ODATE = " . $odate . PHP_EOL;
  $nro_archivo = 0;
  bbva_sorteo_batch_crear_archivos_vacios($odate);
  if (empty($odate))  {
    drush_log(dt('Falta la fecha ODATE(YYYYMMDD)'), 'error');
    $context['results']['failed'] = true;
  }else{
    try {
      $sorteos = _buscar_sorteos_finalizados_pendientes($odate);
      foreach ($sorteos as $node) {
        $nro_archivo++;
        $id_nodo_sorteo = $node->nid;
        if ($nro_archivo > 2) {
          echo "*ATENCION: Existen mas sorteos para cerrar. Siguiente nodo sorteo :".$id_nodo_sorteo.PHP_EOL;
          break;
        }
        echo "*Nro Nodo Sorteo Encontrado: ".$id_nodo_sorteo.PHP_EOL;
        $sorteo_wrapper = entity_metadata_wrapper('node', $id_nodo_sorteo);
        $participantes = _consultar_participantes($id_nodo_sorteo);

        bbva_sorteo_generar_archivo_codigos($sorteo_wrapper, $nro_archivo, $odate);
        bbva_sorteo_generar_archivo_participantes($participantes, $nro_archivo, $odate);

        echo "*Archivos generados para NroNodo: ".$id_nodo_sorteo.PHP_EOL;
        _establecer_estado_sorteo_enviado($id_nodo_sorteo);

      }

      echo "*Cantidad de archivos generados: ".$nro_archivo.PHP_EOL;
      echo "==Fin Export Cierre Sorteos==".PHP_EOL;
    } catch (Exception $e) {
      echo "*Excepcion Generada: " . $e->getMessage().PHP_EOL;
      echo "*Finaliza con errores".PHP_EOL;
      $context['results']['failed'] = true;
      exit(1);
    }
  }
}
function bbva_sorteo_batch_crear_archivos_vacios($odate) {
  $nro_archivo = 1;
  $filePathName=BBVA_VUELCO_DRUPAL_PATH.BBVA_SORTEO_FILE_NAME_SORTEOS.$nro_archivo."-".$odate.".txt";
  $fp = fopen($filePathName,"wb");
  fclose($fp);

  $filePathName=BBVA_VUELCO_DRUPAL_PATH.BBVA_SORTEO_FILE_NAME_PARTICIPANTES.$nro_archivo."-".$odate.".txt";
  $fp = fopen($filePathName,"wb");
  fclose($fp);

  $nro_archivo = 2;
  $filePathName=BBVA_VUELCO_DRUPAL_PATH.BBVA_SORTEO_FILE_NAME_SORTEOS.$nro_archivo."-".$odate.".txt";
  $fp = fopen($filePathName,"wb");
  fclose($fp);

  $filePathName=BBVA_VUELCO_DRUPAL_PATH.BBVA_SORTEO_FILE_NAME_PARTICIPANTES.$nro_archivo."-".$odate.".txt";
  $fp = fopen($filePathName,"wb");
  fclose($fp);
}

function bbva_sorteo_generar_archivo_codigos($sorteo_wrapper, $nro_archivo, $odate) {

  $nro_sorteo_gral = $sorteo_wrapper->field_experience_code_sorteo->value();
  $nro_sorteo_sin_tarjeta = $sorteo_wrapper->field_experience_code_sorteo_nc->value();
  $nro_sorteo_con_tarjeta = $sorteo_wrapper->field_experience_code_sorteo_tc->value();
  $nro_sorteo_no_cliente = $sorteo_wrapper->field_experience_code_sorteo_st->value();

  $es_codigo_sorteo_unico = $sorteo_wrapper->field_experience_check_code_uniq->value();
  $contenido= '';
  if (!empty($es_codigo_sorteo_unico)) {
    $contenido=bbva_sorteo_generar_linea_sorteo(BBVA_SORTEO_ID_GENERAL, $nro_sorteo_gral);
  } else {
    $contenido=bbva_sorteo_generar_linea_sorteo(BBVA_SORTEO_ID_SIN_TARJETA, $nro_sorteo_sin_tarjeta);
    $contenido.=bbva_sorteo_generar_linea_sorteo(BBVA_SORTEO_ID_CON_TARJETA, $nro_sorteo_con_tarjeta);
    $contenido.=bbva_sorteo_generar_linea_sorteo(BBVA_SORTEO_ID_NO_CLIENTE, $nro_sorteo_no_cliente);
  }

  $filePathName=BBVA_VUELCO_DRUPAL_PATH.BBVA_SORTEO_FILE_NAME_SORTEOS.$nro_archivo."-".$odate.".txt";
  $fp = fopen($filePathName,"wb");
  fwrite($fp, $contenido);
  fclose($fp);

}

function bbva_sorteo_generar_archivo_participantes($participantes, $nro_archivo, $odate) {
  $header = "nombre,apellido,sexo,tipoDoc,numDoc,fechaNacimiento,email,telefono,celular";

  $filePathName=BBVA_VUELCO_DRUPAL_PATH.BBVA_SORTEO_FILE_NAME_PARTICIPANTES.$nro_archivo."-".$odate.".txt";
  $fp = fopen($filePathName,"wb");
  fwrite($fp, $header.PHP_EOL);
  $delimitador = ",";
  foreach ($participantes as $participante) {

    $tipo_doc = $participante->field_profile_document_type_value;
    $num_doc = $participante->field_profile_document_number_value;

    $nombre = $participante->field_profile_nombre_value;
    $apellido = $participante->field_profile_apellido_value;
    $sexo = $participante->field_profile_sex_value;
    $tipo_doc_altamira = bbva_subscription_convert_tipo_doc_altamira($tipo_doc, $num_doc, $sexo);
    $numero_documento = $participante->field_profile_document_number_value;
    $fecha_nacimiento = "";
    $email = $participante->field_profile_email_value;
    $telefono = "";
    $celular = $participante->field_profile_area_value . $participante->field_profile_celular_value;

    $linea="";
    $linea.=bbva_comillas(str_pad(substr(sanear_string($nombre),0,20), 20, " ", STR_PAD_RIGHT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad(substr(sanear_string($apellido),0,55), 55, " ", STR_PAD_RIGHT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad(substr($sexo,0,1), 1, " ", STR_PAD_RIGHT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad(substr($tipo_doc_altamira,0,2), 2, "0", STR_PAD_RIGHT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad(substr($numero_documento,0,13), 13, "0", STR_PAD_LEFT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad(substr($fecha_nacimiento,0,8), 8, " ", STR_PAD_RIGHT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad(substr($email,0,80), 80, " ", STR_PAD_RIGHT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad($telefono, 10, " ", STR_PAD_RIGHT));
    $linea.=$delimitador;
    $linea.=bbva_comillas(str_pad(substr($celular,0,10), 10, " ", STR_PAD_RIGHT));

    fwrite($fp, $linea.PHP_EOL);
  }

  fclose($fp);
}

function bbva_sorteo_batch_participaciones($odate, $job, &$context) {
  echo "==Comienzo Export Participaciones==" . PHP_EOL;
  echo "ODATE=" . $odate . PHP_EOL;

  $separador = ';';
  $select = null;
  if ($odate == 'ALL') {
    $select = _query_all_statistics_user_sorteo();
  } else {
    $select = _query_all_statistics_user_sorteo($odate);
  }

  $fp = fopen(BBVA_SORTEO_PARTICIPACIONES_FILE_NAME."_D".$odate . ".txt","wb");

  if (isset($fp)) {
    foreach ($select as $row => $record) {
      $content = '';
      $content .= $record->id;
      $content .= $separador;
      $content .= $record->uid;
      $content .= $separador;
      $content .= $record->name;
      $content .= $separador;
      $content .= $record->sorteo_id;
      $content .= $separador;
      $content .= $record->datetime_participate;
      fwrite($fp, $content.PHP_EOL);
    }
    fclose($fp);
  }

  echo "==Fin Export Participaciones==".PHP_EOL;
}
