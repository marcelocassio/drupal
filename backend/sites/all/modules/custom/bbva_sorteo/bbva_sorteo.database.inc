<?php

// Esta funcion es para los sorteos viejos
// Por ende se le pasa detalle para que se vean todos los sorteos no importando la marca "Seas o no Cliente"
function _get_sorteos_all(){
  $result = _bbva_sorteos_sorteos_disponibles("detalle");
  if (isset($result['node'])) {
    $sorteos = create_sorteos_json($result['node']);
    return $sorteos;
  }else{
    return array();
  }
}

//$type puede ser "detalle" o "lista"
function _get_sorteos_v2($type){
  $sorteos = array();
  $result = _bbva_sorteos_sorteos_disponibles($type);

  if (isset($result['node'])) {
    if ($type == "detalle"){
      $sorteos = create_sorteos_json_detalles($result['node']);
    }elseif($type == "lista"){
      $sorteos = create_sorteos_json_lista($result['node']);
    }
  }

  return $sorteos;
}

function _bbva_sorteos_sorteos_disponibles($type){
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'experience')
    ->fieldCondition('field_experience_fechas_vigencia', 'value', date('Y-m-d'), '<=')
    ->fieldCondition('field_experience_es_sorteo', 'value', '1', '=');
    $query->fieldOrderBy('field_experience_fechas_vigencia', 'value', 'ASC');

  return $query->execute();
}

function _bbva_sorteo_listado() {
  $query2 = db_select('node', 'n');
  $query2->innerJoin('field_data_field_experience_es_sorteo', 'es', 'es.entity_id = n.nid');
  $query2->innerJoin('field_data_field_experience_seas_cliente', 'sc', 'sc.entity_id = n.nid');
  $query2->innerJoin('field_data_field_experience_fechas_vigencia', 'fv', 'fv.entity_id = n.nid');
  $query2->addField('n', 'nid', 'id');
  $query2->addField('n', 'title', 'titulo');
  $query2->addField('fv', 'field_experience_fechas_vigencia_value', 'fecha_vigencia_desde');
  $query2->addField('fv', 'field_experience_fechas_vigencia_value2', 'fecha_vigencia_hasta');

  $query2->condition('es.field_experience_es_sorteo_value', 1)
      ->condition('n.type', 'experience');

  $resultSorteos = $query2->execute()->fetchAll();
  return $resultSorteos;
}

function _get_sorteos_by_node_id($node_id){
  $query = db_select('bbva_sorteos_ganadores', 'gs');

  $query->condition('node_id', $node_id)
        ->fields('gs', array('nombre', 'apellido', 'tipo_documento', 'numero_documento'));

  $results = $query->execute()->fetchAll();
  if ($results) {
    return $results;
  }else{
    return null;
  }
}

function _save_participation_user_sorteo($user_id, $nodo_id) {

  $date = date_create();
  $date = date_timestamp_get($date);

  $query = db_insert('bbva_sorteos_participantes')
            ->fields(array(
              'uid' => $user_id,
              'nodo_id' => $nodo_id,
              'datetime_participate' => $date));

  $result = $query->execute();

  return $result;
}

function _bbva_sorteos_consultar_participacion_user_sorteos($user_id) {
  $query = db_select('bbva_sorteos_participantes', 'bsp');
  $query->leftJoin('node','n','n.nid=bsp.nodo_id');
  $query->leftJoin('field_data_field_experience_fechas_vigencia','fv','fv.entity_id=bsp.nodo_id');
  $query = $query->fields('bsp', array('id', 'datetime_participate'));
  $query = $query->fields('n', array('title'));
  $query = $query->fields('fv', array('field_experience_fechas_vigencia_value', 'field_experience_fechas_vigencia_value2'));
  $query = $query->condition('bsp.uid', $user_id, '=');

  $result = $query->execute()->fetchAll();

  return $result;
}

function _query_all_statistics_user_sorteo($date = null) {

  $query = db_select('bbva_sorteos_participantes', 'bus')
           ->fields('bus', array('id','uid','nodo_id', 'datetime_participate'));

  if (!empty($date)) {
    $query->where("FROM_UNIXTIME(bus.datetime_participate, '%Y%m%d') = :date",
            array(':date' => $date));
  }

  $result = $query->execute()->fetchAll();

  return $result;
}

function _delete_sorteo_ganadores($node_id){

  if ($node_id != '') {
    $query = db_select('bbva_sorteos_ganadores', 'gs');
    $query->addField('gs', 'node_id');
    $query->condition('gs.node_id', $node_id,'=');

    $result = $query->execute()->fetchAll();

    if ($result) {
      $deleted = db_delete('bbva_sorteos_ganadores')
        ->condition('node_id', $node_id)
        ->execute();
    }
  }
}

function _buscar_sorteos_finalizados_pendientes($odate){
  $query = new EntityFieldQuery();

  $fecha_odate = new DateTime($odate);

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'experience')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_experience_fechas_vigencia', 'value2', $fecha_odate->format('Y-m-d 00:00:00'), '<=')
    ->fieldCondition('field_experience_es_sorteo', 'value', '1', '=')
    ->fieldCondition('field_experience_estado_sorteo', 'value', BBVA_SORTEO_ESTADO_NOENVIADO, '=');


  $result = $query->execute();

  if ($result['node']) {
    return $result['node'];
  }else{
    return array();
  }
}

function _consultar_participantes($id_nodo_sorteo) {
  // nombre,apellido,sexo,tipoDoc,numDoc,fechaNacimiento,email,telefono,celular
  $query = db_select('bbva_sorteos_participantes', 'bus');
  $query->leftJoin('users','u','u.uid=bus.uid');
  $query->leftJoin('field_data_field_profile_document_type','dt', 'dt.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_document_number','dn', 'dn.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_sex','s','s.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_email','e','e.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_nombre','n','n.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_apellido','a','a.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_area','ar','ar.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_celular','ce','ce.entity_id = bus.uid');

  $query->fields('bus', array('id','uid'))
        ->fields('dt', array('field_profile_document_type_value'))
        ->fields('dn', array('field_profile_document_number_value'))
        ->fields('s', array('field_profile_sex_value'))
        ->fields('e', array('field_profile_email_value'))
        ->fields('n', array('field_profile_nombre_value'))
        ->fields('a', array('field_profile_apellido_value'))
        ->fields('ar', array('field_profile_area_value'))
        ->fields('ce', array('field_profile_celular_value'))
        ->condition('bus.nodo_id', $id_nodo_sorteo, '=')
        ->condition('dn.field_profile_document_number_value', "", '!=')
        ->condition('dn.field_profile_document_number_value', "0", '!=');

  $result = $query->execute()->fetchAll();

  return $result;
}

function _establecer_estado_sorteo_enviado($id_nodo_sorteo) {
  $sorteo_node = node_load($id_nodo_sorteo);
  $sorteo_wrapper = entity_metadata_wrapper('node', $sorteo_node);
  $sorteo_wrapper->field_experience_estado_sorteo->set(BBVA_SORTEO_ESTADO_ENVIADO);
  $sorteo_wrapper->save();
}

function _bbva_sorteo_existe_sorteo($nid){
  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.nid', $nid);

  $result = $query->execute()->fetchField();

  return $result;
}

function _consultar_participantes_minified($id_nodo_sorteo) {
  $query = db_select('bbva_sorteos_participantes', 'bus');
  $query->leftJoin('field_data_field_profile_document_type','dt', 'dt.entity_id = bus.uid');
  $query->leftJoin('field_data_field_profile_document_number','dn', 'dn.entity_id = bus.uid');

  $query->fields('bus', array('id','uid'))
      ->fields('dt', array('field_profile_document_type_value'))
      ->fields('dn', array('field_profile_document_number_value'))
      ->condition('bus.nodo_id', $id_nodo_sorteo, '=')
      ->condition('dn.field_profile_document_number_value', "", '!=')
      ->condition('dn.field_profile_document_number_value', "0", '!=');

  $result = $query->execute()->fetchAll();

  return $result;
}

function _bbva_sorteo_vigencias($fecha_final, $nid){
  //EJ de Base de datos:2016-08-17 00:00:00
  $query = db_select('node', 'n');
  $query->join('field_data_field_experience_es_sorteo', 'es', 'es.entity_id = n.nid');
  $query->join('field_data_field_experience_fechas_vigencia', 'fv', 'fv.entity_id = n.nid');

  $query->fields('fv', array('field_experience_fechas_vigencia_value2'))
      ->condition('n.type', 'experience')
      ->condition('n.nid', $nid, "!=")
      ->condition('es.field_experience_es_sorteo_value', 1)
      ->condition('fv.field_experience_fechas_vigencia_value2', $fecha_final);

  $result = $query->execute();
  if ($result){
    return $result->rowCount();
  }
}