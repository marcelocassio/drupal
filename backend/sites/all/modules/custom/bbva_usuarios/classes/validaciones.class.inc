<?php

namespace BBVA;

define("BBVA_USUARIOS_TIPO_VALIDACION_EMAIL", "email");
define("BBVA_USUARIOS_TIPO_VALIDACION_VINCULACION", "vinculacion");
define("BBVA_USUARIOS_TIPO_VALIDACION_SUSCRIPCION", "suscripcion");
define("BBVA_USUARIOS_TIPO_VALIDACION_GENERAR_CONTRASENA", "generar_contraseña");
define("BBVA_USUARIOS_TIPO_VALIDACION_NUEVA_CONTRASENA", "nueva_contraseña");

class Validaciones {

  private static $instancia;

  const TIEMPO_VIGENCIA_HASH = 259200;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  /*private function __construct() {
    // El constructor debe ser privado (singleton)
  }*/

  public function generarHash() {
    $hash = md5(time() . rand(0, 9999999));
    return $hash;
  }

  public function generarCodigoFacil() {
    $codigo_facil = rand(0, 9999);
    $codigo_facil = sprintf("%04d", $codigo_facil);
    return $codigo_facil;
  }

  public function cargar($hash) {

    $result = db_select('bbva_validaciones', 'bv')
      ->condition('bv.hash', $hash)
      ->fields('bv')
      ->execute()
      ->fetchObject();

    if(isset($result->extra_data)) {
      $result->extra_data = unserialize($result->extra_data);
    }
    return $result;
  }

  public function insertar($hash, $uid = 0, $tipo, $codigo_facil = NULL, $extra_data = NULL) {
	if ($extra_data) {
      $extra_data = serialize($extra_data);
    }
    $vid = db_insert('bbva_validaciones')
      ->fields(array(
        'uid' => $uid,
        'tipo' => $tipo,
        'hash' => $hash,
        'codigo_facil' => $codigo_facil,
        'extra_data' => $extra_data,
        'fecha_creacion' => REQUEST_TIME,
      ))
      ->execute();

    return $vid;
  }

  public function limpiar() {
    $fecha = (int) (REQUEST_TIME - self::TIEMPO_VIGENCIA_HASH);
    $q = db_delete('bbva_validaciones')
      ->condition('fecha_creacion', $fecha, '<')
      ->execute();

    return $q;
  }

  public function borrar($hash) {
    $q = db_delete('bbva_validaciones')
      ->condition('hash', $hash)
      ->execute();

    return $q;
  }


  public function actualizarMedioComunicacion($vid, $medio_comunicacion) {
    $query = db_update('bbva_validaciones')
    ->fields(array(
      'medio_comunicacion' => $medio_comunicacion,
    ))
    ->condition('vid', $vid, '=')
    ->execute();

    return $query;
  }

  public function insertarIntento($hash) {
    $query = db_update('bbva_validaciones')
      ->expression('intentos', 'intentos + 1')
      ->condition('hash', $hash)
      ->execute();
    return $query;
  }

}