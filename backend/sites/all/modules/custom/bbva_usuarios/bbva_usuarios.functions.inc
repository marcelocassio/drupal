<?php

define("BBVA_USUARIO_TIPO_MEDIO_SMS", "sms");
define("BBVA_USUARIO_TIPO_MEDIO_MAIL", "mail");

function bbva_usuarios_cambiar_clave_validar($data) {
  $errores = array();
  $pass1 = $data['usuario']['pass_nueva1'];
  $pass2 = $data['usuario']['pass_nueva2'];

  if(($pass1 != bbva_sanitizar($pass1)) || ($pass2 != bbva_sanitizar($pass2))) {
    $errores['campos'][] = 'pass_no_coincide';
    $errores['textos'][] = 'La contraseña no cumple con los requisitos.';
  }
  if($pass1 != $pass2) {
    $errores['campos'][] = 'pass_no_coincide';
    $errores['textos'][] = 'Las contraseñas no coinciden.';
  }
  if((strlen($pass1) < 6) || (strlen($pass1) > 8)) {
    $errores['campos'][] = 'pass_minimo';
    $errores['textos'][] = 'La contraseña debe tener entre 6 y 8 caracteres';
  }

  $result = array("valido" => empty($errores), "errores" => $errores);
  return $result;
}


function bbva_usuarios_validar_data_perfil($tipo, $data) {
  $validar = array();
  $errores = array('campos' => array(), 'textos' => array());
  // Validaciones comunes a todos
  $validar[] = 'zonas';
  $validar[] = 'rubros';
  $validar[] = 'mail';
  $validar[] = 'tel_operador';
  $validar[] = 'tel_area';
  $validar[] = 'tel_numero';

  if($tipo == "fnet") {

  } elseif($tipo == "fb" || $tipo == "g") {
    $validar[] = 'documento';
    $validar[] = 'nombre';
    $validar[] = 'apellido';
    $validar[] = 'nacimiento';
    $validar[] = 'sexo';
  } elseif($tipo == 'manual') {
    $validar[] = 'documento';
    $validar[] = 'nombre';
    $validar[] = 'apellido';
    $validar[] = 'nacimiento';
    $validar[] = 'pass';
    $validar[] = 'sexo';
  }

  // Validaciones
  if(in_array('documento', $validar)){
    $validos = array("DNI", "CI", "LE", "LC", "PAS");
    if(!in_array($data['usuario']['documento']['tipo'], $validos)) {
      $errores['campos'][] = 'documento_tipo';
      $errores['textos'][] = 'Por favor, indicá tipo de documento';
    }
    if(!is_numeric($data['usuario']['documento']['numero']) || $data['usuario']['documento']['numero'] < 1) {
      $errores['campos'][] = 'documento_numero';
      $errores['textos'][] = 'Por favor, ingresá número de documento';
    }
  }

  if(in_array('nombre', $validar)){
    if(!isset($data['usuario']['nombre'])) {
      $errores['campos'][] = 'nombre';
      $errores['textos'][] = 'Por favor, ingresá nombre';
    }
  }

  if(in_array('apellido', $validar)){
    if(!isset($data['usuario']['apellido'])) {
      $errores['campos'][] = 'apellido';
      $errores['textos'][] = 'Por favor, ingresá apellido';
    }
  }


  if(in_array('zonas', $validar)){
    if(!is_array($data['preferencias']['zonas']) || empty($data['preferencias']['zonas'])) {
      $errores['campos'][] = "zonas";
      $errores['textos'][] = 'Por favor, indicar al menos una zona';
    }
  }

  if(in_array('rubros', $validar)){
    if(!is_array($data['preferencias']['rubros']) || empty($data['preferencias']['rubros'])) {
      $errores['campos'][] = "rubros";
      $errores['textos'][] = 'Por favor, indicar al menos un rubro';
    }
  }

  if(in_array('mail', $validar)){
    if(!filter_var($data['usuario']['mail'], FILTER_VALIDATE_EMAIL)) {
      $errores['campos'][] = "mail";
      $errores['textos'][] = 'Por favor, ingresá un mail válido';
    }
  }

  if(in_array('nacimiento', $validar)){
    $date = DateTime::createFromFormat('Y-m-d', $data['usuario']['nacimiento']);
    if(!$date) {
      $errores['campos'][] = "nacimiento";
      $errores['textos'][] = 'Por favor, indicá una fecha de nacimiento válida';
    }
  }

  if(in_array('sexo', $validar)){
    $validos = array("M", "F");
    if(!in_array(strtoupper($data['usuario']['sexo']), $validos)) {
      $errores['campos'][] = "sexo";
      $errores['textos'][] = 'Por favor, indicá sexo';
    }
  }

  if(in_array('tel_operador', $validar)){
    $validos = array("0001", "0002", "0003", "0004");
    if(!in_array($data['usuario']['telefono']['operador'], $validos)) {
      $errores['campos'][] = "tel_operador";
      $errores['textos'][] = 'Por favor, indicá compañía de teléfono celular';
    }
  }

  if(in_array('tel_area', $validar)){
    $validos = get_code_area_type();
    if(!in_array($data['usuario']['telefono']['area'], $validos)) {
      $errores['campos'][] = "tel_area";
      $errores['textos'][] = 'Por favor, ingresá código de área telefónico';
    }
  }

  if(in_array('tel_numero', $validar)){
    if(!is_numeric($data['usuario']['telefono']['numero']) || $data['usuario']['telefono']['numero'] < 100000) {
      $errores['campos'][] = "tel_numero";
      $errores['textos'][] = 'Por favor, ingresá número de teléfono';
    }
  }

  if(in_array('pass', $validar)){
    if($data['usuario']['pass']['pass1'] != $data['usuario']['pass']['pass2']) {
      $errores['campos'][] = "pass";
      $errores['textos'][] = 'Por favor, ingresá una contraseña válida y repetila en el siguiente campo';
    }
  }
  $result = array("valido" => empty($errores['campos']), "errores" => $errores);
  return $result;
}

function bbva_usuarios_crear_usuario_by_suscripcion($suscripcionSuip) {
  $usuario = new \BBVA\Usuario(0, true);
  $usuario->modificar(array("name" => $suscripcionSuip->numeroDeCliente));
  $usuario->cargarDatosSuscripcion($suscripcionSuip);
  if ($suscripcionSuip->esta_suscripto()) {
    $usuario->agregarRol(BBVA_AUTENTIFICACION_ROL_SUSCRIPTO);
  }
  $usuario->guardar();
  if (!isset($usuario->data->uid)) {
    throw new Exception("El usuario no se creo correctamente para el cliente: " . $suscripcionSuip->numeroDeCliente);
  }
  bbva_autentificacion_guardar_usuario_id_fnet($usuario->data->uid, 0);
  return $usuario->data->uid;
}

function bbva_usuarios_proceso_generar_clave($data_input){
  $code = "0";
  $message = "";
  $data = new StdClass();
  $dialogoBuilder = new \BBVA\DialogoBuilder();

  $numero_documento = trim($data_input['usuario']['documento']['numero']);
  $documento_tipo = trim($data_input['usuario']['documento']['tipo']);
  $sexo = null;

  if (isset($data_input['usuario']['sexo'])) {
    $sexo = $data_input['usuario']['sexo'];
  }

  // Validacion campos
  if ( ($documento_tipo == "" && $numero_documento == '') || (bbva_sanitizar($numero_documento) != $numero_documento) ) {
    $message = "Datos Inválidos. Revisá tus datos por favor";
    $dialogoBuilder->texto1 = $message;
    $data->dialogo = $dialogoBuilder->get_dialogo();
    $data->codigo_servicio = 2;
  } else {

    //validacion de documento
    try {

      $usuario_encontrado = bbva_suscripcion_verificar_dni($documento_tipo, $numero_documento, $sexo);
      $estado = $usuario_encontrado->usuario->estado;

      if ($estado == strtolower(BBVA_AUTENTIFICACION_ROL_SUSCRIPTO)) {

        if (!$usuario_encontrado->usuario->uid) { // Esto pasa cuando viene de FrancesGo 1.0
          $nuevo_usuario = new \BBVA\Usuario();
          $nuevo_usuario->cargarDatosSuscripcion($usuario_encontrado->usuario->suscripcion);
          $nuevo_usuario->modificar(array("name"=>$usuario_encontrado->usuario->suscripcion->numeroDeCliente));
          $nuevo_usuario->agregarRol(BBVA_AUTENTIFICACION_ROL_SUSCRIPTO);
          $nuevo_usuario->guardar();
        }

        $dialogoBuilder = new \BBVA\DialogoBuilder();
        $dialogoBuilder->titulo = "Generación de contraseña";
        $dialogoBuilder->texto1 = "Te vamos a enviar un código de confirmación para generar tu contraseña.";
        $data->dialogo = $dialogoBuilder->get_dialogo();
        $data->codigo_servicio = 0;
        if ((isset($usuario_encontrado->usuario->suscripcion->email)) && (!empty($usuario_encontrado->usuario->suscripcion->email))) {
          $dialogoBuilder->texto_boton = "Enviar por mail";
        } else {
          $dialogoBuilder->texto_boton = "Enviar por sms";
        }

        $data->dialogo = $dialogoBuilder->get_dialogo();
      } else { // No hay usuario existente, pero se lanza una confimación de envío falsa
        $dialogoBuilder = new \BBVA\DialogoBuilder();
        $dialogoBuilder->titulo = "Recupero de contraseña";
        $dialogoBuilder->texto_boton = "Volver";
        $dialogoBuilder->texto1 = "Aún no estás suscripto";
        $data->dialogo = $dialogoBuilder->get_dialogo();
        $data->codigo_servicio = 3;
      }

    } catch (DniDuplicadoSuscripcionException $e) {
      $data->codigo_servicio = 1;
    } catch (Exception $e) {
      $code = 1;
      $message = BBVA_USUARIO_ERROR_MENSAJE_GENERICO;
      $data = new StdClass();
    }
  }

  return _json_salida_formatear($code, $message, $data);

}

function bbva_usuarios_proceso_generar_clave_enviar($data_input) {
  $code = "0";
  $message = "";
  $data = new StdClass();
  $dialogoBuilder = new \BBVA\DialogoBuilder();

  $numero_documento = trim($data_input['usuario']['documento']['numero']);
  $documento_tipo = trim($data_input['usuario']['documento']['tipo']);

  $sexo = NULL;
  if (isset($data_input['usuario']['sexo'])) {
    $sexo = $data_input['usuario']['sexo'];
  }

  // Validacion campos 
  if( ($documento_tipo == "" && $numero_documento == '') || (bbva_sanitizar($numero_documento) != $numero_documento) ) {
    $data->codigo_servicio = 2;
    $message = "Datos Inválidos. Revisá tus datos por favor";
    $dialogoBuilder->texto1 = $message;
    $dialogoBuilder->dialogo = $dialogoBuilder->get_dialogo();
  } else {
    try {
      $usuario_encontrado = bbva_suscripcion_verificar_dni($documento_tipo, $numero_documento, $sexo);
      $uid = $usuario_encontrado->usuario->uid;
      $estado = $usuario_encontrado->usuario->estado;

      if($estado == strtolower(BBVA_AUTENTIFICACION_ROL_SUSCRIPTO)) {
        $validaciones = \BBVA\Validaciones::getInstance();
        $hash = $validaciones->generarHash();
        $codigo_facil = $validaciones->generarCodigoFacil();
        $validaciones->insertar($hash, $uid, BBVA_USUARIOS_TIPO_VALIDACION_GENERAR_CONTRASENA, $codigo_facil);

        // Funcion de enviar email o SMS
        $nombre_usuario = trim($usuario_encontrado->usuario->suscripcion->nombre);
        $email_usuario = NULL;
        if (isset($usuario_encontrado->usuario->suscripcion->email)) {
          $email_usuario = $usuario_encontrado->usuario->suscripcion->email;
        }
        $telefono_usuario = $usuario_encontrado->usuario->suscripcion->codigoDeArea.$usuario_encontrado->usuario->suscripcion->numeroDeCelular;
        $operador_telefono_usuario = $usuario_encontrado->usuario->suscripcion->codigoDeOperador;

        $envio = bbva_usuarios_enviar_email_sms($codigo_facil, $nombre_usuario, $email_usuario, $telefono_usuario,$operador_telefono_usuario);

        if(!$envio) {
          $code = 1;
          $message = "";
          $data = array();
          return _json_salida_formatear($code, $message, $data);
        }

        if($envio == "mail") {
          $dialogoBuilder->texto1 = "Ingresá el código de confirmación que enviamos a tu casilla de mail registrada.";
          $dialogoBuilder->texto_boton = "Volver a enviar mail";
        } elseif($envio == "sms") {
          $dialogoBuilder->texto1 = "Ingresá el código de confirmación que te enviamos por sms al teléfono registrado.";
          $dialogoBuilder->texto_boton = "Volver a enviar el SMS";
        }

        $data->codigo_servicio = 0;
        $data->hash = $hash;
        $data->dialogo = $dialogoBuilder->get_dialogo();

      } else { // No hay usuario existente, pero se lanza una confimación de envío falsa
        $dialogoBuilder->titulo = "Generación de contraseña";
        $dialogoBuilder->texto_boton = "Volver";
        $dialogoBuilder->texto1 = "Aún no estás suscripto";
        $data->dialogo = $dialogoBuilder->get_dialogo();
        $data->codigo_servicio = 3;
      }
    } catch (DniDuplicadoSuscripcionException $e) {
      $data->codigo_servicio = 1;
    } catch (Exception $e) {
      $code = 1;
      $message = BBVA_USUARIO_ERROR_MENSAJE_GENERICO;
      $data = new StdClass();
    }
  }

  return _json_salida_formatear($code, $message, $data);
}

function bbva_usuarios_proceso_generar_clave_validar($data_input) {
  $code = 0;
  $message = "";
  $resultado = new StdClass();
  $dialogoBuilder = new \BBVA\DialogoBuilder();

  try {
    // Valido que no este logueado
    global $user;
    if($user->uid > 0) {
      $resultado->codigo_servicio = 4;
      $dialogoBuilder->texto1 = "No se pudo validar";
      $resultado->dialogo = $dialogoBuilder->get_dialogo();
      return _json_salida_formatear($code, $message, $resultado);
    }

    // Valido el input
    if(!isset($data_input['input']['hash']) || !isset($data_input['input']['codigo'])) {
      $resultado->codigo_servicio = 3;
      $dialogoBuilder->texto1 = "Código incorrecto";
      $resultado->dialogo = $dialogoBuilder->get_dialogo();
      return _json_salida_formatear($code, $message, $resultado);
    }

    $validaciones = \BBVA\Validaciones::getInstance();
    $data = $validaciones->cargar($data_input['input']['hash']);

    // Si no encuentro el hash o no es del tipo
    if(!$data || $data->tipo != BBVA_USUARIOS_TIPO_VALIDACION_GENERAR_CONTRASENA) {
      $resultado->codigo_servicio = 3;
      $dialogoBuilder->texto1 = "Código incorrecto";
      $resultado->dialogo = $dialogoBuilder->get_dialogo();
      return _json_salida_formatear($code, $message, $resultado);
    }

    // Verifico que está vigente
    $valido_hasta = time() - (int) BBVA_USUARIOS_VIGENCIA_HASH_HS * 60 * 60;
    if($data->fecha_creacion < $valido_hasta) {
      $resultado->codigo_servicio = 3;
      $dialogoBuilder->texto1 = "Código incorrecto";
      $resultado->dialogo = $dialogoBuilder->get_dialogo();
      return _json_salida_formatear($code, $message, $resultado);
    }

    // Verifico que no haya superado la cantidad de intentos
    if($data->intentos >= BBVA_USUARIOS_MAX_INTENTOS_CODIGO_FACIL) {
      $resultado->codigo_servicio = 2;
      $dialogoBuilder->texto1 = "Superaste la cantidad máxima de intentos. Deberás generar un nuevo código de confirmación.";
      $resultado->dialogo = $dialogoBuilder->get_dialogo();
      return _json_salida_formatear($code, $message, $resultado);
    }

    // Verifico el codigo
    if($data->codigo_facil != $data_input['input']['codigo'] || strlen($data_input['input']['codigo']) != 4) {
      // Inserto intento
      $validaciones->insertarIntento($data_input['input']['hash']);
      $resultado->codigo_servicio = 1;
      $dialogoBuilder->texto1 = "Código inválido.";
      $resultado->dialogo = $dialogoBuilder->get_dialogo();
      return _json_salida_formatear($code, $message, $resultado);
    }

    $validaciones->borrar($data_input['input']['hash']);

    $dialogoBuilder = new \BBVA\DialogoBuilder();
    $dialogoBuilder->titulo = "Nueva contraseña";
    $dialogoBuilder->texto1 = "Completá los campos para generar tu nueva contraseña Go.";
    $dialogoBuilder->texto_boton = "Continuar";

    $hash = $validaciones->generarHash();
    $codigo_facil = $validaciones->generarCodigoFacil();
    $resultado->codigo_servicio = 0;
    $resultado->hash = $hash;

    $validaciones->insertar($hash, $data->uid, BBVA_USUARIOS_TIPO_VALIDACION_NUEVA_CONTRASENA, $codigo_facil);

  } catch(\Exception $e) {
    $code = 1;
  }

  return _json_salida_formatear($code, $message, $resultado);
}

function bbva_usuarios_proceso_generar_clave_nueva($data_input) {
  $code = 0;
  $message = "";
  $resultado = new StdClass();

  try {

    // Valido el hash
    if(!isset($data_input['input']['hash'])) {
      $resultado->codigo_servicio = 1;
      return _json_salida_formatear($code, $message, $resultado);
    }

    // Valido el input
    $valido = bbva_usuarios_cambiar_clave_validar($data_input['input']);
    if(!$valido['valido']) {
      $resultado->codigo_servicio = 2;
      $resultado->errores = $valido['errores'];
      return _json_salida_formatear($code, $message, $resultado);
    }

    $validaciones = \BBVA\Validaciones::getInstance();
    $data = $validaciones->cargar($data_input['input']['hash']);

    // Si no encuentro el hash o no es del tipo
    if(!$data || $data->tipo != BBVA_USUARIOS_TIPO_VALIDACION_NUEVA_CONTRASENA) {
      $resultado->codigo_servicio = 1;
      return _json_salida_formatear($code, $message, $resultado);
    }

    // Verifico que está vigente
    $valido_hasta = time() - (int) BBVA_USUARIOS_VIGENCIA_HASH_HS * 60 * 60;
    if($data->fecha_creacion < $valido_hasta) {
      $resultado->codigo_servicio = 1;
      return _json_salida_formatear($code, $message, $resultado);
    }

    $validaciones->borrar($data_input['input']['hash']);

    // Cambio el pass
    $usuario = new \BBVA\Usuario($data->uid, TRUE);
    $data_user["pass"] = $data_input['input']['usuario']['pass_nueva1'];
    $usuario->modificar($data_user);

    // Logueo y guardo (para el pass) el usuario (al loguear se guarda)
    $usuario->login();

    $message = "¡Contraseña actualizada con éxito!";
    $dialogoBuilder = new \BBVA\DialogoBuilder();
    $dialogoBuilder->texto1 = $message;

    $resultado->usuario = $usuario->getDatosBasicos();
    $resultado->dialogo = $dialogoBuilder->get_dialogo();


    $resultado->codigo_servicio = 0;

  } catch(\Exception $e) {
    $code = 1;
  }

  return _json_salida_formatear($code, $message, $resultado);
}

function bbva_usuarios_enviar_email_sms($pass, $nombre_usuario, $email_usuario=NULL, $telefono_usuario=NULL,$operador_telefono_usuario=NULL){
  global $base_url;
  $logo = $base_url.'/sites/default/files/go_icon.png';

  try {

    if ($email_usuario) {
      $html_email_body = theme(
        'mail-recupero',
        array('vars'=>
          array("nombre"=>$nombre_usuario,
            "pass"=>$pass,
            "logo"=>$logo
            )
          )
      );

      $params = array(
        'body' => $html_email_body,
        'subject' => $nombre_usuario . ", este es tu código de confirmación"
      );

      $to = $nombre_usuario . ' <' . $email_usuario . '>';

      drupal_mail('bbva_usuarios', 'recupero_contrasenia', $to, language_default(), $params, $from = NULL, $send = TRUE);

      return "mail";
    } elseif ($telefono_usuario) {
      $codigo_operador = $operador_telefono_usuario;
      $mensaje = "Utiliza este codigo para generar tu contrasena: " . $pass;
      $service = new SmsServiceFmovil();
      $res = $service->enviarSMS($codigo_operador, $telefono_usuario, $mensaje);

      return "sms";
    }

  } catch (\Exception $e) {}

  return FALSE;
}

function bbva_usuarios_enviar_email_sms_vincular($pass, $nombre_usuario, $email_usuario=NULL, $telefono_usuario=NULL,$operador_telefono_usuario=NULL, $medio){
  global $base_url;
  $logo = $base_url.'/sites/default/files/go_icon.png';

  switch ($medio) {
    case 'fb':
      $medio = "Facebook";
      $imagenVinculacion=$base_url.'/sites/default/files/tarjetas/disenios/face_go.png';
      break;
    case 'g':
      $medio = "Google";
      $imagenVinculacion=$base_url.'/sites/default/files/tarjetas/disenios/google_go.png';
      break;
  case 'apple':
      $medio = "Apple";
      $imagenVinculacion=$base_url.'/sites/default/files/tarjetas/disenios/apple_go.png';
      break;
    default:
      # code...
      break;
  }

  try {
    if ($email_usuario) {
      $html_email_body = theme(
        'mail-vinculacion',
        array('vars'=>
         array("nombre"=>$nombre_usuario,
           "pass"=>$pass,
           "logo"=>$logo,
           "imagenVinculacion"=>$imagenVinculacion,
           "medio"=>$medio,
           )
         )
      );

      $params = array(
        'body' => $html_email_body,
        'subject' => $nombre_usuario . ", este es tu código de confirmación"
      );

      $to = $nombre_usuario . ' <' . $email_usuario . '>';

      drupal_mail('bbva_usuarios', 'email_vinculacion', $to, language_default(), $params, $from = NULL, $send = TRUE);

      return "mail";
    } elseif ($telefono_usuario) {
      $codigo_operador = $operador_telefono_usuario;
      $mensaje = "Utiliza este codigo para conectar tus cuentas: " . $pass;
      $service = new SmsServiceFmovil();
      $res = $service->enviarSMS($codigo_operador, $telefono_usuario, $mensaje);

      return "sms";
    }

  } catch (\Exception $e) {}

  return FALSE;
}

function bbva_usuario_email_registrado_dialogo($datos_basicos_usuario){
  $respuesta =  null;

  $respuesta->usuario = $datos_basicos_usuario;
  $respuesta->codigo_servicio = 4;
  $dialogoBuilder = new \BBVA\DialogoBuilder();
  $message = "La actualización falló";
  $dialogoBuilder->titulo = $message;
  $dialogoBuilder->texto1 = "El mail ingresado ya fue registrado";
  $respuesta->dialogo = $dialogoBuilder->get_dialogo();
  return _json_salida_formatear("0", "", $respuesta);
}

function bbva_usuario_celular_registrado_dialogo($datos_basicos_usuario){
  $respuesta =  null;

  $respuesta->usuario = $datos_basicos_usuario;
  $respuesta->codigo_servicio = 3;
  $dialogoBuilder = new \BBVA\DialogoBuilder();
  $message = "La actualización falló";
  $dialogoBuilder->titulo = $message;
  $dialogoBuilder->texto1 = "El número de teléfono ingresado ya fue registrado";
  $respuesta->dialogo = $dialogoBuilder->get_dialogo();
  return _json_salida_formatear("0", "", $respuesta);
}


function bbva_suscripcion_validar_longitud_celular($codigo_area, $numero_celular) {
  $valido = true;
  $codigo_area = ltrim($codigo_area,'0');
  $numero_completo = strval($codigo_area) . strval($numero_celular);
  if (strlen($numero_completo) != 10) {
    $valido = false;
  }
  return $valido;
}


/**
* Servicio para administracion suscriptos
*
* @param $tipo_documento Valores: DNI, DNI_MASC, DNI_FEM, LC
* @param $numero_documento Numerico de documento
* @param $opcion Valores cons:"consulta", baja: "baja de suscripción"
*
*/
function _bbva_usuarios_get_view_per_user($filtros = null) {
  if (!$filtros) {
    return null;
  }

  try {
    $code = 0;
    $message = "ok";
    $respuesta = new stdClass();
    $suip_suscriptor_services = \SuscriptorServiceSuip::getInstance();
    $clienteService = \ClienteServiceFnet::getInstance();

    if (!empty($filtros["tipo_dni"]) && !empty($filtros["dni"])) {
      $tipo_documento_short = $filtros["tipo_dni"];
      $numero_documento = $filtros["dni"];
    }

    if (!empty($filtros['celular']) && !empty($filtros['codigo_area'])) {
      $telefono_area = $filtros['codigo_area'];
      $telefono_numero = $filtros['celular'];
      $suscripcionBo = $suip_suscriptor_services->consultarSuscripcionByTelefono($telefono_area, $telefono_numero);

      if (empty($suscripcionBo->codigoError)) {
        $tipo_documento_short = bbva_compatible_doctype_from_altamira($suscripcionBo->tipoDeDocumento);
        $numero_documento = $suscripcionBo->numeroDeDocumento;
      }

    }
    if ($tipo_documento_short && $numero_documento) {
      $clienteResponse = $clienteService->consultarNoCliente($tipo_documento_short, $numero_documento);
    }else{
      $code = 1;
      $message = "DNI/Celular no registrado en Go.";
    }
    if ($clienteResponse->codigo == "0") {

      // Consulta a Drupal
      $user = user_load_by_name($clienteResponse->numeroAltamira);
      $usuario = new \BBVA\Usuario($user->uid, TRUE);

      // Consulta a SUIP
      $datos_suip = $suip_suscriptor_services->consultarSuscripcionByNumeroCliente($clienteResponse->numeroAltamira);

      // Consulta a Redes Sociales Drupal
      $result_rrss = _bbva_usuarios_buscar_redes_sociales_uid($user->uid);

        // Resultados
      $respuesta->uid = $user->uid;
      $respuesta->nombre = $datos_suip->nombre;
      $respuesta->apellido = $datos_suip->apellido;
      $respuesta->numero_documento = $datos_suip->numeroDeDocumento;
      $respuesta->numero_celular = $datos_suip->codigoDeArea.$datos_suip->numeroDeCelular;
      $respuesta->vinculacion_fb = $result_rrss->FB;
      $respuesta->vinculacion_g = $result_rrss->G;
      $respuesta->vinculacion_apple = $result_rrss->APPLE;
      $respuesta->vinculacion_fnet = $result_rrss->Fnet;
      if ($usuario->data->created) {
        $respuesta->fecha_registro = date('d-m-Y', $usuario->data->created);
      }
    } elseif ($clienteResponse->codigo == "99") {
      $code = 1;
      $message = "La transacción no puede ser ejecutada en este momento";
    } else {
      $code = 1;
      $message = "DNI/Celular no registrado en Go.";
    }

  } catch (Exception $e) {
    $code = 1;
    $message = BBVA_SUSCRIPCION_ERROR_MENSAJE_GENERICO;
    $respuesta = $e->getMessage();
  }

  return array($code, $message, $respuesta);
}
