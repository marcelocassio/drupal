<html>
<head>
    <style>
        .contenedor {
            margin: 10px;
            max-width: 650px;
            font-family: sans-serif;
            font-weight: lighter;
            color: #474B50;
            margin: 0 auto;
            border-style: solid;
            border-color: #EDEDED;
            border-width: 15px;
            padding:10px;
            font-size: 0.9em;
        }
        .table {
            width: 100%;
            text-align: left;
        }
        .logo {
            text-align: right;
        }
        .logo img{
            /*width :155px;*/
            height : 53px;
        }
        .pass {
            text-align: center;
        }
        .lineafrances {
            text-align: center;
            font-size: 12px;
        }
        .botton>td {
            text-align: center;
        }
        .botton a {
            background-color: #009EE5;
            /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            padding: 10px 40px;
            border-radius: 25px;
            margin: 15px;
        }
    </style>
</head>
<body>
    <div class="contenedor">
        <table class="table">
            <thead>
                <th class="logo">
                    <img src="<?php echo $vars['logo']; ?>" />
                </th>
            </thead>
            <tr class="primer-linea">
                <td>
                    <h2>Hola
                        <?php print_r($vars['nombre'])?>,
                </td>
            </tr>

            <tr class="segunda-linea">
                <td>
                    <h3><b>Solicitaste generar tu contraseña de Go</b></h3>
                </td>
            </tr>


            <tr class="tercer-linea">
                <td>Utilizá este código de confirmación para generar tu contraseña:</td>
            </tr>

            <tr class="pass">
                <td>
                    <h2>
                        <?php print_r($vars['pass'])?>
                    </h2>
                </td>
            </tr>
           <tr class="lineafrances" >
             <td>Si no solicitaste generar tu contraseña, ignorá este mensaje.
             </td>
           </tr>
        </table>
    </div>
</body>
</html>