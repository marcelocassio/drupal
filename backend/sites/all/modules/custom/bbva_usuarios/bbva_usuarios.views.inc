<?php

/**
 * Views page.
 */
function bbva_usuarios_view_ate_usuarios(){

  if ((isset($_GET['filter_tipo_dni'])) && ($_GET['filter_tipo_dni'] != '')) {
    $filtros["tipo_dni"] = $_GET['filter_tipo_dni'];
  }
  if ((isset($_GET['filter_dni'])) && ($_GET['filter_dni'] != '')) {
    $filtros["dni"] = $_GET['filter_dni'];
  }
  if ((isset($_GET['filter_codigo_area'])) && ($_GET['filter_codigo_area'] != '')) {
    $filtros["codigo_area"] = $_GET['filter_codigo_area'];
  }
  if ((isset($_GET['filter_celular'])) && ($_GET['filter_celular'] != '')) {
    $filtros["celular"] = $_GET['filter_celular'];
  }

  $view_usuarios_data = _bbva_usuarios_get_view_per_user($filtros);
  return bbva_usuarios_create_view_ate_usuarios($view_usuarios_data[0], $view_usuarios_data[1], $view_usuarios_data[2]);
}

function bbva_usuarios_view_ate_usuarios_submit($form, &$form_state) {
  $form_state['filters']['dni'] = $form_state['values']['filter_dni'];
  $form_state['filters']['tipo_dni'] = $form_state['values']['filter_tipo_dni'];
  $form_state['filters']['celular'] = $form_state['values']['filter_celular'];
  $form_state['filters']['codigo_area'] = $form_state['values']['filter_codigo_area'];
  $form_state['rebuild'] = TRUE;
}

function bbva_usuarios_create_view_ate_usuarios($no_results, $mensaje, $usuario){

  $header_suip = array(
      array('data' => t('DNI')),
      array('data' => t('Nombre y Apellido')),
      array('data' => t('Nro. Celular')),
      array('data' => t('Fecha Registro FGo')),
      array('data' => t('FB')),
      array('data' => t('G+')),
      array('data' => t('Fnet'))
  );

  $form = array();

  $form['#method'] = 'get';

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buscar por:')
  );

  $form['filter']['filter_tipo_dni'] = array(
    '#type' => 'select',
    '#title' => t('Tipo documento'),
    '#default_value' => array('DNI' => 'DNI'),
    '#options' => array(
      'DNI' => 'DNI',
      'CI' => 'CI',
      'LE' => 'LE',
      'LC' => 'LC',
      'PAS' => 'Pasaporte'
      ),
    '#required' => TRUE,
    '#weight' => '0',
  );

  $form['filter']['filter_dni'] = array(
    '#type' => 'textfield',
    '#title' => t('DNI: '),
    '#size' => 6,
    '#weight' => '1',
    '#default_value'=> $_GET['filter_dni'],
  );

  if (!isset($_GET['filter_codigo_area'])) { $codigo_area = "11"; }else{ $codigo_area = $_GET['filter_codigo_area'];}

  $form['filter']['filter_codigo_area'] = array(
    '#type' => 'textfield',
    '#title' => t('Código área: '),
    '#size' => 5,
    '#weight' => '2',
    '#default_value'=> $codigo_area,
    '#description' => t('<b>No usar 15 como código de área</b>.'),
  );

  $form['filter']['filter_celular'] = array(
    '#type' => 'textfield',
    '#title' => t('Celular: '),
    '#size' => 8,
    '#weight' => '3',
    '#default_value'=> $_GET['filter_celular'],
    '#description' => t('<b>No incluir el código de área</b>.'),
  );

  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Buscar'),
    '#weight' => '5'
  );

  // Tomamos los datos
  if (!empty($usuario->vinculacion_fb)) { $fb = "X"; }else{ $fb = ""; }
  if (!empty($usuario->vinculacion_g)) { $g = "X"; }else{ $g = ""; }
  if (!empty($usuario->vinculacion_apple)) { $apple = "X"; }else{ $apple = ""; }
  if (!empty($usuario->vinculacion_fnet)) { $fnet = "X"; }else{ $fnet = ""; }

  $data[] = array(
    array('data' => $usuario->numero_documento, 'style' => 'font-size: 1em'),
    array('data' => $usuario->nombre." ".$usuario->apellido, 'style' => 'font-size: 1em'),
    array('data' => $usuario->numero_celular, 'style' => 'font-size: 1em'),
    array('data' => $usuario->fecha_registro, 'style' => 'font-size: 1em'),
    array('data' => $fb, 'style' => 'font-size: 1em'),
    array('data' => $g, 'style' => 'font-size: 1em'),
    array('data' => $apple, 'style' => 'font-size: 1em'),  
    array('data' => $fnet, 'style' => 'font-size: 1em')
    );

  if ($no_results == "1") {
    unset($data);
  }

  $form['results_fgo_suip'] = array(
    '#type' => 'fieldset',
    '#title' => t('Datos Registración en FGo:')
  );

  $form['results_fgo_suip']['table'] = array(
    '#theme' => 'table',
    '#header' => $header_suip,
    '#rows' => $data,
    '#empty' => $mensaje
  );

  // Se busca si el usuario ha participado en sorteos previa o actualmente

  $participacion = _bbva_sorteos_consultar_participacion_user_sorteos($usuario->uid);

  $header_sorteos = array(
      array('data' => t('Fecha Vigencia')),
      array('data' => t('Nombre del sorteo')),
      array('data' => t('Fecha Participación'))
  );

  $form['results_fgo_sorteos'] = array(
    '#type' => 'fieldset',
    '#title' => t('Participación en Sorteos:')
  );

  if (!empty($participacion)) {

    foreach ($participacion as $dato) {
      // Tomamos los datos
      if (!empty($dato->datetime_participate)) { $fecha_participacion = date('d-m-Y', 1467833225); }else{ $fecha_participacion = ""; }
      if (!empty($dato->field_experience_fechas_vigencia_value)) { $fecha_desde = date('d-m-Y', strtotime($dato->field_experience_fechas_vigencia_value)); }else{ $fecha_desde = ""; }
      if (!empty($dato->field_experience_fechas_vigencia_value2)) { $fecha_hasta = date('d-m-Y', strtotime($dato->field_experience_fechas_vigencia_value2)); }else{ $fecha_hasta = ""; }

      if ($dato->title) {
        $data_sorteos[] = array(
          array('data' => $fecha_desde. " al ".$fecha_hasta, 'style' => 'font-size: 1em'),
          array('data' => $dato->title, 'style' => 'font-size: 1em'),
          array('data' => $fecha_participacion, 'style' => 'font-size: 1em')
        );
      }
    }
  }

  if ($no_results == "1") {
    $mensaje_sorteos = $mensaje;
  }else{
    $mensaje_sorteos = "DNI/Celular no registra participaciones en sorteos.";
  }

  $form['results_fgo_sorteos']['table'] = array(
    '#theme' => 'table',
    '#header' => $header_sorteos,
    '#rows' => $data_sorteos,
    '#empty' => $mensaje_sorteos
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}
