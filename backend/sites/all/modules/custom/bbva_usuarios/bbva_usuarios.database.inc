<?php

define("BBVA_USUARIO_VINCULACION_PENDIENTE", 0);
define("BBVA_USUARIO_VINCULACION_REALIZADA", 1);
define("BBVA_USUARIO_VINCULACION_CHECK_STATUS", 2);

function bbva_usuarios_db_datos_usuario($uid) {
  $query = db_select('bbva_usuarios', 'bu');
  $query = $query->fields('bu', array('uid','nacionalidad'));
  $query = $query->condition('bu.uid', $uid);
  $result = $query->execute()->fetchObject();

  return $result;
}

function bbva_usuarios_db_actualizar_usuario($uid, $fields) {
  $query = db_merge('bbva_usuarios');
  $query = $query->key(
    array(
      'uid' => $uid
    )
  );
  $query = $query->fields($fields);
  $result = $query->execute();

  return $result;
}

function bbva_usuarios_buscar_usuario_by_dni($dni) {
  $user_found = false;
  $uid = db_select('users','u')
    ->fields('u', array('uid'))
    ->condition('name', $nombre_usuario, '=')
    ->execute()
    ->fetchField();
  if (!empty($uid)) {
    $user_found = true;
  }
  return $user_found;
}

function bbva_usuarios_buscar_uid($nombre_usuario) {
  $uid = db_select('users','u')
    ->fields('u', array('uid'))
    ->condition('name', $nombre_usuario, '=')
    ->execute()
    ->fetchField();
  return $uid;
}

function _bbva_usuarios_buscar_redes_sociales_uid($uid) {
  $result = db_select('bbva_user_rrss', 'ur');
      $result->addField('ur', 'ufacebook', 'FB');
      $result->addField('ur', 'ugoogle', 'G');
      $result->addField('ur', 'uapple', 'APPLE');
      $result->addField('ur', 'ufnet', 'Fnet');
      $result = $result->condition('uid', $uid)
      ->execute()->fetchObject();
  return $result;
}


function _bbva_usuario_desvincular_login($uid, $tipo) {

  if ($tipo == BBVA_AUTH_TIPO_FB) {
    bbva_autentificacion_guardar_usuario_id_fb($uid,0);
  } else if ($tipo == BBVA_AUTH_TIPO_GOOGLE) {
    bbva_autentificacion_guardar_usuario_id_google($uid, 0);
  } else if ($tipo == BBVA_AUTH_TIPO_APPLE) {
      bbva_autentificacion_guardar_usuario_id_apple($uid, 0);
  } else if ($tipo == BBVA_AUTH_TIPO_FNET) {
    bbva_autentificacion_guardar_usuario_id_fnet($uid,0);
  } else  {
    throw new Exception();
  }
}
