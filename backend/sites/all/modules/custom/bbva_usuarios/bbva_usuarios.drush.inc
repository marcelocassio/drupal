<?php

//BORRAR
function bbva_usuarios_drush_command() {
  $items = array();

  $items['bbva-usuarios-importar-login-app'] = array(
    'description' => 'Importar login app.',
    'aliases' => array('buila'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'file' => 'Archivo de usuarios',
    ),
  );

  return $items;
}

function drush_bbva_usuarios_importar_login_app($file) {
  $batch = array(
    'operations' => array(),
    'title' => t('Importar login app de usuarios'),
    'init_message' => t('[iniciando] Importar login app de usuarios...'),
    'error_message' => t('[error!]'),
    'finished' => 'bbva_usuarios_drush_finalizado',
    'file' => drupal_get_path('module', 'bbva_usuarios') . '/bbva_usuarios.drush.inc',
  );

  $batch['operations'][] = array('bbva_usuarios_importar_login_app', array($file));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

function bbva_usuarios_drush_finalizado($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Usuarios importados con éxito! - '.date('Y-m-d H:i:s')));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
      ));
    drupal_set_message($message, 'error');
  }
}

function bbva_usuarios_importar_login_app($file) {
  ini_set('max_execution_time', 0);
  ini_set("auto_detect_line_endings", true);
  $start = time();
  $usuarios = array();
  $handle = fopen($file, 'rb');
  if(!$handle) {
    return;
  }
  while (($datos = fgetcsv($handle, 1000, ";")) !== FALSE) {
    $aid = trim($datos[2]);
    $app = trim($datos[3]);
    $fecha = trim($datos[5]);
    if(is_numeric($aid) && $app == "1") {
      $sql = "SELECT uid FROM users WHERE name = :aid";
      $result = db_query($sql, array('aid' => $aid))->fetchObject();
      if(isset($result->uid)) {
        $login = \DateTime::createFromFormat('d/m/Y', $fecha)->getTimestamp();
        if(!$login) {
          $login = 1;
        }
        $query = db_merge('bbva_usuarios');
        $query = $query->key(
          array(
            'uid' => $result->uid
          )
        );
        $query = $query->fields(array('login_app' => $login));
        if(trim($datos[0]) != "31") {
          $query = $query->fields(array('nacionalidad' => 80));
        }
        $result = $query->execute();
        $usuarios['cantidad'] += 1;
      }
    }
  }
  $usuarios['tiempo'] = (int) (time() - $start) . " segundos.";
  drupal_set_message("Usuarios actualizados: " . $usuarios['cantidad']);
  drupal_set_message("Tiempo: " . $usuarios['tiempo']);
}