<?php

/**
 * Admin settings page.
 */
function bbva_usuarios_adminform($form, $form_state) {

  $form['bbva_usuarios_anti_phishing'] = array(
    '#title' => t('Activar protección anti phising'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_usuarios_anti_phishing', FALSE),
  );

  $form['bbva_usuarios_mostrar_datos_perfil'] = array(
    '#title' => t('Mostrar datos en el perfil'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_usuarios_mostrar_datos_perfil', FALSE),
  );

  $form['bbva_usuarios_ofuscar_perfil'] = array(
    '#title' => t('Ofuscar datos en el perfil'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_usuarios_ofuscar_perfil', FALSE),
  );

  return system_settings_form($form);
}
