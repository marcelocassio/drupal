<?php
$data_excel_rubros_subros = array();

$data_excel_rubros_subros["001"] = array(
  "001" => "Combustible",
  "002" => "Concesionarias",
  "003" => "Escuelas de manejo",
  "004" => "Estacionamientos",
  "005" => "Gomerías y neumáticos",
  "006" => "Motos",
  "007" => "Respuestos",
  "008" => "Service del auto",
  "069" => "Aceites y lubricantes"
);

$data_excel_rubros_subros["002"] = array(
  "009" => "Centros de estética",
  "010" => "Farmacias y Perfumerías",
  "011" => "Peluquerías adultos",
  "012" => "Salud",
  "079" => "Spas",
  "091" => "Cosméticos",
  "092" => "Solariums",
  "094" => "Peluquerías niños"
);

$data_excel_rubros_subros["003"] = array(
  "013" => "Clubes",
  "014" => "Gimnasios",
  "015" => "Naútica y Pesca"
);

$data_excel_rubros_subros["004"] = array(
  "016" => "Computación",
  "017" => "Electro",
  "018" => "Telefonía"
);

$data_excel_rubros_subros["005"] = array(
  "080" => "Agencias de viaje",
  "081" => "Alquiler de autos",
  "082" => "Balnearios",
  "083" => "Hoteles",
  "084" => "Líneas aéreas",
  "085" => "Outdoor",
  "086" => "Ski"
);

$data_excel_rubros_subros["006"] = array(
  "019" => "Animaciones y salones",
  "020" => "Cines",
  "021" => "Espectáculos",
  "022" => "Juegos",
  "023" => "Parque de diversiones",
  "024" => "Teatros",
  "095" => "Recitales"
);

$data_excel_rubros_subros["007"] = array(
  "025" => "Bares y cafeterías",
  "026" => "Bombonerías y chocolaterías",
  "027" => "Comida rápida",
  "028" => "Delivery y comida para llevar",
  "029" => "Heladerías",
  "030" => "Panaderías y confiterías",
  "031" => "Restaurantes"
);

$data_excel_rubros_subros["008"] = array(
  "032" => "Blanquerías",
  "034" => "Colchones y accesorios",
  "035" => "Construcción",
  "036" => "Diseño de interiores",
  "037" => "Electricidad",
  "038" => "Ferreterías",
  "039" => "Jardinería",
  "040" => "Muebles",
  "041" => "Pinturerías",
  "096" => "Sanitarios"
);

$data_excel_rubros_subros["009"] = array(
  "042" => "Artículos para bebés",
  "043" => "Cotillón",
  "044" => "Juguetería"
);

$data_excel_rubros_subros["010"] = array(
  "045" => "Accesorios y bijouterie",
  "046" => "Calzados y accesorios",
  "047" => "Indumentaria deportiva",
  "048" => "Indumentaria femenina",
  "049" => "Indumentaria infantil",
  "050" => "Indumentaria masculina",
  "051" => "Indumentaria unisex",
  "052" => "Joyería y relojería",
  "053" => "Lencería",
  "054" => "Marroquinería y equipajes",
  "055" => "Ópticas",
);

$data_excel_rubros_subros["011"] = array(
  "056" => "Bailables",
  "057" => "Bicicleterías",
  "058" => "Diarios y revistas",
  "059" => "Fotografía",
  "061" => "Instituciones educativas",
  "062" => "Insumos Agro",
  "063" => "Kiosko/Tabaquería",
  "064" => "Librería/Papelería/Fotocopias",
  "065" => "Música e instrumentos musicales",
  "066" => "Otros",
  "067" => "Servicios",
  "068" => "Veterinarias y pet shop",
  "070" => "Supermercados"
);

$data_excel_rubros_subros["013"] = array(
  "033" => "Talabertería",
  "060" => "Free Shop",
  "071" => "Armería/Cuchillería",
  "072" => "Artículos regionales",
  "073" => "Bazar",
  "074" => "Florerías",
  "075" => "Librerías",
  "076" => "Objetos de arte",
  "077" => "Orfebrería",
  "078" => "Vinoteca",
  "097" => "Desayunos"
);

$data_excel_rubros_subros["014"] = array(
  "087" => "River Plate",
  "088" => "Boca Juniors",
  "089" => "Talleres de Córdoba"
);

$data_excel_rubros_subros["015"] = array("090" => "Centro Comercial");
