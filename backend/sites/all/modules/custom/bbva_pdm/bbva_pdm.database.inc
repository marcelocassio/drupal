<?php

function _bbva_pdm_get_competencias_view($filtros=NULL, $limit=NULL){

  $query = db_select('bbva_pdm_competencias', 'bpc');
  $query->fields('bpc', array('id', 'cuit', 'nombre_fantasia'));

  if (is_array($filtros)) {
    if (isset($filtros["cuit"])) {
      $query->condition('bpc.cuit', $filtros["cuit"], '=');
    }
    if (isset($filtros["nombre_fantasia"])) {
      $query->condition('bpc.nombre_fantasia', '%' . db_like(trim($filtros["nombre_fantasia"])) . '%', 'LIKE');
    }
  }

  $query->orderBy("id", "desc");

  if ($limit != '') {
    $query = $query->extend('TableSort')->extend('PagerDefault')->limit($limit);
  }

  $results = $query->execute()->fetchAll();
  if ($results) {
    return $results;
  }else{
    return null;
  }
}

function _bbva_pdm_buscar_datos_competencia($id){
    $query = db_select('bbva_pdm_competencias', 'bpc');
    $query = $query->condition('bpc.id',$id);
    $query = $query->fields('bpc', array('cuit','nombre_fantasia'));

    $result = $query->execute()->fetchObject();

    return $result;
}

function _bbva_pdm_crear_competencia($data){
  $query = db_insert('bbva_pdm_competencias')
     ->fields(array(
        'cuit' => $data[0],
        'nombre_fantasia' => $data[1],
      ));
  $result = $query->execute();
  if (!$result) {
    throw new Exception('Error al crear el comercio.');
  }
}

function _bbva_pdm_editar_competencia($data){
  $query = db_update('bbva_pdm_competencias')
     ->fields(array(
        'cuit' => $data[1],
        'nombre_fantasia' => $data[2],
      ))
    ->condition('id', $data[0]);
  $result = $query->execute();
  if (!$result) {
    throw new Exception('Error al editar el comercio.');
  }
}

function _bbva_pdm_borrar_competencia($id){
  $deleted = db_delete('bbva_pdm_competencias')
    ->condition('id', $id)
    ->execute();

  if (!$deleted) {
    throw new Exception('Error al borrar el comercio.');
  }
}
/* Se usa en la antigua view */
function _bbva_pdm_buscar_rubros_observados($id_visa){
    $query = db_select('bbva_pdm_rubros', 'bpr');
    $query = $query->condition('bpr.id_visa',$id_visa);
    $query = $query->fields('bpr', array('observado'));

    $result = $query->execute()->fetchObject();

    return $result;
}

/* Se usa en la antigua view */
function _bbva_pdm_buscar_rubros_excl($id_visa){
    $query = db_select('bbva_pdm_rubros', 'bpr');
    $query = $query->condition('bpr.id_visa',$id_visa);
    $query = $query->fields('bpr', array('excluido'));

    $result = $query->execute()->fetchObject();

    return $result;
}

/* Se usa para la migracion de Rubros PDM del viejo core al nuevo */
function _bbva_pdm_buscar_rubros($id_visa){
    $query = db_select('bbva_pdm_rubros', 'bpr');
    $query = $query->condition('bpr.id_visa',$id_visa);
    $query = $query->condition(
      db_or()
      ->condition('bpr.observado', 1)
      ->condition('bpr.excluido', 1)
    );
    $query = $query->fields('bpr', array('excluido', 'observado'));

    $result = $query->execute()->fetchObject();

    return $result;
}

/* En reemplazo de "_bbva_pdm_buscar_rubros" para nuevo core */
function _bbva_pdm_buscar_rubros_v2($idSUIP){
  $rubroBo = new \Fgo\Bo\RubroBo();
  $rubro = $rubroBo->buscarPorIdSuip($idSUIP);

  return (int)$rubro->estadoPdm;
}

function _bbva_pdm_db_observar_rubros($ids) {

  db_update('bbva_pdm_rubros')
    ->fields(array('observado' => 0))
    ->execute();
  foreach ($ids as $each_id) {
    db_merge('bbva_pdm_rubros')
      ->key(array('id_visa' => $each_id))
      ->fields(array(
          'observado' => 1,
      ))
      ->execute();
  }

  return TRUE;
}

function _bbva_pdm_db_excluir_rubros($ids) {

  db_update('bbva_pdm_rubros')
    ->fields(array('excluido' => 0))
    ->execute();
  foreach ($ids as $each_id) {
    db_merge('bbva_pdm_rubros')
      ->key(array('id_visa' => $each_id))
      ->fields(array(
          'excluido' => 1,
      ))
      ->execute();
  }

  return TRUE;
}

function _bbva_pdm_db_pdm_insertar_registro($values) {
  // Valido los campos
  $values['cuit'] = bbva_pdm_format_cuit_no_hyphen($values['cuit']);
  if($values['venta_en_local'] == "0") {
    $values['sucursales'] = "0";
  }

  if($values['venta_en_internet'] == '0') {
    $values['ecommerce'] = NULL;
  }

  if(!isset($values['telefono_fijo']) || empty($values['telefono_fijo'])){
    $values['telefono_fijo'] = '0';
  }

  $values['rubro'] = (int) $values['rubro'];
  $values['venta_en_local'] = (int) $values['venta_en_local'];
  $values['sucursales'] = (int) $values['sucursales'];
  $values['venta_en_internet'] = (int) $values['venta_en_internet'];
  $values['acreditar_cupones'] = (int) $values['acreditar_cupones'];
  $values['es_cliente'] = (int) $values['es_cliente'];
  $values['es_adquirente'] = (int) $values['es_adquirente'];
  $values['estado'] = (int) $values['estado'];
  $values['alta_suip'] = isset($values['alta_suip']) ? serialize($values['alta_suip']) : null;

  $query = db_insert('bbva_pdm_registros');
  $query = $query->fields($values);
  $result = $query->execute();

  return $result;
}

function _bbva_pdm_rubro_id_suip_by_tid($tid){
  $query = db_select('field_data_field_category_id', 'fdfci');
  $query = $query->condition('fdfci.entity_id',$tid);
  $query = $query->condition('fdfci.entity_type',"taxonomy_term");
  $query = $query->fields('fdfci', array('field_category_id_value'));

  $result = $query->execute()->fetchField();

  return $result;
}

function _bbva_pdm_verify_last_register($cuit){
  if (!$cuit) {
    return null;
  }
  $query = db_select('bbva_pdm_registros', 'bpr');
  $query = $query->condition('bpr.cuit',$cuit);
  $query = $query->condition('bpr.estado', "-1", '!=');
  $query = $query->fields('bpr', array('fecha', 'estado', 'alta_suip'));
  $query->orderBy('fecha', 'DESC')
  ->range(0,1);

  $convalidacion = $query->execute()->fetchObject();

  if (isset($convalidacion->fecha)) {
    $fecha = strtotime("+6 month", $convalidacion->fecha);

    if (REQUEST_TIME < $fecha ) {
      return $convalidacion;
    }

  }
  return FALSE;
}
