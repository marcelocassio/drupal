<html>
<head>
    <style>
        .contenedor {
            margin: 10px;
            max-width: 650px;
            font-family: sans-serif;
            font-weight: lighter;
            color: #474B50;
            margin: 0 auto;
            border-style: solid;
            border-color: #EDEDED;
            border-width: 15px;
            padding:10px;
            font-size: 0.9em;
        }
        .table {
            width: 100%;
            text-align: left;
        }
        .logo {
            text-align: right;
        }
        .logo img{
            /*width :155px;*/
            height : 53px;
        }
        .pass {
            text-align: center;
        }
        .lineafrances {
            text-align: center;
            font-size: 12px;
        }
        .botton>td {
            text-align: center;
        }
        .botton a {
            background-color: #009EE5;
            /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            padding: 10px 40px;
            border-radius: 25px;
            margin: 15px;
        }
    </style>
</head>
<body>
    <div class="contenedor">
        <table class="table">
            <thead>
                <th class="logo">
                    <img src="<?php echo $vars['logo']; ?>" />
                </th>
            </thead>
            <tr class="primer-linea">
                <td>
                    <h2>Datos:</h2>
                </td>
            </tr>

            <tr class="segunda-linea">
                <td>
                    <h3><b>Cuit:</b> <?php print_r($vars['cuit'])?></h3>
                </td>
            </tr>


            <tr class="tercer-linea">
                <td>
                    <h3><b>Nombre Fantasia:</b> <?php print_r($vars['nombre_fantasia'])?></h3>
                </td>
            </tr>

            <tr class="tercer-linea">
                <td>
                    <h3><b>Razon social:</b> <?php print_r($vars['razon_social'])?></h3>
                </td>
            </tr>
          <?php
          if (!empty($vars['detalle'])){
          ?>
              <tr class="tercer-linea">
                  <td>
                      <h3><b>Detalle del estado:</b> <?php print_r($vars['detalle'])?></h3>
                  </td>
              </tr>
          <?php
          }

          if (!empty($vars['explicacion_estado'])){
          ?>
              <tr class="tercer-linea">
                  <td>
                      <h3><b>Explicacion del estado:</b> <?php print_r($vars['explicacion_estado'])?></h3>
                  </td>
              </tr>
          <?php
          }
          ?>
            <tr class="tercer-linea">
                <td>
                    <h3><b>Fecha Registro:</b> <?php print_r($vars['fecha_registro'])?></h3>
                </td>
            </tr>

        </table>
    </div>
</body>
</html>
