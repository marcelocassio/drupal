<?php
define('PDM_NO_RESULTS', 'No hay resultados para mostrar');
/**
 * Views page.
 */
function bbva_pdm_view_competencias(){

  if ((isset($_GET['filter_cuit'])) && ($_GET['filter_cuit'] != '')) {
    $filtros["cuit"] = $_GET['filter_cuit'];
  }
  if ((isset($_GET['filter_nombre_fantasia'])) && ($_GET['filter_nombre_fantasia'] != '')) {
    $filtros["nombre_fantasia"] = $_GET['filter_nombre_fantasia'];
  }
  $data_view_competencias = _bbva_pdm_get_competencias_view($filtros,100);

  return bbva_pdm_create_view_competencias($data_view_competencias);
}

function bbva_pdm_view_competencias_submit($form, &$form_state) {
  $form_state['filters']['cuit'] = $form_state['values']['filter_cuit'];
  $form_state['filters']['nombre_fantasia'] = $form_state['values']['filter_nombre_fantasia'];
  $form_state['rebuild'] = TRUE;
}

function bbva_pdm_create_view_competencias($data_view_competencias){
  $form = array();

  $header = array(
      array('data' => t('CUIT')),
      array('data' => t('Nombre Fantasía')),
      array('data' => t('Editar')),
      array('data' => t('Eliminar'))
  );

  $form['create'] = array(
    '#type' => 'fieldset',
    '#title' => t('Crear comercio competencia:')
  );

  $form['create']['submit'] = array(
    '#type' => 'link',
    '#title' => t('Crear'),
    '#attributes' => array(
      'style'=>'
        cursor: pointer;
        border-radius: 0;
        border: none;
        padding: 6px 10px;
        margin-bottom: 1em;
        margin-right: 1em;
        color: #fff;
        text-align: center;
        font-weight: normal;
        font-size: 1.077em;
        font-family: "Lucida Grande",Verdana,sans-serif;
        background: #4d8f46;
        transition: all, 0.3s;'
    ),
    '#href' => 'admin/competencias/crear'
  );

  $form['#method'] = 'get';

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buscar por:')
  );

  $form['filter']['filter_cuit'] = array(
    '#type' => 'textfield',
    '#title' => t('CUIT: '),
    '#size' => 11,
    '#maxlength' => 11,
    '#weight' => '1',
    '#description' => t('CUIT sin guiones (-)'),
    '#default_value'=> $_GET['filter_cuit'],
  );

  $form['filter']['filter_nombre_fantasia'] = array(
    '#type' => 'textfield',
    '#title' => t('Nombre fantasía: '),
    '#size' => 30,
    '#weight' => '2',
    '#default_value'=> $_GET['filter_nombre_fantasia']
  );

  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Buscar'),
    '#weight' => '3'
  );

  foreach ($data_view_competencias as $dvc) {
    $data[] = array(
      array('data' => $dvc->cuit, 'style' => 'font-size: 1em'),
      array('data' => $dvc->nombre_fantasia, 'style' => 'font-size: 1em'),
      array('data' => array('#type' => 'link', '#title' => t('Editar'), '#href' => 'admin/competencias/editar/'.$dvc->id)),
      array('data' => array('#type' => 'link', '#title' => t('Eliminar'), '#href' => 'admin/competencias/eliminar/'.$dvc->id))
      );
  }

  $form['results'] = array(
    '#type' => 'fieldset',
    '#title' => t('Competencias:')
  );

  $form['results']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $data,
    '#empty' => PDM_NO_RESULTS
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}


/**
 * Views page.
 */
function bbva_pdm_view_rubros_exc(){
  $data_view_rubros_exc = bbva_pdm_api_rubros();
  return bbva_pdm_create_view_rubros_exc($data_view_rubros_exc);
}

function bbva_pdm_view_rubros_exc_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function bbva_pdm_create_view_rubros_exc($data_view_rubros_exc){
  $header = array(
      array('data' => t('Observar'), 'style' => 'text-align:right;'),
      array('data' => t('Excluir'), 'style' => 'text-align:center;'),
      array('data' => t('ID Visa')),
      array('data' => t('Nombre'))
  );

  $form = array();

  $form['#method'] = 'get';

  foreach ($data_view_rubros_exc as $key_rubropadre => $rubropadre) {
    $data[] = array(
      array('data' => t('<i> Rubro !i - ID Visa: '.$key_rubropadre, array('!i' => $rubropadre['nombre'])), 'header' => TRUE),
      array('data' => '', 'header' => TRUE),
      array('data' => '', 'header' => TRUE),
      array('data' => '', 'header' => TRUE),
      );

      if (isset($rubropadre['subrubros'])) {
        foreach ($rubropadre['subrubros'] as $key_subrubros => $subrubros) {
          $excluidos_temp = _bbva_pdm_buscar_rubros_excl($key_subrubros);
          if (isset($excluidos_temp->excluido)) {
            $excluido = $excluidos_temp->excluido;
          }else{
            $excluido = null;
          }
          $observados_temp = _bbva_pdm_buscar_rubros_observados($key_subrubros);
          if (isset($observados_temp->observado)) {
            $observado = $observados_temp->observado;
          }else{
            $observado = null;
          }
          $data[] = array(
            array('data' => array('#type' => 'checkbox', '#checked' => $observado?1:0, '#title' => t(''), '#id' => $key_subrubros, '#name' => "checkobs"), 'style' => 'font-size: 1em; text-align:right;'),
            array('data' => array('#type' => 'checkbox', '#checked' => $excluido?1:0, '#title' => t(''), '#id' => $key_subrubros, '#name' => "checkre"), 'style' => 'font-size: 1em; text-align:center;'),
            array('data' => $key_subrubros, 'style' => 'font-size: 1em'),
            array('data' => $subrubros['nombre'], 'style' => 'font-size: 1em')
           );
        }
      }
  }

 $form['results'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rubros excluidos:')
  );

  $form['results']['bbva_pdm_dialogo'] = array(
    '#markup' => '<p id="bbva_pdm_dialogo" style="display: none;"><span id="messagecheckboxesselected"></span></p>'
  );

  $form['results']['excluir'] = array(
    '#type' => 'submit',
    '#value' => t('Excluir Rubros test')
  );

  $form['results']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $data,
    '#empty' => PDM_NO_RESULTS
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}
