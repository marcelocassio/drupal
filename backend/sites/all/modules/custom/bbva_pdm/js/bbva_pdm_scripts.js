(function($) {


  Drupal.behaviors.bbva_pdm = {
    attach: function (context, settings) {

      $( "#edit-filter-cuit" ).keydown(function(event) {
        if (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 190 && charCode > 31 &&
           (charCode < 48 || charCode > 57) &&
           (charCode < 96 || charCode > 105) &&
           (charCode < 37 || charCode > 40) &&
            charCode != 110 && charCode != 8 && charCode != 46 )
           return false;
        }
      return true;
      });

      $( "#edit-cuit" ).keydown(function(event) {
        if (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 190 && charCode > 31 &&
           (charCode < 48 || charCode > 57) &&
           (charCode < 96 || charCode > 105) &&
           (charCode < 37 || charCode > 40) &&
            charCode != 110 && charCode != 8 && charCode != 46 )
           return false;
        }
      return true;
      });

      //Al iniciar la web se tienen que dejar los demás inactivos
      $("input:checkbox[name=checkobs]:checked").each(function(){
          $('input:checkbox[name=checkre][id='+this.id+']').attr("disabled", true);
      });
      $("input:checkbox[name=checkre]:checked").each(function(){
          $('input:checkbox[name=checkobs][id='+this.id+']').attr("disabled", true);
      });

      //  Checkboxes select/deselect
        //Observados
      $('input:checkbox[name=checkobs]').click(function() {
        if (this.checked) {
          $('input:checkbox[name=checkre][id='+this.id+']').attr("disabled", true);
        }else{
          $('input:checkbox[name=checkre][id='+this.id+']').attr("disabled", false);
        }
      });
      //Excluidos
      $('input:checkbox[name=checkre]').click(function() {
        if (this.checked) {
          $('input:checkbox[name=checkobs][id='+this.id+']').attr("disabled", true);
        }else{
          $('input:checkbox[name=checkobs][id='+this.id+']').attr("disabled", false);
        }
      });

      //  Boton final excluir
      $('#edit-excluir').click(function(event) {
        event.preventDefault();

        var messageToClient = '';

        document.selectedRubrosObservados = new Array();
        $("input:checkbox[name=checkobs]:checked").each(function(){
          document.selectedRubrosObservados.push(this.id);
        });

        if (document.selectedRubrosObservados.length == 0) {
          messageToClient += "Querés dejar que no haya rubros observados?<br/>";
        }else{
          messageToClient += "Querés dejar observados a los comercios con este/os <b>"+document.selectedRubrosObservados.length+"</b> rubro/s?<br/>";
        }

        document.selectedRubrosExcluidos = new Array();
        $("input:checkbox[name=checkre]:checked").each(function(){
          document.selectedRubrosExcluidos.push(this.id);
        });

        if (document.selectedRubrosExcluidos.length == 0) {
          messageToClient += "Querés dejar que no haya rubros excluidos?";
        }else{
          messageToClient += "Querés dejar excluidos a los comercios con este/os <b>"+document.selectedRubrosExcluidos.length+"</b> rubro/s?";
        }
          $( '#bbva_pdm_dialogo' ).dialog({
          autoOpen: false,
          title: 'Confirmación',
          dialogClass: 'alert',
          modal: true,
          show: 'blind',
          resizable: false,
          open: function(event, ui) {
              $('.ui-dialog-titlebar-close', ui.dialog | ui).hide();
          },
          hide: 'explode',
          buttons: {
            Aceptar: function() {
              $( 'html body' ).css({
                'cursor': 'wait',
                'background-color': '#000000',
                'opacity': '0.5'
              });
              $.ajax({
                type: 'POST',
                url: Drupal.settings.basePath + 'admin/rubros_observados_excluidos/modificar',
                dataType: 'json',
                success: function(){
                  alert("Actualizada la lista de rubros");
                  $( '#bbva_pdm_dialogo' ).dialog( 'close' );
                  $( 'html body' ).css({
                    'cursor': 'auto',
                    'background-color': '#fff',
                    'opacity': '1'
                  });
                },
                data: {"observados":document.selectedRubrosObservados, "excluidos":document.selectedRubrosExcluidos}
              });
            },
            Cancelar: function() {
              $( this ).dialog( 'close' );
            }
          }
        });

        $('#messagecheckboxesselected').html(messageToClient);
        $('#bbva_pdm_dialogo').dialog('open');
      });
    }
  }
})(jQuery);
