<?php

function bbva_pdm_batch_rubros_pdm($ODATE) {
  try {
    // leo archivos de PDM RUBROS, no por ahora
    //$retorno = bbva_pdm_read_file_pdm_rubros();
    $retorno = null;
    //Se inserta la data dentro de la tabla bbca_ast_usuarios
    $cache_to_store = _bbva_pdm_insertar_data_cache($retorno);

    //Cache PDM Rubros
    cache_set('jsonPDMRubros',$cache_to_store,'cache_api',CACHE_PERMANENT);
    drupal_set_message(t("PDM Rubros Cacheado"), 'status', FALSE);

  } catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
    exit(1);
  }
}
