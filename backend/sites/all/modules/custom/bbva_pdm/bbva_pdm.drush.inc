<?php
/*
 * Implements hook_drush_command().
 */
function bbva_pdm_drush_command() {

  $items['vuelco-rubros-pdm'] = array(
    'description' => 'Realiza el vuelco de los rubros en suip a drupal',
    'aliases' => array('rubros-pdm'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */

// Imports
function drush_bbva_pdm_vuelco_rubros_pdm($odate = NULL) {
  drupal_set_message( "Empieza el " . date("ymd"), 'status', FALSE);

  $batch = array(
    'operations' => array(),
    'title' => t('Batch actualizacion de rubros'),
    'init_message' => t('Batch rubros is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_ast_batch_finished',
    'file' => drupal_get_path('module', 'bbva_pdm') . '/bbva_pdm.batch.inc',
  );

   //Define operaciones
  $batch['operations'][] = array('bbva_pdm_batch_rubros_pdm',array($odate));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}
