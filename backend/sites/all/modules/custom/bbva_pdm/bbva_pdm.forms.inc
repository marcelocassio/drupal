<?php

function bbva_pdm_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'bbva_pdm_editar_competencias_form') {
    $form['#validate'][] = 'bbva_pdm_competencias_form_validate';
    $form['#submit'][] = 'bbva_pdm_editar_competencias_form_submit_handler';
  }elseif ($form_id == 'bbva_pdm_crear_competencias_form') {
    $form['#validate'][] = 'bbva_pdm_competencias_form_validate';
    $form['#submit'][] = 'bbva_pdm_crear_competencias_form_submit_handler';
  }
}

function bbva_pdm_competencias_form_validate(&$form, &$form_state){
  if ($form["#form_id"] == 'bbva_pdm_editar_competencias_form' || $form["#form_id"] == 'bbva_pdm_crear_competencias_form') {
    $cuit = $form_state['input']['cuit'];
    $nombre_fantasia = $form_state['input']['nombre_fantasia'];
    if (empty($cuit)) {
      form_set_error("cuit", "Complete el CUIT");
    }elseif (!is_numeric($cuit)) {
      form_set_error("cuit", "Sólo se aceptan números en el CUIL");
    }elseif (empty($nombre_fantasia)) {
      form_set_error("nombre_fantasia", "Complete el nombre");
    }
  }
}

/* //////////////////////////////////////////////////////////////////////// */
// EDITAR COMPETENCIA
function bbva_pdm_editar_competencias_form_call($args) {
  $id = $args;
  return drupal_get_form('bbva_pdm_editar_competencias_form', $id);
}

function bbva_pdm_editar_competencias_form($form_state, $build) {
  $id = $build["build_info"]["args"][0];

  if (!empty($build["input"])) {
    $cuit = $build["input"]["cuit"];
    $nombre_fantasia = $build["input"]["nombre_fantasia"];
  }else{
    $datos_competencia = _bbva_pdm_buscar_datos_competencia($id);
    $cuit = $datos_competencia->cuit;
    $nombre_fantasia = $datos_competencia->nombre_fantasia;
  }

  $form['cuit'] = array( '#type' => 'textfield', '#value' => $cuit, '#description' => "<b>Usar sólo números.</b> No usar guiones o carácteres especiales", '#title' => t('CUIT'));
  $form['nombre_fantasia'] = array( '#type' => 'textfield',  '#value' => $nombre_fantasia, '#title' => t('Nombre Fantasía'));
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save'));

  return $form;
}

function bbva_pdm_editar_competencias_form_submit_handler($form, &$form_state) {
  if ($form["#form_id"] == "bbva_pdm_editar_competencias_form") {
    $id = $form_state["build_info"]["args"][0];

    $cuit = $form_state["input"]['cuit'];
    $nombre_fantasia = $form_state["input"]['nombre_fantasia'];

    $datos_competencia = array($id, $cuit, $nombre_fantasia);

    try {
      _bbva_pdm_editar_competencia($datos_competencia);
      drupal_set_message(t("Se actualizó el comercio ".$nombre_fantasia));
    } catch (Exception $e) {
      drupal_set_message(t($e->getMessage()));
    }

  $form_state['redirect'] = 'admin/competencias';
  }
}

// CREAR COMPETENCIA
function bbva_pdm_crear_competencias_form_call() {
  drupal_add_js(drupal_get_path('module', 'bbva_pdm') . '/js/bbva_pdm_scripts.js');
  return drupal_get_form('bbva_pdm_crear_competencias_form');
}

function bbva_pdm_crear_competencias_form($form_state, $build) {

  if (!empty($build["input"])) {
    $cuit = $build["input"]["cuit"];
    $nombre_fantasia = $build["input"]["nombre_fantasia"];
  }else{
    $cuit = "";
    $nombre_fantasia = "";
  }

  $form['cuit'] = array( '#type' => 'textfield', '#value' => $cuit, '#size' => 11,   '#maxlength' => 11,'#title' => t('CUIT'));
  $form['nombre_fantasia'] = array( '#type' => 'textfield', '#size' => 30, '#value' => $nombre_fantasia, '#title' => t('Nombre Fantasía'));
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Save'));

  return $form;
}

function bbva_pdm_crear_competencias_form_submit_handler($form, &$form_state) {
  if ($form["#form_id"] == "bbva_pdm_crear_competencias_form") {

    $cuit = $form_state["input"]['cuit'];
    $nombre_fantasia = $form_state["input"]['nombre_fantasia'];

    $datos_competencia = array($cuit, $nombre_fantasia);

    try {
      _bbva_pdm_crear_competencia($datos_competencia);
      drupal_set_message(t("Se creó el comercio ".$nombre_fantasia));
    } catch (Exception $e) {
      drupal_set_message(t($e->getMessage()));
    }

  $form_state['redirect'] = 'admin/competencias';
  }
}

// BORRAR COMPETENCIA
function bbva_pdm_eliminar_competencias_form_call($form, &$form_state) {
  $form = array();
  $id = arg(3);
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  $form = confirm_form($form,
    'Está seguro de querer borrar este comercio?',
    'admin/competencias',
    'Esta acción no puede ser revertida',
    'Eliminar',
    'Cancelar'
  );
  return $form;
}

function bbva_pdm_eliminar_competencias_form_call_submit($form, &$form_state) {
  $id = $form_state["values"]["id"];
  try {
    _bbva_pdm_borrar_competencia($id);
    drupal_set_message(t("Se eliminó el comercio."));
    $form_state['redirect'] = 'admin/competencias';
    } catch (Exception $e) {
      drupal_set_message(t($e->getMessage()));
    }
}
