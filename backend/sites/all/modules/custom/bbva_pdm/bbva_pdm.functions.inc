<?php
define('BBVA_PDM_RUTA_NOMINAS', '/prod/jp00/nominas/');
define('BBVA_PDM_ARCH_RUBROS', 'TJRUBR03_TABLA_RUBROS_FORM.TXT');

function bbva_pdm_validar_input_alta($data_input) {
  $result = array("valido" => TRUE);
  $errores = array();

  if(!isset($data_input['apellido_nombre']) || strlen($data_input['apellido_nombre']) < 3) {
    $errores['apellido_nombre'] = "Apellido y Nombre no es válido";
  }
  if(!empty($data_input['telefono_fijo']) && (strlen($data_input['telefono_fijo']) < 6 || strlen($data_input['telefono_fijo']) > 30)) {
    $errores['telefono_fijo'] = "Teléfono fijo no es válido";
  }
  if(!isset($data_input['telefono_celular']) || strlen($data_input['telefono_celular']) < 6 || strlen($data_input['telefono_celular']) > 30) {
    $errores['telefono_celular'] = "Teléfono celular no es válido";
  }
  if(!filter_var($data_input['correo_electronico'], FILTER_VALIDATE_EMAIL)) {
    $errores['correo_electronico'] = "El correo electrónico no es válido";
  }
  if(!isset($data_input['nombre_comercio']) || strlen($data_input['nombre_comercio']) < 2 || strlen($data_input['nombre_comercio']) > 90) {
    $errores['nombre_comercio'] = "Nombre del comercio no es válido";
  }
  if(!isset($data_input['razon_social']) || strlen($data_input['razon_social']) < 3 || strlen($data_input['razon_social']) > 90) {
    $errores['razon_social'] = "Razón social no es válido";
  }
  if(!isset($data_input['cuit']) || !bbva_pdm_validar_cuit($data_input['cuit'])) {
    $errores['cuit'] = "CUIT no es válido";
  }
  if(!is_numeric($data_input['rubro']) || $data_input['rubro'] < 1 || !bbva_pdm_search_rubro_in_drupal($data_input['rubro'])) {
    $errores['rubro'] = "Subrubro no es válido";
  }
  if(!isset($data_input['direccion']) || strlen($data_input['direccion']) < 3 || strlen($data_input['direccion']) > 220) {
    $errores['direccion'] = "Dirección no es válido";
  }

  if(strlen($data_input['sitio_web']) > 60) {
    $errores['sitio_web'] = "Sitio web no es válido";
  }

  if(!isset($data_input['venta_en_local'])) {
    $errores['venta_en_local'] = "Venta en local no es válido";
  }

  if($data_input['venta_en_local'] == "1") {
    if(!isset($data_input['sucursales']) || !is_numeric($data_input['sucursales'])) {
      $errores['sucursales'] = "Sucursales no es válido";
    }
  }

  if(!isset($data_input['venta_en_internet'])) {
    $errores['venta_en_internet'] = "Venta en internet no es válido";
  }

  if(strlen($data_input['ecommerce']) > 60) {
    $errores['ecommerce'] = "";
  }

  if(!isset($data_input['acreditar_cupones'])) {
    $errores['acreditar_cupones'] = "Acreditar cupones no es válido";
  }

  if(!empty($errores)) {
    $result['valido'] = FALSE;
  }
  $result['errores'] = $errores;

  return $result;
}

function bbva_pdm_consulta_nosis($cuit) {

  try {
    $fnet = \ClienteServiceFnet::getInstance();
    $result_nosis = $fnet->consultarNOSIS($cuit);

    if($result_nosis->Resultado != "OK") {
      if(module_exists('bbva_logs')) {
        $data_log = array(
          "errorCode" => $result_nosis->ErrorCode,
          "data" => $cuit
        );
        $log = \BBVA\Log::getInstance();
        $log->insertar("pdm_nosis_error", $data_log);
      }
      throw new Exception();
    }
  } catch (\Exception $e) {
    throw new Exception("Error en NOSIS".$e->getMessage());
  }
  //Devuelve string aprobado
  return $result_nosis->Respuesta->dictamen;
}

/* FUNCIONES BATCH */

// leo archivos de PDM RUBROS
function bbva_pdm_read_file_pdm_rubros() {
  $total_data = array();
  $filename = BBVA_PDM_RUTA_NOMINAS.BBVA_PDM_ARCH_RUBROS;

  $nominas_ast_nameuser_list_delimitadores = array(1,3,5,50,3,3,30,4,2,4,9,3,3,3,3,3,3,3,3);
  $handle = fopen($filename, "r");
  if ($handle !== FALSE) {
    while (!feof($handle)) {

      $linea_con_data = batch_messages_parsear_linea_ast($handle);

      $data_per_each_line = batch_messages_parsear_linea_array_ast($linea_con_data, $nominas_ast_nameuser_list_delimitadores);
      if(is_array($data_per_each_line) && ($data_per_each_line[9] == "SUIP")){
        if(!isset($total_data[$data_per_each_line[4]][$data_per_each_line[5]])){
          $total_data[$data_per_each_line[4]][$data_per_each_line[5]] = $data_per_each_line[3];
        }
      }
    }
  }

  return $total_data;
}

function _bbva_pdm_insertar_data_cache($retorno=NULL) {
  $cache_to_store_temp = array();
  $cache_to_store = array();
  $tree = taxonomy_get_tree(taxonomy_vocabulary_machine_name_load('category')->vid);
  $term_tree = array();

  foreach ($tree as $term) {
    $term_tree[$term->tid] = $term;
  }

  $array_rubros_null = array(3410, 3410, 3411, 3413, 3414, 3415, 3416, 3419, 3420, 3421, 3424, 3427, 3430, 3431, 3432, 3433, 3436, 3438, 3448, 3450, 3451, 3452, 3453, 3454, 3460, 3462, 3463, 3465, 3479, 3489, 3502, 3511, 3543, 3544, 3548, 3549, 3555, 3556, 3557, 3558, 3559, 3560, 3562, 3564, 3565, 3566, 3567, 3568, 3569, 3573, 3575, 3581, 4883, 4971,4972, 4974, 12533, 13527, 14288, 13538, 16422, 16457, 16458);

  foreach ($array_rubros_null as $value) {
    unset($term_tree[$value]);
  }

  _construct_nested_taxonomy_array($term_tree);

  $term_tree = array_reverse($term_tree);

  _construct_nested_taxonomy_item_list($term_tree, $cache_to_store_temp);

  //Se cambian los tids por el id de suip
  foreach ($cache_to_store_temp as $key => $data) {
    $id_suip_from_tid = _bbva_pdm_rubro_id_suip_by_tid($key);
    $cache_to_store[$id_suip_from_tid]["nombre"] = $data["nombre"];
    if (isset($data["subrubros"])) {
      foreach ($data["subrubros"] as $key_s => $data_subrubro) {
        $id_suip_from_tid_subrubro = _bbva_pdm_rubro_id_suip_by_tid($key_s);
        $cache_to_store[$id_suip_from_tid]["subrubros"][$id_suip_from_tid_subrubro] = $data_subrubro;
      }
    }
  }
  unset($cache_to_store_temp);
  return $cache_to_store;
}

 function _construct_nested_taxonomy_array(&$tree){
  foreach (array_reverse($tree) as $term) {
    if(array_key_exists($term->parents[0], $tree)){
      $tree[$term->parents[0]]->children[$term->tid] = $term;
      _construct_nested_taxonomy_array($tree[$term->parents[0]]->children);
      unset($tree[$term->tid]);
    }
  }
}
function _construct_nested_taxonomy_item_list(&$tree, &$cache_to_store){
  foreach (array_reverse($tree) as $term) {
    $cache_to_store[$term->tid] = array('nombre' => $term->name);
    if(isset($term->children)){
      _construct_nested_taxonomy_item_list($term->children, $cache_to_store[$term->tid]['subrubros']);
    }
  }
}

function bbva_pdm_estados_valor_alta($estado){
  switch ($estado) {
    case 'APROBADO':
      $codigo_estado = 1;
      break;
    case 'OBSERVADO':
      $codigo_estado = 2;
      break;
    case 'RECHAZADO':
      $codigo_estado = 3;
      break;
    case 'EXISTENTE':
      $codigo_estado = 4;
      break;
    case 'FALTA_COD_ACTIVACION':
      $codigo_estado = 5;
      break;
    case 'EXISTENTE_SIN_ESTADO':
      $codigo_estado = 6;
      break;
    case 'ERROR_INTERNO':
      $codigo_estado = "-1";
      break;
    default:
      $codigo_estado = "-1";
      break;
  }
  return $codigo_estado;
}

function bbva_pdm_estados_valor_alta_string($estado_numero){
  switch ($estado_numero) {
    case 1:
      $codigo_estado = 'APROBADO';
      break;
    case 2:
      $codigo_estado = 'OBSERVADO';
      break;
    case 3:
      $codigo_estado = 'RECHAZADO';
      break;
    case 4:
      $codigo_estado = 'EXISTENTE';
      break;
    case 5:
      $codigo_estado = 'FALTA_COD_ACTIVACION';
      break;
    case 6:
      $codigo_estado = 'EXISTENTE_SIN_ESTADO';
      break;
    case "-1":
      $codigo_estado = 'ERROR_INTERNO';
      break;
  }
  return $codigo_estado;
}

function bbva_pdm_estados_explicacion($estado_numero){
  $explicacion_estado = '';
  switch ($estado_numero) {
    case 4:
      $explicacion_estado = 'Probablemente el cuit existe como contribuyente y posee usuario en la tabla usuario_contribuyente.  "Contribuyente OK ya ingreso al portal". (Usuario posiblemente se le olvido la contraseña....)';
      break;
    case 5:
      $explicacion_estado = 'Se debe verificar que el cuit existe como contribuyente y NO posee usuario en la tabla usuario_contribuyente. "Contribuyente OK no ingresó al portal". (Fue dado de alta por otro medio)';
      break;
    case 6:
      $explicacion_estado = 'Dado posiblemente de alta por alianzas sin que el comercio supiera';
      break;
  }
  return $explicacion_estado;
}

function bbva_pdm_validar_cuit($cuit=NULL) {

  // valido el formato xx-xxxxxxxx-xx (2-8-1)
  if (!$cuit) {    return false;  }
  $ex = explode('-',$cuit);
  if(strlen($ex[0])!=2   || strlen($ex[1])!=8  || strlen($ex[2])!=1) {
    return false; //no es un codigo valido
  }else{
    return true;
  }

}

function bbva_pdm_search_rubro_in_drupal($tid_from_form=NULL){
  if (!$tid_from_form) {
    return false;
  }

  $tree = taxonomy_get_tree(taxonomy_vocabulary_machine_name_load('category')->vid);
  $term_tree = array();

  foreach ($tree as $term) {
    $id_suip_from_tid = _bbva_pdm_rubro_id_suip_by_tid($term->tid);
    $term_tree[$id_suip_from_tid] = true;
  }

  unset($tree);
  if (array_key_exists($tid_from_form, $term_tree)) {
    return true;
  }else{
    return false;
  }

}

function fgo_migrar_a_nuevo_core(){
  //0 = Aprobado; 1 = Observado; 2 = Rechazado; 3 = Oculto
  $array_rubros_null = array(3410, 3410, 3411, 3413, 3414, 3415, 3416, 3419, 3420, 3421, 3424, 3427, 3430, 3431, 3432, 3433, 3436, 3438, 3448, 3450, 3451, 3452, 3453, 3454, 3460, 3462, 3463, 3465, 3479, 3489, 3502, 3511, 3543, 3544, 3548, 3549, 3555, 3556, 3557, 3558, 3559, 3560, 3562, 3564, 3565, 3566, 3567, 3568, 3569, 3573, 3575, 3581, 4883, 4971,4972, 4974, 12533, 13527, 14288, 13538, 16422, 16457, 16458);

  $hola = new \Fgo\Bo\RubroBo();
  $rubros = $hola->listar();

  foreach($rubros as $rubro){
    $estado = 0;
    $rubro_e_y_o = _bbva_pdm_buscar_rubros($rubro->idRubroSuip);

    if($rubro_e_y_o) {
      if($rubro_e_y_o->excluido == "1") {
        $estado = 2;
      }
      if($rubro_e_y_o->observado == "1") {
        $estado = 1;
      }
    }

    $nuevo_rubro = new \Fgo\Bo\RubroBo($rubro->idRubro);
    $nuevo_rubro->estadoPdm = $estado;
    $nuevo_rubro->guardar();
  }

  foreach($array_rubros_null as $rubro_e){
    $estado = 0;
    $rubro_oculto = _bbva_pdm_rubro_id_suip_by_tid($rubro_e);
    $nuevo_rubro = new \Fgo\Bo\RubroBo();
    $rubroBo = $nuevo_rubro->buscarPorIdSuip($rubro_oculto);
    if(!empty($rubroBo->idRubro)){
      $estado = 3;
      $rubroBo->estadoPdm = $estado;
      $rubroBo->guardar();
    }
  }
}
