<?php

function bbva_tarjeta_view_enviadas(){

  // Check if there is sorting request
  if(isset($_GET['order'])){
    $order = campanias_view_order($_GET['order']);
  }

  $sort = $_GET['sort'];

  // Cuando se reciben los filtros
  if ((isset($_GET['filter_id_message_plan'])) && ($_GET['filter_id_message_plan'] != '')) {
    $filtros["id_message_plan"] = $_GET['filter_id_message_plan'];
  }
  if ((isset($_GET['filter_dni'])) && ($_GET['filter_dni'] != '')) {
    $filtros["uid"] = $_GET['filter_dni'];
  }
  if ((isset($_GET['filter_enviado'])) && ($_GET['filter_enviado'] != '')) {
    $filtros["marca"] = $_GET['filter_enviado'];
  }
  $campanias = _get_campanias($order, $sort, $filtros,25);

  return bbva_tarjeta_create_campanias($campanias);
}

function bbva_tarjeta_view_enviadas_submit($form, &$form_state) {
  $form_state['filters']['id_message_plan'] = $form_state['values']['filter_id_message_plan'];
  $form_state['filters']['dni'] = $form_state['values']['filter_dni'];
  $form_state['filters']['enviado'] = $form_state['values']['filter_enviado'];
  $form_state['rebuild'] = TRUE;
}

function bbva_tarjeta_create_campanias($campanias){

  $header = array(
    array('data' => t('ID Campaña'),'field' => ' id_message_plan'),
    array('data' => t('DNI')),
    array('data' => t('Enviado'),'field' => 'marca'),
    array('data' => t('Fecha'),'field' => 'date_marca'),
  );

  $form = array();

  $form['#method'] = 'get';

  $filtro_enviado_opciones[""] = "Cualquiera";
  $filtro_enviado_opciones[0] = "No";
  $filtro_enviado_opciones[1] = "Si";

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buscar por:')
  );
  $form['filter']['filter_id_message_plan'] = array(
    '#type' => 'textfield',
    '#title' => t('Id Campaña/Message Plan:'),
    '#size' => 10,
    '#weight' => '0',
    '#default_value'=> $_GET['filter_id_message_plan']
  );
  $form['filter']['filter_dni'] = array(
    '#type' => 'textfield',
    '#title' => t('DNI: '),
    '#size' => 11,
    '#weight' => '1',
    '#default_value'=> $_GET['filter_dni']
  );
  $form['filter']['filter_enviado'] = array(
    '#type' => 'select',
    '#title' => t('Enviado: '),
    '#default_value' => '',
    '#options' => $filtro_enviado_opciones,
    '#weight' => '2',
    '#default_value'=> $_GET['filter_enviado'],
    '#attributes' => array('onchange' => 'this.form.submit();'),
  );
  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Buscar'),
    '#weight' => '3'
  );


  // Tomamos los datos
  foreach ($campanias as $row) {
    if (($row->uid != 0) && ($row->uid)) {
      $dni_view =  get_dni_by_uid($row->uid);
    }else{
      $dni_view =  "Anónimo";
    }

    if (($row->marca == 0) || ($row->marca == '')) {
      $marca = "No";
    }else{
      $marca = "Si";
    }

    if (($row->date_marca == 0) || ($row->date_marca == '')) {
      $date_sent = "-";
    }else{
      $date_sent = date('d-m-Y', strtotime($row->date_marca));
    }

    $data[] = array(
      array('data' => $row->id_message_plan, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $dni_view, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $marca, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $date_sent, 'align' => 'center', 'style' => 'font-size: 1em'),
      );
  }

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $data,
    '#empty' => t('No hay campañas por el momento')
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}
