<?php

function _get_campanias($order="id_message_plan", $sort="desc", $filtros=NULL, $limit=NULL){

  if (null === $order) {
    $order = "id_message_plan";
  }
  if (null === $sort) {
    $sort = "desc";
  }

  $query = db_select('bbva_campanias', 'bc');
  $query->fields('bc', array('uid', 'id_message_plan', 'marca', 'date_marca','hash'));

  if (is_array($filtros)) {
    if ($filtros["id_message_plan"]) {
      $query->condition('bc.id_message_plan', $filtros["id_message_plan"], '=');
    }
    if ($filtros["uid"]) {
      $query->join('field_data_field_profile_document_number', 'tdn', 'bc.uid = tdn.entity_id');
      $query->condition('tdn.field_profile_document_number_value', $filtros["uid"], '=');
    }
    if ($filtros["marca"] != '') {
      $query->condition('bc.marca', $filtros["marca"], '=');
    }
  }

  $query->orderBy($order, $sort);

  if ($limit != '') {
    $query = $query->extend('TableSort')->extend('PagerDefault')->limit($limit);
  }

  $results = $query->execute()->fetchAll();
  if ($results) {
    return $results;
  }else{
    return null;
  }
}


function _db_search_user_id_by_hash($hash) {
  $query = db_select('bbva_campanias', 'bc');
  $query = $query->fields('bc', array('uid','id', 'oferta_json'));
  $query = $query->condition('bc.hash', $hash);
  $query = $query->condition('bc.marca', BBVA_TARJETA_ESTADO_CONFIRMADA, '!=');
  $result = $query->execute()->fetchAll();

  return $result;
}

function bbva_tarjeta_db_burn_hash($hash) {
  $updated = db_update('bbva_campanias')
    ->fields(array(
      'marca' => BBVA_TARJETA_ESTADO_CONFIRMADA,
      'date_marca' => date('Y-m-d h:i:s')
    ))
    ->condition('hash', $hash)
    ->condition('marca', 0)
    ->execute();

  return $updated;
}

function bbva_tarjeta_db_insert_hash($uid, $hash, $id_message_plan, $oferta_json = null, $origen = null) {

  $json = unserialize($oferta_json);
  $fecha_vencimiento_oferta = null;

  if ($json != null && isset($json["respuesta"]->fechaVigencia) && $json["respuesta"]->fechaVigencia != null) {
    $date = str_replace('/', '-', $json["respuesta"]->fechaVigencia);
    $fecha_vencimiento_oferta = strtotime($date);
  }

  $id = db_insert('bbva_campanias')->fields(array(
    'uid' => $uid,
    'hash' => $hash,
    'id_message_plan' => $id_message_plan,
    'oferta_json' => $oferta_json,
    'origen' => $origen,
    'date_origen' => $origen != null ? strtotime(date("Y-m-d")) : 0,
    'vencimiento_oferta' => $fecha_vencimiento_oferta,
  ))->execute();

  return $id;
}

function _get_tarjetas_all() {
    $query = db_select('node', 'n');
    $query->join('field_data_field_tarjetas_estado', 'bt', 'n.nid = bt.entity_id');
    $query->addField('n', 'nid');
    $query->condition('n.type', 'bbva_tarjetas');
    $query->condition('bt.field_tarjetas_estado_value', '1');
    $nids = $query->execute()->fetchCol();

    $result = node_load_multiple($nids);

    if (isset($result)) { // Si existen tarjetas
      $tarjetas = create_tarjetas_json($result);

        return $tarjetas;
    } else {
        return array('tarjetas' => array());
    }
}

function bbva_tarjeta_db_delete() {

  $today = strtotime(date("Y-m-d"));
  $timestamp = date("Y-m-d 00:00:00",strtotime('-1 month',$today));

  $query = db_delete('bbva_campanias');
  $query = $query->condition('marca', 1);
  $query = $query->condition('date_marca', $timestamp, "<");
  $query = $query->isNotNull('date_marca');

  $result = $query->execute();

  return $result;
}


function _get_tarjetas_disenios()
{
    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_disimg_codigo_imagen', 'cod', 'n.nid = cod.entity_id');
    $query->leftJoin('field_data_field_disimg_img', 'img', 'n.nid = img.entity_id');
    $query->leftJoin('field_data_field_disimg_color_letra', 'co', 'n.nid = co.entity_id');
    $query->addField('cod', 'field_disimg_codigo_imagen_value', 'codigo_imagen');
    $query->addField('img', 'field_disimg_img_fid', 'img_fid');
    $query->addField('co', 'field_disimg_color_letra_rgb',' color_letra');
    $query->condition('n.type', 'disenio_tarjeta');
    $disenios = $query->execute()->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);

    return $disenios;
}
