<?php
define('BBVA_TARJETA_ERR_GENERICO', 'En este momento no podemos ejecutar esta operación.');

/**
*  Consulta un web service para sabes si el DNI es existe como situacion 9
*  @return NoClienteResponse
*/
function bbva_tarjeta_solicitar_tarjeta($data) {

  $fnet_host = variable_get('fnet.host');
  $uri = variable_get('fgo.service.clientes.uri');
  $http_timeout = variable_get('fgo.http.timeout');
  $url = $fnet_host . $uri;

  $dato = http_build_query($data, '', '&');

  $options = array(
    'method' => 'POST',
    'timeout' => $http_timeout,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );

  $url = $fnet_host . $uri . '?' . $dato;

  watchdog('debug', 'Envio, %paquete', array('%paquete' => $ $url));

  $response = drupal_http_request($url, $options);
  $parser = null;


  if (in_array( $response->code, array(200, 304))) {
	$parser = json_decode($response->data, true);
	watchdog('debug', 'Respueta Solic Tarjeta, %respuesta', array('%respuesta' => $parser));
	return $parser;
  }
  else if ($response->code == HTTP_REQUEST_TIMEOUT){
    watchdog('error', 'Time out');
    throw new Exception("Time out");
  }
  else {
    watchdog('error', 'Error desconocido');
    throw new Exception("Error desconocido");
  }
}


function create_tarjetas_json($data=NULL){

  global $base_url;

  foreach ($data as $key => $tarjeta) {

    if ($tarjeta->field_tarjetas_titulo_negritas['und'][0]['value']) {
      $titulo_negritas = $tarjeta->field_tarjetas_titulo_negritas['und'][0]['value'];
    }else{
      $titulo_negritas = null;
    }

    if ($tarjeta->field_tarjetas_imagen_tarjeta['und'][0]["fid"]) {
      $imagen_tarjeta = file_create_url($tarjeta->field_tarjetas_imagen_tarjeta['und'][0]["uri"]);
    }else{
      $imagen_tarjeta = null;
    }

    $caracteristicas_tarjeta = array();
    if (count($tarjeta->field_tarjetas_caracteristicas['und']) >= 1) {
      foreach ($tarjeta->field_tarjetas_caracteristicas['und'] as $value) {
        $caracteristicas_tarjeta[] = $value['value'];
      }
    }else{
      $caracteristicas_tarjeta = null;
    }

    if ($tarjeta->field_tarjetas_encabezado_boton['und'][0]['value']) {
      $encabezado_botones = $tarjeta->field_tarjetas_encabezado_boton['und'][0]['value'];
    }else{
      $encabezado_botones = null;
    }

    if ($tarjeta->field_tarjetas_campana_motor['und'][0]['value']) {
      $campana_motor = $tarjeta->field_tarjetas_campana_motor['und'][0]['value'];
    }else{
      $campana_motor = "default";
    }
    $tarjetas["content"][$key]["id_tarjeta"] = $key;
    $tarjetas["content"][$key]["titulo"] = $tarjeta->title;
    $tarjetas["content"][$key]["titulo_negritas"] = $titulo_negritas;
    $tarjetas["content"][$key]["descripcion"] = $tarjeta->field_tarjetas_descripcion['und'][0]['value'];
    $tarjetas["content"][$key]["caracteristicas"] = $caracteristicas_tarjeta;
    $tarjetas["content"][$key]["encabezado_boton"] = $encabezado_botones;
    $tarjetas["content"][$key]["imagen_tarjeta"] = $imagen_tarjeta;
    $tarjetas["content"][$key]["url_formulario"] = $base_url . '/solicitarTarjeta/'.$campana_motor;

    // sin login
    $tarjetas["content_sin_login"][$key]["id_tarjeta"] = $key;
    $tarjetas["content_sin_login"][$key]["titulo"] = $tarjeta->title;
    $tarjetas["content_sin_login"][$key]["titulo_negritas"] = $titulo_negritas;
    $tarjetas["content_sin_login"][$key]["descripcion"] = $tarjeta->field_tarjetas_descripcion['und'][0]['value'];
    $tarjetas["content_sin_login"][$key]["caracteristicas"] = $caracteristicas_tarjeta;
    $tarjetas["content_sin_login"][$key]["encabezado_boton"] = $encabezado_botones;
    $tarjetas["content_sin_login"][$key]["imagen_tarjeta"] = $imagen_tarjeta;

    $url_campana = variable_get('bbva_oferta_preaprobada_url');
    $tag_omniture = variable_get('bbva_oferta_preaprobada_tag');
    $params['camp'] = $campana_motor;
    $params['onsite'] = $tag_omniture;
    //$url_campana .= "?" . http_build_query($params);
    $url_campana .= $campana_motor;
    $tarjetas["content_sin_login"][$key]["url_formulario"] = $url_campana .variable_get('bbva_oferta_preaprobada_url_track');
  }

  $tarjetas["content"] = array_values($tarjetas["content"]);
  $tarjetas["content_sin_login"] = array_values($tarjetas["content_sin_login"]);

  return array("tarjetas" => $tarjetas['content'], "tarjetas_sin_login" => $tarjetas["content_sin_login"]);
}

function bbva_tarjetas_create_url_form($args){
  global $user;
  $campana_motor = $args;
 
  $url_campana = variable_get('bbva_oferta_preaprobada_url');
  $tag_omniture = variable_get('bbva_oferta_preaprobada_tag', FALSE);

  if($campana_motor != "default") {
    $params['camp'] = $campana_motor;
  }

  if($tag_omniture) {
    $params['onsite'] = $tag_omniture;
  }
 
  if ($user->uid != 0) {
    // Genero hash
    $hash = md5($user->uid . time());
    //$params['h'] = $hash; // El parametro h sirve para que el motor le complete los datos en el formulario
    $oferta_json = NULL;

    // Chequeo si tiene los datos necesarios para una oferta preaprobada
    $datos_usuario = bbva_tarjeta_datos_para_ofertapreaprobada($user->uid);
    // Si no tengo todos los datos los busco en Fnet
    if(!$datos_usuario['valido']) {
      $fnet_data = array(
        "nrocliente" => $user->name,
        "method" => "consultarDatosPersonales"
      );
      try {
        $fnet = new \Services\BBVA\Fnet();
        $fnet->setUri("/fnet/mod/fgo/NL-cliente.do");
        $fnet->setData($fnet_data);
        $datos_usuario_fnet = $fnet->request();
        if(!isset($datos_usuario_fnet->error)) {
          $datos_usuario = bbva_tarjeta_unir_datos_usuario($datos_usuario['data'], $datos_usuario_fnet->data);
          if($datos_usuario) {
            $datos_usuario = bbva_tarjeta_datos_para_ofertapreaprobada($user->uid);
          }
        }
      } catch (Exception $e) {}
    }

    if($datos_usuario['valido'] && variable_get('bbva_campanas_saltar_paso_1', FALSE)) {
      // Consulto a Fnet si tiene oferta preaprobada
      $datos_usuario['data']['captchaResponse'] = "";
      $datos_usuario['data']['origenOferta'] = BBVA_TARJETA_OP_ORIGEN;
      $datos_usuario['data']['fp'] = BBVA_TARJETA_OP_FP;
      $datos_usuario['data']['c'] = BBVA_TARJETA_OP_CANAL;
      try {
        $fnet = new \Services\BBVA\Fnet();
        $fnet->setUri("/fnet/mod/bbva/NL-VentaTC.do?method=consultarOferta");
        $fnet->setData($datos_usuario['data']);

        $i = 0;
        while($result = $fnet->request()) {
          $i++;
          if($i >= 4) {
            break;
          }
          if(!isset($result->error)) {
            if($result->data->respuesta->dictamen == "APROBADO") {
              // Si esta aprobada mandamos el parametro fop en reemplazo de hash.
              unset($params['h']);
              $params['fop'] = $hash;
              $oferta_json = bbva_tarjeta_respuesta_salto_paso1($datos_usuario['data'], $result->data);
            } elseif($result->data->respuesta->tipoMje == "mensaje-4") {
              // Si hay error en el motor vuelvo a intentarlo
              continue;
            }
          }
          break;
        }
      } catch (Exception $e) {}
    }
    bbva_tarjeta_db_insert_hash($user->uid, $hash, 0, $oferta_json);
  }

  $url_campana.="/".$params['camp'];
  
  hd($url_campana.variable_get('bbva_oferta_preaprobada_url_track'));
  header("Location: " . $url_campana.variable_get('bbva_oferta_preaprobada_url_track'));
}

function bbva_tarjeta_datos_para_ofertapreaprobada($uid) {
  $datos_completos = TRUE;
  $user_profile = user_load($uid);

  $document_type_altamira = @bbva_subscription_convert_tipo_doc_altamira($user_profile->field_profile_document_type['und'][0]['value'], $user_profile->field_profile_document_number['und'][0]['value'], $user_profile->field_profile_sex['und'][0]['value']);
  if ($document_type_altamira == NULL) {
    $document_type_altamira = '00';
  }

  @$fecha_nac_str = zerofill($user_profile->field_profile_day['und'][0]['value'], 2) . "/" . zerofill($user_profile->field_profile_month['und'][0]['value'], 2) . "/" . $user_profile->field_profile_year['und'][0]['value'];
  @$fecha_nac = DateTime::createFromFormat("d/m/Y", $fecha_nac_str);
  if($fecha_nac) {
    @$fecha_nac = $fecha_nac->format("d/m/Y");
  }

  $user_extra_data = FALSE;
  if(module_exists('bbva_usuarios')) {
    $user_extra_data = bbva_usuarios_db_datos_usuario($uid);
  }

  $nacionalidad = NULL;

  if($user_extra_data) {
    if($user_extra_data->nacionalidad) {
      $nacionalidad = (int) $user_extra_data->nacionalidad . "|";
      $nacionalidades_list = bbva_subscription_get_nationality_type_list();
      $user_nac_3c = str_pad($user_extra_data->nacionalidad, 3, '0', STR_PAD_LEFT);

      if(isset($nacionalidades_list[$user_nac_3c])) {
        $nacionalidad .= $nacionalidades_list[$user_nac_3c];
      }
    }
  }

  @$data = array(
    'nombre' => $user_profile->field_profile_nombre['und']['0']['safe_value'],
    'apellido' => $user_profile->field_profile_apellido['und']['0']['safe_value'],
    'tipoDoc' => $document_type_altamira,
    'nroDoc' => (int) $user_profile->field_profile_document_number['und'][0]['value'],
    'fechaNacimiento' => $fecha_nac,
    'nacionalidad' => $nacionalidad,
    'sexo' => $user_profile->field_profile_sex['und'][0]['value'],
    'tipoTel' => "CEL",
    'caracTel' => $user_profile->field_profile_area['und']['0']['value'],
    'nroTel' => $user_profile->field_profile_celular['und']['0']['value'],
    'email' => $user_profile->mail
  );

  if(empty($data['nombre'])) {
    $datos_completos = FALSE;
  }

  if(empty($data['apellido'])) {
    $datos_completos = FALSE;
  }

  if($data['nroDoc'] < 1) {
    $datos_completos = FALSE;
  }

  if(!$data['fechaNacimiento']) {
    $datos_completos = FALSE;
  }

  if(empty($data['sexo'])) {
    $datos_completos = FALSE;
  }

  if(!bbva_subscription_validar_longitud_area_numero($data['caracTel'], $data['nroTel'])) {
    $datos_completos = FALSE;
  }

  if(!valid_email_address($data['email'])) {
    $datos_completos = FALSE;
  }

  if(empty($data['nacionalidad'])) {
    $datos_completos = FALSE;
  }

  $result = array(
    "data" => $data,
    "valido" => $datos_completos
  );

  return $result;
}

function bbva_tarjeta_respuesta_salto_paso1($datos_usuario, $response, $origen_oferta = BBVA_TARJETA_OP_ORIGEN) {

  $cliente = $datos_usuario;
  unset($cliente['tipoTel']);
  unset($cliente['caracTel']);
  unset($cliente['nroTel']);
  $cliente['telefono'] = array(
    'tipoTel' => $datos_usuario['tipoTel'],
    'caracteristicaTel' => $datos_usuario['caracTel'],
    'nroTel' => $datos_usuario['nroTel']
  );

  $json = array(
    'retries' => 0,
    'origenOferta' => isset($origen_oferta) ? $origen_oferta : null,
    'cliente' => isset($cliente) ? $cliente : null,
    'respuesta' => isset($response->respuesta) ? $response->respuesta : null,
    'randomkey' => isset($response->randomkey) ? $response->randomkey : null,
    'nroTramite' => isset($response->nroTramite) ? $response->nroTramite : null,
  );

  $json = serialize($json);

  return $json;
}

function get_dni_by_uid($id){
  $user_profile = user_load($id);
  $profile_dni = $user_profile->field_profile_document_number['und']['0']['safe_value'];

  if ($profile_dni) {
    return $profile_dni;
  }else{
    return "Anónimo";
  }
}

function get_type_document_by_uid($id){
  $user_profile = user_load($id);
  $profile_document_type = $user_profile->field_profile_document_type['und']['0']['value'];

  if ($profile_document_type) {
    return $profile_document_type;
  }else{
    return "Anónimo";
  }
}

function campanias_view_order($order = NULL){
  if($order){
    switch($order){
     case 'ID Campaña':
       $order = 'id_campania';
       break;
     case 'Enviado':
       $order = 'marca';
       break;
     case 'Fecha':
       $order = 'date_marca';
       break;
     default:
       $order = 'id_campania';
    }
  } else {
   // Default order
   $order = 'id_campania';
  }

  return $order;
}

function bbva_tarjeta_unir_datos_usuario($datos_usuario, $datos_usuario_fnet) {
  $datos_usuario_fnet = (array) $datos_usuario_fnet;
  $data_update = array();
  if(!$datos_usuario['nombre'] && $datos_usuario_fnet['nombre']) {
    $data_update['field_profile_nombre']['und'][0]['value'] = $datos_usuario_fnet['nombre'];
  }
  if(!isset($datos_usuario['apellido']) && $datos_usuario_fnet['apellido']) {
    $data_update['field_profile_apellido']['und'][0]['value'] = $datos_usuario_fnet['apellido'];
  }
  if(!isset($datos_usuario['tipoDoc']) && $datos_usuario_fnet['tipoDoc']) {
    $data_update['field_profile_document_type']['und'][0]['value'] = $datos_usuario_fnet['tipoDoc'];
  }
  if(!isset($datos_usuario['nroDoc']) && $datos_usuario_fnet['nroDoc']) {
    $data_update['field_profile_document_number']['und'][0]['value'] = $datos_usuario_fnet['nroDoc'];
  }
  if(!DateTime::createFromFormat('d/m/Y', $datos_usuario['fechaNacimiento']) && DateTime::createFromFormat('Ymd', $datos_usuario_fnet['fechaNacimiento'])) {
    $fecha = DateTime::createFromFormat('Ymd', $datos_usuario_fnet['fechaNacimiento']);
    $data_update['field_profile_day']['und'][0]['value'] = $fecha->format('d');
    $data_update['field_profile_month']['und'][0]['value'] = $fecha->format('m');
    $data_update['field_profile_year']['und'][0]['value'] = $fecha->format('Y');
  }
  if(!$datos_usuario['sexo'] && $datos_usuario_fnet['sexo']) {
    $data_update['field_profile_sex']['und'][0]['value'] = $datos_usuario_fnet['sexo'];
  }
  if(!$datos_usuario['caracTel'] && $datos_usuario_fnet['prefijoTelefono']) {
    $data_update['field_profile_area']['und'][0]['value'] = $datos_usuario_fnet['prefijoTelefono'];
  }
  if(!$datos_usuario['nroTel'] && $datos_usuario_fnet['numeroTelefono']) {
    $data_update['field_profile_celular']['und'][0]['value'] = $datos_usuario_fnet['caracteristicaTelefono'] . $datos_usuario_fnet['numeroTelefono'];
  }
  if(!$datos_usuario['email'] && $datos_usuario_fnet['email']) {
    $data_update['mail'] = $datos_usuario_fnet['email'];
    $data_update['field_profile_email']['und'][0]['value'] = $datos_usuario_fnet['email'];
  }
  if(!$datos_usuario['nacionalidad'] && $datos_usuario_fnet['codNacionalidad']) {
    $extra_data_update['nacionalidad'] = $datos_usuario_fnet['codNacionalidad'];
  }


  if(!empty($data_update) || !empty($extra_data_update)) {
    global $user;
    if(!empty($data_update)) {
      $usuario_existente = user_load($user->uid);
      user_save($usuario_existente, $data_update);
    }
    if(!empty($extra_data_update)) {
      bbva_usuarios_db_actualizar_usuario($user->uid, $extra_data_update);
    }

    return TRUE;
  }

  return FALSE;
}

function bbva_tarjeta_datos_paso_1($hash) {
  $data = array();

  $datos_bases_comerciales = bbva_bases_comerciales_db_datos_hash_campania($hash);

  if(count($datos_bases_comerciales) < 1) {
    return $data;
  }

  $fecha_nacimiento = new DateTime();
  $fecha_nacimiento->setTimestamp($datos_bases_comerciales->fecha_nacimiento);

   $nacionalidades_list = bbva_subscription_get_nationality_type_list();
    if(isset($nacionalidades_list[$datos_bases_comerciales->nacionalidad])) {
     $datos_bases_comerciales->nacionalidad = (int) $datos_bases_comerciales->nacionalidad . "|" . $nacionalidades_list[$datos_bases_comerciales->nacionalidad];
    }else{
      $datos_bases_comerciales->nacionalidad = null;
    }

  $data = array(
    'nombre' => $datos_bases_comerciales->nombre,
    'apellido' => $datos_bases_comerciales->apellido,
    'tipoDoc' => $datos_bases_comerciales->tipo_documento,
    'nroDoc' => $datos_bases_comerciales->numero_documento,
    'nacionalidad' => $datos_bases_comerciales->nacionalidad,
    'fechaNacimiento' => $fecha_nacimiento->format("d/m/Y"),
    'sexo' => $datos_bases_comerciales->sexo,
    'caracteristicaTel' => $datos_bases_comerciales->caracteristica_telefono,
    'nroTelefono' => $datos_bases_comerciales->numero_telefono,
    'email' => $datos_bases_comerciales->email
  );

  return $data;
  }
  

function bbva_tarjetas_cache_set_disenios(){

  $listado_desenios = _get_tarjetas_disenios();

  foreach ($listado_desenios as $key => $value) {
    
    $datosTarjeta = (Object)$listado_desenios[$key];
    $file = file_load($datosTarjeta->img_fid);
    $url_imagen_tarjeta = file_create_url($file->uri);

    $listado_desenios[$key]["url_imagen"] = $url_imagen_tarjeta;
  }

  cache_set('jsonDisenioTargetas', $listado_desenios,'cache_api',CACHE_PERMANENT);
  drupal_set_message(t("Diseños Tarjetas Cacheado"), 'status', FALSE);
}


function bbva_tarjeta_get_datos_tarjetas($idimagen) {
  $cacheDisenioTarjetas = cache_get("jsonDisenioTargetas", "cache_api");
  $mapaDiseniosTarjetas = isset($cacheDisenioTarjetas->data) ? $cacheDisenioTarjetas->data : null;
  $result = null;
  if (!empty($mapaDiseniosTarjetas) && array_key_exists($idimagen, $mapaDiseniosTarjetas)) {

    $datosTarjeta = (Object)$mapaDiseniosTarjetas[$idimagen];
    $result = array("id" => $idimagen, "colorLetra"=>$datosTarjeta->color_letra, "urlImagen"=>$datosTarjeta->url_imagen);

  } 
  return $result;
}

function bbva_tarjeta_motortc_validar_input($data) {
  $valido = TRUE;

  if(empty($data['nombre'])) {
    $valido = FALSE;
  }

  if(empty($data['apellido'])) {
    $valido = FALSE;
  }

  if(empty($data['tipoDoc'])) {
    $valido = FALSE;
  }

  if($data['nroDoc'] < 1) {
    $valido = FALSE;
  }

  if(empty($data['fechaNacimiento'])) {
    $valido = FALSE;
  }

  if(empty($data['nacionalidad'])) {
    $valido = FALSE;
  }

  if(empty($data['sexo'])) {
    $valido = FALSE;
  }

  if(empty($data['tipoTel'])) {
    $valido = FALSE;
  }

  if(!bbva_subscription_validar_longitud_area_numero($data['caracTel'], $data['nroTel'])) {
    $valido = FALSE;
  }

  if(!valid_email_address($data['email'])) {
    $valido = FALSE;
  }

  if(empty($data['origenOferta'])) {
    $valido = FALSE;
  }

  if(empty($data['fp'])) {
    $valido = FALSE;
  }

  if(empty($data['c'])) {
    $valido = FALSE;
  }

  return $valido;
}