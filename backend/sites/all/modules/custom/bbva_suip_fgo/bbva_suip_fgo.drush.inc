<?php
/**
 * Implements COMMANDFILE_drush_command().
 */
function bbva_suip_fgo_drush_command() {
  $items = array();

  $items['import-shopping-suip'] = array(
    'description' => 'Comando para importar los shopping de suip.',
    'aliases' => array('isps'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  return $items;
}

/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_bbva_suip_fgo_import_shopping_suip() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Import Suip Shopping'),
    'init_message' => t('Import Suip Shopping is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => '_import_shopping_suip_finished',
    'file' => drupal_get_path('module', 'bbva_suip_fgo') . '/import_shopping.batch.inc',
  );


    $batch['operations'][] = array('_import_shopping', array(''));
  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}
