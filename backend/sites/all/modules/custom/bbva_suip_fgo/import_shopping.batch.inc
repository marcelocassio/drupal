<?php
  
  function _import_shopping() {
   
    echoline("Inicio Importando Shopping", true, true);

    $shoppingFgo = _get_shopping_ids();
    $localizationId = _get_localization_entity_id();
    $linesFileShop = _get_file('/prod/jp00/VuelcoSuip/shopping.txt');
    $localshop =_get_localization_ids_shop();
 
    foreach ($linesFileShop as $key => $value) {
      try {
        $valor = explode("\\tc", $value);
        if (isset($shoppingFgo[$valor[0]])) {
          $ewrapperFile = entity_metadata_wrapper('shopping', $shoppingFgo[$valor[0]]);
          unset($shoppingFgo[$valor[0]]);
        }
        else {
          $values = array('type' => 'shopping', 'status' => 1, 'comment' => 1, 'promote' => 0,);
          $entity = entity_create('shopping', $values);
          $ewrapperFile = entity_metadata_wrapper('shopping', $entity);
        }

        // Se actualiza o se crea el shopping
        $ewrapperFile->field_shopping_id->set($valor[0]);
        $ewrapperFile->title->set(iconv('Windows-1252', 'UTF-8', $valor[2]));
        $idLocal = $localizationId[$valor[1]]['id'];
        $ewrapperFile->field_shopping_local->set($idLocal);
        $ewrapperFile->save();

        $result = $localshop[$valor[0]];
        if (isset($result)) {
          $ewrapperLocalizationShop = entity_metadata_wrapper('localization', $idLocal);

          foreach ($result as $key_n => $id_localization_drupal) {
     
            $ewrapperLocalization = entity_metadata_wrapper('localization', $id_localization_drupal);
            if ($ewrapperLocalizationShop->field_localization_location->value()) {
              $ewrapperLocalization->field_localization_location->set($ewrapperLocalizationShop->field_localization_location->value());
            }
            
            if ($ewrapperLocalizationShop->field_localization_distric->value()) {
              $ewrapperLocalization->field_localization_distric->set($ewrapperLocalizationShop->field_localization_distric->value());
            }
            
            if ($ewrapperLocalizationShop->field_localization_locality->value()) {
              $ewrapperLocalization->field_localization_locality->set($ewrapperLocalizationShop->field_localization_locality->value());
            }
            
            if ($ewrapperLocalizationShop->field_localization_link->value()) {
              $ewrapperLocalization->field_localization_link->set($ewrapperLocalizationShop->field_localization_link->value());
            }
            if ($ewrapperLocalizationShop->field_localization_street->value()) {
              $ewrapperLocalization->field_localization_street->set($ewrapperLocalizationShop->field_localization_street->value());
            }
            if ($ewrapperLocalizationShop->field_localization_number->value()) {
              $ewrapperLocalization->field_localization_number->set($ewrapperLocalizationShop->field_localization_number->value());
            }
            if ($ewrapperLocalizationShop->field_localization_other_data->value()) {
              $ewrapperLocalization->field_localization_other_data->set($ewrapperLocalizationShop->field_localization_other_data->value());
            }
            if ($ewrapperLocalizationShop->field_localization_corner->value() ) {
              $ewrapperLocalization->field_localization_corner->set($ewrapperLocalizationShop->field_localization_corner->value());
            }
            
            $ewrapperLocalization->save();
          }
        }
      }
      catch(Exception $e) {
        echoline($e->getMessage(), true, true);
        echoline('Excepción capturada: '.$valor[0], true, true);
      }
    }
    echoline("Fin Importando Shopping", true, true);
  }


  function _get_shopping_ids() {

    $query = db_select('eck_shopping', 'eb', array());
    $query->leftJoin('field_data_field_shopping_id', 'fshid', 'fshid.entity_id = eb.id');
    $query->fields('fshid', array('field_shopping_id_value'), 'idSuip');
    $query->fields('eb', array('id'));
    $result = $query->execute()->fetchAll(\PDO::FETCH_KEY_PAIR);

    return $result ? $result : array();
  }

  function _get_localization_ids_shop() {
  
    $idshoplocal = array();
    $query = db_select('field_data_field_localization_id_shopping', 'fspid', array());
    $query->fields('fspid', array('entity_id','field_localization_id_shopping_value'));
    $result = $query->execute()->fetchAll();

    if (isset($result)) {
      foreach ($result as $key => $record) {
        $idshoplocal[$record->field_localization_id_shopping_value][] = $record->entity_id;
        unset($result[$key]);
      }
    }

    return $idshoplocal;
  }


  /**
   * The batch finish handler.
   */
  function _import_shopping_suip_finished($success, $results, $operations) {
    if ($success) {
      drupal_set_message(t('Importa Batch finish'.date('Y-m-d H:i:s')));
    }
    else {
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE)
      ));
      drupal_set_message($message, 'error');
    }
  }
