<?php
  
  function _get_guia_uso(){
    
    $guia_uso = views_get_view_result('guia_uso', 'page');
    
    foreach ($guia_uso as $key => $value) {

      $guia[$key]['imagen'] = bbva_api_create_path($value->field_field_guia_uso_imagen_1[0]['raw']['fid'], '360x570');
      $guia[$key]['link'] = isset($value->field_field_guia_uso_link[0]) ? $value->field_field_guia_uso_link[0]['raw']['value'] : null;
      $guia[$key]['close'] = $value->field_field_guia_uso_close[0]['rendered']['#markup'];
        
      $buttons = $value->field_field_guia_uso_buttons;
      $buttons_guia = array();

      foreach ($buttons as $keyButtons => $button) {
         $buttone= entity_load_single('field_collection_item', $button['raw']['value']);

         if (!empty($buttone->field_buttons_texto['und'][0]['value'])) {
          $buttons_guia[$keyButtons]['color'] = $buttone->field_buttons_color['und'][0]['rgb'];
          $buttons_guia[$keyButtons]['texto'] = $buttone->field_buttons_texto['und'][0]['value'];
          $buttons_guia[$keyButtons]['call_to_action'] = _get_call_to_acction_api($buttone->field_call_to_action['und'][0]['value']);
          }
        }

      $guia[$key]['buttons'] = $buttons_guia;
    }
       
    return array("items" => $guia);
  }
