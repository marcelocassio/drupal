(function($) {

   function manageRegionSlideShowMarginTop(){
		if (document.documentElement.clientWidth < 961){
			var dropdownContent = document.getElementById('dropdown-content');
			var newMarginTop = 0;
			if (dropdownContent != null) {
				var newMarginTop = dropdownContent.clientHeight;
			}
			$('.region-slideshow').css('margin-top', newMarginTop);
		} else {
			$('.region-slideshow').css('margin-top',0);
		}
	}
	
  function fix_preferences() {
  	var preference_field_set = $('.group-preference').clone();
  	$('.group-preference').remove();
  	$(preference_field_set).appendTo('.group-profile-top > .fieldset-wrapper');
  }


  Drupal.behaviors.bbva_profile = {
    attach: function (context, settings) {
    	//fix_preferences();
		manageRegionSlideShowMarginTop();
    }
  }

  $(document).ready(function() {

    var description = $('#change-pwd-page-form .description');
    $('#change-pwd-page-form .description').remove();
    $(description).appendTo('#change-pwd-page-form form-item-pass-pass1');

  });

  
})(jQuery);