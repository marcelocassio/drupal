<?php


define('PROFILE_ERR_GENERICO', 'En este momento no podemos ejecutar esta operación.');
define('PROFILE_MSG_RECOVERY_OK', 'En breve te enviaremos una nueva clave a tu celular.');
define('PROFILE_MSG_CLAVE_ENVIADA', 'Su nueva clave es : ');

/*
 * Formulario para dar de baja una cuenta.
 */
function bbva_profile_baja_form($form, &$form_state) {

  $options = bbva_profile_get_subscription_motive_list() ;

  $form['title'] = array (
	'#type' => 'markup',
	'#title' => t('Informacion para el cliente'),
	'#markup' => '<h3>Darse de baja</h3>',
	'#weight' => 0,
  );

  $form['description'] = array (
	'#type' => 'markup',
	'#title' => t('Informacion para el cliente'),
	'#markup' => '<p>Recuerde que si es titular de TC los adicionales no disfrutaran de beneficios exclusivos</p>',
	'#weight' => 1,
  );

  $form['group_botton_baja'] = array(
	'#type' => 'fieldset',
	'#title' => t('Group Bottom'),
	'#collapsible' => TRUE,
    '#weight' => 2,
    '#attributes' => array(
        'class' => array('group-botton-baja'),
    ),
  );
  $form['group_botton_baja']['motivos'] = array(
    '#title' => t('Motivos de la baja'),
    '#type' => 'select',
    '#maxlength' => 3,
    '#weight' => 2,
    '#options' => $options,
  );

  $form['group_botton_baja']['submit'] = array(
    '#value' => t('Darse de baja'),
    '#type' => 'submit',
    '#weight' => 4,
    '#attributes' => array(
      'class' => array('ctools-bbva-profile-modal'),
    ),
  );

  $form['#submit'][] = 'bbva_profile_do_delete_subscription';

  return $form;
}


function bbva_profile_do_delete_subscription(&$form, &$form_state) {

  try {
    $motivo_selected = $form_state['values']['motivos'];
    if ($motivo_selected == 0) {
      form_set_error('motivos','Seleccione un motivo');
    } else {
      global $user;
  		$altamira_number = isset($user->name) ? $user->name : null;
  		if (is_numeric($altamira_number)) {
        if (module_exists('bbva_usuarios')) {
          $usuario = new \BBVA\Usuario($user->uid, true);
          $objDateTime = new DateTime('NOW');
          $modificaciones = array(
            "field_profile_delete_motive" => array("und" => array(0 => array("target_id" => $motivo_selected))),
            "field_profile_fecha_baja" => array("und" => array(0 => array("value" => $objDateTime->format('Y-m-d H:i:s')))),
          );
          $usuario->modificar($modificaciones);
          $usuario->guardar();
          $suscripcionResponse = $usuario->desuscribir();
          bbva_autentificacion_borrar_vinculaciones($user->uid);
          drupal_goto('user/logout');
        } else {
          $suscripcionResponse = bbva_subscription_delete($altamira_number);
          if ($suscripcionResponse->transaccionProcesada) {
            _update_user_profile_delete_motive($altamira_number, $motivo_selected);
            bbva_login_set_download_app(FALSE);
            drupal_goto('user/logout');
          } else {
            form_set_error('',PROFILE_ERR_GENERICO);
          }
        }

      }

    }
  } catch (Exception $e) {
    form_set_error('', PROFILE_ERR_GENERICO);
  }

}

/*
 * Formulario para recuperar pass via sms
 */
function bbva_profile_pass_recovery_form($form, &$form_state){

  $documentTypeKeyValue = bbva_login_get_document_type_list();

  $form['title'] = array(

	'#markup' => '<div id="label-reseteo-nocliente">Recuperaci&oacute;n de Contrase&ntilde;a</div>',
	'#weight' => -3,
  );

  $form['group_recup_body'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fields'),
      '#collapsible' => TRUE,
      '#weight' => 4,
      '#attributes' => array(
        'class' => array('group-recup-body'),
      ),
  );

  $form['group_recup_body']['document_type'] = array(
    '#title' => t('Tipo'),
    '#type' => 'select',
    '#weight' => -2,
    '#options' => $documentTypeKeyValue,
  );

  $form['group_recup_body']['number'] = array(
    '#title' => t('Numero'),
    '#type' => 'textfield',
    '#maxlength' => 15,
    '#size' => 17,
    '#weight' => -1,
  );

  $form['group_footer'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fields'),
      '#collapsible' => TRUE,
      '#weight' => 10,
      '#attributes' => array(
        'class' => array('group-footer'),
      ),
  );

  $form['group_footer']['salir-recupero'] = array(
	'#markup' => '<a class="recuperar-salir trigger-margin-region-slideshow">Salir</a>',
  );

  $form['group_footer']['submit'] = array(
    '#value' => t('Recuperar'),
    '#type' => 'submit',
    '#weight' => 1,
    '#ajax' => array(
      'callback' => 'bbva_profile_recovery_validate',
      'wrapper' => 'bbva-profile-pass-recovery-form',
      'effect' => 'fade',
    ),
  );

  $form['login_error'] = array(
	  '#markup' => '<div class="login_error"></div>',
	  '#weight' => 13,
	);

  return $form;
}

/*
 * Formulario de parametros para password
 */
function bbva_profile_pass_config_form($form, &$form_state){

  $options = array(00 => t('DNI'), 01 => t('CI'));
  $chars_especial = array(t('No'),t('Si'));
  $chars_number = array(0 => t('No'),1 => t('Si'));
  $chars_text = array(0 => t('No'),1 => t('Si'));

  $form['chars'] = array(
    '#title' => t('Cantidad minima de caracteres'),
    '#type' => 'textfield',
    '#maxlength' => 2,
    '#size' => 2,
    '#weight' => -1,
    '#default_value' => variable_get('config_chars', ''),
  );

  $form['chars_max'] = array(
    '#title' => t('Cantidad maxima de caracteres'),
    '#type' => 'textfield',
    '#maxlength' => 2,
    '#size' => 2,
    '#weight' => -1,
    '#default_value' => variable_get('config_chars_max', ''),
  );

  $form['chars_special'] = array(
    '#type' => 'radios',
    '#title' => t('Permite caracteres especiales ?'),
    '#options' => $chars_especial,
    '#default_value' => variable_get('config_chars_special', ''),
  );

  $form['chars_number'] = array(
    '#type' => 'radios',
    '#title' => t('Permite caracteres numericos ?'),
    '#options' => $chars_number,
    '#default_value' => variable_get('config_chars_number', ''),
  );

  $form['chars_text'] = array(
    '#type' => 'radios',
    '#title' => t('Permite texto ?'),
    '#options' => $chars_text,
    '#default_value' => variable_get('config_chars_text', ''),
  );

  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#weight' => 1,
  );

  return $form;
}

function bbva_profile_recovery_validate(&$form, &$form_state){

  $html_salir = '<div id="bbva-profile-pass-recovery-form">';
  $html_salir .= '  <span>'.PROFILE_MSG_RECOVERY_OK.'</span>';
  $html_salir .= '  <a id="recovery-continuar" class="trigger-margin-region-slideshow" href="#">Continuar</a>';
  $html_salir .= '</div>';

  $hay_error = false;
  $error_message = "";
  if (empty($form_state['values']['number'])) {
    $error_message = t('Debe completar el número de documento');
    form_set_error('number', $error_message);
    $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
    $hay_error = true;
  } else if (!is_numeric($form_state['values']['number'])) {
    $error_message = t('Ingrese solo números');
    form_set_error('number', $error_message);
    $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
    $hay_error = true;
  }


  // marcar clave autogenerads
  if (!$hay_error) {
    try {

  		$document_type = $form_state['values']['document_type'];
  		$document_number = $form_state['values']['number'];

  		$noClienteResponse = bbva_login_existe_no_cliente($document_type, $document_number);
      $altamira_number = $noClienteResponse->numeroDeCliente;
  		if ($altamira_number != '0') {
        // Si es cliente y no existe un usuario
        // Creamos un usuario de todas formas para que puedar operar.
        if (!_exits_user($altamira_number)) {
          _createUser($altamira_number);
        }

  			$suscripcionResponse = bbva_subscription_query($document_type, $document_number);
  			if ($suscripcionResponse->esta_suscripto()) {
  				$password = user_password(variable_get('config_chars'));

  				$texto = "Su nueva clave es : " . $password;
  				$smsRequest = new SmsRequest();
  				$smsRequest->numeroDeCelular = $suscripcionResponse->codigoDeArea . $suscripcionResponse->numeroDeCelular;
  				$smsRequest->codigoDeOperador = $suscripcionResponse->codigoDeOperador;
  				$smsRequest->texto = $texto;
  				bbva_subscription_send_sms($smsRequest);

				  _update_user_only_pass($noClienteResponse->numeroDeCliente, $password);
  				_require_user_change_pass($noClienteResponse->numeroDeCliente);
  			}
  			$mensaje = $html_salir;
  			return $mensaje;
  		} else {
  			// Si pasan todas, retornar mensaje ok
  			$mensaje = $html_salir;
  			return $mensaje;
  		}
	  } catch (Exception $e) {
		  $error_message = t(PROFILE_ERR_GENERICO);
		  form_set_error('number', $error_message);
		  $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
		  return $form;
	  }
  } else {
	   return $form;
  }

}


function bbva_profile_pass_config_form_submit($form, &$form_state){
  variable_set('config_chars', $form_state['values']['chars']);
  variable_set('config_chars_max', $form_state['values']['chars_max']);
  variable_set('config_chars_special', $form_state['values']['chars_special']);
  variable_set('config_chars_number', $form_state['values']['chars_number']);
  variable_set('config_chars_text', $form_state['values']['chars_text']);
}

