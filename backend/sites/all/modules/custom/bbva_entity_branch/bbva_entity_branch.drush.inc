<?php

/*
 * Implements hook_drush_command().
 */
function bbva_entity_branch_drush_command() {

  $items['vuelco-branch'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('sucursalesImport'),
        'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,

  );

  $items['delete-branch'] = array(
    'description' => 'Elimina las sucursales de drupal',
    'aliases' => array('deleteBranch'),
  );

  $items['vuelco-localization'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('localizationImport'),
        'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,

  );
  $items['delete-localization'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('localizationDelete'),
  );
  $items['s-prueba'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('sprueba'),
  );
  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_entity_branch_vuelco_branch() {
  import_branch_suip();
}

function drush_bbva_entity_branch_delete_branch() {
  _delete_branchs();
}

function drush_bbva_entity_branch_vuelco_localization() {
  _import_localization();
}

function drush_bbva_entity_branch_delete_localization() {
  _delete_localization();
}

function drush_bbva_entity_branch_s_prueba() {
  s_prueba();
}
