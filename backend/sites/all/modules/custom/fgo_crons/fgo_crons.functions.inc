<?php
/*Vouchers Limpieza*/
function fgo_crons_cupones_limpieza() {
  try {
    $cupones = fgo_crons_borrar_entidad_cupon();
    foreach ($cupones as $cupon) {
      fgo_crons_borrar_vouchers($cupon);
    }
  } catch(\Exception $e) {}
}

function fgo_crons_borrar_entidad_cupon() {
  $fecha_limite = time() - FGO_CRONS_DIAS_ANTIGUEDAD * 24 * 60 * 60;

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'coupon_promo')
    ->entityCondition('bundle', 'coupon_promo')
    ->fieldCondition('field_coupon_promo_date_end', 'value', $fecha_limite, '<');

  $result = $query->execute();

  $cupones = array_keys($result['coupon_promo']);

  entity_delete_multiple('coupon_promo', $cupones);

  return $cupones;
}

function fgo_crons_borrar_vouchers($cupon) {
  db_delete('bbva_vouchers')
    ->condition('id_cupon', $cupon)
    ->execute();
}

/*Sorteos Participantes Limpieza*/
function fgo_crons_sorteos_participantes_limpieza() {
  try {
    $participantes = fgo_crons_obtener_participantes_sorteos();
    foreach ($participantes as $participante) {
      fgo_crons_borrar_participantes_sorteos($participante);
    }
  } catch(\Exception $e) {}
}

function fgo_crons_obtener_participantes_sorteos(){
  $fecha_limite = time() - FGO_CRONS_DIAS_ANTIGUEDAD * 24 * 60 * 60;

  $query = db_select('bbva_sorteos_participantes', 'bus')
      ->fields('bus', array('id'))
      ->condition('bus.datetime_participate', $fecha_limite, '<');
  $query->orderBy('id', 'DESC');
  $result = $query->execute()->fetchAll(PDO::FETCH_COLUMN);

  return $result;
}

function fgo_crons_borrar_participantes_sorteos($participante) {
  db_delete('bbva_sorteos_participantes')
    ->condition('id', $participante)
    ->execute();
}

/*Sorteos Limpieza*/
function fgo_crons_sorteos_limpieza() {
  try {
    $fecha_limite = time() - FGO_CRONS_DIAS_ANTIGUEDAD * 24 * 60 * 60;
    $fecha_limite_date = date('Y-m-d', $fecha_limite);

    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'experience')
          ->fieldCondition('field_experience_fechas_vigencia', 'value', $fecha_limite_date, '<=')
          ->fieldCondition('field_experience_es_sorteo', 'value', '1', '=');

    $result = $query->execute();

    if (isset($result['node'])) {
      $sorteos = array_keys($result['node']);
      node_delete_multiple($sorteos);
    }

  } catch(\Exception $e) {}
}
