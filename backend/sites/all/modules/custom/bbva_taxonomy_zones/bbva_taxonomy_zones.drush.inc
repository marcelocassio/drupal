<?php

/*
 * Implements hook_drush_command().
 */
function bbva_taxonomy_zones_drush_command() {

  $items['zones-import'] = array(
    'description' => 'Realiza el vuelco de los rubros en suip a drupal',
    'aliases' => array('zonesImport'),
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_taxonomy_zones_zones_import() {
  import_zones_suip();
  drupal_set_message("Import zonas  echo", 'status', FALSE);
}