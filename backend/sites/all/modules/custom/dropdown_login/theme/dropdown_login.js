(function($) {

  var SLEEP_TIME_PER_SECOND = 120;

  function urlify(text) {
	var urlRegex = /(https?:\/\/[^\s]+)/g;
	return text.replace(urlRegex, function(url) {
		return '<a href="' + url + '">' + url + '</a>';
	})
  }

  function clearNotificationPanel() {
	$('.notifications-message ul').empty();
  }

  function updateNotificacionCount() {
    $url_notification = $('#url_notification').val();
	$.ajax({
		method: "POST",
		url: $url_notification,
		async: true,
		dataType: "json",
		success:  function(respuesta) {
			if (respuesta.codigo == 0) {
				$('.bbva-cant-mensajes').html(respuesta.cantidadMensajesSinLeer);
			}
		},
		beforeSend:function(){},
		error:function(jqXHR, textStatus, errorThrown) {
		}
	});
  }

  function confirmMessageNotificacion(idMessage) {
    var url_notification_confirm = $('#url_notification_confirm_read').val();
	var cantidadMensajes = parseInt($('.bbva-cant-mensajes').html()) - 1;
	$('.bbva-cant-mensajes').html(cantidadMensajes);

	$.ajax({
		method: "POST",
		url: url_notification_confirm,
		async: true,
		dataType: "json",
		data: { id_msg: idMessage},
		success:  function(respuesta) {
		},
		beforeSend:function(){},
			error:function(jqXHR, textStatus, errorThrown) {
		}
	});
  }

  function updateNotificacion() {
    $url_notification = $('#url_notification').val();
    clearNotificationPanel();
	$('.notifications-message ul').html('<a>Cargando....</a>');
	$.ajax({
		method: "POST",
		url: $url_notification,
		async: true,
		dataType: "json",
		success:  function(respuesta) {
			var item = null;
			clearNotificationPanel();
			if (respuesta.codigo == 0) {

				$('.bbva-cant-mensajes').html(respuesta.cantidadMensajesSinLeer);
					if (respuesta.cantidadMensajesSinLeer > 0) {
						$.each(respuesta.mensajes, function(index, value) {
						var mensaje = urlify(value.texto);
						var idMenssage = value.id;
						var leido = value.leido;
						if (leido) {
							item = '<li id="' +idMenssage+ '" class="notification-item notification-item-read"><div class="logo-content"><img class="logo-icon" src="sites/all/themes/boilerplate/images/logo_francesgo_chico.png"></img></div><div class="notification-text"><p>'+mensaje+'</p></div></li>';
						} else {
							item = '<li id="' +idMenssage+ '" class="notification-item notification-item-not-read"><div class="logo-content"><img class="logo-icon" src="sites/all/themes/boilerplate/images/logo_francesgo_chico.png"></img></div><div class="notification-text"><p>'+mensaje+'</p></div></li>';
						}
						$(item).appendTo('.notifications-message ul');
					});
					$('#bbva-cant-mensajes').html(respuesta.cantidadMensajesSinLeer);
						$('.notification-item-not-read').click(function () {
						var idMessage = $(this).attr('id')
						$(this).removeClass("notification-item-not-read");
						$(this).addClass("notification-item-read");
						confirmMessageNotificacion(idMessage);
						$(this).unbind("click");
					});
				} else {
					item = '<li class="notification-item notification-item-read"><p class="notification-item-aviso">No ten&eacute;s notificaciones</p></li>';
					$(item).appendTo('.notifications-message ul');
				}
			} else {
				item = '<li class="notification-item notification-item-read"><p class="notification-item-aviso">En este momento no podemos obtener los mensajes.</p></li>';
				$(item).appendTo('.notifications-message ul');
			}
		},
		beforeSend:function(){},
		error:function(jqXHR, textStatus, errorThrown) {
			clearNotificationPanel();
			item = '<li class="notification-item notification-item-read"><p class="notification-item-aviso">En este momento no podemos obtener los mensajes.</p></li>';
			$(item).appendTo('.notifications-message ul');
		}
	});
  }


  $(document).ready(function(){

	var one_second = 1000;
	var wait_time = one_second * SLEEP_TIME_PER_SECOND;


	$('#dropdown-login').addClass('enable-dd');
	$('#dropdown-login .login').removeClass('active')
	$('#dropdown-login .login').click(function(){
		if(!$(this).hasClass('active')){
			$(this).addClass('active').siblings('.dropdown').show();
			$("#user-login-form").hide();
		} else {
			$(this).removeClass('active').siblings('.dropdown').hide();
		}
		return false;
	});

	$('.cabezal-ingresar').addClass('enable-dd');
	$('.cabezal-ingresar').click(function(){

		if(!$(this).hasClass('active')){
			if ($("#menu-responsive section#block-menu-menu-responsive").is(':visible')) {
				$("#menu-responsive button").click();
			};

			$(this).addClass('active');
			jQuery('.dropdown').show();
			$("#user-login-form").hide();

		} else {
			$(this).removeClass('active');
			jQuery('.dropdown').hide();
		}
		manageRegionSlideShowMarginTop();
		return false;
	});

	$('#configuracion .config-content').click(function(){
		if(!$('#configuracion .config-content').hasClass('active')){
			$('#configuracion .config-content').addClass('active');
			$('#configuracion ul').show();
		} else {
			$('#configuracion .config-content').removeClass('active');
			$('#configuracion ul').hide();
		}
		return false;
	});

	$('.configuración a').click(function(){
		$('#config-menu-mobile').toggle();
	});

	updateNotificacion(false);
	//setInterval(function () {updateNotificacionCount()}, wait_time);  Si es necesario se puede actualizar cada cierto tiempo.
	$('.notifications, .cabezal-notificaciones').click(function(){
		if(!$('.notifications, .cabezal-notificaciones').hasClass('active')){
			$('.notifications, .cabezal-notificaciones').addClass('active');
			$('.notifications-message ul').show();
			updateNotificacion(true);
		} else {
			$('.notifications, .cabezal-notificaciones').removeClass('active');
			$('.notifications-message ul').hide();
		}

		return false;
	});

	$(".trigger-margin-region-slideshow").click(function() {
		manageRegionSlideShowMarginTop();
	});

	$( window ).resize(function() {
	  manageRegionSlideShowMarginTop();
	});

  });

	function manageRegionSlideShowMarginTop(){
		if (document.documentElement.clientWidth < 961){
			var dropdownContent = document.getElementById('dropdown-content');
			var newMarginTop = 0;
			if (dropdownContent != null) {
				var newMarginTop = dropdownContent.clientHeight;
			}
			$('.region-slideshow').css('margin-top', newMarginTop);
		} else {
			$('.region-slideshow').css('margin-top',0);
		}
	}
})(jQuery);
