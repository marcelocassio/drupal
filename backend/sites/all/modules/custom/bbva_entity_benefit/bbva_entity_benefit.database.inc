<?php
/*
* Replica los beneficios en base al id_beneficios_drupal y la cantidad de AST que tiene el segmento del beneficio.
*/
function _duplicar_beneficio_from_segmento($id_beneficio_drupal, $benefitSegmentos, $lineaBeneficioSuip) {
  
  $listadoSegmentoAst = $benefitSegmentos[$lineaBeneficioSuip[0]];
  
  foreach ($listadoSegmentoAst as $segmentoAst) {
    $idBenefitSuip = $lineaBeneficioSuip[0];
    $codigoAst = $segmentoAst['codigoAst'];
    $codigoSegmento = $segmentoAst['codigoSegmento'];
    $fechaDesde = $segmentoAst['fechaDesde'];
    $fechaHasta = $segmentoAst['fechaHasta'];

    $benefit_nominas = _consultar_beneficio_nomina($codigoSegmento, $codigoAst);
    
     // crear funcion
    if (count($benefit_nominas) == 0) {    
        echoline("REPLICAR beneficio nomina id_benefit_drupal: " . $id_beneficio_drupal . " \n", true, true);
        $benefitEntity = entity_load_single("benefit", $id_beneficio_drupal);
        unset($benefitEntity->id);
        unset($benefitEntity->field_benefit_id);
        $benefitEntity->field_benefit_date_start['und'][0]['value'] = $fechaDesde->format('Y-m-d');
        $benefitEntity->field_benefit_date_end['und'][0]['value'] = $fechaHasta->format('Y-m-d');
        $benefitEntity->field_benefit_ast_id['und'][0]['value'] = $segmentoAst['codigoAst'];
        $benefitEntity->field_benefit_segmento['und'][0]['value'] = $segmentoAst['codigoSegmento'];
        $benefitEntity->field_published['und'][0]['value'] = 0;

        $descripcion = str_replace('$AST', $segmentoAst['descripcion'], $lineaBeneficioSuip[6]);
        $descripcion = iconv('Windows-1252', 'UTF-8', $descripcion);
        
        $benefitEntity->field_benefit_description_portal['und'][0]['value']= $descripcion;
        entity_save('benefit', $benefitEntity);
    } 
    else if (count($benefit_nominas) == 1){
      echoline("ACTUALIZAR beneficio nomina id_benefit_drupal: " . $id_beneficio_drupal . " \n", true, true);
      $id_beneficio_drupal = $benefit_nominas[0]->entity_id;
      $benefitEntity = entity_load_single("benefit", $id_beneficio_drupal);
      $benefitEntity->field_benefit_date_start['und'][0]['value'] = $fechaDesde->format('Y-m-d');
      $benefitEntity->field_benefit_date_end['und'][0]['value'] = $fechaHasta->format('Y-m-d');
      $legalesDefault = $lineaBeneficioSuip[9] . " " . $lineaBeneficioSuip[30];
      $benefitEntity->field_benefit_legal['und'][0]['value'] = iconv('Windows-1252', 'UTF-8', $legalesDefault);
      $benefitEntity->field_benefit_local_add['und'][0]['value'] = iconv('Windows-1252', 'UTF-8', $lineaBeneficioSuip[23]);
      $benefitEntity->field_benefit_title['und'][0]['value'] = iconv('Windows-1252', 'UTF-8', $lineaBeneficioSuip[24]);
      
      if (!empty($lineaBeneficioSuip[28])) {
        $benefitEntity->field_benefit_porcent['und'][0]['value'] = $lineaBeneficioSuip[28];
      }
      if (!empty($lineaBeneficioSuip[29])) {
        $benefitEntity->field_benefit_grl_procent['und'][0]['value'] = $lineaBeneficioSuip[29];
      }
      if (!empty($lineaBeneficioSuip[2])) {
        $benefitEntity->field_benefit_priority['und'][0]['value'] = $lineaBeneficioSuip[2];
      }

      $benefitEntity->field_benefit_cft['und'][0]['value'] = iconv('Windows-1252', 'UTF-8', $lineaBeneficioSuip[10]);

      $descripcion = str_replace('$AST', $segmentoAst['descripcion'], $lineaBeneficioSuip[6]);
      $descripcion = iconv('Windows-1252', 'UTF-8', $descripcion);
	    $benefitEntity->field_benefit_description_portal['und'][0]['value']= $descripcion;
      entity_save('benefit', $benefitEntity);
    } 
    else {
      echoline("Existe mas de un beneficio con beneficio nomina con mismo segmento y ast id_benefit_drupal: " . $id_beneficio_drupal . " \n", true, true);
    }
  }
}


/*
* @param $idBenefitSuip Id de beneficio de suip
* @param $codigoAst  Codigo numerico de AST
*/
function _consultar_beneficio_nomina($segmento, $codigoAst) {
  $query = db_select('field_data_field_benefit_segmento', 'seg');
  $query->join('field_data_field_benefit_ast_id','ast','ast.entity_id = seg.entity_id');
  $query->fields('seg', array('field_benefit_segmento_value', 'entity_id'))
        ->fields('ast', array('field_benefit_ast_id_value'))
        ->condition('seg.field_benefit_segmento_value', $segmento)
        ->condition('ast.field_benefit_ast_id_value', $codigoAst);

  return $query->execute()->fetchAll();
}

/*
* Relaciona los beneficios creados con lo cupones creados con SUIP.
*
*/
function _relate_benefit_suip_to_cuopon() {

  echoline("Inicio Relacion Beneficio con Cupon SUIP", true, true);

  $cupones_incompletos = _bbva_coupon_get_coupon_suip_incompleto();

  foreach ($cupones_incompletos as $cupon) {
    echoline(" :: CUPON SUIP :: id promo cupon incompleto encontrado: " . $cupon->id_cupon, true, true);
    $promo_cupon_entity = entity_load_single('coupon_promo', $cupon->id_cupon);
    $promo_cupon_wrapper = entity_metadata_wrapper('coupon_promo', $promo_cupon_entity);
    $benefit = bbva_buscar_benefit_con_idSUIP($cupon->id_comunicacion_suip);

    if (isset($benefit->entity_id)) {
      $entity_benefit = entity_load_single('benefit', $benefit->entity_id);
      if($entity_benefit) {
        $wrapper_benefit = entity_metadata_wrapper('benefit', $entity_benefit);
        $promo_cupon_wrapper->field_coupon_promo_legal->set($wrapper_benefit->field_benefit_legal->value());
        $promo_cupon_wrapper->field_coupon_promo_benefit_asoc->set(1);
        $promo_cupon_wrapper->field_coupon_promo_benefit->set($entity_benefit->id);
        $promo_cupon_wrapper->save();
        echoline(" :: CUPON SUIP :: id promo cupon COMPLETA Beneficio: " . $cupon->id_cupon, true, true);
      }
    }
  }
  echoline("Fin Relacion Beneficio con Cupon SUIP", true, true);
}

