<?php

/*
 * Implements hook_drush_command().
 */
function bbva_entity_benefit_drush_command() {

  $items['vuelco-benefit'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('benefitImport'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['vuelco-all'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('importSuip'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['vuelco-benefit-campaign'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('campaignImport'),
  );
    $items['vuelco-benefit-type'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('benefitTypeImport'),
  );

  $items['benefit-shop-branch'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('benefitShopBranchImport'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );


// Esto no tiene sentido, pero no se sabe si algun proceso script lo está corriendo ......
  $items['delete-benefit-shop-branch'] = array(
    'description' => 'Elimina todos los benefit shop branch de  drupal',
    'aliases' => array('deleteBenefitShopBranch'),
  );

  $items['delete-benefit'] = array(
    'description' => 'Elimina todos los beneficios de  drupal',
    'aliases' => array('deleteBenefit'),
  );


  $items['prueba-benefit'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('pruebaBenefit'),
  );
  return $items;
}

/**
 * Callback for the drush-demo-command command
 */

// Imports
function drush_bbva_entity_benefit_vuelco_benefit() {
        drupal_set_message( "Start" . date("ymd"), 'status', FALSE);

  $batch = array(
    'operations' => array(),
    'title' => t('Batch Coupon Promotion Visa'),
    'init_message' => t('Batch Coupon Promotion Visa is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'import_benefit_suip_finished',
    'file' => drupal_get_path('module', 'batch_coupon') . '/batch_coupon.batch.inc',
  );


  // Start the batch job.
    $results = array("voucher_process_visa");
  foreach ($results as $result) {
    $batch['operations'][] = array('import_benefit_suip' , array($result));
  }
  batch_set($batch);
  drush_backend_batch_process();

}

function drush_bbva_entity_benefit_vuelco_all() {

  $batch = array(
    'operations' => array(),
    'title' => t('Batch Import All'),
    'init_message' => t('Importando Datos...'),
    'error_message' => t('An error occurred'),
    'finished' => 'import_all_suip_finished',
  );


  // Start the batch job.
  $batch['operations'][] = array('import_all_suip' , array());
  batch_set($batch);
  drush_backend_batch_process();

}

function drush_bbva_entity_benefit_vuelco_benefit_type() {
  _import_benefit_type();
}

function drush_bbva_entity_benefit_vuelco_benefit_campaign() {
  _import_campanias();
}

function drush_bbva_entity_benefit_benefit_shop_branch() {
  import_benefit_shop_branch();
}

// Deletes
function drush_bbva_entity_benefit_delete_benefit() {
  _delete_benefit_suip();
}

function drush_bbva_entity_benefit_delete_benefit_shop_branch() {
  _delete_benefit_shop_branch();
}

function drush_bbva_entity_benefit_prueba_benefit() {
  _get_benefit_titulos();
}

