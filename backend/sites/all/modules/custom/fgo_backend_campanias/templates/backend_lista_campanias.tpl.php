<?php
/*
 * La documentación del componente para crear tablas esta aquí
 * https://github.com/xaksis/vue-good-table
 *
 * Buscar en esta dirección las dependencias que se quieran usar y bajarlas local:
 * https://cdn.jsdelivr.net/npm/vue-particles
 * O
 * https://unpkg.com/vue-particles@1.0.9/src/vue-particles/index.js
 *
 * */
global $base_url;
$path_backend = drupal_get_path('module', 'fgo_backend');
$path_backend_campanias = drupal_get_path('module', 'fgo_backend_campanias');

//Principal VueJS
drupal_add_js($path_backend . '/js/vue.min.js');

//Dependencia para hacer llamadas http //AXIOS
drupal_add_js($path_backend . '/js/axios_0_18_0.min.js');

//Dependencias para tablas
drupal_add_js($path_backend . '/js/vue-good-table.js');
drupal_add_css($path_backend . '/css/vue-good-table.min.css');

// Bootstrap 4
drupal_add_css($path_backend . '/css/bootstrap_4_1_1.min.css');

// Font Awesome
drupal_add_css($path_backend . '/css/fontawesome_5_0_13.css');
drupal_add_css('https://unpkg.com/@fortawesome/fontawesome-free-webfonts@1.0.9/css/fa-solid.css', 'external');
drupal_add_css('https://unpkg.com/@fortawesome/fontawesome-free-webfonts@1.0.9/css/fa-regular.css', 'external');

//Custom
drupal_add_css($path_backend . '/css/fgo_backend_default.css');
drupal_add_css($path_backend_campanias . '/css/fgo_backend_campanias.css');

?>
<div id="container-particles-js">
    <div id="particles-js"></div>
</div>
<div id="admin_container">
    <h1>Administración de Campañas</h1>
    <div v-if="loading" class="spinner_backend" v-cloak>
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    <vue-good-table v-else
            :columns="formattedColumns"
            :rows="campanias"
            style-class="vgt-table striped condensed"
            :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: true, perPage: 50, ofLabel: 'de'}"
            :search-options="{ enabled: true, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
        <div v-slot:emptystate>
            No hay campañas con ese criterio
        </div>
        <template v-slot:table-row="props">
            <div v-if="props.column.field == 'backendImagenesC'">
                <div v-html="props.row.backendImagenesC"></div>
            </div>
            <div v-else-if="props.column.field == 'botones'">
                <div v-html="props.row.botones"></div>
            </div>
            <div v-else-if="props.column.field == 'publicado'">
                <div v-html="props.row.publicado"></div>
            </div>
            <div v-else>
              {{props.formattedRow[props.column.field]}}
            </div>
        </template>
    </vue-good-table>
    </div>
<script src="<?=$base_url?>/<?=$path_backend?>/js/particles.min.js"></script>
<script>
    const link = 'https://' + window.location.host + '/fgo/API/backend/campanias';

    new Vue({
        el: '#admin_container',
        data: {
            campanias: [],
            loading: false,
            columns: [
                {
                    label: 'Publicado por Anticipado',
                    field: 'publicado',
                    type: 'number',
                    width: '110px',
                    globalSearchDisabled: true,
                },
                {
                    label: 'ID Suip',
                    field: 'idCampaniaSuip',
                    type: 'number',
                },{
                    label: 'Título',
                    field: 'nombre',
                    filterable: true
                },{
                    label: 'Fecha Desde',
                    field: 'fechaDesde',
                    type: 'date',
                    dateInputFormat: 'X',
                    dateOutputFormat: 'DD-MM-YYYY',
                },{
                    label: 'Fecha Hasta',
                    field: 'fechaHasta',
                    type: 'date',
                    dateInputFormat: 'X',
                    dateOutputFormat: 'DD-MM-YYYY',
                },{
                    label: 'Vista Previa',
                    field: 'botones',
                    width: '100px',
                    globalSearchDisabled: true,
                },
            ]
        },
        methods:{
            getCampanias(){
                this.loading = true;
                axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200){
                            this.campanias = response.data.data;
                        }else{
                            this.campanias = null;
                        }
                    })
                    .catch(error => {
                        this.campanias = null;
                    })
                    .finally(() => this.loading = false)
            },
            renderData(campanias) {
                campanias.map(item => {
                    item.botones = ' <a href="vista_previa/' + item.idCampaniaSuip + '" class="btn btn-outline-info btn-md">Ver</a>';
                    if (item.siempreVisible == 1){
                        item.publicado = '<i class="far fa-check-square fa-3x" aria-hidden="true"></i>';
                    }else{
                        item.publicado = '<i class="far fa-times-circle fa-3x" aria-hidden="true"></i>';
                    }
                });
            },
        },
        computed:{
            formattedColumns(){
                if(this.campanias){
                    this.renderData(this.campanias);
                    return this.columns;
                }
            }
        },
        mounted() {
            this.getCampanias();
        }
    });
    particlesJS.load('particles-js', '<?=$base_url?>/<?=$path_backend?>/js/particles.json', function() {});
</script>
