<?php
/*
 * La documentación del componente para crear tablas esta aquí
 * https://github.com/xaksis/vue-good-table
 *
 * Buscar en esta dirección las dependencias que se quieran usar y bajarlas local:
 * https://cdn.jsdelivr.net/npm/vue-particles
 * O
 * https://unpkg.com/vue-particles@1.0.9/src/vue-particles/index.js
 *
 * Llamada a Bootstrap-Vue
 * https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js
 *
 * */
global $base_url;
$path_backend = drupal_get_path('module', 'fgo_backend');
$path_backend_home = drupal_get_path('module', 'fgo_backend_home');
$path_backend_campanias = drupal_get_path('module', 'fgo_backend_campanias');
$varsCustom = get_defined_vars();
//Principal VueJS
drupal_add_js($path_backend . '/js/vue.min.js');

//Dependencia para hacer llamadas http //AXIOS
drupal_add_js($path_backend . '/js/axios_0_18_0.min.js');

//Dependencias para tablas
drupal_add_js($path_backend . '/js/vue-good-table.js');
drupal_add_css($path_backend . '/css/vue-good-table.min.css');

// Bootstrap 4
drupal_add_css($path_backend . '/css/bootstrap_4_1_1.min.css');

// Bootstrap Vue
drupal_add_js($path_backend . '/js/bootstrap-vue.js');

// Dependencia de Fechas
drupal_add_js($path_backend_home . '/js/moment_2_22_2.js');

// Depedndencia del modal
drupal_add_js($path_backend . '/js/vuedals.min.js');

//Modals (Pop-Up Estados)
drupal_add_js($path_backend . '/js/modals.js');

// Dependencia para copiar texto
drupal_add_js($path_backend . '/js/vue-clipboard.min.js');

// Font Awesome
drupal_add_css($path_backend . '/css/font_awesome_4_7.min.css');

//Custom
drupal_add_css($path_backend . '/css/fgo_backend_default.css');
drupal_add_css($path_backend_campanias . '/css/fgo_backend_campanias.css');

?>
<div id="container-particles-js">
    <div id="particles-js"></div>
</div>

<div id="admin_container">
    <h1><strong>Previsualización de Campaña</strong></h1>
    <div v-if="loading" class="spinner_backend" v-cloak>
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    <div v-else>
        <b-container class="bv-row">
            <b-row class="copyLink">
                <input type="text" v-model="copyData" disabled="disabled" class="deeplinking">
                <button type="button" @click="doCopy" class="boton_positivo">Copiar link (mailing)</button>
            </b-row>
            <div class="row text-center container_form">
              <?php print drupal_render($varsCustom['formCampania']); ?>
            </div>

            <b-row>
                <b-card :title="comunicacion.titulo"
                    :img-src="comunicacion.imagen"
                    img-alt="Image"
                    tag="comunicacion"
                    style="max-width: 20rem;"
                    class="mb-3"
                    v-for="comunicacion in campania">
                    <p class="card-text">
                        <strong>{{ comunicacion.beneficios | subtituloComunicacion }}</strong>
                    </p>
                    <p class="card-text">
                        Desde: <strong>{{ moment(comunicacion.fecha_vigencia_desde, "X").format('DD/MM/YYYY') }}</strong> -
                        Hasta: <strong>{{ moment(comunicacion.fecha_vigencia_hasta, "X").format('DD/MM/YYYY') }}</strong>
                    </p>
                    <div v-if="comunicacion.beneficios.length">
                        <p class="card-text" v-for="requisito in comunicacion.beneficios[0].requisitos">
                            - {{ requisito }}
                        </p>
                    </div>
                    <b-button v-on:click="openSucursales(comunicacion.id)" variant="primary" class="boton_positivo">Ver Canales</b-button>
                </b-card>
            </b-row>
        </b-container>
    </div>
    <vuedals></vuedals>
</div>
<script src="<?=$base_url."/". $path_backend?>/js/particles.min.js"></script>
<script>

    var idCampania = <?=$idCampania?>;
    const link = 'https://' + window.location.host + '/fgo/API/be/campaign/' + idCampania;
    const linkVerSucursales = 'https://' + window.location.host + '/fgo/API/be/communication/';

    // Contantes para el modal
    const Bus = Vuedals.Bus;
    const Component = Vuedals.Component;
    const Plugin = Vuedals.default;

    Vue.use(Plugin);


    //Componente Ver Canales
    const verCanales = {
        name: 'inside-modal',
        template: ' <div class="container">' +
        '               <div class="row">' +
        '                   <div class="col">' +
        '                   <div v-if="this.$parent.$parent.comunicacionMarcasShopping">' +
        '                       <div v-if="this.$parent.$parent.comunicacionMarcasShopping.length">' +
        '                       Locales Adheridos:' +
        '                           <p class="card-text" v-for="marca in this.$parent.$parent.comunicacionMarcasShopping">' +
        '                               <strong>{{ marca | capitalize }}</strong>' +
        '                           </p>' +
        '                       </div><br>' +
        '                    </div>' +
        '                       <p class="card-text" v-for="zona in this.$parent.$parent.comunicacionZonas">' +
        '                           - <strong>{{ zona | capitalize }}</strong></p>' +
        '                       <p class="card-text" v-for="sucursal in this.$parent.$parent.comunicacionSucursales">' +
        '                           - <strong>{{ sucursal.direccion | capitalize }}</strong> en {{ sucursal.localidad }}</p>' +
        '                       <p class="card-text" v-for="web in this.$parent.$parent.comunicacionWeb">' +
        '                           - <strong> Web: </strong> <b-link :href="web.url" target="_blank">{{ web.name | capitalize }}</b-link></p>' +
        '                       <p class="card-text" v-if="this.$parent.$parent.comunicacionTelefono">' +
        '                           - <strong>Teléfono: </strong>{{ this.$parent.$parent.comunicacionTelefono }}</p>' +
        '                   </div>' +
        '               </div>' +
        '           </div>',
        filters: {
            capitalize: function (value) {
                if (!value) return ''
                value = value.toString()
                return value.charAt(0).toUpperCase() + value.slice(1)
            }
        }
    }

    new Vue({
        el: '#admin_container',
        components: {
            vuedals: Component
        },
        data: {
            copyData: 'https://' + window.location.host + '/fgo/#/beneficios/campania/' + idCampania,
            campania: [],
            comunicacionMarcasShopping: null,
            comunicacionZonas: null,
            comunicacionSucursales: null,
            comunicacionWeb: null,
            comunicacionTelefono: null,
            loading: false
        },
        filters: {
            subtituloComunicacion(valores) {
                let subtitulo = '';
                if (valores.length){
                    //Esta lógica tiene que ir en el ApiBackend.php
                    //TODO
                    if (valores[0].valor){
                        subtitulo = valores[0].valor + "%";
                    }

                    if (valores[0].cuota){
                        if (subtitulo){
                            subtitulo += " y ";
                        }
                        subtitulo += valores[0].cuota + " cuotas";
                    }

                    if (valores[0].casuistica.descripcion){
                        subtitulo = valores[0].casuistica.descripcion;
                    }

                    return subtitulo + " " + valores[0].condicion;
                }
                return null;
            }
        },
        methods:{
            getCampania(){
                this.loading = true;
                axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200){
                            this.campania = response.data.data;
                        }else{
                            this.campania = null;
                        }
                    })
                    .catch(error => {
                        this.campania = null;
                    })
                    .finally(() => this.loading = false)
            },
            openSucursales(idComunicacion){
                axios
                    .get(linkVerSucursales + idComunicacion)
                    .then(response => {
                        if (response.status === 200){
                            if (response.data.data.beneficio){
                                this.comunicacionMarcasShopping = response.data.data.beneficio.canales_venta.marcas_adheridas_shopping;
                                this.comunicacionZonas = response.data.data.beneficio.canales_venta.zonas;
                                this.comunicacionSucursales = response.data.data.beneficio.canales_venta.sucursales;
                                this.comunicacionWeb = response.data.data.beneficio.canales_venta.web;
                                this.comunicacionTelefono = response.data.data.beneficio.canales_venta.telefono;

                                Bus.$emit('new', {
                                    title: 'Canales',
                                    component: verCanales
                                });
                            }else{
                                Bus.$emit('new', {
                                    title: 'Comunicación no vigente',
                                    component: badModal
                                });
                            }
                        }
                    })
                    .catch(error => {
                        Bus.$emit('new', {
                            title: 'No se pueden mostrar las sucursales adheridas',
                            component: badModal
                        });
                    })
            },
            doCopy() {
                this.$copyText(this.copyData).then(e => {
                }, function (e) {
                  alert('ALERTA: no se pudo copiar')
                  console.log(e)
                })
            }
        },
        mounted() {
            this.getCampania();
        }
    });
    particlesJS.load('particles-js', '<?=$base_url?>/<?=$path_backend?>/js/particles.json', function() {});
</script>
