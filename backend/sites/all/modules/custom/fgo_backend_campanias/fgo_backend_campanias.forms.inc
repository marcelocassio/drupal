<?php
function fgo_backend_campanias_editar_campanias_form($form_state, $build) {
  $id = $build["build_info"]["args"][0];

  $campaniaBo = \Fgo\Bo\CampaniaBo::buscarPorIdSuip($id);

  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['#validate'][] = 'fgo_backend_campanias_form_validate';

  $form['grupo_slide'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong>Publicar esta campaña por anticipado</strong>'),
  );

  $form['grupo_slide']['idCampania'] = array(
      '#type' => 'hidden',
      '#default_value' => $campaniaBo->idCampania
  );

  $form['grupo_slide']['publicado'] = array(
      '#type' =>'checkbox',
      '#title'=>t('Anticipar Campaña?'),
      '#default_value' => $campaniaBo->siempreVisible
  );

  $form['grupo_slide']['guardar'] = array(
      '#type' => 'submit',
      '#id' => 'guardar_slide',
      '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
      '#value' => 'Actualizar Campaña',
      '#submit' => array('fgo_backend_campanias_form_submit')
  );

  return $form;
}

function fgo_backend_campanias_form_validate(&$form, &$form_state){
  $campaniaBo = new \Fgo\Bo\CampaniaBo($form_state['values']['idCampania']);

  $campaniaBo->siempreVisible = $form_state['values']['publicado'];

  try {
    $campaniaBo->guardar();
    // Limpio caché
    $cache_data = \Fgo\Services\Api::obtenerConstantesCache();
    $cache = \BBVA\Cache\CacheFile::getInstance();
    $cache->clean(array($cache_data['campanias_be']['tipo']));
  } catch ( \Fgo\Bo\DatosInvalidosException $e) {
    $errores = $e->getErrores();
    foreach ($errores as $error_campo => $error_descripcion){
      form_set_error($error_campo, $error_descripcion);
    }
  }
}

function fgo_backend_campanias_form_submit(&$form, &$form_state){
  $form_state['redirect'] = url('../admin/v2/campanias/lista');
}
