(function($) {
  Drupal.behaviors.bbva_benefit = {
    attach: function (context, settings) {

      if (location.pathname == settings.bbva_benefit.ajaxUrl) {

        detectaNavegador();

        var param_campaign = 'campaign';
        var param_campaign_value = detecta( 'id_campaign' ); // Es el parametro que quiero detectar
        var newHtml = '';
        /* Llamada ajax */
        if (param_campaign_value && param_campaign_value != 0) {
          $.ajax({
            method: "GET",
            url: Drupal.settings.basePath +'json/benefit?'+param_campaign+'='+param_campaign_value+'&moda=tabla',
            dataType: "json",
            async: true,
            xhrFields: {
               withCredentials: true
            },
            success: function (respuesta) {
              $.each(respuesta, function(index, dato) {
                if (dato.img_path_json_2 != '') {
                  newHtml += ' <div class="benefit">';
                    newHtml += ' <a href="'+dato.share_link+'">';
                      newHtml += ' <div class="benefit-img">';
                        newHtml += ' <img src="'+dato.img_path_json_2+'" alt="" />';
                      newHtml += ' </div>';
                      newHtml += ' <div class="benefit-placa">';
                        newHtml += ' <div class="benefit-marca">';
                          if (dato.logo != '') {
                            newHtml += ' <img class="benefit-marca-imagen" src="'+dato.logo+'" alt="'+dato.title+'" />';
                          }else{
                            newHtml += ' <div class="benefit-marca-texto">'+dato.title+'</div> ';
                          }
                        newHtml += ' </div>';
                        newHtml += ' <div class="benefit-info">';
                        if (!dato.mostrar_tarjeta_go) {
                          if (dato.porcentaje_tarjeta && dato.porcentaje_tarjeta != 0) {
                            newHtml += ' <div class="benefit-info-descuento">-<span>'+dato.porcentaje_tarjeta+'</span>%</div>';
                          };
                          if (dato.cuota_tarjeta && dato.cuota_tarjeta != 0) {
                            newHtml += ' <div class="benefit-info-cuotas benefit-info-cuotas"><span>'+dato.cuota_tarjeta+'</span> cuotas</div>';
                          };
                        }else{ // Si existe mostrar_tarjeta_go
                          if (dato.porcentaje_tarjeta_go && dato.porcentaje_tarjeta_go != 0) {
                            newHtml += ' <div class="benefit-info-descuento">-<span>'+dato.porcentaje_tarjeta_go+'</span>%</div>';
                          };
                          if (dato.cuota_tarjeta_go && dato.cuota_tarjeta_go != 0) {
                            newHtml += ' <div class="benefit-info-cuotas benefit-info-cuotas"><span>'+dato.cuota_tarjeta_go+'</span> cuotas</div>';
                          };
                        };
                        newHtml += ' </div>';
                      newHtml += ' </div>';
                    newHtml += ' </a>';
                  newHtml += ' </div>';
                };
              });
              if (newHtml != '') {
                $("#section-benefit").html(newHtml);
              }else{
                location.assign("404");
              }
              // $("html, body").css("cursor", "default");
            },
            error:function(jqXHR, textStatus, errorThrown){
              alert("En este momento el servidor está ocupado, intente más tarde");
              location.assign("404");
              //console.log(textStatus);
            }
          });
        }
      };
    }
  };

  /* DETECTA PARAMETROS EN URL */
  function detecta( name ){
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null ){
    return 0;
    }else{
      return results[1];
    }
  }

  function detectaNavegador(  ){
    var standalone = window.navigator.standalone,
    userAgent = window.navigator.userAgent.toLowerCase(),
    safari = /safari/.test( userAgent ),
    ios = /iphone|ipod|ipad/.test( userAgent );

    if( ios ) {
      if ( !standalone && safari ) {
        //browser
      } else if ( standalone && !safari ) {
        //standalone
      } else if ( !standalone && !safari ) {
        //uiwebview
        jQuery("#smartbanner").remove();
      };
    } else {
      //not iOS
    };

  }

})(jQuery);

