<?php

function _get_benefit_image(){

  $query = db_select('eck_benefit', 'eb');
  $query->leftJoin('field_data_field_benfit_image', 'fbi', 'eb.id = fbi.entity_id');
  $query->leftJoin('field_data_field_benefit_shop_branchs', 'fdfbsb', 'eb.id = fdfbsb.entity_id');

  $query->leftJoin('field_data_field_bsb_branchs', 'bb', 'bb.entity_id = fdfbsb.field_benefit_shop_branchs_target_id');
  $query->leftJoin('eck_branch', 'ebranch', 'ebranch.id = bb.field_bsb_branchs_target_id');

  // Shopping Join
  $query->leftJoin('field_data_field_benefit_shopping', 'fdfbs', 'fdfbs.entity_id = eb.id');
  $query->leftJoin('eck_shopping', 'eshopi', 'fdfbs.field_benefit_shopping_target_id = eshopi.id');
  $query->leftJoin('field_data_field_shopping_imagen', 'fdfshpi', 'fdfshpi.entity_id = eshopi.id');

  // Otro
  $query->leftJoin('eck_benefit_shop_branch', 'ebsb', 'fdfbsb.field_benefit_shop_branchs_target_id = ebsb.id');
  $query->leftJoin('field_data_field_bsb_shop', 'fsfbs', 'fsfbs.entity_id = ebsb.id');
  $query->leftJoin('eck_shop', 'es', 'fsfbs.field_bsb_shop_target_id = es.id');
  $query->leftJoin('field_data_field_shop_imagen', 'fdfsi', 'fdfsi.entity_id = es.id');
  $query->leftJoin('field_data_field_shop_category', 'fdfsc', 'fdfsc.entity_id = es.id');

  $query->fields('eb', array('id'));
  $query->fields('fbi', array('field_benfit_image_fid'));
  $query->fields('fdfshpi', array('field_shopping_imagen_fid'));
  $query->fields('fdfsi', array('field_shop_imagen_fid'));

  $query->groupBy('eb.id');


  $results = $query->execute()->fetchAll();

  if ($results) {
    return bbva_benefit_obtener_todas_img_uri($results);
  }else{
    watchdog("error_get_benefit_image", "SQL de benefit image fallo");
    return null;
  }

}

function bbva_buscar_benefit_con_idSUIP($id_suip) {

  $query = db_select('field_data_field_benefit_id', 'bi');
  $query->condition('bi.field_benefit_id_value', $id_suip);
  $query->fields('bi', array('entity_id'));

  return $query->execute()->fetchObject();
}
