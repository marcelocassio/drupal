<?php

function bbva_benefit_obtener_todas_img_uri($results){

  if (is_array($results)) {

    $array_images_by_id = array();

    foreach ($results as $key) {

      // Si tengo imagen en el beneficio
      if ($key->field_benfit_image_fid) {
        $array_images_by_id[$key->id] = bbva_benefit_obtener_img_uri($key->field_benfit_image_fid);
      }

      // Si NO tengo imagen en el beneficio y tengo un Shopping cargado
      if ((!isset($array_images_by_id[$key->id])) && ($key->field_shopping_imagen_fid)) {
        $array_images_by_id[$key->id] = bbva_benefit_obtener_img_uri($key->field_shopping_imagen_fid);
      }

       // Si NO tengo imagen en el beneficio y tengo un comercio cargado
      if ((!isset($array_images_by_id[$key->id])) && ($key->field_shop_imagen_fid)) {
        $array_images_by_id[$key->id] = bbva_benefit_obtener_img_uri($key->field_shop_imagen_fid);
      }

      if (!isset($array_images_by_id[$key->id])) {
        $array_images_by_id[$key->id] = "";
      }
    }
    return $array_images_by_id;
  }else{
    watchdog("error_benefit_obtener_todas_img_uri", "No se pudo obtener la uri mediante los fid resultados: ".$results);
    throw new Exception("Proceso de obtener imagenes invalido, no se recibe array", 1);
  }
}

function bbva_benefit_obtener_img_uri($fid){

  if ($fid) {
    $file = file_load($fid);
    $uri = $file->uri;

    return file_create_url($uri);
  }else{
    watchdog("error_benefit_obtener_img_uri", "No se pudo obtener la uri mediante el fid: ".$fid_temp);
    return '';
  }
}

function bbva_benefit_obtener_img_uri_by_tid($tid){

  $term = taxonomy_term_load($tid);
  $fid_temp = $term->field_category_image;
  $fid = $fid_temp["und"][0]["fid"];

  if ($fid) {
    $file = file_load($fid);
    $uri = $file->uri;

    return file_create_url($uri);
  }else{
    watchdog("error_benefit_obtener_img_uri", "No se pudo obtener la uri mediante el tid: ".$tid);
    return '';
  }
}

?>
