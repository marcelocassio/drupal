<?php

function _bbva_list_experience($experience){
	$query = db_select('node', 'n');
  $query -> innerJoin('field_data_field_experience_category', 'fec', 'fec.entity_id = n.nid');
  $query -> innerJoin('field_data_field_experience_volanta', 'fev', 'fev.entity_id = n.nid');
  $query -> innerJoin('field_data_field_experience_image_list', 'feil', 'feil.entity_id = n.nid');
  $query -> innerJoin('field_data_field_experience_weight', 'few', 'few.entity_id = n.nid');

	$query = $query
    ->condition('n.type', 'experience', '=')
    ->condition('fec.field_experience_category_tid', $experience, '=')
    ->condition('fev.bundle', 'experience', '=')
	  ->fields('n', array('nid', 'title'))
	  ->fields('fec', array('field_experience_category_tid'))
	  ->fields('fev', array('field_experience_volanta_value'))
    ->fields('feil', array('field_experience_image_list_fid'))
    ->condition('n.status', 1 , '=')
    ->orderBy('few.field_experience_weight_value', 'ASC')
	  ->execute()
	  ->fetchAll();

	foreach($query as $key => $item){
    // generate  image
    if($item->field_experience_image_list_fid != Null){
      $image_path = image_path($item->field_experience_image_list_fid);
      $query[$key]->image_path = image_style_url('experience_teaser', $image_path);
    }
  }

	return $query;
}




/* Funcion para generar path partir del fid */
function image_path($fid){
  $imgpath = file_load($fid)->uri;
  return $imgpath;
}


/**
 * @function: _get_user_uid()
 * @description: Devuelve la lista de UID segun lo buscado
 * @param $listTipNumberDoc (array): array donde cada fila es  la concatenacion del tipo doc y numdoc
 * @param $type_client (int):  0 no cliente y 1 cliente
 * @param $credit_card (int):  0 sin tarjeta y 1 tarjeta
 */

function _get_user_uids($listTipNumberDoc=NULL,$type_client=NULL,$credit_card=NULL){


  $result = _get_user_doc_uids($listTipNumberDoc, $type_client, $credit_card);

  $uids=array();
  foreach ($result as $key => $value) {
         $uids[]=$value->uid;
   }
  return $uids;
}

/**
 * @function: _get_user_uid()
 * @description: Devuelve una mapa con clave tipodocumento mas dni, y valores los uid
 * @param $listTipNumberDoc (array): array donde cada fila es  la concatenacion del tipo doc y numdoc
 * @param $type_client (int):  0 no cliente y 1 cliente
 * @param $credit_card (int):  0 sin tarjeta y 1 tarjeta
 */

function _get_user_tipo_doc_uids($listTipNumberDoc=NULL,$type_client=NULL,$credit_card=NULL){


  $result = _get_user_doc_uids($listTipNumberDoc, $type_client, $credit_card);

  $mapa=array();
  foreach ($result as $key => $value) {
        $clave =$value->field_profile_document_type_value.$value->field_profile_document_number_value;
        $mapa[$clave]=$value->uid;
   }
  return $mapa;

}

/**
 * @function: _get_user_doc_uids()
 * @description: Devuelve la lista de tipodoc, numdoc y UID segun lo buscado
 * @param $listTipNumberDoc (array): array donde cada fila es  la concatenacion del tipo doc y numdoc
 * @param $type_client (int):  0 no cliente y 1 cliente
 * @param $credit_card (int):  0 sin tarjeta y 1 tarjeta
 */

function _get_user_doc_uids($listTipNumberDoc=NULL,$type_client=NULL,$credit_card=NULL){


  $query = db_select('users', 'user');

  $query->join('field_data_field_profile_document_number', 'dn', 'user.uid = dn.entity_id');
  $query->join('field_data_field_profile_document_type', 'dt', 'user.uid = dt.entity_id');
  $query->join('field_data_field_profile_has_credit_card', 'fcc', 'user.uid = fcc.entity_id');
  $query->join('field_data_field_profile_tipo_cliente', 'ftc', 'user.uid = ftc.entity_id');
  $query->join('users_roles', 'ur', 'ur.uid = user.uid');
    $query = $query->fields('user', array('uid'));

  $query = $query->fields('dt', array('field_profile_document_type_value'));
  $query = $query->fields('dn', array('field_profile_document_number_value'));


  if (isset($listTipNumberDoc)) {

   $query->addExpression(" CONCAT(dt.field_profile_document_type_value,dn.field_profile_document_number_value)", 'tipoNumDoc');
   $query->havingCondition('tipoNumDoc', $listTipNumberDoc, 'IN');  }

  if (isset($credit_card)) {
     $query = $query->condition('fcc.field_profile_has_credit_card_value', $credit_card);
  }
  if (isset($type_client)) {
      $query = $query->condition('ftc.field_profile_tipo_cliente_value', $type_client);
  }


  $query = $query->condition('ftc.entity_type', 'user');
  $query = $query->condition('ur.rid', '11');


  $result = $query
   ->execute()
   ->fetchAll();

  return $result;
}


function _get_user_number_uids($listTipNumberDoc=NULL,$type_client=NULL,$credit_card=NULL){


  $query = db_select('users', 'user');

  $query->join('field_data_field_profile_document_number', 'dn', 'user.uid = dn.entity_id');
  $query->join('field_data_field_profile_document_type', 'dt', 'user.uid = dt.entity_id');
  $query->join('field_data_field_profile_has_credit_card', 'fcc', 'user.uid = fcc.entity_id');
  $query->join('field_data_field_profile_tipo_cliente', 'ftc', 'user.uid = ftc.entity_id');
  $query->join('users_roles', 'ur', 'ur.uid = user.uid');
    $query = $query->fields('user', array('uid'));

  $query = $query->fields('dt', array('field_profile_document_type_value'));
  $query = $query->fields('dn', array('field_profile_document_number_value'));


  if (isset($listTipNumberDoc)) {
   $query->condition('dn.field_profile_document_number_value', $listTipNumberDoc, 'IN');  }

  if (isset($credit_card)) {
     $query = $query->condition('fcc.field_profile_has_credit_card_value', $credit_card);
  }
  if (isset($type_client)) {
      $query = $query->condition('ftc.field_profile_tipo_cliente_value', $type_client);
  }


  $query = $query->condition('ftc.entity_type', 'user');
  $query = $query->condition('ur.rid', '11');


  $result = $query
   ->execute()
   ->fetchAll();

  $mapa=array();
  foreach ($result as $key => $value) {
        $clave = $value->field_profile_document_number_value;
        $mapa[$clave]=$value->uid;
   }
  return $mapa;
}
