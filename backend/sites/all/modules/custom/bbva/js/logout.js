(function($) {

  $(document).ready( function() {

    // Function para cerrar sesion (modificar en responsive tmb)
    $('li a#logoffevent').click(function(event){
      event.preventDefault();
      $('html body').css('cursor', "wait");
      $.ajax({
        method: "POST",
        url: Drupal.settings.basePath + '?q=addlogouttouser',
        async: true,
        dataType: "json",
        success:  function(respuesta) {
          if (respuesta.url) {
            window.location.assign(respuesta.url);
          }else{
            window.location.assign(Drupal.settings.basePath);
          }
        },
        error:function(jqXHR, textStatus, errorThrown) {
          //console.log(jqXHR);
          //console.log(textStatus);
          //console.log(errorThrown);
        }
      });

    });
  });

})(jQuery);
