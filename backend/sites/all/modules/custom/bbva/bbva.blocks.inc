<?php

function bbva_search_form($form, &$form_submit){

  $form['keyword'] = array(
    '#type' => 'textfield',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

function bbva_search_form_submit($form, &$form_state) {
  $redirect_url = 'contents/'.$form_state['values']['keyword'];
  $form_state['redirect'] = $redirect_url;
}


/**
 * Create the widgets block.
 *
 * @todo Move markup to template file.
 */
function bbva_block_widgets() {
  $widgets = array();

  $widgets['twitter'] = bbva_twitter_content();
  $widgets['facebook'] = bbva_facebook_content();
  $widgets['email'] = '<a href="#">Email</a>';

  // Build and return the render-able block.
  $content = '<ul>';
  foreach ($widgets as $key => $value) {
    $content .= '<li class="list-' . $key . '">' . $value . '</li>';
  }
  $content .= '</ul>';

  $block = array(
    'subject' => NULL,
    'content' => $content,
  );
  return $block;
}

/**
 * Return Twitter widget markup.
 */
function bbva_twitter_content() {
  $markup = <<<HTML
<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
HTML;
  return $markup;
}

/**
 * Return Facebook widget markup.
 */
function bbva_facebook_content() {
  $markup = <<<HTML
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
HTML;
  return $markup;
}


/**
 * Implements hook_block_configure().
 */
function bbva_block_configure($delta='') {
  $form = array();

  switch($delta) {
    case 'footer_mobile' :
        $form['footer_mobile_subtitle2'] = array(
		    '#title' => t('Suffix'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_mobile_subtitle2', ''),
	    );
	    $form['footer_mobile_subtitle'] = array(
		    '#title' => t('Subtitle'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_mobile_subtitle', ''),
	    );
	    $form['footer_mobile_description'] = array(
		    '#title' => t('Description'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_mobile_description', ''),
	    );
	    $form['footer_mobile_android'] = array(
		    '#title' => t('Link Android'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_mobile_android', ''),
	    );
	    $form['footer_mobile_iphone'] = array(
		    '#title' => t('Link Iphone'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_mobile_iphone', ''),
	    );
	    $form['footer_mobile_wphone'] = array(
		    '#title' => t('Link Windows Phone'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_mobile_wphone', ''),
	    );
	    break;

	  case 'footer_video' :
	    $form['footer_video_subtitle2'] = array(
		    '#title' => t('Suffix'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_video_subtitle2', ''),
	    );
	    $form['footer_video_subtitle'] = array(
		    '#title' => t('Subtitle'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_video_subtitle', ''),
	    );
	    $form['footer_video_description'] = array(
		    '#title' => t('Description'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_video_description', ''),
	    );
	    $form['footer_video_url'] = array(
		    '#title' => t('Link Video'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_video_url', ''),
	    );
	    break;

	  case 'footer_socials' :
	    $form['footer_socials_subtitle'] = array(
		    '#title' => t('Subtitle'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_socials_subtitle', ''),
	    );
	    $form['footer_socials_description'] = array(
		    '#title' => t('Description'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_socials_description', ''),
	    );
	    $form['footer_socials_face'] = array(
		    '#title' => t('Link Facebook'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_socials_face', ''),
	    );
	    $form['footer_socials_twitter'] = array(
		    '#title' => t('Link Twitter'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_socials_twitter', ''),
	    );
	    break;
	  case 'footer_copyright' :
	    $form['footer_copyright_legal'] = array(
		    '#title' => t('Url legal'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_copyright_legal', ''),
	    );
	    $form['footer_copyright_browser'] = array(
		    '#title' => t('Url browser'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_copyright_browser', ''),
	    );
	    $form['footer_copyright_title'] = array(
		    '#title' => t('Title'),
		    '#type' => 'textfield',
		    '#default_value' => variable_get('footer_copyright_title', ''),
	    );
	    break;
  }

  return $form;
}

/**
 * Implements hook_block_save().
 */
function bbva_block_save($delta = '', $edit = array()) {
  switch($delta) {
    case 'footer_mobile' :
      // Saving text     
      variable_set('footer_mobile_subtitle2', $edit['footer_mobile_subtitle2']); 
      variable_set('footer_mobile_subtitle', $edit['footer_mobile_subtitle']);
	  variable_set('footer_mobile_description', $edit['footer_mobile_description']);
	  variable_set('footer_mobile_android', $edit['footer_mobile_android']);
	  variable_set('footer_mobile_iphone', $edit['footer_mobile_iphone']);
	  variable_set('footer_mobile_wphone', $edit['footer_mobile_wphone']);
      break;
    case 'footer_video' :
      // Saving text
      variable_set('footer_video_subtitle2', $edit['footer_video_subtitle2']);      
      variable_set('footer_video_subtitle', $edit['footer_video_subtitle']);
      variable_set('footer_video_subtitle2', $edit['footer_video_subtitle2']);
	  variable_set('footer_video_description', $edit['footer_video_description']);
	  variable_set('footer_video_url', $edit['footer_video_url']);
      break;
    case 'footer_socials' :
      // Saving text      
      variable_set('footer_socials_subtitle', $edit['footer_socials_subtitle']);
		  variable_set('footer_socials_description', $edit['footer_socials_description']);
		  variable_set('footer_socials_face', $edit['footer_socials_face']);
		  variable_set('footer_socials_twitter', $edit['footer_socials_twitter']);
      break;
    case 'footer_copyright' :
      // Saving text      
      variable_set('footer_copyright_legal', $edit['footer_copyright_legal']);
		  variable_set('footer_copyright_browser', $edit['footer_copyright_browser']);
		  variable_set('footer_copyright_title', $edit['footer_copyright_title']);
      break;
  }
}

/** **/
function bbva_view($delta) {
	$block = array();

	if ($delta == 'footer_mobile') {
	  // Block output in HTML with div wrapper
	  $block = '<div class="field-subtitle">'.variable_get('footer_mobile_subtitle').'</div>';
    $block .= '<div class="field-description">'.variable_get('footer_mobile_description').'</div>';
    $block .= '<div class="field-mobiles"><ul>';
    $block .= '<li class="link-android"><a  target="_blank" href="'.variable_get('footer_mobile_android').'">android</a></li>';
    $block .= '<li class="link-iphone"><a target="_blank"   href="'.variable_get('footer_mobile_iphone').'">iphone</a></li>';
    $block .= '</ul></div>';
    $block .= '<span class="go-mobile"> '.variable_get('footer_mobile_subtitle2').'</span>';
	}

	if($delta == 'footer_video'){

    $block = '<div class="field-subtitle">'.variable_get('footer_video_subtitle').'</div>';
    $block .= '<div class="field-description">'.variable_get('footer_video_description').'</div>';
    //$block .= '<div class="field-videos">'.variable_get('footer_video_url').'</div>';
	$block .= '<a href="'.url('video-promocion').'" class="colorbox-node"><div class="field-videos"></div></a>';
    $block .= '<span class="go-video"> '.variable_get('footer_video_subtitle2').'</span>';
	}

	if($delta == 'footer_socials'){
    $block = '<div class="field-subtitle">'.variable_get('footer_socials_subtitle').'</div>';
    $block .= '<div class="field-description">'.variable_get('footer_socials_description').'</div>';
    $block .= '<div class="field-url"><ul>';
    $block .= '<li class="link-facebook"><a target="_blank" href="'.variable_get('footer_socials_face').'">face</a></li>';
    $block .= '<li class="link-twitter"><a target="_blank" href="'.variable_get('footer_socials_twitter').'">twitter</a></li>';
    $block .= '</ul></div>';
	}

	if($delta == 'footer_copyright'){
	    $block = '<div class="footer-bottom">';
	    $block .= '<div class="field-group-right">'.variable_get('footer_copyright_title').'</div>';
	    $block .= '</div>';
	}

	return $block;
}
