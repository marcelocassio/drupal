<?php

/*
 * Store and retrive session variable
 * Usage:
 *    Save a variable in Session: lists_session("s_key", "value");
 *    Retrieve the value:     lists_session("s_key", "value");
*/
function lists_session($key, $value = NULL) {
    static $storage;
    if ($value) {
        $storage[$key] = $value;
        $_SESSION['lists'][$key] = $value;

        // I use 'lists' in case some other module uses 'type' in $_SESSION
    }
    else if (empty($storage[$key]) && isset($_SESSION['lists'][$key])) {
        $storage[$key] = $_SESSION['lists'][$key];
    }
    return $storage[$key];
}

/*
 * Devuelve el Id de un Coupon_Promo que tiene el Benefit asociado
 * Si no encuentra ninguno devuelve FALSE
 * Usage:
 *    Pasar por parámetro el Benefit Id.
*/
function Devolver_CouponPromo_Asociado_Benefit($benefit) {
    $result = db_query("SELECT entity_id as id FROM field_data_field_coupon_promo_benefit WHERE field_coupon_promo_benefit_target_id=:benefit;", array(':benefit' => $benefit));
    if ($result->rowCount()) {
        foreach ($result as $k => $record) {
            $entity_type = 'coupon_promo';
            $entity = entity_load($entity_type, array($record->id));

            //$wrapper = entity_metadata_wrapper($entity_type, $entity);
            return $entity;
        }
    }
    else {
        return FALSE;
    }
}

/*
 * Devuelve el Id de un Coupon_Promo que tiene el Benefit asociado
 * Si no encuentra ninguno devuelve FALSE
 * Usage:
 *     Pasar por parámetro el Benefit Id.
*/
function Devolver_CouponPromo_Asociado_Benefit_ID($benefit) {
    $result = db_query("SELECT entity_id as id FROM field_data_field_coupon_promo_benefit WHERE field_coupon_promo_benefit_target_id=:benefit;", array(':benefit' => $benefit));
    if ($result->rowCount()) {
        foreach ($result as $k => $record) {
            return $record->id;
        }
    }
    else {
        return FALSE;
    }
    return FALSE;
}

/*
 * Devuelve TRUE O FALSE dependiendo si hay vouchers disponibles
 * para el promo_coupon
 * Usage:
 *    Pasar por parámetro el Coupon_id.
*/
function CheckVoucher_Available($coupon_id) {
    $result_vouchers = db_query("SELECT v.id FROM eck_voucher v
      JOIN field_data_field_voucher_state fvs on fvs.entity_id=v.id
      JOIN field_data_field_voucher_coupon_promo fcp on fcp.entity_id=v.id
      WHERE fcp.field_voucher_coupon_promo_target_id=:promo_id and fvs.field_voucher_state_value=2 and v.id not in (SELECT entity_id FROM field_data_field_voucher_user) LIMIT 1;", array(':promo_id' => $coupon_id));

    if ($result_vouchers->rowCount()) {
        return 1;
    }
    else {
        return 0;
    }
}

/*
 * Devuelve TRUE O FALSE dependiendo si hay vouchers disponibles
 * para el promo_coupon
 * Usage:
 *     Pasar por parámetro el Coupon_id.
*/
function CheckVoucher_Available_User($coupon_id, $uid) {
    $result_vouchers = db_query("SELECT v.id FROM eck_voucher v
      JOIN field_data_field_voucher_state fvs on fvs.entity_id=v.id
      JOIN field_data_field_voucher_coupon_promo fcp on fcp.entity_id=v.id
      JOIN field_data_field_voucher_user         fvu on fvu.entity_id=v.id
      WHERE fcp.field_voucher_coupon_promo_target_id=:promo_id
              and fvu.field_voucher_user_target_id=:uid LIMIT 1;", array(':promo_id' => $coupon_id, ':uid' => $uid));
    if ($result_vouchers->rowCount()) {
        return false;
    }
    else {
        return true;
    }
}

function devolver_valor_cupon_promo($benefit_id) {

    $query = db_select('field_data_field_coupon_promo_benefit', 'cb');
    $query->innerJoin('eck_coupon_promo', 'cp', 'cp.id = cb.entity_id');
    $query->innerJoin('field_data_field_coupon_promo_discount', 'cpd', 'cpd.entity_id = cp.id');

    $query = $query->condition('field_coupon_promo_benefit_target_id', $benefit_id, '=')->fields('cpd', array('field_coupon_promo_discount_value'))->execute()->fetchField();
    return $query;
}

function _get_uid_whit_numdoc($tipDoc, $numDoc) {

    $query = db_select('field_data_field_profile_document_number', 'fdpd');

    $query->innerJoin('field_data_field_profile_document_type', 'fdqt', 'fdqt.entity_id = fdpd.entity_id');

    $query = $query->condition('fdpd.field_profile_document_number_value', trim($numDoc));
    $query = $query->condition('fdqt.field_profile_document_type_value', trim($tipDoc));

    $query = $query->fields('fdpd', array('entity_id'));

    $result = $query->execute()->fetchAll();
    foreach ($result as $kr => $record) {
        return $record->entity_id;
    }
    return '0';
}

/**
 * @function: _get_category_by_suip_id()
 * @description: busca un término segun el id de suip
 * @param $vid (int): suip id
 */
function _get_term_by_suip_id($vid = 0) {
  
    $query = db_select('taxonomy_term_data', 't');
    $query->innerJoin('field_data_field_category_id', 'fcid', 'fcid.entity_id = t.tid');
    $query = $query->condition('t.vid', 9, '=');
    $query = $query->condition('fcid.field_category_id_value', $vid, '=');
    $query = $query->fields('t', array('tid'));

    $result = $query->execute()->fetchField();

    return $result;
}

/**
 * @function: _get_list_tiponumdoc_form_file()
 * @description: lee un archivo con numdoc y tipo doc y retorna una lista con la concatenacion tipoDoc+numDoc
 * @param $fileUrl (string): url del archivo
 */
function _get_list_tiponumdoc_form_file($fileUrl = null) {

    $listTipoNumDoc = array();
    $file_listado = fopen(drupal_realpath($fileUrl), "r") or exit("Unable to open file!");

    while (!feof($file_listado)) {
        $line_of_text_users = fgets($file_listado);
        if (!empty($line_of_text_users)) {
            $user_array = explode(';', $line_of_text_users);
            $numdoc = str_replace(array("\r\n", "\r", "\n"), "", $user_array[1]);

            $listTipoNumDoc[] = $user_array[0] . $numdoc;
        }
    }

    return $listTipoNumDoc;
}
function _get_list_numdoc_form_file($fileUrl = null) {

    $listTipoNumDoc = array();
    $file_listado = fopen(drupal_realpath($fileUrl), "r") or exit("Unable to open file!");

    while (!feof($file_listado)) {
        $line_of_text_users = fgets($file_listado);
        if (!empty($line_of_text_users)) {
            $user_array = explode(';', $line_of_text_users);
            $numdoc = str_replace(array("\r\n", "\r", "\n"), "", $user_array[1]);

            $listTipoNumDoc[] = $numdoc;
        }
    }

    return $listTipoNumDoc;
}


function bbva_comillas($cadena) {
    return "\"".$cadena."\"";
}

function _get_list_num_altm_form_file($fileUrl = null) {

    $listTipoNumDoc = array();
    $file_listado = fopen(drupal_realpath($fileUrl), "r") or exit("Unable to open file!");

    while (!feof($file_listado)) {
        $line_of_text_users = fgets($file_listado);
        if (!empty($line_of_text_users)) {
            $listNumAlt[] = $line_of_text_users;
        }
    }

    return $listNumAlt;
}


function bbva_get_document_altamira_type_list() {
  $documentType2altamira = array();
  $documentType2altamira['DNI'] = '00';
  $documentType2altamira['CI'] = '01';
  $documentType2altamira['LE'] = '25';
  $documentType2altamira['LC'] = '26';
  $documentType2altamira['PAS'] = '30';
  $documentType2altamira['DNI_MASC'] = '27';
  $documentType2altamira['DNI_FEM'] = '28';
  $documentType2altamira['DNX'] = '31';
  return $documentType2altamira;
}

/**
* Tranforma el tipo de documento short de Frances Net a Codigo Altamira
* @param $document_type_short Tipo de documento Short
*
**/
function bbva_get_altamira_code($document_type_short) {
  $documentType2altamira = bbva_get_document_altamira_type_list();
  if (empty($documentType2altamira[$document_type_short])) {
    return NULL;
  } else {
    return $documentType2altamira[$document_type_short];
  }
}

function bbva_convert_tipo_doc_altamira($tipo_doc, $num_doc, $sexo = null) {
  $tipo_doc = bbva_validate_tipo_doc($tipo_doc, $num_doc, $sexo);
  $tipo_doc = bbva_validate_doc_type_extranjero($tipo_doc, $num_doc);
  $tipo_doc_altamira = bbva_get_altamira_code($tipo_doc);
  return $tipo_doc_altamira;
}

function bbva_validate_tipo_doc($tipo_doc, $num_doc, $sexo = null) {
  $num = (int)$num_doc;
  $tipo_doc_res = $tipo_doc;
 
  if (isset($sexo)) {
    if ($tipo_doc == 'DNI' && $num_doc < 10000000) {
      if (strtoupper($sexo) == 'F') {
        $tipo_doc_res = 'DNI_FEM';
      } else if (strtoupper($sexo) == 'M') {
        $tipo_doc_res = 'DNI_MASC';
      }
    }
  }

  return $tipo_doc_res;
}

function bbva_validate_numero_documento($tipo_doc, $num_doc, $sexo = null) {
  $valido = FALSE;
  $tipo_doc = bbva_validate_tipo_doc($tipo_doc, $num_doc, $sexo);
  $tipo_doc = bbva_validate_doc_type_extranjero($tipo_doc, $num_doc);

  if ($tipo_doc == 'DNI' && ($num_doc > 10000000 && $num_doc < 53000000)) {
    $valido = TRUE;
  } else if ($tipo_doc == 'DNX' && ($num_doc > 90000000 && $num_doc < 99000000)) {
    $valido = TRUE;
  } else if (($tipo_doc == 'DNI' || $tipo_doc == 'DNI_FEM' || $tipo_doc == 'DNI_MASC' || $tipo_doc == 'LE' || $tipo_doc == 'LC') &&
            ($num_doc > 100000 && $num_doc < 10000000)) {
    $valido = TRUE;
  } else if ($tipo_doc == 'PAS' && $num_doc > 0) {
    $valido = TRUE;
  }
  return $valido;
}


function bbva_validate_doc_type_extranjero($document_type_short, $document_number) {
  $num = (int)$document_number;
  if ($num > 90000000 && $document_type_short == 'DNI') {
    $document_type_short = 'DNX';
  }
  return $document_type_short;
}

function bbva_compatible_doctype_from_altamira($document_type_altamira) {
  $documentType2short['00'] = 'DNI';
  $documentType2short['01'] = 'CI';
  $documentType2short['25'] = 'LE';
  $documentType2short['26'] = 'LC';
  $documentType2short['30'] = 'PAS';
  $documentType2short['27'] = 'DNI';
  $documentType2short['28'] = 'DNI';
  $documentType2short['31'] = 'DNI';
  if (empty($documentType2short[$document_type_altamira])) {
    return NULL;
  } else {
    return $documentType2short[$document_type_altamira];
  }
}

/* Se pasa código y regresa formato completo, ejem: Recibe: 80 / Retorna: 080|ARGENTINA */
function bbva_nacionalidad_completa($nacionalidad_codigo){
  $user_nac_3c = str_pad((int)$nacionalidad_codigo, 3, '0', STR_PAD_LEFT);
  $nacionalidad =  $user_nac_3c. "|";
  $nacionalidades_list = bbva_subscription_get_nationality_type_list();

  if(isset($nacionalidades_list[$user_nac_3c])) {
    $nacionalidad .= $nacionalidades_list[$user_nac_3c];
  }
  return $nacionalidad;
}

function bbva_short_hash($texto) {
  $hash = hash('sha256', $texto);
  $short_hash = substr($hash,0,6);
  return $short_hash;
}

function bbva_sanitizar($texto) {
  $sanatizado = preg_replace('/[^A-Za-z0-9\-\']/', '', $texto);
  return $sanatizado;
}

function bbva_encode($texto) {
  $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E','Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
  $texto = trim(strtr($texto, $unwanted_array));
  return $texto;
}

function bbva_proxy($url, $params=NULL){
  try {
    $ch = curl_init();

    $proxy_ip = '172.29.92.203';
    $proxy_port = '8082';

    if (isset($params)) {
      $request_to = http_build_query($params);
      $url .= "?".$request_to;
    }

    $CURL_OPTS = array(
      CURLOPT_CONNECTTIMEOUT => 10,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_TIMEOUT => 60,
      CURLOPT_URL => $url,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => FALSE,
      CURLOPT_FOLLOWLOCATION => 1,
      CURLOPT_PROXY  => $proxy_ip,
      CURLOPT_PROXYPORT  => $proxy_port,
      CURLOPT_HTTPHEADER => array("Content-type: application/json"),
      CURLOPT_POST => false,
      CURLOPT_VERBOSE => true,
    );

    curl_setopt_array($ch, $CURL_OPTS);

    $result = curl_exec($ch);

    if(curl_error($ch)){
      watchdog('proxy', 'Proxy_error: %value', array('%value' => htmlspecialchars(print_r(curl_error($ch),true))));
    }

    curl_close($ch);
    return $result;
  } catch (Exception $e) {
    watchdog('proxy', 'Proxy_error_exception: %value', array('%value' => htmlspecialchars(print_r($e,true))));
    throw new Exception("En este momento no podemos ejecutar esta operación", 2);
  }

}


function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
  $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
  $str_end = "";
  if ($lower_str_end) {
    $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
  }
  else {
    $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
  }
  $str = $first_letter . $str_end;
  return $str;
}


function mb_ucwords($string) {
  return mb_ereg_replace("(^| )(\w)","\"\\1\".mb_strtoupper(\"\\2\")",$string,'e');
}

function bbva_get_imagenes_rubro_from_tid($tid_padre) {
  // Si es un sub-rubro, tendrá un padre, por ende se guardan las imágenes del padre
  $term_to_img = taxonomy_term_load($tid_padre);
  $all_fids_p = $term_to_img->field_category_image['und'];
  $fid_p = array();
  if (!empty($all_fids_p)) {
    foreach ($all_fids_p as $value_fid_p) {
      $fid_p[] = $value_fid_p['fid'];
    }
  }
  return $fid_p;
}

function bbva_searchForIdInArray($id, $array, $search_field) {
  foreach ($array as $key => $val) {
    if ($val[$search_field] == $id) {
      return $key;
    }
  }
  return null;
}

function bbva_searchForValueInArray($value, $array, $search_field) {
  $new_array = array();
  foreach ($array as $key => $val) {
    if (is_array($val[$search_field])){
      if (in_array($value, $val[$search_field])) {
        $new_array[] = $val;
      }
    }else{
      if ($val[$search_field] == $value) {
        $new_array[] = $val;
      }
    }
  }
  return $new_array;
}