<div id="list-experiences">
  <?php
    foreach ($vars as $key => $item) : ?>
      <div class="row">
        <div class="field-image"><a href="<?php print url('node/'.$item->nid); ?>"><img src="<?php print $item->image_path ?>"/></a></div>
        <div class="group-fields">
          <?php if (!empty($item->title)) : ?>
            <div class="field-title"><a href="<?php print url('node/'.$item->nid); ?>"><?php print $item->title; ?></a></div>
          <?php endif ?>
          <?php if (!empty($item->field_experience_volanta_value)) : ?>
            <div class="field-volanta"><?php print $item->field_experience_volanta_value; ?></div>
          <?php endif ?>
        </div>
      </div>  
  <?php endforeach ?>
</div>