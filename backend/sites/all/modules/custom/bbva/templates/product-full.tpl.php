<div id="parallax">
  <?php foreach ($vars as $key => $items) : ?>
    <?php foreach ($items as $key => $item) : ?>
      <?php //print_r($item); 
        $image_path = image_style_url('experience_big', $item->field_product_image_bg['und'][0]['uri']);
      ?>
      <div class="intro<?php print $item->field_experience_weight_value ?> intro-content" data-speed="5" data-type="background" 
      style="background: url(<?php print $image_path ?>)"></div>

      <!-- Content Parallax -->
      <div class="content-node-parallax intro-unit item-content-<?php print $item->field_experience_weight_value ?>" data-speed="2" data-type="background">
        <?php if (!empty($item->title)) : ?>
          <div class="group-title">
            <div class="field-name-field-experience-title">
              <?php print $item->title; ?>
            </div>
          </div>
        <?php endif ?>
        

        <div class="parallax-inner">
          <div class="group-left">
            <div class="section">
              <div class="field-name-field-experience-bajada">
                <?php 
                  if (!empty($item->field_product_list_descriptions)) {
                    print $item->field_product_description_short['und'][0]['value'];
                  }
                ?>
              </div>
            </div>
          </div>

          <div class="group-right">
            <div class="section">
              <div class="field-name-field-experience-link">
                <?php print l(t('Ver mas'), 'node/' . $item->nid) ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach ?>
  <?php endforeach ?>
</div>