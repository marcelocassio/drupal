<?php

  $url = isset($vars->nid) ? url('node/'.$vars->nid) : null;
  $title = isset($vars->title) ? $vars->title : null;
  $volanta = isset($vars->field_experience_volanta['und'][0]) ? $vars->field_experience_volanta['und'][0]['value'] : null;
  $path_image = isset($vars->field_experience_image_list['und'][0]) ? $vars->field_experience_image_list['und'][0]['uri'] : null;

  $themeImage = theme_image_style(array(
    'style_name' => 'experience_teaser', 
    'path' => $path_image
  ));
?>

<div class="group-fields">
  <div class="field-title">
    <a href="<?= $url ?>">
      <?= $title ?>
    </a>
  </div>
  <div class="field-volanta">
    <?= $volanta ?>
  </div>
</div>
<div class="field-image">
  <a href="<?= $url ?>">
    <?= $themeImage ?>
  </a>
</div>
