<div class="tabs-carrusel">
  <ul>
    <?php if(isset($vars)) { ?>
      <li class="tab-gallery active">Galeria</li>
    <?php } ?>
    <?php if($video != '') { ?>
      <li class="tab-video">Video</li>
    <?php } ?>
  </ul>
</div>
<div id="experience-carrusel">
	<div class="experience-header">
    <div class="header-carousel" style="width: 100%; height: auto; margin: 0 auto; overflow: hidden;">
    	<ul class="jcarousel-experience">
    	  <?php foreach ($vars as $key => $item) : ?>
    	  	<li>
    	  	  <div class="field-image"><?php print theme_image_style(array('style_name' => 'experience_logo', 'path' => $item->field_experience_image['und'][0]['uri'])); ?></div>
    	  	</li>
    	  <?php endforeach ?>
    	</ul>
    </div>
	</div>
</div>
<?php if($video != '') { ?>
<div id="video-header" style="display: none;">
  <iframe width="100%" height="380" src="https://www.youtube.com/embed/<?php print $video[1] ?>" frameborder="0" allowfullscreen></iframe>
</div>
<?php } ?>