<?php
  $vars = node_load($node_id);
  $image_path_app = isset($vars->field_experience_image) ? $vars->field_experience_image['und'][0]['uri'] : null;
  $image_path = image_style_url('app_slide', $image_path_app);
  $url_fake = "https://go.bbva.com.ar/fgo/sorteo/".$node_id;
  $url_actual = "https://go.bbva.com.ar/fgo/#/sorteos/".$node_id;
  $experience_description = isset($vars) && $vars != null ? $vars->field_experience_descripcion['und'][0]['value'] : null;
  $title = isset($vars->title) ? $vars->title : null;

  // Headers para redes sociales
    // FACEBOOK
  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image',
      'content' =>  $image_path,
    ),
  );

  $element2 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:title',
      'content' =>  $title,
    ),
  );

  $element3 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:description',
      'content' => '#AlertaGO '. $experience_description,
    ),
  );
  $element4 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:type',
      'content' => 'website',
    ),
  );

  $element5 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:url',
      'content' => $url_fake,
    ),
  );

  drupal_add_html_head($element2, 'Test2');
  drupal_add_html_head($element3, 'Test3');
  drupal_add_html_head($element, 'Test');
  drupal_add_html_head($element4, 'Test4');
  drupal_add_html_head($element5, 'Test5');

  // TWITTER

  $elementtw = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'twitter:card',
      'content' =>  'summary_large_image',
    ),
  );

  $elementtw2 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'twitter:site',
      'content' =>  'Go',
    ),
  );
  $elementtw3 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'twitter:creator',
      'content' => '@Go',
    ),
  );
  $elementtw4 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'twitter:title',
      'content' => $title,
    ),
  );

  $elementtw5 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'twitter:description',
      'content' => '#AlertaGO '.$experience_description,
    ),
  );

  $elementtw6 = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'twitter:image',
      'content' =>  $image_path,
    ),
  );

  drupal_add_html_head($elementtw5, 'Testw5');
  drupal_add_html_head($elementtw6, 'Testw6');
  drupal_add_html_head($elementtw2, 'Testw2');
  drupal_add_html_head($elementtw3, 'Testw3');
  drupal_add_html_head($elementtw, 'Testw');
  drupal_add_html_head($elementtw4, 'Testw4');

?>
<script type="text/javascript">
var url = "<?php echo $url_actual; ?>";
window.location = url;
</script>
