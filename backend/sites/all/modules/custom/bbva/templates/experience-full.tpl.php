<div id="experience-content">

  <?php
    $image_path_app = isset($vars->field_experience_image['und']) ? $vars->field_experience_image['und'][0]['uri'] : null;

    $image_path = image_style_url('app_slide', $image_path_app);

  ?>
  <div class="field-image" style="background: url(<?=$image_path?>) 0 0 / 100%;">
  </div>
  <div class="group-fields">
    <div class="field-title"><?php print $vars->title ?></div>
    <div class="field-volanta"><?php print $vars->field_experience_volanta['und'][0]['value'] ?></div>
    <?php if(!empty($vars->field_experience_fija_link['und'][0]['title'])) : ?>
      <div class="field-link"><a target="_blank" href="<?php print $vars->field_experience_fija_link['und'][0]['display_url']?>"><?php print $vars->field_experience_fija_link['und'][0]['title'] ?></a></div>
    <?php endif ?>
  </div>
</div>


