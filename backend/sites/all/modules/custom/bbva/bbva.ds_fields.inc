<?php

/**
 * Implements hook_ds_fields_info().
 */
function bbva_ds_fields_info($entity_type) {
  $fields = array();
  $fields['shares'] = array(
    'title' => t('Shares'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => '_bbva_ds_field_shares',
    'properties' => array(
      'formatters' => array(
        'default' => 'Default',
        'full' => 'full'
      )
    )
  );

  $fields['verlocales'] = array(
    'title' => t('Ver Locales'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => '_bbva_ds_field_verlocales',
    'properties' => array(
      'formatters' => array(
        'default' => 'Default'
      )
    )
  );

  $fields['terminos'] = array(
    'title' => t('Terminos y Condiciones'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => '_bbva_ds_field_terminos',
    'properties' => array(
      'formatters' => array(
        'default' => 'Default'
      )
    )
  );

  $fields['experience'] = array(
    'title' => t('Experience Full'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => '_bbva_ds_field_experience_full',
    'properties' => array(
      'formatters' => array(
        'default' => 'Default'
      )
    )
  );

  $fields['experience_teaser'] = array(
    'title' => t('Experience Teaser'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => '_bbva_ds_field_experience_teaser',
    'properties' => array(
      'formatters' => array(
        'default' => 'Default'
      )
    )
  );

  $fields['experience_carrusel'] = array(
    'title' => t('Experience Carrusel'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => '_bbva_ds_field_experience_carrusel',
    'properties' => array(
      'formatters' => array(
        'default' => 'Default'
      )
    )
  );

  if ($entity_type == 'benefit') {
    $fields['benefit_teaser'] = array(
      'title' => t('Benefit Teaser'),
      'field_type' => DS_FIELD_TYPE_FUNCTION,
      'function' => '_bbva_ds_field_benefit_teaser',
      'properties' => array(
        'formatters' => array(
          'default' => 'Default',
          'mini' => 'Mini',
          'soloxhoy' => 'Solo x Hoy',
          'full' => 'Full'
        )
      )
    );

    return array(
      $entity_type => $fields
    );
  }

  if ($entity_type == 'voucher') {
    $fields['cupon'] = array(
      'title' => t('Cupon'),
      'field_type' => DS_FIELD_TYPE_FUNCTION,
      'function' => '_bbva_ds_field_cupon',
      'properties' => array(
        'formatters' => array(
          'default' => 'Default'
        )
      )
    );

    return array(
      $entity_type => $fields
    );
  }
  return array(
    'node' => $fields
  );
}

/*
 * Show shares content.
 */
function _bbva_ds_field_shares($field) {

  // Get information about the entity being rated and the formatter we're using.
  $entity    = $field['entity'];
  $formatter = $field['formatter'];

  // Change the output display based on the formatter being used.
  if ($formatter == 'default') {
    $output = '<div class="share-default">Compartir: Share default</div>';
  } elseif ($formatter == 'full') {
    $output = '<div class="share-face">
                <div class="share-icon">
                  <div class="fb-share-button" data-href="http://go.bbva.com.ar/fgo/' . $entity->nid . '"></div>
                </div>
              </div>';
    $output .= "<div class='share-twitter'>
                  <div class='share-icon'>
                  </div>
                </div>";
  }
  return $output;
}

/*
 * Title ver Locales.
 */
function _bbva_ds_field_verlocales($field) {
  $output = 'Ver locales';
  return $output;
}

/*
 * Title Terminos.
 */
function _bbva_ds_field_terminos($field) {
  $output = 'Terminos y condiciones';
  return $output;
}

/*
 * Experiencia full sola o con sorteos
 */
function _bbva_ds_field_experience_full($field) {
  $vars = $field['entity'];
  global $user;
  $consultarParticipacion = 0;

  // Si es un sorteo
  if ($vars->field_experience_es_sorteo['und'][0]['value'] == 1) {

    // Obtenemos el caché de participantes del sorteo
    $cachesorteo_temp = cache_get('jsonSorteos_'.$vars->nid,'cache_api');

    if (isset($cachesorteo_temp->data)) {
      if (array_key_exists($user->uid, $cachesorteo_temp->data)) {
        $consultarParticipacion = 1;
      }
    }

    return theme('experience_full_sorteo', array(
      'vars' => $vars, 'usuario_participa_sorteo' => $consultarParticipacion
    ));
  } else {
    return theme('experience_full', array(
      'vars' => $vars
    ));
  }
}

/*
 * Title Terminos.
 */
function _bbva_ds_field_experience_teaser($field) {
  return theme('experience_teaser', array(
    'vars' => $field['entity']
  ));

}

/*
 * Experience Full Carrusel.
 */
function _bbva_ds_field_experience_carrusel($field) {
  
  $theme = false;
  $vars = array();
  $video = '';
   
  if (!empty($field['entity']->field_experience_video)) {
    $video = $field['entity']->field_experience_video['und'][0]['uri'];
    $video = explode('v/', $video);
  }
  
  $items = isset($field['entity']->field_experience_slide['und']) ? $field['entity']->field_experience_slide['und'] : null;

  if($items){
    foreach ($items as $item) {
      $vars[] = field_collection_field_get_entity($item);
    }
  }

  //Si es sorteo no mostrar el carrousel
  if ($field['entity']->field_experience_es_sorteo['und'][0] != 1) {
    $theme = theme('experience_carrusel', array(
      'vars' => $vars,
      'video' => $video
    ));
  } 
  
  return $theme;
}

/*
 * Benefit Teaser
 */
function _bbva_ds_field_benefit_teaser($field) {
  $rubros = _get_category();

  $entity = $field['entity'];
  $formatter = $field['formatter'];
  $wrapper = entity_metadata_wrapper('benefit', $entity->id);

  // Relaciono todas las entities con beneficios
  $branch_shop_id = $entity->field_benefit_shop_branchs['und'][0]['target_id'];
  $branch_shop = entity_load('benefit_shop_branch', array($branch_shop_id));

  $shopping_id = isset($entity->field_benefit_shopping['und']) ? $entity->field_benefit_shopping['und'][0]['target_id'] : null;

  $is_shopping = false;

  foreach ($branch_shop as $key => $items) {
    $shop_id = $items->field_bsb_shop['und'][0]['target_id'];
    $shops = entity_load('shop', array($shop_id));

    // Define vars
    $shop['title'] = '';
    $shop['logo'] = '';
    $shop['image'] = '';

    foreach ($shops as $key => $item) {
      $shop['title'] = $item->title;
      $shop['logo'] = $item->field_shiop_mark_tip;
      $shop['image'] = $item->field_shop_imagen;

      $categorys = count($item->field_shop_category['und']);
      $term = taxonomy_term_load($item->field_shop_category['und'][0]['tid']);

      $shop['rubroImageLogo'] = isset($term->field_category_logo_web) ? $term->field_category_logo_web : null;
      $shop['rubroImage'] = isset($term->field_category_image) ? $term->field_category_image : null;

      $shop_parent = isset($term->field_category_parent) ? $term->field_category_parent : null;

      if ((empty($shop['rubroImageLogo']) || $categorys > 1) && !empty($shop_parent)) {
        $shop['rubroImageLogo'] = $rubros[$shop_parent['und'][0]['value']]->field_category_logo_web;
      }
      if ((empty($shop['rubroImage']) || $categorys > 1) && !empty($shop_parent)) {
        $shop['rubroImage'] = $rubros[$shop_parent['und'][0]['value']]->field_category_image;
      }
    }
  }

  if (!empty($entity->field_benfit_image)) {
    $benefit['image'] = $entity->field_benfit_image;
  } elseif (!empty($shop['image'])) {
    $benefit['image'] = $shop['image'];
  } else {
    $benefit['image'] = $shop['rubroImage'];
  }

  if (!empty($entity->field_benefit_logo_img)) {
    $benefit['logo'] = $entity->field_benefit_logo_img;
  } elseif (!empty($shop['logo'])) {
    $benefit['logo'] = $shop['logo'];
  } else {
    $benefit['logo'] = $shop['rubroImageLogo'];
  }

  if (!empty($shopping_id)) {
    $shopping      = entity_load_single('shopping', $shopping_id);
    $shop['title'] = $shopping->title;

    if (!empty($shopping->field_shopping_logo)) {
      $benefit['logo'] = $shopping->field_shopping_logo;
    }

    if (!empty($shopping->field_shopping_imagen)) {
      $benefit['image'] = $shopping->field_shopping_imagen;
    }
    $is_shopping = true;
  }

  $porcentajeGo = isset($entity->field_benefit_porcent['und']) ? $entity->field_benefit_porcent['und'][0]['value'] : null;
  $proncentajeGrl = isset($entity->field_benefit_grl_procent['und']) ? $entity->field_benefit_grl_procent['und'][0]['value'] : null;

  $porcentajeAmostrar;
  
  if (!empty($porcentajeGo)) {
    $porcentajeAmostrar = $porcentajeGo;
  } elseif (!empty($proncentajeGrl)) {
    $porcentajeAmostrar = $proncentajeGrl;
  } else {
    $coupon_promo = Devolver_CouponPromo_Asociado_Benefit($entity->id);
    if ($coupon_promo !== FALSE) {
      foreach ($coupon_promo as $key => $items) {
        $coupon_promo_id        = $items->id;
        $coupon_promo_descuento = $items->field_coupon_promo_discount['und'][0]['value'];
      }
    }
    $porcentajeAmostrar = isset($coupon_promo_descuento) ? $coupon_promo_descuento : null;
  }

  $category['image'] = '';

  //Muestro de las diversas formas
  if ($formatter == 'default') {
    // Default
    if (!empty($benefit['image'])) {
      $build = '<div class="field-image"><a href="' . url('benefit/benefit/' . $entity->id) . '#section-benefit"><img src="' . image_style_url('benefit_teaser', $benefit['image']['und'][0]['uri']) . '"></a></div>';
    }

    $build .= '<div class="group-fields">';
    if (!empty($benefit['logo'])) {
      $build .= '<div class="group-fields-logo"><a href="' . url('benefit/benefit/' . $entity->id) . '#section-benefit"><img src="' . image_style_url('soloxhoy_small', $benefit['logo']['und'][0]['uri']) . '"></a></div>';
    } else {
      $build .= '<div class="group-fields-title">' . l($shop['title'], 'benefit/benefit/' . $entity->id) . '</div>';
    }
    if (!empty($porcentajeAmostrar)) {
      $build .= '<div class="group-fields-promotion"><span class="menos">-</span>' . $porcentajeAmostrar . '</div>';
      $build .= '<div class="group-fields-duration"><span class="porcent">%</span></div>';
      $build .= ' <div class="field-quotes">' . $wrapper->field_benefit_promotion_value->label() . '</div>';
    } else {
      $build .= ' <div class="field-quotes sin-porcent">' . $wrapper->field_benefit_promotion_value->label() . '</div>';
    }
    $build .= '<a href="' . url('benefit/benefit/' . $entity->id) . '#section-benefit"></div>';
  } elseif ($formatter == 'mini') {
    // Mini
    $cucarda = '';

    $entitytype = entity_load_single('type_benefit', $entity->field_benefit_type['und'][0]['target_id']);
    if ($entitytype->field_type_benefit_id['und'][0]['value'] == 24) {
      $cucarda = 'featured';
    }
    $build = '<div class="field-cucarda ' . $cucarda . '"></div>';
    $build .= '<div class="ver-mas-destacados"><a href="' . url('benefit/benefit/' . $entity->id) . '#section-benefit">Ver mas</a></div>';

    if (!empty($benefit['logo'])) {
      $build .= '<div class="field-title field-logo"><img src="' . image_style_url('soloxhoy_small', $benefit['logo']['und'][0]['uri']) . '"></div>';
    } else {
      $build .= '<div class="field-title field-logo-empty">' . $shop['title'] . '</div>';
    }
    $build .= '<div class="group-fields">';
    if (!empty($porcentajeAmostrar)) {
      $build .= '<div class="group-fields-promotion"><span class="menos">-</span>' . $porcentajeAmostrar . '</div>';
      $build .= '<div class="group-fields-duration"><span class="porcent">%</span></div>';
      $build .= ' <div class="field-quotes">' . $wrapper->field_benefit_promotion_value->label() . '</div>';
    } else {
      $build .= ' <div class="field-quotes sin-porcent">' . $wrapper->field_benefit_promotion_value->label() . '</div>';
    }

    $build .= '</div>';
  } elseif ($formatter == 'soloxhoy') {
    // Solo x Hoy
    if (!empty($benefit['image'])) {
      $build = '<div class="field-image"><a href="' . url('benefit/benefit/' . $entity->id) . '#section-benefit"><img src="' . image_style_url('soloxhoy_big', $benefit['image']['und'][0]['uri']) . '"></a></div>';
    }
  } elseif ($formatter == 'full') {

    $mayor_porcentaje = $entity->field_benefit_porcent['und'][0]['value'];

    if ($mayor_porcentaje != 0) {
      $porTop = "<span class='menos'>-</span>" . $mayor_porcentaje;
    }

    if (!empty($benefit['logo'])) {
      $logo = '<img src="' . image_style_url('soloxhoy_small', $benefit['logo']['und'][0]['uri']) . '">';
    } else {
      $logo = $shop['title'];
    }
    if (!empty($entity->field_benefit_grl_procent['und'][0])) {
      $porGrl = "<span class='menos'>-</span>" . $wrapper->field_benefit_grl_procent->value();
    }

    $coupon_promo = Devolver_CouponPromo_Asociado_Benefit($entity->id);
    if ($coupon_promo !== FALSE) {
      foreach ($coupon_promo as $key => $items) {
        $coupon_promo_id        = $items->id;
        $coupon_promo_descuento = "<span class='menos'>-</span>" . $items->field_coupon_promo_discount['und'][0]['value'];
      }
    } else {
      $coupon_promo_descuento = FALSE;
    }

    $cft=$entity->field_benefit_cft['und'][0]['value'];
    $cftExt;
    preg_match_all('/(cftna:|CFTNA:|costo financiero total:|costo financiero total efectivo anual:|COSTO FINANCIERO TOTAL:|COSTO FINANCIERO TOTAL EFECTIVO ANUAL:)\s*(\d{1,2}[\,\.]{1}\d{1,2})(%)/', $cft, $cftExt);
    $cftExt2= $cftExt[2];
    $cftExt3= $cftExt2[0];

    $vars = array(
      'formatter' => $formatter,
      'title' => $shop['title'],
      'imageface' => image_style_url('benefit_full', $benefit['image']['und'][0]['uri']),
      'logo' => $logo,
      'image' => '<img src="' . image_style_url('benefit_full', $benefit['image']['und'][0]['uri']) . '">',
      'id' => $entity->id,
      'description' => $entity->field_benefit_description_portal['und'][0]['value'],
      'descriptionSmall' => $entity->field_benefit_message_small_p['und'][0]['value'],
      'cft' => $entity->field_benefit_cft['und'][0]['value'],
      'validity' => t('Promoción Valida desde ' . date('d/m/Y', $wrapper->field_benefit_date_start->value()) . ' hasta ' . date('d/m/Y', $wrapper->field_benefit_date_end->value())),
      'btn_card' => l(t('Pedi tu Tarjeta'), 'Registrate'),
      'btn_suscribite' => l(t('Registrate a FrancesGo'), 'user/login'),
      'btn_coupon' => l(t('Quiero mi Cupon'), 'cupon/' . $entity->id),
      'legal' => array(
        'title' => t('Términos y Condiciones'),
        'text' => $entity->field_benefit_legal['und'][0]['value']
      ),
      'promotions' => array(
        'quote' => $wrapper->field_benefit_promotion_value->label(),
        'promotion_top' => $porTop,
        'promotion_grl' => $porGrl,
        'coupon_promo_discount' => $coupon_promo_descuento,
        'voucher_available' => _vouchers_con_stock($coupon_promo_id),
        'promotion_top_text' => 'Tarjeta<br>BBVA Frances<br>y FrancesGo',
        'promotion_middle' => 30,
        'promotion_middle_text' => 'Con Tarjeta<br>BBVA Frances',
        'promotion_bottom' => 10,
        'promotion_bottom_text' => 'Sin Tarjeta<br>BBVA Frances<br>y FrancesGo'
      ),
      'like' => l('like', 'node/' . $entity->id . 'flag/' . $entity->id),
      "is_shopping" => $is_shopping,
      "cftExt"=>$cftExt3
    );
    return theme('benefit', array(
      'vars' => $vars
    ));
  }

  return $build;
}

function _bbva_ds_field_cupon($field) {
  $rubros            = _get_category();
  $entity            = $field['entity'];
  $formatter         = $field['formatter'];
  $wrapper           = entity_metadata_wrapper('voucher', $entity->id);
  $wrapperCuponPromo = $wrapper->field_voucher_coupon_promo;
  $wrapperShop       = $wrapperCuponPromo->field_coupon_promo_shop;
  $wrapperBenefit    = $wrapperCuponPromo->field_coupon_promo_benefit;
  $cuponImagenes     = array();

  $cuponImagenBenefit     = $wrapperBenefit->field_benfit_image->value();
  $cuponImagenBenefitLogo = $wrapperBenefit->field_benefit_logo_img->value();

  $cuponImagenShop     = $wrapperShop->field_shop_imagen->value();
  $cuponImagenShopLogo = $wrapperShop->field_shiop_mark_tip->value();

  $categorycup = $wrapperShop->field_shop_category->value();

  $categorys = count($categorycup);
  $term        = taxonomy_term_load($categorycup[0]->tid);

  $shop['rubroImageLogo'] = $term->field_category_logo_web;
  $shop['rubroImage']     = $term->field_category_image;

  $shop_parent = $term->field_category_parent;

  if ((empty($shop['rubroImageLogo']) || $categorys > 1) && !empty($shop_parent)) {
    $shop['rubroImageLogo'] = $rubros[$shop_parent['und'][0]['value']]->field_category_logo_web;
  }
  if ((empty($shop['rubroImage']) || $categorys > 1) && !empty($shop_parent)) {
    $shop['rubroImage'] = $rubros[$shop_parent['und'][0]['value']]->field_category_image;
  }

  if ($cuponImagenBenefit) {
    $cuponImagenes['imagen'] = $cuponImagenBenefit['uri'];
  } elseif ($cuponImagenShop) {
    $cuponImagenes['imagen'] = $cuponImagenShop['uri'];
  } else {
    $cuponImagenes['imagen'] = $shop['rubroImage']['und'][0]['uri'];
  }

  if ($cuponImagenBenefitLogo) {
    $cuponImagenes['logo'] = $cuponImagenBenefit['uri'];
  }

  if ($cuponImagenShopLogo) {
    $cuponImagenes['logo'] = $cuponImagenShopLogo['uri'];
  }

  $cuponImagenes['title'] = $wrapperCuponPromo->field_coupon_promo_shop->label();

  if (!empty($cuponImagenes['logo'])) {
    $cuponImagenes['logo'] = '<img src="' . image_style_url('soloxhoy_small2', $cuponImagenes['logo']) . '">';
  } else {
    $cuponImagenes['logo'] = $cuponImagenes['title'];
  }

  if (!empty($cuponImagenes['imagen'])) {
    $cuponImagenes['imagen'] = image_style_url('benefit_full', $cuponImagenes['imagen']);
  }
  $top = $wrapperCuponPromo->field_coupon_promo_top->value();
  if ($top == 0) {
    $top    = '';
    $msgTop = 'Sin tope de descuento';
  } else {
    # code...
    $msgTop = 'Sobre la totalidad de la compra. Tope $';
  }

  $vars['descuento']     = $wrapperCuponPromo->field_coupon_promo_discount->value();
  $vars['tope']          = $top;
  $vars['fecha']         = date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_end->value());
  $vars['codigo']        = $wrapper->label();
  $vars['legal']         = $wrapperCuponPromo->field_coupon_promo_legal->value();
  $vars['cuponImagenes'] = $cuponImagenes;
  $vars['msjTope']       = $msgTop;
  $vars['codigo'] = $wrapper->label();
  return theme('cupon', array(
    'vars' => $vars
  ));
}
