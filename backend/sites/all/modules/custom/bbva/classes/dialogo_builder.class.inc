<?php

namespace BBVA;

class DialogoBuilder {

  public $titulo;
  public $texto1;
  public $texto2;
  public $texto3;
  public $texto_boton;

  public function __construct() {
    $this->texto_boton = "Volver";
  }

  function get_dialogo() {
    $data_return = array(
        'titulo' => $this->titulo,
        'texto1' => $this->texto1,
        'texto2' => $this->texto2,
        'texto3' => $this->texto3,
        'texto_boton' => $this->texto_boton,
    );
    return $data_return;
  }
}

