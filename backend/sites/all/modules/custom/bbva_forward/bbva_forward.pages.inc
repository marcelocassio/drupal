<?php

function bbva_forward_seguros($aid) {
  drupal_page_is_cacheable(FALSE);

  if($aid != "null") {
    $aid = sprintf("%'.08d", $aid);
  }
  
  $url = variable_get('bbva_forward_seguros_url');
  $params = array(
    'nroCliente' => $aid
  );
  $url .= "?" . http_build_query($params);

  header("Location: " . $url);
  exit();
}

function bbva_forward_seguros_data($hash) {
  $data = array();
  try {
    $forward = \BBVA\Forward::getInstance();
    $hash_data = $forward->cargar($hash);
    if($hash_data->tipo == "seguros") {
      $data['aid'] = $hash_data->extra_data['aid'];
    }
  } catch (Exception $e) {}

  return $data;
}