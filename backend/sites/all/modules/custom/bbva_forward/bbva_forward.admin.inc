<?php

/**
 * Admin settings page.
 */
function bbva_forward_admin($form, $form_state) {

  $form['bbva_forward_seguros_url'] = array(
    '#title' => t('URL Oferta Preaprobada'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_forward_seguros_url'),
  );

  return system_settings_form($form);
}
