<?php

namespace BBVA;

class Forward {

  private static $instancia;

  const TIEMPO_VIGENCIA_HASH = 259200;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  /*private function __construct() {
    // El constructor debe ser privado (singleton)
  }*/

  private function generarHash() {
    $hash = md5(time() . rand(0, 9999999));
    return $hash;
  }

  public function cargar($hash) {
    $result = db_select('bbva_forward', 'bf')
      ->condition('bf.hash', $hash)
      ->fields('bf')
      ->execute()
      ->fetchObject();

    if(isset($result->extra_data)) {
      $result->extra_data = unserialize($result->extra_data);
    }
    return $result;
  }

  public function crear($uid = 0, $tipo, $extra_data = NULL) {
    if ($extra_data) {
      $extra_data = serialize($extra_data);
    }
    // Si falla el insert vuelvo a intentar 3 veces
    $i = 1;
    while(!$rid && $i <= 3) {
      try {
        $hash = $this->generarHash();
        $rid = db_insert('bbva_forward')
          ->fields(array(
            'uid' => $uid,
            'tipo' => $tipo,
            'hash' => $hash,
            'extra_data' => $extra_data,
            'fecha_creacion' => REQUEST_TIME,
          ))
          ->execute();
      } catch (\Exception $e) {}
      ++$i;
    }

    return $hash;
  }

  public function limpiar() {
    $fecha = (int) (REQUEST_TIME - self::TIEMPO_VIGENCIA_HASH);
    $q = db_delete('bbva_forward')
      ->condition('fecha_creacion', $fecha, '<')
      ->execute();

    return $q;
  }

  public function borrar($hash) {
    $q = db_delete('bbva_forward')
      ->condition('hash', $hash)
      ->execute();

    return $q;
  }
}