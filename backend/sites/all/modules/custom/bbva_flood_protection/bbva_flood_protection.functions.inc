<?php

function bbva_flood_protection_is_blocked($username) {
  $host = _bbva_flood_protection_get_host();
  $result = bbva_flood_protection_db_find_blocked($username, $host);

  if(count($result) > 0) {
    return TRUE;
  }

  return FALSE;
}

function bbva_flood_protection_clean() {
  $expire = "PT" . BBVA_FLOOD_PROTECTION_MINUTES_EXPIRATION . "M";
  $date = new DateTime();
  $date->sub(new DateInterval($expire));
  $timestamp = $date->getTimestamp();
  bbva_flood_protection_db_delete_blockeds($timestamp);
  bbva_flood_protection_db_delete_attempts($timestamp);
}

function bbva_flood_protection_insert_attempt($username) {
  $host = _bbva_flood_protection_get_host();
  if(!$host) {
    $host = NULL;
  }
  bbva_flood_protection_db_insert_attempt($username, $host);

  $host_block = NULL;
  $user_block = NULL;

  if($host) {
    $host_attempts = bbva_flood_protection_db_count_host_attempts($host);
    if(count($host_attempts) >= BBVA_FLOOD_PROTECTION_HOST_ATTEMPS) {
      $host_block = $host;
    }
  }

  $user_attempts = bbva_flood_protection_db_count_username_attempts($username);
  if(count($user_attempts) >= BBVA_FLOOD_PROTECTION_USER_ATTEMPS) {
    $user_block = $username;
  }

  if($host_block !== NULL || $user_block !== NULL) {
    bbva_flood_protection_db_insert_blocked($user_block, $host_block);
  }
}

function _bbva_flood_protection_get_host() {
  $host = ip_address();
  if (filter_var($host, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE) === FALSE) {
    return FALSE;
  }
  return $host;
}
