<?php

function bbva_flood_protection_db_find_blocked($username, $host = FALSE) {

  $query = db_select('flood_protection_users_blocked', 'ub');

  if($host) {
    $query = $query->condition(
      db_or()
        ->condition('ub.host', $host)
        ->condition('ub.name', $username)
    );
  } else {
    $query = $query->condition('ub.name', $username);
  }

  $query = $query->fields('ub', array('id'));

  $result = $query->execute()->fetchAll();

  return $result;
}

function bbva_flood_protection_db_insert_blocked($username = NULL, $host = NULL) {
  $id = db_insert('flood_protection_users_blocked')
    ->fields(array(
      'name' => $username,
      'host' => $host,
      'timestamp' => time()
    ))
    ->execute();

  return $id;
}

function bbva_flood_protection_db_delete_blockeds($timestamp) {
  $deleted = db_delete('flood_protection_users_blocked')
    ->condition('timestamp', $timestamp, '<=')
    ->execute();

  return $deleted;
}

function bbva_flood_protection_db_insert_attempt($username, $host) {
  $id = db_insert('flood_protection_track')
    ->fields(array(
      'name' => $username,
      'host' => $host,
      'timestamp' => time()
    ))
    ->execute();

  return $id;
}

function bbva_flood_protection_db_count_host_attempts($host) {
  $query = db_select('flood_protection_track', 't');

  $query = $query->condition('t.host', $host);

  $query = $query->fields('t', array('id'));

  $result = $query->execute()->fetchAll();

  return $result;
}

function bbva_flood_protection_db_count_username_attempts($username) {
  $query = db_select('flood_protection_track', 't');

  $query = $query->condition('t.name', $username);

  $query = $query->fields('t', array('id'));

  $result = $query->execute()->fetchAll();

  return $result;
}

function bbva_flood_protection_db_delete_attempts($timestamp) {
  $deleted = db_delete('flood_protection_track')
    ->condition('timestamp', $timestamp, '<=')
    ->execute();

  return $deleted;
}
