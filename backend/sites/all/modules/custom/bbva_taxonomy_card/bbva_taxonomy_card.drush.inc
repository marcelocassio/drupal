<?php

/*
 * Implements hook_drush_command().
 */
function bbva_taxonomy_card_drush_command() {

  $items['vuelco-card'] = array(
    'description' => 'Realiza el vuelco de las tarjetas',
    'aliases' => array('cardlImport'),
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_taxonomy_card_vuelco_card() {
  import_card();
    drupal_set_message("Card vuelco echo", 'status', FALSE);

}
