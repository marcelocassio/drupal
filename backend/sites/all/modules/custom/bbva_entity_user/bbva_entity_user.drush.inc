<?php

/*
 * Implements hook_drush_command().
 */
function bbva_entity_user_drush_command() {

  $items['vuelco-user'] = array(
    'description' => 'Realiza el vuelco de estados de suscripción en suip a drupal',
    'aliases' => array('usersImport'),
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_entity_user_vuelco_user() {
  import_user_suip();
  drupal_set_message("Update User hecho", 'status', FALSE);
}
