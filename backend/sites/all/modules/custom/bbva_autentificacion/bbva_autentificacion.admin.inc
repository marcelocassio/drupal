<?php

/**
 * Admin settings page.
 */

  /** Esto le da al admin permiso para acceder al modulo
   * Implements hook_permission().
  */
function bbva_autentificacion_permission() {
  return array(
    'bbva_autentificacion_perm_rrss' => array(
    'title' => t("Autentificacion RRSS"),
    'description' => t("Modificar API's & Secret keys"),
    ),
  );
}


/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 *
 * Types:  // Para una lista completa : https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7.x
 * fieldset , textfield , password , textarea , checkbox , radio
 *
 */
function bbva_autentificacion_admin_settings() {
  $form['bbva_autentificacion_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Autentificación Redes Sociales'),
  );

  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_desa'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Autentificación Facebook Desa'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_desa']['bbva_autentificacion_fb_api_activar_desa'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Facebook API Activar Desa'),
    '#default_value' => variable_get('bbva_autentificacion_fb_api_activar_desa'),
    '#description'   => t('Activar credenciales de Desa'),
    '#required' => FALSE, // Puede ser TRUE
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_desa']['bbva_autentificacion_fb_api_key_desa'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook API Key Desa'),
    '#default_value' => variable_get('bbva_autentificacion_fb_api_key_desa', '205411949872896'),
    '#description'   => t('API Key para Desa'),
    '#required' => TRUE, // Puede ser TRUE
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_desa']['bbva_autentificacion_fb_api_secret_desa'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook API Secret Desa'),
    '#default_value' => variable_get('bbva_autentificacion_fb_api_secret_desa', 'e51716e312a5297904d77f88afb73850'),
    '#description'   => t('API Secret para Desa'),
    '#required' => TRUE, // Puede ser TRUE
  );

  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_desa']['bbva_autentificacion_fb_api_version_desa'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook API Version Desa'),
    '#default_value' => variable_get('bbva_autentificacion_fb_api_version_desa', 'v3.3'),
    '#description'   => t('API Version para Desa'),
    '#required' => TRUE, // Puede ser TRUE
  );
  /*-----------------------------------*/
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_prod'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Autentificación Facebook PROD'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_prod']['bbva_autentificacion_fb_api_key_prod'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook API Key PROD'),
    '#default_value' => variable_get('bbva_autentificacion_fb_api_key_prod', '118586505158934'),
    '#description'   => t('API Key para PROD'),
    '#required' => FALSE, // Puede ser TRUE
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_prod']['bbva_autentificacion_fb_api_secret_prod'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook API Secret PROD'),
    '#default_value' => variable_get('bbva_autentificacion_fb_api_secret_prod', '5b3303a47fd9d2bc28053d931435b309'),
    '#description'   => t('API Secret para PROD'),
    '#required' => FALSE, // Puede ser TRUE
  );

  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_fb_prod']['bbva_autentificacion_fb_api_version_prod'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook API Version PROD'),
    '#default_value' => variable_get('bbva_autentificacion_fb_api_version_prod', 'v3.3'),
    '#description'   => t('API Version para PROD'),
    '#required' => FALSE, // Puede ser TRUE
  );

  /* ------------------------------------------------------------------------------------------------------------- */

  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_google_desa'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Autentificación Google Desa'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_google_desa']['bbva_autentificacion_google_api_activar_desa'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Google API Activar Desa'),
    '#default_value' => variable_get('bbva_autentificacion_google_api_activar_desa'),
    '#description'   => t('Activar credenciales de Desa'),
    '#required' => FALSE, // Puede ser TRUE
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_google_desa']['bbva_autentificacion_google_api_android_desa'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Google API Android Desa'),
    '#default_value' => variable_get('bbva_autentificacion_google_api_android_desa', '837119537199-mqac172ss1bh1nibq1bnd2mchqh63so9.apps.googleusercontent.com'),
    '#description'   => t('API Android para Desa'),
    '#required' => TRUE, // Puede ser TRUE
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_google_desa']['bbva_autentificacion_google_api_ios_desa'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Google API iOS Desa'),
    '#default_value' => variable_get('bbva_autentificacion_google_api_ios_desa', '837119537199-kevtg1amn6leek4lk1ufrjl450o8185q.apps.googleusercontent.com'),
    '#description'   => t('API iOS para Desa'),
    '#required' => TRUE, // Puede ser TRUE
  );

   $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_google_prod'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Autentificación Google PROD'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_google_prod']['bbva_autentificacion_google_api_android_prod'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Google API Android PROD'),
    '#default_value' => variable_get('bbva_autentificacion_google_api_android_prod', '221666439054-pbv4up2469r67vtd6ensuidrim1up95b.apps.googleusercontent.com'),
    '#description'   => t('API Android para PROD'),
    '#required' => FALSE,
  );
  $form['bbva_autentificacion_settings']['bbva_autentificacion_settings_google_prod']['bbva_autentificacion_google_api_ios_prod'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Google API iOS PROD'),
    '#default_value' => variable_get('bbva_autentificacion_google_api_ios_prod', '221666439054-vebr8mfode5midgo1v3o22k7kuahcbtq.apps.googleusercontent.com'),
    '#description'   => t('API iOS para PROD'),
    '#required' => FALSE,
  );
  return system_settings_form($form);
}
