<?php

function bbva_autentificacion_buscar_usuario_id_facebook($ufacebook){

  $query = db_select('bbva_user_rrss', 'ur')
    ->fields('ur', array('uid'));

  $query->condition('ur.ufacebook', $ufacebook, '=');
  $result = $query->execute()->fetchObject();

  if (empty($result)) {
    return null;
  }
  return $result->uid;
}

function bbva_autentificacion_buscar_usuario_id_google($ugoogle){

  $query = db_select('bbva_user_rrss', 'ur')
    ->fields('ur', array('uid'));

  $query->condition('ur.ugoogle', $ugoogle, '=');
  $result = $query->execute()->fetchObject();

  if (empty($result)) {
    return null;
  }
  return $result->uid;
}

function bbva_autentificacion_buscar_usuario_id_fnet($ufnet){

  $query = db_select('bbva_user_rrss', 'ur')
    ->fields('ur', array('uid'));

  $query->condition('ur.ufnet',$ufnet, '=');
  $result = $query->execute()->fetchObject();

  if (empty($result)) {
    return null;
  }
  return $result->uid;
}

function bbva_autentificacion_buscar_relacion_id_fnet($ufnet){

  $query = db_select('bbva_user_rrss', 'ur')
    ->fields('ur', array('uid', 'ufacebook', 'ugoogle', 'ufnet'));

  $query->condition('ur.ufnet',$ufnet, '=');
  $result = $query->execute()->fetchObject();

  if (empty($result)) {
    return null;
  }
  return $result;
}

function bbva_autentificacion_borrar_vinculaciones($uid) {
  $deleted = db_delete('bbva_user_rrss')
    ->condition('uid', $uid)
    ->execute();
}

function bbva_autentificacion_guardar_usuario_id_fb($uid, $user_identificador){

  $query = db_merge('bbva_user_rrss')
    ->key(array(
      'uid' => $uid,
    ))
    ->fields(
      array(
        'ufacebook' => $user_identificador,
      ))
    ->execute();

  return $query;
}

function bbva_autentificacion_guardar_usuario_id_google($uid, $user_identificador){

  $query = db_merge('bbva_user_rrss')
    ->key(array(
      'uid' => $uid,
    ))
    ->fields(
      array(
        'ugoogle' => $user_identificador,
      ))
    ->execute();

  return $query;
}

function bbva_autentificacion_guardar_usuario_id_apple($uid, $user_identificador){

  $query = db_merge('bbva_user_rrss')
    ->key(array(
      'uid' => $uid,
    ))
    ->fields(
      array(
        'uapple' => $user_identificador,
      ))
    ->execute();

  return $query;
}


function bbva_autentificacion_guardar_usuario_id_fnet($uid, $user_identificador, $desvincular_otros = false){

  $query = db_merge('bbva_user_rrss');

  $query->key(array(
    'uid' => $uid,
  ));

  if ($desvincular_otros) {
    $query->fields(array(
      'ufnet' => $user_identificador,
      'ufacebook' => 0,
      'ugoogle' => 0,
    ));
  } else {
    $query->fields(array(
      'ufnet' => $user_identificador,
    ));
  }

  $query->execute();

  return $query;
}

function _crear_usuario($nombre_usuario, $rol) {
  $password = user_password(8);

  $user_data = array();
  $user_data['init'] = $nombre_usuario;
  $user_data['name'] = $nombre_usuario;
  $user_data['pass'] = $password;
  $user_data['status'] = 1;
  $user_data['roles'] = array($rol);

  user_save('', $user_data);
}

function _existe_usuario($nombre_usuario) {
  $user_found = false;
  $uid = db_select('users','u')
    ->fields('u', array('uid'))
    ->condition('name', $nombre_usuario, '=')
    ->execute()
    ->fetchField();
  if (!empty($uid)) {
    $user_found = true;
  }
  return $user_found;
}

function _buscar_uid($nombre_usuario) {
  $uid = db_select('users','u')
    ->fields('u', array('uid'))
    ->condition('name', $nombre_usuario, '=')
    ->execute()
    ->fetchField();
  return $uid;
}

function bbva_autentificacion_eliminar_sesiones($uid) {

  $sesiones = bbva_autentificacion_consultar_sesiones($uid);
  foreach ($sesiones as $value) {
    _drupal_session_destroy($value->sid);
    watchdog('bbva_autentificacion', 'Destruimos sesion de uid: %uid y hostname: %hostname',
        array('%uid' => $value->uid, '%hostname' => $value->hostname), WATCHDOG_DEBUG);
  }
}

function bbva_autentificacion_consultar_sesiones($uid) {
  $query =  db_select('sessions', 's')
    ->fields('s', array('uid','sid', 'hostname'))->condition('s.uid', $uid, '=');

  $res = $query->execute()->fetchAll();

  return $res;
}

function _bbva_autentificacion_registrar_sesion($uid, $origen) {
  $nid = db_insert('bbva_sessions')
    ->fields(array(
      'uid' => $uid,
      'sid' => session_id(),
      'canal' => $origen,
      'timestamp' => REQUEST_TIME,
    ))
    ->execute();

}

function _bbva_autentificacion_check_vinculacion_realizada($uid){
  $user_found = false;
  $query = db_select('bbva_vinculaciones','bv')
    ->fields('bv', array('uid_destino'))
    ->condition('uid_origen', $uid, '=')
    ->condition('estado', BBVA_USUARIO_VINCULACION_REALIZADA, '=');

  $result = $query->execute()->fetchField();

  if (!empty($result)) {
    return $result;
  }else{
    return null;
  }
}

function bbva_autentificacion_limpiar_sesiones_antiguas() {
  $expire = "P" . BBVA_AUTENTIFICACION_DURACION_SESION . "D";
  $date = new DateTime();
  $date->sub(new DateInterval($expire));
  $timestamp = $date->getTimestamp();

  $query = db_delete('bbva_sessions')
    ->condition('timestamp', $timestamp, '<')
    ->execute();

  return $query;
}