<?php
function bbva_autentificacion_generar_keys($tipo){

  switch ($tipo) {
    case BBVA_AUTH_TIPO_FB:
      if (variable_get('bbva_autentificacion_fb_api_activar_desa') == 1) {
        $rrss_keys['apiId'] = variable_get('bbva_autentificacion_fb_api_key_desa');
        $rrss_keys['client_secret'] = variable_get('bbva_autentificacion_fb_api_secret_desa');
        $rrss_keys['version'] = variable_get('bbva_autentificacion_fb_api_version_desa');
      }else{// Si están activadas las Keys APIS de PROD
        $rrss_keys['apiId'] = variable_get('bbva_autentificacion_fb_api_key_prod');
        $rrss_keys['client_secret'] = variable_get('bbva_autentificacion_fb_api_secret_prod');
        $rrss_keys['version'] = variable_get('bbva_autentificacion_fb_api_version_prod');
      }
      break;
    case BBVA_AUTH_TIPO_GOOGLE:
      if (variable_get('bbva_autentificacion_google_api_activar_desa') == 1) {
        $rrss_keys['appIdAndroid'] = variable_get('bbva_autentificacion_google_api_android_desa');
        $rrss_keys['appIdiOS'] = variable_get('bbva_autentificacion_google_api_ios_desa');
      }else{// Si están activadas las Keys APIS de PROD
        $rrss_keys['appIdAndroid'] = variable_get('bbva_autentificacion_google_api_android_prod');
        $rrss_keys['appIdiOS'] = variable_get('bbva_autentificacion_google_api_ios_prod');
      }
      break;
    default:
      $rrss_keys = '';
      break;
  }

  return $rrss_keys;
}
function bbva_autentificacion_updates(){
      if (!db_field_exists('bbva_user_rrss', 'uapple')) {
    $col = array(
      'type' => 'varchar',
      'description' => "Id Apple",
      'length' => 150,
      'not null' => TRUE,
      'default' => 0,  
    );
    db_add_field( 'bbva_user_rrss', 'uapple', $col);
  }
}

/**
 * The batch finish handler.
 */
function bbva_autentificacion_updates_finished($success, $results, $operations)
{
  if ($success) {
    drupal_set_message(t('Alter column Process Complete!'));
  } else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], true),
    ));
    drupal_set_message($message, 'error');
  }
}