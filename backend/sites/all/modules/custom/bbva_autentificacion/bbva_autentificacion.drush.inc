<?php 
/**
 * Implements COMMANDFILE_drush_command().
 */
function bbva_autentificacion_drush_command() {
  $items = array();

  // bcpnew
  $items['bbva-autentificacion-updates'] = array(
    'description' => 'Drush process that generates new column in table.',
    'aliases' => array('uapple'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}


/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_bbva_autentificacion_updates() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Autentificacion table update'),
    'init_message' => t('Alter table is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_autentificacion_updates_finished',
    'file' => drupal_get_path('module', 'bbva_autentificacion') . '/batch/bbva_autentificacion.functions.inc',
  );

  $batch['operations'][] = array('bbva_autentificacion_updates', array(''));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}
