<?php

define('BBVA_AUTH_KEY_ANDROID_PR', '837119537199-mqac172ss1bh1nibq1bnd2mchqh63so9.apps.googleusercontent.com');
define('BBVA_AUTH_KEY_IOS_PR', '837119537199-o3nmk2608me6e2ke0bbitpbq8pcnvekc.apps.googleusercontent.com');

class AutentificacionBbvaGoogle extends AutentificacionBbva{
  private $verificar_data;
  private $appId;

  public function __construct($credenciales, $rrss_keys) {

    $this->appId = array($rrss_keys['appIdAndroid'], $rrss_keys['appIdiOS'], BBVA_AUTH_KEY_ANDROID_PR, BBVA_AUTH_KEY_IOS_PR);
    $this->credenciales = $credenciales;
    $this->medio = BBVA_AUTH_TIPO_GOOGLE;
    
    $credenciales_token = isset($this->credenciales['token']) ? $this->credenciales['token'] : null;

    $params = array(
      "id_token" => $credenciales_token
    );
    $data_temp = bbva_proxy('https://www.googleapis.com/oauth2/v3/tokeninfo',$params);

    $this->verificar_data = json_decode($data_temp, true);
  }

  public function validarCredenciales() {
    if ($this->verificar_data) {
      $datos = $this->verificar_data;
     
      if (isset($datos['aud']) && !in_array($datos['aud'], $this->appId)) {
        throw new LoginException("Intento no válido en Inicio de Sesión", 1);
      }

      $email_validado = false;

      if (isset($datos['email_verified'])) {
        $email_validado = $datos['email_verified'];
      }

      if ($email_validado) {
        return $datos['sub'];
      }
    }
    throw new LoginException("Intento no válido en Inicio de Sesión", 1);
  }

  public function getUserIdRrss() {
    $this->uid = bbva_autentificacion_buscar_usuario_id_google($this->name_user);
    if ($this->uid) {
      $this->actualizarDatosPersonales();
    } else {
      throw new LoginUsuarioNoencontradoException($this->rrss_user_id);
    }
  }

  /*OVERRRIDE*/
  public function crearUsuarioDrupal() {
    if (!$this->existe_usuario($this->name_user)) {
      parent::crearUsuarioDrupal();
    } else {
      $this->uid = $this->buscar_uid($this->name_user);
    }
    watchdog('bbva_debug', 'crearUsuarioDrupal uid: %value', array('%value' => $this->uid) );
    $this->actualizarDatosPersonales();
  }

  private function existe_usuario($nombre_usuario) {
    $user_found = false;
    $uid = db_select('users','u')
    ->fields('u', array('uid'))
    ->condition('name', $nombre_usuario, '=')
    ->execute()
    ->fetchField();
    if (!empty($uid)) {
      $user_found = true;
    }
    return $user_found;
  }

  public function registerSession() {
    _bbva_autentificacion_registrar_sesion($this->uid, BBVA_AUTH_TIPO_GOOGLE);
  }

  public function relacionarUserIdRrss() {
    bbva_autentificacion_guardar_usuario_id_google($this->uid, $this->name_user);
  }

}
