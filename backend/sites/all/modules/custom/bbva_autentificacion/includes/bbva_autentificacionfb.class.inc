<?php

class AutentificacionBbvaFB extends AutentificacionBbva{
  public $fb;
  public $fb_access_token;

  public function __construct($credenciales, $rrss_keys) {
    $this->credenciales = $credenciales;
    $this->medio = BBVA_AUTH_TIPO_FB;


    if ((!variable_get('access_token_fb')) || (variable_get('access_token_fb_date') != date("d-m-Y")) ) {

      $app_access_token_temp = bbva_proxy('https://graph.facebook.com/'.$rrss_keys['version'].'/oauth/access_token?client_id='.$rrss_keys['apiId'].'&client_secret='.$rrss_keys['client_secret'].'&grant_type=client_credentials');

      $app_access_token = json_decode($app_access_token_temp);

      if (isset($app_access_token->access_token)) {
        variable_set('access_token_fb', $app_access_token->access_token);
        $this->fb_access_token = variable_get('access_token_fb');
        variable_set('access_token_fb_date', date("d-m-Y"));
      }else{
        $this->fb_access_token = null;
      }
    }else{ // Si existe la variable previamente generada
      $this->fb_access_token = variable_get('access_token_fb');
    }

  }

  public function validarCredenciales() {
    if ($this->fb_access_token){
      
      $credenciales_token = isset($this->credenciales['token']) ? $this->credenciales['token'] : null;

      $verificar_token_temp = bbva_proxy('https://graph.facebook.com/debug_token?input_token='.$credenciales_token.'&access_token='.$this->fb_access_token);

      $verificar_token = json_decode($verificar_token_temp);

      $token_validado = FALSE;
      if (isset($verificar_token->data->is_valid)) {
        $token_validado = $verificar_token->data->is_valid;
      }

      if ($token_validado) {
        return $verificar_token->data->user_id;
      }
    }

    throw new LoginException("Error en el sistema, intente otra vez", 1);
  }

  public function getUserIdRrss() {
    $this->uid = bbva_autentificacion_buscar_usuario_id_facebook($this->name_user);
    if ($this->uid) {
      $this->actualizarDatosPersonales();
    } else {
      throw new LoginUsuarioNoencontradoException($this->rrss_user_id);
    }
  }

  /*OVERRRIDE*/
  public function crearUsuarioDrupal() {
    if (!$this->existe_usuario($this->name_user)) {
      parent::crearUsuarioDrupal();
    } else {
      $this->uid = $this->buscar_uid($this->name_user);
    }
    watchdog('bbva_debug', 'crearUsuarioDrupal uid: %value', array('%value' => $this->uid) );
    $this->actualizarDatosPersonales();
  }

  private function existe_usuario($nombre_usuario) {
    $user_found = false;
    $uid = db_select('users','u')
      ->fields('u', array('uid'))
      ->condition('name', $nombre_usuario, '=')
      ->execute()
      ->fetchField();
    if (!empty($uid)) {
      $user_found = true;
    }
    return $user_found;
  }

  public function relacionarUserIdRrss() {
    if (($this->uid) && ($this->name_user)) {
      bbva_autentificacion_guardar_usuario_id_fb($this->uid, $this->name_user);
    }else{
      watchdog('bbva_autentificacion', 'No se pudo relacionar UserID FB: %value', array('%value' => "UID: ". $this->uid." | Name user: ".$this->name_user));
      throw new LoginException("Error en el sistema, intente otra vez", 1);
    }
  }

}
