<?php
abstract class AutentificacionBbva{
  private $datos_suip;
  protected $medio;
  public $credenciales;
  public $usuarioDrupal;
  public $auth_id;

  /*
  * @return $auth_id Retorno el identificador del usuario segun
  * el medio con el valido credenciales correctamente.
  */
  abstract public function validarCredenciales();

  protected function sincronizarVinculaciones() {}
  protected function actualizarDatosPersonales() {
    return array();
  }
  public function getMediosSugeridos() {
    return NULL;
  }
  public function getDatosCliente() {
    return NULL;
  }

  abstract public function relacionarUserIdRrss();
  abstract public function getUserIdRrss();

  public function autenticar() {
    $this->auth_id = $this->validarCredenciales();
  }

  public function buscarSuscripcion() {
    $this->usuarioDrupal = new \BBVA\Usuario();



    // Obtengo uid y numero de cliente
    $uid = NULL;
    if($this->medio != BBVA_AUTH_TIPO_MANUAL) {
      $uid = $this->usuarioDrupal->buscarUsuario($this->medio, $this->auth_id);
    } else {
      $uid = $this->auth_id;
    }
    if ($uid) {
      $this->usuarioDrupal->obtenerDatos($uid);
    }

    $nro_cliente = NULL;
    if($this->medio == BBVA_AUTH_TIPO_FNET) {
      $nro_cliente = $this->auth_id;
    }
    if(!$nro_cliente && isset($this->usuarioDrupal->data->name)) {
      $nro_cliente = $this->usuarioDrupal->data->name;
    }

    // Veo si esta suscripto en SUIP
    $esta_suscripto = FALSE;
    
    if($nro_cliente) {
      $this->datos_suip = $this->usuarioDrupal->cargarDatosSuip($nro_cliente);
      $esta_suscripto = $this->datos_suip->esta_suscripto();
    }

    if(!$esta_suscripto) {
      if($this->medio != BBVA_AUTH_TIPO_MANUAL) {
        throw new LoginUsuarioNoencontradoException($this->auth_id);
      }

      throw new LoginException();
    }
    
    return $esta_suscripto;
  }

  public function actualizarUsuario() {
    if(!isset($this->usuarioDrupal->data->uid)) {
      $this->usuarioDrupal->cargarDatosSuscripcion($this->datos_suip);
      if($this->medio == BBVA_AUTH_TIPO_FNET) {
        $this->usuarioDrupal->modificar(array("name" => $this->auth_id));
      }
    }
    $this->usuarioDrupal->actualizarEstadoSuscripcion($this->datos_suip);

    // En caso de FNET editamos el usuario de drupal
    $edit = $this->actualizarDatosPersonales();
    if(!empty($edit)) {
      $this->usuarioDrupal->modificar($edit);
      $this->usuarioDrupal->cargarCelularSuscripto($this->datos_suip);
      $this->usuarioDrupal->modificarSuscripcion();
    }

    // Creo/modifico el usuario
    $this->usuarioDrupal->guardar();

    // Sincronizo las vinculaciones
    $this->sincronizarVinculaciones();
  }

  public function login() {
    $this->usuarioDrupal->login();
    $usuario = $this->usuarioDrupal->getDatosBasicos();
    $usuario->medios_sugeridos = $this->getMediosSugeridos();

    return $usuario;
  }

  public function getRespuesta() {
    return $this->userRes;
  }

  public function crearUsuarioDrupal() {
    $password = user_password(8);

    // validar user_id
    $user_data = array();
    $user_data['init'] = $this->name_user;
    $user_data['name'] = $this->name_user;
    $user_data['pass'] = $password;
    if ($this->mail_user) {
      $user_data['mail'] = $this->mail_user;
    }
    $user_data['status'] = 1;
    $user_data['roles'] = $this->roles;

    $account = user_save('', $user_data);
    $this->uid = $account->uid;
  }

  public function sincronizarEstadoSuscricion() {
    $this->usuarioDrupal = new \BBVA\Usuario($this->uid, true);
    $this->usuarioDrupal->actualizarEstadoSuscripcion();
  }

  public function masDataResponse() {
    if ($this->uid != 0) {
      $usuario = new \BBVA\Usuario($this->uid, true);
      $usuario->actualizarEstadoSuscripcion();

      $this->userRes->usuario = $usuario->getDatosBasicos();
    } else {
      $this->userRes = NULL;
    }
  }

  protected function buscar_uid($nombre_usuario) {
    $uid = db_select('users','u')
      ->fields('u', array('uid'))
      ->condition('name', $nombre_usuario, '=')
      ->execute()
      ->fetchField();
    return $uid;
  }

}

class UsuarioResponse{

}
