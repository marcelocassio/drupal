<?php

class LoginException extends Exception {

}

class LoginClaveDigitalVencidaException extends LoginException {

}

class LoginDatosCDIncorrectosException extends LoginException {

}
class LoginManualDatosIncorrectosException extends LoginException {

  public function __construct($fp_username = NULL) {

    parent::__construct(BBVA_AUTENTIFICACION_DATOS_INCORRECTO_MSG);

    if(module_exists('bbva_flood_protection') && $fp_username) {
      bbva_flood_protection_insert_attempt($fp_username);
    }
  }
}

class LoginManualFloodProtectionException extends LoginException {
  public function __construct() {
    parent::__construct(BBVA_AUTENTIFICACION_FLOOD_PROTECTION_USUARIO_BLOQUEADO);
  }
}

class LoginClaveDigitalBloqueadaException extends LoginException {

}
class LoginClaveDigitalInhabilitadaException extends LoginException {

}

class LoginDniRepetidoException extends LoginException {

}

class LoginInvalidoException extends LoginException {

}

class LoginUsuarioNoencontradoException extends LoginException {
  public $auth_id;
  
  public function __construct($auth_id = 0) {
    $this->auth_id = $auth_id;
    parent::__construct();
  }
}