<?php
class AutentificacionBbvaFNET extends AutentificacionBbva
{

  public $datosCliente;
  private $vinculaciones;

  public function __construct($credenciales) {
    $this->credenciales = $credenciales;
    $this->medio = BBVA_AUTH_TIPO_FNET;
  }

  /*OVERRRIDE*/
  public function validarCredenciales() {
    
    $respuesta = null;
    
    $document_type    = $this->credenciales["tipoDoc"];
    $document_number  = $this->credenciales["nroDoc"];
    $digital_user     = $this->credenciales["usuario"];
    $digital_pass     = $this->credenciales["clave"];
      
    $validacion_cd = $this->validarClaveDigital($document_type, $document_number, $digital_user, $digital_pass);

      if ($validacion_cd->codigo == "0") {
        $nombre_usuario = $validacion_cd->nroAltamira;
        if (is_numeric($nombre_usuario) && $nombre_usuario != 0) {
          $this->name_user = $nombre_usuario;
          $this->datosCliente = $validacion_cd;
          return $this->name_user;
        } else{
          throw new LoginException(BBVA_AUTENTIFICACION_ERROR_MENSAJE_GENERICO, "1");
        }
      } else {
        if ($validacion_cd->codigo == "30") {
          throw new LoginDniRepetidoException();
        } else if ($validacion_cd->codigo == "4" || $validacion_cd->codigo == "12") {
          throw new LoginDatosCDIncorrectosException();
        } else if ($validacion_cd->codigo == "2") {
          throw new LoginClaveDigitalVencidaException();
        } else if ($validacion_cd->codigo == "5") {
          throw new LoginClaveDigitalBloqueadaException();
        } else if ($validacion_cd->codigo == "13") {
          throw new LoginClaveDigitalInhabilitadaException();
        } else {
          $msg_retorno = $validacion_cd->descripcion;
          $codigo = $validacion_cd->codigo;
          throw new LoginInvalidoException($msg_retorno, $codigo);
        }
      }
  }

  /*OVERRRIDE*/
  public function getUserIdRrss() {
    $this->uid = bbva_autentificacion_buscar_usuario_id_fnet($this->name_user);
    if ($this->uid) {
      $this->actualizarDatosPersonales();
    }
  }

  /*OVERRRIDE*/
  public function crearUsuarioDrupal() {
    if (!$this->existe_usuario($this->name_user)) {
      parent::crearUsuarioDrupal();
    } else {
      $this->uid = $this->buscar_uid($this->name_user);
    }
    $this->actualizarDatosPersonales();
  }

  /*OVERRRIDE*/
  public function relacionarUserIdRrss() {

    if (isset($this->uid) && $this->uid <> 0) {
      $user_identificador = $this->name_user;

      $usuario = new \BBVA\Usuario($this->uid, null);
      $usuario->obtenerVinculaciones();
      $this->vinculaciones = $usuario->data->vinculaciones;

      bbva_autentificacion_guardar_usuario_id_fnet($this->uid, $this->name_user, true);
    } else {
      throw new Exception("Hubo una inconsistencia al relacionar fnet con uid ");
    }
  }


  private function validarClaveDigital($document_type, $document_number, $digital_user, $digital_pass) {
    $datos_usuario = null;

    $fnet_request_data = array(
      'tipodoc' => $document_type,
      'nrodoc'  => $document_number,
      'clave'   => $digital_pass,
      'usuario' => $digital_user
    );

    $fnet = new \Services\BBVA\Fnet();
    $fnet->setUri("/fnet/mod/fgo/login.do");
    $fnet->setData($fnet_request_data);
    $response = $fnet->request();

    if ($response->code == "200") {
      $datos_usuario = $response->data;
      return $datos_usuario;
    }

    $status_message = isset($response->status_message) ? $response->status_message : "";

    throw new Exception("Hubo un error con la validacion de clave digital a FNET | Mensaje ".$status_message.": HTTP CODE=".$response->code);
  }

  protected function actualizarDatosPersonales() {
    $edit = $this->usuarioDrupal->actualizarDatosPersonales($this->datosCliente);

    return $edit;
  }

  public function getDatosCliente() {
    $edit = $this->actualizarDatosPersonales();
    $this->usuarioDrupal->modificar($edit);
    return $this->usuarioDrupal->getDatosBasicos();
  }

  protected function sincronizarVinculaciones() {
    $this->usuarioDrupal->obtenerVinculaciones();
    if(!in_array(BBVA_AUTH_TIPO_FNET, $this->usuarioDrupal->data->vinculaciones)) {
      $this->vinculaciones = $this->usuarioDrupal->data->vinculaciones;
      $this->usuarioDrupal->vincular(BBVA_AUTH_TIPO_FNET, $this->auth_id, TRUE);
    }
  }

  private function existe_usuario($nombre_usuario) {
    $user_found = false;
    $uid = db_select('users','u')
      ->fields('u', array('uid'))
      ->condition('name', $nombre_usuario, '=')
      ->execute()
      ->fetchField();
    if (!empty($uid)) {
      $user_found = true;
    }
    return $user_found;
  }

  private function buscar_usuario_por_mail($mail) {
    $user_found = false;
    $query = db_select('users','u')
      ->fields('u', array('uid'));
    $query->where('TRIM(LOWER(u.mail)) = :mail', array(':mail' => strtolower(trim($mail))));
    $uid = $query->execute()->fetchField();
    if (!empty($uid)) {
      $uid = true;
    }
    return $uid;
  }

  public function getMediosSugeridos() {
    $vinculaciones = null;
    if (isset($this->vinculaciones)) {
      if (in_array(BBVA_AUTH_TIPO_GOOGLE, $this->vinculaciones)) {
        $vinculaciones[] = BBVA_AUTH_TIPO_GOOGLE;
      }
      if (in_array(BBVA_AUTH_TIPO_FB, $this->vinculaciones)) {
        $vinculaciones[] = BBVA_AUTH_TIPO_FB;
      }
    }
    return $vinculaciones;
  }

}
