<?php
class AutentificacionBbvaManual extends AutentificacionBbva{

  public $datosCliente;
  private $fp_username;

  public function __construct($credenciales) {
    $this->credenciales = $credenciales;
    $this->medio = BBVA_AUTH_TIPO_MANUAL;
    $this->fp_username = NULL;
  }

  /*OVERRRIDE*/
  public function validarCredenciales() {
    if(module_exists('bbva_flood_protection')) {
      $is_blocked = FALSE;
      
      if(isset($this->credenciales["tipoDoc"]) && isset($this->credenciales["nroDoc"])){
        $this->fp_username = $this->credenciales["tipoDoc"] . $this->credenciales["nroDoc"];
      }
      if (isset($this->credenciales["mail"])) {
        $this->fp_username = $this->credenciales["mail"];
      }
      $is_blocked = bbva_flood_protection_is_blocked($this->fp_username);
      if($is_blocked) {
        throw new LoginManualFloodProtectionException();
      }
    }

    if (isset($this->credenciales["mail"])) {
      $nombre_usuario = $this->validarMail();
    } else if (isset($this->credenciales["nroDoc"]) && isset($this->credenciales["tipoDoc"])){
      $nombre_usuario = $this->validarDocumento();
    } else {
      throw new LoginException();
    }

    $uid = user_authenticate($nombre_usuario, $this->credenciales["clave"]);
    if ($uid != FALSE) {
      return $uid;
    }

    throw new LoginManualDatosIncorrectosException($this->fp_username);
  }

  /*OVERRRIDE*/
  public function getUserIdRrss() {
    // no crear usuario
  }

  /*OVERRRIDE*/
  public function crearUsuarioDrupal() {
    // no crear usuario
  }

  /*OVERRRIDE*/
  public function relacionarUserIdRrss() {
    // no relacionar
  }

  private function consultarClientesEstadoOcho($document_type, $document_number)  {

    $datos_usuario = null;

    $fnet_request_data = array('tipodoc'=>$document_type,
      'nrodoc'=>$document_number,
      'method'=>'consultarNoCliente');


    $fnet = new \Services\BBVA\Fnet();
    $fnet->setUri("/fnet/mod/fgo/NL-cliente.do");
    $fnet->setData($fnet_request_data);
    $response = $fnet->request();
    $mensaje = isset($response->status_message) ? $response->status_message : '';

    if ($response->code == "200") {
      $datos_usuario = $response->data;
    } else {
      throw new Exception("Hubo un error con la validacion de clave digital a FNET | Mensaje"  . $mensaje . ": HTTP CODE=" . $response->code);
    }

    return $datos_usuario;
  }


  private function buscarNombreUsuario($mail) {
    $name_user = NULL;
    $query = db_select('users','u')
      ->fields('u', array('uid', 'name'));
    $query->where('TRIM(LOWER(u.mail)) = :mail', array(':mail' => strtolower(trim($mail))));
    $res = $query->execute()->fetchAll();
    if (empty($res)) {
      $name_user = FALSE;
    } else {
      $name_user = $res[0]->name;
    }
    return $name_user;
  }

  private function validarMail() {
    $mail = $this->credenciales["mail"];
    $nombre_usuario = $this->buscarNombreUsuario($mail);
    if ($nombre_usuario) {
      return $nombre_usuario;
    }
    throw new LoginManualDatosIncorrectosException($this->fp_username);
  }

  private function validarDocumento() {
    $document_type = $this->credenciales["tipoDoc"];
    $document_number = $this->credenciales["nroDoc"];
    $password = $this->credenciales["clave"];

    if (isset($this->credenciales["sexo"])) {
      $sexo = strtoupper($this->credenciales["sexo"]);
      if ($sexo == "M") {
        $document_type = "DNI_MASC";
      } else if ($sexo == "F") {
        $document_type = "DNI_FEM";
      }
    }

    $respuesta = $this->consultarClientesEstadoOcho($document_type, $document_number);

    if ($respuesta->codigo == "0") {
      return $respuesta->numeroAltamira;
    } else if ($respuesta->codigo == "01") {
      throw new LoginManualDatosIncorrectosException($this->fp_username);
    } else if ($respuesta->codigo == "05") {
      throw new LoginDniRepetidoException();
    } else {
      throw new LoginException();
    }
  }


}
