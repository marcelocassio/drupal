<?php

class AutentificacionBbvaFactory {
  public static function create($tipo, $credenciales, $rrss_keys=NULL) {

  	switch ($tipo) {
      case BBVA_AUTH_TIPO_FB:
          return new AutentificacionBbvaFB($credenciales, $rrss_keys);
      	break;
      case BBVA_AUTH_TIPO_GOOGLE:
          return new AutentificacionBbvaGoogle($credenciales, $rrss_keys);
      case BBVA_AUTH_TIPO_APPLE:
          return new AutentificacionBbvaApple($credenciales, $rrss_keys);    
          break;
      case BBVA_AUTH_TIPO_FNET:
          return new AutentificacionBbvaFnet($credenciales);
          break;
      case BBVA_AUTH_TIPO_MANUAL:
          return new AutentificacionBbvaManual($credenciales);
          break;
      default:
      	# code...
      	break;
  	}
  }
}
