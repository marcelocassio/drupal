<?php
/**
 * Metodos para los formularios de edicion y creacion de variablees
 */

function fgo_backend_variable_agregar_form()
{
    return fgo_backend_variable_display_form();
}

function fgo_backend_variable_editar_form($form_state, $build)
{
    $variable_id = $build["build_info"]["args"][0];
    return fgo_backend_variable_display_form($variable_id);
}

function fgo_backend_variable_display_form($variable_id = null)
{
    
    $_default = array(
        'cliente' => '',
        'segmento_cod' => '',
        'segmento_desc' => ''

    );

    if (!is_null($variable_id)) {
        $variable = variable_get($variable_id);
        $_default['idSucursalVirtual'] = $variable_id;
        $_default['web'] = $variable;
       
    }

    $form['redirect'] = false;

    $form['#attributes']['autocomplete'] = "off";

    $form['#validate'][] = 'fgo_backend_variable_form_validate';

    $form['idSucursalVirtual'] = array(
        '#type' => 'hidden',
        '#default_value' => $variable_id
    );

    $form['grupo_dato'] = array(
        '#type' => 'fieldset',
        '#title' => t('<strong><i>'.$variable_id.'</i></strong>')
    );

    $form['grupo_dato']['web'] = array(
        '#title' => t('Descripcion'),
        '#type' => 'textfield',
        '#maxlength' => 230,
        '#size' => 117,
        '#weight' => 1,
        '#default_value' => $_default['web']
    );
   
    

    $form['guardar'] = array(
        '#type' => 'submit',
        '#id' => 'guardar_slide',
        '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
        '#value' => 'Guardar',
        '#submit' => array('fgo_backend_variable_submit')
    );

    return $form;
}

function fgo_backend_variable_form_validate(&$form, &$form_state)
{
    $name = $form_state['values']['idSucursalVirtual'];
    $value = $form_state['values']['web'];
    variable_set($name, $value);
}

function fgo_backend_variable_submit(&$form, &$form_state)
{
   // $form_state['redirect'] = url('../admin/v2/variable/lista');
}
