<div id="admin_container">
    <h1>Administración de Variable Web</h1>
    <div v-if="loading" class="spinner_backend" v-cloak>
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    <vue-good-table v-else
    :columns="formattedColumns"
    :rows="rows"
    style-class="vgt-table striped condensed"
    :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: true, perPage: 40, ofLabel: 'de'}"
    :search-options="{ enabled: false, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
        
        <div v-slot:emptystate>
            No hay variablees con ese criterio
        </div>
        <template v-slot:table-row="props">
            <div v-if="props.column.field == 'botones'">
                <div v-html="props.row.botones"></div>
            </div>
            <div v-else>
                {{props.formattedRow[props.column.field]}}
            </div>
        </template>
    </vue-good-table>
    <vuedals></vuedals>
</div>

<script>
    const link = '<?php echo $base_url?>/API/v1/backend/variable';
    const linkAdd = '<?php echo $base_url?>/admin/v2/variable/agregar';
    const linkBorrar = '<?php echo $base_url?>/API/v1/backend/variable/borrar/';
    const FGO_BACKEND_IMG_DEFAULT = '<?php echo $base_url?>/static/media/logogo.1cb228c2.svg';

    // Contantes para el modal
    const Bus = Vuedals.Bus;
    const Component = Vuedals.Component;
    const Plugin = Vuedals.default;

      Vue.use(Plugin);

 

        new Vue({
            el: '#admin_container',
            data: {
                rows: [],
                idSegmentacionHome: null,
                loading: false,
                columns: [
                {
                    label: 'Cliente',
                    field: 'cliente',
                    type: 'text'
                },
                {
                    label: 'Cod',
                    field: 'segmento_cod',
                    type: 'text'
                },
                {
                    label: 'Desc',
                    field: 'segmento_desc',
                    type: 'text'
                },
                {
                    label: 'Editar',
                    field: 'botones',
                    width: '100px',
                    globalSearchDisabled: true,
                },
                ]
            },
            methods:{
                getSegmentaciones(){
                    this.loading = true;
                    axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200 && response.data.code === 0){
                            this.rows = response.data.data;
                        }else{
                            this.rows = null;
                        }
                    })
                    .catch(error => {
                        this.rows = null;
                    })
                    .finally(() => this.loading = false)
                },
                renderData(rows) {
                    if (rows){
                        rows.map(item => {
                            item.botones = ' <div class="accion_editar_variable"><a href="<?=$base_url;?>/admin/v2/variable/editar/'+item.cliente+'" class="btn btn-outline-info btn-md"> Editar </a></div>';
                        });
                    }
                },
                newItem(){
                    window.location.href = linkAdd;
                },
                openDeleteModal(idSegmentacionHome) {
                    this.idSegmentacionHome = idSegmentacionHome;
                    Bus.$emit('new', {
                        title: 'Borrar variable',
                        component: borrarSegmentacion
                    });
                },
                _parseImage(img) {
                    return img.replace("public://","/fgo/sites/default/files/");
                }
            },
            computed:{
                formattedColumns(){
                    if(this.rows){
                        this.renderData(this.rows);
                        return this.columns;
                    }
                }
            },
            mounted() {
                this.getSegmentaciones();
            }
        });
</script>
