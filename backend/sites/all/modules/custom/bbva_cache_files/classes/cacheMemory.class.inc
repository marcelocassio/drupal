<?php

namespace BBVA\Cache;

class CacheMemory implements Cache {

  private static $instancia;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
  }

  public function get($tipo, $id) {
    $name = $tipo . "-" . $id;
    $cache = cache_get($name, 'api');

    if($cache->data) {
      return $cache->data;
    }

    return FALSE;
  }

  public function set($tipo, $id, $data) {
    $name = $tipo . "-" . $id;
    $cache = cache_set($name, $data, 'api');

    return TRUE;
  }

  public function clean() {
    try {
      $tipos = array("campanias", "comunicaciones", "etiquetas", "home", "cta", "parametros");
      foreach ($tipos as $tipo) {
        shell_exec("mkdir " . DRUPAL_ROOT . self::BBVA_CACHE_FILES_PATH . $tipo);
        shell_exec("rm -R " . DRUPAL_ROOT . self::BBVA_CACHE_FILES_PATH . $tipo . "/*");
      }
      return TRUE;
    } catch (Exception $e) {}
    
    return FALSE;
  }
}
