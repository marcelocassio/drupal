<?php

namespace BBVA\Cache;

interface Cache {

  public static function getInstance();

  public function get($tipo, $id);

  public function set($tipo, $id, $data);

  public function clean();
}
