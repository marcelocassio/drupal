<?php

namespace BBVA\Cache;

class CacheFile implements Cache {

  const BBVA_CACHE_FILES_PATH = "/sites/default/files/cache/";
  private static $instancia;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
  }

  private function _crearCarpeta($carpeta) {
    if(!file_exists($carpeta)) {
      shell_exec("mkdir " . $carpeta );
      shell_exec("chmod 760 -R " . $carpeta );

      return true;
    }
    return false;
  }

  public function get($tipo, $id) {
    $json = FALSE;
    $filename = DRUPAL_ROOT . self::BBVA_CACHE_FILES_PATH . $tipo . "/" . $id . ".json";

    if(file_exists($filename)) {
      if(variable_get('bbva_cache_file_activo', false)) {
        $context = stream_context_create(array('http' => array('header' => 'Connection: close')));
        $str = file_get_contents($filename, false, $context);
        $json = json_decode($str);
      }
    }

    if($json) {
      return $json;
    }

    return FALSE;
  }

  public function set($tipo, $id, $data) {
    if(variable_get('bbva_cache_file_activo', false)) {
      $filename = DRUPAL_ROOT . self::BBVA_CACHE_FILES_PATH . $tipo . "/" . $id . ".json";
      $fp = fopen($filename, 'w');
      fwrite($fp, json_encode($data));
      fclose($fp);
      return TRUE;
    }
    return FALSE;
  }

  public function clean($tiposCache = array()) {
    try {
      if(!empty($tiposCache)) {
        $tipos = $tiposCache;
      } else {
        $constantes = \Fgo\Services\Api::obtenerConstantesCache();
        $tipos = array();
        foreach ($constantes as $constante) {
          $tipos[] = $constante["tipo"];
        }
      }
      if(!empty($tipos)) {
        // Si no existen las carpetas principales las creo
        $carpeta_client =  DRUPAL_ROOT . self::BBVA_CACHE_FILES_PATH . "client";
        $this->_crearCarpeta($carpeta_client);
        $carpeta_backend =  DRUPAL_ROOT . self::BBVA_CACHE_FILES_PATH . "backend";
        $this->_crearCarpeta($carpeta_backend);
        
        foreach ($tipos as $tipo) {
          $carpeta =  DRUPAL_ROOT . self::BBVA_CACHE_FILES_PATH . $tipo;
          // Creo la carpeta, si ya existe la vacío
          $crear_carpeta = $this->_crearCarpeta($carpeta);
          if (!$crear_carpeta) {
            shell_exec("rm -R " . $carpeta . "/*");
          }
        }
      }
      return TRUE;
    } catch (Exception $e) {}
    
    return FALSE;
  }
}
