<?php

function bbva_cache_files_ls($directorio){
 
  // Array en el que obtendremos los resultados
  $result = array(
    "carpetas" => array(), 
    "archivos" => array(), 
    "peso" => 0
  );

  $peso = 0;
  // Agregamos la barra invertida al final en caso de que no exista
  if(substr($directorio, -1) != "/") $directorio .= "/";
 
  // Creamos un puntero al directorio y obtenemos el listado de archivos
  $dir = @dir($directorio) or die("getFileList: Error abriendo el directorio $directorio para leerlo");
  while(($archivo = $dir->read()) !== false) {
    $peso = 0;
    if($archivo == '.' || $archivo == '..') {
      continue;
    }
    if(is_dir($directorio.$archivo)) {
      $result["carpetas"][] = array(
        "nombre" => $directorio . $archivo . "/",
        "peso" => 0,
        "modificado" => filemtime($directorio . $archivo),
      );
    } 
    else if (is_readable($directorio . $archivo)) {
        $peso = filesize($directorio . $archivo);
        $result["archivos"][] = array(
          "nombre" => $archivo,
          "peso" => $peso,
          "modificado" => filemtime($directorio . $archivo)
        );
    }
    $result["peso"] += $peso;
  }
  $dir->close();
  return $result;
}