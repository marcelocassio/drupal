<?php

/**
 * Admin settings page.
 */
function bbva_cache_files_settings($form, $form_state) {

  $form['bbva_cache_file_activo'] = array(
    '#title' => t('Habilitar caché en archivos'),
    '#description' => t('Activar para que se generen archivos con las respuestas'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_cache_file_activo', FALSE),
  );

  return system_settings_form($form);
}
