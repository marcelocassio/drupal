<?php
/**
 * Metodos para los formularios de edicion y creacion de segmentaciones
 */

function fgo_backend_segmentacion_agregar_form()
{
    return fgo_backend_segmentacion_display_form();
}

function fgo_backend_segmentacion_editar_form($form_state, $build)
{
    $segmentacion_id = $build["build_info"]["args"][0];
    return fgo_backend_segmentacion_display_form($segmentacion_id);
}

function fgo_backend_segmentacion_display_form($segmentacion_id = null)
{
    
    $_default = array(
        'cliente' => '',
        'segmento_cod' => '',
        'segmento_desc' => '',
        'Cod'=>'',
        'subscription_close_dt'=>'',
        'business_area_cd',
        'col_xt_2'=>'',
        'col_xt_3'=>'',
        'col_xt_4'=>'',
        'es_cliente_actividad' =>'0',
        'tiene_ca_ars_en_actividad'=>'0',
        'tiene_ca_ars_en_actividad'=>'0',
        'tiene_ca_usd_en_actividad'=>'0',
        'tiene_cc_ars_en_actividad'=>'0',
        'tiene_tc_visa_en_actividad'=>'0',
        'tiene_tc_master_en_actividad'=>'0',
        'tiene_pp_en_actividad'=>'0',
        'es_titular_tc_visa'=>'0',
        'es_titular_tc_visa_latam'=>'0',
        'es_titular_tc_visa_nolatam'=>'0',
        'es_titular_tc_visa_nolatam_inter'=>'0',
        'es_titular_tc_master'=>'0',
        'es_titular_tc_master_latam'=>'0',
        'es_titular_tc_master_nolatam'=>'0',
        'es_adicional_tc_visa'=>'0',
        'es_adicional_tc_visa_latam'=>'0',
        'es_adicional_tc_visa_nolatam_inter'=>'0',
        'es_adicional_tc_master'=>'0',
        'es_adicional_tc_master_latam'=>'0',
        'es_adicional_tc_master_nolatam'=>'0',
        'es_adicional_tc_master_nolatam_inter'=>'0'
  );

    

    if (!is_null($segmentacion_id)) {
        $segmentacionBo = new \Fgo\Bo\SegmentacionBo($segmentacion_id);
        $_default['idSegmentacion'] = $segmentacion_id;
        $_default['cliente'] = $segmentacionBo->cliente;
        $_default['Cod'] = $segmentacionBo->segmento_cod;
        $_default['segmento_desc'] = $segmentacionBo->segmento_desc;
        $_default['es_cliente_actividad'] =  $segmentacionBo->es_cliente_actividad;
        $_default['tiene_ca_ars_en_actividad'] =  $segmentacionBo->tiene_ca_ars_en_actividad;
        $_default['tiene_ca_usd_en_actividad'] =  $segmentacionBo->tiene_ca_usd_en_actividad;
        $_default['tiene_cc_ars_en_actividad'] =  $segmentacionBo->tiene_cc_ars_en_actividad;
        $_default['tiene_cc_usd_en_actividad'] =  $segmentacionBo->tiene_cc_usd_en_actividad;
        $_default['tiene_tc_visa_en_actividad'] =  $segmentacionBo->tiene_tc_visa_en_actividad;
        $_default['tiene_tc_master_en_actividad'] =  $segmentacionBo->tiene_tc_master_en_actividad;
        $_default['tiene_pp_en_actividad'] =  $segmentacionBo->tiene_pp_en_actividad;
        $_default['es_titular_tc_visa'] =  $segmentacionBo->es_titular_tc_visa;
        $_default['es_titular_tc_visa_latam'] =  $segmentacionBo->es_titular_tc_visa_latam;
        $_default['es_titular_tc_visa_nolatam'] =  $segmentacionBo->es_titular_tc_visa_nolatam;
        $_default['es_titular_tc_visa_nolatam_inter'] =  $segmentacionBo->es_titular_tc_visa_nolatam_inter;
        $_default['es_titular_tc_master'] =  $segmentacionBo->es_titular_tc_master;
        $_default['es_titular_tc_master_latam'] =  $segmentacionBo->es_titular_tc_master_latam;
        $_default['es_titular_tc_master_nolatam'] =  $segmentacionBo->es_titular_tc_master_nolatam;
        $_default['es_titular_tc_master_nolatam_inter'] =  $segmentacionBo->es_titular_tc_master_nolatam_inter;
        $_default['es_adicional_tc_visa'] =  $segmentacionBo->es_adicional_tc_visa;
        $_default['es_adicional_tc_visa_latam'] =  $segmentacionBo->es_adicional_tc_visa_latam;
        $_default['es_adicional_tc_visa_nolatam'] =  $segmentacionBo->es_adicional_tc_visa_nolatam;
        $_default['es_adicional_tc_visa_nolatam_inter'] =  $segmentacionBo->es_adicional_tc_visa_nolatam_inter;
        $_default['es_adicional_tc_master'] =  $segmentacionBo->es_adicional_tc_master;
        $_default['es_adicional_tc_master_latam'] =  $segmentacionBo->es_adicional_tc_master_latam;
        $_default['es_adicional_tc_master_nolatam'] =  $segmentacionBo->es_adicional_tc_master_nolatam;
        $_default['es_adicional_tc_master_nolatam_inter'] =  $segmentacionBo->es_adicional_tc_master_nolatam_inter;
        $_default['es_ps'] =  $segmentacionBo->es_ps;
        $_default['business_area_cd'] =  $segmentacionBo->business_area_cd;
        $_default['col_xt_2'] =  $segmentacionBo->col_xt_2;
        $_default['col_xt_3'] =  $segmentacionBo->col_xt_3;
        $_default['col_xt_4'] =  $segmentacionBo->col_xt_4;
    }

    $form['redirect'] = false;

    $form['#attributes']['autocomplete'] = "off";

    $form['#validate'][] = 'fgo_backend_segmentacion_form_validate';

    $form['idSegmentacion'] = array(
        '#type' => 'hidden',
        '#default_value' => $_default['idSegmentacion']
    );

    $form['grupo_dato'] = array(
        '#type' => 'fieldset',
        '#title' => t('<strong><i>datos</i></strong>')
    );

    $form['grupo_dato']['cliente'] = array(
        '#title' => t('Cliente'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 1,
        '#default_value' => $_default['cliente']
    );

    $form['grupo_dato']['Cod'] = array(
        '#title' => t('Cod'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 2,
        '#default_value' => $_default['Cod']
    );
    $form['grupo_dato']['segmento_desc'] = array(
        '#title' => t('segmento_desc'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 3,
        '#default_value' => $_default['segmento_desc']
    );
    
    
     $form['grupo_dato']['business_area_cd'] = array(
        '#title' => t('business_area_cd'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 3,
        '#default_value' => $_default['business_area_cd']
    );
      $form['grupo_dato']['col_xt_2'] = array(
        '#title' => t('col_xt_2'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 3,
        '#default_value' => $_default['col_xt_2']
    );
      
     $form['grupo_dato']['col_xt_3'] = array(
        '#title' => t('col_xt_3'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 3,
        '#default_value' => $_default['col_xt_3']
    );
     $form['grupo_dato']['col_xt_4'] = array(
        '#title' => t('col_xt_4'),
        '#type' => 'textfield',
        '#maxlength' => 30,
        '#size' => 17,
        '#weight' => 3,
        '#default_value' => $_default['col_xt_4']
    ); 
    //---------------------//
    
    
    $form['grupo_dato']['es_cliente_actividad'] = array(
      '#title' => t('es_cliente_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_cliente_actividad'],
    );

    $form['grupo_dato']['tiene_ca_ars_en_actividad'] = array(
      '#title' => t('tiene_ca_ars_en_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['tiene_ca_ars_en_actividad'],
    );
    
    $form['grupo_dato']['tiene_ca_usd_en_actividad'] = array(
      '#title' => t('tiene_ca_usd_en_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['tiene_ca_usd_en_actividad'],
    );
    $form['grupo_dato']['tiene_cc_ars_en_actividad'] = array(
      '#title' => t('tiene_cc_ars_en_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['tiene_cc_ars_en_actividad'],
    );
    $form['grupo_dato']['tiene_cc_usd_en_actividad'] = array(
      '#title' => t('tiene_cc_usd_en_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['tiene_cc_usd_en_actividad'],
    );
    $form['grupo_dato']['tiene_tc_visa_en_actividad'] = array(
      '#title' => t('tiene_tc_visa_en_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['tiene_tc_visa_en_actividad'],
    );
    $form['grupo_dato']['tiene_tc_master_en_actividad'] = array(
      '#title' => t('tiene_tc_master_en_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['tiene_tc_master_en_actividad'],
    );
    $form['grupo_dato']['tiene_pp_en_actividad'] = array(
      '#title' => t('tiene_pp_en_actividad '),
      '#type' => 'checkbox',
      '#default_value' => $_default['tiene_pp_en_actividad'],
    );
    $form['grupo_dato']['es_titular_tc_visa'] = array(
      '#title' => t('es_titular_tc_visa '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_titular_tc_visa'],
    );
    $form['grupo_dato']['es_titular_tc_visa_latam'] = array(
      '#title' => t('es_titular_tc_visa_latam '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_titular_tc_visa_latam'],
    );
    $form['grupo_dato']['es_titular_tc_visa_nolatam'] = array(
      '#title' => t('es_titular_tc_visa_nolatam '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_titular_tc_visa_nolatam'],
    );
    $form['grupo_dato']['es_titular_tc_visa_nolatam_inter'] = array(
      '#title' => t('es_titular_tc_visa_nolatam_inter '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_titular_tc_visa_nolatam_inter'],
    );
    $form['grupo_dato']['es_titular_tc_master'] = array(
      '#title' => t('es_titular_tc_master '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_titular_tc_master'],
    );
    
    $form['grupo_dato']['es_titular_tc_master_latam'] = array(
      '#title' => t('es_titular_tc_master_latam '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_titular_tc_master_latam'],
    );
    $form['grupo_dato']['es_titular_tc_master_nolatam'] = array(
      '#title' => t('es_titular_tc_master_nolatam '),
      '#type' => 'checkbox',
      '#default_value' =>$_default['es_titular_tc_master_nolatam'],
    );
    $form['grupo_dato']['es_titular_tc_master_nolatam_inter'] = array(
      '#title' => t('es_titular_tc_master_nolatam_inter '),
      '#type' => 'checkbox',
      '#default_value' =>$_default['es_titular_tc_master_nolatam_inter'],
    );
    $form['grupo_dato']['es_adicional_tc_visa'] = array(
      '#title' => t('es_adicional_tc_visa '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_visa'],
    );
    $form['grupo_dato']['es_adicional_tc_visa_latam'] = array(
      '#title' => t('es_adicional_tc_visa_latam '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_visa_latam'],
    );
    
    $form['grupo_dato']['es_adicional_tc_visa_nolatam'] = array(
      '#title' => t('es_adicional_tc_visa_nolatam '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_visa_nolatam'],
    );
    $form['grupo_dato']['es_adicional_tc_visa_nolatam_inter'] = array(
      '#title' => t('es_adicional_tc_visa_nolatam_inter '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_visa_nolatam_inter'],
    );
    $form['grupo_dato']['es_adicional_tc_master'] = array(
      '#title' => t('es_adicional_tc_master '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_master'],
    );
    $form['grupo_dato']['es_adicional_tc_master_latam'] = array(
      '#title' => t('es_adicional_tc_master_latam '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_master_latam'],
    );
    $form['grupo_dato']['es_adicional_tc_master_nolatam'] = array(
      '#title' => t('es_adicional_tc_master_nolatam '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_master_nolatam'],
    );
    $form['grupo_dato']['es_adicional_tc_master_nolatam_inter'] = array(
      '#title' => t('es_adicional_tc_master_nolatam_inter '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_adicional_tc_master_nolatam_inter'],
    );
    $form['grupo_dato']['es_ps'] = array(
      '#title' => t('es_ps '),
      '#type' => 'checkbox',
      '#default_value' => $_default['es_ps'],
    );
    
    

    $form['guardar'] = array(
        '#type' => 'submit',
        '#id' => 'guardar_slide',
        '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
        '#value' => 'Guardar',
        '#submit' => array('fgo_backend_segmentacion_submit')
    );

    return $form;
}

function fgo_backend_segmentacion_form_validate(&$form, &$form_state)
{
    $segmentacionBo = new \Fgo\Bo\SegmentacionBo($form_state['values']['idSegmentacion']);
    $segmentacionBo->cliente = $form_state['values']['cliente'];
    $segmentacionBo->segmento_cod = $form_state['values']['Cod'];
    $segmentacionBo->segmento_desc = $form_state['values']['segmento_desc'];
    $segmentacionBo->es_cliente_actividad = $form_state['values']['es_cliente_actividad'];
    $segmentacionBo->tiene_ca_ars_en_actividad = $form_state['values']['tiene_ca_ars_en_actividad'];
    $segmentacionBo->tiene_ca_usd_en_actividad = $form_state['values']['tiene_ca_usd_en_actividad'];
    $segmentacionBo->tiene_cc_ars_en_actividad = $form_state['values']['tiene_cc_ars_en_actividad'];
    $segmentacionBo->tiene_cc_usd_en_actividad = $form_state['values']['tiene_cc_usd_en_actividad'];
    $segmentacionBo->tiene_tc_visa_en_actividad = $form_state['values']['tiene_tc_visa_en_actividad'];
    $segmentacionBo->tiene_tc_master_en_actividad = $form_state['values']['tiene_tc_master_en_actividad'];
    $segmentacionBo->tiene_pp_en_actividad = $form_state['values']['tiene_pp_en_actividad'];
    $segmentacionBo->es_titular_tc_visa = $form_state['values']['es_titular_tc_visa'];
    $segmentacionBo->es_titular_tc_visa_latam = $form_state['values']['es_titular_tc_visa_latam'];
    $segmentacionBo->es_titular_tc_visa_nolatam = $form_state['values']['es_titular_tc_visa_nolatam'];
    $segmentacionBo->es_titular_tc_visa_nolatam_inter = $form_state['values']['es_titular_tc_visa_nolatam_inter'];
    $segmentacionBo->es_titular_tc_master = $form_state['values']['es_titular_tc_master'];
    $segmentacionBo->es_titular_tc_master_latam = $form_state['values']['es_titular_tc_master_latam'];
    $segmentacionBo->es_titular_tc_master_nolatam = $form_state['values']['es_titular_tc_master_nolatam'];
    $segmentacionBo->es_titular_tc_master_nolatam_inter = $form_state['values']['es_titular_tc_master_nolatam_inter'];
    $segmentacionBo->es_adicional_tc_visa = $form_state['values']['es_adicional_tc_visa'];
    $segmentacionBo->es_adicional_tc_visa_latam = $form_state['values']['es_adicional_tc_visa_latam'];
    $segmentacionBo->es_adicional_tc_visa_nolatam = $form_state['values']['es_adicional_tc_visa_nolatam'];
    $segmentacionBo->es_adicional_tc_visa_nolatam_inter = $form_state['values']['es_adicional_tc_visa_nolatam_inter'];
    
    $segmentacionBo->es_adicional_tc_master = $form_state['values']['es_adicional_tc_master'];
    $segmentacionBo->es_adicional_tc_master_latam = $form_state['values']['es_adicional_tc_master_latam'];
    $segmentacionBo->es_adicional_tc_master_nolatam = $form_state['values']['es_adicional_tc_master_nolatam'];
    $segmentacionBo->es_adicional_tc_master_nolatam_inter = $form_state['values']['es_adicional_tc_master_nolatam_inter'];
    $segmentacionBo->es_ps = $form_state['values']['es_ps'];
    $segmentacionBo->business_area_cd = $form_state['values']['business_area_cd'];
    $segmentacionBo->col_xt_2 = $form_state['values']['col_xt_2'];
    $segmentacionBo->col_xt_3 = $form_state['values']['col_xt_3'];
    $segmentacionBo->col_xt_4 = $form_state['values']['col_xt_4'];
    $segmentacionBo->subscription_close_dt = isset($form_state['values']['subscription_close_dt'])?($form_state['values']['subscription_close_dt']):date("Y/m/d");
    

    try {
        $segmentacionBo->guardar();
    } catch (\Fgo\Bo\DatosInvalidosException $e) {
        $errores = $e->getErrores();
        foreach ($errores as $error_campo => $error_descripcion) {
            form_set_error($error_campo, $error_descripcion);
        }
    }
}

function fgo_backend_segmentacion_submit(&$form, &$form_state)
{
   // $form_state['redirect'] = url('../admin/v2/segmentacion/lista');
}
