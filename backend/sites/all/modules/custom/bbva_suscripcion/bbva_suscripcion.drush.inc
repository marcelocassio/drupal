<?php

/*
 * Implements hook_drush_command().
 */
function bbva_suscripcion_drush_command() {
  $items['export-reporte-suscripciones'] = array(
    'description' => 'Export de reporte de suscripciones',
    'aliases' => array('export-reporte-suscripciones'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

function drush_bbva_suscripcion_export_reporte_suscripciones($odate = NULL) {
  drupal_set_message( "Start" . date("ymd"), 'status', FALSE);

  $batch = array(
    'operations' => array(),
    'title' => t('Batch export de reporte de suscripciones'),
    'init_message' => t('Batch export de reporte de suscripciones is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'drush_bbva_suscripcion_finished',
    'file' => drupal_get_path('module', 'bbva_suscripcion') . '/bbva_suscripcion.batch.inc',
  );


    //Define operaciones
  $batch['operations'][] = array('bbva_suscripcion_batch_reporte',array($odate));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}
