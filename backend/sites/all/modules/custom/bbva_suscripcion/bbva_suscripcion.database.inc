<?php

function _bbva_suscripcion_check_email($email) {
  $result = db_select('users', 'u')
  ->fields('u', array('uid', 'mail', 'name'))
  ->condition('mail', $email)
  ->range(0, 1)
  ->execute()->fetchAll();

  if ($result) {
    return $result;
  }else{
    return null;
  }
}


function _bbva_suscripcion_get_suscriptos_rrss() {
  $query = db_select('bbva_usuarios', 'us');
  $query->leftJoin('users', 'u', 'u.uid = us.uid');
  $query->leftJoin('bbva_user_rrss', 'rs', 'rs.uid = us.uid');
  $query->leftJoin('field_data_field_profile_document_number', 'dn', 'us.uid = dn.entity_id');
  $query->leftJoin('field_data_field_profile_document_type', 'dt', 'us.uid = dt.entity_id');
  $query->leftJoin('field_data_field_profile_nombre', 'no', 'us.uid = no.entity_id');
  $query->leftJoin('field_data_field_profile_apellido', 'ap', 'us.uid = ap.entity_id');
  $query->fields('us');
  $query->fields('rs');
  $query->fields('dn', array('field_profile_document_number_value'));
  $query->fields('dt', array('field_profile_document_type_value'));
  $query->fields('no', array('field_profile_nombre_value'));
  $query->fields('ap', array('field_profile_apellido_value'));
  $query->fields('u',  array('name'));

  $query = $query->isNotNull('us.first_registration_datetime');

  $result = $query->execute()->fetchAll();

  return $result;
}

function _bbva_suscripcion_rrss_registrados($facebook, $google, $fnet) {

  $result = array();

  $query = db_select('bbva_user_rrss', 'rs');
  $query->leftJoin('users', 'u', 'u.uid = rs.uid');
  $query->fields('u', array('name'));
  $query->fields('u', array('uid'));

  if (!$facebook && !$google && $fnet) {
    $query->condition('rs.ufacebook', 0);
    $query->condition('rs.ugoogle', 0);
    $query->condition('rs.ufnet', 0, '<>');
  }

  if (!$facebook && $google && !$fnet) {
    $query->condition('rs.ufacebook', 0);
    $query->condition('rs.ugoogle', 0, '<>');
    $query->condition('rs.ufnet', 0);
  }

  if (!$facebook && $google && $fnet) {
    $query->condition('rs.ufacebook', 0);
    $query->condition('rs.ugoogle', 0, '<>');
    $query->condition('rs.ufnet', 0, '<>');
  }

  if ($facebook && !$google && !$fnet) {
    $query->condition('rs.ufacebook', 0, '<>');
    $query->condition('rs.ugoogle', 0);
    $query->condition('rs.ufnet', 0);
  }

  if ($facebook && !$google && $fnet) {
    $query->condition('rs.ufacebook', 0, '<>');
    $query->condition('rs.ugoogle', 0);
    $query->condition('rs.ufnet', 0, '<>');
  }

  if ($facebook && $google && !$fnet) {
    $query->condition('rs.ufacebook', 0, '<>');
    $query->condition('rs.ugoogle', 0, '<>');
    $query->condition('rs.ufnet', 0);
  }

  if ($facebook && $google && $fnet) {
    $query->condition('rs.ufacebook', 0, '<>');
    $query->condition('rs.ugoogle', 0, '<>');
    $query->condition('rs.ufnet', 0, '<>');
  }

  $result = $query->execute()->fetchAll(\PDO::FETCH_KEY_PAIR);

  return $result;
}
