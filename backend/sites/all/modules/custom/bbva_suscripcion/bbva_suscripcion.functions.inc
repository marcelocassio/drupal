<?php

function bbva_suscripcion_suscripcion_validar_data($tipo, $data) {
  $validar = array();
  $errores = array('campos' => array(), 'textos' => array());
  // Validaciones comunes a todos
  $validar[] = 'acepto_condiciones';
  $validar[] = 'zonas';
  $validar[] = 'rubros';
  $validar[] = 'mail';
  $validar[] = 'tel_operador';
  $validar[] = 'tel_area';
  $validar[] = 'tel_numero';

  if($tipo == "fnet") {

  } elseif($tipo == "fb" || $tipo == "g") {
    $validar[] = 'documento';
    $validar[] = 'nombre';
    $validar[] = 'apellido';
    $validar[] = 'nacimiento';
    $validar[] = 'sexo';
  } elseif($tipo == 'manual') {
    $validar[] = 'documento';
    $validar[] = 'nombre';
    $validar[] = 'apellido';
    $validar[] = 'nacimiento';
    $validar[] = 'pass';
    $validar[] = 'sexo';
  }

  // Validaciones
  if(in_array('documento', $validar)){
    $validos = array("DNI", "CI", "LE", "LC", "PAS");
    if(!in_array($data['documento']['tipo'], $validos)) {
      $errores['campos'][] = 'documento_tipo';
      $errores['textos'][] = 'Por favor, indicá tipo de documento';
    }
    if(!is_numeric($data['documento']['numero']) || $data['documento']['numero'] < 1) {
      $errores['campos'][] = 'documento_numero';
      $errores['textos'][] = 'Por favor, ingresá número de documento';
    }
  }

  if(in_array('nombre', $validar)){
    if(!isset($data['nombre'])) {
      $errores['campos'][] = 'nombre';
      $errores['textos'][] = 'Por favor, ingresá nombre';
    }
  }

  if(in_array('apellido', $validar)){
    if(!isset($data['apellido'])) {
      $errores['campos'][] = 'apellido';
      $errores['textos'][] = 'Por favor, ingresá apellido';
    }
  }

  if(in_array('acepto_condiciones', $validar)){
    if(!isset($data['acepto_condiciones']) || $data['acepto_condiciones'] != "1") {
      $errores['campos'][] = "acepto_condiciones";
      $errores['textos'][] = 'Aceptá los Términos y Condiciones para continuar';
    }
  }

  if(in_array('zonas', $validar)){
    if(!is_array($data['zonas']) || empty($data['zonas'])) {
      $errores['campos'][] = "zonas";
      $errores['textos'][] = 'Por favor, indicar al menos una zona';
    }
  }

  if(in_array('rubros', $validar)){
    if(!is_array($data['rubros']) || empty($data['rubros'])) {
      $errores['campos'][] = "rubros";
      $errores['textos'][] = 'Por favor, indicar al menos un rubro';
    }
  }

  if(in_array('mail', $validar)){
    if(!filter_var($data['mail'], FILTER_VALIDATE_EMAIL)) {
      $errores['campos'][] = "mail";
      $errores['textos'][] = 'Por favor, ingresá un mail válido';
    }
  }

  if(in_array('nacimiento', $validar)){
    $date = DateTime::createFromFormat('Y-m-d', $data['nacimiento']);
    if(!$date) {
      $errores['campos'][] = "nacimiento";
      $errores['textos'][] = 'Por favor, indicá una fecha de nacimiento válida';
    }
  }

  if(in_array('sexo', $validar)){
    $validos = array("M", "F");
    if(!in_array(strtoupper($data['sexo']), $validos)) {
      $errores['campos'][] = "sexo";
      $errores['textos'][] = 'Por favor, indicá sexo';
    }
  }

  if(in_array('tel_operador', $validar)){
    $validos = array("0001", "0002", "0003", "0004");
    if(!in_array($data['telefono']['operador'], $validos)) {
      $errores['campos'][] = "tel_operador";
      $errores['textos'][] = 'Por favor, indicá compañía de teléfono celular';
    }
  }

  if(in_array('tel_area', $validar)){
    $validos = get_code_area_type();
    if(!in_array($data['telefono']['area'], $validos)) {
      $errores['campos'][] = "tel_area";
      $errores['textos'][] = 'Por favor, ingresá código de área telefónico';
    }
  }

  if(in_array('tel_numero', $validar)){
    if(!is_numeric($data['telefono']['numero']) || $data['telefono']['numero'] < 100000) {
      $errores['campos'][] = "tel_numero";
      $errores['textos'][] = 'Por favor, ingresá número de teléfono';
    }
  }

  if(in_array('pass', $validar)){
    if($data['pass']['pass1'] != $data['pass']['pass2']) {
      $errores['campos'][] = "pass";
      $errores['textos'][] = 'Por favor, ingresá una contraseña válida y repetila en el siguiente campo';
    }
  }
  $result = array("valido" => empty($errores['campos']), "errores" => $errores);
  return $result;
}




function bbva_suscripcion_dto_usuario($data_input) {
  $modificaciones = array(
    "mail" => $data_input["datos_suscripcion"]["mail"],
    "zonas" => $data_input["datos_suscripcion"]["zonas"],
    "rubros" => $data_input["datos_suscripcion"]["rubros"],
    "canal" => BBVA_SUSCRIPCION_CANAL_APP_VALUE,
    "acepto_condiciones" => $data_input["datos_suscripcion"]["acepto_condiciones"],
    "field_profile_document_type" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["documento"]["tipo"]))),
    "field_profile_document_number" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["documento"]["numero"]))),
    "field_profile_nombre" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["nombre"]))),
    "field_profile_apellido" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["apellido"]))),
    "field_profile_sex" => array("und" => array(0 => array("value" => strtoupper($data_input["datos_suscripcion"]["sexo"])))),
    "field_profile_day" => array("und" => array(0 => array("value" => substr($data_input["datos_suscripcion"]["nacimiento"], 8)))),
    "field_profile_month" => array("und" => array(0 => array("value" => substr($data_input["datos_suscripcion"]["nacimiento"], 5, 2)))),
    "field_profile_year" => array("und" => array(0 => array("value" => substr($data_input["datos_suscripcion"]["nacimiento"], 0, 4)))),
    "field_profile_compania" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["telefono"]["operador"]))),
    "field_profile_area" => array(
      "und" => array(
        0 => array(
          "value" => ltrim($data_input["datos_suscripcion"]["telefono"]["area"],'0')
      ))),
    "field_profile_celular" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["telefono"]["numero"]))),
    "pass" => (isset($data_input["datos_suscripcion"]["pass"])?$data_input["datos_suscripcion"]["pass"]:null),
    "nacionalidad" => (isset($data_input["datos_suscripcion"]["codigo_nacionalidad"])?$data_input["datos_suscripcion"]["codigo_nacionalidad"]:null)
  );

  return $modificaciones;
}

function bbva_suscripcion_dto_usuario_fnet($data_input) {
  $modificaciones = array(
    "mail" => $data_input["datos_suscripcion"]["mail"],
    "zonas" => $data_input["datos_suscripcion"]["zonas"],
    "rubros" => $data_input["datos_suscripcion"]["rubros"],
    "canal" => BBVA_SUSCRIPCION_CANAL_APP_VALUE,
    "acepto_condiciones" => $data_input["datos_suscripcion"]["acepto_condiciones"],
    "field_profile_compania" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["telefono"]["operador"]))),
    "field_profile_area" => array(
      "und" => array(
        0 => array(
          "value" => ltrim($data_input["datos_suscripcion"]["telefono"]["area"],'0')
    ))),
    "field_profile_celular" => array("und" => array(0 => array("value" => $data_input["datos_suscripcion"]["telefono"]["numero"]))),
  );

  return $modificaciones;
}

function bbva_suscripcion_verificar_dni_fake($tipo_doc, $numero_documento, $sexo = null) {

    $consulta_usuario = new stdClass();
    $consulta_usuario->usuario->estado = strtolower(BBVA_AUTENTIFICACION_ROL_SUSCRIPTO);
    $consulta_usuario->usuario->suscripcion->numeroDeCliente = "45638566";
    return $consulta_usuario;
}
function bbva_suscripcion_verificar_dni($tipo_doc, $numero_documento, $sexo = null) {

  $respuesta = null;
  $respuesta->usuario->uid = NULL;
  $respuesta->usuario->estado = NULL;
  $numero_cliente = null;

  if ($sexo != null && $numero_documento < 10000000) {
    if (strtoupper($sexo) == "M") {
      $tipo_doc = "DNI_MASC";
    } else if (strtoupper($sexo) == "F") {
      $tipo_doc = "DNI_FEM";
    }
  }
  $service = ClienteServiceFnet::getInstance();
  $response = $service->consultarNoCliente($tipo_doc, $numero_documento);
  if ($response->codigo == "0") {
    $numero_cliente = $response->numeroAltamira;
  } else if ($response->codigo == "05") {
    throw new DniDuplicadoSuscripcionException();
  } else if ($response->codigo == "99") {
    throw new Exception();
  }

  $suscripcion = bbva_consultar_esta_suscripto($numero_cliente);
  if ($suscripcion->esta_suscripto()) {
    $respuesta->usuario->estado = strtolower(BBVA_AUTENTIFICACION_ROL_SUSCRIPTO);
    $respuesta->usuario->suscripcion = $suscripcion;
  } else {
    $respuesta->usuario->estado = NULL;
  }

  $usuario = user_load_by_name($numero_cliente);
  if ($usuario != FALSE) {
    $respuesta->usuario->uid = $usuario->uid;
    $respuesta->usuario->user = $usuario;
  } else {
    $respuesta->usuario->uid = NULL;
  }

  return $respuesta;
}

function bbva_consultar_esta_suscripto($numero_altamira) {
  $suscripcionBo = null;
  try {
    $suip_suscriptor_services = \SuscriptorServiceSuip::getInstance();
    $suscripcionBo = $suip_suscriptor_services->consultarSuscripcionByNumeroCliente($numero_altamira);
  } catch (\Exception $e) {
    throw new \BBVA\UsuarioSuipException("no se pudo obtener el documento en suip", 1, $e);
  }

    return $suscripcionBo;
}



function bbva_suscripcion_mail_reporte_suscriptos_rrss() {
  global $base_url;
  $url = $base_url.'/admin/bbva-reporte-suscripcion-csv';
  $logo = $base_url.'/sites/default/files/go_icon.png';

  $html_email_body = theme(
    'mail-reporte',
    array('vars'=>
      array("url"=>$url,
               "logo"=>$logo
        )
      )
  );

  $params = array(
      'body' => $html_email_body,

      'subject' => "Go - Reporte suscriptos Go ",
  );

  $email = variable_get('bbva_suscripcion_reporteform_emails', '');
  drupal_mail('bbva_suscripcion', 'reporte_suscriptos_rrss', $email, language_default(), $params, NULL, TRUE);

}

function bbva_suscripcion_reporte_suscriptos() {

  $result = _bbva_suscripcion_get_suscriptos_rrss();

  header('Content-Type: application/excel');
  header('Content-Disposition: attachment; filename="reporte_suscriptos_fgo_'.date("d_m_Y").'.csv"');

  $fp = fopen('php://output', 'w');

  $linea = '"numero altamira";"nombre y apellido";"Tipo de documento";"Numero documento"; "medio de suscripcion";  "fecha suscripcion"; "otro medios"'.PHP_EOL;
  fputs($fp, $linea);
  foreach ($result as $value) {

    $medios = bbva_suscripcion_obtener_otras_suscripciones($value);
    $linea = $value->name;
    $linea .= ";";
    $linea .= $value->field_profile_nombre_value . " " . $value->field_profile_apellido_value;
    $linea .= ";";
    $linea .= $value->field_profile_document_type_value;
    $linea .= ";";
    $linea .= $value->field_profile_document_number_value;
    $linea .= ";";
    $linea .= $value->first_registration_medio;
    $linea .= ";";
    $linea .= ($value->first_registration_datetime!=NULL)?date("Y-m-d H:i:s", $value->first_registration_datetime):"";
    $linea .= ";";
    $linea .= (empty($medios))?"":implode(",", $medios);
    $linea .= PHP_EOL;

    fputs($fp, $linea);
  }
  fclose($fp);
}

function bbva_suscripcion_obtener_otras_suscripciones($suscripcion) {

  $medios = array();
  if (isset($suscripcion->ufacebook) && $suscripcion->ufacebook != 0) {
    $medios[BBVA_AUTH_TIPO_FB] = BBVA_AUTH_TIPO_FB;
  }

  if (isset($suscripcion->ugoogle) && $suscripcion->ugoogle != 0) {
    $medios[BBVA_AUTH_TIPO_GOOGLE] = BBVA_AUTH_TIPO_GOOGLE;
  }
  if (isset($suscripcion->ufnet) && $suscripcion->ufnet != 0) {
    $medios[BBVA_AUTH_TIPO_FNET] = BBVA_AUTH_TIPO_FNET;
  }

  if (isset($medios[$suscripcion->first_registration_medio])) {
    unset($medios[$suscripcion->first_registration_medio]);
  }

  return array_values($medios);
}



function bbva_suscripcion_completar_fecha(&$fechas_suscripcion, &$listados_clientes) {

  $clientes_fechas=array();
  foreach ($listados_clientes as $numero_cliente => $uid) {
    if (isset($fechas_suscripcion[$numero_cliente])) {
      $clientes_fechas[$uid] = $fechas_suscripcion[$numero_cliente];
    }
  }

  return $clientes_fechas;

}
