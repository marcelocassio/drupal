<?php

define('BBVA_SUSCRIPCION_PATH_NAME_REPORTE', '/prod/jp00/vuelcoDrupal/reporte_suscriptos.txt');

function bbva_suscripcion_batch_reporte() {
  $result = _bbva_suscripcion_get_suscriptos_rrss();


  if (file_exists(BBVA_SUSCRIPCION_PATH_NAME_REPORTE)) {
    unlink(BBVA_SUSCRIPCION_PATH_NAME_REPORTE);
  }

  $fp = fopen(BBVA_SUSCRIPCION_PATH_NAME_REPORTE, 'w');

  $linea = '"numero altamira";"nombre y apellido";"Tipo de documento";"Numero documento"; "medio de suscripcion";  "fecha suscripcion"; "otro medios"'.PHP_EOL;
  fputs($fp, $linea);
  foreach ($result as $value) {

    $medios = bbva_suscripcion_obtener_otras_suscripciones($value);
    $linea = $value->name;
    $linea .= ";";
    $linea .= $value->field_profile_nombre_value . " " . $value->field_profile_apellido_value;
    $linea .= ";";
    $linea .= $value->field_profile_document_type_value;
    $linea .= ";";
    $linea .= $value->field_profile_document_number_value;
    $linea .= ";";
    $linea .= $value->first_registration_medio;
    $linea .= ";";
    $linea .= ($value->first_registration_datetime!=NULL)?date("Y-m-d H:i:s", $value->first_registration_datetime):"";
    $linea .= ";";
    $linea .= (empty($medios))?"":implode(",", $medios);
    $linea .= PHP_EOL;

    fputs($fp, $linea);
  }
  fclose($fp);
}

/**
 * The batch finish handler.
 */
function drush_bbva_suscripcion_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Import suscripciones is Complete!'.date('Y-m-d H:i:s')));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}
