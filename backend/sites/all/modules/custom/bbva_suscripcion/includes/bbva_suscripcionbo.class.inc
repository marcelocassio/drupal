<?php

class SuscripcionBo {
  public $codigoError;

  public $transaccionProcesada;

  public $tipoDeDocumento;
  public $numeroDeDocumento;

  public $nombre;
  public $apellido;
  public $sexo;
  public $codigoDeArea;
  public $codigoDeOperador;
  public $estadoSuscripcion;
  public $fechaNacimiento;
  public $email;
  public $numeroDeCelular;
  public $numeroDeCliente;

  public $rubrosList;
  public $zonasList;
  public $provinciasList;

  public function esta_suscripto() {
    if (strcasecmp($this->estadoSuscripcion, 'SC') == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function tiene_confirmacion_pendiente() {
    if (strcasecmp($this->estadoSuscripcion, 'SPCT') == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function tiene_carga_datos_pendiente() {
    if (strcasecmp($this->estadoSuscripcion, 'SPID') == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function es_no_registrado() {
    if (strcasecmp($this->estadoSuscripcion, 'NR') == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function es_desconocido() {
    if (strcasecmp($this->estadoSuscripcion, 'NN') == 0) {
      return true;
    } else {
      return false;
    }
  }


  public function es_inexistente() {
    if (strcasecmp($this->codigoError, SUIP_SUSCRIPTOR_INEXISTENTE) == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function get_zonas_provincias_list() {
  $zones = array();
  $provinces = array();
  if (!empty($this->zonasList)) {
    foreach ($this->zonasList as $value) {
      $zones[] = SUFIJO_ZONA . $value;
    }
  }
  if (!empty($this->provinciasList)) {
    foreach ($this->provinciasList as $value) {
      $provinces[] = SUFIJO_PROV . $value;
    }
  }

  return array_merge($zones, $provinces);
  }

  public function get_rubros_list() {
  $rubros = array();
  foreach ($this->rubrosList as $value) {
    $rubros[] = $value;
  }
  return $rubros;
  }

  public function __toString()
  {
    $rubrosString = "";
  if (!empty($this->rubrosList)) {
    $rubrosString = implode(',', $this->rubrosList);
  }
    $zonasString = "";
  if (!empty($this->zonasList)) {
    $zonasString = implode(',', $this->zonasList);
  }
  $provinciasString = "";
  if (!empty($this->provinciasList)) {
    $provinciasString = implode(',', $this->provinciasList);
  }

    return " tipoDeDocumento=". $this->tipoDeDocumento .
       "| numeroDeDocumento=" . $this->numeroDeDocumento .
       "| nombre=" . $this->nombre .
       "| apellido=" . $this->apellido .
       "| sexo=" . $this->sexo .
       "| codigoDeArea=" . $this->codigoDeArea .
       "| codigoDeOperador=" . $this->codigoDeOperador .
       "| fechaNacimiento=" . $this->fechaNacimiento .
       "| email=" . $this->email .
       "| numeroDeCelular=" . $this->numeroDeCelular .
       "| numeroDeCliente=" . $this->numeroDeCliente .
       "| estadoSuscripcion=" . $this->estadoSuscripcion .
       "| rubrosList=" . $rubrosString .
     "| zonasList=" . $zonasString .
       "| provinciasList=" . $provinciasString;
  }

}
