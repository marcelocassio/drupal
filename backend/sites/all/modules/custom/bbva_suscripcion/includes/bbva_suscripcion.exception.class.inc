<?php

class SuscripcionException extends Exception {

}

class DniDuplicadoSuscripcionException extends SuscripcionException {

}

class UsuarioInexistenteSuscripcionException extends SuscripcionException {

}

