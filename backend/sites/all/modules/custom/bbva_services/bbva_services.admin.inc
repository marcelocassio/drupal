<?php

/**
 * Admin settings page.
 */
function bbva_services_config_services($form, &$form_state) {

  $form['group-fnet'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuraciones para los servicios de FNet.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['group-fnet']['bbva_services_fnet_timeout'] = array(
    '#title' => t('Timeout de conexión'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_fnet_timeout'),
  );
  $form['group-fnet']['bbva_services_fnet_host'] = array(
    '#title' => t('Host'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_fnet_host'),
  );

  $form['group-suip'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuraciones para los servicios de SUIP.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['group-suip']['bbva_services_suip_timeout'] = array(
    '#title' => t('Timeout de conexión'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_suip_timeout'),
  );
  $form['group-suip']['bbva_services_suip_host'] = array(
    '#title' => t('Host'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_suip_host'),
  );
  $form['group-fmovil'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuraciones para los servicios de FMOVIL.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['group-fmovil']['bbva_services_fmovil_timeout'] = array(
    '#title' => t('Timeout de conexión'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_fmovil_timeout'),
  );
  $form['group-fmovil']['bbva_services_fmovil_host'] = array(
    '#title' => t('Host'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_fmovil_host'),
  );

  $form['group-bbvapaquete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuraciones para los servicios de ServicioIncial.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['group-bbvapaquete']['bbva_services_servicioinicial_timeout'] = array(
    '#title' => t('Timeout de conexión'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_servicioinicial_timeout'),
  );
  $form['group-bbvapaquete']['bbva_services_servicioinicial_host'] = array(
    '#title' => t('Host'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_servicioinicial_host'),
  );

  $form['group-farmacity'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuraciones para los servicios de Farmacity.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['group-farmacity']['bbva_services_farmacity_host'] = array(
    '#title' => t('Host'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_farmacity_host'),
  );
  $form['group-farmacity']['bbva_services_farmacity_authorization'] = array(
    '#title' => t('Authorization'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_farmacity_authorization'),
  );
  $form['group-farmacity']['bbva_services_farmacity_proxyip'] = array(
    '#title' => t('Proxy'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_farmacity_proxyip'),
  );
  $form['group-farmacity']['bbva_services_farmacity_proxyport'] = array(
    '#title' => t('Puerto del proxy'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_farmacity_proxyport'),
  );

  $form['group-opinator'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuraciones para Opinator.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['group-opinator']['bbva_services_optinator_url'] = array(
    '#title' => t('Url Opinator'),
    '#type' => 'textfield',
    '#maxlength' => 150,
    '#size' => 150,
    '#default_value' => variable_get('bbva_services_optinator_url'),
  );

  $form['group-opinator']['bbva_services_optinator_saturation_url'] = array(
    '#title' => t('Url Opinator Saturation'),
    '#type' => 'textfield',
    '#maxlength' => 150,
    '#size' => 150,
    '#default_value' => variable_get('bbva_services_optinator_saturation_url'),
  );
  $form['group-opinator']['bbva_services_optinator_name'] = array(
    '#title' => t('Opinator Name'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_services_optinator_name'),
  );
  $form['group-opinator']['bbva_services_optinator_names'] = array(
    '#type' => 'textarea',
    '#title' => t('Lista de tipos/nombres/url'),
    '#default_value' => variable_get('bbva_services_optinator_names', ''),
    '#description' => t("Lista de tipos/nombres/url. Separados por \"::\". Uno por linea, ej.: opiname::OPI-FGO-Deslogueo::https://www.opinator.com"),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
