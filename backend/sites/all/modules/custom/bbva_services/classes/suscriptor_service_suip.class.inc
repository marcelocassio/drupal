<?php

define('SERVICE_SUIP_SUFIJO_PROV', 'PROV-');
define('SERVICE_SUIP_SUFIJO_ZONA', 'ZONA-');
define('SERVICE_SUIP_URI', '/francesGo2-Middleware/services/Suscripciones');
define('SERVICE_SUIP_URI_PORTAL', '/francesGo2-Middleware/services/WSSuscripcionGoPortalService');

class SuscriptorServiceSuip {

  private static $instancia;
  private $suipConn;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
    $this->suipConn = new \Services\BBVA\Suip();
  }

  public function modificarSuscripcion($data) {

    $data = (array) $data;
    $date = DateTime::createFromFormat('Ymd', $data['field_profile_year']['und'][0]['value'] . sprintf("%02d", $data['field_profile_month']['und'][0]['value']) . sprintf("%02d", $data['field_profile_day']['und'][0]['value']));

    $tipo_documento_altamira = bbva_convert_tipo_doc_altamira(
      $data['field_profile_document_type']['und'][0]['value'],
      $data['field_profile_document_number']['und'][0]['value'],
      $data['field_profile_sex']['und'][0]['value']
    );

    $suip_data = array(
     "bac:bac:updateSuscripcionForDrupal" => array(
       "bac:bac:numeroAltamira" => $data['name'],
       "bac:bac:nombres" => $data['field_profile_nombre']['und'][0]['value'],
       "bac:bac:apellidos" => $data['field_profile_apellido']['und'][0]['value'],
       "bac:bac:tipoDeDocumento" => $tipo_documento_altamira,
       "bac:bac:numeroDeDocumento" => $data['field_profile_document_number']['und'][0]['value'],
       "bac:bac:codigoDeOperador" => $data['field_profile_compania']['und'][0]['value'],
       "bac:bac:codigoDeArea" => $data['field_profile_area']['und'][0]['value'],
       "bac:bac:numeroDeCelular" => $data['field_profile_celular']['und'][0]['value'],
       "bac:bac:canal" => 'WBPC',
       "bac:bac:requiereConfirmacion" => 'N',
       "bac:bac:mail" => $data['mail'],
       "bac:bac:aceptaNotificacionesPush" => 'true',
       "bac:bac:fechaNacimiento" => $date ? $date->format('d/m/Y') : null,
       "bac:bac:sexo" => strtoupper($data['field_profile_sex']['und'][0]['value']),
      )
    );
    
    $this->completarZonasRubros($suip_data["bac:bac:updateSuscripcionForDrupal"], $data);
    $this->suipConn->setUri(SERVICE_SUIP_URI);
    $this->suipConn->setData($suip_data); // un array
    $respuesta_suip = $this->suipConn->request(); // un object

    $suscripcionResponse = new SuscripcionBo();
    $retorno = $respuesta_suip->data->soapenv_Body->updateSuscripcionForDrupalResponse->updateSuscripcionForDrupalReturn;
    $transaccion_procesada = $retorno->transaccionProcesada;
    
    if (strcasecmp($transaccion_procesada, 'true') == 0) {
      $suscripcionResponse->transaccionProcesada = true;
    } 
    else {
      $suscripcionResponse->transaccionProcesada = false;
      $suscripcionResponse->codigoError = $retorno->codigoError->__toString();
    }

    return $suscripcionResponse;

  }

  public function suscribir($data) {

    $data = (array) $data;
    $date = DateTime::createFromFormat('Ymd', $data['field_profile_year']['und'][0]['value'] . sprintf("%02d", $data['field_profile_month']['und'][0]['value']) . sprintf("%02d", $data['field_profile_day']['und'][0]['value']));

    $tipo_documento_altamira = bbva_convert_tipo_doc_altamira($data['field_profile_document_type']['und'][0]['value'], $data['field_profile_document_number']['und'][0]['value'], $data['field_profile_sex']['und'][0]['value']);

    $suip_data = array(
     "bac:bac:addSuscripcionForDrupal" => array(
       "bac:bac:numeroAltamira" => $data['name'],
       "bac:bac:nombres" => $data['field_profile_nombre']['und'][0]['value'],
       "bac:bac:apellidos" => $data['field_profile_apellido']['und'][0]['value'],
       "bac:bac:tipoDeDocumento" => $tipo_documento_altamira,
       "bac:bac:numeroDeDocumento" => $data['field_profile_document_number']['und'][0]['value'],
       "bac:bac:codigoDeOperador" => $data['field_profile_compania']['und'][0]['value'],
       "bac:bac:codigoDeArea" => $data['field_profile_area']['und'][0]['value'],
       "bac:bac:numeroDeCelular" => $data['field_profile_celular']['und'][0]['value'],
       "bac:bac:canal" => $data['canal'],
       "bac:bac:requiereConfirmacion" => 'N',
       "bac:bac:mail" => $data['mail'],
       "bac:bac:aceptaNotificacionesPush" => 'true',
       "bac:bac:fechaNacimiento" => $date ? $date->format('d/m/Y') : null,
       "bac:bac:sexo" => strtoupper($data['field_profile_sex']['und'][0]['value']),
      )
    );
    
    $this->completarZonasRubros($suip_data["bac:bac:addSuscripcionForDrupal"], $data);
    $this->suipConn->setUri(SERVICE_SUIP_URI);
    $this->suipConn->setData($suip_data); // un array
    $respuesta_suip = $this->suipConn->request(); // un object
    $suscripcionResponse = new SuscripcionBo();
    $retorno = $respuesta_suip->data->soapenv_Body->addSuscripcionForDrupalResponse->addSuscripcionForDrupalReturn;

    $suscripcionResponse->transaccionProcesada = strcasecmp($retorno->transaccionProcesada, 'true') == 0;

    if(!$suscripcionResponse->transaccionProcesada){
      $suscripcionResponse->codigoError = $retorno->codigoError->__toString();
    }

    return $suscripcionResponse;
  }

  private function completarZonasRubros(&$suip_data_oper, &$data) {
    $indice = 0;

    if(isset($data['rubros'])){
      foreach ($data['rubros'] as $value) {

        if($value == 165){
          $value = 170;
        }
     
        $indice += 1;
        $key = "bac:bac:preferencias@".$indice;
        $campos = array(
          "urn:urn:codigo" => $value,
          "urn:urn:tipo" => 'R',
        );
        $suip_data_oper[$key] = $campos;
      }
    }

    if(isset($data['zonas'])){
      foreach ($data['zonas'] as $value) {
        $indice += 1;
        $key = "bac:bac:preferencias@".$indice;
        if (strpos($value, SERVICE_SUIP_SUFIJO_ZONA) !== false) {
          $id = str_replace(SERVICE_SUIP_SUFIJO_ZONA,'',$value);
          $tipo = "Z";
        } else if (strpos($value, SERVICE_SUIP_SUFIJO_PROV) !== false) {
          $id = str_replace(SERVICE_SUIP_SUFIJO_PROV,'',$value);
          $tipo = "P";
        }

        $campos = array(
          "urn:urn:codigo" => $id,
          "urn:urn:tipo" => $tipo,
        );

        $suip_data_oper[$key] = $campos;
      }
    }
  }

  public function consultarSuscripcionByNumeroCliente($numeroCliente) {
    $suip_data = array(
     "bac:bac:getSuscripcionByDniSexoYtipoDniOAltamira" => array(
       "bac:bac:numeroAltamira" => $numeroCliente,
      )
    );

    $this->suipConn->setUri(SERVICE_SUIP_URI);
    $this->suipConn->setData($suip_data); // un array
    $respuesta_suip = $this->suipConn->request(); // un object
    $suip_procesa = isset($respuesta_suip->data->soapenv_Body);

    if($suip_procesa){
      $transaccion_procesada = $respuesta_suip->data->soapenv_Body->getSuscripcionByDniSexoYtipoDniOAltamiraResponse->getSuscripcionByDniSexoYtipoDniOAltamiraReturn->transaccionProcesada->__toString();
    }

    $suscripcionResponse = new  SuscripcionBo();
    if ($suip_procesa && strcasecmp($transaccion_procesada, 'true') == 0) {
      $suscripcionResponse->transaccionProcesada = true;
      $datos_suscripcion = $respuesta_suip->data->soapenv_Body->getSuscripcionByDniSexoYtipoDniOAltamiraResponse->getSuscripcionByDniSexoYtipoDniOAltamiraReturn->suscripcion;

      $suscripcionResponse->tipoDeDocumento = $datos_suscripcion->tipoDeDocumento->__toString();
      $suscripcionResponse->numeroDeDocumento = $datos_suscripcion->numeroDeDocumento->__toString();

      $suscripcionResponse->nombre = $datos_suscripcion->nombres->__toString();
      $suscripcionResponse->apellido = $datos_suscripcion->apellidos->__toString();
      $suscripcionResponse->sexo = $datos_suscripcion->sexo->__toString();
      $suscripcionResponse->codigoDeArea = $datos_suscripcion->codigoDeArea->__toString();
      $suscripcionResponse->codigoDeOperador = $datos_suscripcion->codigoDeOperador->__toString();
      $suscripcionResponse->estadoSuscripcion = $datos_suscripcion->estadoSuscripcion->__toString();
      $suscripcionResponse->fechaNacimiento = $datos_suscripcion->fechaNacimiento->__toString();
      $suscripcionResponse->email = $datos_suscripcion->mail->__toString();
      $suscripcionResponse->numeroDeCelular = $datos_suscripcion->numeroDeCelular->__toString();
      $suscripcionResponse->numeroDeCliente = $datos_suscripcion->numeroDeCliente->__toString();
      $suscripcionResponse->rubrosList = array();
      $suscripcionResponse->provinciasList = array();
      $suscripcionResponse->zonasList = array();
      foreach ($datos_suscripcion->preferencias->preferencias as $preferencia) {
        $codigo = $preferencia->codigo->__toString();
        $tipo = $preferencia->tipo->__toString();
        if (strcmp($tipo, 'R') === 0) {
          $suscripcionResponse->rubrosList[] = $codigo;
        } else if (strcmp($tipo, 'P') === 0){
          $suscripcionResponse->provinciasList[] = SERVICE_SUIP_SUFIJO_PROV . $codigo;
        } else if (strcmp($tipo, 'Z') === 0){
          $suscripcionResponse->zonasList[] = SERVICE_SUIP_SUFIJO_ZONA . $codigo;
        }
      }

    } else {
      $suscripcionResponse->transaccionProcesada = false;
      if($suip_procesa){
        $suscripcionResponse->codigoError = $respuesta_suip->data->soapenv_Body->getSuscripcionByDniSexoYtipoDniOAltamiraResponse->getSuscripcionByDniSexoYtipoDniOAltamiraReturn->codigoError->__toString();
      }
    }

    return $suscripcionResponse;

  }


  public function consultarSuscripcion($tipo_documento_short, $numero_documento) {
    $tipo_documento_altamira = bbva_get_altamira_code($tipo_documento_short);
    $suip_data = array(
     "bac:bac:getSuscripcionByDniSexoYtipoDni" => array(
       "bac:bac:tipoDocumento" => $tipo_documento_altamira,
       "bac:bac:numeroDocumento" => $numero_documento,
      )
    );

    $this->suipConn->setUri(SERVICE_SUIP_URI);
    $this->suipConn->setData($suip_data); // un array
    $respuesta_suip = $this->suipConn->request(); // un object

    $transaccion_procesada = $respuesta_suip->data->soapenv_Body->getSuscripcionByDniSexoYtipoDniResponse->getSuscripcionByDniSexoYtipoDniReturn->transaccionProcesada->__toString();

    $suscripcionResponse = new  SuscripcionBo();
    if (strcasecmp($transaccion_procesada, 'true') == 0) {
      $suscripcionResponse->transaccionProcesada = true;
      $datos_suscripcion = $respuesta_suip->data->soapenv_Body->getSuscripcionByDniSexoYtipoDniResponse->getSuscripcionByDniSexoYtipoDniReturn->suscripcion;

      $suscripcionResponse->tipoDeDocumento = $datos_suscripcion->tipoDeDocumento->__toString();
      $suscripcionResponse->numeroDeDocumento = $datos_suscripcion->numeroDeDocumento->__toString();

      $suscripcionResponse->nombre = $datos_suscripcion->nombres->__toString();
      $suscripcionResponse->apellido = $datos_suscripcion->apellidos->__toString();
      $suscripcionResponse->sexo = $datos_suscripcion->sexo->__toString();
      $suscripcionResponse->codigoDeArea = $datos_suscripcion->codigoDeArea->__toString();
      $suscripcionResponse->codigoDeOperador = $datos_suscripcion->codigoDeOperador->__toString();
      $suscripcionResponse->estadoSuscripcion = $datos_suscripcion->estadoSuscripcion->__toString();
      $suscripcionResponse->fechaNacimiento = $datos_suscripcion->fechaNacimiento->__toString();
      $suscripcionResponse->email = $datos_suscripcion->mail->__toString();
      $suscripcionResponse->numeroDeCelular = $datos_suscripcion->numeroDeCelular->__toString();
      $suscripcionResponse->numeroDeCliente = $datos_suscripcion->numeroDeCliente->__toString();
      $suscripcionResponse->rubrosList = array();
      $suscripcionResponse->provinciasList = array();
      $suscripcionResponse->zonasList = array();
      foreach ($datos_suscripcion->preferencias->preferencias as $preferencia) {
        $codigo = $preferencia->codigo->__toString();
        $tipo = $preferencia->tipo->__toString();
        if (strcmp($tipo, 'R') === 0) {
          $suscripcionResponse->rubrosList[] = $codigo;
        } else if (strcmp($tipo, 'P') === 0){
          $suscripcionResponse->provinciasList[] = SERVICE_SUIP_SUFIJO_PROV . $codigo;
        } else if (strcmp($tipo, 'Z') === 0){
          $suscripcionResponse->zonasList[] = SERVICE_SUIP_SUFIJO_ZONA . $codigo;
        }
      }
    } else {
      $suscripcionResponse->transaccionProcesada = false;
      $suscripcionResponse->codigoError = $respuesta_suip->data->soapenv_Body->getSuscripcionByDniSexoYtipoDniResponse->getSuscripcionByDniSexoYtipoDniReturn->codigoError->__toString();
    }

    return $suscripcionResponse;
  }

  public function consultarSuscripcionByTelefono($codigo_area, $numero_celular) {
    $suip_data = array(
     "sus:processCRUD" => array(
       "sus:inputBean" => array(
          'bean:operacion' =>  'C',
          'urn1:suscripcion' =>  array(
            'urn2:codigoArea' => $codigo_area,
            'urn2:numero' => $numero_celular,
            'urn2:requiereConfirmacion' => 'false'
          )
        )
      )
    );

    $suip = new \Services\BBVA\Suip();
    $suip->setUri(SERVICE_SUIP_URI_PORTAL);
    $suip->setData($suip_data); // un array
    try {
      $respuesta_suip = $suip->request(); // un object

      if (isset($respuesta_suip->data->soapenv_Body->processCRUDResponse->processCRUDReturn->transaccionProcesada)){
        $transaccion_procesada = (string)$respuesta_suip->data->soapenv_Body->processCRUDResponse->processCRUDReturn->transaccionProcesada;
      }

      $suscripcionResponse = new  SuscripcionBo();
      if (strcasecmp($transaccion_procesada, 'true') == 0) {
        $suscripcionResponse->transaccionProcesada = true;
        $datos_suscripcion = $respuesta_suip->data->soapenv_Body->processCRUDResponse->processCRUDReturn->suscripcion;
        $suscripcionResponse->estadoSuscripcion = $datos_suscripcion->estadoSuscripcion->__toString();
        $suscripcionResponse->nombre = $datos_suscripcion->nombres->__toString();
        $suscripcionResponse->apellido = $datos_suscripcion->apellidos->__toString();
        $suscripcionResponse->tipoDeDocumento = $datos_suscripcion->codigoAltamira->__toString();
        $suscripcionResponse->numeroDeDocumento = $datos_suscripcion->numeroDocumento->__toString();
        $suscripcionResponse->numeroDeCliente = $datos_suscripcion->numeroAltamira->__toString();
      } else {
        $suscripcionResponse->transaccionProcesada = false;
        if (isset($respuesta_suip->data->soapenv_Body->processCRUDResponse->processCRUDReturn->codigoError)){
          $suscripcionResponse->codigoError = $respuesta_suip->data->soapenv_Body->processCRUDResponse->processCRUDReturn->codigoError->__toString();
        }else{
          $suscripcionResponse->codigoError = 99;
        }
      }

      return $suscripcionResponse;
    } catch (Exception $e) {
      if(module_exists('bbva_logs')) {
        $log = \BBVA\Log::getInstance();
        $log->insertar("consultarSuscripcionByTelefono", $e->getMessage());
      }
      $suscripcionResponse = new  SuscripcionBo();
      $suscripcionResponse->transaccionProcesada = false;
      $suscripcionResponse->codigoError = 99;

      return $suscripcionResponse;
    }
  }

  public function marcarDescargoApp($numeroAltamira, $marca_descargo_app) {
    $suip_data = array(
     "bac:bac:marcarDescargoApp" => array(
       "bac:bac:numeroAltamira" => $numeroAltamira,
       "bac:bac:marcaDescargoApp" => $marca_descargo_app,
       "bac:bac:aceptaNotificacionesPush" => $marca_descargo_app,
      )
    );

    $this->suipConn->setUri(SERVICE_SUIP_URI);
    $this->suipConn->setData($suip_data); // un array
    $respuesta_suip = $this->suipConn->request(); // un object

    return $respuesta_suip;
  }

  public function bajaSuscripcion($numeroAltamira) {
    $suip_data = array(
     "bac:bac:deleteSuscripcion" => array(
       "bac:bac:numeroAltamira" => $numeroAltamira,
       "bac:bac:requiereConfirmacion" => "N",
      )
    );

    $this->suipConn->setUri(SERVICE_SUIP_URI);
    $this->suipConn->setData($suip_data); // un array
    $respuesta_suip = $this->suipConn->request(); // un object

    return $respuesta_suip;
  }
}
