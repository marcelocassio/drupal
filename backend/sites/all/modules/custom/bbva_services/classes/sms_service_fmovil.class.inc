<?php


class SmsServiceFmovil {


  public function enviarSMS($codigo_operador, $numero_celular, $mensaje) {
    $sms_send_data = array(
     "urn:enviarSMS" => array(
       "idMensajeOrigen" => 0,
       "codigoDeOperador" => $codigo_operador,
       "numeroDeCelular" => $numero_celular,
       "mensaje" => $mensaje,
       "IndivOEmp" => 'I',
      )
    );

    return $this->ejecutar($sms_send_data);
  }



  private function ejecutar($data) {

    $fmovil = new \Services\BBVA\FMovil();
    $fmovil->setUri("/fmovil/services/EnvioSMS");
    $fmovil->setData($data);
    $response = $fmovil->request();

    $datos_usuario =  null;
    if ($response->code == "200") {
      $datos = $response->data;

      if ($datos->soapenv_Body->multiRef->codigo != "0") {
        throw new Exception($datos->soapenv_Body->multiRef->descripcion);
      }

    } else {
      throw new Exception("Hubo un error en la comunicacion con FMOVIL | Mensaje"  . $response->status_message . ": HTTP CODE=" . $response->code);
    }
    return $datos;
  }

}
