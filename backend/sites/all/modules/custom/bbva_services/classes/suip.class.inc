<?php

namespace Services\BBVA;

require_once 'base_services.class.inc';

class Suip extends BaseServices {

  private $xml;

  function __construct() {
    $this->host = variable_get('bbva_services_suip_host', FALSE);
    $options = array(
      'method' => 'POST',
      'timeout' => variable_get('bbva_services_suip_timeout', 10),
      'headers' => array(
        'Content-Type' => 'text/xml;charset=UTF-8',
        'Accept-Encoding' => 'gzip,deflate',
        'SOAPAction' => '',
        'Content-Length' => 0,

      ),
    );

    parent::__construct($options);
  }

  protected function _prepare() {
    $options = $this->options;
    $body = $this->xml->addChild("soapenv:soapenv:Body");
    $this->_completarXML($this->data, $body);
    $options['data'] = $this->xml->asXML();

    $options['headers']['Content-Length'] = strlen($options['data']);
    return $options;
  }

  public function getResponse() {
    $response = $this->response;
    // Si es un xml lo convierto

    if ($response->code != 200) {
      if(module_exists('bbva_logs')) {
        $data_log = array(
          "code" => $response->code,
          "error" => $response->error,
          "data" => $this->data
        );
        $log = \BBVA\Log::getInstance();
        $log->insertar("suip_error", $data_log);
      }
      throw new SuipException($response->error, $response->code);
    }

    $data_tmp = @new \SimpleXMLElement(str_replace(":", "_", $response->data));
    if(is_object($data_tmp)) {
      $response->data = $data_tmp;
    }

    return $response;
  }


  private function _completarXML($array = array(), &$xml) {
    foreach($array as $key => $value) {
      if(is_array($value)) {
        if(!is_numeric($key)){

          $pos = strrpos($key, "@");
          if ($pos === false) {
            $subnode = $xml->addChild("$key");
          } else {
            $key_new = substr($key, 0, $pos);
            $subnode = $xml->addChild("$key_new");
          }
          $this->_completarXML($value, $subnode);

        } else {
          $this->_completarXML($value);
        }
      } else {
        $xml->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }


  public function setData($data = array()) {

    $this->xml = @new \SimpleXMLElement("<soapenv:soapenv:Envelope></soapenv:soapenv:Envelope>");
    $this->xml->addAttribute( 'xmlns:xmlns:soapenv', "http://schemas.xmlsoap.org/soap/envelope/" );
    $this->xml->addAttribute( 'xmlns:xmlns:urn', "urn:ar.com.bbva.ws.beans.backend.zonarubro" );
    $this->xml->addAttribute( 'xmlns:xmlns:bac', "http://backend.facade.ws.bbva.com.ar" );
    $this->xml->addAttribute( 'xmlns:xmlns:crud', "http://crud.facade.ws.bbva.com.ar" );
    $this->xml->addChild('soapenv:soapenv:Header');

    $this->data = $data;
    return $this;
  }
}

class SuipException extends \Exception {

}

