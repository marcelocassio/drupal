<?php
// Debe ser de 8 digitos, sin puntos
define('NOSIS_CDA', '29165001');

class ClienteServiceFnet {

  private static $instancia;

  private $fnet;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
    $this->fnet = new \Services\BBVA\Fnet();
  }

  public function validarClaveDigital($document_type, $document_number, $digital_user, $digital_pass) {

    $fnet_request_data = array('tipodoc'=>$document_type,
      'nrodoc'=>$document_number,
      'clave'=>$digital_pass,
      'usuario'=>$digital_user);

    $this->fnet->setUri("/fnet/mod/fgo/login.do");
    return $this->ejecutar($fnet_request_data);
  }

  public function consultarCelulares($numero_cliente) {

    $fnet_request_data = array(
      'nrocliente'=>$numero_cliente,
      'method'=>'consultarCelulares');
    $this->fnet->setUri("/fnet/mod/fgo/NL-cliente.do");
    return $this->ejecutar($fnet_request_data);
  }

  public function consultarNoCliente($document_type, $document_number) {

    $fnet_request_data = array(
      'tipodoc'=>$document_type,
      'nrodoc'=>$document_number,
      'method'=>'consultarNoCliente');
    $this->fnet->setUri("/fnet/mod/fgo/NL-cliente.do");

    return $this->ejecutar($fnet_request_data);
  }

  public function crearNoCliente($data) {

    $data = (array) $data;
    
    $date = DateTime::createFromFormat('Ymd', $data['field_profile_year']['und'][0]['value'] . $data['field_profile_month']['und'][0]['value'] . $data['field_profile_day']['und'][0]['value']);
    $nacimiento_formateado = $date->format('dmY');


    if($data['field_profile_document_number']['und'][0]['value'] < 90000000) {
      $nacionalidad = "080";
    } else {
      $nacionalidad = $data["nacionalidad"];
    }

    $tipo_documento_short = bbva_validate_tipo_doc($data['field_profile_document_type']['und'][0]['value'],
    $data['field_profile_document_number']['und'][0]['value'],
    $data['field_profile_sex']['und'][0]['value']);

    $input = array(
      'method'=>'crearNoCliente',
      'tipodoc'=>$tipo_documento_short,
      'nrodoc'=>$data['field_profile_document_number']['und'][0]['value'],
      'nombre'=>bbva_subscription_quitar_tildes($data['field_profile_nombre']['und'][0]['value']),
      'apellido'=>bbva_subscription_quitar_tildes($data['field_profile_apellido']['und'][0]['value']),
      'sexo'=>$data['field_profile_sex']['und'][0]['value'],
      'fechaNacimiento'=>$nacimiento_formateado,
      'mail'=>$data['mail'],
      'prefijoCelular'=>$data['field_profile_area']['und'][0]['value'],
      'caracterCelular'=>substr($data['field_profile_celular']['und'][0]['value'], 0, 4),
      'numeroCelular'=>substr($data['field_profile_celular']['und'][0]['value'], 4),
      'codNacionalidad'=>$nacionalidad,
    );

    $this->fnet->setUri("/fnet/mod/fgo/NL-cliente.do");
    return $this->ejecutar($input);
  }

  public function consultarDatosPersonales($numero_cliente) {
    $fnet_request_data = array(
      'nrocliente'=>$numero_cliente,
      'method'=>'consultarDatosPersonales'
    );
    $this->fnet->setUri("/fnet/mod/fgo/NL-cliente.do");
    return $this->ejecutar($fnet_request_data);
  }

  public function consultarNOSIS($doc_cuit) {
    $fnet_request_data = array(
      'method' => 'consultarNOSIS',
      'cda' => NOSIS_CDA,
      'nroDoc' => (int) $doc_cuit
    );
    $this->fnet->setUri("/fnet/mod/bbva/NL-VentaPPHip.do");
    return $this->ejecutar($fnet_request_data);
  }

  private function ejecutar($data) {

    $this->fnet->setData($data);
    $response = $this->fnet->request();

    $datos_usuario =  null;
    if ($response->code == "200") {
      $datos_usuario = $response->data;
    } else {

      $mensaje = isset($response->status_message) ? $response->status_message : " ";
      if(isset($response->error)){
          $mensaje .= " ". $response->error;
      }
      
      throw new Exception("Hubo un error en la comunicacion con FNET | Mensaje"  .$mensaje. ": HTTP CODE=" . $response->code);
    }
    return $datos_usuario;
  }
}
