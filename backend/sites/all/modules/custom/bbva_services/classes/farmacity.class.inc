<?php

namespace Services\BBVA;

class Farmacity {
  private static $instancia;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
  }

  public function obtenerVouchers() {
    $vouchers = array();

    try {
      $url = variable_get('bbva_services_farmacity_host');
      $ch = curl_init();

      $proxy_ip = variable_get('bbva_services_farmacity_proxyip');
      $proxy_port = variable_get('bbva_services_farmacity_proxyport');

      $CURL_OPTS = array(
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_URL => $url,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_PROXY  => $proxy_ip,
        CURLOPT_PROXYPORT  => $proxy_port,
        CURLOPT_HTTPHEADER => array("Accept: application/json", "Authorization: " . variable_get('bbva_services_farmacity_authorization')),
        CURLOPT_POST => false,
        CURLOPT_VERBOSE => true,
      );

      curl_setopt_array($ch, $CURL_OPTS);

      $result = curl_exec($ch);
      @$result = json_decode($result, true);

      if(isset($result['Codigos']['Codigo'])) {
        foreach ($result['Codigos']['Codigo'] as $codigo) {
          $vouchers[] = array('code' => $codigo['CodigoUnico'], 'title' => $codigo['CodigoUnico']);
        }
      }

      if(curl_error($ch)){
        if(module_exists('bbva_logs')) {
          $data_log = array(
            "error" => curl_error($ch)
          );
          $log = \BBVA\Log::getInstance();
          $log->insertar("farmacity_error", $data_log);
        }
      }

      curl_close($ch);
    } catch (Exception $e) {}

    return $vouchers;
  }

 }
