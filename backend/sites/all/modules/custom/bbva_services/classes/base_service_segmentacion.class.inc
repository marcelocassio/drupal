<?php

namespace Services\BBVA;


define('BBVA_SEGMENTACION_EMPRESA', 'BBVA');//
define('BBVA_SEGMENTACION_APLICACION_FGO', 'FGO');//
define('BBVA_SEGMENTACION_TIPOMENSAJE', 'E');//
define('BBVA_SEGMENTACION_MODULO', 'SEGMENTACION');//
define('BBVA_SEGMENTACION_REQUERIMIENTO', 'OBTENER_SEGMENTO_USUARIO');//
define('BBVA_SEGMENTACION_OPCION', '01');//
define('BBVA_SEGMENTACION_SUCURSAL', '0099');//
define('BBVA_SEGMENTACION_REQUERIMIENTO_LOGON', 'LOGON_ALTAMIRA');//
define('BBVA_SEGMENTACION_RESPONSE_LOGON_OK', 'OK');//


require_once 'base_services.class.inc';

class BaseServiceSegmentacion extends BaseServices {

  public $xml;
  public $response;

  function __construct() {
    $this->host = variable_get('bbva_services_segmentacion_servicioinicial_host', FALSE);
    $this->schema = variable_get('bbva_services_segmentacion_servicioinicial_schema', "http");
    $options = array(
      'method' => 'POST',
      'timeout' => variable_get('bbva_services_segmentacion_servicioinicial_timeout', 10),
      'headers' => array(
        'Content-Type' => 'text/xml;charset=UTF-8',
        'Accept-Encoding' => 'gzip,deflate',
        'SOAPAction' => '',
        'Content-Length' => 0,
      ),
    );

    parent::__construct($options);
  }

  protected function _prepare() {
    $options = $this->options;
    $body = $this->xml->addChild("Datos");
    $this->_completarXML($this->data, $body);
    $options['data'] = $this->xml->asXML();
    $options['headers']['Content-Length'] = strlen($options['data']);
    return $options;
  }

  public function request() {
    $options = $this->_prepare();
    $url = $this->schema."://".$this->host . $this->uri;
    $this->response = drupal_http_request($url, $options);
   
    return $this->getResponse();
  }
  
  public function getResponse() {
    //$this->response = $this->getFakeResponse();
    $response = $this->response;
    
    log_seg_online("Request del serv Segm online", json_encode($this->data));           
    log_seg_online("Response del serv Segm online", json_encode($response));
    // Si es un xml lo convierto

    if ($response->code == 200) {
       $data_tmp = new \SimpleXMLElement(str_replace(":", "_", $response->data));
        if(is_object($data_tmp)) {
          $response->data = $data_tmp;
        }
        return $response;
    }
    
    if(module_exists('bbva_logs')) {
      $data_log = array(
        "code" => $response->code,
        "error" => $response->error,
        "data" => $this->data
      );
      $log = \BBVA\Log::getInstance();
      $log->insertar("bbva_paquete_error", $data_log);
    }
    return false;
    
 
  }

  private function _completarXML($array = array(), &$xml) {
    foreach($array as $key => $value) {
      if(is_array($value)) {
         watchdog('xml', ' Key data: %data ', array('%data' => $key, ));
        if(!is_numeric($key)){

          $subnode = $xml->addChild("$key");
          $this->_completarXML($value, $subnode);
        } else {
          $this->_completarXML($value);
        }
      } else {
        $xml->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }


  public function setData($data = array()) {
    $this->xml = new \SimpleXMLElement("<BbvaPaquete></BbvaPaquete>");
    $header = $this->xml->addChild('Header');
    $header->addChild("Empresa", BBVA_SEGMENTACION_EMPRESA);//
    $header->addChild("Aplicacion", BBVA_SEGMENTACION_APLICACION_FGO);//
    $header->addChild("Modulo", BBVA_SEGMENTACION_MODULO);//
    $header->addChild("TipoMensaje", BBVA_SEGMENTACION_TIPOMENSAJE);
    $header->addChild("Requerimiento", BBVA_SEGMENTACION_REQUERIMIENTO);
    
    $this->data = $data;
    return $this;
  }
  
  
  public function login() {      
    $this->xml = new \SimpleXMLElement("<BbvaPaquete></BbvaPaquete>");
    $header = $this->xml->addChild('Header');
    $header->addChild("Empresa", BBVA_SEGMENTACION_EMPRESA);//
    $header->addChild("Aplicacion", BBVA_SEGMENTACION_APLICACION_FGO);//
    $header->addChild("Modulo", BBVA_SEGMENTACION_MODULO);//
    $header->addChild("TipoMensaje", BBVA_SEGMENTACION_TIPOMENSAJE);
    $header->addChild("Requerimiento", BBVA_SEGMENTACION_REQUERIMIENTO_LOGON);
    $header->addChild("Sucursal", BBVA_SEGMENTACION_SUCURSAL);    
    $header->addChild("IdUsuario", variable_get("bbva_segmentacion_service_user", "ECGTEI2"));  
    
    $this->data = array(
        "usuario"=> variable_get("bbva_segmentacion_service_user", "ECGTEI2"),
        "clave"=> variable_get("bbva_segmentacion_service_pass", "eecc0920"),
    );
    $response = $this->request(); // un object
    
    if(isset($response->data->Respuesta->Resultado) && isset($response->data->Respuesta["Tipo"])){
        return ($response->data->Respuesta["Tipo"]->__toString() == BBVA_SEGMENTACION_RESPONSE_LOGON_OK);
        
    }
    return false;

  }
  
  protected function getFakeResponse() {
        $response = new \stdClass();
        $response->data = $this->responseFake();
        $response->code = 200;
        $response->protocol =  "HTTP/1.1";
        return $response;
     
  }
  
  protected function responseFake() {
      return '<BbvaPaquete>
                        <Header>
                                <Empresa>BBVA</Empresa>
                                <Aplicacion>FGO</Aplicacion>
                                <Modulo>SEGMENTACION</Modulo>
                                <IdSession>12223344556666</IdSession>
                                <IdProceso>122233445566662222</IdProceso>
                                <Requerimiento Undo="false">OBTENER_SEGMENTO_USUARIO</Requerimiento>
                                <FechaProc>16/12/2020</FechaProc>
                                <HoraProc>12:52:28</HoraProc>
                                <TipoMensaje>R</TipoMensaje>
                        </Header>
                        <Respuesta Tipo="OK">
                        <Resultado>
                                <negocio>82</negocio>
                                <isPrem>S</isPrem>
                                <segment>82200</segment>
                                <isLan>S</isLan>
                        </Resultado>
                    </Respuesta>
                </BbvaPaquete>';
  }
  
}


