<?php
namespace Services\BBVA;


class ComunicacionServiceSuip {

  private static $instancia;
  private $suipConn;
  const URI = "/francesGo2-Middleware/services/WSBeneficioDrupalBeanService";

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {

    $this->suipConn = new \Services\BBVA\Suip();
  }

  public function consultarNovedades() {
    $suip_data = array(
      'crud:findAll' => array(),
      );

    $this->suipConn->setUri(self::URI);
    $this->suipConn->setData($suip_data);
    $respuesta_suip = $this->suipConn->request();

    $jsonString = json_decode((string)$respuesta_suip->data->soapenv_Body->findAllResponse->findAllReturn->jsonString);
    $comunicaciones = array();
    if (isset($jsonString->BeneficioDrupal->result)) {
      $comunicaciones = $jsonString->BeneficioDrupal->result;
    }
    return $comunicaciones;
  }


}