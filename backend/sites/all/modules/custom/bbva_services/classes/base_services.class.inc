<?php

namespace Services\BBVA;

abstract class BaseServices {
  protected $host;
  protected $uri;
  protected $options = array();
  protected $data = array();
  protected $response;

  function __construct($options) {
    if(!$this->host) {
      throw new \Exception("Host no configurado", 1);
    }
    $this->setOptions($options);
  }

  abstract protected function _prepare();

  abstract public function getResponse();

  public function setUri($uri) {
    $this->uri = trim($uri);

    return $this;
  }

  public function setOptions($options = array()) {
    if(isset($options['data'])) {
      unset($options['data']);
    }
    $this->options = array_merge($this->options, $options);
    return $this;
  }

  public function setData($data = array()) {
    $this->data = array_merge($this->data, $data);
    return $this;
  }

  public function request() {
    $options = $this->_prepare();
    $url = $this->host . $this->uri;

    $this->response = drupal_http_request($url, $options);

    return $this->getResponse();
  }

}
