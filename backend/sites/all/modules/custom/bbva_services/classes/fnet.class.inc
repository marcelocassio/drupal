<?php
// 463

namespace Services\BBVA;

require_once 'base_services.class.inc';

class Fnet extends BaseServices {

  function __construct() {

    $this->host = variable_get('bbva_services_fnet_host', FALSE);
    $options = array(
      'method' => 'POST',
      'timeout' => variable_get('bbva_services_fnet_timeout', 10),
      'headers' => array(
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Accept' => 'application/json'
      ),
    );
    parent::__construct($options);
  }

  protected function _prepare() {
    $options = $this->options;
    $options['data'] = http_build_query($this->data, '', '&');
    return $options;
  }

  public function getResponse() {

    $response = $this->response;
    $responseData = isset($response->data) ? $response->data : null;

    // Si es un json lo convierto
    $data_tmp = json_decode(utf8_encode(trim($responseData)));
    if(json_last_error() == JSON_ERROR_NONE) {
      $response->data = $data_tmp;
    }
    if(isset($response->data->codigo) && $response->data->codigo > 0) {
      $response->error = $response->data->codigo;
    }

    return $response;
  }
}
