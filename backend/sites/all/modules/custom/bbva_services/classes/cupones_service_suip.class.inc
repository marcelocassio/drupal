<?php

class CuponesServiceSuip {

  private static $instancia;
  private $suipConn;
  const URI = "/francesGo2-Middleware/services/WSBancaDigitalService";

  public static function getInstance() {
    if (!self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
    $this->suipConn = new \Services\BBVA\Suip();
  }

  public function getPromoCuponAProcesarAlta() {
    $suip_data = array(
      'crud:crud:getPromoCuponAProcesarAlta' => array()
    );

    $this->suipConn->setUri(self::URI);
    $this->suipConn->setData($suip_data);
    $respuesta_suip = $this->suipConn->request();

    if(!isset($respuesta_suip->code) || $respuesta_suip->code != 200) {
      throw new \Exception("Hubo un error en suip (getPromoCuponAProcesarAlta)", 1);
    }

    $cupones = array();

    if(isset($respuesta_suip->data) && is_object($respuesta_suip->data)) {
      $tmp = json_decode(json_encode((array) $respuesta_suip->data), 1);
      $cupones = (array) $tmp['soapenv_Body']['getPromoCuponAProcesarAltaResponse']['getPromoCuponAProcesarAltaReturn'];
    }

    // Si hay un solo cupon lo meto en una array para normalizar
    if(count($cupones) > 0 && !array_key_exists('0', $cupones)){
      $cupones = array($cupones);
    }

    return $cupones;
  }

  public function confirmarAltaBancaDigital($idCupon, $idPromocion) {
    $suip_data = array(
      'crud:crud:confirmarAltaBancaDigital' => array(
        'crud:crud:idPromocion' => $idPromocion,
        'crud:crud:codigoPromoCupon' => $idCupon
      )
    );

    $this->suipConn->setUri(self::URI);
    $this->suipConn->setData($suip_data);
    $respuesta_suip = $this->suipConn->request();

    return isset($respuesta_suip->data->soapenv_Body->confirmarAltaBancaDigitalResponse);
  }
}
