<?php

namespace Services\BBVA;

require_once 'base_services.class.inc';

class GoShop extends BaseServices {

  function __construct() {
    $this->host = 'https://bbva-apistore-uat.aper.com';
    $options = array(
      'method' => 'POST',
      'timeout' => variable_get('bbva_services_fnet_timeout', 10),
      'headers' => array(
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Accept' => 'application/json'
      ),
    );
    parent::__construct($options);
  }

  protected function _prepare() {
    $options = $this->options;
    $options['data'] = http_build_query($this->data, '', '&');
    return $options;
  }

  public function getResponse() {

    $response = $this->response;
    
    if(isset($response->data)){
    // Si es un json lo convierto
      $data_tmp = json_decode(utf8_encode(trim($response->data)));
      if(json_last_error() == JSON_ERROR_NONE) {
        $response->data = $data_tmp;
      }
      if(isset($response->data->codigo) && $response->data->codigo > 0) {
        $response->error = $response->data->codigo;
      }
    }

    return $response;
  }

  public function enviarMail($datos, $enviar = false){

    $datosArray = array(
      "id_pedido" => $datos['id_pedido'],
      "dni_cliente" => $datos['dni_cliente'],
      "nombre" => $datos['nombre'],
      "apellido" => $datos['apellido'],
      "email" => $datos['email'],
      "referencia_pedido" => $datos['referencia_pedido'],
      "estado_pedido" => $datos['estado_pedido'],
      "cuotas" => $datos['cuotas'],
      "items" => $datos['items'],
      "total_financing" => $datos['total_financing'],
      "importe_envio" => $datos['importe_envio'],
      "importe_productos" => $datos['importe_productos'],
      "importe_total" => $datos['importe_total'],
      "metodo_entrega" => $datos['metodo_entrega'],
      "tiempo_entrega" => $datos['tiempo_entrega'],
      "extra_data" => $datos['extra_data'],        
      "seller" => $datos['seller'],
      "seller_email" => $datos['seller_email'],
      "seller_phone" => $datos['seller_phone'],
      "seller_address" => $datos['seller_address'],
      "seller_horario" => $datos['seller_horario'],
      "seller_image" => $datos['seller_image'],
      "medio_pago" => $datos['medio_pago'],
      "direccion_entrega" => json_decode($datos['direccion_entrega'], true),
      "direccion_facturacion" => json_decode($datos['direccion_facturacion'], true),
      "url_go_shop" => '#',//https://bbva-uat.aper.com/'
      "pathmailassets"=>"https://www.bbva.com.ar/fnetcore/assets/mail/"
    );

    if($enviar){
  
      hd('e-mail APER a enviar.');
      
      $email = $datos['email'];
      /** para test, se verificar si la variable existe y esta en true, envia al cliente, de lo contrario a los que estan **/
      if(!getMagic('bbva_magic_goshop_aper_mail_open')){
          $values = getMagic('bbva_magic_goshop_aper_mail_values');
          $email = ($values) ? $values : "carlosfederico.peralta@bbva.com,juanramon.breard@bbva.com";
      }

      $html_email_body = theme('goshop_aviso_compra', array('datos' => $datosArray));

      $params['body'] = $html_email_body;
      $params['subject'] = "Aviso de Compra en Go Shop";

      $result = drupal_mail('bbva_services', 'aviso_compra', $email, language_default(), $params);
      if($result && isset($result['send'])){
        hd('e-mail APER enviado.');  
        return true;
      }else{
        hd('e-mail APER NO enviado.');
      }
    }

    if(!isProd() || !$enviar){
      drupal_add_css(drupal_get_path('module', 'fgo_backend').'/css/aviso_compra_style.css');
      echo  theme('goshop_aviso_compra', array('datos' => $datosArray));
      return true;
    }
    return false;
  }

  public function processRequest($gsRequestObj){
    $items = array();
    foreach ($gsRequestObj['items'] as $item) {
      $items[] = array(
        "id_item" => $item['id'],
        "titulo_item" => $item['product_name'],
        "importe_base_item" => $item['price_base'],
        "price_final" => $item['price_final'],
        "product_quantity" => $item['product_quantity']
      );
    }

    $data = array(
      "id_pedido" => $gsRequestObj['reference'],
      "dni_cliente" => $gsRequestObj['address_delivery']['dni'],
      "nombre" => $gsRequestObj['customer']['firstname'],
      "apellido" => $gsRequestObj['customer']['lastname'],
      "email" => $gsRequestObj['customer']['email'],
      "referencia_pedido" => $gsRequestObj['reference'],
      "estado_pedido" => $gsRequestObj['state']['name'],
      "cuotas" => $gsRequestObj['payments'][0]['quotas'],
      "items" => $items,
      "total_financing" => $gsRequestObj['total_financing'],
      "importe_envio" => $gsRequestObj['total_shipping'],
      "importe_productos" => $gsRequestObj['total_products'],
      "importe_total" => $gsRequestObj['total_paid'],
      "metodo_entrega" => $gsRequestObj['carrier']['name'],
      "tiempo_entrega" => $gsRequestObj['carrier']['delay'],
      "extra_data" => isset($gsRequestObj['carrier']['extra_data'])?$gsRequestObj['carrier']['extra_data']:"", 
      "seller" => $gsRequestObj['payments'][0]['supplier']['name'],
      "seller_email" => $gsRequestObj['payments'][0]['supplier']['email_post_sale'],
      "seller_phone" => $gsRequestObj['payments'][0]['supplier']['phone'],
      "seller_address" => $gsRequestObj['payments'][0]['supplier']['address'],
      "seller_horario" => $gsRequestObj['payments'][0]['supplier']['description'],
      "seller_image" => $gsRequestObj['payments'][0]['supplier']['image'],
      "medio_pago" => $gsRequestObj['payments'][0]['first_digits']." ".$gsRequestObj['payments'][0]['last_digits']." ".$gsRequestObj['payments'][0]['card_brand'],
      "direccion_entrega" => json_encode($gsRequestObj['address_delivery']),
      "direccion_facturacion" => json_encode($gsRequestObj['address_invoice'])              
    );

    $goShopBo = new \Fgo\Bo\GoShopBo();
    $goShopBo->construirDesdeRegistro($data);
    $goShopBo->guardar();

    // agregar ,true para hacer el envio efectivo del email
    return $this->enviarMail($data, getMagic('bbva_magic_goshop_aper_mail'));
  }




}
