<?php

namespace Services\BBVA;


class SegmentacionService {

  private static $instancia;
  private $bbvaPaqueteConn;
  public $msgError;

  /**
   * 
   * @return SegmentacionService 
   */
  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
    $this->bbvaPaqueteConn = new  \Services\BBVA\BaseServiceSegmentacion();
    $this->bbvaPaqueteConn->setUri(SERVICIO_INICIAL_URI);
  }



  public function obtenerSegmentacionService($cliente) {

        $bbva_paquete_datos = array(
            "opcion" => "01",
            "numcli" => $cliente
        );
//        if(!$this->bbvaPaqueteConn->login()){
//            return false;
//        }
        
        
        $this->bbvaPaqueteConn->setData($bbva_paquete_datos);
        $respuesta = $this->bbvaPaqueteConn->request(); // un object
        
        if (isset($respuesta->data->Respuesta->Errores)) {
            $this->msgError = $respuesta->data->Respuesta->Errores->Error->__toString();
            hd($this->msgError);
            return false;
            
        } elseif (isset($respuesta->data->Respuesta->Resultado)) {
            $messageResponse = new SegmentacionResponse();
            $messageResponse->buildFromXML($respuesta->data);
            return $messageResponse;
            
        }
        if(isset($this->bbvaPaqueteConn->response->error)){
            $this->msgError = $this->bbvaPaqueteConn->response->error;
            hd($this->msgError);
            return false;
        }
        
        $this->msgError = "Error desconocido";
        return false;
    }

}


class SegmentacionException extends \Exception {

}



class SegmentacionResponse {
  public $codigoRetorno;
  public $mensaje;
  

  public function buildFromXML($xmlData) {
    $this->codigoRetorno = $xmlData->Respuesta->Resultado->negocio->__toString();
    $this->mensaje = $xmlData->Respuesta->Resultado;
    
    return $this;
  }
}
