<?php
define('PDM_REQ_CONSULTA_CUIT', 'SUIP_CONSULTA_CUIT_CUIL_CDI');

class PdmServiceSuip{

  private static $instancia;
  private $suipConn;
  const URI = "/francesGo2-Middleware/services/WSContribuyenteProactivoService";

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
    $this->suipConn = new \Services\BBVA\Suip();

    $this->bbvaPaqueteConn = new \Services\BBVA\BbvaPaquete();
    $this->bbvaPaqueteConn->setUri(SERVICIO_INICIAL_URI);
  }

  public function comercioAlta($datos) {
    $date = new Datetime();

    if (!empty($datos["es_cliente"])) {
      $es_cliente = 'true';
    }else{
      $es_cliente = 'false';
    }

    if (!empty($datos["es_adquirente"])) {
      $es_adquirente = 'true';
    }else{
      $es_adquirente = 'false';
    }

    $suip_data = array(
      'crud:crud:altaContribuyenteProactivo' => array(
        'crud:crud:contribuyente' => array(
          'urn:urn:adquirente' => $es_adquirente,
          'urn:urn:cliente' => $es_cliente,
          'urn:urn:cuit' => $datos["cuit"],
          'urn:urn:domicilioLegal' => $datos["direccion"],
          'urn:urn:email' => $datos["correo_electronico"],
          'urn:urn:estado' => $datos["estado"],
          'urn:urn:fechaEstado' => $date->format('Y-m-d\TH:i:s.\000\Z'),
          'urn:urn:nombreFantasia' => $datos["nombre_comercio"],
          'urn:urn:nombreFirmante' => $datos["apellido_nombre"],
          'urn:urn:razonSocial' => $datos["razon_social"],
          'urn:urn:rubro' => $datos["rubro"],
        )
      )
    );
    //watchdog('bbva_pdm', 'mensaje dentro de alta , %error', array('%error' => htmlspecialchars_decode(print_r($suip_data, true))));

    $this->suipConn->setUri(self::URI);
    $this->suipConn->setData($suip_data);
    $respuesta_suip = $this->suipConn->request();

    $response = new AltaContribuyenteConfirmacionResponse();
    $retorno = $response->buildFromXML($respuesta_suip->data); // Esto regresa STRING del estado del alta
    if (isset($retorno)) {
      $codigos['codigoTipoCliente'] = $retorno->codigoTipoCliente;
      $codigos['codigoActivacion'] = $retorno->codigoActivacion;
      $codigos['detalleError'] = $retorno->detalleError;
      $codigos['linkRegistroPortal'] = $retorno->linkRegistroPortal;
      return $codigos;
    }

    throw new \Exception();
  }

  public function consultarCuitComercio($tipo_documento, $numero_documento) {
    try {
      $comercioResponse = FALSE;

      if (mb_strlen($numero_documento) > 13 ) {
        throw new Exception("Numero de documento mayor a 13");
      }

      $bbva_paquete_datos = array(
        "numeroDocumento" => str_pad($numero_documento, 13, "0", STR_PAD_LEFT),
        "tipoDocumento" => $tipo_documento,
        "subtransaccion" => 1,
      );

      $this->bbvaPaqueteConn->setData($bbva_paquete_datos, PDM_REQ_CONSULTA_CUIT);
      $respuesta = $this->bbvaPaqueteConn->request();
      
      if (isset($respuesta->data->Respuesta->Resultado)) {
        $odt = new ComercioResponse();
        $comercioResponse = $odt->buildFromXML($respuesta->data);
      } else {
        $message = "";
        if(isset($respuesta->data->Respuesta->Errores->Error)){
            $message = $respuesta->data->Respuesta->Errores->Error;
        }
        throw new Exception($message);
      }
    } catch (\Exception $e) {
      throw new Exception("Error en el servicio de consultarCuitComercio: ".$e->getMessage(), 1);
    }

    return $comercioResponse;
  }
}

class AltaContribuyenteConfirmacionResponse {

  /*
  * 1) CUIT INEXISTENTE EN TABLAS SUIP
  * Se debe dar de alta el contribuyente y comercio con los datos de entrada, devolviendo un código 1 "alta ok".
  *
  * 2) CUIT EXISTENTE EN TABLA CON ESTADO OBSERVADO
  * Se debe verificar si el cuit que existe como contribuyente y el estado es observado. Si esto es así, se debe devolver código 2 "contribuyente observado".
  *
  * 3) CUIT EXISTENTE EN TABLA CON ESTADO RECHAZADO
  *Se debe verificar si el cuit que existe como contribuyente y el estado es rechazado. Si esto es así, se debe devolver código 3 "contribuyente rechazado".
  *
  * 4) CUIT EXISTENTE EN TABLA CON ESTADO OK CON INGRESO AL PORTAL
  *Se debe verificar que el cuit existe como contribuyente y posee usuario en la tabla usuario_contribuyente. Si esto es así, se debe devolver código 4 "contribuyente OK ya ingresó al portal". (Usuario posiblemente se le olvido la contraseña....)
  *
  * 5) CUIT EXISTENTE EN TABLA CON ESTADO OK SIN INGRESO AL PORTAL
  * Se debe verificar que el cuit existe como contribuyente y NO posee usuario en la tabla usuario_contribuyente. Si esto es así, se deben actualizar los datos enviados que vinieron en la llamada y devolver código 5 "contribuyente OK no ingresó al portal". (Fue dado de alta por otro medio)
  *
  * 6) CUIT EXISTENTE EN TABLA SIN ESTADO (dado de alta por alianzas sin que el comercio supiera)
  * Se devuelve el codigo 6
  */

  public $codigoTipoCliente;
  public $codigoActivacion;
  public $detalleError;
  public $linkRegistroPortal;

  public function buildFromXML($xmlData) {
    $codigo_activacion = null;
    $detalle_error = null;
    $link_registro_portal= null;

    $codigo_retorno = $xmlData->soapenv_Body->altaContribuyenteProactivoResponse->altaContribuyenteProactivoReturn->codigoTipoCliente;

    $codigo_activacion = $xmlData->soapenv_Body->altaContribuyenteProactivoResponse->altaContribuyenteProactivoReturn->codigoActivacion;
    if (!empty($codigo_activacion)){
      $this->codigoActivacion = $codigo_activacion->__toString();
    }

    $detalle_error = $xmlData->soapenv_Body->altaContribuyenteProactivoResponse->altaContribuyenteProactivoReturn->detalleError;
    if (!empty($detalle_error)){
      $this->detalleError = $detalle_error->__toString();
    }

    $link_registro_portal = $xmlData->soapenv_Body->altaContribuyenteProactivoResponse->altaContribuyenteProactivoReturn->linkRegistroPortal;
    if (!empty($link_registro_portal)){
      $this->linkRegistroPortal = $link_registro_portal->__toString();
    }

    if ($codigo_retorno >= 1 && $codigo_retorno <= 6) {
      switch ($codigo_retorno){
        case 1:
          $this->codigoTipoCliente = "APROBADO";
          break;
        case 2:
          $this->codigoTipoCliente = "OBSERVADO";
          break;
        case 3:
          $this->codigoTipoCliente = "RECHAZADO";
          break;
        case 4:
          $this->codigoTipoCliente = "EXISTENTE";
          break;
        case 5:
          $this->codigoTipoCliente = "FALTA_COD_ACTIVACION";
          break;
        case 6:
          $this->codigoTipoCliente = "EXISTENTE_SIN_ESTADO";
          break;
      }
    }

    if (!empty($this->detalleError)){
      throw new Exception($this->detalleError, 1);
    }
    return $this;
  }
}

class ComercioResponse {
  public $es_adquirente;
  public $es_cliente;

  /*Retorno 10
    CLIENTE Y ADQUIRENTE
    Retorno 20
    CLIENTE Y NO ADQUIRENTE
    Retorno 30
    NO CLIENTE Y ADQUIRENTE
    Retorno 40
    NO CLIENTE Y NO ADQUIRENTE*/

  public function buildFromXML($xmlData) {

    $codigoRetorno = $xmlData->Respuesta->Resultado->retorno->__toString();

    if ($codigoRetorno == "10") {
      $this->es_adquirente = TRUE;
      $this->es_cliente = TRUE;
    } else if ($codigoRetorno == "20") {
      $this->es_adquirente = FALSE;
      $this->es_cliente = TRUE;
    } else if ($codigoRetorno == "30") {
      $this->es_adquirente = TRUE;
      $this->es_cliente = FALSE;
    } else if ($codigoRetorno == "40") {
      $this->es_adquirente = FALSE;
      $this->es_cliente = FALSE;
    } else if ($codigoRetorno == "00") {
      $this->es_adquirente = FALSE;
      $this->es_cliente = FALSE;
    } else {
      throw new Exception("Código de retorno Consulta CUIT desconocido", 1);
    }

    return $this;
  }
}