<?php

namespace Services\BBVA;

define('SERVICIO_INICIAL_URI', '/ServicioInicial');

define('BBVA_PAQUETE_EMPRESA', 'BBVA');
define('BBVA_PAQUETE_APLICACION', 'FRANCESGO');
define('BBVA_PAQUETE_IDUSUARIO', 'NH');
define('BBVA_PAQUETE_IDTRAZASERVICIOORIGEN', '1');
define('BBVA_PAQUETE_TIPOMENSAJE', 'E');
define('BBVA_PAQUETE_IDPROCESO', '0001');
define('BBVA_PAQUETE_MODULO', 'AnyModule');
define('BBVA_PAQUETE_IDSESION', '42434');


require_once 'base_services.class.inc';

class BbvaPaquete extends BaseServices {

  private $xml;

  function __construct() {
    $this->host = variable_get('bbva_services_servicioinicial_host', FALSE);
    $options = array(
      'method' => 'POST',
      'timeout' => variable_get('bbva_services_servicioinicial_timeout', 10),
      'headers' => array(
        'Content-Type' => 'text/xml;charset=UTF-8',
        'Accept-Encoding' => 'gzip,deflate',
        'SOAPAction' => '',
        'Content-Length' => 0,
      ),
    );

    parent::__construct($options);
  }

  protected function _prepare() {
    $options = $this->options;
    $body = $this->xml->addChild("Datos");
    $this->_completarXML($this->data, $body);
    $options['data'] = $this->xml->asXML();
    $options['headers']['Content-Length'] = strlen($options['data']);
    return $options;
  }

  public function getResponse() {
    $response = $this->response;
    // Si es un xml lo convierto

    if ($response->code != 200) {
      if(module_exists('bbva_logs')) {
        $data_log = array(
          "code" => $response->code,
          "error" => $response->error,
          "data" => $this->data
        );
        $log = \BBVA\Log::getInstance();
        $log->insertar("bbva_paquete_error", $data_log);
      }
      throw new SuipException($response->error, $response->code);
    }

    $data_tmp = new \SimpleXMLElement(str_replace(":", "_", $response->data));
    if(is_object($data_tmp)) {
      $response->data = $data_tmp;
    }

    return $response;
  }

  private function _completarXML($array = array(), &$xml) {
    foreach($array as $key => $value) {
      if(is_array($value)) {
         watchdog('xml', ' Key data: %data ', array('%data' => $key, ));
        if(!is_numeric($key)){

          $subnode = $xml->addChild("$key");
          $this->_completarXML($value, $subnode);
        } else {
          $this->_completarXML($value);
        }
      } else {
        $xml->addChild("$key", htmlspecialchars("$value"));
      }
    }
  }


  public function setData($data = array(), $requerimiento = null) {

    $d = new \DateTime("now");
    $fecha = $d->format('d-m-Y');
    $hora = $d->format('H:i:s');

    $this->xml = new \SimpleXMLElement("<BbvaPaquete></BbvaPaquete>");
    $header = $this->xml->addChild('Header');
    $header->addChild("Empresa", BBVA_PAQUETE_EMPRESA);
    $header->addChild("Aplicacion", BBVA_PAQUETE_APLICACION);
    $header->addChild("IdUsuario", BBVA_PAQUETE_IDUSUARIO);
    $header->addChild("Requerimiento", $requerimiento);
    $header->addChild("IdTrazaServicioOrigen", BBVA_PAQUETE_IDTRAZASERVICIOORIGEN);
    $header->addChild("FechaPedido", $fecha);
    $header->addChild("HoraPedido", $hora);
    $header->addChild("TipoMensaje", BBVA_PAQUETE_TIPOMENSAJE);
    $header->addChild("IdProceso", BBVA_PAQUETE_IDPROCESO);
    $header->addChild("Modulo", BBVA_PAQUETE_MODULO);
    $header->addChild("IdSesion", BBVA_PAQUETE_IDSESION);

    $this->data = $data;
    return $this;
  }
}


