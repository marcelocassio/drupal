<?php

namespace Services\BBVA;

require_once 'base_services.class.inc';

class FMovil extends BaseServices {

  private $xml;

  function __construct() {
    $this->host = variable_get('bbva_services_fmovil_host', FALSE);
    $options = array(
      'method' => 'POST',
      'timeout' => variable_get('bbva_services_fmovil_timeout', 10),
      'headers' => array(
        'Content-Type' => 'text/xml;charset=UTF-8',
        'Accept-Encoding' => 'gzip,deflate',
        'SOAPAction' => '',
        'Content-Length' => 0,
      ),
    );

    parent::__construct($options);
  }

  protected function _prepare() {
    $options = $this->options;
    $body = $this->xml->addChild("soapenv:soapenv:Body");
    $this->_completarXML($this->data, $body);
    $options['data'] = $this->xml->asXML();
    $options['headers']['Content-Length'] = strlen($options['data']);
    return $options;
  }

  public function getResponse() {
    $response = $this->response;
    // Si es un xml lo convierto

    if ($response->code != 200) {
      watchdog('fmovil', ' Erro en servicios fmovil Codigo: %data - %msg', array('%data' => $response->code, '%msg' => $response->error));
      throw new FMovilException($response->error, $response->code);
    }

    $data_tmp = new \SimpleXMLElement(str_replace(":", "_", $response->data));
    if(is_object($data_tmp)) {
      $response->data = $data_tmp;
    }

    return $response;
  }

  private function _completarXML($array = array(), &$xml) {
    foreach($array as $key => $value) {
      if(is_array($value)) {
        if(!is_numeric($key)){
          $pos = strrpos($key, "@");
          if ($pos === false) {
            $subnode = $xml->addChild("$key");
          } else {
            $key_new = substr($key, 0, $pos);
            $subnode = $xml->addChild("$key_new");
          }
          $this->_completarXML($value, $subnode);

        } else {
          $this->_completarXML($value);
        }
      } else {
        $xml->addChild("$key", utf8_encode(htmlspecialchars("$value")));
      }
    }
  }


  public function setData($data = array()) {

    $this->xml = @new \SimpleXMLElement("<soapenv:soapenv:Envelope></soapenv:soapenv:Envelope>");
    $this->xml->addAttribute( 'xmlns:xmlns:soapenv', "http://schemas.xmlsoap.org/soap/envelope/" );
    $this->xml->addAttribute( 'xmlns:xmlns:urn', "urn:Go2" );
    $this->xml->addAttribute( 'xmlns:xmlns:xsd', "http://www.w3.org/2001/XMLSchema" );
    $this->xml->addAttribute( 'xmlns:xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance" );
    $this->xml->addChild('soapenv:soapenv:Header');

    $this->data = $data;
    return $this;
  }
}


class FMovilException extends \Exception {

}

