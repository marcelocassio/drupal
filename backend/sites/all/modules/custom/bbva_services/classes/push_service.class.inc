<?php

define('PUSH_SERVICE_ENVIRONMENT', 'PRODUCTION');
define('PUSH_SERVICE_APP_ID', 'FRANCESGO');
define('PUSH_SERVICE_ONDEMAND', 'PUSH_MSGS_ONDEMAND');
define('PUSH_SERVICE_TEXT_MSG_LEN', 170);
define('PUSH_SERVICE_TEXT_MSGEX_LEN', 100);
define('PUSH_SERVICE_DEEPLINKING_LEN', 100);
define('PUSH_SERVICE_NOM_IMG_LEN', 32);
define('PUSH_SERVICE_METADATA_LEN', 255);

class PushService {

  private static $instancia;
  private $bbvaPaqueteConn;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
    $this->bbvaPaqueteConn = new \Services\BBVA\BbvaPaquete();
    $this->bbvaPaqueteConn->setUri(SERVICIO_INICIAL_URI);
  }



  public function enviarPushOnDemand($mensaje, $mensaje_extendido = NULL, $deeplinking = NULL, $nombre_imagen = NULL) {
    $pushMessageResponse = null;
    $mensaje = $mensaje;
    $mensaje_extendido = $mensaje_extendido;

    if (mb_strlen($mensaje) >= PUSH_SERVICE_TEXT_MSG_LEN ) {
      throw new PushException("El mensaje no puede superar los " . PUSH_SERVICE_TEXT_MSG_LEN . " caracteres");
    }
    if (mb_strlen($mensaje_extendido) >= PUSH_SERVICE_TEXT_MSGEX_LEN ) {
      throw new PushException("El mensaje extendido no puede superar los " . PUSH_SERVICE_TEXT_MSGEX_LEN . " caracteres");
    }
    if (mb_strlen($deeplinking) >= PUSH_SERVICE_DEEPLINKING_LEN ) {
      throw new PushException("El mensaje no puede superar los " . PUSH_SERVICE_DEEPLINKING_LEN . " caracteres");
    }
    if (mb_strlen($nombre_imagen) >= PUSH_SERVICE_NOM_IMG_LEN ) {
      throw new PushException("El nombre de imagen no puede superar los " . PUSH_SERVICE_NOM_IMG_LEN . " caracteres");
    }

    $metadata = array(
      'texto_adicional' => $mensaje_extendido,
      'call_to_action' => $deeplinking
      );

    if ( variable_get('fgo_activar_cta_push_on_demand') == 1) {
      $metadata_json = str_replace('\\/', '/', json_encode($metadata));
    }else{
      $metadata_json = json_encode($metadata);
    }

    if (strlen($metadata_json) >= PUSH_SERVICE_METADATA_LEN ) {
      throw new PushException("La metadata no puede superar los " . PUSH_SERVICE_METADATA_LEN . " caracteres");
    }

    $bbva_paquete_datos = array(
     "ondemandParams" => array(
       "appId" => PUSH_SERVICE_APP_ID,
       "environment" => PUSH_SERVICE_ENVIRONMENT,
       "textMsg" => $mensaje,
       "metaData" => $metadata_json,
       "imgName" => $nombre_imagen,
      )
    );

    $this->bbvaPaqueteConn->setData($bbva_paquete_datos, PUSH_SERVICE_ONDEMAND);
    $respuesta = $this->bbvaPaqueteConn->request(); // un object
    if (isset($respuesta->data->Respuesta->Errores)) {
      $msgError = $respuesta->data->Respuesta->Errores->Error->__toString();
      throw new PushException($msgError);
    } elseif (isset($respuesta->data->Respuesta->Resultado)) {
      $pushMessageResponse = new PushResponse();
      $pushMessageResponse->buildFromXML($respuesta->data);
    } else {
      throw new PushException("Error inesperado al enviar push on demand");
    }
    return $pushMessageResponse;
  }

}


class PushException extends \Exception {

}



class PushResponse {
  public $codigoRetorno;
  public $mensaje;

  public function buildFromXML($xmlData) {
    $this->codigoRetorno = $xmlData->Respuesta->Resultado->retCode->__toString();
    $this->mesaje = $xmlData->Respuesta->Resultado->retDescrip->__toString();
    return $this;
  }
}
