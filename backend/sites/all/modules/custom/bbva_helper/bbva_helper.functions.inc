<?php
  
  const CUPON_ESTADO_PENDIENTE = 0;
  const CUPON_ESTADO_ENVIADO = 1;
  const CUPON_ESTADO_APROBADO = 2;
  const CUPON_ESTADO_RECHAZADO = 3;
  const CUPON_PROMO_TYPE_VISA = '0';
  const CUPON_PROMO_TYPE_ARCHIVO = '1';
  const CUPON_PROMO_TYPE_EVENTO_CON_ARCHIVO = '2';
  const CUPON_PROMO_TYPE_EVENTO_SIN_ARCHIVO = '3';
  const CUPON_PROMO_TYPE_FARMACITY = '4';
  const NUM_BIN_VISA = 505893;
  const BENEFIT_MEMBERSHIP_CHAN = 3590;
  const TERM_SUIP_ID = 991;
  const CARACTERES_CORRECTOS_VISA = 117;


    /*
     * Logeo en consola, instanciando la clase LOG.
    */
    function helperDebug($debug){
      if(getMagic()){
        $log = \BBVA\Log::getInstance();
        $log->wsdebug($debug);
      }
    }

    function helperDebugBar(){
      if(getMagic()){
        helperDebug('-----------------');
      }
    }

    function hd($debug, $bar = false){
      
      if($bar == true){
        hdb();
      }
      helperDebug($debug);
      if($bar == true){
        hdb();
      }
    }

    function hdb(){
      helperDebugBar();
    }

    /*
     * Time Stamap To Time
     *
    */
    function tst($date = null, $full = false){
      return $date ? $full ? date('d/m/Y H:i:s', $date) : date('d/m/Y', $date) : ''; 
    }


    function formatUri($uri = null){
      return $uri ? file_create_url($uri) : '';
    }

    /*
     * toma los parametros de un json por post
     * return json
    */

    function getPostParams(){
      $received_json = file_get_contents("php://input", true);
      return drupal_json_decode($received_json, true);
    }

    /*
     * Valida si esta seteada la variable o devuelve null.
    */
    function hisset($variable, $var2 = null){
      $variableOK = isset($var2) ? $var2 : $variable;
      return isset($variable) ? $variableOK : null;
    }

    /*
     * Param @object
     * Return Array, con los valors und seteados de ese objeto.
     * Convierto los datos recibidos en un objeto administrable
     * ver bbva_profile_get_subscription()
    */
    function undFields($object, $fid = false){
      $undFields = (array) $object;

      $retu = array();
      foreach ($undFields as $key => $field) {
        if(is_array($field)){

          if($fid){
            $fieldValue = isset($field['und']) ? isset($field['und'][0]) ? isset($field['und'][0]['fid']) ? $field['und'][0]['fid'] : null : null : null;
          }
          else{
            $fieldValue = isset($field['und']) ? isset($field['und'][0]) ? isset($field['und'][0]['value']) ? $field['und'][0]['value'] : null : null : null;
          }
          
          $retu[$key] = $fieldValue;
        }
      }
      if(isset($object->nid)){
        $retu['nid'] = $object->nid;
      }

      return $retu;
    }

    /*
     * Uso de undFields con global $user
     * Agrega al array final el campo [name]
     * return array()
    */
    function userUndFields(){

      global $user;
      $user_profile = user_load($user->uid);
      $userUndFields = undFields($user_profile);
      $userUndFields['name'] = $user_profile->name;
      $userUndFields['uid'] = $user->uid;

      return $userUndFields;
    }

    /*
     * Helper para estructiura form_state
     *
    */
    function valueFormState($form_state, $field, $number = false){
      if($number){
        $valueFormState = $form_state['values'][$field]['und'][0]['value'];
      }
      else{
        $valueFormState = $form_state['values'][$field]['und']['0']['value'];
      }
      return $valueFormState;
    }

    function valueField($dataFields, $field, $number = false){
      if($number){
        $valueField = $dataFields[$field]['und'][0]['value'];
      }
      else{
        $valueField = $dataFields[$field]['und']['0']['value'];
      }
      return $valueField;
    }

    function isLoggedUser(){
      global $user;
      return $user->uid != 0;
    }

    function isAndroid(){
      return strstr($_SERVER[ 'HTTP_USER_AGENT' ], 'Android');
    }

    function isProd(){
      global $conf;
      return $conf['env'] == 'prod';
    }

    function jsonToArray($json){
      return json_decode($json, true);
    }

    function getMagic($varMagic = 'bbva_hd_prod_magic'){
      return variable_get($varMagic, '') == 'true';
    }

    function hdie($text){
      hd($text, true);
      die;
    }

    function borrarCache($nombre){

      $cache = \BBVA\Cache\CacheFile::getInstance();
      
      switch ($nombre) {
        case 'home':
            $cache->clean(array('client/home'));
          break;
          case 'coupon_promo':
          case 'cupones':
            entity_get_controller('coupon_promo')->resetCache();
          break;
          case 'jsonTarjetas':
            cache_set('jsonTarjetas',_get_tarjetas_all(array()),'cache_api',CACHE_PERMANENT);
          break;
          case 'archivos':
            $cache = BBVA\Cache\CacheFile::getInstance();
            $clean = $cache->clean();
          break;
        default:
          break;
      }
    }

    function getRandomNumber(){
      return substr(number_format(time() * rand(), 0, '', ''), 0, 6);  
    }

    function eol(){
      echo PHP_EOL;
    }

    function echoline($message, $usage = false, $hd = true){
      eol();
      $mensaje = $usage ? $message." - Uso de memoria: ".round(memory_get_usage() / 1048576)." MB \n" : $message;
      if($hd){
        hd($mensaje);
      }
      echo $mensaje;
    }

    function getUserRoles(){
      global $user;
      return $user->roles;
    }

    
    function log_seg_online($message, $data){
        if(variable_get("bbva_segmentacion_service_online_cache_log") == "enable"){
            watchdog('segm_online', $message.' : %data ', array('%data' => $data, )); 
        }
         
    }


    // Limpio el cache de cupones

    // Borro el cache de archivo del Home
    // $cache = \BBVA\Cache\CacheFile::getInstance();
    // $cache->clean(array('client/home'));



    /*
drush sql-query 'TRUNCATE TABLE cache';
drush sql-query 'TRUNCATE TABLE cache_block';
drush sql-query 'TRUNCATE TABLE cache_bootstrap';
drush sql-query 'TRUNCATE TABLE cache_field';
drush sql-query 'TRUNCATE TABLE cache_filter';
drush sql-query 'TRUNCATE TABLE cache_form';
drush sql-query 'TRUNCATE TABLE cache_image';
drush sql-query 'TRUNCATE TABLE cache_menu';
drush sql-query 'TRUNCATE TABLE cache_page';
drush sql-query 'TRUNCATE TABLE cache_path';
drush sql-query 'TRUNCATE TABLE cache_token';
drush sql-query 'TRUNCATE TABLE cache_update';


mas claro echale water
$entity = entity_load_single('coupon_promo', $voucher->id_cupon);
$cupon = entity_metadata_wrapper('coupon_promo', $entity);


    */


