<?php
/*
 * La documentación del componente para crear tablas esta aquí
 * https://github.com/xaksis/vue-good-table
 *
 * Buscar en esta dirección las dependencias que se quieran usar y bajarlas local:
 * https://cdn.jsdelivr.net/npm/vue-particles
 * O
 * https://unpkg.com/vue-particles@1.0.9/src/vue-particles/index.js
 *
 * */
global $base_url;
$path_backend = drupal_get_path('module', 'fgo_backend');
//Principal VueJS
drupal_add_js($path_backend . '/js/vue.min.js');

//Dependencia para hacer llamadas http //AXIOS
drupal_add_js($path_backend . '/js/axios_0_18_0.min.js');

//Dependencias para tablas
drupal_add_js($path_backend . '/js/vue-good-table.js');
drupal_add_css($path_backend . '/css/vue-good-table.min.css');

// Bootstrap 4
drupal_add_css($path_backend . '/css/bootstrap_4_1_1.min.css');

// Font Awesome
drupal_add_css($path_backend . '/css/font_awesome_4_7.min.css');

//Custom
drupal_add_css($path_backend . '/css/fgo_backend_default.css');
drupal_add_css($path_backend . '/css/fgo_backend.css');


?>
<div id="container-particles-js">
    <div id="particles-js"></div>
</div>
<div id="admin_container">
    <h1>Administración de Call to Action</h1>
    <div class="row no-gutters">
      <div class="col-md-12 mb-2">
        <div class="d-flex">
          <button @click="nuevo()" class="boton_fgo boton_positivo ml-auto">Agregar Accion</button>
        </div>
      </div>
      
    </div>
    <div v-if="loading" class="spinner_backend" v-cloak>
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    <vue-good-table v-else
            :columns="formattedColumns"
            :rows="comercios"
            style-class="vgt-table striped condensed"
            :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: true, perPage: 50, ofLabel: 'de'}"
            :search-options="{ enabled: true, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
        <div v-slot:emptystate>
            No hay comercios con ese criterio
        </div>
        <template v-slot:table-row="props">
            <div v-if="props.column.field == 'backendImagenesC'">
                <div v-html="props.row.backendImagenesC"></div>
            </div>
            <div v-else-if="props.column.field == 'botones'">
                <div v-html="props.row.botones"></div>
            </div>
            <div v-else>
              {{props.formattedRow[props.column.field]}}
            </div>
        </template>
    </vue-good-table>
</div>
<script src="<?=$base_url?>/<?=$path_backend?>/js/particles.min.js"></script>
<script>
    let link = '<?=$base_url?>/API/backend/cta_param1';
    const FGO_BACKEND_IMG_DEFAULT = '/fgo/static/media/logogo.1cb228c2.svg';

    new Vue({
        el: '#admin_container',
        data: {
            comercios: [],
            loading: false,
            columns: [
                {
                    label: 'Id Parametro',
                    field: 'idParam1',
                    type: 'number',
                },{
                    label: 'Nombre',
                    field: 'nombre',
                },{
                    label: 'Accion',
                    field: 'callToAction',
                    filterable: true
                },{
                    label: 'Acciones',
                    field: 'botones',
                    globalSearchDisabled: true,
                },
            ]
        },
        methods:{
            getComercios(){
                this.loading = true;
                axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200){
                            this.comercios = response.data;
                        }else{
                            this.comercios = null;
                        }
                    })
                    .catch(error => {
                        this.comercios = null;
                    })
                    .finally(() => this.loading = false)
            },
            nuevo(){
              window.location.href = '<?php echo $base_url?>/admin/v2/call_to_action/agregar';
            },
            renderData(comercios) {
                comercios.map(item => {
                    item.botones = ' <a href="call_to_action/editar/' + item.idParam1 + '" class="btn btn-outline-info btn-md"> Agregar/Editar </a>';
                });
            },
        },
        computed:{
            formattedColumns(){
                if(this.comercios){
                    this.renderData(this.comercios);
                    return this.columns;
                }
            }
        },
        mounted() {
            this.getComercios();
        }
    });
    particlesJS.load('particles-js', '<?=$base_url?>/<?=$path_backend?>/js/particles.json', function() {});
</script>
