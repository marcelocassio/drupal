<?php
function fgo_cta_param1_form($form_state, $build) {
  
  $form_state['redirect'] = false;
  
  $_default = array(
    'idParam1' => null,
    'nombre' => '',
    'callToAction' => ''
  );
  
  if( ! empty ( $build["build_info"]["args"] ) ) {
    $row = $build["build_info"]["args"][0];
    $_default['idParam1'] = $row->idParam1;
    $_default['nombre'] = $row->nombre;
    $_default['callToAction'] = $row->callToAction;
  }
  
  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['#attributes']['autocomplete'] = "off";
  $form['#validate'][] = 'fgo_cta_param1_form_validate';
  
  $form['grupo_acciones'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Acciones</i></strong>'),
  );

  $form['grupo_acciones']['idParam1'] = array(
      '#type' => 'hidden',
      '#default_value' => $_default['idParam1']
  );

  $form['grupo_acciones']['nombre'] = array(
    '#title' => t('Nombre'),
    '#type' => 'textfield',
    '#default_value' => $_default['nombre']
  );

  $form['grupo_acciones']['callToAction'] = array(
    '#title' => t('Call to Action'),
    '#type' => 'textfield',
    '#default_value' => $_default['callToAction']
  );
  
  $form['guardar'] = array(
      '#type' => 'submit',
      '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
      '#value' => 'Guardar',
      '#submit' => array('fgo_cta_param1_form_submit')
  );

  return $form;
}

function fgo_cta_param1_form_submit(&$form, &$form_state){
  $form_state['redirect'] = url('../admin/v2/call_to_action');
}

function fgo_cta_param1_form_validate(&$form, &$form_state){
  $ctaParam1Bo = new Fgo\Bo\CtaParam1Bo( $form_state['values']['idParam1'] );
  $ctaParam1Bo->nombre = $form_state['values']['nombre'];
  $ctaParam1Bo->callToAction = $form_state['values']['callToAction'];
  
  
  try {
    $ctaParam1Bo->guardar();
  } catch ( \Fgo\Bo\DatosInvalidosException $e) {
    $errores = $e->getErrores();
    foreach ($errores as $error_campo => $error_descripcion){
      form_set_error($error_campo, $error_descripcion);
    }
  }
  
}
