<?php
/* //////////////////////////////////////////////////////////////////////// */
// Admin CTA
/* //////////////////////////////////////////////////////////////////////// */

function fgo_backend_admin_cta() {
  $form = array();

  $form['fgo_activar_cta_push'] = array(
      '#type' => 'checkbox',
      '#title' => t('Usar nuevo CTA en las Notificaciones/Push'),
      '#default_value' => variable_get('fgo_activar_cta_push', 0),
      '#description' => t("Habilitar el uso del nuevo CTA en el admin de Push."),
  );

  $form['fgo_activar_cta_push_on_demand'] = array(
      '#type' => 'checkbox',
      '#title' => t('Usar nuevo CTA en Push on Demand'),
      '#default_value' => variable_get('fgo_activar_cta_push_on_demand', 0),
      '#description' => t("Habilitar el uso del nuevo CTA en el admin de Push on demand."),
  );

  $form['fgo_activar_cta_push_sorteo_ganadores'] = array(
      '#type' => 'checkbox',
      '#title' => t('Usar nuevo CTA para mandar Push a los ganadores'),
      '#default_value' => variable_get('fgo_activar_cta_push_sorteo_ganadores', 0),
      '#description' => t("Habilitar el uso del nuevo CTA en el proceso de generacion de push para ganadores."),
  );

  return system_settings_form($form);
}