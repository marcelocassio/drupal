const badModal = {
    name: "inside-modal",
    template: "<div class='sweet-modal-icon sweet-modal-warning pulseWarning'><span class='sweet-modal-body pulseWarningIns'></span> <span class='sweet-modal-dot pulseWarningIns'></span></div>"
};

const goodModal = {
    name: "inside-modal",
    template: "<div class='sweet-modal-icon sweet-modal-success animate'><span class='sweet-modal-line sweet-modal-tip animateSuccessTip'></span> <span class='sweet-modal-line sweet-modal-long animateSuccessLong'></span> <div class='sweet-modal-placeholder'></div></div>"
};

const validateModal = {
    name: "inside-modal",
    template: "<div class='sweet-modal-icon sweet-modal-warning pulseWarning'><span class='sweet-modal-body pulseWarningIns'></span> <span class='sweet-modal-dot pulseWarningIns'></span></div></div>"
};