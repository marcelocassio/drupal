<?php
/*
 * La documentación del componente para crear tablas esta aquí
 * https://github.com/xaksis/vue-good-table
 *
 * Buscar en esta dirección las dependencias que se quieran usar y bajarlas local:
 * https://cdn.jsdelivr.net/npm/vue-particles
 * O
 * https://unpkg.com/vue-particles@1.0.9/src/vue-particles/index.js
 *
 * Versión de Vue 2.6
 * Versión de Vue-Good-Table 2.16
 * */
global $base_url;
$path_backend = drupal_get_path('module', 'fgo_backend');
$path_backend_home = drupal_get_path('module', 'fgo_backend_home');

//Principal VueJS
drupal_add_js( $path_backend . '/js/vue.min.js');

//Dependencia para hacer llamadas http //AXIOS
drupal_add_js($path_backend . '/js/axios_0_18_0.min.js');

//Dependencias para tablas
drupal_add_js($path_backend . '/js/vue-good-table.js');
drupal_add_css($path_backend . '/css/vue-good-table.min.css');

// Bootstrap 4
drupal_add_css($path_backend . '/css/bootstrap_4_1_1.min.css');

// Font Awesome
drupal_add_css($path_backend . '/css/font_awesome_4_7.min.css');

//Custom
drupal_add_css($path_backend .'/css/fgo_backend_default.css');
drupal_add_css($path_backend . '/css/fgo_backend.css');
?>
<div id="container-particles-js">
    <div id="particles-js"></div>
</div>
<div id="admin_container">
    <h1>Administración de Comunicaciones</h1>
    <div v-if="loading" class="spinner_backend" v-cloak>
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    <vue-good-table v-else
            :columns="formattedColumns"
            :rows="communications"
            style-class="vgt-table striped condensed"
            :select-options="{ enabled: true, selectionText: 'Seleccionados', selectOnCheckboxOnly: true,  clearSelectionText: 'Destildar', selectionInfoClass: 'fila_seleccionada', }"
            :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: true, perPage: 40, ofLabel: 'de'}"
            @on-selected-rows-change="selectionChanged"
            :search-options="{ enabled: true, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
        <div v-slot:emptystate>
            No hay beneficios con ese criterio
        </div>
        <div v-slot:selected-row-actions>
            <button @click="exportCommunications">Exportar comunicaciones</button>
        </div>
        <template v-slot:table-row="props">
            <div v-if="props.column.field == 'backendImagenesB'">
                <div v-html="props.row.backendImagenesB"></div>
            </div>
            <div v-else-if="props.column.field == 'backendImagenesC'">
                <div v-html="props.row.backendImagenesC"></div>
            </div>
            <div v-else-if="props.column.field == 'botones'">
                <div v-html="props.row.botones"></div>
            </div>
            <div v-else>
              {{props.formattedRow[props.column.field]}}
            </div>
        </template>
    </vue-good-table>
    </div>
<script>
    const link = 'https://' + window.location.host + '/fgo/API/v1/backend/comunicaciones';
    const linkExport = 'https://' + window.location.host + '/fgo/API/v1/backend/comunicaciones/exportar';
    var seleccionados = [];
    const FGO_BACKEND_IMG_DEFAULT = '/fgo/static/media/logogo.1cb228c2.svg';

    new Vue({
        el: '#admin_container',
        data: {
            communications: [],
            loading: false,
            columns: [
                {
                    label: 'ID',
                    field: 'idComunicacion',
                    type: 'number',
                },{
                    label: 'ID Suip',
                    field: 'idBeneficioSuip',
                    type: 'number',
                },{
                    label: 'Título',
                    field: 'titulo',
                },{
                    label: 'Campaña',
                    field: 'nombreCampania',
                },{
                    label: 'Descripción',
                    field: 'descripcionExtendida',
                },{
                    label: 'Comercio Imagen',
                    field: 'backendImagenesC',
                    globalSearchDisabled: true,
                },{
                    label: 'Beneficio Imagen',
                    field: 'backendImagenesB',
                    globalSearchDisabled: true,
                },{
                    label: 'Fecha Desde',
                    field: 'fechaDesde',
                    type: 'date',
                    dateInputFormat: 'DD-MM-YYYY',
                    dateOutputFormat: 'DD-MM-YYYY',
                    width: '90px',
                },{
                    label: 'Fecha Hasta',
                    field: 'fechaHasta',
                    type: 'date',
                    dateInputFormat: 'DD-MM-YYYY',
                    dateOutputFormat: 'DD-MM-YYYY',
                    width: '90px',
                },{
                    label: 'Acciones',
                    field: 'botones',
                    globalSearchDisabled: true,
                }
            ]
        },
        methods:{
            getCommunications(){
                this.loading = true;
                axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200 && response.data.code === 0){
                            this.communications = response.data.data;
                        }else{
                            this.communications = null;
                        }
                    })
                    .catch(error => {
                        this.communications = null;
                    })
                    .finally(() => this.loading = false)
            },
            renderData(communications) {
                communications.map(item => {
                    var todasImagenes = item.imagenes;
                    let imagenComunicacion = FGO_BACKEND_IMG_DEFAULT;
                    let imagenComercio = FGO_BACKEND_IMG_DEFAULT;

                    if (todasImagenes.length) {
                        imagenComunicacion = this._parseImage(todasImagenes[0].datosImagen.uri);
                    }
                    if (item.comercio !== null) {
                        var todasImagenesComercio = item.comercio;
                        imagenComercio = this._parseImage(todasImagenesComercio[0].datosImagen.uri);
                    }

                    item.backendImagenesB = '<img src="'+ imagenComunicacion + '" alt="" width="130" height="80">';
                    item.backendImagenesC = '<img src="'+ imagenComercio + '" alt="" width="130" height="80">';
                    item.botones = ' <a href="comunicaciones/editar/'+item.idComunicacion+'" class="btn btn-outline-info btn-md"> Agregar/Editar </a>';
                })
            },
            exportCommunications(source){
                let url = linkExport + "?ids=" + seleccionados.join(',');
                window.open(url, "_blank");
            },
            selectionChanged(params) {
                seleccionados = [];
                for (var i=0; i < params.selectedRows.length; i++) {
                    seleccionados.push(params.selectedRows[i].idComunicacion);
                }
            },
            _parseImage(img) {
                if (img){
                    return img.replace("public://","/fgo/sites/default/files/");
                }
            }
        },
        computed:{
            formattedColumns(){
                if(this.communications){
                    this.renderData(this.communications);
                    return this.columns;
                }
            }
        },
        mounted() {
            this.getCommunications();
        }
    });
</script>
