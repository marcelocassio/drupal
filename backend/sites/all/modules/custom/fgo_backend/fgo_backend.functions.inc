<?php
/***
 * Guarda un imagen cargada en el core de Go y de Drupal
 * @param $fid
 * @param $codigoImagen
 * @param null $idComunicacion
 * @return \Fgo\Bo\ImagenBo|null
 */
function fgo_backend_guardar_imagen_drupal($fid, $codigoImagen, $idComunicacion=NULL) {
  $resultado = null;
  $imagenBoId = null;

  $imagenFid = $fid;

  $imagenBo = new \Fgo\Bo\ComunicacionImagenBo();
  $imagenes = $imagenBo->buscarImagenesPorIdComunicacion($idComunicacion);

  if (isset($imagenes[0]->datosImagen->fid)) {
    $imagenFidBo = $imagenes[0]->datosImagen->fid;
  }else{
    $imagenFidBo = 0;
  }

  if ($imagenFidBo != $fid ){

    $fileimagen = file_load($imagenFid);
    $fileimagen->status = FILE_STATUS_PERMANENT;
    $imagen_data = file_save($fileimagen);

    //La imagen se guarda en BD FGO

    $imagenBoCreado = fgo_backend_guardar_imagen_bo($imagen_data, null, $codigoImagen);
    $imagenFid = $imagen_data->fid;
    $imagenBoCreado->idImagen;
    if ($imagenBoCreado->idImagen != 0){
      /* Guardado de imagenes dentro de la BD de Drupal */
      file_usage_add(file_load($imagenFid), 'fgo_backend', 'imagen_beneficio', $imagenFid);
    }

    $resultado = $imagenBoCreado;
  } else {
    $resultado = $imagenes[0];
  }

  return $resultado;
}

/***
 * @param $data imagen cargada
 * @param null $id Id de ImagenBo
 * @param $codigo_imagen
 * @return \Fgo\Bo\ImagenBo
 */
function fgo_backend_guardar_imagen_bo($data, $id=NULL, $codigo_imagen){
  $fid_imagen = $data->fid;

  $imagenBo = new \Fgo\Bo\ImagenBo($id);
  $imagenBo->datosImagen->fid = $fid_imagen;
  if (!$id){
    $imagenBo->codigoTipoImagen = $codigo_imagen;
  }
  $imagenBo->guardar();

  return $imagenBo;
}

function fgo_backend_formatear_imagen($imagen_uri) {
  $nombre_imagen = '';
  if (!empty($imagen_uri)) {
    $nombre_imagen = str_replace("public://", "/fgo/sites/default/files/", $imagen_uri);
  }
  return $nombre_imagen;
}

/*
* Funciones para autocompletar inputs de CTA
*/
function fgo_backend_autocomplete_cta($args, $palabra) {
  $id_seccion = $args;

  $palabra = mb_strtolower($palabra);
  $matches = fgo_backend_seleccionar_cta_direccion_data($id_seccion, $palabra);

  drupal_add_http_header('Content-Type', 'application/json');
  drupal_add_http_header('Access-Control-Allow-Origin', "*");
  drupal_add_http_header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

  return isset($matches) ? drupal_json_output($matches) : null;;
  
}

function fgo_backend_seleccionar_cta_direccion_data($seccion, $palabraPorBuscar) {

  $resultado = array();
  $cta_dto = fgo_backend_api_cta();

  if ($cta_dto['code'] == 0){
    $cta_datos = $cta_dto["data"];

    $cta_direcciones = (array)$cta_datos->datos->direcciones;

    foreach ($cta_direcciones as $key => $cta_direccion_temp){
      if ($key == $seccion){
        foreach ($cta_direccion_temp as $cta_direccion){
          $tituloDireccion = mb_strtolower(trim($cta_direccion->titulo));
          if(strpos($tituloDireccion, $palabraPorBuscar) !== false) {
            $resultado[$cta_direccion->titulo ." (".$cta_direccion->id.")"] = $cta_direccion->titulo . " Vigencia: ".$cta_direccion->fecha_vigencia_desde. " al ". $cta_direccion->fecha_vigencia_hasta;
          }
        }
      }
    }
  }

  return $resultado;
}