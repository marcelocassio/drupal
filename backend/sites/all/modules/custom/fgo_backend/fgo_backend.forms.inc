<?php
function fgo_backend_editar_comunicaciones_form($form_state, $build) {

  $id = $build["build_info"]["args"][0];

  $comunicacion = new \Fgo\Bo\ComunicacionBo($id);
  
  $fid_imagen = 0;
  $id_imagen = '';
  
  if (!empty($comunicacion->imagenes)){
    foreach ($comunicacion->imagenes as $imagen_temp){
      $fid_imagen = $imagen_temp->datosImagen->fid;
      $id_imagen = $imagen_temp->idImagen;
    }
  }

  $form['#attributes']['enctype'] = "multipart/form-data";

  $form['grupo_imagen_beneficio'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Imagen Beneficio</i></strong>'),
  );

  $form['grupo_imagen_beneficio']['idComunicacion'] = array(
      '#type' => 'hidden',
      '#default_value' => isset($id) ? $id : ''
  );

  $form['grupo_imagen_beneficio']['id_imagen'] = array(
      '#value' => $id_imagen,
      '#type' => 'hidden',
  );

  $form['grupo_imagen_beneficio']['primer_paso'] = array(
      '#type' => 'fieldset',
  );

  $form['grupo_imagen_beneficio']['idImagenFid'] = array(
      '#type' => 'managed_file',
      '#name' => 'custom_content_block_image',
      '#size' => 40,
      '#default_value' => $fid_imagen,
      '#description' => t("Los archivos deben ser menores que <b>2 MB</b>. <br> Tipos de archivo permitidos: <b>png jpg jpeg</b>."),
      '#upload_location' => 'public://',
      '#theme' => 'preview_img',
      '#upload_validators' => array(
          'file_validate_is_image' => array(),
          'file_validate_extensions' => array('png jpg jpeg'),
          'file_validate_size' => array(2 * 1024 * 1024),
      ),
  );

  $form['grupo_datos_persistentes'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Datos que no se actualizan desde SUIP</i></strong>'),
  );
  $form['grupo_datos'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Datos Temporales (Se importan diariamente)</i></strong>'),
  );
  
    $form['grupo_datos_persistentes']['titulo'] = array(
    '#title' => t('Titulo.'),
    '#type' => 'textfield',
    "#description" => t('titulo de la comunicacion'),
    '#default_value' => $comunicacion->titulo,
  );
  
  $form['grupo_datos_persistentes']['publicado'] = array(
    '#title' => t('Publicado (sin tilde no se visualiza).'),
    '#type' => 'checkbox',
    '#prefix' => '<div style="clear: both;">',
    '#suffix' => '</div>', 
    '#default_value' => $comunicacion->publicado,
  );

  $form['grupo_datos_persistentes']['siempreVisible'] = array(
    '#title' => t('Mostrar siempre (ignorar la fecha de inicio y mostrar la comunicación hasta que termine la vigencia.'),
    '#type' => 'checkbox',
    '#prefix' => '<div style="clear: both;">',
    '#suffix' => '</div>', 
    '#default_value' => $comunicacion->siempreVisible,
  );

  $form['grupo_datos_persistentes']['como_uso'] = array(
    '#title' => t('Como uso personalizado.'),
    '#type' => 'textarea',
    "#description" => t('Usar enters para que se vea en otra viñeta.<br>Usar &lt;br&gt; para un salto de linea en la misma viñeta.'),
    '#default_value' => $comunicacion->comoUso,
  );
  
   $form['grupo_datos']['fecha_desde'] = array(
    '#type' => 'date',
    '#title' => 'Fecha desde',
    '#description' => 'Fecha inicio de vigencia',
      '#attributes'=> array('class'=>'container-inline-date'),
    '#required' => TRUE,
    '#default_value' => array(
      'day' => format_date($comunicacion->fechaDesde, 'custom', 'j'),
      'month' => format_date($comunicacion->fechaDesde, 'custom', 'n'),
      'year' => format_date($comunicacion->fechaDesde, 'custom', 'Y'),
    ),
  );

  $form['grupo_datos']['fecha_hasta'] = array(
    '#type' => 'date',
    '#title' => 'Fecha hasta',
      '#attributes'=> array('class'=>'container-inline-date'),
    '#description' => 'Fecha fin de vigencia ',
    '#required' => TRUE,
    '#default_value' => array(
      'day' => format_date($comunicacion->fechaHasta, 'custom', 'j'),
      'month' => format_date($comunicacion->fechaHasta, 'custom', 'n'),
      'year' => format_date($comunicacion->fechaHasta, 'custom', 'Y'),
    )
  );
  
  $form['grupo_datos']['descripcion_corta'] = array(
    '#title' => t('Descripcion corta'),
    '#type' => 'textarea',
    "#description" => t('Usar enters para que se vea en otra viñeta.<br>Usar &lt;br&gt; para un salto de linea en la misma viñeta.'),
    '#default_value' => $comunicacion->descripcionCorta,
  );
  
    $form['grupo_datos']['descripcion_extendida'] = array(
    '#title' => t('Descripcion extendida'),
    '#type' => 'textarea',
    "#description" => t('Usar enters para que se vea en otra viñeta.<br>Usar &lt;br&gt; para un salto de linea en la misma viñeta.'),
    '#default_value' => $comunicacion->descripcionExtendida,
  );
    
  $form['grupo_datos']['terminos_condiciones'] = array(
    '#title' => t('Términos y condiciones'),
    '#type' => 'textarea',
    "#description" => t('Usar enters para que se vea en otra viñeta.<br>Usar &lt;br&gt; para un salto de linea en la misma viñeta.'),
    '#default_value' => $comunicacion->terminosCondiciones,
  );

  $form['grupo_datos']['cft'] = array(
    '#title' => t('CFT'),
    '#type' => 'textarea',
    "#description" => t('Usar enters para que se vea en otra viñeta.<br>Usar &lt;br&gt; para un salto de linea en la misma viñeta.'),
    '#default_value' => $comunicacion->cft,
  );
  
   $form['grupo_datos']['id_beneficio_suip'] = array(
    '#title' => t('Beneficio suip'),
    // '#autocomplete_path' => 'admin/autocomplete-cta/3',
    '#type' => 'textfield',
    '#default_value' => $comunicacion->idBeneficioSuip,
  );
    
  $form['grupo_datos']['id_comercio'] = array(
    '#title' => t('Comercio'),
    // '#autocomplete_path' => 'admin/autocomplete-comercio',
    '#type' => 'textfield',
    '#default_value' => isset( $comunicacion->comercio ) ? $comunicacion->comercio->idComercio : null,
  );
  
  $form['grupo_datos']['zonas_adheridas'] = array(
    '#title' => t('Zonas adheridas'),
    '#type' => 'textfield',
    '#default_value' => $comunicacion->zonasAdheridas,
  );
  
  $form['grupo_datos']['prioridad'] = array(
    '#title' => t('Prioridad'),
    '#type' => 'textfield',
    '#default_value' => $comunicacion->prioridad,
  );
  
  $form['guardar'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
    '#value' => 'Guardar',
    '#submit' => array('fgo_backend_editar_comunicaciones_form_submit_now')
  );

  return $form;
}

function fgo_backend_editar_comunicaciones_form_submit_now(&$form, &$form_state){
  $form_state['redirect'] = url('../admin/v2/comunicaciones');
}

function fgo_backend_editar_comunicaciones_form_validate(&$form, &$form_state){
  $comunicacionBo = new \Fgo\Bo\ComunicacionBo($form_state['values']['idComunicacion']);
  $comunicacionBo->siempreVisible = $form_state['values']['siempreVisible'];
  $comunicacionBo->publicado = $form_state['values']['publicado'];
  $comunicacionBo->titulo = $form_state['values']['titulo'];
  $comunicacionBo->comoUso = null;
  $comunicacionBo->idBeneficioSuip = $form_state['values']['id_beneficio_suip'];
  $comunicacionBo->descripcionCorta = $form_state['values']['descripcion_corta'];
  $comunicacionBo->terminosCondiciones = $form_state['values']['terminos_condiciones'];
  $comunicacionBo->cft = (float) $form_state['values']['cft'];
  $comunicacionBo->fechaDesde = strtotime( $form_state['values']['fecha_desde']['year'] .'-'. $form_state['values']['fecha_desde']['month'] .'-'. $form_state['values']['fecha_desde']['day'] );
  $comunicacionBo->fechaHasta = strtotime( $form_state['values']['fecha_hasta']['year'] .'-'. $form_state['values']['fecha_hasta']['month'] .'-'. $form_state['values']['fecha_hasta']['day'] );
  
  //Comercio
  $comunicacionBo->idComercio = $form_state['values']['id_comercio'];
  $comercioBo = new \Fgo\Bo\ComercioBo($comunicacionBo->idComercio);
  $comunicacionBo->comercio = $comercioBo;
  
  $comunicacionBo->descripcionExtendida = $form_state['values']['descripcion_extendida'];
  $comunicacionBo->zonasAdheridas = $form_state['values']['zonas_adheridas'];
  $comunicacionBo->prioridad = $form_state['values']['prioridad'];

  if(!empty($form_state['values']['como_uso'])) {
    $comunicacionBo->comoUso = $form_state['values']['como_uso'];
  }
  try {
    $comunicacionBo->guardar();
  } catch ( \Fgo\Bo\DatosInvalidosException $e) {
    $errores = $e->getErrores();
    foreach ($errores as $error_campo => $error_descripcion){
      form_set_error($error_campo, $error_descripcion);
    }
  } catch (\Exception $e) {
    form_set_error($e->getMessage());
  }

  $objectComunicacion = new stdClass();

  $objectComunicacion->id_comunicacion = $form_state['values']['idComunicacion'];
  $idImagenFid = $form_state['values']['idImagenFid'];
  $objectComunicacion->id_imagen = $form_state['values']['id_imagen'];

  // Si se carga la imagen
  if (!empty($idImagenFid)){
    try {
      $imagenBo = fgo_backend_guardar_imagen_drupal($idImagenFid, FGO_BACKEND_COD_TIPO_IMAGEN, $objectComunicacion->id_comunicacion);
      $objectComunicacion->id_imagen = $imagenBo->idImagen;

      \Fgo\Bo\ComunicacionImagenBo::eliminarPorIdComunicacion($form_state['values']['idComunicacion']);

      $relacionComunicacionImagen = new \Fgo\Bo\ComunicacionImagenBo();
      $relacionComunicacionImagen->construir($objectComunicacion);

      $relacionComunicacionImagen->guardar();
    } catch ( \Fgo\Bo\DatosInvalidosException $e) {
      $errores = $e->getErrores();
      foreach ($errores as $error_campo => $error_descripcion){
        form_set_error($error_campo, $error_descripcion);
      }
    } catch ( \Exception $e) {
      form_set_error($e->getMessage());
    }

  }else{

    $relacionComunicacionImagen = new \Fgo\Bo\ComunicacionImagenBo();
    $relacionComunicacionImagen->listarComunicacionComunicacionImagen($objectComunicacion->id_comunicacion);

    if (!empty($relacionComunicacionImagen->idComunicacionImagen)){
      $relacionComunicacionImagen->eliminar();
    }
  }
}

function theme_preview_img($variables) {
  $element = $variables['element'];

  $output = '';
  $output .= '<div class="image-widget form-managed-file clearfix">';

  if ($element['fid']['#value'] != 0) {
    $output .= '<div class="image-preview">';
    $output .= theme('image_style', array('style_name' => 'experience_full', 'path' => 'public://'.$element['#file']->filename, 'getsize' => FALSE));
    $output .= '</div>';
  }

  $output .= '<div class="image-widget-data">';

  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }

  // The remove button is already taken care of by rendering the rest of the form. No need to hack up some HTML!
  $output .= drupal_render_children($element);

  $output .= '</div>';
  $output .= '</div>';

  return $output;
}
