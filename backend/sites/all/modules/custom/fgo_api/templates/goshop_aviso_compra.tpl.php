<html>
<head>
</head>
   <body>
      <div class="mails-con-texto" style="background-color:#fff;  width:600px" bgcolor="#ffffff"  width="600">
         <div class="fill-1959" style="background-color:#072146; height:75px; width:600px" bgcolor="#072146" height="75" width="600">
         	<table style="height:100%" height="100%">
         		<tr>
         			<td class="group-5" style="height:28px; object-fit:contain; padding-left:238px; width:93px" height="28" width="93"><img src="<?=$datos["pathmailassets"]?>group-5.png"></td>
         		</tr>
         	</table>
         </div>
		 <div class="Rectangle2" style="background-color:#fff;  width:600px" bgcolor="#ffffff"  width="600">
         	<div class="Hola" style="color:#121212; font-family:Arial; font-size:20px; font-stretch:normal; font-style:normal; font-weight:bold; height:23px; letter-spacing:-0.2px; line-height:normal; padding-bottom:18px; padding-left:50px; padding-top:90px" height="23"> Hola <?=$datos['nombre']?>,
			</div> 
			<div class="Tu-compra-con-Pago" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:32.9px; letter-spacing:normal; line-height:normal; padding-left:50px; width:494px" height="32.9" width="494"> Tu compra a través de <b>BBVA Go</b> fue realizada con éxito.
			</div> 
			<div class="Resumen-de-tu-compra" style="color:#001b46; font-family:sans-serif; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:19px; letter-spacing:normal; line-height:normal; padding-bottom:13px; padding-left:50px; padding-top:24px; width:290px" height="19" width="290"> Resumen de tu compra
			</div>
			<div>
				<hr class="Line" style="border:solid 1px #001b46; width:500px" width="500">
			</div>
			<div class="Pods-realizar-pagos" style="padding-left:50px" >
				<table class="Tabla" style="height:100%; width:500px" height="100%" width="500">
                                    <tr>
                                            <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71; white-space: nowrap;" height="24">Referencia de compra</td>
                                            <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$datos['id_pedido']?></td>
                                    </tr>
                                    <tr>
                                            <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Comercio</td>
                                            <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$datos['seller']?></td>
                                    </tr>
                                    <?php foreach ($datos['items'] as $item): ?>
                                            <tr>
                                                    <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71; vertical-align:top" height="24">Producto</td>
                                                    <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$item['titulo_item']?></td>
                                            </tr>
                                            <tr>
                                                    <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Cantidad</td>
                                                    <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$item['product_quantity']?></td>
                                            </tr>
                                            <tr>
                                                    <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Importe</td>
                                                    <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right">$ <?=number_format( $item['price_final'],2,",",".") ?> </td>
                                            </tr>
                                        <?php endforeach; ?>
					
                                        <tr>
						<td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Costo Env&iacute;o</td>
						<td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"> $ <?=number_format($datos['importe_envio'],2,",",".")?></td>
					</tr>	
                                        <tr>
						<td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Intereses</td>
						<td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"> $ <?=number_format($datos['total_financing'],2,",",".")?></td>
					</tr>	
					<tr>
						<td colspan="2" class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">
						<hr class="Line" style="border:solid 1px #001b46; width:500px" width="500">
					</td>
					</tr>
					<tr>
						<td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Total de la compra</td>
						<td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right">$ <?=number_format(($datos['importe_productos']+$datos['total_financing']+$datos['importe_envio']),2,",",".")?></td>
					</tr>					
				</table>

			</div>
                     
                     <!-- Informacion del vendedor  -->
                        <div class="Resumen-de-tu-compra" style="color:#001b46; font-family:sans-serif; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:19px; letter-spacing:normal; line-height:normal; padding-bottom:0px; padding-left:50px; padding-top:32px; width:290px" height="19" width="290"> Información del vendedor
			</div>
			<div>
				<hr class="Line" style="border:solid 1px #001b46; width:500px" width="500">
			</div>
			<div class="Pods-realizar-pagos" style=" padding-left:50px" >
				<table class="Tabla" style=" width:500px"  width="500">
                                    
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Vendedor</td>
                                                <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$datos['seller']?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Mail</td>
                                                <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$datos['seller_email']?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Teléfono</td>
                                                <td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$datos['seller_phone']?> </td>
                                        </tr>
                                        
					<tr>
						<td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Dirección</td>
						<td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$datos['seller_address']?></td>
					</tr>
					<tr>
						<td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71; white-space: nowrap;" height="24">Horario de Atención</td>
						<td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"><?=$datos['seller_horario']?></td>
					</tr>
                                         <!-- formulario de consultas 
                                        <tr>
						<td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24">Formulario de consultas</td>
						<td class="Subtitle2" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:bold; height:24px; letter-spacing:normal; line-height:1.71; text-align:right" height="24" align="right"> <?=$datos['seller_email']?></td>
					</tr>	
                                        -->
					<tr>
						<td colspan="2" class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:20px; letter-spacing:normal; line-height:1.71" height="24">
						<hr class="Line" style="border:solid 1px #001b46; width:500px" width="500">
					</td>
					</tr>
					<tr>
						<td colspan="2" class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:20px; letter-spacing:normal; line-height:1.71" height="24">Ante cualquier consulta o reclamo con respecto a tu compra, por favor contactate con el vendedor correspondiente.</td>
					</tr>					
				</table>

			</div>
                     <!-- Metodo de envio -->
                     <br>
                     <div class="Resumen-de-tu-compra" style="color:#001b46; font-family:sans-serif; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:19px; letter-spacing:normal; line-height:normal; padding-bottom:0px; padding-left:50px; padding-top:32px; width:290px" height="19" width="290"> Método de envío
                     </div>
			<div>
				<hr class="Line" style="border:solid 1px #001b46; width:500px" width="500">
			</div>
			<div class="Pods-realizar-pagos" style="padding-left:50px">
				<table class="Tabla" style="width:500px"  width="500">
                                    <tr>
                                            <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['metodo_entrega'] ?></td>
                                    </tr>
                                    <tr>
                                            <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['tiempo_entrega'] ?></td>
                                    </tr>
                                     <?php if($datos['extra_data']!="") {?>
                                    <tr>
                                            <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['extra_data'] ?></td>
                                    </tr> 
                                     <?php } ?>
              
				</table>

                        </div>
                     <!-- direccion de entrega -->
                     <div class="Resumen-de-tu-compra" style="color:#001b46; font-family:sans-serif; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:19px; letter-spacing:normal; line-height:normal; padding-bottom:0px; padding-left:50px; padding-top:32px; width:290px" height="19" width="290"> Dirección de entrega
			</div>
			<div>
				<hr class="Line" style="border:solid 1px #001b46; width:500px" width="500">
			</div>
			<div class="Pods-realizar-pagos" style=" padding-left:50px" >
				<table class="Tabla" style=width:500px"  width="500">
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['nombre'] ?>  <?php echo $datos['apellido'] ?> </td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?=$datos['direccion_entrega']['address1']?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?=$datos['direccion_entrega']['address2']?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_entrega']['postcode']." ".$datos['direccion_entrega']['city'] ?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_entrega']['state'] ?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_entrega']['country'] ?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_entrega']['dni'] ?></td>
                                        </tr>
								
				</table>

			</div>
                     <!-- direccion de facturacion -->
                     <div class="Resumen-de-tu-compra" style="color:#001b46; font-family:sans-serif; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:19px; letter-spacing:normal; line-height:normal; padding-bottom:0px; padding-left:50px; padding-top:32px; width:290px" height="19" width="290"> Dirección de facturación
			</div>
			<div>
				<hr class="Line" style="border:solid 1px #001b46; width:500px" width="500">
			</div>
			<div class="Pods-realizar-pagos" style="padding-left:50px" >
				<table class="Tabla" style="width:500px"  width="500">
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['nombre'] ?>  <?php echo $datos['apellido'] ?> </td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?=$datos['direccion_facturacion']['address1']?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?=$datos['direccion_facturacion']['address2']?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_facturacion']['postcode']." ".$datos['direccion_facturacion']['city'] ?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_facturacion']['state'] ?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_facturacion']['country'] ?></td>
                                        </tr>
                                        <tr>
                                                <td class="Subtitle" style="color:#121212; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:24px; letter-spacing:normal; line-height:1.71" height="24"><?php echo $datos['direccion_facturacion']['dni'] ?></td>
                                        </tr>
									
				</table>

			</div>
		</div>
         </div>
              <div class="fill-1976" style="background-color:#fafafa; height:34px; width:599px;margin-top: 30px;" bgcolor="#014381" height="54" width="599">
         	<table style="height:100%" height="100%">
         		<tr>
         			<td class="email" style="height:17px; margin-top:19px; object-fit:contain; padding-left:59px; width:17px" height="17" width="17"></td>
        			
         		</tr>
         	</table>
         </div>
         <div class="rectangle" style="background-color:#237aba; height:154px; width:599px" bgcolor="#237aba" height="154" width="599">
         	<table style="height:50%" height="50%">
         		<tr>
         			<td class="sigamos-conectados" style="color:#fff; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:17px; letter-spacing:-0.5px; line-height:normal; padding-left:238px; padding-top:38px; width:130px" height="17" width="130">Sigamos conectados</td>
         		</tr>
         	</table>
         	<table style="height:50%" height="50%">
         		<tr>
         			<td class="group-2025" style="height:100%; padding-bottom:38px; padding-left:251px" height="100%"><a href="https://www.facebook.com/bbva.argentina/"><img src="<?=$datos["pathmailassets"]?>group-2025.png"></a></td>
         			<td class="group-2026" style="height:100%; padding-bottom:38px; padding-left:13px" height="100%"><a href="https://www.twitter.com/bbva_argentina?lang=es"><img src="<?=$datos["pathmailassets"]?>group-2064.png"></a></td>
         			<td class="group-2026" style="height:100%; padding-bottom:38px; padding-left:13px" height="100%"><a href="https://www.instagram.com/bbva_argentina/"><img class="filled" src="<?=$datos["pathmailassets"]?>filled.png" style="height:24px; object-fit:contain; width:24px" height="24" width="24"></a></td>
         		</tr>
         	</table>
         </div>
       
       
         <div class="fill-1976" style="background-color:#014381; height:54px; width:599px" bgcolor="#014381" height="54" width="599">
         	<table style="height:100%" height="100%">
         		<tr>
         			<td class="email" style="height:17px; margin-top:19px; object-fit:contain; padding-left:59px; width:17px" height="17" width="17"><a href="https://online.bbva.com.ar/fnet/mod/bbva/NL-contactenos.jsp"><img src="<?=$datos["pathmailassets"]?>email.png"></a></td>
         			<td class="correo-electronico" style="color:#fff; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:17px; letter-spacing:normal; line-height:normal; margin-top:19px; width:115px" height="17" width="115">Correo electrónico</td>
         			<td class="group-1982" style="height:18px; margin-top:19px; object-fit:contain; padding-left:237px; width:14px" height="18" width="14"><a href="https://buscador.bbva.com.ar/buscadordecajerosysucursales"><img src="<?=$datos["pathmailassets"]?>group-1982.png"></a></td>
         			<td class="sucursales" style="color:#fff; font-family:Arial; font-size:14px; font-stretch:normal; font-style:normal; font-weight:normal; height:17px; letter-spacing:normal; line-height:normal; margin-top:19px; width:70px" height="17" width="70">Sucursales</td>
         		</tr>
         	</table>
         </div>
         <div class="fill-2110" style="background-color:#072146; height:211px; width:599px" bgcolor="#072146" height="211" width="599">
         	<table style="height:100%" height="100%">
         		<tr>
         			<td class="bbva_tagline_esp_blanco_rgb" style="height:43px; object-fit:contain; padding-left:217px; padding-top:64px; width:166px" height="43" width="166"><img src="<?=$datos["pathmailassets"]?>bbva-tagline-esp-blanco-rgb.png"></td>
         		</tr>
         		<tr>
         			<td class="bbva-av-cordoba-11" style="color:#fff; font-family:Arial; font-size:11px; font-stretch:normal; font-style:normal; font-weight:normal; height:24.7px; letter-spacing:normal; line-height:normal; padding-left:181px; padding-top:38px; text-align:center; width:270px" height="24.7" align="center" width="270"><p>Banco BBVA Argentina S.A. - Av. Córdoba 111 Piso 31, Ciudad Autónoma de Buenos Aires, (C1054AAA), AR</p></td> 
         		</tr>
         	</table>
         </div>
	  
   </body>
</html>