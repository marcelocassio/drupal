<?php
$imagen = $_SERVER['HTTP_ORIGIN']."/fgo/static/media/logogo.1cb228c2.svg";
$comunicacionBo = fgo_api_comunication($comunicacion_id);
if ($comunicacionBo['code'] == 0){
  if ($comunicacionBo['data']['codigo_servicio'] == 0){
    $comunicacion = $comunicacionBo['data']['beneficio'];
    $titulo = $comunicacion->titulo;
    $url_actual = $comunicacion->compartir->link;
    $url_fake =  "https://".$_SERVER["HTTP_HOST"]."/fgo/comunicacion/".$comunicacion_id;
    if ($comunicacion->compartir->imagen){
      $imagen = $comunicacion->compartir->imagen[0]->url;
    }
    $descripcion = $comunicacion->compartir->texto;
  }
}

// Headers para redes sociales
// FACEBOOK
$element = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'og:image:secure_url',
        'content' =>  $imagen,
    ),
);

$element6 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'og:image:url',
        'content' =>  $imagen,
    ),
);

$element2 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'og:title',
        'content' =>  $titulo,
    ),
);
$element3 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'og:description',
        'content' => '#Go '. $descripcion,
    ),
);
$element4 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'og:type',
        'content' => 'website',
    ),
);

$element5 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'og:url',
        'content' => $url_fake,
    ),
);

drupal_add_html_head($element2, 'Test2');
drupal_add_html_head($element3, 'Test3');
drupal_add_html_head($element, 'Test');
drupal_add_html_head($element4, 'Test4');
drupal_add_html_head($element5, 'Test5');
drupal_add_html_head($element6, 'Test6');

// TWITTER

$elementtw = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'twitter:card',
        'content' =>  'summary_large_image',
    ),
);

$elementtw2 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'twitter:site',
        'content' =>  'Go',
    ),
);
$elementtw3 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'twitter:creator',
        'content' => '@Go',
    ),
);
$elementtw4 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'twitter:title',
        'content' => $titulo,
    ),
);

$elementtw5 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'twitter:description',
        'content' => $descripcion,
    ),
);

$elementtw6 = array(
    '#tag' => 'meta',
    '#attributes' => array(
        'property' => 'twitter:image',
        'content' =>  $imagen,
    ),
);

drupal_add_html_head($elementtw5, 'Testw5');
drupal_add_html_head($elementtw6, 'Testw6');
drupal_add_html_head($elementtw2, 'Testw2');
drupal_add_html_head($elementtw3, 'Testw3');
drupal_add_html_head($elementtw, 'Testw');
drupal_add_html_head($elementtw4, 'Testw4');

?>
<script type="text/javascript">
    var url = "<?php echo $url_actual; ?>";
    window.location = url;
</script>
