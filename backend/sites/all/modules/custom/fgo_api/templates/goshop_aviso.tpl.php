<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Mensaje desde Go Shop</title>
</head>
<body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
<table class="table table-mail" style="width:100%;margin-top:10px;-moz-box-shadow:0 0 5px #afafaf;-webkit-box-shadow:0 0 5px #afafaf;-o-box-shadow:0 0 5px #afafaf;box-shadow:0 0 5px #afafaf;filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5)">
    <tr>
        <td align="center" style="padding:7px 0">
            <table   class="table" bgcolor="#ffffff" style="width:100%">
                <tr>
                    <td align="center" class="logo" style="padding:15px 0 0 0" bgcolor="#004284">
                        <img src="<?php echo  file_create_url("static/media/goshop_logo.png");?>" alt="Go Shop" style="max-width:150px">
                    </td>
                </tr>
                <tr>
                    <td align="center" class="banner" style="padding:0 0 15px 0">
                        <div class="banner-content">
			<img src="<?php echo  file_create_url("static/media/goshop_bannerconfirmacion.png");?>" style="max-width: 650px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="titleblock" style="text-align: left; padding:7px 15px">
                        <font size="2" face="Open-sans, sans-serif" color="#333333">
                            <span class="title" style="font-weight:600;font-size:18px;line-height:33px">Hola, <?=$datos['nombre']?>.</span>
                            <span class="subtitle" style="font-weight:600;font-size:18px;line-height:25px">A continuaci&oacute;n podr&aacute;s visualizar los detalles de tu pedido.</span>
                        </font>
                    </td>
                </tr>

                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>

                <tr>
                    <td class="box" style="padding:7px 15px">
                        <table class="table" style="width:100%">
                            <tr>
                                <td style="border:1px solid #D6D4D4;padding:15px">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                            <span style="color:#777">
                                                <span style="color:#333; font-size: 18px">
                                                    <strong>Código del pedido:</strong>
                                                </span> <?=$datos['id_pedido']?>
                                            </span>
                                    </font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="padding:7px 15px">
                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                            <table class="table table-recap" bgcolor="#ffffff" style="width:100% !important; border-collapse:collapse">
                                <!-- Title -->
                                <tr>
                                    <th style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px; " width="1px"></th>

                                    <th style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px; " width="25%">Producto</th>

                                    <th style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="25%">Precio unitario</th>

                                    <th style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="25%">Cantidad</th>

                                    <th style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;" width="25%">Precio total</th>
                                </tr>
                                <tr><td colspan="4" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">
                                &nbsp;&nbsp;</td></tr>

                                <?php foreach ($datos['items'] as $item): ?>
                                  <tr>
                                    <td style="border:1px solid #d6d4d4">
                                      <table class="m_-6392238629930333010table"><tbody><tr><td width="10">&nbsp;</td><td><font size="2" face="Open-sans, sans-serif" color="#555454"></font></td><td width="10">&nbsp;</td></tr></tbody></table>
                                    </td>
                                    <td style="border:1px solid #d6d4d4">
                                      <table class="m_-6392238629930333010table"><tbody><tr><td width="10">&nbsp;</td><td> <font size="2" face="Open-sans, sans-serif" color="#555454"> <strong><?=$item['titulo_item']?></strong> </font></td><td width="10">&nbsp;</td></tr></tbody></table>
                                    </td>
                                    <td style="border:1px solid #d6d4d4">
                                      <table class="m_-6392238629930333010table"><tbody><tr><td width="10">&nbsp;</td><td align="right"> <font size="2" face="Open-sans, sans-serif" color="#555454"> $ <?=number_format( $item['price_final'],2,",",".") ?> </font></td><td width="10">&nbsp;</td></tr></tbody></table>
                                    </td>
                                    <td style="border:1px solid #d6d4d4">
                                      <table class="m_-6392238629930333010table"><tbody><tr><td width="10">&nbsp;
                                    </td>
                                    <td align="right">
                                      <font size="2" face="Open-sans, sans-serif" color="#555454"> <?=$item['product_quantity']?> </font></td><td width="10">&nbsp;</td></tr></tbody></table>
                                    </td>
                                    <td style="border:1px solid #d6d4d4">
                                      <table class="m_-6392238629930333010table"><tbody><tr><td width="10">&nbsp;</td><td align="right"> <font size="2" face="Open-sans, sans-serif" color="#555454"> $ <?=number_format($item['product_quantity']*$item['price_final'],2,",",".")?> </font></td><td width="10">&nbsp;</td></tr></tbody></table>
                                    </td>
                                  </tr>
                                <?php endforeach; ?>

                                <tr><td colspan="4" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">
                                &nbsp;&nbsp;</td></tr>
                                <tr class="conf_body">
                                    <td colspan="3" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        <strong>Precio</strong>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        $ <?=number_format($datos['importe_productos'],2,",",".")?>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td colspan="3" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        <strong>Costo Env&iacute;o</strong>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td colspan="3" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        $ <?=number_format($datos['importe_envio'],2,",",".")?>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr class="conf_body">
                                    <td colspan="3" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        <strong>Intereses</strong>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td colspan="3" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        $ <?=number_format($datos['total_financing'],2,",",".")?>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="conf_body">
                                    <td colspan="3" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        <strong>Pago total</strong>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td colspan="3" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="4" face="Open-sans, sans-serif" color="#555454">
                                                        $ <?=number_format(($datos['importe_productos']+$datos['total_financing']+$datos['importe_envio']),2,",",".")?>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="box" style="padding:7px 15px">
                        <table class="table" style="width:100%">
                            <tr>
                                <td style="padding:7px 0">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                        <p style="color: #bf252d; margin:3px 0 7px;font-weight:600;font-size:18px;padding-bottom:10px" data-html-only="1">Informaci&oacute;n del Vendedor</p>
                                        <span style="color:#757575; font-size: 18px;">
                                            <span>Vendedor: </span> <?=$datos['seller']?><br/>
                                            <span>Mail: </span> <?=$datos['seller_email']?> <br/>
                                            <span>Tel&eacute;fono: </span> <?=$datos['seller_phone']?> <br/>
                                            <span>Direcci&oacute;n: </span> <?=$datos['seller_address']?> <br/>
                                            <span>Horario de atenci&oacute;n: </span> <?=$datos['seller_horario']?> <br/><br/>
                                            <span>Ante cualquier consulta o reclamo con respecto a tu compra, por favor contactate con el vendedor correspondiente.</span>
                                            </span>
                                    </font>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
              
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                <tr>
                    <td class="box" style="padding:7px 15px">
                        <table class="table" style="width:100%">
                            <tr>
                                <td style="padding:7px 0">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                        <p style="color: #bf252d; margin:3px 0 7px;font-weight:600;font-size:18px;padding-bottom:10px" data-html-only="1">Forma de entrega seleccionada </p>
                                        <span style="color:#757575; font-size: 18px;">
                                            <?=$datos['metodo_entrega']?><br/><br>
                                            <?=$datos['tiempo_entrega']?> <br/><br/>
                                           <?php if($datos['extra_data']!="") {
                                               echo $datos['extra_data']."<br/><br/>";
                                           } ?>
                                            <!--{deliveryWindow}-->
                                            </span>
                                    </font>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                <tr>
                    <td class="box" style="padding:7px 15px">
                        <table class="table" style="width:100%">
                            <tr>
                                <td style="padding:7px 0">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                        <p style="color: #bf252d; margin:3px 0 7px;font-weight:600;font-size:18px;padding-bottom:10px" data-html-only="1">Informaci&oacute;n registrada en tu cuenta</p>
                                    </font>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="delivery-row">
                    <td style="padding:7px 15px">
                        <table class="table" style="width:100%">
                            <tr>
                                <td class="box address" width="310" style="border:1px solid #D6D4D4;padding:7px 0">
                                    <table class="table" style="width:100%">
                                        <tr>

                                            <td style="padding:7px 15px">
                                                <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                    <p data-html-only="1" style="margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:16px;padding-bottom:10px">
                                                        Dirección de entrega </p>
                                                    <span data-html-only="1" style="color:#777">
                                                      <span style="font-weight:bold"> <?php echo $datos['nombre'] ?> </span> 
                                                      <span style="font-weight:bold"> <?php echo $datos['apellido'] ?> </span>
                                                      <br><?php echo $datos['direccion_entrega']['address1'] ?>
                                                      <br><?php echo $datos['direccion_entrega']['address2'] ?>
                                                      <br><?php echo $datos['direccion_entrega']['postcode']." ".$datos['direccion_entrega']['city'] ?>
                                                      <br><?php echo $datos['direccion_entrega']['state'] ?>
                                                      <br><?php echo $datos['direccion_entrega']['country'] ?>
                                                      <br><?php echo $datos['direccion_entrega']['dni'] ?>
                                                    </span>
                                                </font>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                                <td width="15" class="space_address" style="padding:7px 0">&nbsp;</td>
                                <td class="box address" width="310" style="border:1px solid #D6D4D4;padding:7px 0">
                                    <table class="table" style="width:100%">
                                        <tr>

                                            <td style="padding:7px 15px">
                                                <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                    <p data-html-only="1" style="margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:16px;padding-bottom:10px">
                                                        Dirección de facturación </p>
                                                    <span data-html-only="1" style="color:#777">
                                                      <span style="font-weight:bold"><?php echo $datos['nombre']?> </span> 
                                                      <span style="font-weight:bold"><?php echo $datos['apellido']?> </span>
                                                      <br><?php echo $datos['direccion_facturacion']['address1'] ?>
                                                      <br><?php echo $datos['direccion_facturacion']['address2'] ?>
                                                      <br><?php echo $datos['direccion_facturacion']['postcode']." ".$datos['direccion_entrega']['city'] ?>
                                                      <br><?php echo $datos['direccion_facturacion']['state'] ?>
                                                      <br><?php echo $datos['direccion_facturacion']['country'] ?>
                                                      <br><?php echo $datos['direccion_facturacion']['dni'] ?>
                                                    </span>
                                                </font>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                <tr>
                    <td class="footer" style="padding:15px 15px;text-align: right;">
                        <span><a href="<?=$datos['url_go_shop']?>" style="color:#337ff1">Go Shop</a></span>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
</table>
</body>

</html>