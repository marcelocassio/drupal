<?php
/*
 * Variables Tipos de Call To Actions
 * 1 = Beneficio
 * 2 = Campaña Beneficios
 * 3 = Sorteos
 * 4 = Link Interno
 * 7 = Link Externo
 * 5 = Secciones Generales
*/

namespace Fgo\Services\Dto;

class CTA {
  const CTA_SECCION_ID_COMUNICACION = 1;
  const CTA_SECCION_ID_CAMPANIA = 2;
  const CTA_SECCION_ID_SORTEO = 3;
  const CTA_SECCION_ID_LINK = 4;
  const CTA_SECCION_ID_GENERAL = 5;
  const CTA_SECCION_ID_WALLET = 6;
  const CTA_SECCION_ID_LINK_EXTERNO = 7;
  const CTA_SECCION_ID_MARKET_PLACE = 8;
  const CTA_SECCION_ID_WALLET_NFC = 9;
  const CTA_SECCION_ID_WALLET_SUBE = 10;

  private $_data;
  private $_seccion;
  private $_direccion;
  public $_seleccionado;
  public $_ctaIdEdit; // Se usa en caso de que se vaya a modificar un CTA previamente cargado

  public function __construct($data=NULL, $parametros=NULL) {
    $this->_data = $data;
    $this->_seccion = (isset($parametros["seccion"])) ? $parametros["seccion"] : null;
    $this->_direccion = (isset($parametros["direccion"])) ? $parametros["direccion"] : null;
    $this->_ctaIdEdit = (isset($parametros["ctaIdEdit"])) ? $parametros["ctaIdEdit"] : null;
  }

  public function dataCTA() {
    $resultado = new \stdClass();
    $resultado->secciones = $this->listaSecciones();
    $resultado->direcciones = $this->listaDirecciones();
    return $resultado;
  }

  public function encontrarCTA() {
    if (empty($this->_seccion) && empty($this->_direccion)){ return; }

    $direccion = null;
    $direcciones = (array) $this->_data;

    foreach ($direcciones as $key => $value){
      if ($key == $this->_seccion){
        foreach ($value as $direccion_temporal){
          $direccion = $this->_buscarCTA($direccion_temporal);
          if ($direccion){ break; }
        }
      }
    }

    $this->_seleccionado = $this->_formatoDireccion($direccion, $this->_seccion);
  }

  public function listaSecciones() {
    $ct_secciones = array();
    $ct_secciones[0] = '-Ninguno-';
    $ctaParam1Bo = new \Fgo\Bo\CtaParam1Bo();
    foreach( $ctaParam1Bo->listar() AS $item ) {
      $ct_secciones[$item->idParam1] = $item->nombre;  
    }

    return $ct_secciones;
  }

  public function listaSeccionesNav() {
    $ct_secciones = array();

    $ct_secciones[0] = '-Ninguno-';
    $ct_secciones[self::CTA_SECCION_ID_COMUNICACION] = 'communication';
    $ct_secciones[self::CTA_SECCION_ID_CAMPANIA] = 'campaign';
    $ct_secciones[self::CTA_SECCION_ID_SORTEO] = 'raffle';
    $ct_secciones[self::CTA_SECCION_ID_LINK] = 'internalURL';
    $ct_secciones[self::CTA_SECCION_ID_LINK_EXTERNO] = 'externalURL';
    $ct_secciones[self::CTA_SECCION_ID_GENERAL] = 'menu';
    $ct_secciones[self::CTA_SECCION_ID_WALLET] = 'wallet';
    $ct_secciones[self::CTA_SECCION_ID_MARKET_PLACE] = 'marketPlace';
    $ct_secciones[self::CTA_SECCION_ID_WALLET_NFC] = 'wallet';
    $ct_secciones[self::CTA_SECCION_ID_WALLET_SUBE] = 'wallet';

    return $ct_secciones;
  }

  public function listaDirecciones() {
    $campanias = new \Fgo\Bo\CampaniaBo();

    $comunicaciones = fgo_backend_api_lista_comunicaciones_cta();

    $resultados = array();
    $resultados[0] = null;
    $resultados[self::CTA_SECCION_ID_COMUNICACION] = $this->_formatoComunicaciones($comunicaciones["data"]);
    $resultados[self::CTA_SECCION_ID_CAMPANIA] = $this->_formatoCampanias($campanias->listarBasico());
    $resultados[self::CTA_SECCION_ID_SORTEO] = $this->formatoSorteos();
    $resultados[self::CTA_SECCION_ID_GENERAL] = $this->seccionesGenerales();

    return $resultados;
  }

  public function seccionesGenerales() {
    $resultado = array();
    $resultado["geo"]  = "Descuentos Cercanos";
    $resultado["login"]  = "Login";
    $resultado["registro"]  = "Registro";
    $resultado["perfil"]  = "Perfil";
    $resultado["solicitarTarjeta"]  = "Solicitar tarjeta";
    $resultado["communications"]  = "Comunicaciones";
    $resultado["contacto"]  = "Contacto";
    $resultado["cuponera"]  = "Cuponera";
    $resultado["raffles"]  = "Sorteos";
    $resultado["wallet"]  = "Mis tarjetas";

    return $resultado;
  }

  public function formatoSorteos() {
    $resultado = array();
    $sorteos = bbva_sorteos_vigentes();

    foreach ($sorteos as $sorteo){
      $sorteo_api = new \stdClass();
      $sorteo_api->id = $sorteo->id;
      $sorteo_api->titulo = $sorteo->titulo;
      $sorteo_api->fecha_vigencia_desde = $this->_formatoFecha(strtotime($sorteo->fecha_vigencia_desde));
      $sorteo_api->fecha_vigencia_hasta = $this->_formatoFecha(strtotime($sorteo->fecha_vigencia_hasta));

      $resultado[] = $sorteo_api;
    }

    return $resultado;
  }

  public function obtenerNav1($cta) {
    
    $ctaParam1Bo = new \Fgo\Bo\CtaParam1Bo($cta->param1);
    return $ctaParam1Bo->call_to_action . $cta->param2;
    
  }
  
  public function obtenerNav($cta) {

    $nav = array();
    $secciones = $this->listaSeccionesNav();
    
    if($cta->param1) {
      $nav[] = $secciones[$cta->param1];
      if($cta->param2) {
        switch ($cta->param1){
          case self::CTA_SECCION_ID_COMUNICACION:
          case self::CTA_SECCION_ID_SORTEO:
            $nav[] = "id=" . $cta->param2;
            break;
          case self::CTA_SECCION_ID_CAMPANIA:
            $nav[] = "id_campaign=" . $cta->param2;
            break;
          case self::CTA_SECCION_ID_LINK:
          case self::CTA_SECCION_ID_LINK_EXTERNO:
          case self::CTA_SECCION_ID_MARKET_PLACE:
            $nav[] = "url=" . $cta->param2;
            break;
          default:
            $nav[] = $cta->param2;
            break;
        }
      }
    }

    if(!empty($nav)) {
      return "nav://" . implode("/", $nav);
    }

    return NULL;
  }


  public function ctaForm($direcciones){
    $cta_seccion = $this->_seccion;
    $cta_direccion_s = $this->_direccion;

    $cta_secciones = $this->listaSecciones();
    $cta_direcciones = $direcciones;
    $seleccionado = $this->_seleccionado;

    if(!is_array($cta_direcciones)){
      $cta_direcciones = (array) $cta_direcciones;
    }
    foreach ($cta_direcciones as $key => $value){
      if ($key == 5){
        $cta_opciones_menu = (array) $value;
      }
    }

    global $base_url;
    global $conf;

    // fix para prod buscador
    if($base_url == "https://www.francesgo.com.ar/fgo"){
      if($conf['env'] == 'desa'){
        $base_url = 'https://qa.bbvafrances.com.ar/fgo';
      }
      if($conf['env'] == 'prod'){
        $base_url = 'https://go.bbva.com.ar/fgo';
      }
    }
    if ($cta_seccion == 1 || $cta_seccion == 2 || $cta_seccion == 3 ){

      //En caso de que no sea link, ni Login Tarjetas
      if (isset($seleccionado->id)){
        $cta_direccion_s = $seleccionado->titulo." (".$seleccionado->id.")";
      }else{
        $cta_direccion_s = "Sin Vigencia (0)";
      }
    }

    $form = array();

    $form['grupo_deeplinking'] = array(
        '#type' => 'fieldset',
        '#title' => t('<i>Deep Linking</i>'),
    );

    $form['grupo_deeplinking']['idCta'] = array(
      '#type' => 'hidden',
      '#default_value' => !empty($this->_ctaIdEdit) ? $this->_ctaIdEdit : NULL
    );

    $form['grupo_deeplinking']['callToActionSeccion'] = array(
      '#title' => t('Sección'),
      '#type' => 'select',
      '#options' => $cta_secciones,
      '#default_value' => $cta_seccion,
    );

    $ctaParam1Bo = new \Fgo\Bo\CtaParam1Bo;
    foreach( $ctaParam1Bo->listar() AS $row ){
      $form['grupo_deeplinking']['callToActionDireccion_' . $row->idParam1] = array(
        '#title' => t($row->nombre),
        '#type' => 'textfield',
        // '#autocomplete_path' => 'admin/autocomplete-cta/1',
        '#default_value' => $cta_direccion_s,
        '#states' => array(
          'visible' => array(
            ':input[name="callToActionSeccion"]' => array(
                array('value' => $row->idParam1),
            ),
          ),
        )
      );  
        
    }
    /* CTA DIRECCIONES */
    $form['grupo_deeplinking']['callToActionDireccion_1'] = array(
      '#title' => t('Comunicación'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'admin/autocomplete-cta/1',
      '#default_value' => $cta_direccion_s,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
              array('value' => 1),
          ),
        ),
      )
    );


    $form['grupo_deeplinking']['callToActionDireccion_2'] = array(
      '#title' => t('Campaña'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'admin/autocomplete-cta/2',
      '#default_value' => $cta_direccion_s,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
            array('value' => 2),
          ),
        ),
      )
    );

    $form['grupo_deeplinking']['callToActionDireccion_3'] = array(
      '#title' => t('Sorteo'),
      '#type' => 'textfield',
      '#autocomplete_path' => 'admin/autocomplete-cta/3',
      '#default_value' => $cta_direccion_s,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
            array('value' => 3),
          ),
        ),
      )
    );

    $form['grupo_deeplinking']['callToActionDireccion_4'] = array(
      '#title' => t('Link Interno (WebView)'),
      '#type' => 'textfield',
      '#default_value' => $cta_direccion_s,
      '#size' => 25,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
            array('value' => 4),
          ),
        ),
      )
    );

    $form['grupo_deeplinking']['callToActionDireccion_5'] = array(
      '#title' => t('General'),
      '#type' => 'select',
      '#default_value' => $cta_direccion_s,
      '#options' => $cta_opciones_menu,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
            array('value' => 5),
          ),
        )
      )
    );

    $form['grupo_deeplinking']['callToActionDireccion_7'] = array(
      '#title' => t('Link Externo (Navegador)'),
      '#type' => 'textfield',
      '#default_value' => $cta_direccion_s,
      '#size' => 25,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
              array('value' => 7),
          ),
        ),
      )
    );

    $form['grupo_deeplinking']['callToActionDireccion_6'] = array(
      '#title' => t('Wallet parametro (wallet, NFC, P2P=1 (Modo), SUBE=1, QR=1, latamPass=1)'),
      '#type' => 'textfield',
      '#default_value' => $cta_direccion_s,//''
      '#size' => 25,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
              array('value' => 6),
          ),
        ),
      )
    );

    $form['grupo_deeplinking']['callToActionDireccion_8'] = array(
      '#title' => t('Url Shop'),
      '#type' => 'textfield',
      '#default_value' => 'https://shop.bbva.com.ar/login/callback',
      '#size' => 25,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
              array('value' => 8),
          ),
        ),
      )
    );

    $form['grupo_deeplinking']['callToActionDireccion_9'] = array(
      '#title' => t('Link Externo (Navegador)'),
      '#type' => 'hidden',
      '#default_value' => 'NFC',
      '#size' => 3,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
              array('value' => 7),
          ),
        ),
      )
    );

     $form['grupo_deeplinking']['callToActionDireccion_10'] = array(
      '#title' => t(''),
      '#type' => 'hidden',
      '#default_value' => 'SUBE=1',
      '#size' => 4,
      '#states' => array(
        'visible' => array(
          ':input[name="callToActionSeccion"]' => array(
              array('value' => 7),
          ),
        ),
      )
    );
     
    
    return $form;
  }

  private function _formatoDireccion($direccion, $seccion) {

    if ($seccion != self::CTA_SECCION_ID_COMUNICACION || $seccion != self::CTA_SECCION_ID_CAMPANIA || $seccion != self::CTA_SECCION_ID_SORTEO){
      $resultado = $direccion;
      return $resultado;
    }

    $resultado = new \stdClass();

    $resultado->id = $direccion->id;
    $resultado->titulo = $direccion->titulo;
    $resultado->fecha_vigencia_desde = $direccion->fecha_vigencia_desde;
    $resultado->fecha_vigencia_hasta = $direccion->fecha_vigencia_hasta;
    if (isset($direccion->id_suip)){
      $resultado->id_suip = $direccion->id_suip;
    }

    return $resultado;
  }

  private function _formatoComunicaciones($comunicaciones) {
    $comunicaciones_formateadas = array();

    foreach ($comunicaciones as $comunicacion){
      $comunicacion_api = new \stdClass();
      $comunicacion_api->id = $comunicacion->idComunicacion;
      $comunicacion_api->id_suip = $comunicacion->idBeneficioSuip;
      $comunicacion_api->titulo = $comunicacion->titulo;
      $comunicacion_api->fecha_vigencia_desde = $this->_formatoFecha($comunicacion->fechaDesde);
      $comunicacion_api->fecha_vigencia_hasta = $this->_formatoFecha($comunicacion->fechaHasta);

      $comunicaciones_formateadas[] = $comunicacion_api;
    }

    return $comunicaciones_formateadas;
  }

  private function _formatoCampanias($campanias) {
    $campanias_formateadas = array();
    $start_date = strtotime(date('Y-m-d'));
    $fecha_15_dias = strtotime("-15 days", $start_date);

    foreach ($campanias as $campania){
      $campania_api = new \stdClass();
      $campania_api->id = $campania->idCampaniaSuip;
      $campania_api->id_suip = $campania->idCampaniaSuip;
      $campania_api->titulo = $campania->nombre;
      $campania_api->fecha_vigencia_desde = $this->_formatoFecha($campania->fechaDesde);
      $campania_api->fecha_vigencia_hasta = $this->_formatoFecha($campania->fechaHasta);
      if ($campania->fechaDesde > $fecha_15_dias || $campania->fechaHasta > $start_date){
        $campanias_formateadas[] = $campania_api;
      }
    }

    return $campanias_formateadas;
  }

  private function _buscarCTA($data) {
    $resultado = null;
    switch ($this->_seccion){
      case self::CTA_SECCION_ID_COMUNICACION:
      case self::CTA_SECCION_ID_CAMPANIA:
      case self::CTA_SECCION_ID_SORTEO:
        if ($data->id == $this->_direccion){
          $resultado = $data;
        }
        break;
      default:
        $resultado = $this->_direccion;
        break;
    }

    return $resultado;
  }

  private function _formatoFecha($date){
    if ($date && $date!=null){
      return date('d-m-Y', $date);
    }
    return null;
  }

  
}