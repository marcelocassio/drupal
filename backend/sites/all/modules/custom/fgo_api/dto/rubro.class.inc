<?php

namespace Fgo\Services\Dto;


class Rubro {

  public $id;
  public $idSuip;
  public $nombre;
  public $subrubros;

  public function __construct($rubroBo) {
    $options = \Fgo\Bo\RubroBo::estadosPdm();
    $this->id = $rubroBo->idRubro;
    $this->idSuip = $rubroBo->idRubroSuip;
    $this->nombre = $rubroBo->nombre;
    $this->estadoPdm = $options[$rubroBo->estadoPdm];
    $this->subrubros = array();
  }

  public function agregarHijo($rubroBo) {
    if (isset($rubroBo)) {
      $this->subrubros[$rubroBo->idRubroSuip] = new Rubro($rubroBo);
    }
  }

  public static function contruirEnJerarquia($listadoRubros) {
    $rubrosPadres = array();
    foreach ($listadoRubros as $rubroBo) {
      if ($rubroBo->esPadre()) {
        $rubrosPadres[$rubroBo->idRubroSuip] = new \Fgo\Services\Dto\Rubro($rubroBo);
      }
    }

    foreach ($listadoRubros as $rubroBo) {
      if (!$rubroBo->esPadre()) {
        if (isset($rubrosPadres[$rubroBo->rubroPadre->idRubroSuip])) {
          $rubrosPadres[$rubroBo->rubroPadre->idRubroSuip]->agregarHijo($rubroBo);
        }
      }
    }
    return $rubrosPadres;
  }
}