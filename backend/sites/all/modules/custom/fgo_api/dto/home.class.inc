<?php

namespace Fgo\Services\Dto;


class Home {

  const TEXTO_BIENVENIDA = 'Tenemos los mejores beneficios para vos';

  private $_slideFechas;
  private $_shortcuts;

  public function __construct($slideFechas, $shortcuts = array()) {
    $this->_slideFechas = $slideFechas;
    $this->_shortcuts = $shortcuts;
  }
  
  public function setCarruseles($carruseles)
  {
      $this->_carruseles = $carruseles;
  }

  public function lista($slideFechas = false) {

    $response = array();
    $slides = array();

    if(!$slideFechas){

      $slideDefault = new \Fgo\Bo\SlideBo();

      foreach ($slideDefault->listar() as $key => $slide) {
        if($slide->publicado == '3'){

          $slideDto = new \stdClass();
          $slideDto->image = formatUri($slide->imagenApp->uri);
          $slideDto->image_web = formatUri($slide->imagenWeb->uri);
          $cta = new \Fgo\Services\Dto\CTA();
          $slideDto->call_to_action = $cta->obtenerNav($slide->cta);
          $slides[] = $slideDto;
       }
      }

      if(isProd()){
        hd('Se esta utilizando un Slide Home Default.');
      }
    }
    else{
      foreach ($this->_slideFechas as $slideFecha) {
        if($slideFecha->slide->publicado == 1) {
          $slideDto = new \stdClass();
          $slideDto->image = formatUri($slideFecha->slide->imagenApp->uri);
          $slideDto->image_web = formatUri($slideFecha->slide->imagenWeb->uri);
          $cta = new \Fgo\Services\Dto\CTA();
          $slideDto->call_to_action = $cta->obtenerNav($slideFecha->slide->cta);
          $slides[] = $slideDto;
        }
      }
    }

    $shortcuts = array();
    foreach ($this->_shortcuts as $shortcut) {
      if($shortcut->visible) {
        $shortcutDto = new \stdClass();
        $shortcutDto->name = $shortcut->nombre;
        $shortcutDto->type = $shortcut->tipo;
        $cta = new \Fgo\Services\Dto\CTA();
        $shortcutDto->call_to_action = $cta->obtenerNav($shortcut->cta);
        $shortcuts[] = $shortcutDto;
      }
    }

    $response['slides'] = $slides;
    $response['shortcuts'] = $shortcuts;
    $response['mensajes'] = $this->_mensajes();
    $response['carruseles'] = $this->_carruseles;

    return $response;
  }

  public function listaBackend() {
    $result = array();
    foreach ($this->_slideFechas as $slideFecha) {
      if($slideFecha->slide->publicado == 1) {
        $slideDto = new \stdClass();
        $slideDto->idSlideFecha = $slideFecha->idSlideFecha;
        $slideDto->imageApp = formatUri($slideFecha->slide->imagenApp->uri);
        $slideDto->imageWeb = formatUri($slideFecha->slide->imagenWeb->uri);
        $slideDto->fechaDesde = tst($slideFecha->slide->fechaDesde);
        $slideDto->fechaHasta = tst($slideFecha->slide->fechaHasta);
        $slideDto->orden = $slideFecha->orden;
        $result[] = $slideDto;
      }
    }

    $response = array("slides" => $result);

    return $response;
  }

  private function _mensajes() {
    global $user;

    $name = "";
    if($user->uid > 0) {
      $name = " @name";
    }
    $titulo1 = "Buen día" . $name;
    $titulo2 = "Buenas tardes" . $name;
    $titulo3 = "Buenas noches" . $name;

    $response = array();
    //Mañana
    $response[] = array(
      "titulo" => $titulo1,
      "subtitulo" => self::TEXTO_BIENVENIDA,
      "vigencia" => array(
        "desde" => 6,
        "hasta" => 13,
      )
    );
    $response[] = array(
      "titulo" => $titulo2,
      "subtitulo" => self::TEXTO_BIENVENIDA,
      "vigencia" => array(
        "desde" => 13,
        "hasta" => 20,
      )
    );
    $response[] = array(
      "titulo" => $titulo3,
      "subtitulo" => self::TEXTO_BIENVENIDA,
      "vigencia" => array(
        "desde" => 20,
        "hasta" => 6,
      )
    );

    return $response;
  }
}