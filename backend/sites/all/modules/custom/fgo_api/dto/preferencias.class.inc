<?php

namespace Fgo\Services\Dto;

class Preferencias {

  public $_data;

  public function __construct() {
    global $base_url;
    $result = array();
    $result['config']['url_portal_comercios'] = variable_get('bbva_login_fgo_comercio_url', 'https://go.bbva.com.ar/fgo/comercios/#/');
    $result['config']['rubros'] = $this->_rubros();
    $result['config']['zonas'] = $this->_zonas();
    $result['config']['filtros'] = $this->_filtros();
    $result['config']['refresh_time'] = 5;
    $result['personas']['tipo_documentos'] = $this->_lista_documentos();
    $result['personas']['telefonia']['codigos_area'] = $this->_codigos_area();
    $result['personas']['telefonia']['companias'] = $this->_companias_telefonicas();
    $result['personas']['nacionalidades'] = $this->_nacionalidades();
    $result['registro']['edad_minima'] = 16;
    $result['registro']['baja'] = $base_url . '/profile/baja';
    $result['url_olvido_clave'] = variable_get('bbva_login_recover_cliente_url', variable_get('fnet.host.web').'/fnetcore/#/firstAccess//recoverUser');
    $result['url_nueva_clave'] = variable_get('bbva_login_new_cliente_url', variable_get('fnet.host.web').'/fnetcore/#/firstAccess//firstAccess');
    $result['opinator'] = $this->_opinator();
    $result['urlsolicitarTarjeta'] = variable_get('preferences_urlSolicitarTarjeta');
    $result['urlContactenos'] = variable_get('preferences_urlContactenos');
    $this->_data = $result;

    return $this->_data;
  }

  public function getData() {
    return $this->_data;
  }

  private function _lista_documentos() {
    $doc_type_altamira_list = array();
    $doc_type_altamira = bbva_login_get_document_altamira_type_list_complete();
    foreach ($doc_type_altamira as $key => $value) {
      $nombre = $key;
      if ($value == "27" || $value == "28") {
        continue;
      } else if ($value == "30") {
        $nombre = "Pasaporte";
      }

      $doc_type_altamira_list[] = array(
        'id' => $key ,
        'nombre' => $nombre,
      );
    }

    return $doc_type_altamira_list;
  }

  private function _codigos_area() {
    $areas = get_code_area_type();
    foreach ($areas as $key => $value) {
      $area[] = array(
        'id' => $key,
        'nombre' => $value,
      );
    }
    return $area;
  }

  private function _companias_telefonicas() {
    $companias_telefonicas = array(
      array(
        'id' => '0001',
        'nombre' => 'Movistar'),
      array(
        'id' => '0002',
        'nombre' => 'Claro'),
      array(
        'id' => '0003',
        'nombre' => 'Personal'),
      array(
        'id' => '0004',
        'nombre' => 'Nextel'),
    );

    return $companias_telefonicas;
  }

  private function _rubros() {

    $rubrosBo = new \Fgo\Bo\RubroBo();
    $rubros = $rubrosBo->listar();
    
    $result = array();
    foreach ($rubros as $rubro) {
      if($rubro->rubroPadre != NULL) {
        continue;
      }

      if($rubro->idRubroSuip == 165){
        $rubro->idRubroSuip = 170;
      }
      
      $result[] = array(
        "id" => $rubro->idRubroSuip,
        "nombre" => $rubro->nombre
      );
    }
    return $result;
  }

  private function _zonas() {
    $zones = _bbva_subscription_zones();
    $provinces = _bbva_subscription_provinces();

    $zonas = array_merge($zones, $provinces);

    foreach ($zonas as $key => $value) {
      $zona[] = array(
        'id' => $key,
        'nombre' => $value,
      );
      if ($key=='PROV-1') {
        array_unshift($zona, array_pop($zona));
      }
    }

    return $zona;
  }

  private function _nacionalidades() {
    $nacionalidades_list = bbva_subscription_get_nationality_type_list();
    $nacionalidades = array();
    foreach ($nacionalidades_list as $key => $value) {
      $nacionalidades[] = array(
        'id' => (string)$key,
        'nombre' => $value,
      );
    }

    return $nacionalidades;
  }

  private function _filtros() {
    $response = array();
    
    // Rubros
    $response[] = array(
      "nombre" => "Rubros",
      "parametro" => "rubros",
      "valores" => $this->_rubros()
    );

    // Provincias
    $provinciasBo = new \Fgo\Bo\ProvinciaBo();
    $provincias = $provinciasBo->listar();
    $result = array();
    foreach ($provincias as $provincia) {
      $result[] = array(
        "id" => $provincia->idProvincia,
        "nombre" => $provincia->nombre
      );
    }
    $response[] = array(
      "nombre" => "Ubicación",
      "parametro" => "provincias",
      "valores" => $result
    );

    // Puntos de venta
    $response[] = array(
      "nombre" => "Punto de Venta",
      "parametro" => "canales",
      "valores" => array(
        array("id" => "1", "nombre" => "Locales"),
        array("id" => "2", "nombre" => "Venta Online"),
        array("id" => "3", "nombre" => "Venta Telefónica"),
      )
    );

    return $response;
  }

  private function _opinator() {
    $opinator = new \stdClass();
    $opinator->opinator_url = variable_get('bbva_services_optinator_url', NULL);
    $opinator->saturation_url = variable_get('bbva_services_optinator_saturation_url', NULL);
    $opinator_names_values = explode("\n", variable_get('bbva_services_optinator_names', null));
    $opinator_names = null;
    foreach ($opinator_names_values as $opinator_name) {
      $row = explode("::", $opinator_name);
      if(!isset($row[1])) {
        continue;
      }
      $opinator_names[] = array(
        "type" => trim($row[0]),
        "name" => trim($row[1]),
        "url" => trim($row[2])
      );
    }
    $opinator->opinator_names = $opinator_names;

    return $opinator;
  }
}
