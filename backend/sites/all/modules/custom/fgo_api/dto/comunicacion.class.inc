<?php

namespace Fgo\Services\Dto;

class Comunicacion {
  const TEXT_REQUISITOS_CUPON = 'Presentando el cupón de descuento';
  const TEXT_REQUISITOS_TARJETA = 'Pagando con Tarjeta de Crédito BBVA';
  const TEXT_REQUISITOS_GO = 'Para registrados en Go';
  const TEXT_TOTAL_COMPRA = 'en el total de tu compra';
  const TEXT_CUOTAS = 'cuotas';

  const TEXT_DESCUENTO_PUNTO_VENTA = "El beneficio se aplicará en el punto de venta";
  const TEXT_DESCUENTO_EN_RESUMEN = "El beneficio se aplicará según lo detallado en las bases y condiciones";

  // Tipos de beneficio
  const TIPO_BENEFICIO_CARTERA = 1;
  const TIPO_BENEFICIO_GO = 2;
  const TIPO_BENEFICIO_CUPON = 3;

  private $_base_url;
  private $_comunicacion;
  private $_tipos_beneficios = array(
  	self::TIPO_BENEFICIO_CARTERA => "cartera", 
	self::TIPO_BENEFICIO_GO => "go", 
	self::TIPO_BENEFICIO_CUPON => "cupon"
  );
  private $_text_aplicacion_descuento = array(
    1 => self::TEXT_DESCUENTO_EN_RESUMEN,
    2 => self::TEXT_DESCUENTO_EN_RESUMEN,
    3 => self::TEXT_DESCUENTO_PUNTO_VENTA
  );

  private $_requisitos = array(
    1 => "Pagando con Tarjeta de Crédito BBVA",
    2 => "Para registrados en Go",
    3 => "Presentando el cupón de descuento"
  );

  public function __construct($comunicacion) {
    global $base_url;
    $this->_base_url = $base_url;
    $this->_comunicacion = $comunicacion;
  }

  public function lista() {
    $result = new \stdClass();
    $result->id = $this->_comunicacion->idComunicacion;
    $result->id_suip = $this->_comunicacion->idBeneficioSuip;
    $result->titulo = $this->_comunicacion->titulo;
    $result->fecha_vigencia_desde = $this->_comunicacion->fechaDesde;
    $result->fecha_vigencia_hasta = $this->_comunicacion->fechaHasta;
    $result->subtitulo = !empty($this->_comunicacion->descripcionCorta) ? $this->_comunicacion->descripcionCorta : self::TEXT_TOTAL_COMPRA;
    $result->destacado = ($this->_comunicacion->destacado==1) ? true : false;
    $result->beneficios = $this->_beneficios();
    $result->imagen = $this->_imagenes();
    $result->compartir = $this->_compartir();
    $result->campania = $this->_accionComercial();
    return $result;
  }

  public function listaSimilares() {
    $result = new \stdClass();
    $result->id = $this->_comunicacion->idComunicacion;
    $result->id_suip = $this->_comunicacion->idBeneficioSuip;
    $result->titulo = $this->_comunicacion->titulo;
    $result->destacado = ( $this->_comunicacion->destacado==1 ) ? true : false;
    $result->beneficios = $this->_beneficios();
    $result->imagen = $this->_imagenes();
    $result->campania = $this->_accionComercial();
    return $result;
  }

  public function listaCarrusel() {
    $result = new \stdClass();
    $result->image = $this->_imagenes(true);
    if($result->image == null ){
       $result->image = file_create_url("static/media/go_logo.png"); 
    }
        
    $result->titulo = $this->_comunicacion->titulo;

    $result->beneficio = $this->getCarruselBeneficioValor($this->_beneficios()); 
    
    $result->call_to_action = 'nav://communication/id=' . $this->_comunicacion->idComunicacion;
    return $result;
  }

  public function listaSucursalesCercanas() {
    $result = new \stdClass();
    $result->id = $this->_comunicacion->idComunicacion;
    $result->titulo = $this->_comunicacion->titulo;
    $result->es_shopping = false;
    if(isset($this->_comunicacion->comercio)){
        $result->es_shopping = ($this->_comunicacion->comercio->sucursalFisica != null) ? true : false;
    }
    $result->sucursales_cercanas = $this->_sucursales();
    return $result;
  }

  public function detalle() {
    $result = null;
    $codigoServicio = 0;
    if ($this->_comunicacion) {
      $result = new \stdClass();
      $result->id = $this->_comunicacion->idComunicacion;
      $result->id_suip = $this->_comunicacion->idBeneficioSuip;
      $result->titulo = $this->_comunicacion->titulo;
      $result->subtitulo = !empty($this->_comunicacion->descripcionCorta) ? $this->_comunicacion->descripcionCorta : self::TEXT_TOTAL_COMPRA;
      $result->destacado = ($this->_comunicacion->destacado==1)?true:false;
      $result->beneficios = $this->_beneficios();
      $result->vigencia = $this->_vigencia();
      $result->canales_venta = $this->_canalesVenta();
      $result->como_uso = $this->_comoUso();
      $result->bases_condiciones = $this->_comunicacion->terminosCondiciones;
      $result->bio = isset($this->_comunicacion->comercio) ? $this->_comunicacion->comercio->avvio : null;
      $result->rubros = $this->_rubros();
      $result->cftna = $this->_comunicacion->cft;
      $result->imagen = $this->_imagenes();
      $result->compartir = $this->_compartir();
      $result->beneficios_similares = $this->_comunicacionesSimilares();
      $result->campania = $this->_accionComercial();
    } else {
      $codigoServicio = 1;
    }

    $result = array("codigo_servicio" => $codigoServicio, "beneficio" => $result);

    return $result;
  }

  public function cache() {
    $result = new \stdClass();
    $result->idComunicacion = $this->_comunicacion->idComunicacion;
    $result->idBeneficioSuip = $this->_comunicacion->idBeneficioSuip;
    $result->titulo = $this->_comunicacion->titulo;
    $result->comercio = $this->_comunicacion->comercio;
    $result->descripcionCorta = $this->_comunicacion->descripcionCorta;
    $result->descripcionExtendida = $this->_comunicacion->descripcionExtendida;
    $result->destacado = $this->_comunicacion->destacado;
    $result->beneficios = $this->_comunicacion->beneficios;
    $result->fechaDesde = $this->_comunicacion->fechaDesde;
    $result->fechaHasta = $this->_comunicacion->fechaHasta;
    $result->accionComercial = $this->_comunicacion->accionComercial;
    $result->terminosCondiciones = $this->_comunicacion->terminosCondiciones;
    $result->imagenes = $this->_comunicacion->imagenes;
    $result->rubros = $this->_comunicacion->rubros;
    $result->sucursalesFisicas = $this->_comunicacion->sucursalesFisicas;
    $result->sucursalesVirtuales = $this->_comunicacion->sucursalesVirtuales;
    $result->tags = $this->_comunicacion->tags;
    $result->comoUso = $this->_comunicacion->comoUso;

    return $result;
  }

  public function lista_chatbot() {
    $result = new \stdClass();

    $result->id = $this->_comunicacion->idComunicacion;
    $result->title = $this->_comunicacion->titulo;
    
    $result->mostrar_cupon = false;
    $result->mostrar_tarjeta = false;
    $result->mostrar_tarjeta_go = false;

    $result->cuota_tarjeta = 0;
    $result->porcentaje_tarjeta = 0;
    if($this->_comunicacion->beneficios[0]->tipoBeneficio == TIPO_BENEFICIO_CARTERA) {
      $result->cuota_tarjeta = $this->_comunicacion->beneficios[0]->cuota;
      $result->porcentaje_tarjeta = $this->_comunicacion->beneficios[0]->valor;
      $result->mostrar_tarjeta = true;
    }

    $result->cuota_tarjeta_go = 0;
    $result->porcentaje_tarjeta_go = 0;
    if($this->_comunicacion->beneficios[0]->tipoBeneficio == TIPO_BENEFICIO_GO) {
      $result->cuota_tarjeta_go = $this->_comunicacion->beneficios[0]->cuota;
      $result->porcentaje_tarjeta_go = $this->_comunicacion->beneficios[0]->valor;
      $result->mostrar_tarjeta_go = true;
    }

    $result->porcentaje_cupon = 0;
    if($this->_comunicacion->beneficios[0]->tipoBeneficio == TIPO_BENEFICIO_CUPON) {
      $result->mostrar_cupon = true;
      $result->porcentaje_cupon = $this->_comunicacion->beneficios[0]->valor;
    }

    $result->locales_fisicos = false;
    if ($this->_comunicacion->comercio->sucursalFisica != null || count($this->_comunicacion->sucursalesFisicas) > 0) {
      $result->locales_fisicos = true;
    }
    
    $imagenes = $this->_imagenes();
    $result->image = $imagenes;
    $result->image_web = $imagenes;
    
    $result->destacado = ($this->_comunicacion->destacado==1) ? true : false;
    $result->rubros = $this->_rubrosChatbot();
    $result->sucursales = $this->_sucursalesChatbot();
    $result->share_url = $this->_base_url . '/#/comunicacion/' . $this->_comunicacion->idComunicacion;

    return $result;
  }

  public function lista_backend() {
    $result = new \stdClass();
    $result->comercio = null;
    $result->fechaDesde = null;
    $result->fechaHasta = null;
    $result->backendImagenesB = null;
    $result->backendImagenesC = null;
    $result->backendImagenesL = null;
    $result->botones = null;
    $result->legales = null;
    $result->cuota = null;
    $result->descuento = null;
    $result->casuistica = null;
    $result->publicado = (int) $this->_comunicacion->publicado;
    $result->siempreVisible = (int) $this->_comunicacion->siempreVisible;

    $result->idComunicacion = $this->_comunicacion->idComunicacion;
    $result->idBeneficioSuip = $this->_comunicacion->idBeneficioSuip;
    $result->titulo = $this->_comunicacion->titulo;
    $result->descripcionExtendida = $this->_comunicacion->descripcionExtendida;
    $result->nombreCampania = $this->_comunicacion->nombreCampania;
    $result->imagenes = $this->_comunicacion->imagenes;

    $result->legales = $this->_comunicacion->terminosCondiciones;

    if (isset($result->beneficios) && count($this->_comunicacion->beneficios) > 0) {
      $beneficio = $this->_comunicacion->beneficios[0];
      $result->cuota = $beneficio->cuota;
      $result->descuento = $beneficio->valor;
      if (isset($beneficio->casuistica)) {
        $result->casuistica = $beneficio->casuistica->descripcion;
      }
    }

    if (isset($this->_comunicacion->comercio->imagenes)){
      $result->comercio = $this->_comunicacion->comercio->imagenes;
    }
    if (!empty($this->_comunicacion->fechaDesde)){
      $result->fechaDesde = date("d-m-Y",$this->_comunicacion->fechaDesde);
    }
    if (!empty($this->_comunicacion->fechaHasta)){
      $result->fechaHasta = date("d-m-Y",$this->_comunicacion->fechaHasta);
    }

    return $result;
  }


  private function _beneficios() {
    $beneficios = array();
    foreach ($this->_comunicacion->beneficios as $beneficio) {
      $beneficio_dto = new \stdClass();
      $beneficio_dto->tipo = $this->_tipos_beneficios[$beneficio->tipoBeneficio];
      $beneficio_dto->cuando = $this->_vigencia();
      if (!empty($beneficio->tipoAplicacionBeneficio)) {
        $beneficio_dto->cuando = $beneficio->tipoAplicacionBeneficio->nombre;
      }
      $beneficio_dto->valor = !empty($beneficio->valor) ? $beneficio->valor : "";
      $beneficio_dto->cuota = !empty($beneficio->cuota) ? $beneficio->cuota : 0;
      $beneficio_dto->tope = !empty($beneficio->tope) ? $beneficio->tope : null;
      $beneficio_dto->casuistica->descripcion = isset($beneficio->casuistica->descripcion) ? $beneficio->casuistica->descripcion : null;
      /** FIX ANDROID */
      if(isset($beneficio->casuistica->descripcion)) {
        $beneficio_dto->valor = "";
        $beneficio_dto->cuota = 0;
      }
      $beneficio_dto->condicion = !empty($this->_comunicacion->descripcionCorta) ? $this->_comunicacion->descripcionCorta : self::TEXT_TOTAL_COMPRA;
      $beneficio_dto->texto_aplicacion = $this->_text_aplicacion_descuento[$beneficio->tipoBeneficio];

      $beneficio_dto->requisitos[] = $this->_requisitos[$beneficio->tipoBeneficio];
      if(!empty($this->_comunicacion->descripcionExtendida)) {
        $beneficio_dto->requisitos = array($this->_comunicacion->descripcionExtendida);
      }

      $beneficios[] = $beneficio_dto;
    }
    return $beneficios;
  }

  private function _resumen($beneficio) {
    $resumen = "";

    if ($beneficio->casuistica) {
      $resumen .= $beneficio->valor;
    } else {
      if($beneficio->valor > 0) {
        $resumen .= $beneficio->valor . "%";
        if($beneficio->cuota > 1) {
          $resumen .= " y ";
        }
      }
      if($beneficio->cuota > 1) {
        $resumen .= $beneficio->cuota . " " . self::TEXT_CUOTAS;
      }
    }
    return $resumen;
  }

  private function _textoGrande($beneficio) {
    $texto_grande = null;

    if ($beneficio->casuistica) {
      $texto_grande .= $beneficio->valor;
    } else {
      if($beneficio->valor > 0) {
        $texto_grande = $beneficio->valor . "%";
      } elseif($beneficio->cuota > 1) {
        $texto_grande = $beneficio->cuota . " " . self::TEXT_CUOTAS;
      }
    }
    return $texto_grande;
  }

  private function _textoChico($beneficio) {
    $texto_chico = null;
    if ($beneficio->casuistica) {
      $texto_chico .= "";
    } else {
      if($beneficio->valor > 0 && $beneficio->cuota > 1) {
        $texto_chico = " y " . $beneficio->cuota . " " . self::TEXT_CUOTAS;
      }
    }
    return $texto_chico;
  }

  private function _texto($beneficio) {
    $texto = null;

    if ($beneficio->casuistica) {
      $texto .= $beneficio->valor;
    } else {
      if($beneficio->valor > 0) {
        $texto .= $beneficio->valor . "%";
        if($beneficio->cuota > 1) {
          $texto .= " y ";
        }
      }
      if($beneficio->cuota > 1) {
        $texto .= $beneficio->cuota . " " . self::TEXT_CUOTAS;
      }
    }

    if($texto) {
      $texto .= " " . t(self::TEXT_TOTAL_COMPRA);
    }

    return $texto;
  }

  private function _imagenes($usarRubros = false) {
    $url = null;
    $imagen = null;
    
    if (!empty($this->_comunicacion->imagenes)) {
      $imagen = $this->_comunicacion->imagenes[0];
    } else {
      if (isset($this->_comunicacion->comercio)) {
        if (!empty($this->_comunicacion->comercio->imagenes)) {
          $imagen = $this->_comunicacion->comercio->imagenes[0];
        }
      }
    }
    
    if ($imagen === null){
      if($usarRubros){
        foreach( $this->_comunicacion->rubros as $rubroObject ){
          if( isset( $rubroObject->imagenes ) ) {
            $imagen = $rubroObject->imagenes->data[0];
            break;
          }
        }
      }
    }
    
    if (isset($imagen->datosImagen->uri)){
      $url = file_create_url($imagen->datosImagen->uri);
    }

    return $url;
  }

  private function _compartir() {
    $compartir = new \stdClass();
    $compartir->link = $this->_base_url . '/#/comunicacion/' . $this->_comunicacion->idComunicacion;
    $compartir->texto = $this->_comunicacion->descripcionExtendida;
    $compartir->imagen = $this->_imagenes();

    return $compartir;
  }

  private function _accionComercial() {
    $accionComercial = null;
    if (isset($this->_comunicacion->accionComercial)) {
      $accionComercial = new \stdClass();
      $accionComercial->id = $this->_comunicacion->accionComercial->idAccionComercial;
      $accionComercial->descripcion = $this->_comunicacion->accionComercial->nombre;
    }
    return $accionComercial;
  }

  private function _vigencia() {
    $vigencia = "Del ";
    $vigencia .= date('d/m/Y', $this->_comunicacion->fechaDesde);
    $vigencia .= " al ";
    $vigencia .= date('d/m/Y', $this->_comunicacion->fechaHasta);

    return $vigencia;
  }

  private function _canalesVenta() {
    $canales_venta = new \stdClass();
    $canales_venta->marcas_adheridas_shopping = $this->_localesAdheridos();
    $canales_venta->zonas = $this->_zonasAdheridas();
    $canales_venta->sucursales = $this->_sucursales();
    $canales_venta->web = $this->_web();
    $canales_venta->telefono = $this->_telefono();

    return $canales_venta;
  }

  private function _localesAdheridos() {
    $nombreLocalesAdheridos = null;
    if (isset($this->_comunicacion->comercio->sucursalFisica) && $this->_comunicacion->comercio->sucursalFisica != null)  {
      $nombreLocalesAdheridos = array();
      if(count($this->_comunicacion->sucursalesFisicas) > 0) {
        foreach ($this->_comunicacion->sucursalesFisicas as $sucursal) {
          $nombreLocalesAdheridos[] = $sucursal->nombre;
        }
      }
    }
    return $nombreLocalesAdheridos;
  }

  private function _zonasAdheridas() {
    $zonas = ($this->_comunicacion->zonasAdheridas!=null)?explode(",",$this->_comunicacion->zonasAdheridas):null;
    if (isset($zonas)) {
      $i=0;
      for ($i=0; $i<count($zonas); $i++) {
        $zonas[$i] =trim($zonas[$i]);
      }
    }
    return $zonas;
  }

  private function _sucursales() {
    $sucursales = array();
    
    if (isset($this->_comunicacion->comercio->sucursalFisica) && $this->_comunicacion->comercio->sucursalFisica != null) {
      $sucursal = $this->_comunicacion->comercio->sucursalFisica;
      $sucursal_dto = new \stdClass();
      $sucursal_dto->direccion = $sucursal->calle . " " . $sucursal->calleNumero;
      $sucursal_dto->localidad = $sucursal->localidad;
      $sucursal_dto->latitude = $sucursal->latitud;
      $sucursal_dto->longitude = $sucursal->longitud;
      $sucursales[] = $sucursal_dto;
    } else {
      if (count($this->_comunicacion->sucursalesFisicas) > 0) {
        foreach ($this->_comunicacion->sucursalesFisicas as $sucursal) {
          $sucursal_dto = new \stdClass();
          $sucursal_dto->direccion = $sucursal->calle . " " . $sucursal->calleNumero;
          $sucursal_dto->localidad = $sucursal->localidad;
          $sucursal_dto->latitude = $sucursal->latitud;
          $sucursal_dto->longitude = $sucursal->longitud;
          $sucursales[] = $sucursal_dto;
        }
      }
    }
    return $sucursales;
  }

  private function _sucursalesChatbot() {
    $sucursales = array();
    
    if ($this->_comunicacion->comercio->sucursalFisica != null) {
      $sucursal = $this->_comunicacion->comercio->sucursalFisica;
      $sucursal_dto = new \stdClass();
      $sucursal_dto->street = $sucursal->calle . " " . $sucursal->calleNumero;
      $sucursal_dto->city = $sucursal->localidad;
      $sucursal_dto->latitude = $sucursal->latitud;
      $sucursal_dto->longitude = $sucursal->longitud;
      $sucursales[] = $sucursal_dto;
    } else {
      if (count($this->_comunicacion->sucursalesFisicas) > 0) {
        foreach ($this->_comunicacion->sucursalesFisicas as $sucursal) {
          $sucursal_dto = new \stdClass();
          $sucursal_dto->street = $sucursal->calle . " " . $sucursal->calleNumero;
          $sucursal_dto->city = $sucursal->localidad;
          $sucursal_dto->latitude = $sucursal->latitud;
          $sucursal_dto->longitude = $sucursal->longitud;
          $sucursales[] = $sucursal_dto;
        }
      }
    }
    return $sucursales;
  }

  private function _web() {
    $webs = array();
    if(count($this->_comunicacion->sucursalesVirtuales) > 0) {
      foreach ($this->_comunicacion->sucursalesVirtuales as $sucursalVirtual) {
        if(!empty($sucursalVirtual->web)) {
          $web_dto = new \stdClass();
          $web_dto->name = $this->_comunicacion->titulo;
          $web_dto->url = $sucursalVirtual->web;
          $webs[] = $web_dto;
        }
      }
    }
    return $webs;
  }

  private function _telefono() {
    $telefono = null;
    if(count($this->_comunicacion->sucursalesVirtuales) > 0) {
      foreach ($this->_comunicacion->sucursalesVirtuales as $sucursalVirtual) {
        if(!empty($sucursalVirtual->tlf)) {
          $telefono = $sucursalVirtual->tlf;
          break;
        }
      }
    }
    return $telefono;
  }

  private function _comoUso() {
    if(($this->_comunicacion->comoUso)) {
      $como_uso = explode("\r\n", $this->_comunicacion->comoUso);
      return $como_uso;
    }

    $sucursal = count($this->_comunicacion->sucursalesFisicas) > 0;
    $web = false;
    $telefono = false;
    if(count($this->_comunicacion->sucursalesVirtuales) > 0) {
      foreach ($this->_comunicacion->sucursalesVirtuales as $sucursalVirtual) {
        if(!empty($sucursalVirtual->web)) {
          $web = true;
        }
        if(!empty($sucursalVirtual->tlf) && !$telefono) {
          $telefono = $sucursalVirtual->tlf;
        }
      }
    }

    $como_uso = array();
    if($this->_comunicacion->beneficios[0]->tipoBeneficio == 3) {
      if($sucursal) {
        $como_uso[] = "Mostralo desde la pantalla de tu móvil en cualquier local adherido.";
      }
      if($web) {
        $como_uso[] = "Ingresá a la web y cargá el código de descuento al momento de la compra.";
      }
      if($telefono) {
        $como_uso[] = "Llamá al ".$telefono." y al momento de la compra mencioná el código de descuento al operador.";
      }

      if(count($como_uso) > 1) {
        array_unshift($como_uso, "Elegí una de las siguientes opciones:");
        $como_uso = array(implode("<br><br> - ", $como_uso));
      }
    } else {
      if($sucursal) {
        $como_uso[] = "Acercate a cualquier local adherido.";
      }
      if($web) {
        $como_uso[] = "Ingresá a la web.";
      }
      if($telefono) {
        $como_uso[] = "Llamá al ".$telefono.".";
      }

        /* BENEFICIO TIPO RUBRO */
      if (!$sucursal && !$web && !$telefono)  {
        $como_uso[] = "Acercate a cualquier local adherido.";
      }

      if(count($como_uso) > 1) {
        array_unshift($como_uso, "Elegí una de las siguientes opciones:");
        $como_uso = array(implode("<br><br> - ", $como_uso));
      }

      $como_uso[] = "Pagá con tu tarjeta de crédito BBVA.";

    }
    return $como_uso;
  }

  private function _rubros() {
    $rubros = array();
    foreach($this->_comunicacion->rubros as $rubro) {
      $rubro_dto = new \stdClass();
      $rubro_dto->nombre = $rubro->nombre;
      $rubro_dto->id = $rubro->idRubroSuip;
      $rubros[] = $rubro_dto;
    }

    return $rubros;
  }

  private function _rubrosChatbot() {
    $rubros = array();
    foreach($this->_comunicacion->rubros as $rubro) {
      $rubros[] = $rubro->idRubro;
    }

    return array_unique($rubros);
  }

  private function _comunicacionesSimilares() {
    $similares = array();
    foreach ($this->_comunicacion->comunicacionesSimilares as $comunicacionSimilar) {
      $comunicacionDto = new Comunicacion($comunicacionSimilar);
      $similares[] = $comunicacionDto->listaSimilares();
    }
    return $similares;
  }
  
  /**
   * Devuelve el valor del texto beneficio para la nueva home (Carruseles)
   * @param type $beneficios
   * @return string
   */
  protected function getCarruselBeneficioValor($beneficios){
        if(!isset($beneficios[0])){
            return "";
        }

        $unBeneficio = $beneficios[0];
        if($unBeneficio->valor != ""){
            if($unBeneficio->cuota > 0){
                return $unBeneficio->valor."% y ".$unBeneficio->cuota." cuotas";//return "10% y 12 cuotas";
            } 
            return $unBeneficio->valor."%"; //"10%";
        }
        if($unBeneficio->cuota > 0){
            return $unBeneficio->cuota." cuotas";
        }
        if( isset($unBeneficio->casuistica->descripcion)){
            return $unBeneficio->casuistica->descripcion;
        }
        return "";
  }

}
