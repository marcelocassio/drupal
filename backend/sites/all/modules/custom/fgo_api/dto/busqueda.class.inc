<?php

namespace Fgo\Services\Dto;

class Busqueda {

  private $_busquedaNombres;
  private $_busquedaIdRubros;
  private $_palabra;

  public function __construct($busquedaNombres, $busquedaIdRubros) {
    $this->_busquedaNombres = $busquedaNombres;
    $this->_busquedaIdRubros = $busquedaIdRubros;
  }

  public function resultadoBusqueda($palabra) {
    $this->_palabra = $palabra;
    $result = $this->_resultado();

    return $result;
  }

  private function _resultado() {
    $tags_coincidentes = array();
    // Ahora se devuelve el resultado
    foreach( $this->_busquedaNombres as $idRubro => $nombre ) {
      if( strpos(bbva_encode($nombre), bbva_encode($this->_palabra) ) !== false) {
        $tags_coincidentes[] = array(
            "call_to_action" => $this->_obtenerCallToAction($idRubro),
            "name"=> $this->_parsearNombre($nombre)
        );
      }
    }

    return $tags_coincidentes;
  }

  private function _obtenerCallToAction($tid) {
    $cta = null;
    switch ($this->_busquedaIdRubros[$tid]){
      case "beneficio":
        $cta = "nav://benefitList/search_tid=".$tid;
        break;
      case "sorteo":
        $cta = "nav://sorteoList/search_tid=".$tid;
        break;
    }
    return $cta;
  }

  private function _parsearNombre($string) {
    $parser_string = null;
    if (!empty($string)) {
      $parser_string = mb_ucwords(mb_strtolower($string));
    }
    return $parser_string;
  }

}