<?php

namespace Fgo\Services;

use Fgo\Services\Dto\Comunicacion;

class ApiBackend extends Api {
  const Q_RESULTADOS = 20;
  const MENSAJE_COMUNICACION_NOENCONTRADA = "La comunicacion no existe";

  /**
   * Exporta un archivo csv con las comunicaciones indicadas en el parametro ids del request
   */
  public function exportarBackendComunicaciones() {

    $idComunicaciones = explode(",",  $this->_parametros['ids']);
    $comunicaciones = array();
    foreach ( $idComunicaciones as $id) {
      $comunicacion = \Fgo\Bo\ComunicacionBo::buscarComunicacionListado($id);
      if ($comunicacion->idComunicacion != null) {
        $comunicaciones[] = $comunicacion;
      }
    }

    $this->_exportar_comunicaciones($comunicaciones);

  }

  public function obtenerBackendComunicaciones() {
    $fechaDesde = null;

    if ($this->_parametros['ate']) {
      $fechaDesde = date('Y-m-d', strtotime("-1 year"));
      $response = \Fgo\Bo\ComunicacionBo::buscarHistoricoBackend($fechaDesde);
    } else {
      $fechaHastaInicial = date('Y-m-d', strtotime("-1 months"));
      $response = \Fgo\Bo\ComunicacionBo::buscarVigentesBackend($fechaHastaInicial);
    }

    $comunicaciones = array();
    if(count($response) > 0) {
      foreach ($response as $comunicacion) {
        $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
        $comunicaciones[] = $comunicacion_dto->lista_backend();
      }
    }

    return $this->_output($comunicaciones);
  }

  public function obtenerBackendComunicacion($idComunicacion) {
    $detalle = NULL;
    if (is_numeric($idComunicacion)) {
      $comunicacionBo = new \Fgo\Bo\ComunicacionBo($idComunicacion);
      if ($comunicacionBo->idComunicacion != null) {
       $comunicacionDto = new Comunicacion($comunicacionBo);
        $detalle = $comunicacionDto->detalle();
      } else {
        return $this->_output($detalle, self::MENSAJE_COMUNICACION_NOENCONTRADA);
      }
    }
    return $this->_output($detalle);
  }

  public function obtenerBackendComunicacionesCta() {
    $response = \Fgo\Bo\ComunicacionBo::buscarDisponiblesDesdeHoy();
    return $this->_output($response);
  }

  public function obtenerBackendSlidesLista() {
    $slides = new \Fgo\Bo\SlideBo();
    $this->_data = $slides->listar(array("fecha_hasta"=>"ASC"));
    $response = $this->_dataEspecificaBackendSlidesLista();

    return $this->_output($response);
  }

  public function obtenerBackendSlidesOrdenar($fecha) {
    $slides = new \Fgo\Bo\SlideFechaBo();
    $slideFechas = $slides->buscarPorFecha($fecha);

    $dto = new \Fgo\Services\Dto\Home($slideFechas);
    $response = $dto->listaBackend();

    return $this->_output($response);
  }

  public function guardarBackendSlidesOrdenar($carousel, $stock) {

    $carouselArray = explode(",", $carousel);
    $stockArray = explode(",", $stock);

    try {
      $contadorCarousel = 1;
      foreach ($carouselArray as $itemC) {
        if ($itemC == 0){ continue; }
        $slide = new \Fgo\Bo\SlideFechaBo($itemC);
        $slide->orden = $contadorCarousel;
        $slide->guardar();
        $contadorCarousel++;
      }

      foreach ($stockArray as $itemS) {
        if ($itemS == 0){ continue; }
        $slide = new \Fgo\Bo\SlideFechaBo($itemS);
        $slide->orden = null;
        $slide->guardar();
      }

      borrarCache('home');

      return $this->_output('OK');

    } catch (Exception $e) {
      return $this->_output('ERROR');
    }
  }

  public function borrarBackendSlide($idSlide) {

    try {
      $slide = new \Fgo\Bo\SlideBo($idSlide);
      if ($slide->idSlide){
        $slide->eliminar();

        // Borro el cache de archivo del Home
        $cache = \BBVA\Cache\CacheFile::getInstance();
        $cache->clean(array('client/home'));

        return $this->_output("OK");
      }else{
        return $this->_output("No se consigue Slide");
      }
    } catch (Exception $e) {
      return $this->_output("ERROR");
    }
  }

  public function obtenerDataCallToAction() {
    $response = new \stdClass();

    $datos = $this->_obtenerDataCTA();

    $response->datos->direcciones = isset($datos->direcciones)?$datos->direcciones:null;
    $dto = new \Fgo\Services\Dto\CTA(isset($datos->direcciones)?$datos->direcciones:null, $this->_parametros);
    $dto->encontrarCTA();
    $response->formulario = $dto->ctaForm($datos->direcciones);

    return $this->_output($response);
  }

  public function obtenerCampania($id) {
    $campania = $this->_obtenerDataCampania($id);

    $comunicaciones = array();
    if(count($campania->comunicaciones) > 0) {
      foreach ($campania->comunicaciones as $comunicacion) {
        $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
        $comunicaciones[] = $comunicacion_dto->lista();
      }
    }

    return $this->_output($comunicaciones);
  }

  public function obtenerCampanias() {
    $campanias = $this->_obtenerCampanias();

    return $this->_output($campanias);
  }

  private function _obtenerCampanias() {
    $this->_data = null;
    // Busco en caché
    $cache_nombre = self::NOMBRE_CACHE_GENERAL;
    if($this->_usar_cache) {
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_BE_CAMPANIA, $cache_nombre);
    }

    // Si no encontré nada en caché
    if(!$this->_data) {
      $campaniaBo = new \Fgo\Bo\CampaniaBo();
      $this->_data = $campaniaBo->listarBasico(array("id_campania"=>"DESC"));

      if ($this->_data) {
        // Guardo el general en caché
        $this->_generarCache(self::TIPO_CACHE_BE_CAMPANIA, $cache_nombre, $this->_data);
      }
    }

    return $this->_data;
  }

  private function _obtenerDataCampania($id) {
    $this->_data = null;
    // Busco en caché
    if($this->_usar_cache) {
      $cache_nombre = self::NOMBRE_CACHE_CAMPANIA . "_" . (int) $id;
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_BE_CAMPANIA, $cache_nombre);

      if(!$this->_data) {
        $campaniaEncontrada = \Fgo\Bo\CampaniaBo::buscarPorIdSuip($id);
        $this->_data = $campaniaEncontrada;

        if ($this->_data) {
          $this->_generarCache(self::TIPO_CACHE_BE_CAMPANIA, $cache_nombre, $this->_data);
        }
      }
    }

    return $this->_data;
  }

  private function _obtenerDataCTA() {
    $this->_data = null;
    // Busco en caché
    $cache_nombre = self::NOMBRE_CACHE_GENERAL;
    if($this->_usar_cache) {
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_BE_CTA, $cache_nombre);
    }

    // Si no encontré nada en caché
    if(!$this->_data) {
      $cta_dto = new \Fgo\Services\Dto\CTA();
      $this->_data = $cta_dto->dataCTA();

      if ($this->_data) {
        // Guardo el general en caché
        $this->_generarCache(self::TIPO_CACHE_BE_CTA, $cache_nombre, $this->_data);
      }
    }

    return $this->_data;
  }

  protected function _setearParametros($parametros) {
    $_parametros = array();

    $_parametros['pagina'] = 0;
    if(isset($parametros['pager'])) {
      $_parametros['pagina'] = (int) $parametros['pager'];
    }

    $_parametros['rubros'] = array();
    if(isset($parametros['rubros'])) {
      // Guardo los rubros en un array ordenado numericamente
      $_parametros['rubros'] = explode(",", $parametros['rubros']);
      sort($_parametros['rubros'], SORT_NUMERIC);
    }

    $_parametros['campania'] = 0;
    if(isset($parametros['campaign'])) {
      $_parametros['campania'] = $parametros['campaign'];
    }

    $_parametros['destacado'] = false;
    if(isset($parametros['destacado'])) {
      $_parametros['destacado'] = "s";
      if($parametros['destacado'] == "n") {
        $_parametros['destacado'] = "n";
      }
    }

    $_parametros['provincias'] = array();
    if(isset($parametros['provincias'])) {
      $_parametros['provincias'] = explode(",", $parametros['provincias']);
      sort($_parametros['provincias'], SORT_NUMERIC);
    }

    $_parametros['canal_fisico'] = false;
    $_parametros['canal_online'] = false;
    $_parametros['canal_telefonico'] = false;
    if(isset($parametros['canales'])) {
      $canales = explode(",", $parametros['canales']);
      if(in_array("1", $canales)) {
        $_parametros['canal_fisico'] = true;
      }
      if(in_array("2", $canales)) {
        $_parametros['canal_online'] = true;
      }
      if(in_array("3", $canales)) {
        $_parametros['canal_telefonico'] = true;
      }
    }

    if(isset($parametros['lat']) && isset($parametros['long']) && isset($parametros['radio'])) {
      $_parametros['lat'] = $parametros['lat'];
      $_parametros['long'] = $parametros['long'];
      $_parametros['radio'] = $parametros['radio'];
    }

    if(isset($parametros['seccion']) && isset($parametros['direccion'])) {
      $_parametros['seccion'] = $parametros['seccion'];
      $_parametros['direccion'] = $parametros['direccion'];
      $_parametros['ctaIdEdit'] = $parametros['ctaIdEdit'];
    }

    $_parametros['tags'] = array();
    if (isset($parametros['tags'])) {
      $_parametros['tags'] = explode(",", $parametros['tags']);
      sort($_parametros['tags'], SORT_NUMERIC);
    }

    $_parametros['tag'] = "";
    if(isset($parametros['tag'])) {
      $_parametros['tag'] = $parametros['tag'];
    }

    $_parametros['ids'] = "";
    if(isset($parametros['ids'])) {
      $_parametros['ids'] = $parametros['ids'];
    }

    $_parametros['no_cliente'] = "";
    if(isset($parametros['no_cliente'])) {
      $_parametros['no_cliente'] = $parametros['no_cliente'];
    }

    $_parametros['ate'] = "";
    if(isset($parametros['ate'])) {
      $_parametros['ate'] = TRUE;
    }

    $_parametros['oldId'] = FALSE;
    if(isset($parametros['oldId']) && $parametros['oldId'] > 0) {
      $_parametros['oldId'] = TRUE;
    }

    return $_parametros;
  }

  private function _dataEspecificaBackendSlidesLista() {
    $result = array();
    if (!empty($this->_data)){
      foreach ($this->_data as $slide){
        $slide_dto = new \stdClass();
        $slide_dto->fechaDesde = null;
        $slide_dto->fechaHasta = null;
        $slide_dto->backendImagenWeb = null;
        $slide_dto->backendImagenApp = null;
        $slide_dto->botones = null;

        $slide_dto->id = $slide->idSlide;
        $slide_dto->publicado = $slide->publicado;

        if (!empty($slide->imagenWeb->uri)){
          $slide_dto->backendImagenWeb = $slide->imagenWeb->uri;
        }

        if (!empty($slide->imagenApp->uri)){
          $slide_dto->backendImagenApp = $slide->imagenApp->uri;
        }

        if (!empty($slide->fechaDesde)){
          $slide_dto->fechaDesde = date("d-m-Y",$slide->fechaDesde);
        }
        if (!empty($slide->fechaHasta)){
          $slide_dto->fechaHasta = date("d-m-Y",$slide->fechaHasta);
        }
        $result[] = $slide_dto;
      }
    }

    return $result;
  }

  /**
   * Crea un archivo csv con los datos de las comunicaciones
   * @param $comunicaciones Array de ComunicacionesBo
   */
  private function _exportar_comunicaciones($comunicaciones) {
    global $base_url;
    header('Content-Type: application/excel; charset=utf-8');
    header('Content-Disposition: attachment; filename="fgo_comunicaciones_exportacion_' . date("d_m_Y") . '.csv"');

    $fp = fopen('php://output', 'w');

    $linea = utf8_decode('"Marca";"Campaña";"Descuento";"Cuotas";"Descripción";"Fecha Desde";"Fecha Hasta";"Link de Comunicacion";"Link Imagen"') . PHP_EOL;
    fputs($fp, $linea);
    if (!empty($comunicaciones)) {
      foreach ($comunicaciones as $comunicacion) {

        $imagenEncontrada = $comunicacion->getImagenPortal();

        $urlImagen = "";
        if ($imagenEncontrada) {
          $urlImagen = $base_url.$imagenEncontrada->obtenerNombreRuta();
        }
        $urlComunicacion = $base_url."/#/comunicacion/".$comunicacion->idComunicacion;
        $linea = utf8_decode($comunicacion->titulo);
        $linea .= ";";
        $linea .= utf8_decode($comunicacion->nombreCampania);
        $linea .= ";";
        $linea .= utf8_decode($comunicacion->beneficios[0]->valor);
        $linea .= ";";
        $linea .= utf8_decode($comunicacion->beneficios[0]->cuota);
        $linea .= ";";
        $linea .= utf8_decode($comunicacion->descripcionExtendida);
        $linea .= ";";
        $linea .= date("d/m/Y", $comunicacion->fechaDesde);
        $linea .= ";";
        $linea .= date("d/m/Y", $comunicacion->fechaHasta);
        $linea .= ";";
        $linea .= $urlComunicacion;
        $linea .= ";";
        $linea .= $urlImagen;
        $linea .= PHP_EOL;

        fputs($fp, $linea);
      }
    }
    fclose($fp);
  }

  /**
   * Obtener rubros en jerarquias
   */
  public function obtenerBackendRubros() {
    $bo = new \Fgo\Bo\RubroBo();
    $listado = $bo->listar();

    $rubrosPadres = \Fgo\Services\Dto\Rubro::contruirEnJerarquia($listado);
    return $this->_output($rubrosPadres);
  }

  public function obtenerBackendCarruselLista()
  {
    $carruselBo = new \Fgo\Bo\CarruselBo();
    $items = array();
    foreach($carruselBo->listar() as $item) {
      $rubroBo = new \Fgo\Bo\RubroBo($item->idRubro);
      $rubro = $item->idRubro ? $rubroBo->nombre : "No definido";
      $item->rubro = $rubro;
      $items[] = $item;
    }
    return $this->_output($items);
  }
  public function obtenerBackendSegmentacionLista($page)
  {
    $objBo = new \Fgo\Bo\SegmentacionBo();
    $items = array();
    foreach($objBo->listarPage($page) as $item) {
      $items[] = $item;
    }
    return $this->_output($items);
  }

}