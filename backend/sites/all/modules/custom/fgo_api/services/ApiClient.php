<?php

namespace Fgo\Services;

use Fgo\Services\Dto\Comunicacion;
use Fgo\Services\BBVA;

class ApiClient extends Api {
  const Q_RESULTADOS = 20;
  const MENSAJE_COMUNICACION_NOENCONTRADA = "La comunicacion no existe";
  const MENSAJE_OK = "OK";

  private $_busqueda_palabra;
  private $_busqueda_nombres;
  private $_busqueda_id_rubros;
  protected $segmentacionBo;



  public function obtenerComunicacionesCarrusel($rubro_id, $rubroSuip = null){
    $this->_parametros['rubros'] = array($rubroSuip);
    $tmp = $this->_data;
    $response = $this->_obtenerDataComunicaciones();
    // Filtros
    if(!$this->_filtro_aplicado) {
      $response = $this->_filtrarComunicaciones();
    }
    // Paginado
    $response = $this->_paginar($response);
    
    $comunicaciones = array();
    $i = 0;
    if(count($response) > 0) {
      foreach ($response as $comunicacion) {
        $i ++;
        if($i > 5){
            break;
        }  
        $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
        $comunicaciones[] = $comunicacion_dto->listaCarrusel();
      }
    }

    $this->_data = $tmp ;
    return $comunicaciones;
  }
  
  public function obtenerComunicaciones() {
    $response = $this->_obtenerDataComunicaciones();
    // Filtros
    if(!$this->_filtro_aplicado) {
      $response = $this->_filtrarComunicaciones();
      $this->_generarCache(self::TIPO_CACHE_COMUNICACIONES, $this->_obtenerNombreCacheComunicaciones(), array_values($this->_data));
    }
    // Paginado
    $response = $this->_paginar($response);

    // DTO
    $comunicaciones = array();
    if(count($response) > 0) {
      foreach ($response as $comunicacion) {
        $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
        $comunicaciones[] = $comunicacion_dto->lista();
      }
    }

    return $this->_output($comunicaciones);
  }

  public function obtenerComunicacionesChatbot() {
    if($this->_usar_cache) {
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_CHATBOT, self::NOMBRE_CACHE_GENERAL);
    }

    if(!$this->_data) {
      $response = $this->_obtenerDataComunicaciones();

      // DTO
      $comunicaciones = array();
      if(count($response) > 0) {
        foreach ($response as $comunicacion) {
          // Si tiene casuistica se excluye
          if($comunicacion->beneficios[0]->casuistica != null) {
            continue;
          }
          $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
          $comunicaciones[] = $comunicacion_dto->lista_chatbot();
        }
      }
      $this->_data = $comunicaciones;
      
      if(!empty($this->_data)) {
        $this->_generarCache(self::TIPO_CACHE_CHATBOT, self::NOMBRE_CACHE_GENERAL, $this->_data);
      }
    }

    return $this->_output($this->_data);
  }

  public function obtenerComunicacionesCercanas() {
    $this->_obtenerDataComunicaciones();

    // Filtros
    $response = $this->_filtrarComunicaciones();

    // DTO
    $comunicaciones = array();
    if (count($response) > 0) {
      foreach ($response as $comunicacion) {
        $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
        $comunicaciones[] = $comunicacion_dto->listaSucursalesCercanas();
      }
    }

    return $this->_output($comunicaciones, self::MENSAJE_OK);
  }

  public function obtenerComunicacionesCarruselOld($rubro_id){
    $comunicacionesRows = \Fgo\Bo\ComunicacionBo::filtrarPorIdRubro($rubro_id);

    // DTO
    $comunicaciones = array();
    if(count($comunicacionesRows) > 0) {
      foreach ($comunicacionesRows as $registro) {
        $comunicacionBo   = new \Fgo\Bo\ComunicacionBo();
        $comunicacion     = $comunicacionBo->construirDesdeRegistro($registro);
        $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion( $comunicacion );
        $comunicaciones[] = $comunicacion_dto->listaCarrusel();
      }
    }

    return $comunicaciones;
  }

  public function obtenerComunicacion($id, $idSuip = NULL) {
    if($this->_parametros['oldId']) {
      $emparejamiento = $this->_obtenerCache(self::TIPO_CACHE_COMUNICACIONES, self::NOMBRE_CACHE_EMPAREJAMIENTO);
      $idSuip = (isset($emparejamiento->$id)) ? $emparejamiento->$id: NULL;
      $id = NULL;
    }
    $comunicacion = $this->_obtenerDataComunicacion($id, $idSuip);
    $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
    $detalle = $comunicacion_dto->detalle();
    if ($detalle["codigo_servicio"] == 0) {
      return $this->_output($detalle, self::MENSAJE_OK);
    } else if ($detalle["codigo_servicio"] == 1) {
      return $this->_output($detalle, self::MENSAJE_COMUNICACION_NOENCONTRADA);
    }
  }

  public function obtenerCampania($id) {
    $campania = $this->_obtenerDataCampania($id);

    // Paginado de comunicaciones
    $campania->comunicaciones = isset($campania->comunicaciones) ? $this->_paginar($campania->comunicaciones) : null;

    $comunicaciones = array();
    if(count($campania->comunicaciones) > 0) {
      foreach ($campania->comunicaciones as $comunicacion) {
        $comunicacion_dto = new \Fgo\Services\Dto\Comunicacion($comunicacion);
        $comunicaciones[] = $comunicacion_dto->lista();
      }
    }

    return $this->_output($comunicaciones);
  }

  public function obtenerResultadosBusqueda() {
    //?tag=palabra
    $resultado = array();
    if(strlen($this->_parametros['tag']) <= 250){
      $this->_busqueda_palabra = mb_strtoupper(trim($this->_parametros['tag']));
      $tags = $this->_obtenerDataTags();

      if (!empty($this->_busqueda_palabra) && !empty($tags) && strlen($this->_busqueda_palabra) >= 3) {
        $this->_separarDataBusqueda();
        $this->_obtenerPalabrasCercanasBusqueda();
        $busqueda_dto = new \Fgo\Services\Dto\Busqueda($this->_busqueda_nombres, $this->_busqueda_id_rubros);
        $resultado = $busqueda_dto->resultadoBusqueda($this->_busqueda_palabra);
      }
    }

    return $this->_output($resultado);
  }

  public function obtenerBasesMarket()
  {
    if ($cache = cache_get('fgo_backend_bases_market_field_texto')) {
      $texto = $cache->data;
    } else {
      $basesMarketBo = new \Fgo\Bo\BasesMarketBo();
      $basesMarketBo->obtenerRegistroActual();
      $texto = $basesMarketBo->texto;
      cache_set('fgo_backend_bases_market_field_texto', $texto);
    }

    return $this->_output(array('texto' => nl2br($texto)));
  }

  public function avisoCompraGoShop($params){

      
    if(!$this->validatePostMethod()){
        watchdog('aviso compra shop', ' METHOD_NOT_ALLOWED');
        return $this->_error( array("method not allowed"), "METHOD_NOT_ALLOWED", 400);
    }
    /*$token = isset($_SERVER['HTTP_TOKEN'])?$_SERVER['HTTP_TOKEN']:false;
    if(!$token || !$this->isTokenValid($token)){
      return $this->_error( array("token invalido o expirado"), "INVALID_TOKEN",400);
    }
    */
   
    $gsRequest = getPostParams();
    watchdog('aviso compra shop', ' AvisoCompraGoShop JSON: , %respuesta', array('%respuesta' => file_get_contents("php://input", true)));
    
    $id = isset($gsRequest["id"])?$gsRequest["id"]:false;
    $id_pedido = isset($gsRequest["reference"])? $gsRequest["reference"]: false;
    if(!$gsRequest || !$id || !$id_pedido){
       watchdog('aviso compra shop', ' MAIL_DELIVERED_FAIL, Mail no enviado');
      return $this->_error(array("Mail no enviado"), "MAIL_DELIVERED_FAIL", 400);
    }
    //hd("pasa validacion");
    // buscar si ya existe la orden 
    $goShopBo = new \Fgo\Bo\GoShopBo();
    $result = $goShopBo->buscarPorIdPedido($id_pedido);
    if($result){
       watchdog('aviso compra shop', ' MAIL_DELIVERED_FAIL, Mail ya enviado');
      return $this->_error(array("Mail ya enviado"), "MAIL_DELIVERED_FAIL", 400);
    }
   
    $goShopService = new \Services\BBVA\GoShop();
    
    if($goShopService->processRequest($gsRequest)){
       watchdog('aviso compra shop', ' MAIL_DELIVERED_SUCCESS, Mail enviado');
      return $this->_output(array("Mail enviado"), "MAIL_DELIVERED_SUCCESS", 200);
    }
    watchdog('aviso compra shop', ' MAIL_DELIVERED_FAIL, Mail no enviado 2');
    return $this->_error(array("Mail no enviado"), "MAIL_DELIVERED_FAIL", 400);

  }

  public function monitoreHome(){
    
    $monitore_home = array();
    $data_home = $this->_obtenerDataHome(strtotime("today"), true);

    $monitore_home['slides'] = $data_home->slides != array();
    $monitore_home['shortcuts'] = $data_home->shortcuts != array();

    return $monitore_home;
  }

  public function monitoreComunicaciones(){
    
    $comunicaciones = new \Fgo\Bo\ComunicacionBo();
    $data_comunicaciones = $comunicaciones->monitoreVigentes();

    return count($data_comunicaciones) > 0;
  } 

  public function monitoreTarjetas(){
    return count(_get_tarjetas_all()) > 0;
  } 

  public function monitoreSorteos(){
    return count(_get_sorteos_all()) > 0;
  } 


  public function obtenerHome($fecha, $monitore = false) {
    $slideDefault = array();
    $data_home = $this->_obtenerDataHome($fecha, $monitore);
    $slideFechas = $data_home->slides;

    $shortcuts = $data_home->shortcuts;
    $dto = new \Fgo\Services\Dto\Home($slideFechas, $shortcuts);
    $dto->setCarruseles( $data_home->carruseles );
    $response = $dto->lista($slideFechas);

    return $this->_output($response);
  }

  public function obtenerPreferencias() {
    $response = $this->_obtenerDataPreferencias();

    return $this->_output($response);
  }

  public function monitore($response) {

    global $conf;
    $bad = array();
    $ok = array();
    $code = 1;

    foreach($response as $key => $statusEndpoint){
      if($statusEndpoint){
        $ok[] = $key;
      }else{
        $bad[] = $key;
      }
    }

    $code = count($response) == count($bad) ? 0: $code;
    $message = count($response) == count($bad) ? 0 : "Todos los servicios estan caidos";
    
    $code = count($ok) > count($bad) ? 2 : $code;
    $code = count($ok) < count($bad) ? 3 : $code;
    $code = count($bad) == 0 ? 1 : $code; 

    $message = $code == 2 || $code == 3 ? "Servicios caidos: " : count($bad) > count($ok) ? "Servicios caidos: " : $message;
    $message = count($bad) == 0 ? 'OK' : $message;

    if($code == 2 || $code == 3){
      if($bad != array()){
        foreach ($bad as $badEndpoint) {
          $message .= " ".$badEndpoint." ";  
        }
      }
    }

    // data message code
    return $this->_output($response, "Monitore ".$conf['env'].". ".$message, $code);
  }

  private function _obtenerDataPreferencias() {

    $cache_nombre = self::NOMBRE_CACHE_PREFERENCIAS;
    $tipo_cache = self::TIPO_CACHE_PARAMETROS;

    $this->_data = null;
    // Busco en caché
    if($this->_usar_cache) {
      $this->_data = $this->_obtenerCache($tipo_cache, $cache_nombre);
    }
   
    // Si no encontré nada en caché
    if(!$this->_data) {
      $preferenciasDto = new \Fgo\Services\Dto\Preferencias();
      $this->_data = $preferenciasDto->getData();

      if($this->_data) {
        // Guardo el general en caché
        $this->_generarCache($tipo_cache, $cache_nombre, $this->_data);
      }
    }

    return $this->_data;
  }

  private function _obtenerDataComunicaciones() {
    $this->_data = null;
    // Busco en caché
    if($this->_usar_cache) {
      $cache_nombre = $this->_obtenerNombreCacheComunicaciones();
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_COMUNICACIONES, $cache_nombre);

      // Si obtengo cache con filtros aplicados no necesito filtrar
      if($this->_data) {
        $this->_filtro_aplicado = true;
      }

      // Si no obtengo cache filtrado, busco el cache general
      if(!$this->_data && $cache_nombre != self::NOMBRE_CACHE_GENERAL) {

        $this->_data = $this->_obtenerCache(self::TIPO_CACHE_COMUNICACIONES, self::NOMBRE_CACHE_GENERAL);
      }
    }

    // Si no encontré nada en caché
    if(!$this->_data) {
      $comunicaciones = new \Fgo\Bo\ComunicacionBo();
      $this->_data = $comunicaciones->buscarVigentes();

      if($this->_data) {
        // Guardo el general en caché
        $this->_generarCache(self::TIPO_CACHE_COMUNICACIONES, self::NOMBRE_CACHE_GENERAL, array_values($this->_data));
      }
    }

    return $this->_data;
  }

  private function _obtenerDataComunicacion($id = NULL, $idSuip = NULL) {
    $this->_data = array();
    // Busco en caché
    if($this->_usar_cache) {
      $cache_nombre = self::NOMBRE_CACHE_COMUNICACION . "_" . (int) $id;
      if($idSuip) {
        $cache_nombre = self::NOMBRE_CACHE_COMUNICACION_SUIP . "_" . (int) $idSuip;
      }
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_COMUNICACIONES, $cache_nombre);

      // Si obtengo cache con filtros aplicados no necesito filtrar
      if(!$this->_data) {
        $comunicacion = $this->_obtenerDataComunicacionDesdeCache($id, $idSuip);
        if ($comunicacion) {
          $this->_data = $comunicacion;
          $this->_generarCache(self::TIPO_CACHE_COMUNICACIONES, self::NOMBRE_CACHE_COMUNICACION . "_" . (int) $comunicacion->idComunicacion, $this->_data);
        } else {
          // Si no encuentro nada guardo igual para no buscar la proxima vez la misma comunicacion
          $this->_generarCache(self::TIPO_CACHE_COMUNICACIONES, $cache_nombre, $this->_data);
        }
        // Guardo tambien cache por id de Suip
        if($comunicacion && $comunicacion->idBeneficioSuip != null) {
          $this->_generarCache(self::TIPO_CACHE_COMUNICACIONES, self::NOMBRE_CACHE_COMUNICACION_SUIP . "_" . (int) $comunicacion->idBeneficioSuip, $this->_data);
        }
      }
    }

    return $this->_data;
  }

  private function _obtenerDataComunicacionDesdeCache($id = NULL, $idSuip = NULL) {
    $cache = $this->_obtenerCache(self::TIPO_CACHE_COMUNICACIONES, self::NOMBRE_CACHE_GENERAL);
    if(!empty($cache)) {
      foreach ($cache as $comunicacion) {
        if($id && $comunicacion->idComunicacion == $id) {
          return $comunicacion;
        }
        if($idSuip && $comunicacion->idBeneficioSuip == $idSuip) {
          return $comunicacion;
        }
      }
    }
    return false;
  }

  private function _obtenerDataCampania($id) {
    $this->_data = null;
    // Busco en caché
    if($this->_usar_cache) {
      $cache_nombre = self::NOMBRE_CACHE_CAMPANIA . "_" . (int) $id;
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_CAMPANIA, $cache_nombre);

      if(!$this->_data) {
        $campaniaEncontrada = \Fgo\Bo\CampaniaBo::buscarPorIdSuip($id);
        if ($campaniaEncontrada && $campaniaEncontrada->esVisible()) {
          // Si la campaña no tiene activada la opción siempre visible muestro SOLO las comunicaciones vigentes
          if(!empty($campaniaEncontrada->comunicaciones) && !$campaniaEncontrada->siempreVisible) {
            $comunicaciones = array();
            foreach ($campaniaEncontrada->comunicaciones as $comunicacion) {
              if($comunicacion->esVisible()) {
                $comunicaciones[] = $comunicacion;
              }
            }
            $campaniaEncontrada->comunicaciones = $comunicaciones;
          }
          $this->_data = $campaniaEncontrada;
        }
        $this->_generarCache(self::TIPO_CACHE_CAMPANIA, $cache_nombre, $this->_data);
      }
    }

    return $this->_data;
  }

  private function _obtenerDataTags() {
    $this->_data = null;

    // Busco en caché
    if ($this->_usar_cache) {
      $cache_nombre = self::NOMBRE_CACHE_ETIQUETAS;
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_ETIQUETAS, $cache_nombre);

      if (!$this->_data) {
        $tagsEncontrados = array();

        //Comunicaciones
        $response_comunicaciones = $this->_obtenerDataComunicaciones();
        if(count($response_comunicaciones) > 0) {
          foreach ($response_comunicaciones as $comunicacion) {
            $tags = $comunicacion->tags;
            foreach ( $tags as $tag ) {
              $tagsEncontrados[$tag->idTag] = array(mb_strtoupper($tag->nombre), "beneficio");
            }
          }
        }

        //Sorteos
        $response_sorteos = bbva_sorteos_vigentes_tags();
        if(count($response_sorteos) > 0) {
          foreach ($response_sorteos as $sorteo) {
            $tagsEncontrados[$sorteo->tid] = array(mb_strtoupper($sorteo->name), "sorteo");
          }
        }
        $this->_data = $tagsEncontrados;

        $this->_generarCache(self::TIPO_CACHE_ETIQUETAS, $cache_nombre, $this->_data);
      }
    }

    return $this->_data;
  }

  private function _obtenerDataHome($fecha, $monitore = false) {
    $this->_data = null;
    // Busco en caché
    if($this->_usar_cache && !$monitore) {
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_HOME, $fecha);
    }

    // Si no encontré nada en caché
    if(!$this->_data) {
      $slides = new \Fgo\Bo\SlideFechaBo();
      $this->_data = new \stdClass();
      $this->_data->slides = $slides->buscarPorFecha($fecha, true);
      $this->_data->shortcuts =\Fgo\Bo\AccesoDirectoBo::obtenerAccesosDirectos();
      
      
      $this->_data->carruseles =  null;
      if(module_exists("fgo_backend_carrusel")){
          $this->_data->carruseles = $this->_obtenerCarruseles();
      }
      

      if($this->_data && !$monitore) {
        // Guardo el general en caché
        $this->_generarCache(self::TIPO_CACHE_HOME, $fecha, (array) $this->_data);
      }
    }
    
    if(module_exists("bbva_segmentacion")){
          $segmentacion_front_enable = variable_get("fgo_segmentacion_front_enable",false);
          if($segmentacion_front_enable == "enable"){
            $this->_data->shortcuts = $this->_obtenerSegmentacionShortcuts($this->_data->shortcuts);
          }
    }
    
    return $this->_data;
  }

  private function _obtenerSegmentacionShortcuts( $accesoDirectoList ){
    global $user;
    $res_service = false;;
    if(!isset($user->name) || !is_numeric($user->name)){
          //  return $accesoDirectoList;
    } 
    $lista = array();
    $this->segmentacionBo = new \Fgo\Bo\SegmentacionBo();
     
    $esCliente = $this->segmentacionBo->esCliente() ? SEGMENTACION_SOS_CLIENTE : SEGMENTACION_NO_SOS_CLIENTE;
    if($esCliente == SEGMENTACION_NO_SOS_CLIENTE){
        $res_service = $this->segmentacionServiceOnline();
    }

    foreach( $accesoDirectoList as $accesoDirecto ) {
      
      if( $accesoDirecto->tipo == 'latam_pass' && !$this->segmentacionBo->esLatam($res_service) ) 
        continue;
      if( $accesoDirecto->tipo == 'pagos_nfc' && !$this->segmentacionBo->esMaster($res_service) )
        continue;
      
      if( $accesoDirecto->tipo == 'pagos_qr' && !$this->segmentacionBo->esDomoQr($res_service) )
        continue;
      if( $accesoDirecto->tipo == 'p2p' && !$this->segmentacionBo->esDomoP2p($res_service) )
        continue;
              
      $accesoDirecto->nombre = $accesoDirecto->tipo == SEGMENTACION_TIPO_SEAS_O_NO_CLIENTE ? $esCliente : $accesoDirecto->nombre;
      $lista[] = $accesoDirecto;
    }

    return $lista;
  }

  private function _obtenerCarruseles()
  {
    $response = array();
    $listado = \Fgo\Bo\CarruselBo::obtenerCarruseles();

    foreach ($listado as $carrusel) {
      $data_carrusel = array();
      if( $carrusel->publicado == 0 ) {
        $response[$carrusel->nombre] = null;
      } else {
        if ($carrusel->id_rubro !== null) {
          $rubro_bo = new \Fgo\Bo\RubroBo($carrusel->id_rubro);
          $response[$carrusel->nombre] = array(
            "rubro_id" => $carrusel->id_rubro,
            "rubro" =>  $rubro_bo->nombre,
            "call_to_action" => "nav://communications/rubros=" .$rubro_bo->idRubroSuip,
            "data" => $this->obtenerComunicacionesCarrusel($carrusel->id_rubro, $rubro_bo->idRubroSuip)
          );
        } else {
          $response[$carrusel->nombre] = array('empty');
        }
      }
    }

    return $response;
  }

  private function _obtenerNombreCacheComunicaciones() {
    $nombre = array();
    $filtro = false;
    $nombre[] = self::NOMBRE_CACHE_FILTROS;
    
    if(count($this->_parametros['rubros']) > 0) {
      $filtro = true;
      $nombre[] = implode("-", $this->_parametros['rubros']);
    } else {
      $nombre[] = "x";
    }
    if(count($this->_parametros['provincias']) > 0) {
      $filtro = true;
      $nombre[] = implode("-", $this->_parametros['provincias']);
    } else {
      $nombre[] = "x";
    }

    if($this->_parametros['canal_fisico']) {
      $filtro = true;
      $nombre[] = "1";
    } else {
      $nombre[] = "x";
    }
    if($this->_parametros['canal_online']) {
      $filtro = true;
      $nombre[] = "1";
    } else {
      $nombre[] = "x";
    }
    if($this->_parametros['canal_telefonico']) {
      $filtro = true;
      $nombre[] = "1";
    } else {
      $nombre[] = "x";
    }
    if($this->_parametros['destacado']) {
      $filtro = true;
      $nombre[] = "1";
      if($this->_parametros['destacado'] == "n") {
        $nombre[] = "0";
      }
    } else {
      $nombre[] = "x";
    }
    if(count($this->_parametros['tags']) > 0) {
      $filtro = true;
      $nombre[] = implode("-", $this->_parametros['tags']);
    } else {
      $nombre[] = "x";
    }
    if($this->_parametros['no_cliente']) {
      $filtro = true;
      $nombre[] = "1";
    } else {
      $nombre[] = "x";
    }

    if($filtro) {
      $nombre_cache = implode("_", $nombre);
    } else {
      $nombre_cache = self::NOMBRE_CACHE_GENERAL;
    }

    return $nombre_cache;
  }

  private function _filtrarComunicaciones() {

    if(count($this->_data) > 0) {
      foreach ($this->_data as $k_comunicacion => $comunicacion) {


        // Comunicaciones para no clientes
        if($this->_parametros['no_cliente']) {
          if ($comunicacion->beneficios == null) {
            unset($this->_data[$k_comunicacion]);
            continue;
          }
          foreach ($comunicacion->beneficios as $beneficio) {
            if ($beneficio->tipoBeneficio != 3) {
              unset($this->_data[$k_comunicacion]);
              continue;
            }
          }
        }

        // Rubros
        if(!empty($this->_parametros['rubros'])) {
          $filter_rubros = true;
          foreach ($comunicacion->rubros as $rubro) {
            if(isset($rubro->idRubroSuip) && isset($this->_parametros['rubros']) && isset($rubro->rubroPadre->idRubroSuip)){
              if(in_array($rubro->idRubroSuip, $this->_parametros['rubros']) || in_array($rubro->rubroPadre->idRubroSuip, $this->_parametros['rubros'])) {
                $filter_rubros = false;
                break;
              }
            }
          }
          if($filter_rubros) {
            unset($this->_data[$k_comunicacion]);
            continue;
          }
        }

        // Provincias
        if(!empty($this->_parametros['provincias'])) {
          $filter_provincias = true;
          if(!empty($comunicacion->sucursalesFisicas)) {
            foreach ($comunicacion->sucursalesFisicas as $sucursalFisica) {
              if(in_array($sucursalFisica->provincia, $this->_parametros['provincias'])) {
                $filter_provincias = false;
                break;
              }
            }
          }
          if($filter_provincias) {
            unset($this->_data[$k_comunicacion]);
            continue;
          }
        }

        // Tags
        if (!empty($this->_parametros['tags'])) {
          $filter_tags = true;
          foreach ($comunicacion->tags as $tag) {
            if (in_array($tag->idTag, $this->_parametros['tags'])) {
              $filter_tags = false;
              break;
            }
          }
          if($filter_tags) {
            unset($this->_data[$k_comunicacion]);
            continue;
          }
        }

        // Destacados
        if($this->_parametros['destacado']) {
          if($this->_parametros['destacado'] == "s" && !$comunicacion->destacado) {
            unset($this->_data[$k_comunicacion]);
            continue;
          } elseif ($this->_parametros['destacado'] == "n" && $comunicacion->destacado) {
            unset($this->_data[$k_comunicacion]);
            continue;
          }
        }

        // Puntos de venta
        if($this->_parametros['canal_fisico']) {
          if(empty($comunicacion->sucursalesFisicas)) {
            unset($this->_data[$k_comunicacion]);
            continue;
          }
        }

        // Ubicacion
        if (isset($this->_parametros['radio']) && $this->_parametros['radio']) {
          $radio = ($this->_parametros['radio'] < 50)?$this->_parametros['radio']:50;
          if(!empty($this->_data[$k_comunicacion]->sucursalesFisicas) && $this->_data[$k_comunicacion]->sucursalesFisicas != null) {

            foreach ($comunicacion->sucursalesFisicas as $ks => $sucursal) {
              $distancia = $this->_haversineGreatCircleDistance($this->_parametros['lat'], $this->_parametros['long'], $sucursal->latitud, $sucursal->longitud);
              if ($distancia > $radio) {
                unset($this->_data[$k_comunicacion]->sucursalesFisicas[$ks]);
              }
            }

          }
          if(empty($this->_data[$k_comunicacion]->sucursalesFisicas)) {
            unset($this->_data[$k_comunicacion]);
          }
        }

        if($this->_parametros['canal_online'] || $this->_parametros['canal_telefonico']) {
          $filter_canales = true;
          foreach ($comunicacion->sucursalesVirtuales as $sucursal) {
            // Online
            if($this->_parametros['canal_online']) {
              if(!empty($sucursal->web)) {
                $filter_canales = false;
              }
            }
            // Telefonico
            if($this->_parametros['canal_telefonico']) {
              if(!empty($sucursal->tlf)) {
                $filter_canales = false;
              }
            }
          }
          if($filter_canales) {
            unset($this->_data[$k_comunicacion]);
            continue;
          }
        }
      }
    }
    return $this->_data;
  }

  protected function _setearParametros($parametros) {
    $_parametros = array();

    $_parametros['pagina'] = 0;
    if(isset($parametros['pager'])) {
      $_parametros['pagina'] = (int) $parametros['pager'];
    }

    $_parametros['rubros'] = array();
    if(isset($parametros['rubros'])) {
      // Guardo los rubros en un array ordenado numericamente
      $_parametros['rubros'] = explode(",", $parametros['rubros']);
      sort($_parametros['rubros'], SORT_NUMERIC);
    }

    $_parametros['campania'] = 0;
    if(isset($parametros['campaign'])) {
      $_parametros['campania'] = $parametros['campaign'];
    }

    $_parametros['destacado'] = false;
    if(isset($parametros['destacado'])) {
      $_parametros['destacado'] = "s";
      if($parametros['destacado'] == "n") {
        $_parametros['destacado'] = "n";
      }
    }

    $_parametros['provincias'] = array();
    if(isset($parametros['provincias'])) {
      $_parametros['provincias'] = explode(",", $parametros['provincias']);
      sort($_parametros['provincias'], SORT_NUMERIC);
    }

    $_parametros['canal_fisico'] = false;
    $_parametros['canal_online'] = false;
    $_parametros['canal_telefonico'] = false;
    if(isset($parametros['canales'])) {
      $canales = explode(",", $parametros['canales']);
      if(in_array("1", $canales)) {
        $_parametros['canal_fisico'] = true;
      }
      if(in_array("2", $canales)) {
        $_parametros['canal_online'] = true;
      }
      if(in_array("3", $canales)) {
        $_parametros['canal_telefonico'] = true;
      }
    }

    if(isset($parametros['lat']) && isset($parametros['long']) && isset($parametros['radio'])) {
      $_parametros['lat'] = $parametros['lat'];
      $_parametros['long'] = $parametros['long'];
      $_parametros['radio'] = $parametros['radio'];
    }

    if(isset($parametros['seccion']) && isset($parametros['direccion'])) {
      $_parametros['seccion'] = $parametros['seccion'];
      $_parametros['direccion'] = $parametros['direccion'];
      $_parametros['ctaIdEdit'] = $parametros['ctaIdEdit'];
    }

    $_parametros['tags'] = array();
    if (isset($parametros['tags'])) {
      $_parametros['tags'] = explode(",", $parametros['tags']);
      sort($_parametros['tags'], SORT_NUMERIC);
    }

    $_parametros['tag'] = "";
    if(isset($parametros['tag'])) {
      $_parametros['tag'] = $parametros['tag'];
    }

    $_parametros['ids'] = "";
    if(isset($parametros['ids'])) {
      $_parametros['ids'] = $parametros['ids'];
    }

    $_parametros['no_cliente'] = "";
    if(isset($parametros['no_cliente'])) {
      $_parametros['no_cliente'] = $parametros['no_cliente'];
    }

    $_parametros['ate'] = "";
    if(isset($parametros['ate'])) {
      $_parametros['ate'] = TRUE;
    }

    $_parametros['oldId'] = FALSE;
    if(isset($parametros['oldId']) && $parametros['oldId'] > 0) {
      $_parametros['oldId'] = TRUE;
    }

    return $_parametros;
  }

  /**
   * Calculates the great-circle distance between two points, with
   * the Haversine formula.
   * @param float $latitudeFrom Latitude of start point in [deg decimal]
   * @param float $longitudeFrom Longitude of start point in [deg decimal]
   * @param float $latitudeTo Latitude of target point in [deg decimal]
   * @param float $longitudeTo Longitude of target point in [deg decimal]
   * @param float $earthRadius Mean earth radius in [km]
   * @return float Distance between points in [Km]
   */
  private function _haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo,
                                        $earthRadius = 6371){
    // convert from degrees to radians
    $latFrom = M_PI * $latitudeFrom / 180;
    $lonFrom = M_PI * $longitudeFrom / 180;
    $latTo = M_PI * $latitudeTo / 180;
    $lonTo = M_PI * $longitudeTo / 180;

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

    return $angle * $earthRadius;
  }

  private function _separarDataBusqueda() {
    $nombres = array();
    $idRubros = array();
    if (!empty($this->_data)){
      foreach ($this->_data as $key => $value){
        $nombres[$key] = $value[0];
        $idRubros[$key] = $value[1];
      }
    }
    $this->_busqueda_nombres = $nombres;
    $this->_busqueda_id_rubros = $idRubros;
  }

  private function _obtenerPalabrasCercanasBusqueda() {
    $palabra = $this->_busqueda_palabra;
    // Se realiza la búsqueda más cercana al string
    uasort($this->_busqueda_nombres, function ($a, $b) use ($palabra) {
      $lev_a = levenshtein($palabra, $a);
      $lev_b = levenshtein($palabra, $b);

      return $lev_a === $lev_b ? 0 : ($lev_a > $lev_b ? 1 : -1);
    });
  }

  /**
   * Obtener rubros en jerarquias para pdm
   */
  public function obtenerRubrosPdm() {
    $rubrosPadres = $this->_obtenerDataRubrosPdm();
    return $this->_output($rubrosPadres);
  }

  private function _obtenerDataRubrosPdm() {

    $cache_nombre = self::NOMBRE_CACHE_RUBROS_PDM;
    if ($this->_usar_cache) {
      $this->_data = $this->_obtenerCache(self::TIPO_CACHE_PARAMETROS, $cache_nombre);
    }
   
    if (!$this->_data) {
      $listado = \Fgo\Bo\RubroBo::listarRubrosVisiblesEnPdm();
      $this->_data = \Fgo\Services\Dto\Rubro::contruirEnJerarquia($listado);
      if ($this->_data) {
        // Guardo el general en caché
        $this->_generarCache(self::TIPO_CACHE_PARAMETROS, $cache_nombre, $this->_data);
      }
    }

    return $this->_data;
  }

  public function login($params){
      if(!$this->validatePostMethod()){
        return $this->_error( array("method not allowed"), "METHOD_NOT_ALLOWED", 400);
      }
      $params = getPostParams();
      $user = isset($params['user'])?$params['user']:false;
      if(!$user){
        return $this->_error( array("user is required"), "INVALID_DATA", 400);
      } 
      $pass = isset($params['pass'])?$params['pass']:false;
      if(!$pass){
        return $this->_error( array("pass is required"), "INVALID_DATA", 400);
      }
      return $this->generarToken($user, $pass);
    }

  protected function generarToken($user, $password){
        $usuario = $this->verificarLogin($user, $password);
        if(!$usuario){
          return $this->_error( array("login failed"), "INVALID_DATA", 400);
        }
        $token = $this->token();
        $usuario->token = $token;
        $usuario->modified = date("Y-m-d H:i:s");
        $usuario->guardar();
        return $this->_output( array("TOKEN"=>$token), "TOKEN", 200);
  }

  public function isTokenValid($token){
    // verificar si el token todavia no expira. en la db
    $model = new \Fgo\Bo\GoShopUsuarioBo();
    //$date = date("Y-m-d 10:i:s");
    $date = date("Y-m-d H:i:s", (strtotime ("-1 Hours")));
    $usuario = $model->buscarPorToken($token, $date);
    return $usuario;
  }

    protected function _error( $data, $message, $code){
      drupal_add_http_header('Access-Control-Allow-Methods', 'POST');
      drupal_add_http_header('STATUS', '400');
      drupal_add_http_header('Content-Type', 'application/json');
      return $this->_output( $data, $message, $code);
    }

    protected function verificarLogin($user, $password){
      $model = new \Fgo\Bo\GoShopUsuarioBo();
      $usuario = $model->buscarPorUserPass($user, $password);
      return $usuario;
    }

  protected function token() {
      return md5(uniqid(mt_rand(), true));
  }

  public function validatePostMethod(){
    $metodo = $_SERVER['REQUEST_METHOD'];
    if( $metodo == "POST") {
      return true;
    }
    return false;
  }
  
  public function segmentacionServiceOnline(){
      global $user;
      $fgo_segmentacion_front_service_enable = variable_get("fgo_segmentacion_front_service_enable",false);
       if(!$fgo_segmentacion_front_service_enable){
           return false;
       }
       if($fgo_segmentacion_front_service_enable != "enable" ){
           return false;
       }
       // verificar cache
       if(variable_get("bbva_segmentacion_service_online_cache") == "enable"){
          log_seg_online("enable", "bbva_segmentacion_service_online_cache");
          $cache_clientes = cache_get("bi_clientes_online");
          $bi_clientes = $cache_clientes->data;
          if(array_key_exists($user->name , $bi_clientes)){
             log_seg_online("read cache ", $bi_clientes[$user->name]);
            return json_decode($bi_clientes[$user->name]);
          }
        }
        // se guarda en cache
       $response =  $this->segmentacionServiceOnlineCall();
       if(variable_get("bbva_segmentacion_service_online_cache") == "enable"){
           $cache_clientes = cache_get("bi_clientes_online");
           $bi_clientes = $cache_clientes->data;
           $bi_clientes[$user->name] = json_encode($response);
           log_seg_online("save cache ", json_encode($response));
           cache_set("bi_clientes_online",$bi_clientes );
       }
       return $response;
  }
  
  /**
   * 
   * @global type $user
   * @return boolean|\stdClass
   */
  public function segmentacionServiceOnlineCall($cliente = false){
    global $user;
    if(!$cliente){
        if(!isset($user->name)){
            return false;
        }
        $cliente = $user->name;
    }
    if(!$this->segmentacionBo){
        $this->segmentacionBo = new \Fgo\Bo\SegmentacionBo();
    }
       
    $esCliente = $this->segmentacionBo->esCliente() ? SEGMENTACION_SOS_CLIENTE : SEGMENTACION_NO_SOS_CLIENTE;
    
    if($esCliente == SEGMENTACION_NO_SOS_CLIENTE){
        try {
            $service = \Services\BBVA\SegmentacionService::getInstance();
            $res = $service->obtenerSegmentacionService($cliente);
            log_seg_online(" servicio response ", json_encode($res));
            $response = new \stdClass();
            if($res && isset($res->mensaje->isLan)){
                $response->esLatam = (string)$res->mensaje->isLan;
            }
            return $response;
        } catch (Exception $e){
            hd($e->getMessage());
        }
    }
    return false;
  }
  
  
  
}