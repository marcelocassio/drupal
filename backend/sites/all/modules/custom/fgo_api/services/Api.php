<?php

namespace Fgo\Services;

class Api {
  // Si se agrega un tipo NO OLVIDAR agregarlo en el método obtenerConstantesCache (para que se creen las carpetas)
  const TIPO_CACHE_CAMPANIA = "client/campanias";
  const TIPO_CACHE_CHATBOT = "client/chatbot";
  const TIPO_CACHE_BE_CAMPANIA = "backend/campanias";
  const TIPO_CACHE_COMUNICACIONES = "client/comunicaciones";
  const TIPO_CACHE_ETIQUETAS = "client/etiquetas";
  const TIPO_CACHE_BE_CTA = "backend/cta";
  const TIPO_CACHE_PARAMETROS = "client/parametros";
  const TIPO_CACHE_HOME = "client/home";

  const NOMBRE_CACHE_CAMPANIA = "campania";
  const NOMBRE_CACHE_COMUNICACION = "comunicacion";
  const NOMBRE_CACHE_COMUNICACION_SUIP = "comunicacion_suip";
  const NOMBRE_CACHE_GENERAL = "general";
  const NOMBRE_CACHE_PREFERENCIAS = "preferencias";
  const NOMBRE_CACHE_RUBROS_PDM = "rubros_pdm";
  const NOMBRE_CACHE_ETIQUETAS = "tag";
  const NOMBRE_CACHE_FILTROS = "filtro";
  const NOMBRE_CACHE_EMPAREJAMIENTO = "emparejamiento";

  protected $_cache;
  protected $_data;
  protected $_filtro_aplicado;
  protected $_parametros;
  protected $_usar_cache;

  public function __construct($parametros = array()) {
    $this->_cache = \BBVA\Cache\CacheFile::getInstance();
    $this->_data = null;
    $this->_filtro_aplicado = false;
    $this->_parametros = $this->_setearParametros($parametros);
    $this->_usar_cache = variable_get('bbva_cache_file_activo', false);

    return $this;
  }

  public static function obtenerConstantesCache() {
    return array(
      "comunicaciones" => array(
        "tipo" => self::TIPO_CACHE_COMUNICACIONES,
        "nombres" => array(
          "general" => self::NOMBRE_CACHE_GENERAL,
          "comunicacion" => self::NOMBRE_CACHE_COMUNICACION,
          "comunicacion_suip" => self::NOMBRE_CACHE_COMUNICACION_SUIP,
          "emparejamiento" => self::NOMBRE_CACHE_EMPAREJAMIENTO
        )
      ),
      "campanias" => array(
        "tipo" => self::TIPO_CACHE_CAMPANIA,
        "nombres" => array(
          "general" => self::NOMBRE_CACHE_CAMPANIA
        )
      ),
      "chatbot" => array(
        "tipo" => self::TIPO_CACHE_CHATBOT,
        "nombres" => array(
          "general" => self::NOMBRE_CACHE_GENERAL
        )
      ),
      "campanias_be" => array(
        "tipo" => self::TIPO_CACHE_BE_CAMPANIA,
        "nombres" => array(
          "general" => self::NOMBRE_CACHE_CAMPANIA
        )
      ),
      "tags" => array(
        "tipo" => self::TIPO_CACHE_ETIQUETAS,
        "nombres" => array(
          "general" => self::NOMBRE_CACHE_ETIQUETAS
        )
      ),
      "call_to_actions" => array(
        "tipo" => self::TIPO_CACHE_BE_CTA,
        "nombres" => array(
          "general" => self::NOMBRE_CACHE_GENERAL
        )
      ),
      "parametros" => array(
        "tipo" => self::TIPO_CACHE_PARAMETROS,
        "nombres" => array(
          "preferencias" => self::NOMBRE_CACHE_PREFERENCIAS,
          "rubros_pdm" => self::NOMBRE_CACHE_RUBROS_PDM
        )
      ),
      "home" => array(
        "tipo" => self::TIPO_CACHE_HOME,
        "nombres" => array()
      ),
    );
  }

  protected function _generarCache($cache_tipo, $cache_nombre, $data) {
    if($this->_usar_cache) {
      $cache_result = $this->_cache->set($cache_tipo, $cache_nombre, $data);
    }
  }

  protected function _obtenerCache($cache_tipo, $cache_nombre) {
    $cache_result = null;
    if($this->_usar_cache) {
      $cache_result = $this->_cache->get($cache_tipo, $cache_nombre);
    }
    return $cache_result;
  }

  protected function _setearParametros($parametros) {
    $_parametros = array();

    return $_parametros;
  }

  protected function _paginar($data) {
    $inicio = (int) ($this->_parametros['pagina'] * static::Q_RESULTADOS);
    $data = $data ? $data : array();
    $pagina = array_slice($data, $inicio, static::Q_RESULTADOS);
    return $pagina;
  }

  protected function _output($data = null, $message = "", $code = 0) {
    $output = array(
      'code' => (int) $code,
      'message' => (string) $message,
      'data' => $data
    );

    return $output;
  }

}