<?php
/*
 * Implements hook_drush_command().
 */
function bbva_data_export_drush_command() {

  $items['export-no-client'] = array(
    'description' => 'Exporta archivo con informaci? de no clientes.',
    'aliases' => array('enc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-suscriptos'] = array(
    'description' => 'Exporta archivo con informaci? de suscriptos.',
    'aliases' => array('esus'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-promo-cupon'] = array(
    'description' => 'Exporta archivos con informaci? de las promociones de cupones',
    'aliases' => array('epc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-vouchers-downloaded'] = array(
    'description' => 'Exporta archivos con informaci? de los vouchers descargados.',
    'aliases' => array('evd'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-vouchers-used'] = array(
    'description' => 'Exporta archivos con información de los vouchers usados.',
    'aliases' => array('evu'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  // Nueva Version
  // drush_bbva_data_export_export_vouchers_descargados
  $items['export-vouchers-descargados'] = array(
    'description' => 'Exporta archivos con informacion de los vouchers descargados.',
    'aliases' => array('evsd'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  // Nueva Version
  // drush_bbva_data_export_export_vouchers_usados
  $items['export-vouchers-usados'] = array(
    'description' => 'Exporta archivos con informacion de los vouchers usados.',
    'aliases' => array('evsu'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-users-login'] = array(
    'description' => 'Exporta archivo de ancho fijo con información de los usuarios logueados.',
    'aliases' => array('eul'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-motive-delete'] = array(
    'description' => 'Exporta archivo de ancho fijo con información de los motivos de baja.',
    'aliases' => array('emd'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-coupon-code'] = array(
    'description' => 'Exporta archivo de ancho fijo con información de los codigos de cupon.',
    'aliases' => array('ecc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

    $items['export-maestro-sorteos'] = array(
    'description' => 'Exporta archivo de ancho fijo con información el maestro de sorteos.',
    'aliases' => array('ems'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  $items['export-users-access'] = array(
    'description' => 'Exporta archivos con informacion de las usuario con acceso mayor al ultimo al dia anterior de la ejecucion',
    'aliases' => array('euacceso'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'odate' => 'Fecha ODATE',
      'job' => 'Jobname',
    ),
  );

  return $items;
}

/**
 * Callback for the export-no-client command
 */
function drush_bbva_data_export_export_maestro_sorteos($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Maestro sorteo'),
    'init_message' => t('Batch Export Maestro sorteo is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_master_sorteos',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Callback for the export-no-client command
 */
function drush_bbva_data_export_export_no_client($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export No Cliente'),
    'init_message' => t('Batch Export No Cliente is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_file_clients',array(0, 'export_no_clientes', $odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Callback for the export-suscriptos command
 */
function drush_bbva_data_export_export_suscriptos($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Suscriptos'),
    'init_message' => t('Batch Export Suscriptos is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_file_clients', array(null, 'export_suscriptos', $odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Callback for the export-promo-cupon command
 */
function drush_bbva_data_export_export_promo_cupon($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Coupons Promo'),
    'init_message' => t('Batch Export Coupons Promo is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_coupons_promo',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}



/**
 * Callback for the export-vouchers-downloaded command
 */
function drush_bbva_data_export_export_vouchers_downloaded($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Voucher Downloaded Information'),
    'init_message' => t('Batch Export Voucher Downloaded is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_vouchers_downloaded',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Callback for the export-vouchers-used command
 */
function drush_bbva_data_export_export_vouchers_used($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Voucher Used Information'),
    'init_message' => t('Batch Export Voucher Used is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_vouchers_used',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}


/**
 * Callback for the export-vouchers-descargados command
 */
function drush_bbva_data_export_export_vouchers_descargados($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Voucher Downloaded Information'),
    'init_message' => t('Batch Export Voucher Downloaded is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_vouchers_descargados',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Callback for the export-vouchers-usados command
 */
function drush_bbva_data_export_export_vouchers_usados($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Voucher Used Information'),
    'init_message' => t('Batch Export Voucher Used is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_vouchers_usados',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}



/**
 * Callback for the export-users-login command
 */
function drush_bbva_data_export_export_users_login($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Users Login'),
    'init_message' => t('Batch Export Users Login is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_users_login',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Callback for the export-motive-delete command
 */
function drush_bbva_data_export_export_motive_delete($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Motive Delete'),
    'init_message' => t('Batch Export Motives Delte is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_motive_delete',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}


/**
 * Callback for the export-coupon-code command
 */
function drush_bbva_data_export_export_coupon_code($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Coupon Code'),
    'init_message' => t('Batch Export Coupon Code is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_coupon_code',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

function drush_bbva_data_export_export_users_access($odate = NULL, $job = "") {
  //Set up batch job
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Export Usuario Acceso'),
    'init_message' => t('Batch Export Usuario Acceso is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_data_export_finished',
    'file' => drupal_get_path('module', 'bbva_data_export') . '/bbva_data_export.batch.inc',
  );

  //Define operaciones
  $batch['operations'][] = array('_bbva_data_export_users_access',array($odate, $job));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}