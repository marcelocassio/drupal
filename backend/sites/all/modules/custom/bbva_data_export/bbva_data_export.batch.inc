<?php

define('BBVA_DATA_EXPORT_SORTEO_TC', 'TJ');
define('BBVA_DATA_EXPORT_SORTEO_ST', 'ST');
define('BBVA_DATA_EXPORT_SORTEO_NC', 'NC');
define('BBVA_DATA_EXPORT_SORTEO_ALL', 'XX');

function _bbva_data_export_file_clients($type, $file_name, $odate, $job, &$context)
{
    echo '==Comienzo Export Suscriptos=='.PHP_EOL;

  //separador de campos
  $separador = ';';

  //select
  $select = get_data_cliente($type);

  //genera o abre archivo
  $fp = fopen(BBVA_DATA_EXPORT_FILE_URL.$file_name.'.txt', 'wb');

  //recorre query
  if (isset($fp)) {
      foreach ($select as $row => $record) {
          $tipo_doc_altamira = bbva_subscription_convert_tipo_doc_altamira($record->field_profile_document_type_value, $record->field_profile_document_number_value, $record->field_profile_sex_value);
          $dado_de_baja = 0;
          $motivo_de_baja = 0;

          $fecha_baja = '';
          if (!empty($row->field_profile_fecha_baja_value)) {
              $fecha_baja = date('Ymd', $row->field_profile_fecha_baja_value);
          }
      //contenido
      $content = '';
          $content .= $record->uid;
          $content .= $separador;
          $content .= $record->field_profile_celular_value;
          $content .= $separador;
          $content .= $record->field_profile_area_value;
          $content .= $separador;
          $content .= $tipo_doc_altamira;
          $content .= $record->field_profile_document_number_value;
          $content .= $separador;
          $content .= $record->name;  // Numero Cliente Altamira
      $content .= $separador;
          $content .= $record->field_profile_tipo_cliente_value;
          $content .= $separador;
          if (isset($record->field_profile_delete_motive_target_id) && $record->field_profile_delete_motive_target_id > 0) {
              $dado_de_baja = 1;
              $motivo_de_baja = $record->field_profile_delete_motive_target_id;
          }
          $content .= $dado_de_baja;
          $content .= $separador;
          $content .= $motivo_de_baja;
          $content .= $separador;
          $content .= $fecha_baja;

          fwrite($fp, $content.PHP_EOL);
      }
      fclose($fp);
  }
    echo '==Fin Export Suscriptos=='.PHP_EOL;
}

/*
 * Cupones promo
 */
function _bbva_data_export_coupons_promo($odate, $job, &$context)
{
    echo '==Comienzo Export Cupones Promotion=='.PHP_EOL;
    $cupons_promo = get_coupons_promo();
    $data = array();
    foreach ($cupons_promo as $row) {

        $nombre = sanear_string(trim($row->title));
        $nombre = substr($nombre, 0, 60);
        $data[] = array(
        'prefijo_de_linea' => 'D',
        'tipo_cupon' => '0000', // aun no lo tenemos
        'nombre' => str_pad($nombre, 60, ' ', STR_PAD_RIGHT),
        'inicio' => date('Ymd', $row->field_coupon_promo_date_start_value),
        'fin' => date('Ymd', $row->field_coupon_promo_date_end_value),
        'id' => str_pad($row->id, 8, '0', STR_PAD_LEFT),
        'codigo_cupon' => str_pad($row->field_coupon_number_promo_value, 6, '0', STR_PAD_LEFT),
        'descuento' => str_pad($row->field_coupon_promo_discount_value, 3, '0', STR_PAD_LEFT),
        'tope' => str_pad($row->field_coupon_promo_top_value, 5, '0', STR_PAD_LEFT),
        'comercio_suip_id' => str_pad($row->field_shop_id_value, 7, '0', STR_PAD_LEFT),
      );
    }

    _dump_file(BBVA_DATA_EXPORT_COUPONS_FILE_NAME.$odate, $data, '', '|'.PHP_EOL, $job, $odate);
    echo '==Fin Export Cupones Promotion=='.PHP_EOL;
}

function _bbva_data_export_vouchers_downloaded($odate, $job, &$context)
{
    echo '==Comienzo Export Voucher Descargados=='.PHP_EOL;
    $vouchers_descargados = get_vouchers_descargados($odate);
    $data = array();
    foreach ($vouchers_descargados as $row) {
        $tipo_doc = bbva_subscription_convert_tipo_doc_altamira($row->field_profile_document_type_value, $row->field_profile_document_number_value, $row->field_profile_sex_value);
        $data[] = array(
      'prefijo_de_linea' => 'D',
      'id' => str_pad($row->id, 14, '0', STR_PAD_LEFT),
      'codigo_voucher' => str_pad($row->title, 22, ' ', STR_PAD_RIGHT),
      'id_promo_coupon' => str_pad($row->coupon_id, 8, '0', STR_PAD_LEFT),
      'tipo_cupon' => '0000', // aun no lo tenemos
      'nro_cliente' => str_pad($row->name, 8, '0', STR_PAD_LEFT),
      'tipo_doc' => str_pad($tipo_doc, 2, '0', STR_PAD_LEFT),
      'nro_doc' => str_pad($row->field_profile_document_number_value, 13, '0', STR_PAD_LEFT),
      'fecha_descarga' => date('Ymd', $row->changed),
    );
    }

    _dump_file(BBVA_DATA_EXPORT_VOUCHERS_DOWNLOADS_FILE_NAME.$odate, $data, '', '|'.PHP_EOL, $job, $odate);
    echo '==Fin Export Voucher Descargados=='.PHP_EOL;
}

function _bbva_data_export_vouchers_used($odate, $job, &$context)
{
    echo '==Comienzo Export Voucher Usados=='.PHP_EOL;
    $vouchers_usados = get_vouchers_usados($odate);
    $data = array();
    foreach ($vouchers_usados as $row) {
        $tipo_doc = bbva_subscription_convert_tipo_doc_altamira($row->field_profile_document_type_value, $row->field_profile_document_number_value, $row->field_profile_sex_value);

        $fechaSola = '';
        $horaSola = '';
        if (empty($row->field_voucher_fecha_uso_value)) {
            $fechaSola = str_pad($fechaSola, 8, ' ', STR_PAD_RIGHT);
            $horaSola = str_pad($horaSola, 6, ' ', STR_PAD_RIGHT);
        } else {
            $fechaFormated = DateTime::createFromFormat('Y-m-d H:i:s', $row->field_voucher_fecha_uso_value);
            $fechaSola = $fechaFormated->format('Ymd');
            $horaSola = $fechaFormated->format('Hms');
        }

        $data[] = array(
      'prefijo_de_linea' => 'D',
      'id' => str_pad($row->id, 14, '0', STR_PAD_LEFT),
      'codigo_voucher' => str_pad($row->title, 22, ' ', STR_PAD_RIGHT),
      'id_promo_coupon' => str_pad($row->coupon_id, 8, '0', STR_PAD_LEFT),
      'tipo_cupon' => '0000', // aun no lo tenemos
      'nro_cliente' => str_pad($row->name, 8, '0', STR_PAD_LEFT),
      'tipo_doc' => str_pad($tipo_doc, 2, '0', STR_PAD_LEFT),
      'nro_doc' => str_pad($row->field_profile_document_number_value, 13, '0', STR_PAD_LEFT),
      'fecha_uso' => $fechaSola,
      'hora_uso' => $horaSola,
      'monto_compra' => str_pad($row->field_voucher_monto_compra_value, 12, '0', STR_PAD_LEFT),
      'monto_descuento' => str_pad($row->field_voucher_monto_descuento_value, 12, '0', STR_PAD_LEFT),
    );
    }

    _dump_file(BBVA_DATA_EXPORT_VOUCHERS_USED_FILE_NAME.$odate, $data, '', '|'.PHP_EOL, $job, $odate);
    echo '==Fin Export Voucher Usados=='.PHP_EOL;
}

function _bbva_data_export_users_login($odate, $job, &$context)
{
    echo '==Comienzo Export Logines=='.PHP_EOL;
    $users = get_users_login();

    $data = array();
    foreach ($users as $row) {
        $tipo_doc = bbva_subscription_convert_tipo_doc_altamira($row->field_profile_document_type_value, $row->field_profile_document_number_value, $row->field_profile_sex_value);

        $canal = BBVA_LOGIN_CANAL_NINGUNO;
        if (!empty($row->field_profile_canal_logueo_value)) {
            if ($row->field_profile_canal_logueo_value == 1) {
                $canal = BBVA_LOGIN_CANAL_WEB;
            } elseif ($row->field_profile_canal_logueo_value == 2) {
                $canal = BBVA_LOGIN_CANAL_APP;
            }
        }

        $fecha_logoff = '';
        $hora_logoff = '';
        if (empty($row->field_profile_fecha_log_off_value)) {
            $fecha_logoff = str_pad($fecha_logoff, 8, '9', STR_PAD_RIGHT);
            $hora_logoff = str_pad($hora_logoff, 6, '0', STR_PAD_RIGHT);
        } else {
            $fechaFormated = DateTime::createFromFormat('Y-m-d H:i:s', $row->field_profile_fecha_log_off_value);
            $fecha_logoff = $fechaFormated->format('Ymd');
            $hora_logoff = $fechaFormated->format('His');
        }

        $data[] = array(
      'prefijo_de_linea' => 'D',
      'nro_cliente' => str_pad($row->name, 8, '0', STR_PAD_LEFT),
      'tipo_doc' => str_pad($tipo_doc, 2, '0', STR_PAD_LEFT),
      'nro_doc' => str_pad($row->field_profile_document_number_value, 13, '0', STR_PAD_LEFT),
      'canal' => $canal,
      'fecha_login' => date('Ymd', $row->login),
      'hora_login' => date('His', $row->login),
      'fecha_logout' => $fecha_logoff,
      'hora_logout' => $hora_logoff,
    );
    }

    _dump_file(BBVA_DATA_EXPORT_USERS_LOGIN.$odate, $data, '', '|'.PHP_EOL, $job, $odate);
    echo '==Fin Export Logines=='.PHP_EOL;
}

function _bbva_data_export_coupon_code($odate, $job, &$context)
{
    echo '==Comienzo Export Codigos de Cupones=='.PHP_EOL;
    $cupons = get_coupon_numbers();

    $data = array();
    foreach ($cupons as $row) {
        $data[] = array(
      'prefijo_de_linea' => 'D',
      'id' => str_pad($row->field_coupon_number_promo_value, 6, '0', STR_PAD_LEFT),
      'name' => str_pad(sanear_string($row->name), 50, ' ', STR_PAD_RIGHT),
    );
    }

    _dump_file(BBVA_DATA_EXPORT_MAESTROS_COUPON_CODE.$odate, $data, '', '|'.PHP_EOL, $job, $odate);
    echo '==Fin Export Codigos de Cupones=='.PHP_EOL;
}

function _bbva_data_export_motive_delete($odate, $job, &$context){

  echo "==Comienzo Export Motivos de Baja==".PHP_EOL;

  $motivos = get_motivos_baja();
  $data = array();
  $data[] = array(
    'prefijo_de_linea' => 'D',
    'id' => str_pad("0", 8, "0", STR_PAD_LEFT),
    'name' => str_pad("SIN MOTIVO", 50, " ", STR_PAD_RIGHT),
  );


  foreach ($motivos as $row) {
    $data[] = array(
      'prefijo_de_linea' => 'D',
      'id' => str_pad($row->id, 8, "0", STR_PAD_LEFT),
      'name' => str_pad(sanear_string($row->title), 50, " ", STR_PAD_RIGHT),
    );
  }

  _dump_file(BBVA_DATA_EXPORT_MAESTROS_MOTIVOS . $odate, $data, '', '|' . PHP_EOL, $job,$odate);
  echo "==Fin Export Motivos de Baja==".PHP_EOL;
}

function _dump_file($filename, $data, $separator, $line_separator, $job, $odate, $ancho_fijo = 0)
{
  $filename = BBVA_DATA_EXPORT_FILE_URL.$filename.'.txt';

  $ancho = 0;
  if (empty($data[0])) {
      echo 'No hay datos para exportar'.PHP_EOL;
  } else {
      $ancho = strlen(implode('', $data[0]));
  }

  if ($ancho_fijo != 0) {
    $ancho = $ancho_fijo;
  }

  // header
  $header = 'H'; // prefijo
  $header .= str_pad($odate, 8, '0', STR_PAD_LEFT); // odate
  $header .= date('Ymd'); // fecha de ejecucion
  $header .= date('H:i:s'); // hora de ejecucion
  $header .= str_pad($job, 8, ' ', STR_PAD_RIGHT); // jobname
  $header = str_pad($header, $ancho, ' ', STR_PAD_RIGHT); // completo con espacios
  // trailer
  $trailer = 'T'; // prefijo
  $trailer .= str_pad(count($data), 10, '0', STR_PAD_LEFT); // cantidad de registros
  $trailer = str_pad($trailer, $ancho, ' ', STR_PAD_RIGHT); // completo con espacios

  // Inserto el header al inicio y el trailer al final
  array_unshift($data, array($header));
    array_push($data, array($trailer));

    try {
        $fp = fopen($filename, 'wb');
        $content = '';

        foreach ($data as $row) {
            $content = implode($separador, $row);
            $content .= $line_separator;
            fwrite($fp, $content);
        }

        fclose($fp);
    } catch (Exception $e) {
        drupal_set_message(t('Hubo un error al intentar escribir el archivo "@filename"', array('@filename' => $filename)), 'error');
    }
}

function _bbva_data_export_master_sorteos($odate, $job, &$context)
{
    echo '==Comienzo Export Maestro de Sorteo=='.PHP_EOL;
    try {
        $data = array();
        if ($odate == 'ALL') {
            $sorteos = bbva_data_export_get_all_sorteos();
        } else {
            $sorteos = bbva_data_export_get_all_sorteos($odate);
        }

        foreach ($sorteos as $sorteo) {
            $title = $sorteo->title;

            $fechaFormated = DateTime::createFromFormat('Y-m-d H:i:s', $sorteo->field_experience_fechas_vigencia_value);
            $fdesde = $fechaFormated->format('Ymd');
            $fechaFormated = DateTime::createFromFormat('Y-m-d H:i:s', $sorteo->field_experience_fechas_vigencia_value2);
            $fhasta = $fechaFormated->format('Ymd');
            $fechaFormated = DateTime::createFromFormat('Y-m-d H:i:s', $sorteo->field_experience_fecha_sortea_value);
            $fsorteo = $fechaFormated->format('Ymd');

            if (isset($sorteo->field_experience_check_code_uniq_value) &&
        $sorteo->field_experience_check_code_uniq_value == 0) {
                $nro_sorteo_tc = $sorteo->field_experience_code_sorteo_tc_value;
                $nro_sorteo_st = $sorteo->field_experience_code_sorteo_st_value;
                $nro_sorteo_nc = $sorteo->field_experience_code_sorteo_nc_value;
                $data[] = bbva_data_export_build_line_array($nro_sorteo_tc, $title, BBVA_DATA_EXPORT_SORTEO_TC, $fdesde, $fhasta, $fsorteo);
                $data[] = bbva_data_export_build_line_array($nro_sorteo_st, $title, BBVA_DATA_EXPORT_SORTEO_ST, $fdesde, $fhasta, $fsorteo);
                $data[] = bbva_data_export_build_line_array($nro_sorteo_nc, $title, BBVA_DATA_EXPORT_SORTEO_NC, $fdesde, $fhasta, $fsorteo);
            } else {
                $nro_sorteo = $sorteo->field_experience_code_sorteo_value;
                $data[] = bbva_data_export_build_line_array($nro_sorteo, $title, BBVA_DATA_EXPORT_SORTEO_ALL, $fdesde, $fhasta, $fsorteo);
            }
        }
        _dump_file(BBVA_DATA_EXPORT_MAESTRO_SORTEOS_FILE_NAME.$odate, $data, '', '|'.PHP_EOL, $job, $odate, 133);
    } catch (Exception $e) {
        echo '*Excepcion Generada: '.$e->getMessage().PHP_EOL;
        echo '*Finaliza con errores'.PHP_EOL;
        exit(1);
    }
    echo '==Fin Export Maestro de Sorteo=='.PHP_EOL;
}

function bbva_data_export_build_line_array($nro_sorteo, $title, $segmento, $fdesde, $fhasta, $fsorteo)
{
    $array_line = array(
      'prefijo_de_linea' => 'D',
      'nro_sorteo' => str_pad(trim($nro_sorteo), 6, '0', STR_PAD_LEFT),
      'nombre' => str_pad(substr(sanear_string(trim($title)), 0, 100), 100, ' ', STR_PAD_RIGHT),
      'segmento' => str_pad($segmento, 2, ' ', STR_PAD_RIGHT),
      'fecha_desde' => str_pad(substr(trim($fdesde), 0, 8), 8, ' ', STR_PAD_RIGHT),
      'fecha_hasta' => str_pad(substr(trim($fhasta), 0, 8), 8, ' ', STR_PAD_RIGHT),
      'fecha_sorteo' => str_pad(substr(trim($fsorteo), 0, 8), 8, ' ', STR_PAD_RIGHT),
    );

    return $array_line;
}

function bbva_data_export_finished($success, $results, $operations)
{
    if ($success) {
        drupal_set_message(t('File create is complete.'));
    } else {
        $error_operation = reset($operations);
        $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], true),
    ));
        drupal_set_message($message, 'error');
    }
}

function _bbva_data_export_vouchers_descargados($odate, $job, &$context)
{
    echo '==Comienzo Export Voucher Descargados=='.PHP_EOL;
    $vouchers_descargados = _bbva_data_export_get_vouchers_descargados($odate);
    $data = array();
    foreach ($vouchers_descargados as $row) {

      $fechaFormated = DateTime::createFromFormat('Y-m-d H:i:s', $row->fecha_descarga);
        $fechaSola = '';
        $horaSola = '';
        if (empty($fechaFormated)) {
            $fechaSola = str_pad($fechaSola, 8, ' ', STR_PAD_RIGHT);
            $horaSola = str_pad($horaSola, 6, ' ', STR_PAD_RIGHT);
        } else {
            $fechaSola = $fechaFormated->format('Ymd');
            $horaSola = $fechaFormated->format('His');
        }

      $data[] = array(
      'prefijo_de_linea' => 'D',
      'id' => str_pad($row->id, 14, '0', STR_PAD_LEFT),
      'codigo_voucher' => str_pad($row->title, 22, ' ', STR_PAD_RIGHT),
      'id_promo_coupon' => str_pad($row->id_cupon, 8, '0', STR_PAD_LEFT),
      'tipo_cupon' => '0000', // aun no lo tenemos
      'nro_cliente' => str_pad($row->name_user, 8, '0', STR_PAD_LEFT),
      'tipo_doc' => str_pad('', 2, '0', STR_PAD_LEFT),
      'nro_doc' => str_pad('', 13, '0', STR_PAD_LEFT),
      'fecha_descarga' => $fechaSola,
    );
    }

    _dump_file(BBVA_DATA_EXPORT_VOUCHERS_DOWNLOADS_FILE_NAME.$odate, $data, '', '|'.PHP_EOL, $job, $odate);
    echo '==Fin Export Voucher Descargados=='.PHP_EOL;
}
function _bbva_data_export_vouchers_usados($odate, $job, &$context)
{
    echo '==Comienzo Export Voucher Usados=='.PHP_EOL;

    if ($odate) {
        $fechaFormatedTemp = DateTime::createFromFormat('Ymd', $odate);
        $odate = array();
        $odate['start'] = $fechaFormatedTemp->format('Y-m-d 00:00:00');
        $odate['end'] = $fechaFormatedTemp->format('Y-m-d 23:59:59');
        $odate['no_hour'] = $fechaFormatedTemp->format('Ymd');
    } else {
        $odate['no_hour'] = date('Ymd');
    }

    $vouchers_usados = _bbva_data_export_get_vouchers_usados($odate);

    $data = array();
    foreach ($vouchers_usados as $row) {
        $fechaFormated = DateTime::createFromFormat('Y-m-d H:i:s', $row->fecha_uso);
        $fechaSola = '';
        $horaSola = '';
        if (empty($fechaFormated)) {
            $fechaSola = str_pad($fechaSola, 8, ' ', STR_PAD_RIGHT);
            $horaSola = str_pad($horaSola, 6, ' ', STR_PAD_RIGHT);
        } else {
            $fechaSola = $fechaFormated->format('Ymd');
            $horaSola = $fechaFormated->format('His');
        }

      $monto_compra = number_format($row->monto_compra, 2, '.', '');
      $monto_descuento = number_format($row->monto_descuento, 2, '.', '');

        $data[] = array(
      'prefijo_de_linea' => 'D',
      'id' => str_pad($row->id, 14, '0', STR_PAD_LEFT),
      'codigo_voucher' => str_pad($row->title, 22, ' ', STR_PAD_RIGHT),
      'id_promo_coupon' => str_pad($row->id_cupon, 8, '0', STR_PAD_LEFT),
      'tipo_cupon' => '0000', // aun no lo tenemos
      'nro_cliente' => str_pad($row->name_user, 8, '0', STR_PAD_LEFT),
      'tipo_doc' => str_pad('', 2, '0', STR_PAD_LEFT),
      'nro_doc' => str_pad('', 13, '0', STR_PAD_LEFT),
      'fecha_uso' => $fechaSola,
      'hora_uso' => $horaSola,
      'monto_compra' => str_pad($monto_compra, 12, '0', STR_PAD_LEFT),
      'monto_descuento' => str_pad($monto_descuento, 12, '0', STR_PAD_LEFT),
    );
    }

    _dump_file(BBVA_DATA_EXPORT_VOUCHERS_USED_FILE_NAME.$odate['no_hour'], $data, '', '|'.PHP_EOL, $job, $odate['no_hour'],110);
    echo '==Fin Export Voucher Usados=='.PHP_EOL;
}



function _bbva_data_export_users_access($odate, $job, &$context)
{
    echo '==Comienzo Export Acceso=='.PHP_EOL;
    $users = get_users_acceso($odate);

    $data = array();
    foreach ($users as $row) {



        $data[] = array(
      'prefijo_de_linea' => 'D',
      'nro_cliente' => str_pad($row->name, 8, '0', STR_PAD_LEFT),
      'fecha_acceso' => date('Ymd', $row->access),
      'hora_acceso' => date('His', $row->access),
      'evento' => str_pad('', 30, ' ', STR_PAD_LEFT),
  
    );
    }

    _dump_file(BBVA_DATA_EXPORT_USERS_ACCESS.$odate, $data, '', '|'.PHP_EOL, $job, $odate);
    echo '==Fin Export accesos=='.PHP_EOL;
}