<?php

function get_data_cliente($type = null)
{
    $query = db_select('field_data_field_profile_tipo_cliente', 'ftc');

    $query->leftJoin('users', 'u', 'u.uid = ftc.entity_id');
    $query->leftJoin('field_data_field_profile_celular', 'fpc', 'fpc.entity_id = ftc.entity_id');
    $query->leftJoin('field_data_field_profile_area', 'fpa', 'fpa.entity_id = ftc.entity_id');
    $query->leftJoin('field_data_field_profile_document_type', 'fpdt', 'fpdt.entity_id = ftc.entity_id');
    $query->leftJoin('field_data_field_profile_document_number', 'fpdn', 'fpdn.entity_id = ftc.entity_id');
    $query->leftJoin('field_data_field_profile_sex', 'fdps', 'fdps.entity_id = ftc.entity_id');
    $query->leftJoin('field_data_field_profile_delete_motive', 'fpdm', 'fpdm.entity_id = ftc.entity_id');
    $query->leftJoin('field_data_field_profile_fecha_baja', 'fpfb', 'fpfb.entity_id = ftc.entity_id');
    $query->leftJoin('users_roles', 'ur', 'ur.uid = ftc.entity_id');

    $query->fields('u', array('name', 'uid'));
    $query->fields('fpc', array('field_profile_celular_value'));
    $query->fields('fpa', array('field_profile_area_value'));
    $query->fields('fpdt', array('field_profile_document_type_value'));
    $query->fields('fpdn', array('field_profile_document_number_value'));
    $query->fields('fdps', array('field_profile_sex_value'));
    $query->fields('ftc', array('field_profile_tipo_cliente_value'));
    $query->fields('fpdm', array('field_profile_delete_motive_target_id'));
    $query->fields('fpfb', array('field_profile_fecha_baja_value'));

    $query = $query->condition('ftc.entity_type', 'user');
    $query = $query->condition('ur.rid', '11');

    if ($type !== null) {
        $query = $query->condition('ftc.field_profile_tipo_cliente_value', $type);
    }

    $resultCliente = $query->execute()->fetchAll();

    return $resultCliente;
}

function get_coupons_promo()
{
    $query = db_select('eck_coupon_promo', 'ecp');
    $query->leftJoin('field_data_field_coupon_promo_numero', 'n', 'ecp.id = n.entity_id AND n.entity_type = :type', array(':type' => 'coupon_promo'));
    $query->leftJoin('field_data_field_coupon_number_promo', 'np', 'n.field_coupon_promo_numero_tid = np.entity_id');
    $query->leftJoin('field_data_field_coupon_promo_date_start', 'ds', 'ecp.id = ds.entity_id AND ds.entity_type = :type', array(':type' => 'coupon_promo'));
    $query->leftJoin('field_data_field_coupon_promo_date_end', 'de', 'ecp.id = de.entity_id AND de.entity_type = :type', array(':type' => 'coupon_promo'));
    $query->leftJoin('field_data_field_coupon_promo_discount', 'd', 'ecp.id = d.entity_id AND d.entity_type = :type', array(':type' => 'coupon_promo'));
    $query->leftJoin('field_data_field_coupon_promo_top', 't', 'ecp.id = t.entity_id AND t.entity_type = :type', array(':type' => 'coupon_promo'));
    $query->leftJoin('field_data_field_coupon_promo_shop', 'sh', 'ecp.id = sh.entity_id AND sh.entity_type = :type', array(':type' => 'coupon_promo'));
    $query->leftJoin('field_data_field_shop_id', 'shid', 'sh.field_coupon_promo_shop_target_id = shid.entity_id');
    $query->fields('ecp', array('id', 'title'));
    $query->fields('np', array('field_coupon_number_promo_value'));
    $query->fields('ds', array('field_coupon_promo_date_start_value'));
    $query->fields('de', array('field_coupon_promo_date_end_value'));
    $query->fields('d', array('field_coupon_promo_discount_value'));
    $query->fields('t', array('field_coupon_promo_top_value'));
    $query->fields('shid', array('field_shop_id_value'));
    $query->condition('ecp.type', 'coupon_promo');

    $result = $query->execute()->fetchAll();

    return $result;
}

function get_vouchers_descargados($odate)
{
    $query = db_select('eck_voucher', 'ev');
    $query->leftJoin('field_data_field_voucher_coupon_promo', 'vcp', 'ev.id = vcp.entity_id');
    $query->leftJoin('eck_coupon_promo', 'ecp', 'vcp.field_voucher_coupon_promo_target_id = ecp.id');
    $query->leftJoin('field_data_field_voucher_user', 'vu', 'ev.id = vu.entity_id');
    $query->innerJoin('users', 'u', 'vu.field_voucher_user_target_id = u.uid');
    $query->leftJoin('field_data_field_profile_document_type', 'pdt', 'u.uid = pdt.entity_id');
    $query->leftJoin('field_data_field_profile_document_number', 'pdn', 'u.uid = pdn.entity_id');
    $query->leftJoin('field_data_field_profile_sex', 'ps', 'u.uid = ps.entity_id');
    $query->leftJoin('field_data_field_voucher_state', 'vs', 'ev.id = vs.entity_id');
    $query->fields('ev', array('id', 'title', 'changed'));
    $query->addField('ecp', 'id', 'coupon_id');
    $query->fields('u', array('name'));
    $query->fields('pdt', array('field_profile_document_type_value'));
    $query->fields('pdn', array('field_profile_document_number_value'));
    $query->fields('ps', array('field_profile_sex_value'));
    $query->condition('vs.field_voucher_state_value', '2', '=');
  //$query->condition('ev.changed', $odate, '=');

  $result = $query->execute()->fetchAll();

    return $result;
}

function get_vouchers_usados($odate)
{
    $query = db_select('eck_voucher', 'ev');
    $query->leftJoin('field_data_field_voucher_coupon_promo', 'vcp', 'ev.id = vcp.entity_id');
    $query->leftJoin('eck_coupon_promo', 'ecp', 'vcp.field_voucher_coupon_promo_target_id = ecp.id');
    $query->leftJoin('field_data_field_voucher_user', 'vu', 'ev.id = vu.entity_id');
    $query->innerJoin('users', 'u', 'vu.field_voucher_user_target_id = u.uid');
    $query->leftJoin('field_data_field_profile_document_type', 'pdt', 'u.uid = pdt.entity_id');
    $query->leftJoin('field_data_field_profile_document_number', 'pdn', 'u.uid = pdn.entity_id');
    $query->leftJoin('field_data_field_profile_sex', 'ps', 'u.uid = ps.entity_id');
    $query->leftJoin('field_data_field_voucher_state', 'vs', 'ev.id = vs.entity_id');
    $query->leftJoin('field_data_field_voucher_monto_compra', 'vmc', 'ev.id = vmc.entity_id');
    $query->leftJoin('field_data_field_voucher_monto_descuento', 'vmd', 'ev.id = vmd.entity_id');
    $query->leftJoin('field_data_field_voucher_fecha_uso', 'vfu', 'ev.id = vfu.entity_id');
    $query->fields('ev', array('id', 'title'));
    $query->addField('ecp', 'id', 'coupon_id');
    $query->fields('ecp', array('id'));
    $query->fields('u', array('name'));
    $query->fields('pdt', array('field_profile_document_type_value'));
    $query->fields('pdn', array('field_profile_document_number_value'));
    $query->fields('ps', array('field_profile_sex_value'));
    $query->fields('vmc', array('field_voucher_monto_compra_value'));
    $query->fields('vmd', array('field_voucher_monto_descuento_value'));
    $query->fields('vfu', array('field_voucher_fecha_uso_value'));
    $query->condition('vs.field_voucher_state_value', '3', '=');

    $result = $query->execute()->fetchAll();

    return $result;
}

function get_users_login()
{
    $query = db_select('users', 'u');
    $query->leftJoin('field_data_field_profile_document_type', 'pdt', 'u.uid = pdt.entity_id');
    $query->leftJoin('field_data_field_profile_document_number', 'pdn', 'u.uid = pdn.entity_id');
    $query->leftJoin('field_data_field_profile_sex', 'ps', 'u.uid = ps.entity_id');
    $query->leftJoin('field_data_field_profile_canal_logueo', 'cl', 'u.uid = cl.entity_id');
    $query->leftJoin('field_data_field_profile_fecha_log_off', 'lo', 'u.uid = lo.entity_id');
    $query->leftJoin('users_roles', 'ur', 'u.uid = ur.uid');
    $query->fields('u', array('name', 'access', 'login'));
    $query->fields('pdt', array('field_profile_document_type_value'));
    $query->fields('pdn', array('field_profile_document_number_value'));
    $query->fields('ps', array('field_profile_sex_value'));
    $query->fields('cl', array('field_profile_canal_logueo_value'));
    $query->fields('lo', array('field_profile_fecha_log_off_value'));
    $query->condition('ur.rid', '11', '=');
    $query->condition('u.login', '0', '>');

    $result = $query->execute()->fetchAll();

    return $result;
}

function get_users_acceso($odate)
{
    $start=new DateTime();
    $end=new DateTime();
    $start->setTimestamp( strtotime($odate));
    $end->setTimestamp( strtotime($odate));
    $start->setTime(0,0,0);
    $end->setTime(23,59,59);

    $query = db_select('users', 'u');
    $query->leftJoin('users_roles', 'ur', 'u.uid = ur.uid');
    $query->fields('u', array('name', 'access', 'login'));
    $query->condition('ur.rid', '11', '=');
    $query->condition('u.login', '0', '>');
    $query->condition('u.access', array($start->getTimestamp(),$end->getTimestamp()) , 'BETWEEN');

    $result = $query->execute()->fetchAll();

    return $result;
}



function get_coupon_numbers()
{
    $query = db_select('taxonomy_term_data', 'ttd');
    $query->leftJoin('field_data_field_coupon_number_promo', 'cnp', 'cnp.entity_id = ttd.tid');

    $query->fields('ttd', array('name'));
    $query->fields('cnp', array('field_coupon_number_promo_value'));

    $query->condition('ttd.vid', '21', '=');

    $result = $query->execute()->fetchAll();

    return $result;
}

function get_motivos_baja()
{
    $query = db_select('eck_motive', 'em');
    $query->fields('em', array('id', 'title'));

    $query->condition('em.type', 'subscription_delete_motive');

    $result = $query->execute()->fetchAll();

    return $result;
}

function get_notifications()
{
    $query = db_select('eck_bbva_message_plan', 'ebmp');

    $query->leftJoin('users', 'u', 'ebmp.uid = u.uid');
    $query->leftJoin('field_data_field_message_plan_date_send', 'ds', 'ds.entity_id = ebmp.id');

    $query->fields('ebmp', array('id', 'created'));
    $query->fields('u', array('name'));
    $query->fields('ds', array('field_message_plan_date_send_value'));

    $result = $query->execute()->fetchAll();

    return $result;
}

function bbva_data_export_get_all_sorteos($date = null)
{
    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_experience', 'e', 'n.nid = e.entity_id');
    $query->leftJoin('field_data_field_experience_code_sorteo', 'cs', 'n.nid = cs.entity_id');
    $query->leftJoin('field_data_field_experience_code_sorteo_nc', 'csn', 'n.nid = csn.entity_id');
    $query->leftJoin('field_data_field_experience_code_sorteo_st', 'css', 'n.nid = css.entity_id');
    $query->leftJoin('field_data_field_experience_code_sorteo_tc', 'csc', 'n.nid = csc.entity_id');
    $query->leftJoin('field_data_field_experience_es_sorteo', 'es', 'n.nid = es.entity_id');
    $query->leftJoin('field_data_field_experience_fechas_vigencia', 'fv', 'n.nid = fv.entity_id');
    $query->leftJoin('field_data_field_experience_fecha_sortea', 'fs', 'n.nid = fs.entity_id');
    $query->leftJoin('field_data_field_experience_check_code_uniq', 'ck', 'n.nid = ck.entity_id');

    $query->fields('n', array('nid'));
    $query->fields('n', array('title'));
    $query->fields('cs', array('field_experience_code_sorteo_value'));
    $query->fields('csn', array('field_experience_code_sorteo_nc_value'));
    $query->fields('css', array('field_experience_code_sorteo_st_value'));
    $query->fields('csc', array('field_experience_code_sorteo_tc_value'));
    $query->fields('fv', array('field_experience_fechas_vigencia_value'));
    $query->fields('fv', array('field_experience_fechas_vigencia_value2'));
    $query->fields('fs', array('field_experience_fecha_sortea_value'));
    $query->fields('ck', array('field_experience_check_code_uniq_value'));

    $query->condition('es.field_experience_es_sorteo_value', 1);
    $query->condition('n.type', 'experience');
    if (isset($date)) {
        $fecha = new DateTime($date);
        $query->condition('fv.field_experience_fechas_vigencia_value2', $date);
    }

    $result = $query->execute()->fetchAll();

    return $result;
}
function _bbva_data_export_get_vouchers_descargados($odate = null)
{
    // u.name es el altamira
  $query = db_select('bbva_vouchers', 'v');
    $query->fields('v', array('id', 'state', 'id_cupon', 'code', 'num_bin_visa', 'code_promo_visa', 'fecha_activo', 'fecha_uso', 'fecha_descarga', 'monto_compra', 'monto_descuento', 'title', 'name_user'));
    $query->condition('v.state', 5, '=');

    $result = $query->execute()->fetchAll();

    return $result;
}

function _bbva_data_export_get_vouchers_usados($odate = null)
{
    // u.name es el altamira
  $query = db_select('bbva_vouchers', 'v');
    $query->fields('v', array('id', 'state', 'id_cupon', 'code', 'num_bin_visa', 'code_promo_visa', 'fecha_activo', 'fecha_uso', 'fecha_descarga', 'monto_compra', 'monto_descuento', 'title', 'name_user'));
    $query->condition('v.state', 3, '=');
    $result = $query->execute()->fetchAll();

    return $result;
}
