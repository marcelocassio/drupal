<?php

/*
 * Implements hook_drush_command().
 */
function bbva_entity_taxpayer_drush_command() {

  $items['vuelco-taxpayer'] = array(
    'description' => 'Realiza el vuelco de las tarjetas',
    'aliases' => array('vuelcoTaxpayer'),
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_entity_taxpayer_vuelco_taxpayer() {
  import_taxpayer();
  drupal_set_message("Taxpayer vuelco echo", 'status', FALSE);

}
