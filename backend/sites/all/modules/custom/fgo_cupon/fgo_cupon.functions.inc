<?php

function fgo_cupon_get_cupones_suip() {
  $cupones = array();
  try {
    $cs = \CuponesServiceSuip::getInstance();
    $cupones = $cs->getPromoCuponAProcesarAlta();
  } catch (Exception $e) {}

  return $cupones;
}

function fgo_cupon_archivo_visa_cupones($file, $state_visa, $promo_numero_visa = null, $id_comercio = null, $fecha_desde = null, $fecha_hasta = null, $nombre_estab = null, $descuento, $tope) {

  $content = $state_visa;

  $content.= '   ';

  // //NUMEST2: Entity relacionado shop  el field id
  $content.= zerofill(substr($id_comercio, 0, 8), 8) . substr($promo_numero_visa, 0, 4);

  // //NOMBRE-ESTAB: Informar el title de la entitiy relacionada shop
  $content.= zerofill_espacio(strtoupper(substr($nombre_estab, 0, 40)), 40);

  // //solo los primeros 40 caracteres
  // //FECHA-DESDE: Informar el campo data start coupon promotion
  $content.= date('ymd', $fecha_desde);

  // //FECHA-HASTA: Informar el campo data end de la entity coupon promotion
  $content.= date('ymd', $fecha_hasta);

  // //FILLER: Tres espacios en blanco
  $content.= '   ';

  // //PAIS: Dos espacios en blanco
  $content.= '  ';

  // //RUBRO-NAC: Cuatro espacios en blanco
  $content.= '    ';

  // //RUBRO-INTER: Cuatro espacios en blanco
  $content.= '    ';

  // //MONEDA: Tres espacios en blanco
  $content.= '   ';

  // //ESTADO: Siempre 30 (Comercio normal)
  $content.= '30';

  // //PROVINCIA: Dos espacios en blanco
  $content.= '  ';

  // //BANCO: Tres espacios en blanco
  $content.= '   ';

  // //SUCURSAL: Tres espacios en blanco
  $content.= '   ';

  // //DESCUENTO: Informar el campo discount de la entity coupon promotion
  if ($descuento == '100') {

      $descuento = '00';
  }

  $content.= zerofill(substr($descuento, 0, 2), 2);

  // //FILLER: Siete espacios en blanco
  $content.= '       ';

  // //MAX-CUOTAS: Dos espacios en blanco
  $content.= '  ';

  // //TIPO-PLAN: Un espacio en blanco
  $content.= ' ';

  // //FILLER: Cuatro espacios en blanco
  $content.= '    ';

  // //GRUPO-CERR: Un espacio en blanco
  $content.= ' ';

  // // //TOPE: Informar el campo tope de la entity coupon promotion
  $content.= zerofill(substr($tope, 0, 5), 5);

  // // //FILLER: Cuatro espacios en blanco
  $content.= '      ';

  // //SALTO DE CARRO

  fwrite($file, $content . PHP_EOL);
}

function fgo_cupon_generar_vouchers($fpVoucher, $codes_vouchers, $promo_type, $promo_numero_tid, $promo_tid, $fecha_end, $segmentado = false, $users, $promo_numero_code) {

    //Abro el archivo para escribir las promociones de visa

    foreach ($codes_vouchers as $key => $value) {

        $user = NULL;
        $title = FGO_CUPON_NUM_VISA . " - " . $promo_numero_code . " - " . $code;
        $code = $value;

        $voucher = new Voucher();
        $voucher->id_cupon       =$promo_tid;
        $voucher->code_promo_visa=$promo_numero_code;
        $voucher->code           =$code;

        if ($segmentado) {

            //por si se cargaron mal los archivos valido que exista si no existe y es segmentada  di de alta todos los vouchers necesarios
            if (array_key_exists($key, $users)) {
                $user = $users[$key];
                $voucher->name_user=$user;
            }
            else {
                break;
            }
        }

        switch ($promo_type) {
            case '0':

                //Promo por Visa


                //enviado
                $voucher->state = 1;
                $voucher->num_bin_visa = FGO_CUPON_NUM_VISA;
                $voucher->title =$voucher->num_bin_visa." - ".$promo_numero_code." - ".$voucher->code;

                $voucher->create();

                fgo_cupon_archivo_visa_vouchers($fpVoucher, $promo_numero_code, $code, $fecha_end);
                break;
              case '3':

                    //Promo por Visa

                    //enviado
                    $voucher->state = 1;
                    $voucher->num_bin_visa = FGO_CUPON_NUM_VISA;
                    $voucher->title = $voucher->num_bin_visa.' - '.$promo_numero_code.' - '.$voucher->code;

                    $voucher->create();

                    fgo_cupon_archivo_visa_vouchers($fpVoucher, $promo_numero_code, $code, $fecha_end);
                    break;
            case '1':
            case '4':

                //Promo vouchers generados
                $voucher->state = 2;
                $voucher->num_bin_visa = 0;
                $voucher->title =$voucher->code;
                $voucher->create();


                break;
            case '2':

                //Promo vouchers generados
                $voucher->state = 4;
                $voucher->num_bin_visa = 0;
                $voucher->title =$voucher->code;
                $voucher->create();


        }

    }
}

function fgo_cupon_archivo_visa_vouchers($file, $promo_numero_visa = null, $code = null, $fecha_hasta = null) {

    //Generamos lineas para el archivo Voucher
    //NUMCUP: Num Bim Visa + las proximas 4  con el numero de la taxonomya relacionada al cupon_promo + las ultimas 6 con el campo de la entidad voucher generada  + 3 espacios en blanco
    $content = FGO_CUPON_NUM_VISA;
    $content.= zerofill(substr($promo_numero_visa, 0, 4), 4);
    $content.= $code;
    $content.= '   ';

    //LONG-NUMCUP
    $content.= '  ';

    //ESTADO
    $content.= '20';

    //LIM-COMPRA
    $content.= '000000001';

    //LIM-ADELANTO
    $content.= '000000000';

    //LIM-CUOTAS
    $content.= '000000000';

    //FECHA
    $content.= '         ';

    //CUENTA-ASOC
    $content.= '000000000000000';

    //LIM-PROP
    $content.= ' ';

    //AAMM-VTO: Informar el campo data end de la entity coupon promotion
    $content.= date('ym', $fecha_hasta);

    //FILLER
    $content.= ' ';
    fwrite($file, $content . PHP_EOL);
}