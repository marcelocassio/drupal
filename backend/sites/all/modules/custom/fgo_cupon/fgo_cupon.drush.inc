<?php

/**
 * Implements COMMANDFILE_drush_command().
 */
function fgo_cupon_drush_command() {
  $items = array();

  $items['fgo-cupon-envio'] = array(
    'description' => 'Envio de cupones a VISA.',
    'aliases' => array('fgo_ce'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['fgo-cupon-recibo-vouchers'] = array(
    'description' => 'Recibo de vouchers desde VISA.',
    'aliases' => array('fgo_cr'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_fgo_cupon_envio() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Cupon VISA - Envio'),
    'init_message' => t('Iniciando...'),
    'error_message' => t('Ha ocurrido un error'),
    'finished' => 'fgo_cupon_finished',
  );

  $batch['operations'][] = array('fgo_cupon_envio', array(''));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_fgo_cupon_recibo_vouchers() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Vouchers VISA - Recibo'),
    'init_message' => t('Iniciando...'),
    'error_message' => t('Ha ocurrido un error'),
    'finished' => 'fgo_cupon_finished',
  );

  $batch['operations'][] = array('fgo_cupon_recibo_vouchers', array(''));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

function fgo_cupon_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Proceso cupon/vouchers completado: @operations', array('@operations' => implode(", ", $operations))));
  } else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], true),
    ));
    drupal_set_message($message, 'error');
  }
}

function fgo_cupon_envio($data, &$context) {
  //Abro los archivos a escribir

  $fpCupon = fopen("/prod/jp00/cupones/enviar/coupon_promo_visa.txt", "wb");
  $fpVoucher = fopen("/prod/jp00/cupones/enviar/vouchers_promo_visa.txt", "wb");

    // Agrego los cupones de SUIP
  $cuponesSuip = fgo_cupon_get_cupones_suip();
  if(count($cuponesSuip) > 0) {
    foreach ($cuponesSuip as $cuponSuip) {
      // Busco si ya no creamos el cupon
      $cupon = new \Fgo\Bo\CuponBo();
      if($cupon->estaCreado($cuponSuip['idPromocion'])) {
        continue;
      }
      if (!isset($cuponSuip['idComunicacion']) || !is_numeric($cuponSuip['idComunicacion'])) {
        echo "PROMO NO GENERADA. La comunicacion aun no fue creada para la promo suip " . $cuponSuip['idPromocion']. PHP_EOL;
        continue;
      }
      try {
        // Busco o creo el codigo promo
        $codigo_promo = new \Fgo\Bo\CodigoPromoBo();
        $codigo_promo = $codigo_promo->buscarPorCodigoPromo($cuponSuip['codigoPromoCupon']);
        if($codigo_promo->idCodigoPromo == null) {
          $codigo_promo->codigoPromo = $cuponSuip['codigoPromoCupon'];
          $codigo_promo->guardar();
        }

        $fecha_desde = \DateTime::createFromFormat("Y-m-d", $cuponSuip['fechaDesdePromocion']);
        $fecha_hasta = \DateTime::createFromFormat("Y-m-d", $cuponSuip['fechaHastaPromocion']);

        // Creo el cupon
        $cupon->codigoPromo = $codigo_promo;
        $cupon->cuponSuip = $cuponSuip['idPromocion'];
        $cupon->tipoCupon = 0; // por ahora es solo cupon visa
        $cupon->comunicacionSuip = $cuponSuip['idComunicacion'];
        $cupon->vouchers = $cuponSuip['cantidadCupon'];
        $cupon->fechaDesde = $fecha_desde->getTimestamp();
        $cupon->fechaHasta = $fecha_hasta->getTimestamp();
        $cupon->guardar();

      } catch (Exception $e) {
        echo "Error al procesar PROMOS de SUIP: " . $e->getMessage() . PHP_EOL;
        echo "STACK: " . $e->getTraceAsString() . PHP_EOL;
      }
    }
  }

  $cupones = \Fgo\Bo\CuponBo::obtenerCupones(0);
  foreach ($cupones as $cupon) {
    $context['message'] = "[" . date('Y-m-d H:i:s') . "] Procesando cupon con id suip: " . $cupon->comunicacionSuip;
    if($cupon->vouchers > 0) {
      $vouchers = $cupon->generarVouchers($cupon->vouchers);
    }
    // ToDo crear vouchers
    if($cupon->codigoPromo->enviadoAVisa) {
      $state_promo_visa = "UP";
    } else {
      $state_promo_visa = "IN";
      $cupon->codigoPromo->enviadoAVisa = 1;
    }

    $tope = 0;
    if($comunicacionBo->beneficios[0]->tope) {
      $tope = $comunicacionBo->beneficios[0]->tope;
    }

    $comunicacionBo = new \Fgo\Bo\ComunicacionBo();
    $comunicacionBo = $comunicacionBo->buscarPorIdSuip($cupon->comunicacionSuip);
    fgo_cupon_archivo_visa_cupones($fpCupon, $state_promo_visa, $cupon->codigoPromo->codigoPromo, $comunicacionBo->comercio->idComercio, $cupon->fechaDesde, $cupon->fechaHasta, $comunicacionBo->comercio->nombre, $comunicacionBo->beneficios[0]->valor, $tope);

    $context['message'] = "[" . date('Y-m-d H:i:s') . "] Procesando vouchers de cupon con id suip: " . $cupon->comunicacionSuip;
    fgo_cupon_generar_vouchers($fpVoucher, $vouchers, $cupon->tipoCupon, null, $cupon->idCupon, $fecha_end, false, false, $cupon->codigoPromo->codigoPromo);

    $cupon->idEstado = 1;
    $cupon->guardar();
  }

  fclose($fpCupon);
  fclose($fpVoucher);

  return "Finalizó el proceso de envio de cupones y vouchers a VISA";
}

function fgo_cupon_recibo_vouchers($data, &$context) {

}