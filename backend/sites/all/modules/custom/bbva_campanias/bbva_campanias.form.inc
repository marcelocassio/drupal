<?php
/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 *
 * Types:  // Para una lista completa : https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7.x
 * fieldset , textfield , password , textarea , checkbox , radio
 *
 */
function bbva_campanias_campanias_cupones() {
  $form['bbva_campanias_form_cc'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Relación Campaña - Cupones'),
  );

  $form['bbva_campanias_form_cc']['bbva_campanias_titulo'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Titulo Campaña'),
    '#default_value' => "",
    '#description'   => '',
    '#required' => TRUE, // Puede ser TRUE
  );

  $form['bbva_campanias_form_cc']['bbva_campanias_fecha_inicio'] = array(
    '#type'          => 'date_popup',
    '#title'         => t('Fecha inicio de la campaña'),
    '#date_format' => 'd/m/Y',
    '#default_value' => date('Y-m-d 00:00:00', strtotime('+1 days')),
    '#required' => TRUE,
  );

  $form['bbva_campanias_form_cc']['bbva_campanias_fecha_final'] = array(
    '#type'          => 'date_popup',
    '#title'         => t('Fecha final de la campaña'),
    '#date_format' => 'd/m/Y',
    '#default_value' => date('Y-m-d', strtotime('+1 weeks +1 days')),
    '#required' => TRUE,
  );

  $form['bbva_campanias_form_cc']['bbva_campanias_beneficios'] = array(
    '#type' => 'checkboxes',
    '#title'         => t('Beneficios asociados'),
    '#options' => _bbva_campanias_beneficios_cupones_disponibles(),
    '#description'   => '',
    '#required' => TRUE,
  );

  $form['bbva_campanias_form_cc']['bbva_campanias_dialogo'] = array(
    '#markup' => '<p id="bbva_campanias_dialogo" style="display: none;"><span id="messagebenefitselected"></span></p>'
  );

  $jslink = " (function($) {
    var count = $('input[name*=\'bbva_campanias_beneficios\']:checked').length;
    $('#messagebenefitselected').html('Has seleccionado '+count+' beneficio/s');
    $( '#bbva_campanias_dialogo' ).dialog({
      autoOpen: false,
      title: 'Está seguro de su selección?',
      dialogClass: 'alert',
      modal: true,
      show: 'blind',
      resizable: false,
      open: function(event, ui) {
          $('.ui-dialog-titlebar-close', ui.dialog | ui).hide();
      },
      hide: 'explode',
      buttons: {
        Aceptar: function() {
          $( 'html body' ).css({
            'cursor': 'wait',
            'background-color': '#000000',
            'opacity': '0.5'
          });
          $( '#bbva-campanias-campanias-cupones' ).submit();
        },
        Cancelar: function() {
          $( this ).dialog( 'close' );
        }
      }
    });

    $('#bbva_campanias_dialogo').dialog('open');

   })(jQuery); return false;";


  $form['bbva_campanias_form_cc']['bbva_campanias_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Crear Campaña'),
    '#attributes' => array("onclick"=>$jslink),
  );

  return ($form);
}

/**
* Quitar el label del popup en la date
*/
function bbva_campanias_date_popup_process_alter(&$element, &$form_state, $context) {
  if ($element['#name'] == 'bbva_campanias_fecha_inicio' || $element['#name'] == 'bbva_campanias_fecha_final') {
    unset($element['date']['#description']);
    unset($element['date']['#title']);
  }
}

function bbva_campanias_campanias_cupones_validate($form_id, &$form_state){

  if ($form_id["#form_id"] == 'bbva_campanias_campanias_cupones') {

    $fecha_inicio  = $form_state['values']['bbva_campanias_fecha_inicio'];
    $fecha_final  = $form_state['values']['bbva_campanias_fecha_final'];

    if ($fecha_inicio < date('Y-m-d')) {
      form_set_error('bbva_campanias_fecha_inicio',t("La fecha de inicio no puede ser menor que el día de hoy"));
    }

    if ($fecha_final < $fecha_inicio) {
      form_set_error('bbva_campanias_fecha_final',t("La fecha final no puede ser menor que la fecha de inicio"));
    }
  }
}

function bbva_campanias_campanias_cupones_submit($form_id, &$form_state){

  if ($form_id["#form_id"] == 'bbva_campanias_campanias_cupones') {
    $beneficios_adheridos = $form_state['values']['bbva_campanias_beneficios'];

    $values = array('type' => 'campaign', 'status' => 1, 'comment' => 0, 'promote' => 1,);

    // Se crea la entity Campaign
    $entityCampaign = entity_create('campaign', $values);
    $wrapper_campaign = entity_metadata_wrapper('campaign', $entityCampaign);

    $wrapper_campaign->title->set($form_state['values']['bbva_campanias_titulo']);
    $wrapper_campaign->field_campaign_start_date->set(strtotime($form_state['values']['bbva_campanias_fecha_inicio']));
    $wrapper_campaign->field_campaign_end_date->set(strtotime($form_state['values']['bbva_campanias_fecha_final']));
    //. ' +1 day'

    $wrapper_campaign->save();

    $id_campania = $wrapper_campaign->getIdentifier();

    // Se agrega el id "Suip"
    $entity = entity_load_single('campaign', $id_campania);
    $entity->field_campaign_id['und'][0]['value'] = '10'.$id_campania;
    $entity->save();

    // Se dan de alta los beneficios adheridos a la campaña creada
    try {
      foreach ($beneficios_adheridos as $value) {
        if (!empty($value)) {
          $entity = entity_load_single('benefit', $value);
          $entity->field_benefit_campaign['und'][0]['target_id'] = $id_campania;
          $entity->save();
        }
      }
    }catch(Exception $e) {
      drupal_set_message("Error guardando los beneficios");
    }

    //$form_state['rebuild'] = TRUE;

    $message = t("La campaña se ha creado correctamente, buscarla por: <b>10" . $id_campania. " </b>");

    drupal_set_message($message);
    $form_state['redirect'] = 'admin/lista/campanias_cupones';
  }
}
