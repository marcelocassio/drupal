(function($) {

  var ctaMappingValues = {
    '0': 'field_call_to_acction_nav_app',
    '1': 'field_call_to_action_exp_1',
    '2': 'field_call_to_action_exp_2',
    '3': 'field_call_to_action_sorteos',
    '4': 'field_call_to_action_benefit',
    '5': 'field_call_to_action_rubro',
    '6': 'field_field_call_to_action_camp',
    '7': 'field_call_to_acction_link',
  };

  var checkEventChange = function(event_value) {
    setearEsquemaDefecto(event_value);
    setearValoresDefecto(event_value);
  };

  var checkComunicacion = function() {
    var map = {};
    /*$('.field-name-field-tipo:visible select[name*="[field_seccion_app"]').each(function() {
      // Chequeo que no haya secciones iguales
      var v_parent_seccion = $(this).closest(".fieldset-wrapper");
      var v_tmp = $(this).val() + "-" + $('input[name*="[' + ctaMappingValues[$(this).val()] + ']"', v_parent_seccion).val();
      map[v_tmp] = '1';
      // Chequeo que la sección no sea igual que el link
      var v_parent_cta = $(this).closest("td");
      var v_select_cta = $('.field-name-field-call-to-action select[name*="[field_call_to_action"]', v_parent_cta).val();
      var v_cta_tmp = v_select_cta + "-" + $('.field-name-field-call-to-action input[name*="[' + ctaMappingValues[v_select_cta] + ']"', v_parent_cta).val();
      // Si la sección y el link coinciden lanzo un error
      if(v_tmp == v_cta_tmp) {
        alert("La sección elegida no puede ser igual al link de destino");
        return false;
      }
    });
    // Si hay secciones iguales lanzo un error
    if(Object.keys(map).length < $('.field-name-field-tipo:visible select[name*="[field_seccion_app]"]').length) {
      alert("No puede elegir dos veces la misma sección");
      return false;
    }*/
  };

  var customizarLabels = function() {
    var labels = $(".field-name-field-campana-vigencia label");
    $(labels[0]).text("Fecha inicio");
    $(labels[1]).hide();
    $(labels[2]).text("Fecha fin");
  };

  var setearEsquemaDefecto = function(nombre_evento) {
    var input_con_cupon = $(':input[name="field_campana_con_cupon[und]"]');
    $(".js-esq-defecto").remove();

    if(nombre_evento != 'bienvenida' && nombre_evento != 'cumpleanos') {
      if(input_con_cupon.is(":checked")) {
        input_con_cupon.click().change();
      }
    }
    if(nombre_evento == 'recordatorio') {
      $('.field-name-field-campana-barra-inferior .field-name-field-call-to-action').hide();
      var markup = $('<div class="messages warning"></div>');
      markup.addClass("js-esq-defecto");
      markup.text("El link es hacia el login");
      $(".field-name-field-campana-barra-texto2").append(markup);
    } else {
      $('.field-name-field-campana-barra-inferior .field-name-field-call-to-action').show();
    }
    if(nombre_evento == 'listado') {
      $(':input[name="field_campana_target[und]"]').attr('disabled', false);
      $(':input[name="field_campana_target[und]"][value="0"]').attr('checked', true).change();
      $(':input[name="field_campana_target[und]"]').attr('disabled', true);
    } else {
      $(':input[name="field_campana_target[und]"]').attr('disabled', false);
    }
    if(nombre_evento == 'bienvenida' || nombre_evento == 'cumpleanos' || nombre_evento == 'aviso_tc') {
      $('.field-name-field-campana-popup .field-name-field-call-to-action').hide();
    } else {
      $('.field-name-field-campana-popup .field-name-field-call-to-action').show();
    }
  };

  var setearValoresDefecto = function(nombre_evento) {
    var valores_defecto = Drupal.settings.bbva_campanias.valores_defecto;
    var es_con_cupon = $(':input[name="field_campana_con_cupon[und]"]').is(":checked");
    $(".js-markup-defecto").remove();
    $(".js-val-defecto").val("").removeClass("js-val-defecto");
    if(nombre_evento == 'recordatorio') {
      $(".field-name-field-campana-barra-texto input").val(valores_defecto[nombre_evento].barra.texto).addClass("js-val-defecto");
    }
    if(nombre_evento == 'bienvenida') {
      var markup = $('<div class="messages warning form-item"><label>El título será: <b>' + valores_defecto[nombre_evento].popup.titulo + '</b></label></div>');
      markup.addClass("js-markup-defecto");
      $(".field-name-field-campana-popup-texto").prepend(markup);
      var markup = $('<div class="messages warning"></div>');
      markup.addClass("js-markup-defecto");
      markup.text("El link se genera automáticamente hacia el cupón (si corresponde).");
      $(".field-name-field-campana-popup").append(markup);
      $(".field-name-field-campana-notificacion-local input").val(valores_defecto[nombre_evento].notificacion_local).addClass("js-val-defecto");
      $(':input[name*="field_campana_popup_texto"]').val(valores_defecto[nombre_evento].popup.texto[es_con_cupon]).addClass("js-val-defecto");
      if($('.field-name-field-campana-popup-imagen input[name*="[field_campana_popup_imagen][und][0][fid]"]').val() < 1) {
        var markup = $('<div><img src="' + valores_defecto[nombre_evento].popup.imagen + '" /></div>');
        markup.addClass("js-markup-defecto");
        $('.field-name-field-campana-popup-imagen label').append(markup);
      }
      var markup = $('<div class="messages warning"></div>');
      markup.addClass("js-markup-defecto");
      markup.text("Si tiene cupón la campaña debe comenzar como mínimo dentro de dos días.");
      $(".start-date-wrapper .description").append(markup);
    }
    if(nombre_evento == 'cumpleanos') {
      var markup = $('<div class="messages warning form-item"><label>El título será: <b>' + valores_defecto[nombre_evento].popup.titulo + '</b></label></div>');
      markup.addClass("js-markup-defecto");
      $(".field-name-field-campana-popup-texto").prepend(markup);
      var markup = $('<div class="messages warning"></div>');
      markup.addClass("js-markup-defecto");
      markup.text("El link se genera automáticamente hacia el cupón (si corresponde).");
      $(".field-name-field-campana-popup").append(markup);
      $(".field-name-field-campana-notificacion-local input").val(valores_defecto[nombre_evento].notificacion_local).addClass("js-val-defecto");
      $(':input[name*="field_campana_popup_texto"]').val(valores_defecto[nombre_evento].popup.texto[es_con_cupon]).addClass("js-val-defecto");
      if($('.field-name-field-campana-popup-imagen input[name*="[field_campana_popup_imagen][und][0][fid]"]').val() < 1) {
        var markup = $('<div><img src="' + valores_defecto[nombre_evento].popup.imagen + '" /></div>');
        markup.addClass("js-markup-defecto");
        $('.field-name-field-campana-popup-imagen label').append(markup);
      }
      var markup = $('<div class="messages warning"></div>');
      markup.addClass("js-markup-defecto");
      markup.text("Si tiene cupón la campaña debe comenzar como mínimo dentro de dos días.");
      $(".start-date-wrapper .description").append(markup);
    }
    if(nombre_evento == 'listado') {
      var markup = $('<div class="messages warning form-item"><label>El título será: <b>' + valores_defecto[nombre_evento].popup.titulo + '</b></label></div>');
      markup.addClass("js-markup-defecto");
      $(".field-name-field-campana-popup-texto").prepend(markup);
      $(".field-name-field-campana-notificacion-local input").val(valores_defecto[nombre_evento].notificacion_local).addClass("js-val-defecto");
      $(':input[name*="field_campana_popup_texto"]').val(valores_defecto[nombre_evento].popup.texto).addClass("js-val-defecto");
      if($('.field-name-field-campana-popup-imagen input[name*="[field_campana_popup_imagen][und][0][fid]"]').val() < 1) {
        var markup = $('<div><img src="' + valores_defecto[nombre_evento].popup.imagen + '" /></div>');
        markup.addClass("js-markup-defecto");
        $('.field-name-field-campana-popup-imagen label').append(markup);
      }
    }
    if(nombre_evento == 'anonimos' || nombre_evento == 'todos') {
      $('.field-name-field-campana-barra-inferior select').val("_none").trigger("change");
    }
    mostrarAvisoCambioTexto();
  };

  var avisoCambioTextos  = function() {
    var markup = $('<div class="cambio-textos" title="Modificación de textos"><p>Al modificar el tipo de evento o la opción de cupones se modifican los textos cargados reemplazandose por los textos por defecto.</p></div>');
    $('#eck-entity-form-edit-campana-campana, #eck-entity-form-add-campana-campana').append(markup);
    $(".cambio-textos").dialog({
      width: 350,
      autoOpen: false,
    });
  };

  var mostrarAvisoCambioTexto = function() {
    var last_value = $(':input[name="field_campana_tipo_de_evento[und]"]').attr("last-value");
    if(last_value != "_none") {
      $(".cambio-textos").dialog( "open" );
    }
  };

  var ini_events = function() {
    $(':input[name="field_campana_tipo_de_evento[und]"]').live("change", function(e) {
      checkEventChange($(this).val());
      $(this).attr("last-value", $(this).val());
    }).trigger('change');

    $('#eck-entity-form-edit-campana-campana, #eck-entity-form-add-campana-campana').live("submit", function(){
      var is_valid = checkComunicacion();
      return is_valid;
    });

    $('#eck-entity-form-add-campana-campana :input[name="field_campana_con_cupon[und]"]').live("change", function(e){
      var event_name = $(':input[name="field_campana_tipo_de_evento[und]"]').val();
      setearValoresDefecto(event_name);
    });

    $('#eck-entity-form-add-campana-campana, #eck-entity-form-edit-campana-campana').ajaxComplete(function(event, xhr, settings) {
      var nombre_evento = $(':input[name="field_campana_tipo_de_evento[und]"]').val();
      /*if(nombre_evento == 'recordatorio') {
        console.log("si");
        var contexto = $('.field-name-field-campana-barra-inferior');
        $('.field-name-field-call-to-action', contexto).hide();
      } else {
        console.log("no");
        var contexto = $('.field-name-field-campana-barra-inferior');
        $('.field-name-field-call-to-action', contexto).show();
      }*/
      setearEsquemaDefecto(nombre_evento);
    });
  };
  $(document).ready(function() {
    ini_events();
    customizarLabels();
    avisoCambioTextos();
  });

})(jQuery);