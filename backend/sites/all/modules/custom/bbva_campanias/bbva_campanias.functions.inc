<?php

function get_user_campanas_messages() {

  $campanas_messages = array();

  if(variable_get('bbva_campanas_campanas_activas', 0) == 0) {
    return $campanas_messages;
  }

  global $user;

  $roles = bbva_campanias_get_roles_id($user);

  if(!isset($user->name)) {
    $user->name = NULL;
  }
  $campanas = bbva_campanias_db_user_campanas($user->name, $roles);

  // Si no hay campañas para el usuario/rol devuelvo un array vacio
  if(count($campanas) < 1) {
    return $campanas_messages;
  }

  $cache_campanas_messages = cache_get('campanas_messages','cache_api');
  if($cache_campanas_messages) {
    $cache_campanas_messages = $cache_campanas_messages->data;
  }
  // Si no tengo mensajes en cache devuelvo un array vacio
  if(count($cache_campanas_messages) < 1) {
    return $campanas_messages;
  }

  $campanas_sent = array(); // Lista de alertas que voy a marcar enviadas
  $campanas_filter = array(); // Filtro para no enviar dos campañas del mismo tipo
  foreach ($campanas as $campana) {
    $message_campana = array();
    // Se ignora la campaña de bienvenida y cumpleaños general
    if(($campana->tipo == 'bienvenida' || $campana->tipo == 'cumpleanos') && $campana->altamira_id == NULL) {
      continue;
    }
    // Si ya tengo una campaña de este tipo la ignoro
    if(in_array($campana->tipo, $campanas_filter)) {
      continue;
    }

    if($cache_campanas_messages[$campana->cid]) {
      $message_campana = $cache_campanas_messages[$campana->cid];
    } else {
      continue;
    }

    $campanas_filter[] = $campana->tipo;

    $message_campana['uid'] = "anonimo";
    if($user->uid > 0) {
      $message_campana['uid'] = (string) $user->name;
    }

    // Si tiene voucher le agrego el link al popup
    if($campana->voucher_id > 0) {
      if(isset($message_campana['popup'])) {
        $message_campana['popup']['link'] = BBVA_CAMPANIAS_CTA_TO_VOUCHER . $campana->voucher_id;
      }
    }
    // Titulo del popup
    if($campana->tipo == 'bienvenida' || $campana->tipo == 'cumpleanos' || $campana->tipo == 'listado') {

      $u_data = user_load($user->uid);

      $texto_bienvenido = isset($u_data->field_profile_sex['und']) && $u_data->field_profile_sex['und'][0]['value'] == "F" ? "Bienvenida" : "Bienvenido";
 
      if(isset($message_campana['popup'])) {

        $profileNombre = isset($u_data->field_profile_nombre['und']) ? $u_data->field_profile_nombre['und'][0]['safe_value'] : null;

        $obtenerTitulo = bbva_campanias_obtener_titulo_popup($campana->tipo, $profileNombre);
        $message_campana['popup']['titulo'] = str_replace("%bienvenido", $texto_bienvenido, $obtenerTitulo);
      } elseif($campana->tipo == 'bienvenida') { // Es la bienvenida por default
        // Barra inferior
        $message_campana['cid'] = $user->login;
        foreach ($message_campana['barra_inferior'] as $bi_key => $bi_value) {

          $profileNombre = isset($u_data->field_profile_nombre['und']) ? $u_data->field_profile_nombre['und'][0]['safe_value'] : null;

          $message_campana['barra_inferior'][$bi_key]['texto'] = str_replace("%name", $profileNombre, $bi_value['texto']);

          $message_campana['barra_inferior'][$bi_key]['texto2'] = str_replace("%bienvenido", $texto_bienvenido, $bi_value['texto2']);
        }
      }
    }


    $campanas_messages[] = $message_campana;
  }
  return $campanas_messages;
}

function bbva_campanias_clean_campanas() {
  // Actualizo el estado de los mensajes que expiraron
  bbva_campanias_db_clean_eventos_campanas();

  // Cambio el estado de las entidades que expiraron
  $hoy = new DateTime('today');

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'campana')
    ->entityCondition('bundle', 'campana')
    ->fieldCondition('field_campana_estado', 'value', BBVA_CAMPANIAS_CAMPANA_STATUS_ACTIVE, '=')
    ->fieldCondition('field_campana_vigencia', 'value2', $hoy->getTimestamp(), '<');

  $result = $query->execute();

  if(isset($result['campana'])) {
    $entity_ids = array_keys($result['campana']);
    foreach ($entity_ids as $campana_id) {
      $entity = entity_load_single('campana', $campana_id);
      $entity->field_campana_estado['und'][0]['value'] = BBVA_CAMPANIAS_CAMPANA_STATUS_EXPIRED;
      $entity->save();
    }
  }

  bbva_campanias_set_cache_campanas_messages();
}

function bbva_campanias_set_cache_campanas_messages() {
  $campanas = bbva_campanias_db_active_campanas();
  $messages = bbva_campanias_get_messages($campanas);
  $bienvenida_default = bbva_campanias_bienvenida_default();
  $messages[$bienvenida_default['cid']] = $bienvenida_default;

  cache_set('campanas_messages', $messages, 'cache_api', CACHE_PERMANENT);

  return "Caché de mensajes de camañanas regenerado";
}

function bbva_campanias_validar_campania(&$form, &$form_state) {
  $evento = $form_state['values']['field_campana_tipo_de_evento']['und'][0]['value'];
  $vigencia = $form_state['values']['field_campana_vigencia']['und'][0];

  $validaciones = array();
  if($evento == 'anonimos') {
    $validaciones[] = 'vigencia';
    $validaciones[] = 'frecuencia';
    $validaciones[] = 'vistas';
    $validaciones[] = 'barra_inferior';
    $validaciones[] = 'unico_activo';
  }
  if($evento == 'todos') {
    $validaciones[] = 'vigencia';
    $validaciones[] = 'frecuencia';
    $validaciones[] = 'barra_inferior';
    $validaciones[] = 'unico_activo';
  }
  if($evento == 'recordatorio') {
    $validaciones[] = 'vigencia';
    $validaciones[] = 'frecuencia';
    $validaciones[] = 'barra_inferior';
    $validaciones[] = 'unico_activo';
  }
  if($evento == 'listado') {
    $validaciones[] = 'vigencia';
    $validaciones[] = 'archivo';
    $validaciones[] = 'frecuencia';
    $validaciones[] = 'vistas';
    $validaciones[] = 'notificacion_local';
    $validaciones[] = 'popup';
    $validaciones[] = 'vouchers';
  }
  if($evento == 'bienvenida') {
    $validaciones[] = 'vigencia';
    $validaciones[] = 'cupon';
    $validaciones[] = 'target';
    $validaciones[] = 'notificacion_local';
    $validaciones[] = 'popup';
  }
  if($evento == 'cumpleanos') {
    $validaciones[] = 'vigencia';
    $validaciones[] = 'cupon';
    $validaciones[] = 'target';
    $validaciones[] = 'notificacion_local';
    $validaciones[] = 'popup';
  }
  if($evento == 'aviso_tc') {
    $validaciones[] = 'notificacion_local';
    $validaciones[] = 'popup';
  }

  if(in_array('vigencia', $validaciones)) {
    if($vigencia['value'] == NULL || $vigencia['value2'] <= $vigencia['value']) {
      form_set_error('field_campana_vigencia', 'Por favor revisar las fechas de vigencia.');
    }
    // Si tiene cupón valido la fecha de inicio
    if($form_state['values']['field_campana_con_cupon']['und'][0]['value'] === 1) {
      $date = DateTime::createFromFormat('Y-m-d H:i:s', $vigencia['value'] . " 00:00:00");
      if(!$date) {
        $date = new DateTime();
        $date->setTimestamp($vigencia['value']);
      }
      $fecha_vigencia['value'] = $date->getTimestamp();

      $min_day = new DateTime('tomorrow');
      if($fecha_vigencia['value'] <= $min_day->getTimestamp()) {
        global $user;
        // Solo a un administrador le dejo crear una campaña con cupones sin anticipación
        if(!in_array('administrator', $user->roles)) {
          form_set_error('field_campana_vigencia', 'Al ser con cupón la fecha de inicio debe ser dentro de dos días.');
        }
      }

      $date = DateTime::createFromFormat('Y-m-d H:i:s', $vigencia['value2'] . " 00:00:00");
      if(!$date) {
        $date = new DateTime();
        $date->setTimestamp($vigencia['value2']);
      }
      $fecha_vigencia['value2'] = $date->getTimestamp();
    }
  }
  if(in_array('target', $validaciones)) {
    if($form_state['values']['field_campana_target']['und'][0]['value'] == '0') {
      $validaciones[] = 'archivo';
    }
  }
  if(in_array('archivo', $validaciones)) {
    try {
      $file_value = $form_state['values']['field_campana_archivo']['und'][0];
      $file = file_load($file_value['fid']);
      $uri = $file->uri;
    } catch (Exception $e) {}
    if(!file_exists(drupal_realpath($uri))) {
      form_set_error('field_campana_archivo', 'Por favor revisa el archivo de destinatarios.');
    }
  }
  if(in_array('frecuencia', $validaciones)) {
    $frecuencia = $form_state['values']['field_campana_frecuencia']['und'][0]['value'];
    if(!is_numeric($frecuencia) || $frecuencia < 1) {
      form_set_error('field_campana_frecuencia', 'Por favor revisar la frecuencia.');
    }
  }
  if(in_array('vistas', $validaciones)) {
    $vistas = $form_state['values']['field_campana_vistas']['und'][0]['value'];
    if(!is_numeric($vistas) || $vistas < 1) {
      form_set_error('field_campana_vistas', 'Por favor revisa las vistas.');
    }
  }
  if(in_array('notificacion_local', $validaciones)) {
    $notificacion_local = $form_state['values']['field_campana_notificacion_local']['und'][0]['value'];
    if(empty($notificacion_local)) {
      form_set_error('field_campana_notificacion_local', 'Por favor revisa la notificación local.');
    }
  }
  if(in_array('barra_inferior', $validaciones)) {
    $barra_inferior = $form_state['values']['field_campana_barra_inferior']['und'];
    foreach ($barra_inferior as $bi_key => $bi) {
      if(!is_array($bi)) {
        break;
      }
      if(empty($bi['field_campana_barra_texto']['und'][0]['value'])) {
        form_set_error('', 'Por favor revisa el texto de la barra inferior.');
      }
      if($bi['field_seccion_app']['und'][0]['tid'] == NULL) {
        form_set_error('', 'Por favor revisa la sección de la barra inferior.');
      }
      if($evento != 'recordatorio') {
        if(!bbva_campanias_validate_CTA($bi['field_call_to_action']['und'][0])) {
          form_set_error('', 'Por favor revisa el link de la barra inferior.');
        }
      }
    }
  }
  if(in_array('unico_activo', $validaciones)) {
    $hoy = new DateTime('today');

    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'campana')
      ->entityCondition('bundle', 'campana')
      ->fieldCondition('field_campana_estado', 'value', BBVA_CAMPANIAS_CAMPANA_STATUS_ACTIVE, '=')
      ->fieldCondition('field_campana_tipo_de_evento', 'value', $evento, '=')
      ->fieldCondition('field_campana_vigencia', 'value', $hoy->getTimestamp(), '<=')
      ->fieldCondition('field_campana_vigencia', 'value2', $hoy->getTimestamp(), '>=');

    // Si estoy editando una campaña no quiero incluirla en la query
    if(isset($form_state['values']['entity']->id) && $form_state['values']['entity']->id != NULL) {
      $query->propertyCondition('id', $form_state['values']['entity']->id, '!=');
    }

    $result = $query->execute();

    if(isset($result['campana'])) {
      form_set_error('field_campana_tipo_de_evento', 'Ya hay un evento de este tipo activo.');
    }
  }
  if(in_array('popup', $validaciones)) {
    $popup = $form_state['values']['field_campana_popup']['und'][0];
    if(empty($popup['field_campana_popup_texto']['und'][0]['value'])) {
      form_set_error('', 'Por favor revisa el texto del popup.');
    }
    // Si el evento no tiene imagen por defecto chequeo que haya una cargada
    $file_value = $popup['field_campana_popup_imagen']['und'][0];
    if($file_value['fid'] > 0) {
      try {
        $file = file_load($file_value['fid']);
        $uri = $file->uri;
        $path = drupal_realpath($uri);
        // Chequeo el ratio de la imagen
        $img_data = image_get_info($uri);
        if(!empty($img_data)) {
          if($img_data['width'] / $img_data['height'] != 3 / 2) {
            form_set_error('', 'La imagen del popup no tiene la relación de aspecto correcta (3:2).');
          }
        }
      } catch (Exception $e) { }
      if(!file_exists($path)) {
        form_set_error('', 'Por favor revisa la imagen del popup.');
      }
    }
    // Si no va a un cupon valido el link
    if($evento != 'bienvenida' && $evento != 'cumpleanos' && $evento != 'aviso_tc') {
      if(!bbva_campanias_validate_CTA($popup['field_call_to_action']['und'][0])) {
        form_set_error('', 'Por favor revisa el link del popup.');
      }
    }
  }
  if(in_array('cupon', $validaciones)) {
    if($form_state['values']['field_campana_con_cupon']['und'][0]['value'] === 1) {
      if($form_state['values']['field_coupon_promo_numero']['und'][0]['tid'] == NULL) {
        form_set_error('field_coupon_promo_numero', 'Por favor revisa el Promo numero.');
      }
      if($form_state['values']['field_coupon_promo_shop']['und'][0]['target_id'] == NULL) {
        form_set_error('field_coupon_promo_shop', 'Por favor revisa la marca.');
      }
      if(!is_numeric($form_state['values']['field_coupon_promo_discount']['und'][0]['value'])) {
        form_set_error('field_coupon_promo_discount', 'Por favor revisa el descuento del comercio.');
      }
      if(!is_numeric($form_state['values']['field_coupon_promo_top']['und'][0]['value'])) {
        form_set_error('field_coupon_promo_top', 'Por favor revisa el tope en $ del cupon.');
      }
      if(empty($form_state['values']['field_coupon_promo_legal']['und'][0]['value'])) {
        form_set_error('field_coupon_promo_legal', 'Por favor revisa el texto legal del cupon.');
      }
      // Con archivo de vouchers
      if($form_state['values']['field_coupon_type']['und'][0]['value'] == 2) {
      try {
        $file_value = $form_state['values']['field_coupon_vochers_file']['und'][0];
        $file = file_load($file_value['fid']);
        $uri = $file->uri;
      } catch (Exception $e) {}
      if(!file_exists(drupal_realpath($uri))) {
        form_set_error('field_coupon_vochers_file', 'Por favor revisa el archivo de vouchers.');
      }
      } else { // Sin archivo de vouchers
        if(empty($form_state['values']['field_coupon_promo_lquantity']['und'][0]['value']) || !is_numeric($form_state['values']['field_coupon_promo_lquantity']['und'][0]['value'])) {
          form_set_error('field_coupon_promo_lquantity', 'Por favor revisa la cantidad de cupones.');
        }
      }
      $promo_numero_tid = $form_state['values']['field_coupon_promo_numero']['und'][0]['tid'];
      $cupones_en_uso_con_ese_promo_numero = bbva_cuopon_promo_numero_en_uso($promo_numero_tid, $fecha_vigencia);
      if(count($cupones_en_uso_con_ese_promo_numero) > 0) {
        form_set_error('field_coupon_promo_numero', 'El promo numero ya se encuentra reservado en la fecha de vigencia.');
      }
    }
  }
}

function bbva_campanias_get_messages($campanas) {
  $campanas_messages = array();

  foreach ($campanas as $key => $campana) {
    $entity = entity_load_single('campana', $campana->entity_id);
    $ewrapper = entity_metadata_wrapper('campana', $entity);

    $vigencia = $ewrapper->field_campana_vigencia->raw();
    $con_cupon = $ewrapper->field_campana_con_cupon->raw();

    $messages = array();
    $messages['cid'] = $campana->entity_id;
    $messages['tipo_evento'] = $ewrapper->field_campana_tipo_de_evento->raw();
    $messages['por_usuario'] = FALSE;
    $messages['para_todos'] = FALSE;
    $messages['actualizado'] = $entity->changed;
    $messages['expira'] = $vigencia['value2'];
    $messages['prioridad'] = $ewrapper->field_campana_orden->raw();
    $messages['vistas'] = $ewrapper->field_campana_vistas->raw();
    $messages['frecuencia'] = $ewrapper->field_campana_frecuencia->raw();
    $messages['notificacion_local'] = NULL;
    $messages['popup'] = NULL;
    $messages['barra_inferior'] = NULL;

    if($messages['tipo_evento'] == 'todos') {
      $messages['para_todos'] = TRUE;
    }

    if($messages['tipo_evento'] == 'cumpleanos' || $messages['tipo_evento'] == 'bienvenida' || $messages['tipo_evento'] == 'listado' || $messages['tipo_evento'] == 'aviso_tc') {
      $messages['por_usuario'] = TRUE;
    }

    if($messages['tipo_evento'] == 'anonimos' || $messages['tipo_evento'] == 'todos' || $messages['tipo_evento'] == 'recordatorio') {
      $barra_inferior = array();
      $fc_barra_inferior = $ewrapper->field_campana_barra_inferior->value();
      foreach ($fc_barra_inferior as $bi) {
        $array = array();
        $array['texto'] = "<b>" . $bi->field_campana_barra_texto['und'][0]['value'] . "</b>";
        $array['texto2'] = $bi->field_campana_barra_texto2['und'][0]['value'];
        $array['link'] = _get_call_to_acction_api($bi->field_call_to_action['und'][0]['value']);
        $array['seccion'] = bbva_campanias_obtener_seccion($bi->field_seccion_app['und'][0]['tid']);
        $barra_inferior[] = $array;
      }
      $messages['barra_inferior'] = $barra_inferior;
    }

    if($con_cupon == 1 || $messages['tipo_evento'] == 'listado' || $messages['tipo_evento'] == 'aviso_tc') {
      $messages['notificacion_local'] = array(
        'texto' => $ewrapper->field_campana_notificacion_local->raw(),
        'link' => NULL,
      );
    }

    // Entregamos el cupon al loguearse, no necesita notificacion local
    if($messages['tipo_evento'] == 'bienvenida') {
      $messages['notificacion_local'] = NULL;
    }

    if($messages['tipo_evento'] == 'aviso_tc') {
      $messages['expira'] = strtotime("+" . BBVA_CAMPANIAS_AVISO_TC_DIAS_VIGENCIA . " days");
    }

    if($messages['tipo_evento'] == 'listado' || $messages['tipo_evento'] == 'bienvenida' || $messages['tipo_evento'] == 'cumpleanos' || $messages['tipo_evento'] == 'aviso_tc') {

      $messages['popup'] = array(
        'titulo' => NULL,
        'texto' => $ewrapper->field_campana_popup->field_campana_popup_texto->raw(),
        'imagen' => NULL,
      );

      $img = $ewrapper->field_campana_popup->field_campana_popup_imagen->raw();
      if($img) {
        $messages['popup']['imagen'] = file_create_url($img['uri']);
      }

      // Si no hay imagen seteo la imagen por defecto
      if($messages['popup']['imagen'] == NULL) {
        $messages['popup']['imagen'] = bbva_campanias_imagen_defecto_segun_tipo_evento($messages['tipo_evento']);
      }
    }

    if($messages['tipo_evento'] == 'listado') {
      $messages['popup']['link'] = _get_call_to_acction_api($ewrapper->field_campana_popup->field_call_to_action->raw()->item_id);
    } elseif($messages['tipo_evento'] == 'bienvenida' || $messages['tipo_evento'] == 'cumpleanos') {
      $messages['popup']['link'] = NULL;
    } elseif($messages['tipo_evento'] == 'aviso_tc') {
      $messages['popup']['link'] = BBVA_CAMPANIAS_CTA_TO_OFERTAPREAPROBADA;
    }

    $campanas_messages[$campana->entity_id] = $messages;
  }

  return $campanas_messages;
}

function bbva_campanias_validate_CTA($cta) {
  $ctaMappingValues = array(
    '0' => 'field_call_to_acction_nav_app',
    '1' => 'field_call_to_action_exp_1',
    '2' => 'field_call_to_action_exp_2',
    '3' => 'field_call_to_action_sorteos',
    '4' => 'field_call_to_action_benefit',
    '5' => 'field_call_to_action_rubro',
    '6' => 'field_field_call_to_action_camp',
    '7' => 'field_call_to_acction_link',
  );

  $type = $cta['field_tipo']['und'][0]['value'];

  if(!isset($ctaMappingValues[$type])) {
    return FALSE;
  }

  $value = $cta[$ctaMappingValues[$type]]['und'][0];
  if(isset($value['value']) && empty($value['value']) ) {
    return FALSE;
  }
  if(isset($value['target_id']) && !is_numeric($value['target_id']) ) {
    return FALSE;
  }

  return TRUE;
}

function bbva_campanias_obtener_seccion($value){
  $prefijo = "seccion://";
  $return = "";

  $term = taxonomy_term_load($value);
  if($term) {
    $return = $term->field_seccion_app_valor['und'][0]['safe_value'];
  }

  return $prefijo . $return;
};

function bbva_campanias_cumpleanos() {

  $params = drupal_get_query_parameters();
  $marcahoy = date("Ymd");
  $ultima_marca = variable_get("bbva_campanias_cumpleanos_marca", 0);

  if($marcahoy == $ultima_marca && !isset($params["reproceso"])) {
    return true;
  }

  // Busco si hay campañas del tipo cumpleaños
  $campanias_cumpleanos = bbva_campanias_db_active_campanas('cumpleanos');
  // Escapo si no hay campañas
  if(count($campanias_cumpleanos) < 1) {
    return;
  }

  // Busco los usuarios que cumplen años
  $hoy = new DateTime('today');
  $fin = new Datetime('tomorrow');
  $d = $hoy->format('j');
  $m = $hoy->format('n');
  $usuarios_cumpleanos = bbva_campanias_usuarios_cumpleanos($d, $m);
  // Escapo si no hay cumpleañeros
  if(count($usuarios_cumpleanos) < 1) {
    return;
  }

  // Creo las alertas
  $values = array();
  foreach ($campanias_cumpleanos as $campana) {
    $con_cupon = $campana->field_campana_con_cupon_value;
    // Obtengo el ID del cupon
    if($con_cupon == 1) {
      $id_cupon = _get_cupon_whit_campania($campana->entity_id);
    } else {
      $voucher = new stdClass();
      $voucher->id = NULL;
    }
    reset($usuarios_cumpleanos);
    foreach ($usuarios_cumpleanos as $k => $usuario) {
      // Si es por listado busco si le corresponde al usuario
      if($campana->field_campana_target_value == "0") {
        $usuario_en_campana = bbva_campanias_db_user_campanas($usuario->name, array(), "cumpleanos", TRUE, $campana->entity_id);
        if(count($usuario_en_campana) < 1) {
          continue;
        }
      }
      // Asigno voucher de cumpleaños al usuario
      if($con_cupon == 1) {
        $voucher = asignar_voucher($id_cupon->entity_id, $usuario->name);
        if($voucher == NULL) {
          continue;
        }
      }
      $values = array(
        'cid' => $campana->entity_id,
        'tipo' => 'cumpleanos',
        'altamira_id' => $usuario->name,
        'rid' => NULL,
        'orden' => $campana->field_campana_orden_value,
        'start' => $hoy->getTimestamp(),
        'expire' => $fin->getTimestamp(),
        'voucher_id' => $voucher->id,
        'state' => BBVA_CAMPANIAS_MSG_STATUS_READY
      );
      unset($usuarios_cumpleanos[$k]);
      bbva_campanias_db_merge_msg_campania($values);
    }
  }

  variable_set("bbva_campanias_cumpleanos_marca", $marcahoy);

  return TRUE;
}

function bbva_campanias_aviso_tc() {
  $campanas =  bbva_campanias_db_active_campanas("aviso_tc");
  $campania_usuarios = array();
  $usuarios_agregados = array();
  if(!empty($campanas)) {
    foreach ($campanas as $campania) {
      $fecha_origen_oferta = null;
      $fecha_vencimiento_oferta = null;
      if($campania->field_campana_inicio_de_oferta_value > 0) {
        $fecha_origen_oferta = new DateTime('today');
        $fecha_origen_oferta->sub(new DateInterval('P'.$campania->field_campana_inicio_de_oferta_value.'D'));
        $fecha_origen_oferta = $fecha_origen_oferta->getTimestamp();
      } elseif($campania->field_campana_inicio_de_oferta_value < 0) {
        $fecha_vencimiento_oferta = new DateTime('today');
        $fecha_vencimiento_oferta->add(new DateInterval('P'.($campania->field_campana_inicio_de_oferta_value * -1).'D'));
        $fecha_vencimiento_oferta = $fecha_vencimiento_oferta->getTimestamp();
      }
      // Lista de usuarios
      $usuarios = bbva_campanias_db_buscar_ofertastc($fecha_origen_oferta, $fecha_vencimiento_oferta);
      foreach ($usuarios as $usuario) {
        if(!in_array($usuario, $usuarios_agregados)) {
          // Agrego al usuario
          $campania_usuarios[$campania->entity_id][] = $usuario;
          // Lo agrego a la lista de usuarios agregados para no incluirlo en otra campaña
          $usuarios_agregados[] = $usuario;
        }
      }
    }
  }
  // Consulto al motor de nuevo y creo el evento-campaña para el usuario
  if(!empty($campania_usuarios)) {
    $values = array();
    $usuarios_para_borrar = array();
    // Obtengo el orden
    $orden = bbva_campanias_orden_segun_tipo_evento("aviso_tc");
    // Campañas
    foreach ($campania_usuarios as $kcampania => $campania_usuarios) {
      if(!empty($campania_usuarios)) {
        // Usuarios de cada campaña
        foreach ($campania_usuarios as $usuario_campania) {
          try {
            $usuario = new \BBVA\Usuario($usuario_campania, TRUE);
            $respuesta = $usuario->evaluarOfertaMotorTarjeta(FALSE);
            if(!is_numeric($usuario->data->name)) {
              continue;
            }
            // Dato para borrar las campañas de este tipo
            $usuarios_para_borrar[] = $usuario->data->name;
            if(isset($respuesta['data']) && isset($respuesta['data']['data']) && isset($respuesta['data']['data']['respuesta']) && $respuesta['data']['data']['respuesta']->dictamen == "APROBADO" && $respuesta['data']['data']['respuesta']->mensajeFeliz != true) {
              // Inserto campaña
              $values[] = array(
                'cid' => $kcampania,
                'tipo' => "aviso_tc",
                'altamira_id' => $usuario->data->name,
                'orden' => $orden,
                'start' => strtotime("today"),
                'expire' => strtotime("+" . BBVA_CAMPANIAS_AVISO_TC_DIAS_VIGENCIA . " days"),
                'voucher_id' => null,
                'state' => BBVA_CAMPANIAS_MSG_STATUS_READY
              );
            } elseif(isset($respuesta['data']['data']['respuesta']->tipoMje)) {
              // Consulto datos en bbva_campanias
              $dato_campania = bbva_campanias_db_consultar_ofertatc_x_usuario($usuario_campania);
              // Muevo a historico y Borro registro en bbva_campanias
              bbva_campanias_db_mover_campana_historico($dato_campania[0], substr($respuesta['data']['data']['respuesta']->tipoMje, strpos($respuesta['data']['data']['respuesta']->tipoMje, "-") + 1));
            }
          } catch (\Exception $e) {
            continue;
          }
        }
      }
    }
    // Borro cualquier campania de este tipo que tengan los usuarios
    if(!empty($usuarios_para_borrar)) {
      bbva_campanias_db_borrar_campanias_avisotc($usuarios_para_borrar);
    }
    // Inserto las nuevas campañas (eventos)
    if(!empty($values)) {
      bbva_campanias_db_insert_alerts($values);
    }
  }
}

function bbva_campanias_orden_segun_tipo_evento($tipo_evento, $con_cupon = FALSE, $por_listado = FALSE, $inicio_oferta = 0) {
  $orden_eventos = array(
    'aviso_tc' => 9,
    'recordatorio' => 19,
    'anonimos' => 29,
    'bienvenida' => 39,
    'cumpleanos' => 49,
    'listado' => 59,
    'todos' => 69,
  );
  $orden = $orden_eventos[$tipo_evento];

  if($con_cupon) {
    $orden = $orden - 2;
  }
  if($por_listado) {
    $orden = $orden - 1;
  }
  if($inicio_oferta < 0) {
    $orden = $orden - 1;
  }

  return $orden;
}

function bbva_campanias_imagen_defecto_segun_tipo_evento($tipo_evento) {
  $imagen = url(drupal_get_path('module', 'bbva_campanias') . '/img/' . $tipo_evento . '.jpg', array('absolute' => TRUE));

  return $imagen;
}

function bbva_campanias_tiene_bienvenida() {
  try {
    global $user;
    if(!isset($user->name)) {
      return FALSE;
    }
    
    /*// Chequeo si el usuario entra en el periodo de antiguedad
    if(bbva_campanias_db_nueva_subscripcion($user->name) < 1) {
      return FALSE;
    }*/

    // Chequeo si tiene campaña de bienvenida
    $roles = bbva_campanias_get_roles_id($user);
    $campanas_bienvenida = bbva_campanias_db_user_campanas($user->name, $roles, $tipo = "bienvenida", TRUE);
    if(count($campanas_bienvenida) < 1) {
      if(variable_get('bbva_campanas_bienvenida_default', FALSE)) {
        $campana = bbva_campanias_bienvenida_default();
        $values = array(
          'cid' => $campana['cid'],
          'tipo' => $campana['tipo_evento'],
          'altamira_id' => $user->name,
          'rid' => NULL,
          'orden' => $campana['prioridad'],
          'start' => $campana['actualizado'],
          'expire' => $campana['expira'],
          'state' => BBVA_CAMPANIAS_MSG_STATUS_READY
        );
        bbva_campanias_db_merge_msg_campania($values);

        return TRUE;
      }
      return FALSE;
    }

    // Chequeo si la campaña es con cupon
    foreach ($campanas_bienvenida as $campana_row) {
      $entity = entity_load_single('campana', $campana_row->cid);
      $ewrapper = entity_metadata_wrapper('campana', $entity);

      $vigencia = $ewrapper->field_campana_vigencia->raw();
      $con_cupon = $ewrapper->field_campana_con_cupon->raw();

      if($con_cupon) {
        // Si la campaña es con cupon es válida hasta X dias antes de expirar
        if(!bbva_campanias_tiempo_suficiente($vigencia['value2'])) {
          continue;
        }
        // Obtengo ID de cupon
        $id_cupon = _get_cupon_whit_campania($campana_row->cid);
        if(!$id_cupon) {
          continue;
        }
        // Asigno voucher
        $voucher = asignar_voucher($id_cupon->entity_id, $user->name);
        if($voucher == NULL) {
          continue;
        }
      } else {
        $voucher = new stdClass();
        $voucher->id = NULL;
      }

      if($campana_row->altamira_id == NULL || $campana_row->state == -1) {
        $expire = "P" . BBVA_CAMPANIAS_SUBSCRIPCION_WINDOW . "D";
        $date = new DateTime();
        $date->add(new DateInterval($expire));
        $timestamp = $date->getTimestamp();

        $values = array(
          'cid' => $campana_row->cid,
          'tipo' => 'bienvenida',
          'altamira_id' => $user->name,
          'rid' => NULL,
          'orden' => $campana_row->orden,
          'start' => $campana_row->start,
          'expire' => $timestamp,
          'voucher_id' => $voucher->id,
          'state' => BBVA_CAMPANIAS_MSG_STATUS_READY
        );
        bbva_campanias_db_merge_msg_campania($values);

        return TRUE;
      }
    }
  } catch (\Exception $e) {
    return FALSE;
  }
}

function bbva_campanias_get_roles_id($user) {
  $roles = array();
  $campanas_messages = array();

  foreach ($user->roles as $role_name) {
    $role = user_role_load_by_name($role_name);
    $roles[] = $role->rid;
  }

  return $roles;
}

function bbva_campanias_tiempo_suficiente($expire = 0) {
  $min_expire = new Datetime("today");
  $min_expire->add(new DateInterval("P" . BBVA_CAMPANIAS_MINIMO_DIAS_VENCIMIENTO . "D"));
  if($expire >= $min_expire->getTimestamp()) {
    return TRUE;
  }
  return FALSE;
}

function bbva_campanias_obtener_titulo_popup($tipo_evento, $nombre) {
  $titulos = array(
    'bienvenida' => BBVA_CAMPANIAS_MSG_BIENVENIDA_TITLE,
    'cumpleanos' => BBVA_CAMPANIAS_MSG_CUMPLE_TITLE,
    'listado' => BBVA_CAMPANIAS_MSG_LISTADO_TITLE
  );
  $str = str_replace("%name", trim($nombre), $titulos[$tipo_evento]);

  return $str;
}

function bbva_campanias_bienvenida_default() {
  $messages = array();
  $messages['cid'] = "999999999";
  $messages['tipo_evento'] = "bienvenida";
  $messages['por_usuario'] = TRUE;
  $messages['para_todos'] = FALSE;
  $messages['actualizado'] = (string) (time() - 60 * 60 * 24);
  $messages['expira'] = (string) (time() + 60 * 60 * 24 * 30);
  $messages['prioridad'] = "99";
  $messages['vistas'] = "1";
  $messages['frecuencia'] = "99999999";
  $messages['notificacion_local'] = NULL;
  $messages['popup'] = NULL;
  $messages['barra_inferior'] = array(
    array(
      "texto" => "<b>Hola %name!</b>",
      "texto2" => "%bienvenido a Go",
      "seccion" => "seccion://menu/home",
      "link" => NULL,
    )
  );

  return $messages;
}


function bbva_campanias_historico_aviso_tc() {
  $fecha_actual = strtotime(date("Y-m-d"));
  $ofertas_vencidas = bbva_campanias_db_consulta_campanias_vencidas($fecha_actual);
  foreach ( $ofertas_vencidas as $registro_campanias_tc) {
    bbva_campanias_db_mover_campana_historico($registro_campanias_tc);
  }
}