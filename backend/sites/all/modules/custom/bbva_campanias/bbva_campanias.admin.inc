<?php

/**
 * Admin settings page.
 */
function bbva_campanias_admin($form, $form_state) {

  $form['bbva_oferta_preaprobada_url'] = array(
  '#title' => t('URL Oferta Preaprobada'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_oferta_preaprobada_url'),
  );
  $form['bbva_oferta_preaprobada_tag'] = array(
  '#title' => t('Tag para omniture default'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_oferta_preaprobada_tag'),
  );
  $form['bbva_oferta_preaprobada_tag_aviso_tc'] = array(
    '#title' => t('Tag para omniture para aviso tc para usuario que vienen de la suscripcion'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_oferta_preaprobada_tag_aviso_tc'),
  );
  $form['bbva_campania_url_redirect'] = array(
    '#title' => t('Url del host que realiza el redirect'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_campania_url_redirect'),
  );
  $form['bbva_campanas_campanas_activas'] = array(
    '#title' => 'Campa�as activas',
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_campanas_campanas_activas', FALSE),
  );
  $form['bbva_campanas_bienvenida_default'] = array(
    '#title' => 'Enviar una campa�a de bienvenida por defecto si no hay ninguna cargada',
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_campanas_bienvenida_default', FALSE),
  );
  $form['bbva_campanas_saltar_paso_1'] = array(
    '#title' => t('Intentar saltar el paso 1 del motor'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_campanas_saltar_paso_1', FALSE),
  );
  $form['bbva_campanas_habilitar_motortc_suscriptos'] = array(
    '#title' => t('Habilitar evaluacion con motor TC en suscripcion'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_campanas_habilitar_motortc_suscriptos', FALSE),
  );

  return system_settings_form($form);
}

