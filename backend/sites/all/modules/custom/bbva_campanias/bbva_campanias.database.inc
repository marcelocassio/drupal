<?php

function bbva_campanias_db_user_campanas($altamira_id = NULL, $rid = array(), $tipo = FALSE, $pending = FALSE, $cid = FALSE) {
  $hoy = new DateTime('today');
  if(!is_array($rid)) {
    $rid = array($rid);
  }
  $query = db_select('bbva_campanias_eventos', 'ce');

  if(!empty($rid)) {
    $query = $query->condition(
      db_or()
        ->condition('ce.altamira_id', $altamira_id)
        ->condition('ce.rid', $rid, 'IN')
    );
  } else {
    $query = $query->condition('ce.altamira_id', $altamira_id);
  }
  $query = $query->condition(
    db_or()
    ->isNull('ce.start')
    ->condition('ce.start', $hoy->getTimestamp(), '<=')
  );
  $query = $query->condition(
    db_or()
    ->isNull('ce.expire')
    ->where('ce.start = ce.expire')
    ->condition('ce.expire', $hoy->getTimestamp(), '>=')
  );
  if($pending) {
    $query = $query->condition(
      db_or()
      ->condition('ce.state', BBVA_CAMPANIAS_MSG_STATUS_PENDING)
      ->condition('ce.state', BBVA_CAMPANIAS_MSG_STATUS_READY)
    );
  } else {
    $query = $query->condition('ce.state', BBVA_CAMPANIAS_MSG_STATUS_READY);
  }
  if($tipo) {
    $query = $query->condition('ce.tipo', $tipo);
  }
  if($cid) {
    $query = $query->condition('ce.cid', $cid);
  }

  $query = $query->fields('ce', array('id', 'altamira_id', 'tipo', 'rid', 'cid', 'start', 'expire', 'voucher_id', 'orden', 'state'));

  $query = $query->orderBy('ce.orden', 'ASC');

  $result = $query->execute()->fetchAll();

  return $result;
}

function bbva_campanias_db_clean_eventos_campanas() {
  $expire = new DateTime('now');

  $updated = db_update('bbva_campanias_eventos')
    ->fields(array(
      'state' => BBVA_CAMPANIAS_MSG_STATUS_EXPIRED
    ))
    ->condition('expire', $expire->getTimestamp(), '<')
    ->execute();
}

function bbva_campanias_db_clean_entidad_campanas() {
  $expire = new DateTime('now');

  $sql = "
    UPDATE field_data_field_campana_estado as ce
    LEFT JOIN field_data_field_campana_vigencia as cv on ce.entity_id = cv.entity_id
    SET ce.field_campana_estado_value = :estado_expirado
    WHERE ce.field_campana_estado_value = :estado_activo
    AND cv.field_campana_vigencia_value2 < :timestamp_actual
  ";

  $args = array(':estado_expirado' => BBVA_CAMPANIAS_CAMPANA_STATUS_EXPIRED, ':estado_activo' => BBVA_CAMPANIAS_CAMPANA_STATUS_ACTIVE, ':timestamp_actual' => $expire->getTimestamp());

  $result = db_query($sql, $args);

  return $result;
}

function bbva_campanias_db_regenerate_alert_message($cid, $rids = false, $order = 0, $start = NULL,$expire = NULL, $tipo_evento = "") {
  // Borro las alertas cargadas para el id de alerta que aún no hayan sido enviadas
  $deleted = db_delete('bbva_campanias_eventos')
    ->condition('cid', $cid)
    ->execute();
  // Si es por roles inserto los roles
  if($rids) {
    if(!is_array($rids)) { // Si es uno solo lo pongo en un array
      $rids = array($rids);
    }
    foreach ($rids as $rid) {
      $nid = db_insert('bbva_campanias_eventos')
        ->fields(array(
          'cid' => $cid,
          'tipo' => $tipo_evento,
          'rid' => $rid,
          'orden' => $order,
          'start' => $start,
          'expire' => $expire,
          'state' => BBVA_CAMPANIAS_MSG_STATUS_READY
        ))
        ->execute();
    }
  }
}

function bbva_campanias_db_active_campanas($evento = NULL) {
  $hoy = new DateTime('today');

  $min_expire = new Datetime("today");
  $min_expire->add(new DateInterval("P" . BBVA_CAMPANIAS_MINIMO_DIAS_VENCIMIENTO . "D"));

  $query = db_select('field_data_field_campana_estado', 'ce');
  $query->leftJoin('field_data_field_campana_vigencia','e_cv', 'e_cv.entity_id = ce.entity_id');
  $query->leftJoin('field_data_field_campana_tipo_de_evento','e_te', 'e_te.entity_id = ce.entity_id');
  $query->leftJoin('field_data_field_campana_orden','co', 'co.entity_id = ce.entity_id');
  $query->leftJoin('field_data_field_campana_con_cupon','ccc', 'ccc.entity_id = ce.entity_id');
  $query->leftJoin('field_data_field_campana_target','ct', 'ct.entity_id = ce.entity_id');
  $query->leftJoin('field_data_field_campana_inicio_de_oferta','io', 'io.entity_id = ce.entity_id');

  $query = $query->condition('ce.field_campana_estado_value', BBVA_CAMPANIAS_CAMPANA_STATUS_ACTIVE);
  $query = $query->condition(
    db_or()
      ->isNull('e_cv.field_campana_vigencia_value')
      ->condition('e_cv.field_campana_vigencia_value', $hoy->getTimestamp(), '<=')
  );
  $query = $query->condition(
    db_or()
      ->isNull('e_cv.field_campana_vigencia_value2')
      ->condition(
        db_and()
          ->condition('e_cv.field_campana_vigencia_value2', $hoy->getTimestamp(), '>=')
          ->condition('ccc.field_campana_con_cupon_value', 0)
      )
      ->condition(
        db_and()
          ->condition('e_cv.field_campana_vigencia_value2', $min_expire->getTimestamp(), '>=')
          ->condition('ccc.field_campana_con_cupon_value', 1)
      )
  );

  if($evento !== NULL) {
    $query = $query->condition('e_te.field_campana_tipo_de_evento_value', $evento);
  }

  $query = $query->fields('ce', array('entity_id'));
  $query = $query->fields('e_te', array('field_campana_tipo_de_evento_value'));
  $query = $query->fields('e_cv', array('field_campana_vigencia_value', 'field_campana_vigencia_value2'));
  $query = $query->fields('co', array('field_campana_orden_value'));
  $query = $query->fields('ccc', array('field_campana_con_cupon_value'));
  $query = $query->fields('ct', array('field_campana_target_value'));
  $query = $query->fields('io', array('field_campana_inicio_de_oferta_value'));

  $query = $query->orderBy('co.field_campana_orden_value', 'ASC');

  $result = $query->execute()->fetchAll();

  return $result;
}

function bbva_campanias_db_insert_alerts($values) {
  try {
    $query = db_insert('bbva_campanias_eventos');
    $query = $query->fields(array('cid', 'tipo', 'altamira_id', 'orden', 'start', 'expire', 'state', 'voucher_id'));
    foreach ($values as $record) {
      $query->values($record);
    }
    $result = $query->execute();

  } catch (Exception $e) {
    $result = FALSE;
  }

  return $result;
}

function bbva_campanias_db_merge_msg_campania($values) {
  $query = db_merge('bbva_campanias_eventos');
  $query = $query->key(
    array(
      'cid' => $values['cid'],
      'altamira_id' => $values['altamira_id'],
      'rid' => $values['rid']));
  $query = $query->fields($values);
  $result = $query->execute();

  return $result;
}

function bbva_campanias_buscar_usuarios_en_campania($altamira_ids, $tipo = FALSE) {
  $hoy = new DateTime('today');
  $query = db_select('bbva_campanias_eventos', 'ce');

  $query = $query->condition('ce.altamira_id', $altamira_ids, 'IN');
  $query = $query->condition(
    db_or()
    ->isNull('ce.start')
    ->condition('ce.start', $hoy->getTimestamp(), '<=')
  );
  $query = $query->condition(
    db_or()
    ->isNull('ce.expire')
    ->where('ce.start = ce.expire')
    ->condition('ce.expire', $hoy->getTimestamp(), '>=')
  );
  $query = $query->condition('ce.state', BBVA_CAMPANIAS_MSG_STATUS_READY);
  if($tipo) {
    $query = $query->condition('ce.tipo', $tipo);
  }

  $query = $query->fields('ce', array('altamira_id'));

  $result = $query->execute()->fetchAll();

  $return = array();
  if(!empty($result)) {
    $return = array();
    foreach ($result as $altamira_id_row) {
      $return[] = $altamira_id_row->altamira_id;
    }
  }

  return $return;
}

function bbva_campanias_usuarios_cumpleanos($d, $m, $suscripto = TRUE) {
  $query = db_select('field_data_field_profile_day', 'pd');
  $query->leftJoin('field_data_field_profile_month','pm', 'pd.entity_id = pm.entity_id');
  $query->leftJoin('users','u', 'pd.entity_id = u.uid');
  if($suscripto) {
    $query->leftJoin('users_roles','ur', 'pd.entity_id = ur.uid');
  }

  $query = $query->condition(
    db_or()
    ->condition('pd.field_profile_day_value', (int) $d)
    ->condition('pd.field_profile_day_value', sprintf("%02d", $d))
  );
  $query = $query->condition(
    db_or()
    ->condition('pm.field_profile_month_value', (int) $m)
    ->condition('pm.field_profile_month_value', sprintf("%02d", $m))
  );
  if($suscripto) {
    $query = $query->condition('ur.rid', 11);
  }

  $query = $query->fields('u', array('name'));

  $result = $query->execute()->fetchAll();

  return $result;
}

function bbva_campanias_db_nueva_subscripcion($name) {
  $expire = "P" . BBVA_CAMPANIAS_SUBSCRIPCION_WINDOW . "D";
  $date = new DateTime();
  $date->sub(new DateInterval($expire));
  $timestamp = $date->getTimestamp();

  $query = db_select('bbva_user_subscription', 'us');
  $query = $query->condition('us.name_user', $name);
  $query = $query->condition('us.date_subscription', $timestamp, ">=");

  $query = $query->fields('us', array('name_user'));

  $result = $query->execute()->rowCount();

  return $result;
}

/**
 * Consulta los registros de bbva_campanias con aviso_tc;
 * @param $uid El id de usuario
 */
function bbva_campanias_db_consulta_campanias_aviso_tc($uid) {
  $query = db_select('bbva_campanias', 'c')
    ->fields('c')
    ->condition('origen', BBVA_TARJETA_ORIGEN_SUSCRIPCION)
    ->condition('uid', $uid)
    ->orderBy('id', 'DESC');
  return $query->execute()->fetchObject();
}

/**
 * Consulta los registros de bbva_campanias cuya oferta esta vencida;
 * @param $fecha_actual Timestamp de la fecha de actual
 */
function bbva_campanias_db_consulta_campanias_vencidas($fecha_actual) {
  $query = db_select('bbva_campanias', 'c')
    ->fields('c')
    ->condition('origen', BBVA_TARJETA_ORIGEN_SUSCRIPCION)
    ->condition('vencimiento_oferta', $fecha_actual, "<");
  $result = $query->execute()->fetchAll();
  return $result;
}

/**
 * Guarda el registro en la tabla
 * @param $registro_campanias_tc Un registro de la tabla bbva_campanias
 * @param $numero_mensaje_motor Numero de mensaje que se obtiene de la consulta al motor de TC.
 * */
function bbva_campanias_db_mover_campana_historico($registro_campanias_tc, $numero_mensaje_motor=NULL) {
  $id = db_insert('bbva_campanias_historico')
    ->fields(array(
      'uid'=> $registro_campanias_tc->uid,
      'oferta_json'=> $registro_campanias_tc->oferta_json,
      'numero_mensaje'=> $numero_mensaje_motor,
      'fecha_evaluacion_oferta'=> $registro_campanias_tc->date_origen,
    ))->execute();
  if ($id > 0) {
    db_delete('bbva_campanias')
      ->condition('id', $registro_campanias_tc->id)
      ->execute();
  }
  return $id;
}
/**
Para formulario de campañas - beneficios
 */
function _bbva_campanias_beneficios_cupones_disponibles($string=NULL) {

  $query = db_select('eck_benefit', 'b');
  $query->leftJoin('field_data_field_published', 'b_pub', 'b_pub.entity_id = b.id');
  $query->leftJoin('field_data_field_benefit_date_start', 'b_ds', 'b_ds.entity_id = b.id');
  $query->leftJoin('field_data_field_benefit_date_end', 'b_de', 'b_de.entity_id = b.id');
  $query->leftJoin('field_data_field_benefit_app', 'b_app', 'b_app.entity_id = b.id');
  $query->leftJoin('field_data_field_benefit_campaign', 'benefit_c', 'benefit_c.entity_id = b.id');
  // Solo beneficios con cupones
  $query->join('field_data_field_coupon_promo_benefit','cp','b.id=cp.field_coupon_promo_benefit_target_id');

  // Fields and Conditions
  $query = $query->condition('b.type', 'benefit');
  $query = $query->condition('b_pub.field_published_value', 1);
  $query = $query->condition('b_app.field_benefit_app_value', 1);
  $query = $query->isNull('benefit_c.entity_id');
  $query = $query->condition('b_ds.field_benefit_date_start_value', date('Y-m-d H:i:s', strtotime('-15 days')), '>');

  // Campos
  $query = $query->fields('b', array('id', 'title'));
  $query = $query->fields('b_ds', array('field_benefit_date_start_value'));
  $query = $query->fields('b_de', array('field_benefit_date_end_value'));

  // Orden
  $query = $query->orderBy('b_ds.field_benefit_date_start_value', 'DESC');
  $query = $query->orderBy('b.title');

  $resultBenefitAll = $query->execute();

  $matches = array();

  // save the query to matches
  foreach ($resultBenefitAll as $row) {
    $fecha_inicio = date("d-m-Y", strtotime($row->field_benefit_date_start_value));
    $fecha_final = date("d-m-Y", strtotime($row->field_benefit_date_end_value));
    $matches[$row->id] = '<b>'.check_plain($row->title). "</b><i> (". $fecha_inicio. " a ".$fecha_final.')</i>';
  }

  return $matches;
}


function _get_campanias_view($filtros=NULL, $limit=NULL){

  $query = db_select('eck_campaign', 'c');
  $query->leftJoin('field_data_field_campaign_id', 'c_ci', 'c_ci.entity_id = c.id');
  $query->leftJoin('field_data_field_campaign_start_date', 'c_sd', 'c_sd.entity_id = c.id');
  $query->leftJoin('field_data_field_campaign_end_date', 'c_ed', 'c_ed.entity_id = c.id');


  // Fields and Conditions
  $query = $query->condition('c_sd.field_campaign_start_date_value', date('Y-m-d H:i:s', strtotime('-15 days')), '>');

  // Campos
  $query = $query->fields('c', array( 'id', 'title'));
  $query = $query->fields('c_ci', array('field_campaign_id_value'));
  $query = $query->fields('c_sd', array('field_campaign_start_date_value'));
  $query = $query->fields('c_ed', array('field_campaign_end_date_value'));

  if (is_array($filtros)) {
    if ($filtros["id_suip"]) {
      $query->condition('c_ci.field_campaign_id_value', trim($filtros["id_suip"]));
    }
    if ($filtros["titulo"] != '') {
      $query->condition('c.title', '%' . db_like(trim($filtros["titulo"])) . '%', 'LIKE');
    }
  }

  // Orden
  $query = $query->orderBy('c.created', 'DESC');
  $query = $query->orderBy('c_sd.field_campaign_start_date_value', 'DESC');
  $query = $query->orderBy('c.title');

  if ($limit != '') {
    $query = $query->extend('TableSort')->extend('PagerDefault')->limit($limit);
  }

  $resultCampaignAll = $query->execute()->fetchAll();

  if ($resultCampaignAll) {
    return $resultCampaignAll;
  }else{
    return null;
  }
}

function bbva_campanias_db_buscar_ofertastc($fecha_origen_oferta = null, $fecha_vencimiento_oferta = null) {
  $query = db_select('bbva_campanias', 'bc');

  $query = $query->isNotNull('bc.oferta_json');
  $query->condition('bc.origen', 1);
  if($fecha_origen_oferta) {
    $query->condition('bc.date_origen', $fecha_origen_oferta, "=");
  }
  if($fecha_vencimiento_oferta) {
    $query->condition('bc.vencimiento_oferta', $fecha_vencimiento_oferta, "=");
  } else {
    $query->condition('bc.vencimiento_oferta', time(), ">=");
  }

  $query = $query->fields('bc', array('uid'));
  $result = $query->execute()->fetchCol();

  return $result;
}

function bbva_campanias_db_borrar_campanias_avisotc($altamira_ids) {
  $deleted = db_delete('bbva_campanias_eventos')
    ->condition('tipo', "aviso_tc")
    ->condition('altamira_id', $altamira_ids, "IN")
    ->execute();
}

function bbva_campanias_db_consultar_ofertatc_x_usuario($uid) {
  $query = db_select('bbva_campanias', 'bc');

  $query->condition('bc.origen', 1);
  $query->condition('bc.uid', $uid);

  $query = $query->fields('bc');
  $query = $query->range(0,1);
  $result = $query->execute()->fetchAll();

  return $result;
}