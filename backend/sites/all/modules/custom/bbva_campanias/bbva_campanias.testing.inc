<?php

function bbva_campanias_testing_ona($dni) {
  $numero_dni = $dni;
  $query = db_select("field_data_field_profile_document_number", "dn");
  $query = $query->condition("dn.field_profile_document_number_value", $numero_dni);
  $query = $query->fields('dn', array('entity_id'));
  $result = $query->execute()->fetchAll();

  $data = array(
    'header' => array("AID", "UID")
  );

  foreach($result as $row) {
    $u_data = user_load($row->entity_id);
    $data_t['aid'] = $u_data->name;
    $data_t['uid'] = $u_data->uid;
    $data['rows'][] = $data_t;
  }
  return theme("table", $data);
}
