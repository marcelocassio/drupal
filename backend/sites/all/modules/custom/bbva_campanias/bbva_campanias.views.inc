<?php

/**
 * Views page.
 */

function bbva_campanias_view_admin(){

  $filtros = '';

  if ((isset($_GET['filter_id_suip'])) && ($_GET['filter_id_suip'] != '')) {
    $filtros["id_suip"] = $_GET['filter_id_suip'];
  }

  if ((isset($_GET['filter_titulo'])) && ($_GET['filter_titulo'] != '')) {
    $filtros["titulo"] = $_GET['filter_titulo'];
  }

  $view_campanias = _get_campanias_view($filtros, 100);
  return bbva_campanias_create_view($view_campanias);
}

function bbva_campanias_view_admin_submit($form, &$form_state) {
  $form_state['filters']['id_suip'] = $form_state['values']['filter_id_suip'];
  $form_state['filters']['titulo'] = $form_state['values']['filter_titulo'];
  $form_state['rebuild'] = TRUE;
}

function bbva_campanias_create_view($view_campanias){

  $header = array(
      array('data' => t('Nombre')),
      array('data' => t('Id')),
      array('data' => t('Fecha Comienzo')),
      array('data' => t('Fecha Final')),
      array('data' => t('Vista Campaña'))
  );

  $form = array();
  $data = array();

  $form['#method'] = 'get';

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buscar por:')
  );
  $form['filter']['filter_id_suip'] = array(
    '#type' => 'textfield',
    '#title' => t('ID: '),
    '#size' => 11,
    '#weight' => '1',
    '#default_value'=> isset($_GET['filter_id_suip']) ? $_GET['filter_id_suip'] : null,
  );

  $form['filter']['filter_titulo'] = array(
    '#type' => 'textfield',
    '#title' => t('Título: '),
    '#size' => 20,
    '#weight' => '2',
    '#default_value' => isset($_GET['filter_titulo']) ? $_GET['filter_titulo'] : null,
  );

  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Buscar'),
    '#weight' => '5'
  );

  // Tomamos los datos
  if(is_array($view_campanias)){
    foreach ($view_campanias as $row) {

      // Fecha inicio campaña
      if (($row->field_campaign_start_date_value == 0) || ($row->field_campaign_start_date_value == '')) {
        $fecha_inicio = "-";
      }else{
        $fecha_inicio = date('d-m-Y', strtotime($row->field_campaign_start_date_value));
      }

      // Fecha final campaña
      if (($row->field_campaign_end_date_value == 0) || ($row->field_campaign_end_date_value == '')) {
        $fecha_final = "-";
      }
      else{
        //. ' -1 day'
        $fecha_final = date('d-m-Y', strtotime($row->field_campaign_end_date_value));
      }

      $data[] = array(
        array('data' => $row->title, 'style' => 'font-size: 1em'),
        array('data' => $row->field_campaign_id_value, 'style' => 'font-size: 1em'),
        array('data' => $fecha_inicio, 'style' => 'font-size: 1em'),
        array('data' => $fecha_final, 'style' => 'font-size: 1em'),
        array('data' => array('#type' => 'link', '#title' => t('Vista'), '#href' => 'admin/campanas_experiencia/'.$row->field_campaign_id_value))
        );
      }
   }
  
    $form['table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $data,
      '#empty' => t('No hay campañas disponibles.')
    );

    $form['pager'] = array('#markup' => theme('pager'));

    return $form;
}
