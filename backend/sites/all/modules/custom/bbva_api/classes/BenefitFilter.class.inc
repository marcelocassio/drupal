<?php


final class BenefitFilter
{


	public static function buscarBeneficiosSimilares($unBeneficio, $listadoBeneficios)
    {

    	$benefitFilter=array();
        $rubros_seleccionados = $unBeneficio->rubro2;
        $id_beneficio_excluir = $unBeneficio->id;

		if ($rubros_seleccionados != null) {
			$rubros_seleccionados = explode(",", $rubros_seleccionados);
			foreach ($listadoBeneficios as $key => $beneficio) {

			  $rubros = $beneficio->rubro2;

			  if ($rubros == null || $id_beneficio_excluir == $beneficio->id) {
			    continue;
			  }

			  $rubros = explode(",", $rubros);

			  $result = array_intersect($rubros_seleccionados, $rubros);

			  if (!empty($result)) {
			    $benefitFilter[$beneficio->id]=$beneficio;
			  }
			}
		}

		return $benefitFilter;

    }

     public static function tienePuntosVentas($unBeneficioParser) {

    	if ((!$unBeneficioParser->locales_fisicos) &&
    		(!$unBeneficioParser->web) &&
    		(!$unBeneficioParser->tel) &&
    		(!$unBeneficioParser->zona_rubros) &&
            (!$unBeneficioParser->shops)){
    		return FALSE;
    	}
    	return TRUE;
    }

}
