<?php

define("BBVA_API_BENEFICIO_IMAGE_LARGE", "IMAGE_LARGE");

class BeneficioDto {

  const TEXT_REQUISITOS_CUPON = 'Presentando el cupón de descuento';

  const TEXT_REQUISITOS_TARJETA = 'Pagando con Tarjeta de Crédito BBVA';
  const TEXT_REQUISITOS_GO = 'Para registrados en Go';
  const TEXT_TOTAL_COMPRA = 'en el total de tu compra';
  const TEXT_CUOTAS = 'cuotas';

  const TIPO_BENEFICIO_CUPON = 'cupon';
  const TIPO_BENEFICIO_TARJETA = 'cartera';
  const TIPO_BENEFICIO_GO = 'go';

  const TIPO_DESCUENTO_TODO_PAGO_SERVICIOS_150 = 8885;
  const TIPO_DESCUENTO_TODO_PAGO_SERVICIOS_200 = 8886;
  const TIPO_DESCUENTO_TODO_PAGO = 8887;
  const TIPO_DESCUENTO_2X1 = 8888;
  const TIPO_DESCUENTO_KTM = 8889;

  const TIPO_IMAGE_LARGE = 'IMAGE_LARGE';

  const CODIGO_SUIP_DESTACADO = 9;

  function buildResumidoCercano($beneficioParseado) {
    $this->id = $beneficioParseado->id;
    $this->titulo = $beneficioParseado->title;
    $this->buildMarcaEsShopping($beneficioParseado);
    $this->addSucursalCercana($beneficioParseado);
    return $this;
  }

  function buildResumido($beneficioParseado) {
    $this->id = $beneficioParseado->id;
    $this->id_suip = $beneficioParseado->id_suip;
    $this->titulo = $beneficioParseado->title;
    $this->buildSubtitulo($beneficioParseado);
    $this->buildMarcaAccionComercial($beneficioParseado);
    $this->buildValoresBeneficios($beneficioParseado);
    $this->buildImagen(self::TIPO_IMAGE_LARGE, $beneficioParseado->image_web);
    $this->buildLinkCompartir(
      $beneficioParseado->share_link,
      $beneficioParseado->share_text,
      $beneficioParseado->share_image
    );
    return $this;
  }


  function build($beneficioParseado) {

    $this->id = $beneficioParseado->id;
    $this->id_suip = $beneficioParseado->id_suip;
    $this->titulo = $beneficioParseado->title;
    $this->buildSubtitulo($beneficioParseado);
    $this->buildMarcaAccionComercial($beneficioParseado);
    $this->buildValoresBeneficios($beneficioParseado);

    $this->buildCuando($beneficioParseado);
    $this->buildSucursales($beneficioParseado);

    $this->como_uso = $beneficioParseado->pasos_como_uso_v2;
    $this->buildImagen(self::TIPO_IMAGE_LARGE, $beneficioParseado->image);

    $this->tope = "";
    $this->info_adicional = NULL;
    $this->bases_condiciones = $beneficioParseado->legal;
    $this->bio = "";
    $this->rubro = $beneficioParseado->rubro;
    $this->rubro_name = $beneficioParseado->rubro_name;
    $this->cftna = $beneficioParseado->cftna;

    $this->buildLinkCompartir(
      $beneficioParseado->share_link,
      $beneficioParseado->share_text,
      $beneficioParseado->share_image
    );

    return $this;
  }

  function buildSubtitulo($beneficioParseado) {
    if (!isset($beneficioParseado->benefit_description)) {
      $this->subtitulo = t(self::TEXT_TOTAL_COMPRA);
    } else {
      $this->subtitulo = $beneficioParseado->benefit_description;
    }
  }


  function buildSucursales($beneficioParseado) {
    $sucursales = array();
    if (isset($beneficioParseado->sucursales)) {
      foreach ($beneficioParseado->sucursales as $sucu) {
        $sucursal = null;
        $sucursal->direccion = $sucu->street . " " . $sucu->number;
        $sucursal->localidad = $sucu->city;
        $sucursal->latitude = $sucu->latitude;
        $sucursal->longitude = $sucu->longitude;

        $sucursales[] = $sucursal;
      }
    }
    $this->canales_venta->marcas_adheridas_shopping = null;
    $this->canales_venta->zonas = (empty($beneficioParseado->zona_rubros)?null:$beneficioParseado->zona_rubros);
    $this->canales_venta->sucursales = $sucursales;
    $this->canales_venta->web = $beneficioParseado->web_v2;
    $this->canales_venta->telefono = $beneficioParseado->tel;
    if (isset($beneficioParseado->shops)) {
      $this->canales_venta->marcas_adheridas_shopping = $beneficioParseado->shops;
      $sucursal = null;
      $sucursal->direccion = $beneficioParseado->shopping_localization->street . " " . $beneficioParseado->shopping_localization->number;
      $sucursal->localidad = $beneficioParseado->shopping_localization->city;
      $sucursal->latitude = $beneficioParseado->shopping_localization->latitude;
      $sucursal->longitude = $beneficioParseado->shopping_localization->longitude;
      $this->canales_venta->sucursales[] = $sucursal;
    }

  }

  function buildMarcaAccionComercial($beneficioParseado) {
    $this->destacado = false;
    $canal_pertenencia = explode(',', $beneficioParseado->mc);
    if (in_array(self::CODIGO_SUIP_DESTACADO, $canal_pertenencia)) {
      $this->destacado = true;
    }

    // Va a estar disponible en el nuevo core.
    /*if (!empty($beneficioParseado->comercial_action) && $beneficioParseado->comercial_action != "NULL") {
      $this->campania = new \stdClass();
      $this->campania->id = 0;
      $this->campania->descripcion = $beneficioParseado->comercial_action;
    }*/
  }

  function buildBeneficiosSimilares($listadoBeneficios) {
    $beneficios_similares = array();
    foreach ($listadoBeneficios as $beneficio_similar) {
        $beneficio_similar_dto = new BeneficioDto();
        $beneficio_similar_dto->id = $beneficio_similar->id;
        $beneficio_similar_dto->titulo = $beneficio_similar->title;
        $beneficio_similar_dto->buildValoresBeneficios($beneficio_similar);
        $beneficio_similar_dto->buildMarcaAccionComercial($beneficio_similar);
        $beneficio_similar_dto->buildImagen(
              self::TIPO_IMAGE_LARGE,
              $beneficio_similar->image
            );

        $beneficios_similares[] = $beneficio_similar_dto;
    }
    $this->beneficios_similares = $beneficios_similares;
    return $this;
  }

  function buildCuando($beneficioParseado) {
    if (isset($beneficioParseado->benefit_application)) {
      $this->vigencia = $beneficioParseado->benefit_application;
    } else{
      $this->vigencia = "Del " . $beneficioParseado->fecha_desde . " al " . $beneficioParseado->fecha_hasta;
    }
  }

  function buildValoresBeneficios($beneficioParseado) {
    $beneficios_valores = array();

    if ($beneficioParseado->porcentaje_tarjeta_go != 0 ) {

      $descuento_go = $this->generarDescuentoCuotas(
        self::TIPO_BENEFICIO_GO,
        $beneficioParseado->porcentaje_tarjeta_go,
        $beneficioParseado->cuota_tarjeta,
        $beneficioParseado->benefit_description
      );

      //$descuento_go->requisitos = array(self::TEXT_REQUISITOS_GO, self::TEXT_REQUISITOS_TARJETA);
      $descuento_go->requisitos = array(self::TEXT_REQUISITOS_GO, $beneficioParseado->description);
      $beneficios_valores[] = $descuento_go;
    }

    if ($beneficioParseado->porcentaje_tarjeta != 0 ) {

      $descuento_cartera = $this->generarDescuentoCuotas(
        self::TIPO_BENEFICIO_TARJETA,
        $beneficioParseado->porcentaje_tarjeta,
        $beneficioParseado->cuota_tarjeta,
        $beneficioParseado->benefit_description
      );

      $descuento_cartera->requisitos = array($beneficioParseado->description);

      $beneficios_valores[] = $descuento_cartera;
    }

    if ($beneficioParseado->porcentaje_cupon != 0) {
    	$beneficio_cupon = $this->generarDescuentoCuotas(
        self::TIPO_BENEFICIO_CUPON,
        $beneficioParseado->porcentaje_cupon, 0,
        $beneficioParseado->benefit_description
      );
      $beneficio_cupon->requisitos = array(self::TEXT_REQUISITOS_CUPON);
      $beneficios_valores[] = $beneficio_cupon;

    }

    if ($beneficioParseado->porcentaje_cupon == 0 &&
        $beneficioParseado->porcentaje_tarjeta == 0 &&
        $beneficioParseado->porcentaje_tarjeta_go == 0 &&
        $beneficioParseado->cuota_tarjeta != "") {

      $descuento_cartera = $this->generarDescuentoCuotas(self::TIPO_BENEFICIO_TARJETA, 0, $beneficioParseado->cuota_tarjeta, $beneficioParseado->benefit_description, $beneficioParseado->type);

      //$descuento_cartera->requisitos = array(self::TEXT_REQUISITOS_TARJETA);
      $descuento_cartera->requisitos = array($beneficioParseado->description);

      $beneficios_valores[] = $descuento_cartera;
    }

    if(count($beneficios_valores) < 1) {
      $beneficio = new stdClass();
      $beneficio->resumen = "";

      if ($beneficioParseado->type  == self::TIPO_DESCUENTO_2X1) {
        $beneficio->resumen = "2x1";
        $beneficio->texto_grande = "2x1";
        $beneficio->texto_chico = "";
      } elseif($beneficioParseado->type  == self::TIPO_DESCUENTO_TODO_PAGO) {
        $beneficio->texto_grande = "$100";
        $beneficio->texto_chico = "de reintegro";
      } elseif($beneficioParseado->type  == self::TIPO_DESCUENTO_TODO_PAGO_SERVICIOS_200) {
        $beneficio->texto_grande = "$200";
        $beneficio->texto_chico = "de reintegro";
        $beneficio->resumen = $beneficio->texto_grande . " ". $beneficio->texto_chico;
      } elseif($beneficioParseado->type  == self::TIPO_DESCUENTO_TODO_PAGO_SERVICIOS_150) {
        $beneficio->texto_grande = "$150";
        $beneficio->texto_chico = "de reintegro";
        $beneficio->resumen = $beneficio->texto_grande . " ". $beneficio->texto_chico;
      } else {
        $beneficio->texto_grande = "";
        $beneficio->texto_chico = "Preventa exclusiva";
      }

      $subtitulo_texto = t(self::TEXT_TOTAL_COMPRA);
      if ($beneficioParseado->benefit_description) {
        $subtitulo_texto = $beneficioParseado->benefit_description;
      }
      if($beneficioParseado->type  == self::TIPO_DESCUENTO_TODO_PAGO_SERVICIOS_200 || $beneficioParseado->type  == self::TIPO_DESCUENTO_TODO_PAGO_SERVICIOS_150) {
        $beneficio->texto = $subtitulo_texto;
      }else{
        $beneficio->texto = $beneficio->resumen . " ". $subtitulo_texto;
      }
      $beneficio->tipo = self::TIPO_BENEFICIO_TARJETA;
      $beneficio->requisitos = array($beneficioParseado->description);

      $beneficios_valores[] = $beneficio;
    }

    $this->beneficios_valores = $beneficios_valores;
  }

  function generarDescuentoCuotas($tipo_beneficio_tarjeta, $porcentaje_descuento, $cant_cuotas, $subtitulo = NULL, $tipo_beneficio = NULL) {
    $beneficio = new stdClass();
    $beneficio->texto_chico = null;
    if ($porcentaje_descuento != 0) {
    	$beneficio->tipo = $tipo_beneficio_tarjeta;
      $beneficio->resumen = $porcentaje_descuento."%";
      $subtitulo_texto = t(self::TEXT_TOTAL_COMPRA);
      if ($subtitulo) {
        $subtitulo_texto = $subtitulo;
      }
      $beneficio->texto = $beneficio->resumen . " ". $subtitulo_texto;
      $beneficio->texto_grande = $porcentaje_descuento."%";
    }

    if ($cant_cuotas != 0) {
      $beneficio->tipo = $tipo_beneficio_tarjeta;
    	if ($porcentaje_descuento != 0) {
        $texto_cuotas = " y " . $cant_cuotas. " " .self::TEXT_CUOTAS;
        $beneficio->resumen .= $texto_cuotas;
        $beneficio->texto_chico = $texto_cuotas;
      } else {
        $beneficio->resumen = $cant_cuotas. " " .self::TEXT_CUOTAS;
        $beneficio->texto_grande = $cant_cuotas. " " .self::TEXT_CUOTAS;
    	}
      if ($tipo_beneficio  == self::TIPO_DESCUENTO_KTM){
        $beneficio->texto = $subtitulo;
      }else{
        $beneficio->texto = $beneficio->resumen." ".t(self::TEXT_TOTAL_COMPRA);
      }
    }
    return $beneficio;
  }

  function buildImagen($image_type, $url) {
    $image = new stdClass();
    $image->tipo = $image_type;
    $image->name = "";
    $image->url = $url;
    $this->imagenes[] = $image;
  }

  function buildLinkCompartir($link = null, $texto = null, $imagen = null) {
    $compartir = new stdClass();
    $compartir->link = $link;
    $compartir->texto = $texto;
    $compartir->imagen = $imagen;
    $this->compartir = $compartir;
  }

  function addSucursalCercana($beneficioCercanosParseado) {
    $sucursal = new stdClass();
    $sucursal->direccion = $beneficioCercanosParseado->street;
    $sucursal->localidad = $beneficioCercanosParseado->city;
    $sucursal->latitude = $beneficioCercanosParseado->latitude;
    $sucursal->longitude = $beneficioCercanosParseado->longitude;
    $this->sucursales_cercanas[] = $sucursal;
    return $this;
  }

  function buildMarcaEsShopping($beneficioParseado) {
    $this->es_shopping = isset($beneficioParseado->shops) ? $beneficioParseado->shops : false;
  }
}
