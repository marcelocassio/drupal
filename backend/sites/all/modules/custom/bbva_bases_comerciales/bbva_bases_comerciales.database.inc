<?php

function _bbva_bases_comerciales_guardar_base_comercial($datos_para_guardar){
  try {
    $guarda_ganador = db_insert('bbva_bases_comerciales')
      ->fields($datos_para_guardar)
      ->execute();
  }catch (PDOException $e) {
    watchdog('bbva_bases_comerciales', 'No se pudo guardar el siguiente altamira en la campania , %error', array('%error' => htmlspecialchars_decode(print_r("Campania: ".$datos_para_guardar["id_campania"]."- Altamira: ".$datos_para_guardar["altamira"], true))));
  }

}

function _bbva_bases_comerciales_get_all($filtros){

  $order = "id";
  $sort = "asc";

  $query = db_select('bbva_bases_comerciales', 'bc');
  $query->fields('bc', array('altamira', 'nombre', 'apellido', 'email', 'sexo','fecha_nacimiento','segmento','marcatc','tipo','limitetdcvisa','url','hash'));

  $query->condition('bc.id_campania', $filtros, '=');

  $query->orderBy($order, $sort);

  if ($limit != '') {
    //$query = $query->extend('TableSort')->extend('PagerDefault')->limit($limit);
  }

  $results = $query->execute()->fetchAll();
  if ($results) {
    return $results;
  }else{
    return null;
  }
}

function _delete_csv_bases_comerciales($node_id){

  if ($node_id != '') {
    $query = db_select('bbva_bases_comerciales', 'bc');
    $query->addField('bc', 'id_campania');
    $query->condition('bc.id_campania', $node_id,'=');

    $result = $query->execute()->fetchAll();

    if ($result) {
      $deleted = db_delete('bbva_bases_comerciales')
        ->condition('id_campania', $node_id)
        ->execute();
    }
  }
}

function bbva_bases_comerciales_db_datos_hash($hash) {
  $query = db_select('bbva_bases_comerciales', 'bbc');
  $query = $query->fields('bbc');
  $query = $query->condition('bbc.hash', $hash);
  $result = $query->execute()->fetchObject();

  return $result;
}

function bbva_bases_comerciales_db_datos_hash_campania($hash_campania) {
  $query = db_select('bbva_bases_comerciales', 'bbc');
  $query = $query->fields('bbc');
  $query = $query->condition('bbc.hash_campania', $hash_campania);
  $result = $query->execute()->fetchObject();

  return $result;
}

function bbva_bases_comerciales_db_insertar_hash_campania($id, $hash_campania) {
  $query = db_merge('bbva_bases_comerciales');
  $query = $query->key(
    array(
      'id' => $id
    )
  );
  $query = $query->fields(array('hash_campania' => $hash_campania));
  $result = $query->execute();

  return $result;
}

function bbva_bases_comerciales_cambiar_estado($nodo_id) {
  $node = node_load($nodo_id);
  $bc_wrapper = entity_metadata_wrapper('node', $node);
  $bc_wrapper->field_bbva_bc_estado->set(1);
  $bc_wrapper->save();
}

function _bbva_bases_comerciales_get_tipo_tarjeta($id){

  $query = db_select('field_data_field_bbva_bc_tipotc', 'tipotc');
  $query->fields('tipotc', array('field_bbva_bc_tipotc_value'));

  $query->condition('tipotc.entity_id', $id);

  $results = $query->execute()->fetchField();
  if ($results) {
    return $results;
  }else{
    return null;
  }

}
