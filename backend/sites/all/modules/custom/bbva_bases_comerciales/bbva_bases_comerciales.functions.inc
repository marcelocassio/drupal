<?php

function bbva_bases_comerciales_leer_csv($fid_csv_data = null, $node_id, $tag_omniture = null){
  global $base_url;
  if ($fid_csv_data) {
    $file = file_load($fid_csv_data);
    $uri = $file->uri;

    $handle = fopen($uri, 'r');
    $row = fgetcsv($handle);
    $columns = array();
    foreach ($row as $i => $header) {
      $columns[$i] = trim($header);
    }

    while ($row = fgetcsv($handle)) {
      $record = array();
      foreach ($row as $i => $field) {
        $datos_para_guardar = array();
        $record[$columns[$i]] = $field;
        $data = explode(";", $record[$columns[$i]]);

        $datos_para_guardar["id_campania"] = $node_id;
        $datos_para_guardar["fecha_subida"] = REQUEST_TIME;
        $datos_para_guardar["tipo_telefono"] = "CEL";

        //Numero Cliente Altamira
        if ( (is_numeric($data[0])) && ($data[0] != '')) {
          $datos_para_guardar["altamira"] = ltrim((string)((int)$data[0]), 0);
          // Hash
          $hash = md5($datos_para_guardar["altamira"] . time());
          $datos_para_guardar["hash"] = $hash;
        }else{
          $datos_para_guardar["altamira"] = '';
          $datos_para_guardar["hash"] = 0;
        }

         // tipo_documento
        if ($data[1] != '') {
          $tipo_documento = trim($data[1]);
          if (strlen($tipo_documento) == 1) {
            $tipo_documento = "0".$tipo_documento;
          }
          $datos_para_guardar["tipo_documento"] = $tipo_documento;
        }else{
          $datos_para_guardar["tipo_documento"] = "0";
        }
        // numero documento
        if ($data[2] != '') {
          $numero_documento = trim($data[2]);
          $datos_para_guardar["numero_documento"] = $numero_documento;
        }else{
          $datos_para_guardar["numero_documento"] = "0";
        }

        // Sexo
        if ($data[3] != '') {
          $datos_para_guardar["sexo"] = utf8_encode(trim($data[3]));
        }else{
          $datos_para_guardar["sexo"] = '';
        }

        // Nombre
        if ($data[4] != '') {
          $datos_para_guardar["nombre"] = utf8_encode(ucwords(mb_strtolower(trim($data[4]))));
        }else{
          $datos_para_guardar["nombre"] = '';
        }

        // Apellido
        if ($data[5] != '') {
          $datos_para_guardar["apellido"] = utf8_encode(ucwords(mb_strtolower(trim($data[5]))));
        }else{
          $datos_para_guardar["apellido"] = '';
        }

        // Fecha de nacimiento
        if ($data[6] != '') {
          $fecha_nacimiento = DateTime::createFromFormat('d/m/Y', trim($data[6]))->getTimestamp();
          $datos_para_guardar["fecha_nacimiento"] = $fecha_nacimiento;
        }else{
          $datos_para_guardar["fecha_nacimiento"] = NULL;
        }

         // Nacionalidad
        if ($data[7] != '') {
          $nacionalidad = strtoupper(trim($data[7]));
          // Nacionalidades
          $nacionalidades_list = bbva_subscription_get_nationality_type_list();
          $key_nacionalidad = array_search($nacionalidad, $nacionalidades_list);
          if ($key_nacionalidad) {
            $datos_para_guardar["nacionalidad"] = $key_nacionalidad;
          }else{
            $datos_para_guardar["nacionalidad"] = NULL;
          }
        }else{
          $datos_para_guardar["nacionalidad"] = NULL;
        }

        // Segmento
        if ($data[8] != '') {
          $segmento_temp = trim($data[8]);
          $segmento = bbva_bases_comerciales_obtener_titulo_segmento($segmento_temp);

          if ($segmento) {
            $datos_para_guardar["segmento"] = $segmento;
          }else{
            $segmento_temp = mb_strtolower(trim($segmento_temp));
            switch ($segmento_temp) {
              case 'clasico':
                $datos_para_guardar["segmento"] = "Masivo";
                break;
              case 'premium':
                $datos_para_guardar["segmento"] = "Premium";
                break;
              case 'premium world':
                $datos_para_guardar["segmento"] = "PW";
                break;
              default:
                $datos_para_guardar["segmento"] = $segmento_temp;
                break;
            }
          }
        }

        // Tipo
        if ($data[9] != '') {
          $tipo_temp = utf8_encode($data[9]);
          $tipo_temp = str_replace("Visa", "", $tipo_temp);
          $datos_para_guardar["tipo"] = trim($tipo_temp);
        }else{
          $datos_para_guardar["tipo"] = '';
        }

        // Limite TDC Visa
        if ( (is_numeric($data[10])) && ($data[10] != '')) {
          $datos_para_guardar["limitetdcvisa"] = $data[10];
        }else{
          $datos_para_guardar["limitetdcvisa"] = '';
        }

        // Adicionales
        $datos_para_guardar["adicional"] = 0;
        if ($data[11] != '') {
          $adicional_temp = strtolower(trim($data[11]));
          $adicional = substr($adicional_temp, 0, 2);
          if ($adicional == "es") {
            $datos_para_guardar["adicional"] = 1;
          }
        }

        // Email
        if ($data[12] != '') {
          $datos_para_guardar["email"] = utf8_encode(strtolower(trim($data[12])));
        }else{
          $datos_para_guardar["email"] = '';
        }

        // caracteristica telefono
        if ($data[13] != '') {
          $caracteristica_telefono = trim($data[13]);
          $datos_para_guardar["caracteristica_telefono"] = $caracteristica_telefono;
        }else{
          $datos_para_guardar["caracteristica_telefono"] = NULL;
        }

        // Numero telefono
        if ($data[14] != '') {
          $numero_telefono = trim($data[14]);
          $datos_para_guardar["numero_telefono"] = $numero_telefono;
        }else{
          $datos_para_guardar["numero_telefono"] = NULL;
        }

        // Marca TC (Siempre va Visa)
        $datos_para_guardar["marcatc"] = "Visa";

        // URL con hash
        if ($tag_omniture) {
          $datos_para_guardar["url"] = variable_get('fnet.sitio')."fgo/oferta_preaprobada/" . $datos_para_guardar["hash"]."/".$tag_omniture;
        }else{
          $datos_para_guardar["url"] = variable_get('fnet.sitio')."fgo/oferta_preaprobada/" . $datos_para_guardar["hash"];
        }

        _bbva_bases_comerciales_guardar_base_comercial($datos_para_guardar);
      }
    }
    fclose($handle);
  }
}

function bbva_bases_comerciales_obtener_titulo_segmento($segmento){
  //$array_segmentos = array("81100" => "Premium World Titular", "81800" => "Premium World Cotitular", "82200" => "Premium Titular", "82800" => "Premium Cotitular", "83300" => "Classic Titular", "83800" => "Classic Cotitular", "84400" => "Inclusión Financiera Titular", "84800" => "Inclusión Financiera Cotitular", "86000" => "Masivo");
  $array_segmentos = array("81100" => "PW", "81800" => "PW", "82200" => "Premium", "82800" => "Premium", "83300" => "Masivo", "83800" => "Masivo", "84400" => "Masivo", "84800" => "Masivo", "86000" => "Masivo");
  if (array_key_exists($segmento, $array_segmentos)) {
    return $array_segmentos[$segmento];
  }else{
    return null;
  }
}
