<?php

/*
 * Implements hook_drush_command().
 */
function bbva_cache_api_drush_command() {

  $items['set-cache-api'] = array(
    'description' => 'Realiza el cache de las apis',
    'aliases' => array('cacheapi'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  $items['set-cache-api-all'] = array(
    'description' => 'Realiza el cache de las apis',
    'aliases' => array('cacheapiall'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_cache_api_set_cache_api() {
  $batch = array(
    'operations' => array(),
    'title' => t('Cache batch process'),
    'init_message' => t('Cache api start is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_cache_api_finished',
  );

  $batch['operations'][] = array('bbva_cache_api_set', array($result));
  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_cache_api_set_cache_api_all() {
  
  // cache_set_api_all();
}
