(function($) {

    function borrarDirecciones(id_direccion){
        for (var i = 1; i < 8; i++) {
            if (id_direccion != i ){
                jQuery("input[name=callToActionDireccion_"+ i +"]")
                    .not('input[name=callToActionDireccion_5]')
                    .val("");
            }
        }
    }

    Drupal.behaviors.fgo_backend_home = {
    attach: function (context, settings) {
        var seccion = jQuery("select[name='callToActionSeccion']").val();

        if (seccion != ""){
            borrarDirecciones(seccion);
        }
        jQuery("select[name='callToActionSeccion']").change(function(){
          borrarDirecciones(jQuery(this).val());
        });
    }
  };


})(jQuery);
