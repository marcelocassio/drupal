<?php
function fgo_backend_home_guardar_imagen_drupal($fid, $codigoImagen) {
  $resultado = null;
  $imagenBoId = null;

  $imagenFid = $fid;
  $fileimagen = file_load($imagenFid);
  $fileimagen->status = FILE_STATUS_PERMANENT;
  $imagen_data_web = file_save($fileimagen);

  $imagenBo = new \Fgo\Bo\ImagenBo();
  $imagenBo->buscarPorFid($imagenFid);

  if (!empty($imagenBo->idImagen)){
    $imagenBoId = $imagenBo->idImagen;
  }

  //La imagen se guarda en nuestra BD
  $crearImagen = fgo_backend_guardar_imagen_bo($imagen_data_web, $imagenBoId, $codigoImagen);

  if ($crearImagen){
    $resultado = $crearImagen;
  }

  return $resultado;
}