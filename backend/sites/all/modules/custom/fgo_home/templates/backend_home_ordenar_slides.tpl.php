<div id="main">
    <h1>Administración para Ordenar Slides</h1>
    <div class="container container-ordenar-lista">
        <div class="row">
            <vuedals></vuedals>
            <div class="container-fluid machine-contents list col-md-4">
                <div class="container_header_mini"><h2>Fecha de Slides</h2></div>
                <date-picker placeholder="Seleccioná la fecha" autocomplete="off" name="date" v-model="fechaInput" :config="options"></date-picker>
            </div>
        </div>
        <div class="row">

        <div class="container-fluid machine-contents list col-md-4">
                <div class="container_header_mini"><h2>Carrusel APP / Web</h2></div>
                <draggable class="dragArea" :list="carrusel" v-model="carrusel" :options="sortOptions" :move="checkMove" @change="log">
                    <transition-group name="list-complete">
                        <li v-for="(slide, index) in carrusel" :key="slide.idSlideFecha" class="list-group-item">
                            <div id="carrusel_vacio" v-if="slide.idSlideFecha===0">
                                Slides en Carrusel
                            </div>
                            <div v-else>
                                <div class="item_lista_slides_imagen">
                                    <img v-bind:src="slide.imageApp">
                                    <div class="item_lista_slides_imagen_tag app">App</div>
                                </div>
                                <div class="item_lista_slides_fechas">{{ slide.fechaDesde }} <br>al<br> {{ slide.fechaHasta }}</div>
                                <div class="item_lista_slides_imagen web">
                                    <img v-bind:src="slide.imageWeb">
                                    <div class="item_lista_slides_imagen_tag web">Web</div>
                                </div>
                            </div>
                        </li>
                    </transition-group>
                </draggable>
            </div>

            <div class="container-fluid machine-contents list col-md-4">
                <div class="container_header_mini"><h2>Slides para seleccionar</h2></div>
                <draggable class="dragArea" :list="stock" v-model="stock" :options="sortOptions" :move="checkMove" @change="log">
                    <transition-group name="list-complete">
                        <li v-for="(slide, index) in stock" :key="slide.idSlideFecha" class="list-group-item">
                            <div id="stock_vacio" v-if="slide.idSlideFecha===0">
                                Slides en Stock
                            </div>
                            <div v-else>
                                <div class="item_lista_slides_imagen">
                                    <img v-bind:src="slide.imageApp">
                                    <div class="item_lista_slides_imagen_tag app">App</div>
                                </div>
                                <div class="item_lista_slides_fechas">{{ slide.fechaDesde }} <br>al<br> {{ slide.fechaHasta }}</div>
                                <div class="item_lista_slides_imagen web">
                                    <img v-bind:src="slide.imageWeb">
                                    <div class="item_lista_slides_imagen_tag web">Web</div>
                                </div>
                            </div>
                        </li>
                    </transition-group>
                </draggable>
            </div>

            <div class="container-fluid machine-contents col-md-12">
                <button @click="guardarSlide()" class="boton_fgo boton_positivo" id="boton_guardar_slide">Guardar Carrusel</button>
            </div>

        </div>
    </div>
</div>
<script>
    const link = 'https://' + window.location.host + '/fgo/API/v1/backend/slides/fecha/';
    const linkGuardar = 'https://' + window.location.host + '/fgo/API/v1/backend/slides/guardar_orden/';
    var slideNeutro = {"idSlideFecha":0};

    // Contantes para el modal
    const Bus = Vuedals.Bus;
    const Component = Vuedals.Component;
    const Plugin = Vuedals.default;

    Vue.use(Plugin);

    // Initialize as global component
    Vue.component('date-picker', VueBootstrapDatetimePicker);

    // Using font-awesome 5 icons
    jQuery.extend(true, jQuery.fn.datetimepicker.defaults, {
        icons: {
            time: 'far fa-clock',
            date: 'far fa-calendar',
            up: 'fas fa-arrow-up',
            down: 'fas fa-arrow-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right',
            today: 'fas fa-calendar-check',
            clear: 'far fa-trash-alt',
            close: 'far fa-times-circle'
        }
    });

    new Vue({
        el: '#main',
        components: {
            vuedals: Component
        },
        data: {
            fechaInput: null,
            options: {
                format: "DD/MM/YYYY",
                useCurrent: true,
                showClear: true,
                showClose: true,
                locale: 'es',
                showTodayButton: true,
                minDate: moment().subtract(1, 'month').format()
            },
            sortOptions: {
                animation: 180,
                group: 'slides',
                draggable: 'li'
            },
            carrusel: new Array(slideNeutro),
            stock: new Array(slideNeutro)
        },
        methods: {
            /**
            * Verifica si existe algún Slide que viene del servicio
            * Si hay un Slide, se oculta el que está cargado por default
            * Si no hay Slide, se muestra el que está cargado por default
            * */
            checkSlides(){
                let var_carrusel_vacio = jQuery("#carrusel_vacio").parent("li");
                let var_stock_vacio = jQuery("#stock_vacio").parent("li");

                if(this.carrusel.length > 1){
                    var_carrusel_vacio.hide(100);
                }else{
                    var_carrusel_vacio.show(100);
                }

                if(this.stock.length > 1){
                    var_stock_vacio.hide(100);
                }else{
                    var_stock_vacio.show(100);
                }
            },
            /**
            * Obtiene los slides con o sin orden cargados previamente
            * Se instancia siempre de vuelta los arrays stock y carrusel para tener el slide = 0
            * Si se obtienen datos del endpoint, se agrega a los Arrays 'stock' y 'carrusel' según su orden
            * Luego de haber hecho la llamada no importando la respuesta, se vuelve a llamar a la función @checkSlides()
            * @param {string} fechaFormateada - La fecha en timestamp
            * */
            getSlides(fechaFormateada) {
                jQuery("body").css('cursor', 'wait');
                axios
                    .get(link + fechaFormateada)
                    .then(response => {
                        if (response.status === 200 && response.data.code === 0){
                            var slides = response.data.data.slides;
                            this.carrusel = new Array(slideNeutro);
                            this.stock = new Array(slideNeutro);

                            if (slides.length) {
                                slides.forEach(item => {
                                    var obj = {};
                                    obj["idSlideFecha"] = item.idSlideFecha;
                                    obj["imageApp"] = item.imageApp;
                                    obj["imageWeb"] = item.imageWeb;
                                    obj["fechaDesde"] = item.fechaDesde;
                                    obj["fechaHasta"] = item.fechaHasta;
                                    if (item.orden){
                                        this.carrusel.push(obj);
                                    }else{
                                        this.stock.push(obj);
                                    }
                                })
                            }
                        }
                        this.checkSlides();
                    })
                    .catch(error => {
                        this.openBadModal();
                    })
                    .finally(() => { this.loading = false; jQuery("body").css('cursor', 'default') })
            },
            /*
            * No deja mover el SlideNeutro
            * */
            checkMove(evt){
                return (evt.draggedContext.element.idSlideFecha!==0);
            },
            /**
            * Cada vez que se agrega o se borra un slide de una columna se llama a la función @checkSlide()
            * */
            log(event){
                if(event.removed){
                    this.checkSlides();
                }
                if(event.added){
                    this.checkSlides();
                }
            },
            /**
            * Cuando se clickea botón guardado
            * */
            guardarSlide() {
                let carruselForE = this.carrusel;
                let stockForE = this.stock;

                let carruselGuardar = carruselForE.map(a => a.idSlideFecha).join(',');
                let stockGuardar = stockForE.map(a => a.idSlideFecha).join(',');

                if(carruselGuardar == 0){
                    this.openValidateModal();
                    return;
                }
                jQuery("body").css('cursor', 'wait');
                axios
                    .get(linkGuardar + carruselGuardar + "/" + stockGuardar)
                    .then(response => {
                        if (response.status === 200){
                            let respuestaSlides = response.data;
                            if(respuestaSlides.data == "OK"){
                                this.openGoodModal();
                            }else{
                                this.openBadModal();
                            }
                        }
                    })
                    .catch(error => {
                        this.openBadModal();
                    })
                    .finally(() => { this.loading = false; jQuery("body").css('cursor', 'default') })
            },
            openGoodModal() {
                Bus.$emit('new', {
                    title: 'Slide/s Guardados',
                    component: goodModal
                });
            },
            openBadModal() {
                Bus.$emit('new', {
                    title: 'Slide Error',
                    component: badModal
                });
            },
            openValidateModal() {
                Bus.$emit('new', {
                    title: 'Carrousel sin Slides cargados',
                    component: validateModal
                });
            }
        },
        /**
        * Cuando se carga la página por primera vez
        * */
        created() {
          this.checkSlides();
        },
        watch: {
            /**
             * Cuando se cambia la fecha en Datepicker se formatea la fecha a timestamp
             * y luego se pasa como parametro a getSlides() para hacer la llamada al endpoint
             * */
            fechaInput() {
                let fechaFormateada = moment(this.fechaInput, 'DD/MM/YYYY').format("X");
                this.getSlides(fechaFormateada);
            }
        },
    });
</script>
