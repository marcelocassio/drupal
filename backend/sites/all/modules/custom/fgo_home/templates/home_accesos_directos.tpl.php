<div id="container-particles-js">
  <div id="particles-js"></div>
</div>

<div id="main">
  <h1>Accesos directos</h1>
  <div class="container-fluid machine-contents col-md-12 mb-2">
    <div class="d-flex">
      <button @click="nuevo()" class="boton_fgo boton_positivo ml-auto">Agregar Acceso directo</button>
      
    </div>
  </div>
  <div class="container col-md-12">
    <div class="row">
      <vuedals></vuedals>
      <div class="container-fluid machine-contents list">
        <draggable class="dragArea" :list="accesos_directos" v-model="accesos_directos" :options="sortOptions">
          <transition-group name="list-complete">
            <li v-for="(acceso_directo, index) in accesos_directos" :key="acceso_directo.idAccesoDirecto" class="list-group-item">
              <div>
                <div class="item_lista_slides_fechas d-flex align-items-center">
                  <input type="checkbox" v-bind:name="'acceso_directo[' + acceso_directo.idAccesoDirecto + ']'" v-bind:id="'acceso_directo_' + acceso_directo.idAccesoDirecto" v-bind:checked="(acceso_directo.visible) == 1 ? 'checked' : ''">
                  <label v-bind:for="'acceso_directo_' + acceso_directo.idAccesoDirecto" class="mr-auto"> {{ acceso_directo.nombre }}</label>
                  <button @click="editar(acceso_directo.idAccesoDirecto)" class="boton_fgo boton_positivo"  >Editar</button>
                  <button style="display: none" @click="eliminar(acceso_directo.idAccesoDirecto)" class="boton_fgo boton_negativo ml-2" >Eliminar</button> 
                </div>
              </div>
            </li>
          </transition-group>
        </draggable>
      </div>
      <div class="container-fluid machine-contents col-md-12">
        <button @click="guardar()" class="boton_fgo boton_positivo" id="  boton_guardar_slide">Guardar</button>
      </div>
    </div>
  </div>
</div>

<script>
 // const linkGuardar = '<?php echo $base_url?>/admin/v2/home/accesos_directos_change/guardar';
  const linkGuardar = 'https://' + window.location.host + '/fgo/admin/v2/home/accesos_directos_change/guardar';

  // Contantes para el modal
  const Bus = Vuedals.Bus;
  const Component = Vuedals.Component;
  const Plugin = Vuedals.default;

  Vue.use(Plugin);

  new Vue({
    el: '#main',
    components: {
      vuedals: Component
    },
    data: {
      sortOptions: {
        animation: 180,
        group: 'accesos_directos',
        draggable: 'li'
      },
      accesos_directos: <?php echo json_encode($accesos_directos);?>,
    },
    methods: {
      /**
      * Cuando se clickea botón guardado
      * */
      guardar() {
        let new_accesos_directos = this.accesos_directos.map(
          function(item, index){
            let result = new Object();
            result.orden = index;
            result.idAccesoDirecto = item.idAccesoDirecto;
            result.visible = jQuery("input#acceso_directo_" + item.idAccesoDirecto).is(":checked");

            return result;
          });
        
          axios.post(linkGuardar, {data: new_accesos_directos})
          .then(response => {
            if (response.status === 200){
              if(response.data.data == "OK"){
                this.openGoodModal();
              }
              else{
                console.log(new_accesos_directos);
                this.openBadModal();
              }
            }
        })
        .catch(error => {
          this.openBadModal();
        })
        .finally(() => { this.loading = false; jQuery("body").css('cursor', 'default') })
      },
      
      nuevo() {
        window.location.href = '<?php echo $base_url?>/admin/v2/home/accesos_directos/agregar';
      },
      
      editar(idAccesoDirecto) {
        window.location.href = '<?php echo $base_url?>/admin/v2/home/accesos_directos/editar/' + idAccesoDirecto;
      },
      
      eliminar(idAccesoDirecto) {
        window.location.href = '<?php echo $base_url?>/admin/v2/home/accesos_directos/eliminar/' + idAccesoDirecto;
      },
      
      openGoodModal() {
        Bus.$emit('new', {
          title: 'Accesos directos guardados',
          component: goodModal
        });
      },
    
      openBadModal() {
        Bus.$emit('new', {
          title: 'Hubo un error',
          component: badModal
        });
      }
    },
  });
 verTodos =  function verTodos(){
   jQuery(".boton_negativo").css("display", "block");
  }
</script>
