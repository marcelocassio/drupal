<div id="container-particles-js">
  <div id="particles-js"></div>
</div>
<div id="admin_container">
  <h1>Administración de Slides para Carrusel</h1>
  <div v-if="loading" class="spinner_backend" v-cloak>
    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
  </div>
  <vue-good-table v-else
    :columns="formattedColumns"
    :rows="slides"
    style-class="vgt-table striped condensed"
    :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: true, perPage: 40, ofLabel: 'de'}"
    :search-options="{ enabled: true, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
    <div slot="table-actions">
      <button id="boton_agregar_slide" @click="newSlide">Agregar Slide</button>
    </div>
    <div v-slot:emptystate>
      No hay slides con ese criterio
    </div>
    <template v-slot:table-row="props">
      <div v-if="props.column.field == 'publicado'">
        <div v-html="props.row.publicado"></div>
      </div>
      <div v-else-if="props.column.field == 'backendImagenWeb'">
        <div v-html="props.row.backendImagenWeb"></div>
      </div>
      <div v-else-if="props.column.field == 'backendImagenApp'">
        <div v-html="props.row.backendImagenApp"></div>
      </div>
      <div v-else-if="props.column.field == 'botones'">
        <div v-html="props.row.botones"></div>
        <div class="accion_borrar_slide">
          <i @click="openDeleteModal(props.row.id)" class="fas fa-window-close fa-3x fa-fw"></i>
        </div>
      </div>
      <div v-else>
        {{props.formattedRow[props.column.field]}}
      </div>
    </template>
  </vue-good-table>
  <vuedals></vuedals>
</div>

<script>
  const link = 'https://'+window.location.host+'/fgo/API/v1/backend/slides';
  const linkAdd = 'https://'+window.location.host+'/fgo/admin/v2/home/agregar';
  const linkBorrar = 'https://'+window.location.host+'/fgo/API/v1/backend/slides/borrar/';
  const FGO_BACKEND_IMG_DEFAULT = '/fgo/static/media/logogo.1cb228c2.svg';

  // Contantes para el modal
  const Bus = Vuedals.Bus;
  const Component = Vuedals.Component;
  const Plugin = Vuedals.default;

  Vue.use(Plugin);

  // Componente para borrar Slide
  const borrarSlide = {
    name: 'inside-modal',
    methods: {
      closeModal(){
        this.$vuedals.close();
      },
      openBadModal() {
        Bus.$emit('new', {
          title: 'Slide Error',
          component: badModal
        });
      },
      borrarSlide() {
        this.closeModal();
        this.$parent.$parent.loading = true;
        axios
          .get(linkBorrar + this.$parent.$parent.idSlide)
            .then(response => {
                if (response.status === 200){
                    let respuestaBorrarSlide = response.data;
                    if(respuestaBorrarSlide.data == "OK"){
                        this.$parent.$parent.getSlides();
                    }else{
                        this.openBadModal();
                    }
                }
            })
              .catch(error => {
                  this.$parent.$parent.slides = null;
              })
              .finally(() => this.$parent.$parent.loading = false)
      },
      },
        template: ' <div class="container">' +
                        '<div class="row">' +
                            '<div class="col-6">' +
                                '<button @click="borrarSlide()" id="borrar_slide_aceptar">Borrar Slide</button>' +
                            '</div>' +
                            '<div class="col-6">' +
                                '<button @click="closeModal()" id="borrar_slide_cancelar">Cancelar</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
    };

    new Vue({
        el: '#admin_container',
        components: {
            vuedals: Component
        },
        data: {
            slides: [],
            idSlide: null,
            loading: false,
            columns: [
                {
                    label: 'Publicado',
                    field: 'publicado',
                    type: 'number',
                    globalSearchDisabled: true,
                },{
                    label: 'Imagen App',
                    field: 'backendImagenApp',
                    globalSearchDisabled: true,
                },{
                    label: 'Imagen Web',
                    field: 'backendImagenWeb',
                    globalSearchDisabled: true,
                },{
                    label: 'Fecha Desde',
                    field: 'fechaDesde',
                    type: 'date',
                    dateInputFormat: 'DD-MM-YYYY',
                    dateOutputFormat: 'DD-MM-YYYY',
                },{
                    label: 'Fecha Hasta',
                    field: 'fechaHasta',
                    type: 'date',
                    dateInputFormat: 'DD-MM-YYYY',
                    dateOutputFormat: 'DD-MM-YYYY',
                },{
                    label: 'Editar/Eliminar',
                    field: 'botones',
                    globalSearchDisabled: true,
                },
            ]
        },
        methods:{
            getSlides(){
                this.loading = true;
                axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200 && response.data.code === 0){
                            this.slides = response.data.data;
                        }else{
                            this.slides = null;
                        }
                    })
                    .catch(error => {
                        this.slides = null;
                    })
                    .finally(() => this.loading = false)
            },
            renderData(slides) {
                if (slides){
                    slides.map(item => {
                        if (item.backendImagenWeb){
                            let imag_loop = item.backendImagenWeb;
                            item.backendImagenWeb = '<img class="lista_imagen" src="' + this._parseImage(imag_loop) + '" width="130" height="80">';
                        }else{
                            item.backendImagenWeb = '<img class="lista_imagen" src="' + FGO_BACKEND_IMG_DEFAULT + '" width="130" height="80">';
                        }
                        if (item.backendImagenApp){
                            let imag_loop = item.backendImagenApp;
                            item.backendImagenApp = '<img class="lista_imagen" src="' + this._parseImage(imag_loop) + '" width="130" height="80">';
                        }else{
                            item.backendImagenApp = '<img class="lista_imagen" src="' + FGO_BACKEND_IMG_DEFAULT + '" width="130" height="80">';
                        }
                        if (item.publicado == 1){
                            item.publicado = '<i class="far fa-check-square fa-3x" aria-hidden="true"></i>';
                        }
                        if (item.publicado == 0){
                            item.publicado = '<i class="far fa-times-circle fa-3x" aria-hidden="true"></i>';
                        }
                        if (item.publicado == 3){
                            item.publicado = '<i class="far fa-bookmark fa-3x" aria-hidden="true"></i><br><span style="font-size:15px;">'+'default'+'</span>';
                        }

                        item.botones = ' <div class="accion_editar_slide"><a href="<?=$base_url;?>/admin/v2/home/editar/'+item.id+'" class="btn btn-outline-info btn-md"> Editar </a></div>';
                    });
                }
            },
            newSlide(){
                window.location.href = linkAdd;
            },
            openDeleteModal(idSlide) {
                this.idSlide = idSlide;
                Bus.$emit('new', {
                    title: 'Borrar Slide',
                    component: borrarSlide
                });
            },
            _parseImage(img) {
                return img.replace("public://","/fgo/sites/default/files/");
            }
        },
        computed:{
            formattedColumns(){
                if(this.slides){
                    this.renderData(this.slides);
                    return this.columns;
                }
            }
        },
        mounted() {
            this.getSlides();
        }
    });
</script>
