<?php

function fgo_backend_home_agregar_slide_form($form_state) {
  $form_state['redirect'] = false;
  $cta_datos = array();

  // Se invoca al formulario de CTA
  $cta_dto = fgo_backend_api_cta();

  if ($cta_dto['code'] == 0){
    $cta_datos = $cta_dto["data"]->formulario;
  }

  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['#attributes']['autocomplete'] = "off";
  $form['#validate'][] = 'fgo_backend_form_validate';

  $form['grupo_slide'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Info Slide</i></strong>'),
  );

  $form['grupo_slide']['idSlide'] = array(
      '#type' => 'hidden',
      '#default_value' => ''
  );

  $form['grupo_slide']['imagenApp'] = array(
      '#type' => 'managed_file',
      '#name' => 'custom_content_block_image',
      '#title' => t('Imagen App'),
      '#size' => 40,
      '#description' => t("Los archivos deben ser menores que <b>2 MB</b>. <br> Tipos de archivo permitidos: <b>png jpg jpeg</b>."),
      '#upload_location' => 'public://',
      '#theme' => 'preview_img',
      '#upload_validators' => array(
          'file_validate_is_image' => array(),
          'file_validate_extensions' => array('png jpg jpeg'),
          'file_validate_size' => array(2 * 1024 * 1024),
      ),
  );

  $form['grupo_slide']['imagenWeb'] = array(
      '#type' => 'managed_file',
      '#name' => 'custom_content_block_image',
      '#title' => t('Imagen Web'),
      '#size' => 40,
      '#description' => t("Los archivos deben ser menores que <b>2 MB</b>. <br> Tipos de archivo permitidos: <b>png jpg jpeg</b>."),
      '#upload_location' => 'public://',
      '#theme' => 'preview_img',
      '#upload_validators' => array(
          'file_validate_is_image' => array(),
          'file_validate_extensions' => array('png jpg jpeg'),
          'file_validate_size' => array(2 * 1024 * 1024),
      ),
  );

  $form['grupo_slide']['fechaDesde'] = array(
      '#title' => t('Fecha Desde'),
      '#type' => 'date_popup',
      '#datepicker_options' => array(
          'minDate' => 0,
          'maxDate' => '+1y'
      ),
  );

  $form['grupo_slide']['fechaHasta'] = array(
      '#title' => t('Fecha Hasta'),
      '#type' => 'date_popup',
      '#datepicker_options' => array(
          'minDate' => 0,
          'maxDate' => '+2y'
      ),
  );

  // Se une el formulario de CTA con el actual
  array_push($form['grupo_slide'], $cta_datos);


  $form['grupo_slide']['publicado'] = array(
    '#type' => 'checkbox',
    '#title'=> t('Publicado'),
  );

  $form['grupo_slide']['default'] = array(
    '#type' => 'checkbox',
    '#title'=> t('Slide Default'),
  );

  $form['grupo_slide']['guardar'] = array(
    '#type' => 'submit',
    '#id' => 'guardar_slide',
    '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
    '#value' => 'Guardar Slide',
    '#submit' => array('fgo_backend_home_form_submit')
  );

  return $form;
}

function fgo_backend_home_editar_slide_form($form_state, $build) {

  $id = $build["build_info"]["args"][0];
  $slide = new \Fgo\Bo\SlideBo($id);
  $cta_datos = array();

  if (isset($slide->idSlide)){
    $fid_web = !empty($slide->imagenWeb->fid) ? $slide->imagenWeb->fid : 0;
    $fid_app = !empty($slide->imagenApp->fid) ? $slide->imagenApp->fid : 0;
    $cta_seccion = !empty($slide->cta->param1) ? $slide->cta->param1 : 0;
    $cta_direccion_s = !empty($slide->cta->param2) ? $slide->cta->param2 : '';
    $idCta = isset($slide->cta->idCta) ? $slide->cta->idCta : NULL;

    // Se invoca al formulario de CTA
    $cta_dto = fgo_backend_api_cta($cta_seccion, $cta_direccion_s, $idCta);

    if ($cta_dto['code'] == 0){
      $cta_datos = $cta_dto["data"]->formulario;
    }

    $fecha_desde = !empty($slide->fechaDesde) ? date('Y-m-d', $slide->fechaDesde) : '';
    $fecha_hasta = !empty($slide->fechaHasta) ? date('Y-m-d', $slide->fechaHasta) : '';
    $publicado = !empty($slide->publicado) ? $slide->publicado : 0;
  }

  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['#attributes']['autocomplete'] = "off";
  $form['#validate'][] = 'fgo_backend_form_validate';
  $form['grupo_slide'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Info Slide</i></strong>'),
  );

  $form['grupo_slide']['idSlide'] = array(
      '#type' => 'hidden',
      '#default_value' => isset($slide->idSlide) ? $slide->idSlide : ''
  );

  $form['grupo_slide']['imagenApp'] = array(
      '#type' => 'managed_file',
      '#name' => 'custom_content_block_image',
      '#title' => t('Imagen App'),
      '#size' => 40,
      '#default_value' => $fid_app,
      '#description' => t("Los archivos deben ser menores que <b>2 MB</b>. <br> Tipos de archivo permitidos: <b>png jpg jpeg</b>."),
      '#upload_location' => 'public://',
      '#theme' => 'preview_img',
      '#upload_validators' => array(
          'file_validate_is_image' => array(),
          'file_validate_extensions' => array('png jpg jpeg'),
          'file_validate_size' => array(2 * 1024 * 1024),
      ),
  );

  $form['grupo_slide']['imagenWeb'] = array(
      '#type' => 'managed_file',
      '#name' => 'custom_content_block_image',
      '#title' => t('Imagen Web'),
      '#size' => 40,
      '#default_value' => $fid_web,
      '#description' => t("Los archivos deben ser menores que <b>2 MB</b>. <br> Tipos de archivo permitidos: <b>png jpg jpeg</b>."),
      '#upload_location' => 'public://',
      '#theme' => 'preview_img',
      '#upload_validators' => array(
          'file_validate_is_image' => array(),
          'file_validate_extensions' => array('png jpg jpeg'),
          'file_validate_size' => array(2 * 1024 * 1024),
      ),
  );

  $form['grupo_slide']['fechaDesde'] = array(
      '#title' => t('Fecha Desde'),
      '#type' => 'date_popup',
      '#datepicker_options' => array(
          'minDate' => 0,
          'maxDate' => '+1y'
      ),
      '#default_value' => $fecha_desde,

  );

  $form['grupo_slide']['fechaHasta'] = array(
      '#title' => t('Fecha Hasta'),
      '#type' => 'date_popup',
      '#datepicker_options' => array(
          'minDate' => 0,
          'maxDate' => '+2y'
      ),
      '#default_value' => $fecha_hasta,
  );


  // Se une el formulario de CTA con el actual
  array_push($form['grupo_slide'], $cta_datos);

  $form['grupo_slide']['publicado'] = array(
      '#type' =>'checkbox',
      '#title'=>t('Cargar Slide'),
      '#default_value' => $publicado
  );

  $form['grupo_slide']['default'] = array(
      '#type' => 'checkbox',
      '#title' => t('Slide Default'),
      '#default_value' => $publicado == 3 ? 1 : 0
  );

  $form['grupo_slide']['guardar'] = array(
      '#type' => 'submit',
      '#id' => 'guardar_slide',
      '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
      '#value' => 'Guardar Slide',
      '#submit' => array('fgo_backend_home_form_submit')
  );

  return $form;
}

function fgo_backend_form_validate(&$form, &$form_state){
  $slide = new \Fgo\Bo\SlideBo($form_state['values']['idSlide']);

  $slide->imagenWeb = $form_state['values']['imagenWeb'];
  $slide->imagenApp = $form_state['values']['imagenApp'];
  $slide->fechaDesde = $form_state['values']['fechaDesde'];
  $slide->fechaHasta = $form_state['values']['fechaHasta'];
  $slide->publicado = $form_state['values']['default'] ? 3 : $form_state['values']['publicado'];

  //ImagenWeb
  if (!empty($slide->imagenWeb)){
    $slide->imagenWeb = fgo_backend_home_guardar_imagen_drupal($slide->imagenWeb, FGO_BACKEND_COD_TIPO_IMAGEN);
    /* Guardado de imagenes dentro de la BD de Drupal */
    if (isset($slide->imagenWeb->datosImagen->fid)){
      $fidWeb = $slide->imagenWeb->datosImagen->fid;
      file_usage_add(file_load($fidWeb), 'fgo_backend_home', 'imagen_web', $fidWeb);
    }
  }

  //ImagenApp
  if (!empty($slide->imagenApp)){
    $slide->imagenApp = fgo_backend_home_guardar_imagen_drupal($slide->imagenApp, FGO_BACKEND_COD_TIPO_APP);
    /* Guardado de imagenes dentro de la BD de Drupal */
    if (isset($slide->imagenApp->datosImagen->fid)) {
      $fidApp = $slide->imagenApp->datosImagen->fid;
      file_usage_add(file_load($fidApp),  'fgo_backend_home', 'imagen_app', $fidApp);
    }
  }

  //fechaDesde
  if (!empty($slide->fechaDesde)){
    $slide->fechaDesde = strtotime($slide->fechaDesde);
  }

  //fechaHasta
  if (!empty($slide->fechaHasta)){
    $slide->fechaHasta = strtotime($slide->fechaHasta);
  }

  // Tiene que tener CTA siempre
  if ($form_state['values']['callToActionSeccion'] == 0){
    form_set_error('callToActionSeccion', "Seleccioná un Deep Linking");
  }

  //callToActionSeccion & callToActionDireccion
  $slide->cta = NULL;
  $cta = new \Fgo\Bo\CtaBo($form_state['values']['idCta']);
  if ($form_state['values']['callToActionSeccion'] != 0) {
    $seccion = $form_state['values']['callToActionSeccion'];
    $cta->param1 = $seccion;

    $direccion = $form_state['values']['callToActionDireccion_'.$seccion];

    if ($seccion == 1 || $seccion == 2 || $seccion == 3){
      if (preg_match("/.+\(([0-9]+)\)$/", trim($direccion), $matches) == 1) {
        $idDireccion = $matches[1];
        $cta->param2 = $idDireccion;
      }
    }else{
      $cta->param2 = $direccion;
    }
  }

  try {
    $cta->guardar();
    $slide->cta = $cta;
    $slide->guardar();
  } catch ( \Fgo\Bo\DatosInvalidosException $e) {
    $errores = $e->getErrores();
    foreach ($errores as $error_campo => $error_descripcion){
      form_set_error($error_campo, $error_descripcion);
    }
  }
}

function fgo_backend_home_form_submit(&$form, &$form_state){
  $form_state['redirect'] = url('../admin/v2/home/lista');
}

/**
 * Implements hook_date_popup_process_alter().
 */
function fgo_backend_home_date_popup_process_alter(&$element, &$form_state, $context) {
  if ($form_state['build_info']['form_id'] == 'fgo_backend_home_editar_slide_form' || $form_state['build_info']['form_id'] == 'fgo_backend_home_agregar_slide_form') {
    $element['time']['#value'] = '00:00';
    $element['time']['#default_value'] = '00:00';
    $element['time']['#type'] = 'hidden';
    unset($element['date']['#title']);
  }
}

function fgo_backend_home_accesos_directos_form($form_state, $build){
  $_default = array(
      'idAccesoDirecto' => '',
      'nombre' => '',
      'tipo' => '',
      'visible' => false,
      'orden' => 0,
  
  );
   
  if( !empty( $build['build_info']['args'] ) ) { 
    $acceso_directoBo = $build['build_info']['args'][0];
    $_default['idAccesoDirecto'] = $acceso_directoBo->idAccesoDirecto;
    $_default['nombre'] = $acceso_directoBo->nombre;
    $_default['tipo'] = $acceso_directoBo->tipo;
    $_default['visible'] = $acceso_directoBo->visible;
  }
  
  
  $form_state['redirect'] = false;
  $cta_datos = array();

   $cta_seccion = !empty($acceso_directoBo->cta->param1) ? $acceso_directoBo->cta->param1 : 0;
    $cta_direccion_s = !empty($acceso_directoBo->cta->param2) ? $acceso_directoBo->cta->param2 : '';
    $idCta = isset($acceso_directoBo->cta->idCta) ? $acceso_directoBo->cta->idCta : NULL;

    // Se invoca al formulario de CTA
    $cta_dto = fgo_backend_api_cta($cta_seccion, $cta_direccion_s, $idCta);

  if ($cta_dto['code'] == 0){
    $cta_datos = $cta_dto["data"]->formulario;
  }

  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['#attributes']['autocomplete'] = "off";
  $form['#validate'][] = 'fgo_backend_home_accesos_directos_form_validate';

  $form['grupo_acceso_directo'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Acceso Directo</i></strong>'),
  );

  $form['grupo_acceso_directo']['idAccesoDirecto'] = array(
      '#type' => 'hidden',
      '#default_value' => $_default['idAccesoDirecto']
  );

  $form['grupo_acceso_directo']['nombre'] = array(
    '#title' => t('Nombre'),
    '#type' => 'textfield',
    '#default_value' => $_default['nombre']
  );
  
  $form['grupo_acceso_directo']['tipo'] = array(
    '#title' => t('Tipo '),
     '#attributes' => array('placeholder' => t('pagos_nfc, market_place, latam_pass, solicita_tu_tarjeta, pagos_qr, p2p, sorteos ...')),
      '#type' => 'textfield',
    '#default_value' => $_default['tipo']
  );

  $form['grupo_acceso_directo']['visible'] = array(
    '#type' =>'checkbox',
    '#title'=>t('Visible'),
    '#default_value' =>  $_default['visible']
  );
  // Se une el formulario de CTA con el actual
  if( isset( $cta_datos['grupo_deeplinking'] ) ) {
    $form['grupo_deeplinking'] = $cta_datos['grupo_deeplinking'];
  }

  $form['guardar'] = array(
    '#type' => 'submit',
    '#id' => 'guardar_slide',
    '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
    '#value' => 'Guardar acceso directo',
    '#submit' => array('fgo_backend_home_accesos_directos_form_submit')
  );

  return $form;
}

function fgo_backend_home_accesos_directos_form_submit(&$form, &$form_state){
  $form_state['redirect'] = url('../admin/v2/home/accesos_directos');
}

function fgo_backend_home_accesos_directos_form_validate(&$form, &$form_state)
{
  $acceso_directoBo = new Fgo\Bo\AccesoDirectoBo( $form_state['values']['idAccesoDirecto'] );
  $acceso_directoBo->nombre = $form_state['values']['nombre'];
  $acceso_directoBo->tipo = $form_state['values']['tipo'];
  $acceso_directoBo->visible = $form_state['values']['visible'];
  $acceso_directoBo->orden = 0;
  
  //callToActionSeccion & callToActionDireccion
  $acceso_directoBo->cta = NULL;
  $cta = new \Fgo\Bo\CtaBo($form_state['values']['idCta']);
  if ($form_state['values']['callToActionSeccion'] != 0) {
    $seccion = $form_state['values']['callToActionSeccion'];
    $cta->param1 = $seccion;

    $direccion = $form_state['values']['callToActionDireccion_'.$seccion];

    if ($seccion == 1 || $seccion == 2 || $seccion == 3){
      if (preg_match("/.+\(([0-9]+)\)$/", trim($direccion), $matches) == 1) {
        $idDireccion = $matches[1];
        $cta->param2 = $idDireccion;
      }
    }else{
      $cta->param2 = $direccion;
    }
  }

  try {
    $cta->guardar();
    $acceso_directoBo->cta = $cta;
    $acceso_directoBo->guardar();
  } catch ( \Fgo\Bo\DatosInvalidosException $e) {
    $errores = $e->getErrores();
    foreach ($errores as $error_campo => $error_descripcion){
      form_set_error($error_campo, $error_descripcion);
    }
  }
}