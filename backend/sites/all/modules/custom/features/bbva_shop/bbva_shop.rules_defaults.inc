<?php
/**
 * @file
 * bbva_shop.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function bbva_shop_default_rules_configuration() {
  $items = array();
  $items['rules_publicacion_promocion_shop'] = entity_import('rules_config', '{ "rules_publicacion_promocion_shop" : {
      "LABEL" : "Publicacion Shop (promocion)",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "eck" ],
      "ON" : { "shop_presave" : [] },
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "2" : "2", "3" : "3", "7" : "7" } }
          }
        },
        { "entity_has_field" : { "entity" : [ "shop" ], "field" : "field_published" } },
        { "data_is" : { "data" : [ "shop:field-published" ], "value" : "1" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "shop:field-published" ], "value" : "0" } },
        { "drupal_message" : {
            "message" : "No tiene permisos para publicar este contenido.",
            "type" : "warning"
          }
        }
      ]
    }
  }');
  return $items;
}
