<?php
/**
 * @file
 * bbva_shop.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function bbva_shop_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'shop|shop|carusel3';
  $ds_layout->entity_type = 'shop';
  $ds_layout->bundle = 'shop';
  $ds_layout->view_mode = 'carusel3';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_shiop_mark_tip',
        1 => 'title',
        2 => 'field_shop_imagen',
      ),
    ),
    'fields' => array(
      'field_shiop_mark_tip' => 'ds_content',
      'title' => 'ds_content',
      'field_shop_imagen' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['shop|shop|carusel3'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'shop|shop|default';
  $ds_layout->entity_type = 'shop';
  $ds_layout->bundle = 'shop';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_shop_id',
        1 => 'field_shop_email',
        2 => 'field_shop_web_site',
        3 => 'field_shop_taxpayer',
        4 => 'field_shop_category',
        5 => 'field_shop_imagen',
        6 => 'field_shiop_mark_tip',
      ),
    ),
    'fields' => array(
      'field_shop_id' => 'ds_content',
      'field_shop_email' => 'ds_content',
      'field_shop_web_site' => 'ds_content',
      'field_shop_taxpayer' => 'ds_content',
      'field_shop_category' => 'ds_content',
      'field_shop_imagen' => 'ds_content',
      'field_shiop_mark_tip' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['shop|shop|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'shop|shop|teaser';
  $ds_layout->entity_type = 'shop';
  $ds_layout->bundle = 'shop';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_shop_imagen',
      ),
    ),
    'fields' => array(
      'field_shop_imagen' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['shop|shop|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'shop|shop|token';
  $ds_layout->entity_type = 'shop';
  $ds_layout->bundle = 'shop';
  $ds_layout->view_mode = 'token';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_shiop_mark_tip',
      ),
    ),
    'fields' => array(
      'field_shiop_mark_tip' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['shop|shop|token'] = $ds_layout;

  return $export;
}
