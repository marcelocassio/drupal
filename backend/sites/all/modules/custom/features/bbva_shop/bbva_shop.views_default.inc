<?php
/**
 * @file
 * bbva_shop.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bbva_shop_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'shop_backend';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_shop';
  $view->human_name = 'Shop Backend';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Shop Backend';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    5 => '5',
    7 => '7',
    8 => '8',
    6 => '6',
    18 => '18',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Todos -';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« primera';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ anterior';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'siguiente ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'última »';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_shop_id' => 'field_shop_id',
    'title' => 'title',
    'edit_link' => 'edit_link',
    'field_shop_imagen' => 'field_shop_imagen',
    'field_shiop_mark_tip' => 'field_shiop_mark_tip',
    'field_benefit_identify' => 'field_benefit_identify',
    'views_bulk_operations' => 'views_bulk_operations',
    'title_1' => 'title_1',
    'field_taxpayer_cuit' => 'title_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_shop_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_link' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_shop_imagen' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_shiop_mark_tip' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_benefit_identify' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => 'views-align-center',
      'separator' => ' / ',
      'empty_column' => 0,
    ),
    'field_taxpayer_cuit' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_bsb_shop_benefit_shop_branch']['id'] = 'reverse_field_bsb_shop_benefit_shop_branch';
  $handler->display->display_options['relationships']['reverse_field_bsb_shop_benefit_shop_branch']['table'] = 'eck_shop';
  $handler->display->display_options['relationships']['reverse_field_bsb_shop_benefit_shop_branch']['field'] = 'reverse_field_bsb_shop_benefit_shop_branch';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_benefit_shop_branchs_benefit']['id'] = 'reverse_field_benefit_shop_branchs_benefit';
  $handler->display->display_options['relationships']['reverse_field_benefit_shop_branchs_benefit']['table'] = 'eck_benefit_shop_branch';
  $handler->display->display_options['relationships']['reverse_field_benefit_shop_branchs_benefit']['field'] = 'reverse_field_benefit_shop_branchs_benefit';
  $handler->display->display_options['relationships']['reverse_field_benefit_shop_branchs_benefit']['relationship'] = 'reverse_field_bsb_shop_benefit_shop_branch';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_shop_taxpayer_target_id']['id'] = 'field_shop_taxpayer_target_id';
  $handler->display->display_options['relationships']['field_shop_taxpayer_target_id']['table'] = 'field_data_field_shop_taxpayer';
  $handler->display->display_options['relationships']['field_shop_taxpayer_target_id']['field'] = 'field_shop_taxpayer_target_id';
  /* Campo: Shop: id */
  $handler->display->display_options['fields']['field_shop_id']['id'] = 'field_shop_id';
  $handler->display->display_options['fields']['field_shop_id']['table'] = 'field_data_field_shop_id';
  $handler->display->display_options['fields']['field_shop_id']['field'] = 'field_shop_id';
  $handler->display->display_options['fields']['field_shop_id']['label'] = 'ID shop';
  /* Campo: Shop: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_shop';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Título';
  /* Campo: Shop: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'eck_shop';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['label'] = '';
  $handler->display->display_options['fields']['edit_link']['text'] = 'Editar';
  /* Campo: Shop: Imagen */
  $handler->display->display_options['fields']['field_shop_imagen']['id'] = 'field_shop_imagen';
  $handler->display->display_options['fields']['field_shop_imagen']['table'] = 'field_data_field_shop_imagen';
  $handler->display->display_options['fields']['field_shop_imagen']['field'] = 'field_shop_imagen';
  $handler->display->display_options['fields']['field_shop_imagen']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_shop_imagen']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Campo: Shop: Mark Tip */
  $handler->display->display_options['fields']['field_shiop_mark_tip']['id'] = 'field_shiop_mark_tip';
  $handler->display->display_options['fields']['field_shiop_mark_tip']['table'] = 'field_data_field_shiop_mark_tip';
  $handler->display->display_options['fields']['field_shiop_mark_tip']['field'] = 'field_shiop_mark_tip';
  $handler->display->display_options['fields']['field_shiop_mark_tip']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_shiop_mark_tip']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Campo: Benefit: Identify */
  $handler->display->display_options['fields']['field_benefit_identify']['id'] = 'field_benefit_identify';
  $handler->display->display_options['fields']['field_benefit_identify']['table'] = 'field_data_field_benefit_identify';
  $handler->display->display_options['fields']['field_benefit_identify']['field'] = 'field_benefit_identify';
  $handler->display->display_options['fields']['field_benefit_identify']['relationship'] = 'reverse_field_benefit_shop_branchs_benefit';
  $handler->display->display_options['fields']['field_benefit_identify']['label'] = 'Id Beneficio';
  $handler->display->display_options['fields']['field_benefit_identify']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Campo: Operaciones en masa: Shop */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'eck_shop';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_delete_revision' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          'shop::field_shiop_mark_tip' => 'shop::field_shiop_mark_tip',
          'shop::field_shop_imagen' => 'shop::field_shop_imagen',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
  );
  /* Campo: Taxpayer: Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'eck_taxpayer';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'field_shop_taxpayer_target_id';
  $handler->display->display_options['fields']['title_1']['label'] = 'Datos Contribuyente';
  /* Campo: Taxpayer: CUIT */
  $handler->display->display_options['fields']['field_taxpayer_cuit']['id'] = 'field_taxpayer_cuit';
  $handler->display->display_options['fields']['field_taxpayer_cuit']['table'] = 'field_data_field_taxpayer_cuit';
  $handler->display->display_options['fields']['field_taxpayer_cuit']['field'] = 'field_taxpayer_cuit';
  $handler->display->display_options['fields']['field_taxpayer_cuit']['relationship'] = 'field_shop_taxpayer_target_id';
  /* Campo: Shop: Category */
  $handler->display->display_options['fields']['field_shop_category']['id'] = 'field_shop_category';
  $handler->display->display_options['fields']['field_shop_category']['table'] = 'field_data_field_shop_category';
  $handler->display->display_options['fields']['field_shop_category']['field'] = 'field_shop_category';
  $handler->display->display_options['fields']['field_shop_category']['label'] = 'Categoría';
  $handler->display->display_options['fields']['field_shop_category']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_shop_category']['delta_offset'] = '0';
  /* Filter criterion: Shop: shop type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_shop';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'shop' => 'shop',
  );
  /* Filter criterion: Shop: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'eck_shop';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Título';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Shop: id (field_shop_id) */
  $handler->display->display_options['filters']['field_shop_id_value']['id'] = 'field_shop_id_value';
  $handler->display->display_options['filters']['field_shop_id_value']['table'] = 'field_data_field_shop_id';
  $handler->display->display_options['filters']['field_shop_id_value']['field'] = 'field_shop_id_value';
  $handler->display->display_options['filters']['field_shop_id_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_shop_id_value']['expose']['operator_id'] = 'field_shop_id_value_op';
  $handler->display->display_options['filters']['field_shop_id_value']['expose']['label'] = 'id  Suip';
  $handler->display->display_options['filters']['field_shop_id_value']['expose']['operator'] = 'field_shop_id_value_op';
  $handler->display->display_options['filters']['field_shop_id_value']['expose']['identifier'] = 'field_shop_id_value';
  $handler->display->display_options['filters']['field_shop_id_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    11 => 0,
    12 => 0,
  );
  /* Filter criterion: Benefit: Date End (field_benefit_date_end) */
  $handler->display->display_options['filters']['field_benefit_date_end_value']['id'] = 'field_benefit_date_end_value';
  $handler->display->display_options['filters']['field_benefit_date_end_value']['table'] = 'field_data_field_benefit_date_end';
  $handler->display->display_options['filters']['field_benefit_date_end_value']['field'] = 'field_benefit_date_end_value';
  $handler->display->display_options['filters']['field_benefit_date_end_value']['relationship'] = 'reverse_field_benefit_shop_branchs_benefit';
  $handler->display->display_options['filters']['field_benefit_date_end_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_benefit_date_end_value']['default_date'] = 'now';
  /* Filter criterion: Taxpayer: CUIT (field_taxpayer_cuit) */
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['id'] = 'field_taxpayer_cuit_value';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['table'] = 'field_data_field_taxpayer_cuit';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['field'] = 'field_taxpayer_cuit_value';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['relationship'] = 'field_shop_taxpayer_target_id';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['expose']['operator_id'] = 'field_taxpayer_cuit_value_op';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['expose']['label'] = 'CUIT Contribuyente';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['expose']['operator'] = 'field_taxpayer_cuit_value_op';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['expose']['identifier'] = 'field_taxpayer_cuit_value';
  $handler->display->display_options['filters']['field_taxpayer_cuit_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
  );
  /* Filter criterion: Shop: Category (field_shop_category) */
  $handler->display->display_options['filters']['field_shop_category_tid']['id'] = 'field_shop_category_tid';
  $handler->display->display_options['filters']['field_shop_category_tid']['table'] = 'field_data_field_shop_category';
  $handler->display->display_options['filters']['field_shop_category_tid']['field'] = 'field_shop_category_tid';
  $handler->display->display_options['filters']['field_shop_category_tid']['value'] = '';
  $handler->display->display_options['filters']['field_shop_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_shop_category_tid']['expose']['operator_id'] = 'field_shop_category_tid_op';
  $handler->display->display_options['filters']['field_shop_category_tid']['expose']['label'] = 'Rubro/Subrubro';
  $handler->display->display_options['filters']['field_shop_category_tid']['expose']['operator'] = 'field_shop_category_tid_op';
  $handler->display->display_options['filters']['field_shop_category_tid']['expose']['identifier'] = 'field_shop_category_tid';
  $handler->display->display_options['filters']['field_shop_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    17 => 0,
    18 => 0,
  );
  $handler->display->display_options['filters']['field_shop_category_tid']['vocabulary'] = 'category';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/content/shop';
  $translatables['shop_backend'] = array(
    t('Master'),
    t('Shop Backend'),
    t('more'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Ordenar por'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- Todos -'),
    t('Offset'),
    t('« primera'),
    t('‹ anterior'),
    t('siguiente ›'),
    t('última »'),
    t('Benefit Shop Branch referencing Shop from field_bsb_shop'),
    t('Benefit referencing Benefit Shop Branch from field_benefit_shop_branchs'),
    t('Taxpayer entity referenced from field_shop_taxpayer'),
    t('ID shop'),
    t('Título'),
    t('Editar'),
    t('Imagen'),
    t('Mark Tip'),
    t('Id Beneficio'),
    t('Shop'),
    t('- Elegir una operación -'),
    t('Datos Contribuyente'),
    t('CUIT'),
    t('Categoría'),
    t('id  Suip'),
    t('CUIT Contribuyente'),
    t('Rubro/Subrubro'),
    t('Page'),
  );
  $export['shop_backend'] = $view;

  $view = new view();
  $view->name = 'shops';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_shop';
  $view->human_name = 'Shops';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Shops';
  $handler->display->display_options['css_class'] = 'tb-obelisk-views-listing';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'jcarousel';
  $handler->display->display_options['style_options']['wrap'] = 'circular';
  $handler->display->display_options['style_options']['visible'] = '';
  $handler->display->display_options['style_options']['scroll'] = '1';
  $handler->display->display_options['style_options']['auto'] = '0';
  $handler->display->display_options['style_options']['autoPause'] = 1;
  $handler->display->display_options['style_options']['easing'] = '';
  $handler->display->display_options['style_options']['vertical'] = 0;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Campo: Shop: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_shop';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Campo: Shop: Imagen */
  $handler->display->display_options['fields']['field_shop_imagen']['id'] = 'field_shop_imagen';
  $handler->display->display_options['fields']['field_shop_imagen']['table'] = 'field_data_field_shop_imagen';
  $handler->display->display_options['fields']['field_shop_imagen']['field'] = 'field_shop_imagen';
  $handler->display->display_options['fields']['field_shop_imagen']['label'] = '';
  $handler->display->display_options['fields']['field_shop_imagen']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_shop_imagen']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_shop_imagen']['settings'] = array(
    'image_style' => 'img__180x270_',
    'image_link' => '',
  );
  /* Filter criterion: Shop: shop type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_shop';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'shop' => 'shop',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['path'] = 'shops';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $translatables['shops'] = array(
    t('Master'),
    t('Shops'),
    t('more'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Ordenar por'),
    t('Asc'),
    t('Desc'),
    t('Page'),
    t('Block'),
  );
  $export['shops'] = $view;

  return $export;
}
