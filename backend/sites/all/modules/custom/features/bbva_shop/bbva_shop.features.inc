<?php
/**
 * @file
 * bbva_shop.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_shop_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bbva_shop_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function bbva_shop_eck_bundle_info() {
  $items = array(
    'shop_shop' => array(
      'machine_name' => 'shop_shop',
      'entity_type' => 'shop',
      'name' => 'shop',
      'label' => 'Shop',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function bbva_shop_eck_entity_type_info() {
  $items = array(
    'shop' => array(
      'name' => 'shop',
      'label' => 'Shop',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'language' => array(
          'label' => 'Entity language',
          'type' => 'language',
          'behavior' => 'language',
        ),
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
  );
  return $items;
}
