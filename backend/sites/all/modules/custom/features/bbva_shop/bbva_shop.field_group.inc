<?php
/**
 * @file
 * bbva_shop.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_shop_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_shop_default|shop|shop|form';
  $field_group->group_name = 'group_shop_default';
  $field_group->entity_type = 'shop';
  $field_group->bundle = 'shop';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Datos',
    'weight' => '0',
    'children' => array(
      0 => 'group_shop_general',
      1 => 'group_shop_otros',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-shop-default field-group-htabs',
      ),
    ),
  );
  $export['group_shop_default|shop|shop|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_shop_general|shop|shop|form';
  $field_group->group_name = 'group_shop_general';
  $field_group->entity_type = 'shop';
  $field_group->bundle = 'shop';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_shop_default';
  $field_group->data = array(
    'label' => 'General',
    'weight' => '13',
    'children' => array(
      0 => 'field_shiop_mark_tip',
      1 => 'field_shop_imagen',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-shop-general field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_shop_general|shop|shop|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_shop_otros|shop|shop|form';
  $field_group->group_name = 'group_shop_otros';
  $field_group->entity_type = 'shop';
  $field_group->bundle = 'shop';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_shop_default';
  $field_group->data = array(
    'label' => 'Otros',
    'weight' => '14',
    'children' => array(
      0 => 'field_shop_category',
      1 => 'field_shop_email',
      2 => 'field_shop_id',
      3 => 'field_shop_taxpayer',
      4 => 'field_shop_web_site',
      5 => 'field_published',
      6 => 'field_tags',
      7 => 'metatags',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-shop-otros field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_shop_otros|shop|shop|form'] = $field_group;

  return $export;
}
