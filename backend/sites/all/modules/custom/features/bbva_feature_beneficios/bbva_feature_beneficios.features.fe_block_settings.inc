<?php
/**
 * @file
 * bbva_feature_beneficios.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function bbva_feature_beneficios_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-bloque_benefit_sorteos-block'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'bloque_benefit_sorteos-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'boilerplate' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'boilerplate',
        'weight' => 0,
      ),
      'ember' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ember',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
