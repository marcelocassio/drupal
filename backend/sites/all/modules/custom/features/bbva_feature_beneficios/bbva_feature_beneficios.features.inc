<?php
/**
 * @file
 * bbva_feature_beneficios.features.inc
 */

/**
 * Implements hook_views_api().
 */
function bbva_feature_beneficios_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
