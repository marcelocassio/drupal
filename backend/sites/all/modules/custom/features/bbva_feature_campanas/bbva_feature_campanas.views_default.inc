<?php
/**
 * @file
 * bbva_feature_campanas.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bbva_feature_campanas_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'campania_suip';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_campaign';
  $view->human_name = 'campania_suip';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Administracion Campanias';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    5 => '5',
    7 => '7',
    8 => '8',
    6 => '6',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Campo: Campaign: Id */
  $handler->display->display_options['fields']['field_campaign_id']['id'] = 'field_campaign_id';
  $handler->display->display_options['fields']['field_campaign_id']['table'] = 'field_data_field_campaign_id';
  $handler->display->display_options['fields']['field_campaign_id']['field'] = 'field_campaign_id';
  $handler->display->display_options['fields']['field_campaign_id']['label'] = 'Id suip';
  /* Campo: Campaign: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_campaign';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Campo: Campaign: End Date */
  $handler->display->display_options['fields']['field_campaign_end_date']['id'] = 'field_campaign_end_date';
  $handler->display->display_options['fields']['field_campaign_end_date']['table'] = 'field_data_field_campaign_end_date';
  $handler->display->display_options['fields']['field_campaign_end_date']['field'] = 'field_campaign_end_date';
  /* Campo: Campaign: Description */
  $handler->display->display_options['fields']['field_campaign_description']['id'] = 'field_campaign_description';
  $handler->display->display_options['fields']['field_campaign_description']['table'] = 'field_data_field_campaign_description';
  $handler->display->display_options['fields']['field_campaign_description']['field'] = 'field_campaign_description';
  $handler->display->display_options['fields']['field_campaign_description']['label'] = 'Descripción';
  /* Filter criterion: Campaign: campaign type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_campaign';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'campaign' => 'campaign',
  );
  /* Filter criterion: Campaign: End Date (field_campaign_end_date) */
  $handler->display->display_options['filters']['field_campaign_end_date_value']['id'] = 'field_campaign_end_date_value';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['table'] = 'field_data_field_campaign_end_date';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['field'] = 'field_campaign_end_date_value';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['operator'] = '>';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['default_date'] = 'now +2 days';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Campaign: campaign type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_campaign';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'campaign' => 'campaign',
  );
  /* Filter criterion: Campaign: Start Date (field_campaign_start_date) */
  $handler->display->display_options['filters']['field_campaign_start_date_value']['id'] = 'field_campaign_start_date_value';
  $handler->display->display_options['filters']['field_campaign_start_date_value']['table'] = 'field_data_field_campaign_start_date';
  $handler->display->display_options['filters']['field_campaign_start_date_value']['field'] = 'field_campaign_start_date_value';
  $handler->display->display_options['filters']['field_campaign_start_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_campaign_start_date_value']['granularity'] = 'month';
  $handler->display->display_options['filters']['field_campaign_start_date_value']['default_date'] = '-8 month';
  /* Filter criterion: Campaign: Start Date (field_campaign_start_date) */
  $handler->display->display_options['filters']['field_campaign_start_date_value_1']['id'] = 'field_campaign_start_date_value_1';
  $handler->display->display_options['filters']['field_campaign_start_date_value_1']['table'] = 'field_data_field_campaign_start_date';
  $handler->display->display_options['filters']['field_campaign_start_date_value_1']['field'] = 'field_campaign_start_date_value';
  $handler->display->display_options['filters']['field_campaign_start_date_value_1']['operator'] = '>=';
  $handler->display->display_options['filters']['field_campaign_start_date_value_1']['default_date'] = 'now -2 days';
  $handler->display->display_options['path'] = 'campania-suip';

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '150';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'field_campaign_id' => 'field_campaign_id',
    'title' => 'title',
    'field_campaign_description' => 'field_campaign_description',
    'field_campaign_end_date' => 0,
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_campaign_id' => 'field_campaign_id',
    'title' => 'title',
    'field_campaign_end_date' => 'field_campaign_end_date',
  );
  $handler->display->display_options['row_options']['separator'] = ',';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Campaign: campaign type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_campaign';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'campaign' => 'campaign',
  );
  /* Filter criterion: Campaign: End Date (field_campaign_end_date) */
  $handler->display->display_options['filters']['field_campaign_end_date_value']['id'] = 'field_campaign_end_date_value';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['table'] = 'field_data_field_campaign_end_date';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['field'] = 'field_campaign_end_date_value';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_campaign_end_date_value']['default_date'] = 'now -2 days';
  $translatables['campania_suip'] = array(
    t('Master'),
    t('Administracion Campanias'),
    t('more'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Ordenar por'),
    t('Asc'),
    t('Desc'),
    t('Id suip'),
    t('End Date'),
    t('Descripción'),
    t('Page'),
    t('Entity Reference'),
  );
  $export['campania_suip'] = $view;

  return $export;
}
