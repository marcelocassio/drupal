<?php
/**
 * @file
 * bbva_feature_campanas.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function bbva_feature_campanas_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'display_cache_campana_campana_default';
  $strongarm->value = array(
    'default' => array(
      'use' => '0',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_tipo_de_evento' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_con_cupon' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_target' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_archivo' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_frecuencia' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_vistas' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_notificacion_local' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_popup' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_barra_inferior' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_estado' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_titulo' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_vigencia' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_coupon_promo_shop' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_coupon_promo_numero' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_coupon_promo_discount' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_coupon_promo_top' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_coupon_promo_legal' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_coupon_vochers_file' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
    'field_campana_orden' => array(
      'override' => '2',
      'page_granularity' => '0',
      'user_granularity' => '0',
      'granularity' => '0',
    ),
  );
  $export['display_cache_campana_campana_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_campana__campana';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '5',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_campana__campana'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uc-progress:bbva_campanias_cumpleanos';
  $strongarm->value = FALSE;
  $export['uc-progress:bbva_campanias_cumpleanos'] = $strongarm;

  return $export;
}
