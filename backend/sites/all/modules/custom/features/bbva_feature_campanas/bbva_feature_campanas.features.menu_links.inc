<?php
/**
 * @file
 * bbva_feature_campanas.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bbva_feature_campanas_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_add-campaign:admin/structure/entity-type/campaign/campaign/add
  $menu_links['management_add-campaign:admin/structure/entity-type/campaign/campaign/add'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/entity-type/campaign/campaign/add',
    'router_path' => 'admin/structure/entity-type/campaign/campaign/add',
    'link_title' => 'Add Campaign',
    'options' => array(
      'attributes' => array(
        'title' => 'Add an entity of type Campaign with bundle Campaign',
      ),
      'identifier' => 'management_add-campaign:admin/structure/entity-type/campaign/campaign/add',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_campaign:admin/structure/entity-type/campaign/campaign',
  );
  // Exported menu link: management_bbva-campaas:admin/config/bbva/bbva_campanias
  $menu_links['management_bbva-campaas:admin/config/bbva/bbva_campanias'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/bbva/bbva_campanias',
    'router_path' => 'admin/config/bbva/bbva_campanias',
    'link_title' => 'BBVA Campañas',
    'options' => array(
      'attributes' => array(
        'title' => 'Configurar Campañas',
      ),
      'identifier' => 'management_bbva-campaas:admin/config/bbva/bbva_campanias',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -3,
    'customized' => 0,
    'parent_identifier' => 'management_bbva-configuraciones:admin/config/bbva',
  );
  // Exported menu link: management_campaign:admin/structure/entity-type/campaign
  $menu_links['management_campaign:admin/structure/entity-type/campaign'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/entity-type/campaign',
    'router_path' => 'admin/structure/entity-type/campaign',
    'link_title' => 'Campaign',
    'options' => array(
      'attributes' => array(
        'title' => 'View all the bundles for \'Campaign\'',
      ),
      'identifier' => 'management_campaign:admin/structure/entity-type/campaign',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_entity-types:admin/structure/entity-type',
  );
  // Exported menu link: management_campaign:admin/structure/entity-type/campaign/campaign
  $menu_links['management_campaign:admin/structure/entity-type/campaign/campaign'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/entity-type/campaign/campaign',
    'router_path' => 'admin/structure/entity-type/campaign/campaign',
    'link_title' => 'Campaign',
    'options' => array(
      'attributes' => array(
        'title' => 'View all entites of type Campaign with bundle Campaign',
      ),
      'identifier' => 'management_campaign:admin/structure/entity-type/campaign/campaign',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_campaign:admin/structure/entity-type/campaign',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add Campaign');
  t('BBVA Campañas');
  t('Campaign');


  return $menu_links;
}
