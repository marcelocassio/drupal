<?php
/**
 * @file
 * bbva_feature_campanas.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function bbva_feature_campanas_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'campana|campana|form';
  $ds_layout->entity_type = 'campana';
  $ds_layout->bundle = 'campana';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_campana_datos_generales',
        1 => 'field_campana_titulo',
        2 => 'group_campana_cupones',
        3 => 'group_campana_destinatarios',
        4 => 'field_campana_tipo_de_evento',
        5 => 'group_campana_comunicacion',
        6 => 'field_campana_vigencia',
        7 => 'field_campana_estado',
        8 => 'field_campana_con_cupon',
        10 => 'field_campana_target',
        12 => 'field_coupon_type',
        13 => 'field_campana_archivo',
        14 => 'field_campana_frecuencia',
        15 => 'field_coupon_vochers_file',
        16 => 'field_coupon_promo_lquantity',
        17 => 'field_campana_vistas',
        18 => 'field_campana_notificacion_local',
        19 => '_add_existing_field',
        20 => 'field_coupon_promo_numero',
        21 => 'field_campana_popup',
        22 => 'field_campana_barra_inferior',
        23 => 'field_coupon_promo_shop',
        24 => 'field_coupon_promo_discount',
        25 => 'field_coupon_promo_top',
        26 => 'field_coupon_promo_legal',
      ),
      'hidden' => array(
        9 => 'metatags',
        11 => 'field_campana_orden',
      ),
    ),
    'fields' => array(
      'group_campana_datos_generales' => 'ds_content',
      'field_campana_titulo' => 'ds_content',
      'group_campana_cupones' => 'ds_content',
      'group_campana_destinatarios' => 'ds_content',
      'field_campana_tipo_de_evento' => 'ds_content',
      'group_campana_comunicacion' => 'ds_content',
      'field_campana_vigencia' => 'ds_content',
      'field_campana_estado' => 'ds_content',
      'field_campana_con_cupon' => 'ds_content',
      'metatags' => 'hidden',
      'field_campana_target' => 'ds_content',
      'field_campana_orden' => 'hidden',
      'field_coupon_type' => 'ds_content',
      'field_campana_archivo' => 'ds_content',
      'field_campana_frecuencia' => 'ds_content',
      'field_coupon_vochers_file' => 'ds_content',
      'field_coupon_promo_lquantity' => 'ds_content',
      'field_campana_vistas' => 'ds_content',
      'field_campana_notificacion_local' => 'ds_content',
      '_add_existing_field' => 'ds_content',
      'field_coupon_promo_numero' => 'ds_content',
      'field_campana_popup' => 'ds_content',
      'field_campana_barra_inferior' => 'ds_content',
      'field_coupon_promo_shop' => 'ds_content',
      'field_coupon_promo_discount' => 'ds_content',
      'field_coupon_promo_top' => 'ds_content',
      'field_coupon_promo_legal' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['campana|campana|form'] = $ds_layout;

  return $export;
}
