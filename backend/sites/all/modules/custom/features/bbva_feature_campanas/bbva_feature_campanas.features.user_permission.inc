<?php
/**
 * @file
 * bbva_feature_campanas.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bbva_feature_campanas_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'permisos_campanias'.
  $permissions['permisos_campanias'] = array(
    'name' => 'permisos_campanias',
    'roles' => array(),
    'module' => 'bbva_campanias',
  );

  return $permissions;
}
