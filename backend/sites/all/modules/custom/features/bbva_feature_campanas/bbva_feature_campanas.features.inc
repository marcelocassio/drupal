<?php
/**
 * @file
 * bbva_feature_campanas.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_feature_campanas_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bbva_feature_campanas_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function bbva_feature_campanas_eck_bundle_info() {
  $items = array(
    'campana_campana' => array(
      'machine_name' => 'campana_campana',
      'entity_type' => 'campana',
      'name' => 'campana',
      'label' => 'Campaña',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function bbva_feature_campanas_eck_entity_type_info() {
  $items = array(
    'campana' => array(
      'name' => 'campana',
      'label' => 'Campaña',
      'properties' => array(
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
      ),
    ),
  );
  return $items;
}
