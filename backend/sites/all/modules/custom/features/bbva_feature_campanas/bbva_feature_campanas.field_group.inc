<?php
/**
 * @file
 * bbva_feature_campanas.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_feature_campanas_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campana_comunicacion|campana|campana|form';
  $field_group->group_name = 'group_campana_comunicacion';
  $field_group->entity_type = 'campana';
  $field_group->bundle = 'campana';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Comunicación',
    'weight' => '3',
    'children' => array(
      0 => 'field_campana_barra_inferior',
      1 => 'field_campana_frecuencia',
      2 => 'field_campana_notificacion_local',
      3 => 'field_campana_popup',
      4 => 'field_campana_vistas',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-campana-comunicacion field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_campana_comunicacion|campana|campana|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campana_cupones|campana|campana|form';
  $field_group->group_name = 'group_campana_cupones';
  $field_group->entity_type = 'campana';
  $field_group->bundle = 'campana';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Cupón vinculado',
    'weight' => '1',
    'children' => array(
      0 => 'field_coupon_promo_discount',
      1 => 'field_coupon_promo_legal',
      2 => 'field_coupon_promo_numero',
      3 => 'field_coupon_promo_shop',
      4 => 'field_coupon_promo_top',
      5 => 'field_coupon_vochers_file',
      6 => 'field_coupon_promo_lquantity',
      7 => 'field_coupon_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-campana-cupones field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_campana_cupones|campana|campana|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campana_datos_generales|campana|campana|form';
  $field_group->group_name = 'group_campana_datos_generales';
  $field_group->entity_type = 'campana';
  $field_group->bundle = 'campana';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Datos generales',
    'weight' => '0',
    'children' => array(
      0 => 'field_campana_con_cupon',
      1 => 'field_campana_tipo_de_evento',
      2 => 'field_campana_titulo',
      3 => 'field_campana_vigencia',
      4 => 'field_campana_inicio_de_oferta',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-campana-datos-generales field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_campana_datos_generales|campana|campana|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campana_destinatarios|campana|campana|form';
  $field_group->group_name = 'group_campana_destinatarios';
  $field_group->entity_type = 'campana';
  $field_group->bundle = 'campana';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Destinatarios',
    'weight' => '2',
    'children' => array(
      0 => 'field_campana_archivo',
      1 => 'field_campana_target',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-campana-destinatarios field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_campana_destinatarios|campana|campana|form'] = $field_group;

  return $export;
}
