<?php
/**
 * @file
 * bbva_entiity_nevegacion_app.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bbva_entiity_nevegacion_app_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'eck add navegacion_app bundles'.
  $permissions['eck add navegacion_app bundles'] = array(
    'name' => 'eck add navegacion_app bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck add navegacion_app navegacion_app entities'.
  $permissions['eck add navegacion_app navegacion_app entities'] = array(
    'name' => 'eck add navegacion_app navegacion_app entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer navegacion_app bundles'.
  $permissions['eck administer navegacion_app bundles'] = array(
    'name' => 'eck administer navegacion_app bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer navegacion_app navegacion_app entities'.
  $permissions['eck administer navegacion_app navegacion_app entities'] = array(
    'name' => 'eck administer navegacion_app navegacion_app entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete navegacion_app bundles'.
  $permissions['eck delete navegacion_app bundles'] = array(
    'name' => 'eck delete navegacion_app bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete navegacion_app navegacion_app entities'.
  $permissions['eck delete navegacion_app navegacion_app entities'] = array(
    'name' => 'eck delete navegacion_app navegacion_app entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit navegacion_app bundles'.
  $permissions['eck edit navegacion_app bundles'] = array(
    'name' => 'eck edit navegacion_app bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit navegacion_app navegacion_app entities'.
  $permissions['eck edit navegacion_app navegacion_app entities'] = array(
    'name' => 'eck edit navegacion_app navegacion_app entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck list navegacion_app bundles'.
  $permissions['eck list navegacion_app bundles'] = array(
    'name' => 'eck list navegacion_app bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck list navegacion_app navegacion_app entities'.
  $permissions['eck list navegacion_app navegacion_app entities'] = array(
    'name' => 'eck list navegacion_app navegacion_app entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck view navegacion_app bundles'.
  $permissions['eck view navegacion_app bundles'] = array(
    'name' => 'eck view navegacion_app bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck view navegacion_app navegacion_app entities'.
  $permissions['eck view navegacion_app navegacion_app entities'] = array(
    'name' => 'eck view navegacion_app navegacion_app entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'manage navegacion_app properties'.
  $permissions['manage navegacion_app properties'] = array(
    'name' => 'manage navegacion_app properties',
    'roles' => array(),
    'module' => 'eck',
  );

  return $permissions;
}
