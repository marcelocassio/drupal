<?php
/**
 * @file
 * bbva_entiity_nevegacion_app.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bbva_entiity_nevegacion_app_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'navegacion_app-navegacion_app-field_nav_code'
  $field_instances['navegacion_app-navegacion_app-field_nav_code'] = array(
    'bundle' => 'navegacion_app',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'navegacion_app',
    'field_name' => 'field_nav_code',
    'label' => 'code',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('code');

  return $field_instances;
}
