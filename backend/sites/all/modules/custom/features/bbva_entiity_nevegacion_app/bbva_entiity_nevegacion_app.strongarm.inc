<?php
/**
 * @file
 * bbva_entiity_nevegacion_app.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function bbva_entiity_nevegacion_app_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_navegacion_app__navegacion_app';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '3',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_navegacion_app__navegacion_app'] = $strongarm;

  return $export;
}
