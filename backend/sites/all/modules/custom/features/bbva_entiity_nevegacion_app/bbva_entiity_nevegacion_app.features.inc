<?php
/**
 * @file
 * bbva_entiity_nevegacion_app.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_entiity_nevegacion_app_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function bbva_entiity_nevegacion_app_eck_bundle_info() {
  $items = array(
    'navegacion_app_navegacion_app' => array(
      'machine_name' => 'navegacion_app_navegacion_app',
      'entity_type' => 'navegacion_app',
      'name' => 'navegacion_app',
      'label' => 'navegacion_app',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function bbva_entiity_nevegacion_app_eck_entity_type_info() {
  $items = array(
    'navegacion_app' => array(
      'name' => 'navegacion_app',
      'label' => 'navegacion_app',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'language' => array(
          'label' => 'Entity language',
          'type' => 'language',
          'behavior' => 'language',
        ),
      ),
    ),
  );
  return $items;
}
