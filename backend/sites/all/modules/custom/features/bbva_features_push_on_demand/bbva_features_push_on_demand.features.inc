<?php
/**
 * @file
 * bbva_features_push_on_demand.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_features_push_on_demand_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bbva_features_push_on_demand_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function bbva_features_push_on_demand_node_info() {
  $items = array(
    'push_on_demand' => array(
      'name' => t('Push on Demand'),
      'base' => 'node_content',
      'description' => t('Son los push/sms que se envían durante el día, teniendo un registro de quién lo envía, limitando 1 por día y 5 por mes'),
      'has_title' => '1',
      'title_label' => t('Título del mensaje (Modo referencia)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
