<?php
/**
 * @file
 * bbva_features_tarjetas.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_features_tarjetas_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bbva_features_tarjetas_node_info() {
  $items = array(
    'bbva_tarjetas' => array(
      'name' => t('BBVA Tarjetas'),
      'base' => 'node_content',
      'description' => t('Carga de Promoción Tarjetas BBVA'),
      'has_title' => '1',
      'title_label' => t('Titulo'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
