<?php
/**
 * @file
 * bbva_features_tarjetas.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bbva_features_tarjetas_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create bbva_tarjetas content'.
  $permissions['create bbva_tarjetas content'] = array(
    'name' => 'create bbva_tarjetas content',
    'roles' => array(
      'Alianzas - Experiencias' => 'Alianzas - Experiencias',
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Alianzas - Promociones' => 'Alianzas - Promociones',
      'Publicidad' => 'Publicidad',
      'Tarjetas' => 'Tarjetas',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bbva_tarjetas content'.
  $permissions['delete any bbva_tarjetas content'] = array(
    'name' => 'delete any bbva_tarjetas content',
    'roles' => array(
      'Alianzas - Experiencias' => 'Alianzas - Experiencias',
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Alianzas - Promociones' => 'Alianzas - Promociones',
      'Publicidad' => 'Publicidad',
      'Tarjetas' => 'Tarjetas',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bbva_tarjetas content'.
  $permissions['delete own bbva_tarjetas content'] = array(
    'name' => 'delete own bbva_tarjetas content',
    'roles' => array(
      'Alianzas - Experiencias' => 'Alianzas - Experiencias',
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Alianzas - Promociones' => 'Alianzas - Promociones',
      'Publicidad' => 'Publicidad',
      'Tarjetas' => 'Tarjetas',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bbva_tarjetas content'.
  $permissions['edit any bbva_tarjetas content'] = array(
    'name' => 'edit any bbva_tarjetas content',
    'roles' => array(
      'Alianzas - Experiencias' => 'Alianzas - Experiencias',
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Alianzas - Promociones' => 'Alianzas - Promociones',
      'Publicidad' => 'Publicidad',
      'Tarjetas' => 'Tarjetas',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bbva_tarjetas content'.
  $permissions['edit own bbva_tarjetas content'] = array(
    'name' => 'edit own bbva_tarjetas content',
    'roles' => array(
      'Alianzas - Experiencias' => 'Alianzas - Experiencias',
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Alianzas - Promociones' => 'Alianzas - Promociones',
      'Publicidad' => 'Publicidad',
      'Tarjetas' => 'Tarjetas',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view any unpublished bbva_tarjetas content'.
  $permissions['view any unpublished bbva_tarjetas content'] = array(
    'name' => 'view any unpublished bbva_tarjetas content',
    'roles' => array(
      'Alianzas - Experiencias' => 'Alianzas - Experiencias',
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Alianzas - Promociones' => 'Alianzas - Promociones',
      'Publicidad' => 'Publicidad',
    ),
    'module' => 'view_unpublished',
  );

  return $permissions;
}
