<?php
/**
 * @file
 * bbva_features_seccion_app.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function bbva_features_seccion_app_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Seas o no cliente BBVA',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '255b460d-3056-4581-a0bf-22c6ba09d9f6',
    'vocabulary_machine_name' => 'taxonomia_seccion_app',
    'field_seccion_app_valor' => array(
      'und' => array(
        0 => array(
          'value' => 'benefitList/rubro=4883',
          'format' => NULL,
          'safe_value' => 'benefitList/rubro=4883',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Home',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '2daaa168-6dea-48ce-ac5a-2f3c9fdd189d',
    'vocabulary_machine_name' => 'taxonomia_seccion_app',
    'field_seccion_app_valor' => array(
      'und' => array(
        0 => array(
          'value' => 'menu/home',
          'format' => NULL,
          'safe_value' => 'menu/home',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Destacados',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '348741d4-d92f-4bde-bda4-cec1c914e543',
    'vocabulary_machine_name' => 'taxonomia_seccion_app',
    'field_seccion_app_valor' => array(
      'und' => array(
        0 => array(
          'value' => 'benefitList/rubro=992',
          'format' => NULL,
          'safe_value' => 'benefitList/rubro=992',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Panel de notificaciones',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '3c6311e4-05bb-4ddc-a1ac-498035eabac2',
    'vocabulary_machine_name' => 'taxonomia_seccion_app',
    'field_seccion_app_valor' => array(
      'und' => array(
        0 => array(
          'value' => 'menu/notificaciones',
          'format' => NULL,
          'safe_value' => 'menu/notificaciones',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Beneficio / Cupón',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '88bb9b60-d8b0-4c4f-a781-ada674b1adf3',
    'vocabulary_machine_name' => 'taxonomia_seccion_app',
    'field_seccion_app_valor' => array(
      'und' => array(
        0 => array(
          'value' => 'beneficioCupon',
          'format' => NULL,
          'safe_value' => 'beneficioCupon',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Descuentos Cercanos',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '9128c104-2ca9-4737-a347-5cd97b8a5a5c',
    'vocabulary_machine_name' => 'taxonomia_seccion_app',
    'field_seccion_app_valor' => array(
      'und' => array(
        0 => array(
          'value' => 'menu/geo',
          'format' => NULL,
          'safe_value' => 'menu/geo',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Todos los descuentos',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b6bb710f-0f2d-484a-b05d-d3cbafcdcb7f',
    'vocabulary_machine_name' => 'taxonomia_seccion_app',
    'field_seccion_app_valor' => array(
      'und' => array(
        0 => array(
          'value' => 'benefitList/rubro=',
          'format' => NULL,
          'safe_value' => 'benefitList/rubro=',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
