<?php
/**
 * @file
 * bbva_features_seccion_app.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bbva_features_seccion_app_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'delete terms in taxonomia_seccion_app'.
  $permissions['delete terms in taxonomia_seccion_app'] = array(
    'name' => 'delete terms in taxonomia_seccion_app',
    'roles' => array(),
    'module' => 'taxonomy_permissions',
  );

  // Exported permission: 'edit terms in taxonomia_seccion_app'.
  $permissions['edit terms in taxonomia_seccion_app'] = array(
    'name' => 'edit terms in taxonomia_seccion_app',
    'roles' => array(),
    'module' => 'taxonomy_permissions',
  );

  // Exported permission: 'view terms in 22'.
  $permissions['view terms in 22'] = array(
    'name' => 'view terms in 22',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'taxonomy_permissions',
  );

  return $permissions;
}
