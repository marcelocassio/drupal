<?php
/**
 * @file
 * bbva_features_seccion_app.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function bbva_features_seccion_app_taxonomy_default_vocabularies() {
  return array(
    'taxonomia_seccion_app' => array(
      'name' => 'Seccion App',
      'machine_name' => 'taxonomia_seccion_app',
      'description' => 'Secciones de la app',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
