<?php
/**
 * @file
 * bbva_features_seccion_app.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_features_seccion_app_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
