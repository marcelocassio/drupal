<?php
/**
 * @file
 * bbva_features_seccion_app.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bbva_features_seccion_app_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-taxonomia_seccion_app-field_seccion_app_valor'
  $field_instances['taxonomy_term-taxonomia_seccion_app-field_seccion_app_valor'] = array(
    'bundle' => 'taxonomia_seccion_app',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_seccion_app_valor',
    'label' => 'Valor',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Valor');

  return $field_instances;
}
