<?php
/**
 * @file
 * bbva_feature_message_plan.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bbva_feature_message_plan_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_call_to_action'
  $field_instances['bbva_message_plan-bbva_message_plan-field_call_to_action'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'field_collection',
        'settings' => array(
          'add' => 'Agregar',
          'delete' => 'Eliminar',
          'description' => TRUE,
          'edit' => 'Editar',
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_view',
        'weight' => 16,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_call_to_action',
    'label' => 'Deep Linking',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_field_message_plan_segment'
  $field_instances['bbva_message_plan-bbva_message_plan-field_field_message_plan_segment'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_field_message_plan_segment',
    'label' => 'Todos',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_attached'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_attached'] = array(
    'bundle' => 'bbva_message_plan',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_attached',
    'label' => 'Documento Adjunto',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'pdf txt',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 'default',
          'image' => 0,
          'video' => 0,
        ),
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 14,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_date_send'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_date_send'] = array(
    'bundle' => 'bbva_message_plan',
    'deleted' => 0,
    'description' => 'Fecha de envío del mensaje.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_date_send',
    'label' => 'Fecha Envio',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_imagen1'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_imagen1'] = array(
    'bundle' => 'bbva_message_plan',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_imagen1',
    'label' => 'Imagen',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'push/imagen',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '200 KB',
      'max_resolution' => '1200x675',
      'min_resolution' => '800x400',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_list_file'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_list_file'] = array(
    'bundle' => 'bbva_message_plan',
    'deleted' => 0,
    'description' => 'Si se desea enviar una notificación a un mundo selecto de usuarios se deberá cargar el archivo con los DNI a cuales se le quiera enviar la notificación.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_list_file',
    'label' => 'Listado',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'txt',
      'max_filesize' => '12 MB',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_long_text'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_long_text'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Es el texto que se visualiza en el panel de notificaciones adentro de la aplicación a continuación del texto corto.
',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_long_text',
    'label' => 'Mensaje push extendido ',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 1,
        'maxlength_js_label' => 'Tamaño máximo: 105. Usados: @count',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_mail_body'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_mail_body'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_mail_body',
    'label' => 'Cuerpo del Mail',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_mail_subject'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_mail_subject'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_mail_subject',
    'label' => 'Asunto del Mail',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_oferta_pa'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_oferta_pa'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 16,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_oferta_pa',
    'label' => 'Oferta Preaprobada',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_por_listado'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_por_listado'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 18,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_por_listado',
    'label' => 'Por Listado',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 18,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_push'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_push'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Es el texto que se previsualizara en la notificación que llega al dispositivo.
',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_push',
    'label' => 'Mensaje push corto',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 1,
        'maxlength_js_label' => 'Tamaño máximo: 160. Usados: @count',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_segment_anony'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_segment_anony'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_segment_anony',
    'label' => 'Anonimos',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_segment_cli'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_segment_cli'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_segment_cli',
    'label' => 'Clientes',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 16,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_segment_sc'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_segment_sc'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_segment_sc',
    'label' => 'Suscriptos',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 15,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_sms'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_sms'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_sms',
    'label' => 'Mensaje SMS',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_state'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_state'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'Pendiente : Indica que el push esta pendiente de envio. <br>
Cancelado: Indica que  se cancelo el envio del push.<br>
Enviado    : Indica que el push ya se envio.<br>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_state',
    'label' => 'Estado',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_tag_omniture'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_tag_omniture'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_tag_omniture',
    'label' => 'Tag Omniture',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_message_plan_tilde_mail'
  $field_instances['bbva_message_plan-bbva_message_plan-field_message_plan_tilde_mail'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_message_plan_tilde_mail',
    'label' => 'Mail',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'bbva_message_plan-bbva_message_plan-field_tilde_sms_push'
  $field_instances['bbva_message_plan-bbva_message_plan-field_tilde_sms_push'] = array(
    'bundle' => 'bbva_message_plan',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bbva_message_plan',
    'field_name' => 'field_tilde_sms_push',
    'label' => 'Mensaje SMS/Push',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 9,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Anonimos');
  t('Asunto del Mail');
  t('Call to Action');
  t('Clientes');
  t('Cuerpo del Mail');
  t('Deep Linking');
  t('Documento Adjunto');
  t('Es el texto que se previsualizara en la notificación que llega al dispositivo.
');
  t('Es el texto que se visualiza en el panel de notificaciones adentro de la aplicación a continuación del texto corto.
');
  t('Estado');
  t('Fecha Envio');
  t('Fecha de envío del mensaje.');
  t('Imagen');
  t('Listado');
  t('Mail');
  t('Mensaje Push');
  t('Mensaje SMS');
  t('Mensaje SMS/Push');
  t('Mensaje push corto');
  t('Mensaje push extendido ');
  t('Oferta Preaprobada');
  t('Pendiente : Indica que el push esta pendiente de envio. <br>
Cancelado: Indica que  se cancelo el envio del push.<br>
Enviado    : Indica que el push ya se envio.<br>');
  t('Por Listado');
  t('Si se desea enviar una notificación a un mundo selecto de usuarios se deberá cargar el archivo con los DNI a cuales se le quiera enviar la notificación.');
  t('Suscriptos');
  t('Tag Omniture');
  t('Texto extendido');
  t('Todos');

  return $field_instances;
}
