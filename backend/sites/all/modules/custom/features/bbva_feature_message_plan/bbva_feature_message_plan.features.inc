<?php
/**
 * @file
 * bbva_feature_message_plan.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_feature_message_plan_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bbva_feature_message_plan_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function bbva_feature_message_plan_eck_bundle_info() {
  $items = array(
    'bbva_message_plan_bbva_message_plan' => array(
      'machine_name' => 'bbva_message_plan_bbva_message_plan',
      'entity_type' => 'bbva_message_plan',
      'name' => 'bbva_message_plan',
      'label' => 'Message Plan',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function bbva_feature_message_plan_eck_entity_type_info() {
  $items = array(
    'bbva_message_plan' => array(
      'name' => 'bbva_message_plan',
      'label' => 'Message Plan',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
  );
  return $items;
}
