<?php
/**
 * @file
 * bbva_feature_message_plan.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_feature_message_plan_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_messa_destinatarios|bbva_message_plan|bbva_message_plan|form';
  $field_group->group_name = 'group_messa_destinatarios';
  $field_group->entity_type = 'bbva_message_plan';
  $field_group->bundle = 'bbva_message_plan';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Destinatarios',
    'weight' => '1',
    'children' => array(
      0 => 'field_message_plan_list_file',
      1 => 'field_message_plan_segment_anony',
      2 => 'field_field_message_plan_segment',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-messa-destinatarios field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_messa_destinatarios|bbva_message_plan|bbva_message_plan|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_message_texto|bbva_message_plan|bbva_message_plan|form';
  $field_group->group_name = 'group_message_texto';
  $field_group->entity_type = 'bbva_message_plan';
  $field_group->bundle = 'bbva_message_plan';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Textos',
    'weight' => '0',
    'children' => array(
      0 => 'field_message_plan_push',
      1 => 'field_call_to_action',
      2 => 'field_message_plan_long_text',
      3 => 'field_message_plan_imagen1',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-message-texto field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_message_texto|bbva_message_plan|bbva_message_plan|form'] = $field_group;

  return $export;
}
