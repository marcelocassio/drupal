<?php
/**
 * @file
 * bbva_feature_message_plan.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function bbva_feature_message_plan_default_rules_configuration() {
  $items = array();
  $items['rules_push_save'] = entity_import('rules_config', '{ "rules_push_save" : {
      "LABEL" : "Push Save\\/Update",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "eck" ],
      "ON" : { "bbva_message_plan_insert" : [], "bbva_message_plan_update" : [] },
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3", "5" : "5", "6" : "6", "7" : "7", "8" : "8" } },
            "subject" : "Se cre\\u00f3\\/edit\\u00f3 una notificaci\\u00f3n push [bbva-message-plan:title] - fecha de envio [bbva-message-plan:field-message-plan-date-send]\\r\\n",
            "message" : "Titulo : [bbva-message-plan:title]\\t\\r\\nMensaje : [bbva-message-plan:field-message-plan-push]\\t\\r\\nEnvio :   [bbva-message-plan:field-message-plan-date-send]\\r\\nDestinatarios : Suscriptos [bbva-message-plan:field-message-plan-segment-sc],\\r\\n                        Anonimos  [bbva-message-plan:field-message-plan-segment-anony]\\t\\r\\n                        Clientes     [bbva-message-plan:field-message-plan-segment-cli]\\r\\n                        Todos        [bbva-message-plan:field-field-message-plan-segment]\\r\\n\\r\\n\\r\\n"
          }
        }
      ]
    }
  }');
  return $items;
}
