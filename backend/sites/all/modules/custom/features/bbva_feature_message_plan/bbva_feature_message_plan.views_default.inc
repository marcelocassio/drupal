<?php
/**
 * @file
 * bbva_feature_message_plan.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bbva_feature_message_plan_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'notificaciones';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_bbva_message_plan';
  $view->human_name = 'Notificaciones';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Notificaciones';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    5 => '5',
    7 => '7',
    8 => '8',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Todos -';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« primera';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ anterior';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'siguiente ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'última »';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'field_message_plan_push' => 'field_message_plan_push',
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_message_plan_push' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Campo: Message Plan: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_bbva_message_plan';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Título';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Campo: Message Plan: Mensaje Push */
  $handler->display->display_options['fields']['field_message_plan_push']['id'] = 'field_message_plan_push';
  $handler->display->display_options['fields']['field_message_plan_push']['table'] = 'field_data_field_message_plan_push';
  $handler->display->display_options['fields']['field_message_plan_push']['field'] = 'field_message_plan_push';
  $handler->display->display_options['fields']['field_message_plan_push']['label'] = 'Texto';
  $handler->display->display_options['fields']['field_message_plan_push']['element_label_colon'] = FALSE;
  /* Campo: Message Plan: Fecha Envio */
  $handler->display->display_options['fields']['field_message_plan_date_send']['id'] = 'field_message_plan_date_send';
  $handler->display->display_options['fields']['field_message_plan_date_send']['table'] = 'field_data_field_message_plan_date_send';
  $handler->display->display_options['fields']['field_message_plan_date_send']['field'] = 'field_message_plan_date_send';
  $handler->display->display_options['fields']['field_message_plan_date_send']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_message_plan_date_send']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Campo: Message Plan: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'eck_bbva_message_plan';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['label'] = 'Operaciones';
  $handler->display->display_options['fields']['edit_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_link']['text'] = 'editar';
  /* Sort criterion: Message Plan: Fecha Envio (field_message_plan_date_send) */
  $handler->display->display_options['sorts']['field_message_plan_date_send_value']['id'] = 'field_message_plan_date_send_value';
  $handler->display->display_options['sorts']['field_message_plan_date_send_value']['table'] = 'field_data_field_message_plan_date_send';
  $handler->display->display_options['sorts']['field_message_plan_date_send_value']['field'] = 'field_message_plan_date_send_value';
  $handler->display->display_options['sorts']['field_message_plan_date_send_value']['order'] = 'DESC';
  /* Filter criterion: Message Plan: bbva_message_plan type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_bbva_message_plan';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'bbva_message_plan' => 'bbva_message_plan',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/notificaciones';
  $translatables['notificaciones'] = array(
    t('Master'),
    t('Notificaciones'),
    t('more'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- Todos -'),
    t('Offset'),
    t('« primera'),
    t('‹ anterior'),
    t('siguiente ›'),
    t('última »'),
    t('Título'),
    t('Texto'),
    t('Fecha Envio'),
    t('Operaciones'),
    t('editar'),
    t('Page'),
  );
  $export['notificaciones'] = $view;

  return $export;
}
