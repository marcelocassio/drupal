<?php
/**
 * @file
 * bbva_feature_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_feature_profile_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_left|user|user|form';
  $field_group->group_name = 'group_profile_left';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile_top';
  $field_group->data = array(
    'label' => 'Group Left',
    'weight' => '6',
    'children' => array(
      0 => 'field_profile_nombre',
      1 => 'field_profile_apellido',
      2 => 'field_profile_sex',
      3 => 'field_profile_day',
      4 => 'field_profile_month',
      5 => 'field_profile_year',
      6 => 'field_profile_email',
      7 => 'field_profile_area',
      8 => 'field_profile_compania',
      9 => 'field_profile_celular',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-profile-left field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_profile_left|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_top|user|user|form';
  $field_group->group_name = 'group_profile_top';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group Top',
    'weight' => '5',
    'children' => array(
      0 => 'field_profile_tipo_cliente',
      1 => 'group_profile_left',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-profile-top field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_profile_top|user|user|form'] = $field_group;

  return $export;
}
