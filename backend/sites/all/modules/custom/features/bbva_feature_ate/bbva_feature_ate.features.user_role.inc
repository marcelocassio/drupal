<?php
/**
 * @file
 * bbva_feature_ate.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function bbva_feature_ate_user_default_roles() {
  $roles = array();

  // Exported role: ATE.
  $roles['ATE'] = array(
    'name' => 'ATE',
    'weight' => 22,
  );

  return $roles;
}
