<?php
/**
 * @file
 * bbva_feature_ate.backup_migrate_exportables.inc
 */

/**
 * Implements hook_exportables_backup_migrate_profiles().
 */
function bbva_feature_ate_exportables_backup_migrate_profiles() {
  $export = array();

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->machine_name = 'archivos';
  $item->name = 'Archivos';
  $item->filename = '[site:name]';
  $item->append_timestamp = TRUE;
  $item->timestamp_format = 'Y-m-d\\TH-i-s';
  $item->filters = array(
    'compression' => 'gzip',
    'notify_success_enable' => 0,
    'notify_success_email' => 'lucas.sebastian.monzon@bbva.com',
    'notify_failure_enable' => 0,
    'notify_failure_email' => 'lucas.sebastian.monzon@bbva.com',
    'utils_site_offline' => 0,
    'utils_site_offline_message' => 'FrancesGo está en mantenimiento en estos momentos. Pronto estaremos de regreso. Gracias por su paciencia.',
    'utils_description' => '',
    'use_cli' => 1,
    'ignore_errors' => 0,
    'sources' => array(
      'db' => array(
        'exclude_tables' => array(),
        'nodata_tables' => array(
          'accesslog' => 'accesslog',
          'cache' => 'cache',
          'cache_admin_menu' => 'cache_admin_menu',
          'cache_block' => 'cache_block',
          'cache_bootstrap' => 'cache_bootstrap',
          'cache_field' => 'cache_field',
          'cache_filter' => 'cache_filter',
          'cache_form' => 'cache_form',
          'cache_image' => 'cache_image',
          'cache_location' => 'cache_location',
          'cache_menu' => 'cache_menu',
          'cache_page' => 'cache_page',
          'cache_path' => 'cache_path',
          'cache_update' => 'cache_update',
          'cache_views' => 'cache_views',
          'cache_views_data' => 'cache_views_data',
          'search_dataset' => 'search_dataset',
          'search_index' => 'search_index',
          'search_total' => 'search_total',
          'sessions' => 'sessions',
          'watchdog' => 'watchdog',
        ),
        'utils_lock_tables' => 0,
      ),
      'files' => array(
        'exclude_filepaths' => 'backup_migrate
styles
css
js
ctools
less',
      ),
      'archive' => array(
        'exclude_filepaths' => 'public://backup_migrate
public://styles
public://css
public://js
public://ctools
public://less
sites/default/settings.php
tmp',
      ),
    ),
  );
  $export['archivos'] = $item;

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->machine_name = 'default';
  $item->name = 'Opciones por defecto';
  $item->filename = '[site:name]';
  $item->append_timestamp = TRUE;
  $item->timestamp_format = 'Y-m-d\\TH-i-s';
  $item->filters = array(
    'compression' => 'gzip',
    'notify_success_enable' => 0,
    'notify_success_email' => 'lucas.sebastian.monzon@bbva.com',
    'notify_failure_enable' => 0,
    'notify_failure_email' => 'lucas.sebastian.monzon@bbva.com',
    'utils_site_offline' => 0,
    'utils_site_offline_message' => 'FrancesGo está en mantenimiento en estos momentos. Pronto estaremos de regreso. Gracias por su paciencia.',
    'utils_description' => '',
    'use_cli' => 0,
    'ignore_errors' => 0,
    'sources' => array(
      'db' => array(
        'exclude_tables' => array(),
        'nodata_tables' => array(
          'accesslog' => 'accesslog',
          'cache' => 'cache',
          'cache_admin_menu' => 'cache_admin_menu',
          'cache_api' => 'cache_api',
          'cache_api_benefit' => 'cache_api_benefit',
          'cache_block' => 'cache_block',
          'cache_bootstrap' => 'cache_bootstrap',
          'cache_display_cache' => 'cache_display_cache',
          'cache_eck' => 'cache_eck',
          'cache_entity_comment' => 'cache_entity_comment',
          'cache_entity_file' => 'cache_entity_file',
          'cache_entity_node' => 'cache_entity_node',
          'cache_entity_taxonomy_term' => 'cache_entity_taxonomy_term',
          'cache_entity_taxonomy_vocabulary' => 'cache_entity_taxonomy_vocabulary',
          'cache_entity_user' => 'cache_entity_user',
          'cache_field' => 'cache_field',
          'cache_filter' => 'cache_filter',
          'cache_form' => 'cache_form',
          'cache_image' => 'cache_image',
          'cache_l10n_update' => 'cache_l10n_update',
          'cache_libraries' => 'cache_libraries',
          'cache_location' => 'cache_location',
          'cache_media_xml' => 'cache_media_xml',
          'cache_menu' => 'cache_menu',
          'cache_metatag' => 'cache_metatag',
          'cache_page' => 'cache_page',
          'cache_path' => 'cache_path',
          'cache_path_breadcrumbs' => 'cache_path_breadcrumbs',
          'cache_rules' => 'cache_rules',
          'cache_session_cache' => 'cache_session_cache',
          'cache_token' => 'cache_token',
          'cache_ultimate_cron' => 'cache_ultimate_cron',
          'cache_update' => 'cache_update',
          'cache_views' => 'cache_views',
          'cache_views_data' => 'cache_views_data',
          'search_dataset' => 'search_dataset',
          'search_index' => 'search_index',
          'search_total' => 'search_total',
          'sessions' => 'sessions',
          'watchdog' => 'watchdog',
        ),
        'utils_lock_tables' => 1,
      ),
      'files' => array(
        'exclude_filepaths' => 'backup_migrate
styles
css
js
ctools
less',
      ),
      'archive' => array(
        'exclude_filepaths' => 'public://backup_migrate
public://styles
public://css
public://js
public://ctools
public://less
sites/default/settings.php
tmp',
      ),
    ),
  );
  $export['default'] = $item;

  return $export;
}

/**
 * Implements hook_exportables_backup_migrate_schedules().
 */
function bbva_feature_ate_exportables_backup_migrate_schedules() {
  $export = array();

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->machine_name = 'back';
  $item->name = 'Back';
  $item->source_id = 'db';
  $item->destination_id = 'server';
  $item->copy_destination_id = '';
  $item->profile_id = 'default';
  $item->keep = 5;
  $item->period = 86400;
  $item->enabled = TRUE;
  $item->cron = 'builtin';
  $item->cron_schedule = '0 4 * * *';
  $export['back'] = $item;

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->machine_name = 'files';
  $item->name = 'Files';
  $item->source_id = 'files';
  $item->destination_id = 'server';
  $item->copy_destination_id = '';
  $item->profile_id = 'archivos';
  $item->keep = 0;
  $item->period = 86400;
  $item->enabled = FALSE;
  $item->cron = 'builtin';
  $item->cron_schedule = '0 4 * * *';
  $export['files'] = $item;

  $item = new stdClass();
  $item->disabled = FALSE; /* Edit this to true to make a default item disabled initially */
  $item->api_version = 1;
  $item->machine_name = 'tarea_programada_sin_nombre';
  $item->name = 'Tarea programada sin nombre';
  $item->source_id = 'db';
  $item->destination_id = 'server';
  $item->copy_destination_id = '';
  $item->profile_id = 'default';
  $item->keep = 0;
  $item->period = 86400;
  $item->enabled = FALSE;
  $item->cron = 'none';
  $item->cron_schedule = '0 4 * * *';
  $export['tarea_programada_sin_nombre'] = $item;

  return $export;
}
