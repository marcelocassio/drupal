<?php
/**
 * @file
 * bbva_feature_ate.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function bbva_feature_ate_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Facebook Share';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'facebook_share';
  $fe_block_boxes->body = '<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FBBVAFrancesArg&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=21&amp;appId=404055236433403" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>';

  $export['facebook_share'] = $fe_block_boxes;

  return $export;
}
