<?php
/**
 * @file
 * bbva_feature_ate.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function bbva_feature_ate_default_rules_configuration() {
  $items = array();
  $items['rules_cupon_promotion_save'] = entity_import('rules_config', '{ "rules_cupon_promotion_save" : {
      "LABEL" : "Cupon Promotion save",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "eck" ],
      "ON" : { "coupon_promo_insert" : [] },
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "3" : "3", "5" : "5", "6" : "6", "7" : "7", "8" : "8" } },
            "subject" : "Se creo un nuevo cupon  [coupon-promo:title]\\r\\n",
            "message" : "Titulo : [coupon-promo:title]\\r\\nComercio : [coupon-promo:field-coupon-promo-shop]\\t\\r\\nDesde : [coupon-promo:field-coupon-promo-date-start]  a\\t[coupon-promo:field-coupon-promo-date-end]\\t\\r\\nDescuento [coupon-promo:field-coupon-promo-discount]\\t\\r\\nTope : [coupon-promo:field-coupon-promo-top]\\t\\r\\nCantidad : [coupon-promo:field-coupon-promo-lquantity]\\t\\r\\nLegal: [coupon-promo:field-coupon-promo-legal]\\t\\r\\nA quien :[coupon-promo:field-coupon-promo-segments-user]\\t\\r\\nArchivo de vouchers[coupon-promo:field-coupon-vochers-file]\\t\\r\\nMensaje : [coupon-promo:field-coupon-promo-sms-push-msg]\\t\\r\\n"
          }
        }
      ]
    }
  }');
  return $items;
}
