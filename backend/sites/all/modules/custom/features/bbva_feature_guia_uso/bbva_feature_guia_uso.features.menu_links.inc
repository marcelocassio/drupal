<?php
/**
 * @file
 * bbva_feature_guia_uso.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bbva_feature_guia_uso_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_guia-uso:node/add/guia-uso
  $menu_links['navigation_guia-uso:node/add/guia-uso'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/guia-uso',
    'router_path' => 'node/add/guia-uso',
    'link_title' => 'Guia Uso',
    'options' => array(
      'identifier' => 'navigation_guia-uso:node/add/guia-uso',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'navigation_agregar-contenido:node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Guia Uso');


  return $menu_links;
}
