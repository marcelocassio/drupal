<?php
/**
 * @file
 * bbva_feature_guia_uso.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bbva_feature_guia_uso_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create guia_uso content'.
  $permissions['create guia_uso content'] = array(
    'name' => 'create guia_uso content',
    'roles' => array(
      'Publicidad' => 'Publicidad',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any guia_uso content'.
  $permissions['delete any guia_uso content'] = array(
    'name' => 'delete any guia_uso content',
    'roles' => array(
      'Publicidad' => 'Publicidad',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own guia_uso content'.
  $permissions['delete own guia_uso content'] = array(
    'name' => 'delete own guia_uso content',
    'roles' => array(
      'Publicidad' => 'Publicidad',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any guia_uso content'.
  $permissions['edit any guia_uso content'] = array(
    'name' => 'edit any guia_uso content',
    'roles' => array(
      'Publicidad' => 'Publicidad',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own guia_uso content'.
  $permissions['edit own guia_uso content'] = array(
    'name' => 'edit own guia_uso content',
    'roles' => array(
      'Publicidad' => 'Publicidad',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view any unpublished guia_uso content'.
  $permissions['view any unpublished guia_uso content'] = array(
    'name' => 'view any unpublished guia_uso content',
    'roles' => array(),
    'module' => 'view_unpublished',
  );

  return $permissions;
}
