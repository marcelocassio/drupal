<?php
/**
 * @file
 * bbva_feature_guia_uso.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function bbva_feature_guia_uso_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_guia_uso|default';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_guia_uso';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_guia_uso_imagen_1',
        1 => 'field_guia_uso_buttons',
        2 => 'field_guia_uso_link',
        3 => 'field_guia_uso_close',
        4 => 'field_guia_uso_order',
      ),
    ),
    'fields' => array(
      'field_guia_uso_imagen_1' => 'ds_content',
      'field_guia_uso_buttons' => 'ds_content',
      'field_guia_uso_link' => 'ds_content',
      'field_guia_uso_close' => 'ds_content',
      'field_guia_uso_order' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['field_collection_item|field_guia_uso|default'] = $ds_layout;

  return $export;
}
