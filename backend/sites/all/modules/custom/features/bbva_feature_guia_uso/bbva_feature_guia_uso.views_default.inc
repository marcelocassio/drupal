<?php
/**
 * @file
 * bbva_feature_guia_uso.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bbva_feature_guia_uso_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'guia_uso';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'guia uso';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'guia uso';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Todos -';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« primera';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ anterior';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'siguiente ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'última »';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Campo: Field collection item: imagen */
  $handler->display->display_options['fields']['field_guia_uso_imagen_1']['id'] = 'field_guia_uso_imagen_1';
  $handler->display->display_options['fields']['field_guia_uso_imagen_1']['table'] = 'field_data_field_guia_uso_imagen_1';
  $handler->display->display_options['fields']['field_guia_uso_imagen_1']['field'] = 'field_guia_uso_imagen_1';
  $handler->display->display_options['fields']['field_guia_uso_imagen_1']['label'] = '';
  $handler->display->display_options['fields']['field_guia_uso_imagen_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_guia_uso_imagen_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_guia_uso_imagen_1']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Campo: Field collection item: order */
  $handler->display->display_options['fields']['field_guia_uso_order']['id'] = 'field_guia_uso_order';
  $handler->display->display_options['fields']['field_guia_uso_order']['table'] = 'field_data_field_guia_uso_order';
  $handler->display->display_options['fields']['field_guia_uso_order']['field'] = 'field_guia_uso_order';
  $handler->display->display_options['fields']['field_guia_uso_order']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Campo: Field collection item: buttons */
  $handler->display->display_options['fields']['field_guia_uso_buttons']['id'] = 'field_guia_uso_buttons';
  $handler->display->display_options['fields']['field_guia_uso_buttons']['table'] = 'field_data_field_guia_uso_buttons';
  $handler->display->display_options['fields']['field_guia_uso_buttons']['field'] = 'field_guia_uso_buttons';
  $handler->display->display_options['fields']['field_guia_uso_buttons']['label'] = '';
  $handler->display->display_options['fields']['field_guia_uso_buttons']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_guia_uso_buttons']['type'] = 'field_collection_fields';
  $handler->display->display_options['fields']['field_guia_uso_buttons']['settings'] = array(
    'view_mode' => 'full',
  );
  $handler->display->display_options['fields']['field_guia_uso_buttons']['delta_offset'] = '0';
  /* Campo: Field collection item: link */
  $handler->display->display_options['fields']['field_guia_uso_link']['id'] = 'field_guia_uso_link';
  $handler->display->display_options['fields']['field_guia_uso_link']['table'] = 'field_data_field_guia_uso_link';
  $handler->display->display_options['fields']['field_guia_uso_link']['field'] = 'field_guia_uso_link';
  $handler->display->display_options['fields']['field_guia_uso_link']['label'] = '';
  $handler->display->display_options['fields']['field_guia_uso_link']['element_label_colon'] = FALSE;
  /* Campo: Field collection item: Se Puede cerrar */
  $handler->display->display_options['fields']['field_guia_uso_close']['id'] = 'field_guia_uso_close';
  $handler->display->display_options['fields']['field_guia_uso_close']['table'] = 'field_data_field_guia_uso_close';
  $handler->display->display_options['fields']['field_guia_uso_close']['field'] = 'field_guia_uso_close';
  $handler->display->display_options['fields']['field_guia_uso_close']['label'] = '';
  $handler->display->display_options['fields']['field_guia_uso_close']['element_label_colon'] = FALSE;
  /* Filter criterion: Field collection item: imagen (field_guia_uso_imagen_1:fid) */
  $handler->display->display_options['filters']['field_guia_uso_imagen_1_fid']['id'] = 'field_guia_uso_imagen_1_fid';
  $handler->display->display_options['filters']['field_guia_uso_imagen_1_fid']['table'] = 'field_data_field_guia_uso_imagen_1';
  $handler->display->display_options['filters']['field_guia_uso_imagen_1_fid']['field'] = 'field_guia_uso_imagen_1_fid';
  $handler->display->display_options['filters']['field_guia_uso_imagen_1_fid']['operator'] = 'not empty';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'guia-uso';
  $translatables['guia_uso'] = array(
    t('Master'),
    t('guia uso'),
    t('more'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Ordenar por'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- Todos -'),
    t('Offset'),
    t('« primera'),
    t('‹ anterior'),
    t('siguiente ›'),
    t('última »'),
    t('order'),
    t('Page'),
  );
  $export['guia_uso'] = $view;

  return $export;
}
