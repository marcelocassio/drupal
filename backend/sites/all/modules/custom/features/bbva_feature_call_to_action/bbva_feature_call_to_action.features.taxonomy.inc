<?php
/**
 * @file
 * bbva_feature_call_to_action.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function bbva_feature_call_to_action_taxonomy_default_vocabularies() {
  return array(
    'experience' => array(
      'name' => 'Experiencias',
      'machine_name' => 'experience',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -9,
    ),
    'experience_list' => array(
      'name' => 'Experiencia Lista (2do Nivel)',
      'machine_name' => 'experience_list',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -7,
    ),
  );
}
