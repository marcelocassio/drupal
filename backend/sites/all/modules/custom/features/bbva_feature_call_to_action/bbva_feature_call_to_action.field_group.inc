<?php
/**
 * @file
 * bbva_feature_call_to_action.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_feature_call_to_action_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_multiple_column_right|field_collection_item|field_term_items_multiple|form';
  $field_group->group_name = 'group_multiple_column_right';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_term_items_multiple';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_multiple_columns';
  $field_group->data = array(
    'label' => 'Right',
    'weight' => '9',
    'children' => array(
      0 => 'field_multiple_related_term2',
      1 => 'field_multiple_pos_app',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Right',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'column-right',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_multiple_column_right|field_collection_item|field_term_items_multiple|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_multiple_columns_left|field_collection_item|field_term_items_multiple|form';
  $field_group->group_name = 'group_multiple_columns_left';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_term_items_multiple';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_multiple_columns';
  $field_group->data = array(
    'label' => 'Left',
    'weight' => '7',
    'children' => array(
      0 => 'field_multiple_logo',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Left',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'column-left',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_multiple_columns_left|field_collection_item|field_term_items_multiple|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_multiple_columns|field_collection_item|field_term_items_multiple|form';
  $field_group->group_name = 'group_multiple_columns';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_term_items_multiple';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Columns',
    'weight' => '7',
    'children' => array(
      0 => 'group_multiple_columns_left',
      1 => 'group_multiple_column_right',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-multiple-columns field-group-htabs',
      ),
    ),
  );
  $export['group_multiple_columns|field_collection_item|field_term_items_multiple|form'] = $field_group;

  return $export;
}
