<?php
/**
 * @file
 * bbva_feature_call_to_action.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_feature_call_to_action_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
