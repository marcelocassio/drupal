<?php
/**
 * @file
 * bbva_feature_json_view_home_slide.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bbva_feature_json_view_home_slide_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'json_home_slide';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Json Home Slide';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Json Home Slide';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Campo: Field collection item: Image */
  $handler->display->display_options['fields']['field_slide_image']['id'] = 'field_slide_image';
  $handler->display->display_options['fields']['field_slide_image']['table'] = 'field_data_field_slide_image';
  $handler->display->display_options['fields']['field_slide_image']['field'] = 'field_slide_image';
  $handler->display->display_options['fields']['field_slide_image']['label'] = '';
  $handler->display->display_options['fields']['field_slide_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_slide_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_slide_image']['settings'] = array(
    'image_style' => 'slideshow',
    'image_link' => '',
  );
  /* Campo: Field collection item: Imagen APP */
  $handler->display->display_options['fields']['field_slide_image_app']['id'] = 'field_slide_image_app';
  $handler->display->display_options['fields']['field_slide_image_app']['table'] = 'field_data_field_slide_image_app';
  $handler->display->display_options['fields']['field_slide_image_app']['field'] = 'field_slide_image_app';
  $handler->display->display_options['fields']['field_slide_image_app']['label'] = '';
  $handler->display->display_options['fields']['field_slide_image_app']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_image_app']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_slide_image_app']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Campo: Field collection item: Type */
  $handler->display->display_options['fields']['field_slide_type']['id'] = 'field_slide_type';
  $handler->display->display_options['fields']['field_slide_type']['table'] = 'field_data_field_slide_type';
  $handler->display->display_options['fields']['field_slide_type']['field'] = 'field_slide_type';
  $handler->display->display_options['fields']['field_slide_type']['label'] = '';
  $handler->display->display_options['fields']['field_slide_type']['element_label_colon'] = FALSE;
  /* Campo: Field collection item: Benefit Reference */
  $handler->display->display_options['fields']['field_slide_reference_benefit']['id'] = 'field_slide_reference_benefit';
  $handler->display->display_options['fields']['field_slide_reference_benefit']['table'] = 'field_data_field_slide_reference_benefit';
  $handler->display->display_options['fields']['field_slide_reference_benefit']['field'] = 'field_slide_reference_benefit';
  $handler->display->display_options['fields']['field_slide_reference_benefit']['label'] = '';
  $handler->display->display_options['fields']['field_slide_reference_benefit']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slide_reference_benefit']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_reference_benefit']['type'] = 'entityreference_entity_id';
  $handler->display->display_options['fields']['field_slide_reference_benefit']['settings'] = array(
    'link' => 0,
  );
  /* Campo: Field collection item: Experience Reference */
  $handler->display->display_options['fields']['field_slide_reference']['id'] = 'field_slide_reference';
  $handler->display->display_options['fields']['field_slide_reference']['table'] = 'field_data_field_slide_reference';
  $handler->display->display_options['fields']['field_slide_reference']['field'] = 'field_slide_reference';
  $handler->display->display_options['fields']['field_slide_reference']['label'] = '';
  $handler->display->display_options['fields']['field_slide_reference']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_slide_reference']['alter']['text'] = '[field_slide_reference][field_slide_reference_benefit]';
  $handler->display->display_options['fields']['field_slide_reference']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_reference']['type'] = 'entityreference_entity_id';
  $handler->display->display_options['fields']['field_slide_reference']['settings'] = array(
    'link' => 0,
  );
  /* Campo: Field collection item: Campain Reference */
  $handler->display->display_options['fields']['field_slide_reference_campain']['id'] = 'field_slide_reference_campain';
  $handler->display->display_options['fields']['field_slide_reference_campain']['table'] = 'field_data_field_slide_reference_campain';
  $handler->display->display_options['fields']['field_slide_reference_campain']['field'] = 'field_slide_reference_campain';
  $handler->display->display_options['fields']['field_slide_reference_campain']['label'] = '';
  $handler->display->display_options['fields']['field_slide_reference_campain']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_slide_reference_campain']['alter']['text'] = '[field_slide_reference][field_slide_reference_campain-target_id]';
  $handler->display->display_options['fields']['field_slide_reference_campain']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slide_reference_campain']['settings'] = array(
    'link' => 0,
  );
  /* Campo: Campo: Call to Action */
  $handler->display->display_options['fields']['field_call_to_action']['id'] = 'field_call_to_action';
  $handler->display->display_options['fields']['field_call_to_action']['table'] = 'field_data_field_call_to_action';
  $handler->display->display_options['fields']['field_call_to_action']['field'] = 'field_call_to_action';
  $handler->display->display_options['fields']['field_call_to_action']['label'] = '';
  $handler->display->display_options['fields']['field_call_to_action']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_call_to_action']['alter']['text'] = '[field_call_to_action-value] ';
  $handler->display->display_options['fields']['field_call_to_action']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_call_to_action']['type'] = 'services';
  $handler->display->display_options['fields']['field_call_to_action']['settings'] = array(
    'skip_safe' => 0,
    'skip_empty_values' => 0,
  );
  /* Sort criterion: Field collection item: Pos App (field_slide_pos_app) */
  $handler->display->display_options['sorts']['field_slide_pos_app_value']['id'] = 'field_slide_pos_app_value';
  $handler->display->display_options['sorts']['field_slide_pos_app_value']['table'] = 'field_data_field_slide_pos_app';
  $handler->display->display_options['sorts']['field_slide_pos_app_value']['field'] = 'field_slide_pos_app_value';
  /* Filter criterion: Field collection item: Image (field_slide_image:fid) */
  $handler->display->display_options['filters']['field_slide_image_fid']['id'] = 'field_slide_image_fid';
  $handler->display->display_options['filters']['field_slide_image_fid']['table'] = 'field_data_field_slide_image';
  $handler->display->display_options['filters']['field_slide_image_fid']['field'] = 'field_slide_image_fid';
  $handler->display->display_options['filters']['field_slide_image_fid']['operator'] = 'not empty';
  /* Filter criterion: Campo: Destacado en App (field_category_app) */
  $handler->display->display_options['filters']['field_category_app_value']['id'] = 'field_category_app_value';
  $handler->display->display_options['filters']['field_category_app_value']['table'] = 'field_data_field_category_app';
  $handler->display->display_options['filters']['field_category_app_value']['field'] = 'field_category_app_value';
  $handler->display->display_options['filters']['field_category_app_value']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Campo: Publicar (field_published) */
  $handler->display->display_options['filters']['field_published_value']['id'] = 'field_published_value';
  $handler->display->display_options['filters']['field_published_value']['table'] = 'field_data_field_published';
  $handler->display->display_options['filters']['field_published_value']['field'] = 'field_published_value';
  $handler->display->display_options['filters']['field_published_value']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Field collection item: Vigencia - end date (field_slide_vigencia:value2) */
  $handler->display->display_options['filters']['field_slide_vigencia_value2']['id'] = 'field_slide_vigencia_value2';
  $handler->display->display_options['filters']['field_slide_vigencia_value2']['table'] = 'field_data_field_slide_vigencia';
  $handler->display->display_options['filters']['field_slide_vigencia_value2']['field'] = 'field_slide_vigencia_value2';
  $handler->display->display_options['filters']['field_slide_vigencia_value2']['operator'] = '>=';
  $handler->display->display_options['filters']['field_slide_vigencia_value2']['default_date'] = 'now';
  /* Filter criterion: Field collection item: Vigencia -  start date (field_slide_vigencia) */
  $handler->display->display_options['filters']['field_slide_vigencia_value']['id'] = 'field_slide_vigencia_value';
  $handler->display->display_options['filters']['field_slide_vigencia_value']['table'] = 'field_data_field_slide_vigencia';
  $handler->display->display_options['filters']['field_slide_vigencia_value']['field'] = 'field_slide_vigencia_value';
  $handler->display->display_options['filters']['field_slide_vigencia_value']['operator'] = '<=';
  $handler->display->display_options['filters']['field_slide_vigencia_value']['default_date'] = 'now';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'json-home-slide';
  $translatables['json_home_slide'] = array(
    t('Master'),
    t('Json Home Slide'),
    t('more'),
    t('Aplicar'),
    t('Reiniciar'),
    t('Ordenar por'),
    t('Asc'),
    t('Desc'),
    t('[field_slide_reference][field_slide_reference_benefit]'),
    t('[field_slide_reference][field_slide_reference_campain-target_id]'),
    t('[field_call_to_action-value] '),
    t('Page'),
  );
  $export['json_home_slide'] = $view;

  return $export;
}
