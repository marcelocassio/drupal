<?php
/**
 * @file
 * bbva_feature_json_view_home_slide.features.inc
 */

/**
 * Implements hook_views_api().
 */
function bbva_feature_json_view_home_slide_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
