<?php
/**
 * @file
 * bbva_features_bases_comerciales.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_features_bases_comerciales_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function bbva_features_bases_comerciales_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function bbva_features_bases_comerciales_node_info() {
  $items = array(
    'bases_comerciales' => array(
      'name' => t('Bases Comerciales'),
      'base' => 'node_content',
      'description' => t('Carga de CSV desde IC a ser procesada para diversos usos'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
