<?php
/**
 * @file
 * bbva_features_bases_comerciales.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function bbva_features_bases_comerciales_user_default_roles() {
  $roles = array();

  // Exported role: IC.
  $roles['IC'] = array(
    'name' => 'IC',
    'weight' => 20,
  );

  return $roles;
}
