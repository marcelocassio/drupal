<?php
/**
 * @file
 * bbva_features_bases_comerciales.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function bbva_features_bases_comerciales_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'quicktabs';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Quicktabs';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta__experiences-block',
      'hide_title' => 1,
      'title' => 'Experiencias',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'views_delta__experiences-block_1',
      'hide_title' => 1,
      'title' => 'Experiencias',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'views_delta_b3-block',
      'hide_title' => 1,
      'title' => 'Otras Oportunidades',
      'weight' => '-98',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Experiencias');
  t('Otras Oportunidades');
  t('Quicktabs');

  $export['quicktabs'] = $quicktabs;

  return $export;
}
