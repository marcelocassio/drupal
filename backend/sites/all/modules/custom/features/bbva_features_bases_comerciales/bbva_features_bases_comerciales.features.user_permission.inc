<?php
/**
 * @file
 * bbva_features_bases_comerciales.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bbva_features_bases_comerciales_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'Permisos para la View de Bases comerciales'.
  $permissions['Permisos para la View de Bases comerciales'] = array(
    'name' => 'Permisos para la View de Bases comerciales',
    'roles' => array(
      'IC' => 'IC',
      'administrator' => 'administrator',
    ),
    'module' => 'bbva_bases_comerciales',
  );

  // Exported permission: 'create bases_comerciales content'.
  $permissions['create bases_comerciales content'] = array(
    'name' => 'create bases_comerciales content',
    'roles' => array(
      'IC' => 'IC',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bases_comerciales content'.
  $permissions['delete any bases_comerciales content'] = array(
    'name' => 'delete any bases_comerciales content',
    'roles' => array(
      'IC' => 'IC',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bases_comerciales content'.
  $permissions['delete own bases_comerciales content'] = array(
    'name' => 'delete own bases_comerciales content',
    'roles' => array(
      'IC' => 'IC',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bases_comerciales content'.
  $permissions['edit any bases_comerciales content'] = array(
    'name' => 'edit any bases_comerciales content',
    'roles' => array(
      'IC' => 'IC',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bases_comerciales content'.
  $permissions['edit own bases_comerciales content'] = array(
    'name' => 'edit own bases_comerciales content',
    'roles' => array(
      'IC' => 'IC',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view any unpublished bases_comerciales content'.
  $permissions['view any unpublished bases_comerciales content'] = array(
    'name' => 'view any unpublished bases_comerciales content',
    'roles' => array(
      'IC' => 'IC',
    ),
    'module' => 'view_unpublished',
  );

  return $permissions;
}
