<?php
/**
 * @file
 * bbva_feature_product.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function bbva_feature_product_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|product|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function bbva_feature_product_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_product_link_button',
        2 => 'group_inner_footer',
        3 => 'field_product_description_short',
        4 => 'field_product_image_bg',
        5 => 'field_product_tyc',
        6 => 'field_product_image_button',
        7 => 'field_product_titulo_descripcion',
        8 => 'group_inner_footer_right',
        9 => 'group_footer',
        10 => 'field_product_lista_descripcion',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_product_link_button' => 'ds_content',
      'group_inner_footer' => 'ds_content',
      'field_product_description_short' => 'ds_content',
      'field_product_image_bg' => 'ds_content',
      'field_product_tyc' => 'ds_content',
      'field_product_image_button' => 'ds_content',
      'field_product_titulo_descripcion' => 'ds_content',
      'group_inner_footer_right' => 'ds_content',
      'group_footer' => 'ds_content',
      'field_product_lista_descripcion' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|product|full'] = $ds_layout;

  return $export;
}
