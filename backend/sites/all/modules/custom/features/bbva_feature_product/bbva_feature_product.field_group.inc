<?php
/**
 * @file
 * bbva_feature_product.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_feature_product_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_footer|node|product|full';
  $field_group->group_name = 'group_footer';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer',
    'weight' => '5',
    'children' => array(
      0 => 'group_inner_footer',
      1 => 'group_inner_footer_right',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-footer field-group-fieldset',
      ),
    ),
  );
  $export['group_footer|node|product|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_inner_footer_right|node|product|full';
  $field_group->group_name = 'group_inner_footer_right';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_footer';
  $field_group->data = array(
    'label' => 'Inner Right',
    'weight' => '5',
    'children' => array(
      0 => 'field_product_titulo_descripcion',
      1 => 'field_product_lista_descripcion',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-inner-footer-right field-group-fieldset',
      ),
    ),
  );
  $export['group_inner_footer_right|node|product|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_inner_footer|node|product|full';
  $field_group->group_name = 'group_inner_footer';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_footer';
  $field_group->data = array(
    'label' => 'Inner',
    'weight' => '2',
    'children' => array(
      0 => 'field_product_description_short',
      1 => 'field_product_tyc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-inner-footer field-group-fieldset',
      ),
    ),
  );
  $export['group_inner_footer|node|product|full'] = $field_group;

  return $export;
}
