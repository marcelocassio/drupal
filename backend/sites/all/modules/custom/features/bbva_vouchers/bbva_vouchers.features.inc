<?php
/**
 * @file
 * bbva_vouchers.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_vouchers_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_entity_type_info().
 */
function bbva_vouchers_eck_entity_type_info() {
  $items = array(
    'voucher' => array(
      'name' => 'voucher',
      'label' => 'Voucher',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
  );
  return $items;
}
