<?php
/**
 * @file
 * bbva_feature_draw.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function bbva_feature_draw_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_draw|default';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_draw';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_3col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_draw_image',
      ),
      'middle' => array(
        1 => 'field_draw_description',
        2 => 'field_draw_link',
      ),
    ),
    'fields' => array(
      'field_draw_image' => 'left',
      'field_draw_description' => 'middle',
      'field_draw_link' => 'middle',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['field_collection_item|field_draw|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_draw|full';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_draw';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_3col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_draw_image',
      ),
      'middle' => array(
        1 => 'field_draw_title',
        2 => 'field_draw_sub_title',
        3 => 'field_draw_description',
        4 => 'field_draw_link',
      ),
    ),
    'fields' => array(
      'field_draw_image' => 'left',
      'field_draw_title' => 'middle',
      'field_draw_sub_title' => 'middle',
      'field_draw_description' => 'middle',
      'field_draw_link' => 'middle',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['field_collection_item|field_draw|full'] = $ds_layout;

  return $export;
}
