<?php
/**
 * @file
 * bbva_feature_draw.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_feature_draw_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}
