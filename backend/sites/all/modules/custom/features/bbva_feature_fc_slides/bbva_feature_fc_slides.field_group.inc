<?php
/**
 * @file
 * bbva_feature_fc_slides.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_feature_fc_slides_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_carrusel_app_web_gr|field_collection_item|field_slides|form';
  $field_group->group_name = 'group_carrusel_app_web_gr';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_slides';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'C1',
    'weight' => '0',
    'children' => array(
      0 => 'group_carrusel_web',
      1 => 'group_carrusel_app',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-carrusel-app-web-gr field-group-htabs',
      ),
    ),
  );
  $export['group_carrusel_app_web_gr|field_collection_item|field_slides|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_carrusel_app|field_collection_item|field_slides|form';
  $field_group->group_name = 'group_carrusel_app';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_slides';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_carrusel_app_web_gr';
  $field_group->data = array(
    'label' => 'App',
    'weight' => '18',
    'children' => array(
      0 => 'field_category_app',
      1 => 'field_slide_pos_app',
      2 => 'field_call_to_action',
      3 => 'field_slide_image_app',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-carrusel-app field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_carrusel_app|field_collection_item|field_slides|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_carrusel_web|field_collection_item|field_slides|form';
  $field_group->group_name = 'group_carrusel_web';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_slides';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_carrusel_app_web_gr';
  $field_group->data = array(
    'label' => 'Web',
    'weight' => '16',
    'children' => array(
      0 => 'field_slide_image',
      1 => 'field_slide_reference',
      2 => 'field_slide_type',
      3 => 'field_slide_reference_benefit',
      4 => 'field_slide_link',
      5 => 'field_slide_reference_campain',
      6 => 'field_published',
      7 => 'field_slide_posicion_web',
      8 => 'group_costado_uno',
      9 => 'group_col_dest',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-carrusel-web field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_carrusel_web|field_collection_item|field_slides|form'] = $field_group;

  return $export;
}
