<?php
/**
 * @file
 * bbva_coupon_promo.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function bbva_coupon_promo_taxonomy_default_vocabularies() {
  return array(
    'code_promo_cuopon' => array(
      'name' => 'Code Promo Cuopon',
      'machine_name' => 'code_promo_cuopon',
      'description' => 'codigos de cupones habilitados',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
