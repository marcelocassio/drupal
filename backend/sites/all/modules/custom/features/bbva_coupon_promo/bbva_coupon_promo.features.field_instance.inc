<?php
/**
 * @file
 * bbva_coupon_promo.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bbva_coupon_promo_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_destacado'
  $field_instances['coupon_promo-coupon_promo-field_coupon_destacado'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 26,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_destacado',
    'label' => 'Destacado',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_benefit'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_benefit'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'colorbox_node_classes' => '',
          'colorbox_node_height' => 600,
          'colorbox_node_link' => FALSE,
          'colorbox_node_width' => 600,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 20,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_benefit',
    'label' => 'benefit',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'editable' => FALSE,
        'fields' => array(),
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
        'type_settings' => array(),
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_benefit_asoc'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_benefit_asoc'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 21,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_benefit_asoc',
    'label' => 'Vincular Beneficio?',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_branchs'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_branchs'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_branchs',
    'label' => 'Branchs',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_campania_id'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_campania_id'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 25,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_campania_id',
    'label' => 'ID de Campaña',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_date_end'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_date_end'] = array(
    'bundle' => 'coupon_promo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_date_end',
    'label' => 'Hasta',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'none',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 35,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_date_send'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_date_send'] = array(
    'bundle' => 'coupon_promo',
    'deleted' => 0,
    'description' => 'No puede ser posterior fecha de inicio Vigencia menos 1',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_date_send',
    'label' => 'Desde (Date Send Visa)',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'none',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_date_start'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_date_start'] = array(
    'bundle' => 'coupon_promo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_date_start',
    'label' => 'Desde',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'none',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_discount'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_discount'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_discount',
    'label' => '% Descuento comercio',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 27,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_id_comunica'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_id_comunica'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 28,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_id_comunica',
    'label' => 'Id Comunicación SUIP',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_image'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_image'] = array(
    'bundle' => 'coupon_promo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 18,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'txt',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_legal'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_legal'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_legal',
    'label' => 'Legal',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_lquantity'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_lquantity'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '(Hasta 999.999)',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_lquantity',
    'label' => 'Cantidad de cupones',
    'required' => 0,
    'settings' => array(
      'max' => 999999,
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_mail'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_mail'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 16,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_mail',
    'label' => 'Mail',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 34,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_mail_subject'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_mail_subject'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_mail_subject',
    'label' => 'Asunto',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 35,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_msg_web_app'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_msg_web_app'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_msg_web_app',
    'label' => 'Web/App Mensaje corto',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_numero'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_numero'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 22,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_numero',
    'label' => 'Promo numero',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 24,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_promo_suip'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_promo_suip'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 27,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_promo_suip',
    'label' => 'Promo Suip',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_segments_user'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_segments_user'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 3,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_segments_user',
    'label' => 'Destinatarios',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 30,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_shop'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_shop'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'colorbox_node_classes' => '',
          'colorbox_node_height' => 600,
          'colorbox_node_link' => FALSE,
          'colorbox_node_width' => 600,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_shop',
    'label' => 'Marca',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 25,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_sms_push_msg'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_sms_push_msg'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_sms_push_msg',
    'label' => 'SMS/Push',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 144,
      ),
      'type' => 'text_textfield',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_subtitle'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_subtitle'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Esta descripción acompaña al descuento en el listado y en el detalle del beneficio. <strong>Ejemplo:</strong> "<i>En un perfume particular </i>".<br>
En caso que no se complete este campo se publicará el siguiente texto "<i>en el total de tu compra </i>". <br>
Este campo está limitado a <strong>25 caracteres</strong>.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 29,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_subtitle',
    'label' => 'Descripción del Beneficio',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_sucursal_file'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_sucursal_file'] = array(
    'bundle' => 'coupon_promo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_sucursal_file',
    'label' => 'Sucursal File',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'txt',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 0,
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 'default',
          'image' => 0,
          'video' => 0,
        ),
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_target'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_target'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_target',
    'label' => 'Target',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 28,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_top'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_top'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'Monto máximo de la venta con descuento (Hasta 5 digitos)',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 2,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_decimal',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_top',
    'label' => 'Tope en $ cupón',
    'required' => 0,
    'settings' => array(
      'max' => 99999,
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 28,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_users_file'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_users_file'] = array(
    'bundle' => 'coupon_promo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_users_file',
    'label' => 'Users File',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => '',
      'file_extensions' => 'txt csv',
      'max_filesize' => '6 MB',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 29,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_promo_whit_send'
  $field_instances['coupon_promo-coupon_promo-field_coupon_promo_whit_send'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_promo_whit_send',
    'label' => 'Whit Send',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_state_shipping'
  $field_instances['coupon_promo-coupon_promo-field_coupon_state_shipping'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_state_shipping',
    'label' => 'State shipping',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 14,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_type'
  $field_instances['coupon_promo-coupon_promo-field_coupon_type'] = array(
    'bundle' => 'coupon_promo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 24,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_type',
    'label' => 'Tipo de cupon',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 26,
    ),
  );

  // Exported field_instance: 'coupon_promo-coupon_promo-field_coupon_vochers_file'
  $field_instances['coupon_promo-coupon_promo-field_coupon_vochers_file'] = array(
    'bundle' => 'coupon_promo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 23,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'coupon_promo',
    'field_name' => 'field_coupon_vochers_file',
    'label' => 'Vochers File',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'voucher',
      'file_extensions' => 'csv',
      'max_filesize' => '6 MB',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'taxonomy_term-code_promo_cuopon-field_coupon_number_promo'
  $field_instances['taxonomy_term-code_promo_cuopon-field_coupon_number_promo'] = array(
    'bundle' => 'code_promo_cuopon',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_coupon_number_promo',
    'label' => 'number',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 4,
      ),
      'type' => 'text_textfield',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'taxonomy_term-code_promo_cuopon-field_coupon_number_state_send'
  $field_instances['taxonomy_term-code_promo_cuopon-field_coupon_number_state_send'] = array(
    'bundle' => 'code_promo_cuopon',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_coupon_number_state_send',
    'label' => 'state send',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 32,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('% Descuento comercio');
  t('(Hasta 999.999)');
  t('Asunto');
  t('Branchs');
  t('Cantidad de cupones');
  t('Descripción del Beneficio');
  t('Desde');
  t('Desde (Date Send Visa)');
  t('Destacado');
  t('Destinatarios');
  t('Esta descripción acompaña al descuento en el listado y en el detalle del beneficio. <strong>Ejemplo:</strong> "<i>En un perfume particular </i>".<br>
En caso que no se complete este campo se publicará el siguiente texto "<i>en el total de tu compra </i>". <br>
Este campo está limitado a <strong>25 caracteres</strong>.');
  t('Hasta');
  t('ID de Campaña');
  t('Id Comunicación SUIP');
  t('Image');
  t('Legal');
  t('Mail');
  t('Marca');
  t('Monto máximo de la venta con descuento (Hasta 5 digitos)');
  t('No puede ser posterior fecha de inicio Vigencia menos 1');
  t('Promo Suip');
  t('Promo numero');
  t('SMS/Push');
  t('State shipping');
  t('Sucursal File');
  t('Target');
  t('Tipo de cupon');
  t('Tope en $ cupón');
  t('Users File');
  t('Vincular Beneficio?');
  t('Vochers File');
  t('Web/App Mensaje corto');
  t('Whit Send');
  t('benefit');
  t('number');
  t('state send');

  return $field_instances;
}
