<?php
/**
 * @file
 * bbva_coupon_promo.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_coupon_promo_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_branch|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_branch';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sucursales',
    'weight' => '2',
    'children' => array(),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Sucursales',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-branch field-group-html-element',
        'element' => 'div',
        'show_label' => '1',
        'label_element' => 'h3',
        'attributes' => '',
      ),
    ),
  );
  $export['group_branch|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_coupon_dest_desc|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_coupon_dest_desc';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_coupon_promo_default';
  $field_group->data = array(
    'label' => 'Destinatarios y Descuentos',
    'weight' => '30',
    'children' => array(
      0 => 'field_coupon_promo_shop',
      1 => 'field_coupon_promo_users_file',
      2 => 'field_coupon_promo_target',
      3 => 'field_coupon_promo_segments_user',
      4 => 'field_coupon_promo_numero',
      5 => 'field_coupon_vochers_file',
      6 => 'field_coupon_type',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Destinatarios y Descuentos',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-coupon-dest-desc field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_coupon_dest_desc|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_coupon_promo_default|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_coupon_promo_default';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Datos',
    'weight' => '0',
    'children' => array(
      0 => 'group_coupon_dest_desc',
      1 => 'group_coupon_promo_tipo_comu',
      2 => 'group_identifiacion',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-coupon-promo-default field-group-htabs',
      ),
    ),
  );
  $export['group_coupon_promo_default|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_coupon_promo_tipo_comu|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_coupon_promo_tipo_comu';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_coupon_promo_default';
  $field_group->data = array(
    'label' => 'Benefic. Vinculado y Comunicacion',
    'weight' => '32',
    'children' => array(
      0 => 'field_coupon_destacado',
      1 => 'field_coupon_promo_subtitle',
      2 => 'group_leagal',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Benefic. Vinculado y Comunicacion',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-coupon-promo-tipo-comu field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_coupon_promo_tipo_comu|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_date_end|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_date_end';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_identifiacion';
  $field_group->data = array(
    'label' => 'Date end ',
    'weight' => '30',
    'children' => array(
      0 => 'field_coupon_promo_date_end',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'element' => 'div',
        'show_label' => 0,
        'label_element' => 'div',
        'classes' => 'group-date-end field-group-html-element',
        'attributes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_date_end|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_date_send|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_date_send';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_identifiacion';
  $field_group->data = array(
    'label' => 'Fecha Envio ',
    'weight' => '32',
    'children' => array(
      0 => 'field_coupon_promo_date_send',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Fecha Envio ',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-date-send field-group-html-element',
        'element' => 'div',
        'show_label' => '1',
        'label_element' => 'h3',
        'attributes' => '',
      ),
    ),
  );
  $export['group_date_send|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_identifiacion|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_identifiacion';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_coupon_promo_default';
  $field_group->data = array(
    'label' => 'Datos de la promo',
    'weight' => '31',
    'children' => array(
      0 => 'field_coupon_promo_discount',
      1 => 'field_coupon_promo_top',
      2 => 'field_coupon_promo_lquantity',
      3 => 'group_date_end',
      4 => 'group_date_send',
      5 => 'group_start_h',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Datos de la promo',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_identifiacion|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_leagal|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_leagal';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_coupon_promo_tipo_comu';
  $field_group->data = array(
    'label' => 'Texto Legal',
    'weight' => '15',
    'children' => array(
      0 => 'field_coupon_promo_legal',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Texto Legal',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-leagal field-group-html-element',
        'element' => 'div',
        'show_label' => '1',
        'label_element' => 'h3',
        'attributes' => '',
      ),
    ),
  );
  $export['group_leagal|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_shops|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_shops';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Comercios',
    'weight' => '5',
    'children' => array(),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Comercios',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-shops field-group-html-element',
        'element' => 'div',
        'show_label' => '1',
        'label_element' => 'h3',
        'attributes' => '',
      ),
    ),
  );
  $export['group_shops|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_start_h|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_start_h';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_identifiacion';
  $field_group->data = array(
    'label' => 'start',
    'weight' => '29',
    'children' => array(
      0 => 'field_coupon_promo_date_start',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'element' => 'div',
        'show_label' => 0,
        'label_element' => 'div',
        'classes' => 'group-start-h field-group-html-element',
        'attributes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_start_h|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_type_com|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_type_com';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Tipo Comunicacion',
    'weight' => '12',
    'children' => array(
      0 => 'field_coupon_promo_sms_push_msg',
      1 => 'field_coupon_promo_whit_send',
      2 => 'field_coupon_promo_mail',
      3 => 'field_coupon_promo_mail_subject',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Tipo Comunicacion',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-type-com field-group-html-element',
        'element' => 'div',
        'show_label' => '1',
        'label_element' => 'h3',
        'attributes' => '',
      ),
    ),
  );
  $export['group_type_com|coupon_promo|coupon_promo|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vigencia|coupon_promo|coupon_promo|form';
  $field_group->group_name = 'group_vigencia';
  $field_group->entity_type = 'coupon_promo';
  $field_group->bundle = 'coupon_promo';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Vigencia',
    'weight' => '4',
    'children' => array(),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Vigencia',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-vigencia field-group-html-element',
        'element' => 'div',
        'show_label' => '1',
        'label_element' => 'h3',
        'attributes' => '',
      ),
    ),
  );
  $export['group_vigencia|coupon_promo|coupon_promo|form'] = $field_group;

  return $export;
}
