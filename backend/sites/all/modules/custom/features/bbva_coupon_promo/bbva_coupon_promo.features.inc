<?php
/**
 * @file
 * bbva_coupon_promo.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_coupon_promo_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function bbva_coupon_promo_eck_bundle_info() {
  $items = array(
    'coupon_promo_coupon_promo' => array(
      'machine_name' => 'coupon_promo_coupon_promo',
      'entity_type' => 'coupon_promo',
      'name' => 'coupon_promo',
      'label' => 'coupon promotion',
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function bbva_coupon_promo_eck_entity_type_info() {
  $items = array(
    'coupon_promo' => array(
      'name' => 'coupon_promo',
      'label' => 'coupon promotion',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
  );
  return $items;
}
