<?php
/**
 * @file
 * bbva_features_experience.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bbva_features_experience_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create experience content'.
  $permissions['create experience content'] = array(
    'name' => 'create experience content',
    'roles' => array(
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Publicidad' => 'Publicidad',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any experience content'.
  $permissions['delete any experience content'] = array(
    'name' => 'delete any experience content',
    'roles' => array(
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Publicidad' => 'Publicidad',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own experience content'.
  $permissions['delete own experience content'] = array(
    'name' => 'delete own experience content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any experience content'.
  $permissions['edit any experience content'] = array(
    'name' => 'edit any experience content',
    'roles' => array(
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Publicidad' => 'Publicidad',
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own experience content'.
  $permissions['edit own experience content'] = array(
    'name' => 'edit own experience content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view any unpublished experience content'.
  $permissions['view any unpublished experience content'] = array(
    'name' => 'view any unpublished experience content',
    'roles' => array(
      'Alianzas - Fgo' => 'Alianzas - Fgo',
      'Publicidad' => 'Publicidad',
      'administrator' => 'administrator',
    ),
    'module' => 'view_unpublished',
  );

  return $permissions;
}
