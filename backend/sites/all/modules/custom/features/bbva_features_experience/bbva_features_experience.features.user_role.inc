<?php
/**
 * @file
 * bbva_features_experience.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function bbva_features_experience_user_default_roles() {
  $roles = array();

  // Exported role: Alianzas - Experiencias.
  $roles['Alianzas - Experiencias'] = array(
    'name' => 'Alianzas - Experiencias',
    'weight' => 4,
  );

  return $roles;
}
