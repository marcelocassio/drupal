<?php
/**
 * @file
 * bbva_features_experience.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bbva_features_experience_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bbva_features_experience_node_info() {
  $items = array(
    'experience' => array(
      'name' => t('Sorteos'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
