<?php
/**
 * @file
 * bbva_features_experience.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bbva_features_experience_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_experience_default|node|experience|form';
  $field_group->group_name = 'group_experience_default';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'experience';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Datos',
    'weight' => '0',
    'children' => array(
      0 => 'group_experience_general',
      1 => 'group_experience_sorteo',
      2 => 'group_experience_useless',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-experience-default field-group-htabs',
      ),
    ),
  );
  $export['group_experience_default|node|experience|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_experience_general|node|experience|form';
  $field_group->group_name = 'group_experience_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'experience';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_experience_default';
  $field_group->data = array(
    'label' => 'General',
    'weight' => '1',
    'children' => array(
      0 => 'field_experience_image',
      1 => 'field_experience_bajada',
      2 => 'field_experience_volanta',
      3 => 'field_experience_descripcion',
      4 => 'field_experience_weight',
      5 => 'field_experience_category',
      6 => 'field_experience_color',
      7 => 'field_experience_logo',
      8 => 'field_experience_fija_titulo3',
      9 => 'field_experience_fija_texto3',
      10 => 'field_experience_slide',
      11 => 'field_experience_video',
      12 => 'field_experience_map',
      13 => 'field_tags',
      14 => 'field_experience_image_list',
      15 => 'field_experience_previous',
      16 => 'field_experience',
      17 => 'field_experience_imagen_app',
      18 => 'locations',
      19 => 'metatags',
      20 => 'title',
      21 => 'path',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-experience-general field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_experience_general|node|experience|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_experience_sorteo|node|experience|form';
  $field_group->group_name = 'group_experience_sorteo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'experience';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_experience_default';
  $field_group->data = array(
    'label' => 'Sorteo',
    'weight' => '2',
    'children' => array(
      0 => 'field_experience_fija_link',
      1 => 'field_experience_code_sorteo',
      2 => 'field_experience_es_sorteo',
      3 => 'field_experience_fecha_sortea',
      4 => 'field_experience_fechas_vigencia',
      5 => 'field_experience_ganador_sorteo',
      6 => 'field_experience_mecanica_sorteo',
      7 => 'field_experience_tyc',
      8 => 'field_experience_seas_cliente',
      9 => 'field_experience_check_code_uniq',
      10 => 'field_experience_code_sorteo_nc',
      11 => 'field_experience_code_sorteo_st',
      12 => 'field_experience_code_sorteo_tc',
      13 => 'field_experience_estado_sorteo',
      14 => 'field_experience_fecha_enterate',
      15 => 'field_experience_ganadores_push',
      16 => 'field_experience_web_view',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-experience-sorteo field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_experience_sorteo|node|experience|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_experience_useless|node|experience|form';
  $field_group->group_name = 'group_experience_useless';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'experience';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_experience_default';
  $field_group->data = array(
    'label' => 'No necesario',
    'weight' => '22',
    'children' => array(
      0 => 'field_experience_weight',
      1 => 'field_experience_category',
      2 => 'field_experience_color',
      3 => 'field_experience_logo',
      4 => 'field_experience_fija_titulo3',
      5 => 'field_experience_fija_texto3',
      6 => 'field_experience_slide',
      7 => 'field_experience_video',
      8 => 'field_experience_map',
      9 => 'field_experience_image_list',
      10 => 'field_experience_previous',
      11 => 'field_experience',
      12 => 'field_experience_leyenda_ganador',
      13 => 'field_experience_bitly_url',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-experience-useless field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_experience_useless|node|experience|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_c3|node|forms|form';
  $field_group->group_name = 'group_home_c3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'forms';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_home_general';
  $field_group->data = array(
    'label' => 'Carusel 3',
    'weight' => '10',
    'children' => array(
      0 => 'field_group_experiences',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_home_c3|node|forms|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_general|node|forms|form';
  $field_group->group_name = 'group_home_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'forms';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'General',
    'weight' => '3',
    'children' => array(
      0 => 'group_home_c1',
      1 => 'group_home_rubros',
      2 => 'group_home_c3',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_home_general|node|forms|form'] = $field_group;

  return $export;
}
