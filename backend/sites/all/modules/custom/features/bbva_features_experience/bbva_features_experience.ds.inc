<?php
/**
 * @file
 * bbva_features_experience.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function bbva_features_experience_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|experience|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'experience';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'experiences_related' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'experience' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'experience_carrusel' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|experience|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|experience|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'experience';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'experience_teaser' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|experience|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function bbva_features_experience_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'experiences_related';
  $ds_field->label = 'Experiences Related';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|experience_related-block',
    'block_render' => '2',
  );
  $export['experiences_related'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function bbva_features_experience_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_experience_items|full';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_experience_items';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'field_experience_image',
      ),
      'left' => array(
        1 => 'field_experience_bajada',
      ),
      'right' => array(
        2 => 'field_experience_descripcion',
        3 => 'field_experience_link',
      ),
    ),
    'fields' => array(
      'field_experience_image' => 'header',
      'field_experience_bajada' => 'left',
      'field_experience_descripcion' => 'right',
      'field_experience_link' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['field_collection_item|field_experience_items|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|experience|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'experience';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_3col_stacked_equal_width';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'experience',
      ),
      'left' => array(
        1 => 'experience_carrusel',
      ),
      'right' => array(
        2 => 'field_experience_bajada',
        3 => 'field_experience_descripcion',
        4 => 'field_experience_fija_titulo3',
        5 => 'field_experience_fija_texto3',
      ),
      'footer' => array(
        6 => 'field_experience_map',
        7 => 'experiences_related',
      ),
    ),
    'fields' => array(
      'experience' => 'header',
      'experience_carrusel' => 'left',
      'field_experience_bajada' => 'right',
      'field_experience_descripcion' => 'right',
      'field_experience_fija_titulo3' => 'right',
      'field_experience_fija_texto3' => 'right',
      'field_experience_map' => 'footer',
      'experiences_related' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|experience|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|experience|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'experience';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'experience_teaser',
      ),
    ),
    'fields' => array(
      'experience_teaser' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|experience|teaser'] = $ds_layout;

  return $export;
}
