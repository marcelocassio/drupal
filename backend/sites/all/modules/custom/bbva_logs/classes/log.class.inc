<?php

namespace BBVA;

class Log {

  private static $instancia;

  public static function getInstance() {
    if (  !self::$instancia instanceof self) {
      self::$instancia = new self;
    }
    return self::$instancia;
  }

  private function __construct() {
    // El constructor debe ser privado (singleton)
  }

  public function wsdebug($debug){
    $salida = print_r($debug, true);
    watchdog('debug',"DEBUG:   %salida", array('%salida' => $salida), WATCHDOG_DEBUG);
  }

  public function insertar($nombre, $log) {
    try {
      if(!$nombre) {
        $nombre = "indefinido";
      }
      if($log) {
        $log = serialize($log);
      }

      $this->guardar($nombre, $log);
    } catch (\Exception $e) {
      return FALSE;
    }

    return TRUE;
  }

  public function obtener($inicio = 0, $cantidad = 100, $nombre = NULL) {
    $q = db_select("bbva_logs", "bl");
    if($nombre) {
      $q = $q->condition("nombre", $nombre);
    }
    $q = $q->fields("bl", array("fecha_creacion", "nombre", "data"));
    $q = $q->range($inicio, $cantidad);
    $q = $q->orderBy('bl.fecha_creacion', 'DESC');

    $q = $q->execute();

    $datos = array();
    while($fila = $q->fetchObject()) {
      if($fila->data) {
        $data = unserialize($fila->data); 
      }
      $fila->fecha_creacion = date("Y-m-d H:i:s", $fila->fecha_creacion);
      $fila->data_string = print_r($data, TRUE);
      unset($fila->data);
      $datos[] = (array) $fila;
    }

    return $datos;
  }

  private function guardar($nombre, $log) {
    $q = db_insert("bbva_logs")
      ->fields(
        array(
          "nombre" => $nombre,
          "data" => $log,
          "fecha_creacion" => REQUEST_TIME
        )
      )
      ->execute();

    if(!$q) {
      throw new Exception("No se pudo insertar el log", 1);
    }
  }

  public function limpiar($timestamp) {
    $deleted = db_delete('bbva_logs')
      ->condition('fecha_creacion', $timestamp, '<')
      ->execute();
  }

}