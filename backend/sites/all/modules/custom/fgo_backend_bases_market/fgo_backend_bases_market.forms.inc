<?php
// Metodos para los formularios de edicion y creacion de Bases

function fgo_backend_bases_market_form_display($form_state)
{
    $basesMarketBo = new \Fgo\Bo\BasesMarketBo();
    $basesMarketBo->obtenerRegistroActual();

    $form['redirect'] = false;
    $form['#attributes']['autocomplete'] = "off";
    $form['#validate'][] = 'fgo_backend_bases_market_form_validate';

    $form['idBasesMarket'] = array(
        '#type' => 'hidden',
        '#default_value' => $basesMarketBo->idBasesMarket
    );

    $form['grupo_dato'] = array(
        '#type' => 'fieldset',
        '#title' => t('<strong><i>Bases y condiciones de sección de Market</i></strong>')
    );

    $form['grupo_dato']['texto'] = array(
        '#type' => 'textarea',
        '#id' => 'fgo_backend_bases_market_texto',
        "#description" => t('Usar enters para que se vea en otra viñeta.<br>Usar &lt;br&gt; para un salto de linea en la misma viñeta.'),
        '#default_value' => $basesMarketBo->texto
    );

    $form['guardar'] = array(
        '#type' => 'submit',
        '#id' => 'guardar_bases',
        '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
        '#value' => 'Guardar',
        '#submit' => array('fgo_backend_bases_market_submit')
    );  

    return $form;
}

function fgo_backend_bases_market_form_validate(&$form, &$form_state)
{
    $basesMarketBo = new \Fgo\Bo\BasesMarketBo($form_state['values']['idBasesMarket']);
    $basesMarketBo->texto = $form_state['values']['texto'];

    try {
        $basesMarketBo->guardar();
        cache_set('fgo_backend_bases_market_field_texto', $form_state['values']['texto']);
    } catch (\Fgo\Bo\DatosInvalidosException $e) {
        $errores = $e->getErrores();
        foreach ($errores as $error_campo => $error_descripcion) {
            form_set_error($error_campo, $error_descripcion);
        }
    }
}

function fgo_backend_bases_market_submit(&$form, &$form_state)
{
    $form_state['redirect'] = url('../admin/v2/bases_market');
}
