<?php

/**
 * Admin settings page.
 */
function bbva_cupones_admin($form, $form_state) {

  $form['bbva_cupones_version'] = array(
    '#title' => t('Version nueva de cupones?'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bbva_cupones_version', FALSE),
  );

  return system_settings_form($form);
}