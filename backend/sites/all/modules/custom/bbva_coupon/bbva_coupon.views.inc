<?php

/**
 * Views page.
 */

function bbva_vouchers_view_admin(){

  $sort = $_GET['sort'];

  if ((isset($_GET['filter_id_cupon'])) && ($_GET['filter_id_cupon'] != '')) {
    $filtros["id_cupon"] = $_GET['filter_id_cupon'];
  }
  if ((isset($_GET['filter_state'])) && ($_GET['filter_state'] != '')) {
    $filtros["state"] = $_GET['filter_state'];
  }
  if ((isset($_GET['filter_name_user'])) && ($_GET['filter_name_user'] != '')) {
    $filtros["name_user"] = $_GET['filter_name_user'];
  }

  $view_vouchers = _get_voucher_view('id_cupon', $sort, $filtros,100);

  return bbva_vouchers_create_voucher_view($view_vouchers);
}

function bbva_vouchers_view_admin_submit($form, &$form_state) {
  $form_state['filters']['id_cupon'] = $form_state['values']['filter_id_cupon'];
  $form_state['filters']['state'] = $form_state['values']['filter_state'];
  $form_state['filters']['name_user'] = $form_state['values']['filter_name_user'];
  $form_state['rebuild'] = TRUE;
}

function bbva_vouchers_create_voucher_view($view_vouchers){

  $header = array(
      array('data' => t('id')),
      array('data' => t('Estado')),
      array('data' => t('Id Cupon')),
      array('data' => t('code')),
      array('data' => t('code_promo_visa')),
      array('data' => t('Fecha Descarga')),
      array('data' => t('title')),
      array('data' => t('name_user (Altamira)'))
  );

  $form = array();

  $form['#method'] = 'get';

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buscar por:')
  );
  $form['filter']['filter_id_cupon'] = array(
    '#type' => 'textfield',
    '#title' => t('Id Cupon:'),
    '#size' => 10,
    '#weight' => '0',
    '#default_value'=> $_GET['filter_id_cupon'],
  );
  $form['filter']['filter_state'] = array(
    '#type' => 'textfield',
    '#title' => t('Estado: '),
    '#size' => 10,
    '#weight' => '0',
    '#default_value'=> $_GET['filter_state'],
  );

  $form['filter']['filter_name_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Num. Altamira:'),
    '#size' => 10,
    '#weight' => '0',
    '#default_value'=> $_GET['filter_name_user'],
  );

  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Buscar'),
    '#weight' => '3'
  );

  // Tomamos los datos
  foreach ($view_vouchers as $row) {

    if (($row->fecha_descarga == 0) || ($row->fecha_descarga == '')) {
      $date_sent = "-";
    }else{
      $date_sent = date('d-m-Y', strtotime($row->fecha_descarga));
    }

    $data[] = array(
      array('data' => $row->id, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->state, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->id_cupon, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->code, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->code_promo_visa, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $date_sent, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->title, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->name_user, 'align' => 'center', 'style' => 'font-size: 1em'),
      );
  }

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $data,
    '#empty' => t('No hay vouchers por el momento')
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}
