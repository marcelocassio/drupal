 <?php
//-----------------------------------------------------
// Funciones para los completar los servicios de cupones
//-----------------------------------------------------
# Funciones para la V1
# Funciones para la V2

/*
FUNCIONES PARA LA V1
*/

function _donwload_cupon($id_benefit,$user){

    if ($user->uid==0) {
         return array('vouchers' => array(),'codigo'=>9,'descripcion'=>'No estás identificado');
    }

    if (!array_key_exists('11', $user->roles)) {
        return array('vouchers' => $response,'codigo'=>2,'descripcion'=>'No estás registrado');
    }

    $id_cupon=_get_cupon_with_benefit($id_benefit);
    $voucher_descargado = _voucher_disponible($id_cupon,$user->name);
    if($voucher_descargado){
       return array(
        'vouchers' => array(),
        'codigo'=>4,
        'descripcion'=>'Ya bajaste este cupón. Encontralo en la sección Mis cupones.',
        'id_voucher'=>$voucher_descargado);
    };

    $voucher = download_voucher($id_cupon,$user->name);

    if ($voucher) {

        if (cache_get('cupon'.$id_cupon,'cache')->data) {
            $cupon = cache_get('cupon'.$id_cupon,'cache')->data;
        }else{
            $cupon = _set_cache_coupon($id_cupon);
        }

        $response[]=array(
          'codigo' => $voucher->title,
          'id' => $voucher->id,
          'coupon' =>$cupon);
       return  array('vouchers'=>$response,'codigo'=>0,'descripcion'=>'');
    }

   return array('vouchers' => array(),'codigo'=>1,'descripcion'=>'Cupón agotado');
}

function _get_cuponera_user($user,$state){

  $vouchers = load_vouchers_user_state($user->name,$state);

  if(empty($vouchers)){
    return array();
  }

  $vouchers_cupon = array();
  foreach ($vouchers as $key => $value) {
  if (cache_get('cupon'.$value->id_cupon,'cache')->data) {
      $cupon= cache_get('cupon'.$value->id_cupon,'cache')->data;
  }else{
      $cupon= _set_cache_coupon($value->id_cupon);
  }

 $vouchers_cupon[]=  array(
    'codigo' => $value->title,
    'id' => $value->id,
    'coupon' =>$cupon);
  }

   return array("vouchers"=>$vouchers_cupon);
};

function _get_voucher($id_voucher,$user){

  $voucher = load_voucher_user($id_voucher,$user->name);
  $id_cupon = $voucher->id_cupon;
  if ($voucher) {

    if (cache_get('cupon'.$id_cupon,'cache')->data) {
      $cupon = cache_get('cupon'.$id_cupon,'cache')->data;
    }else{
      $cupon=_set_cache_coupon($id_cupon);
    }

    $responese[] = array('codigo' => $voucher->title,
      'id' => $voucher->id,
      'coupon' =>$cupon);

    return array('vouchers'=>$responese,'codigo'=>0,'descripcion'=>'');
  }

  return array("vouchers"=>array());
};

 /**
  * @deprecated
  */
function _set_cache_coupon($idcupon){
  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
  $cuponImagenes = array();
  $entity = entity_load_single('coupon_promo', $idcupon);

  $wrapperCuponPromo = entity_metadata_wrapper('coupon_promo', $entity);
  $wrapperShop = $wrapperCuponPromo->field_coupon_promo_shop;
  $wrapperBenefit = $wrapperCuponPromo->field_coupon_promo_benefit;

  if (isset($wrapperBenefit->field_benfit_image)) {
    $cuponImagenBenefit = $wrapperBenefit->field_benfit_image->value();
    $cuponImagenBenefitLogo = $wrapperBenefit->field_benefit_logo_img->value();
  }

  $cuponImagenShop = $wrapperShop->field_shop_imagen->value();
  $cuponImagenShopLogo = $wrapperShop->field_shiop_mark_tip->value();
  $categorycup=$wrapperShop->field_shop_category->value();
  $term = taxonomy_term_load($categorycup[0]->tid);

  if (!empty($term->field_category_parent['und'][0]['value'])) {
      $term = $rubros[$term->field_category_parent['und'][0]['value']];
  }

   $shop['rubroImageLogo'] = $term->field_category_logo_web;
   $shop['rubroImage'] = $term->field_category_image;

  if ($cuponImagenBenefit) {
    $cuponImagenes['imagen'] = $cuponImagenBenefit['uri'];
  }elseif ($cuponImagenShop) {
    $cuponImagenes['imagen'] = $cuponImagenShop['uri'];
  }else{
    $cuponImagenes['imagen'] =$shop['rubroImage']['und'][0]['uri'];
  }

  if ($cuponImagenBenefitLogo) {
    $cuponImagenes['logo'] = $cuponImagenBenefitLogo['uri'];
  }

  if ($cuponImagenShopLogo) {
    $cuponImagenes['logo'] = $cuponImagenShopLogo['uri'];
  }

  $cuponImagenes['title'] = $wrapperCuponPromo->field_coupon_promo_shop->label();

  if (!empty($cuponImagenes['logo'])) {
    $cuponImagenes['logo'] =  image_style_url('soloxhoy_small', $cuponImagenes['logo']);
  }
  else {
    $cuponImagenes['logo'] = $cuponImagenes['title'];
  }

  if (!empty($cuponImagenes['imagen'])) {
    $cuponImagenes['imagen'] = image_style_url('app_slide', $cuponImagenes['imagen']);
  }

  $cupon = new stdClass();
  $cupon->discount = $wrapperCuponPromo->field_coupon_promo_discount->value();

  if ($wrapperCuponPromo->field_coupon_promo_top->value()!=0) {
    $cupon->top = 'Sobre la totalidad de la compra. Tope $'. $wrapperCuponPromo->field_coupon_promo_top->value();
  }else {
    $cupon->top = 'Sin tope de descuento';
  }

  $legal = "Promoción Valida desde  ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_start->value());
  $legal .=" hasta ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_end->value());
  $legal .= " ".$wrapperCuponPromo->field_coupon_promo_legal->value();

  $cupon->legal = $legal;
  $cupon->image = $cuponImagenes['imagen'];
  $cupon->imageSmall = $cuponImagenes['logo'];
  $cupon->dateEnd = date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_end->value());
  $cupon->informDate = 'Del '.date('d', $wrapperCuponPromo->field_coupon_promo_date_start->value())." al ".date('d', $wrapperCuponPromo->field_coupon_promo_date_end->value())." de ".$meses[date('n',$wrapperCuponPromo->field_coupon_promo_date_end->value())-1];
  $cupon->descMediosPago = 'Con todos los medios de pago';
  $cupon->benefit_id = $entity->field_coupon_promo_benefit['und'][0]['target_id'];

  cache_set('cupon'.$idcupon,$cupon,'cache',CACHE_PERMANENT);
  return $cupon;
};


function bbva_cupon_descargar_voucher($id_cupon, $user){

  if(!array_key_exists('11', $user->roles)) {
    return array(
      'code_service' => 2,
      'message' => 'No estás registrado'
    );
  }
  $voucher_descargado = _voucher_disponible($id_cupon, $user->name);

  if($voucher_descargado) {
    return array(
      'code_service' => 4,
      'message' => 'Ya bajaste este cupón. Encontralo en la sección Mis cupones.',
      'voucher' => array("id" => $voucher_descargado)
    );
  }
  $voucher = download_voucher($id_cupon, $user->name);
  if ($voucher) {
    $response = array(
      'id' => $voucher->id,
    );
    return array('voucher' => $response);
  }

  return array('code_service' => 1, 'message' => 'Cupón agotado');
}

function bbva_coupon_get_user_vouchers($user, $state) { // Cupones listas

 $vouchers = load_vouchers_user_state($user->name, $state);

 if(empty($vouchers)){
   return array("vouchers" => array());
 }

 $vouchers_cupon = array();

 foreach ($vouchers as $voucher) {

   $cupon = bbva_coupon_consultar_voucher($voucher);

   $vouchers_cupon[] = array(
     'id' => $voucher->id,
     'title' => $cupon->title,
     'discount' => $cupon->discount,
     'inform_date' => $cupon->inform_date
   );
 }

 return array("vouchers" => $vouchers_cupon);
}

function bbva_coupon_obtener_voucher_por_id($id_voucher, $user) { // Detalle cupon
 $voucher = load_voucher_user($id_voucher, $user->name);

 if (isset($voucher->id)) {

   $voucherDetalle = bbva_coupon_consultar_voucher($voucher);
   $voucherDetalle->id_comunicacion = intval($voucherDetalle->id_comunicacion);
   return array('voucher' => $voucherDetalle);
 }

 return array("voucher" => array());
}

function bbva_coupon_consultar_voucher($voucher){

  // Se carga la entidad del cupon
  $entity = entity_load_single('coupon_promo', $voucher->id_cupon);
  $wrapperCuponPromo = entity_metadata_wrapper('coupon_promo', $entity);
  $cupon = new stdClass();

  // Se carga la entidad del beneficio
  $cupon->id = $voucher->id;
  $cupon->title = $wrapperCuponPromo->field_coupon_promo_shop->label();
  $cupon->code_voucher = $voucher->title;
  $promoCuponBo = new \Fgo\Bo\PromoCuponBo();
  $promoEncontrada = $promoCuponBo->buscarPorIdCupon($voucher->id_cupon);
  $cupon->id_comunicacion = isset($promoEncontrada) ? $promoEncontrada->idComunicacion : null;

  $api = new \Fgo\Services\ApiClient();
  $respuestaApiComunicacion = $api->obtenerComunicacion($cupon->id_comunicacion);

  if ($respuestaApiComunicacion["code"] == 0 ) {
    
    $comunicacion = $respuestaApiComunicacion["data"]["beneficio"];

    $cupon->discount = $comunicacion ? $comunicacion->beneficios[0]->valor . "%" : null;
    $cupon->summary = $comunicacion ? $comunicacion->beneficios[0]->valor . "% ". $comunicacion->beneficios[0]->condicion : null;

    if ($wrapperCuponPromo->field_coupon_promo_top->value()!=0) {
      $cupon->top = 'Sobre la totalidad de la compra. Tope $'. $wrapperCuponPromo->field_coupon_promo_top->value();
    }
    else {
      $cupon->top = 'Sin tope de descuento';
    }

    $cupon->inform_date = "Del ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_start->value()). " al ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_end->value());

    $cupon->where["branches"] = $comunicacion ? $comunicacion->canales_venta->sucursales : null;
    $cupon->where["web"] = $comunicacion ? $comunicacion->canales_venta->web : null;
    $cupon->how_to = isset($comunicacion->como_uso) ? $comunicacion->como_uso : null;
  }

 $bases_condiciones = "Promoción Valida desde ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_start->value());
 $bases_condiciones .=" hasta ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_end->value());
 $bases_condiciones .= " ".$wrapperCuponPromo->field_coupon_promo_legal->value();

 $cupon->term_conditions = trim($bases_condiciones);

 return $cupon;
};

 /**
  * @deprecated
  * */
 function bbva_cupon_descargar_cupon($id_cupon, $user){

   if(!array_key_exists('11', $user->roles)) {
     return array(
       'code_service' => 2,
       'message' => 'No estás registrado'
     );
   }
   $voucher_descargado = _voucher_disponible($id_cupon, $user->name);

   if($voucher_descargado) {
     return array(
       'code_service' => 4,
       'message' => 'Ya bajaste este cupón. Encontralo en la sección Mis cupones.',
       'voucher' => array("id" => $voucher_descargado)
     );
   }

   $voucher = download_voucher($id_cupon, $user->name);


   if($voucher) {
     $cupon_temp = cache_get('cupon'.$voucher->id.'_v2','cache');
     if(isset($cupon_temp->data)) {
       $cupon = $cupon_temp->data;
     } else {
       $cupon = bbva_coupon_set_cache_coupon_v2($voucher);
     }

     $response = array(
       'id' => $voucher->id,
     );

     return array('voucher' => $response);
   }

   return array('code_service' => 1, 'message' => 'Cupón agotado');
 }

 /**
  * @deprecated
  */
 function bbva_coupon_get_user_vouchers_v2($user, $state) { // Cupones listas
   $vouchers = load_vouchers_user_state($user->name, $state);
   if(empty($vouchers)){
     return array("vouchers" => array());
   }

   $vouchers_cupon = array();

   foreach ($vouchers as $voucher) {
     $cupon_temp = cache_get('cupon'.$voucher->id.'_v2','cache');
     if(isset($cupon_temp->data)) {
       $cupon = $cupon_temp->data;
     } else {
       $cupon = bbva_coupon_set_cache_coupon_v2($voucher);
     }

     $vouchers_cupon[] = array(
       'id' => $voucher->id,
       'title' =>$cupon->title,
       'discount' =>$cupon->discount,
       'inform_date' =>$cupon->inform_date
     );
   }

   return array("vouchers" => $vouchers_cupon);
 }

 /**
  * @deprecated
  */
function _get_voucher_with_id($id_voucher, $user) { // Detalle cupon
  $voucher = load_voucher_user($id_voucher, $user->name);

  if (isset($voucher->id)) {
    $cupon_temp = cache_get('cupon'.$voucher->id.'_v2','cache');
    if(isset($cupon_temp->data)) {
      $cupon = $cupon_temp->data;
    } else {
      $cupon = bbva_coupon_set_cache_coupon_v2($voucher);
    }

    return array('voucher' => $cupon);
  }

  return array("voucher" => array());
}

 /**
  * @deprecated
  */
function bbva_coupon_set_cache_coupon_v2($voucher){
  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

  // Se carga la entidad del cupon
  $entity = entity_load_single('coupon_promo', $voucher->id_cupon);
  $wrapperCuponPromo = entity_metadata_wrapper('coupon_promo', $entity);
  $cupon = new stdClass();

  // Se carga la entidad del beneficio
  $wrapper_beneficio =_api_beneficio_puntual($entity->field_coupon_promo_benefit['und'][0]['target_id']);

  /* ////////////////////// */
  $cupon->id = $voucher->id;
  $cupon->title = $wrapperCuponPromo->field_coupon_promo_shop->label();
  $cupon->code_voucher = $voucher->title;
  $cupon->id_benefit = (int)$entity->field_coupon_promo_benefit['und'][0]['target_id'];

  if ($wrapper_beneficio["code"] == 0) {
    $data_beneficio = $wrapper_beneficio["data"]["beneficio"];

    $cupon->discount = $data_beneficio->beneficios_valores[0]->texto_grande;
    $cupon->summary = $data_beneficio->beneficios_valores[0]->texto;

    if ($wrapperCuponPromo->field_coupon_promo_top->value()!=0) {
      $cupon->top = 'Sobre la totalidad de la compra. Tope $'. $wrapperCuponPromo->field_coupon_promo_top->value();
    }else {
      $cupon->top = 'Sin tope de descuento';
    }

    $cupon->inform_date = "Del ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_start->value()). " al ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_end->value());

    $cupon->where["branches"] = $data_beneficio->canales_venta->sucursales;
    $cupon->where["web"] = $data_beneficio->canales_venta->web;
    $cupon->how_to = $data_beneficio->como_uso;
  }

  $wrapperShop = $wrapperCuponPromo->field_coupon_promo_shop;
  $wrapperBenefit = $wrapperCuponPromo->field_coupon_promo_benefit;

  $bases_condiciones = "Promoción Valida desde ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_start->value());
  $bases_condiciones .=" hasta ".date('d/m/Y', $wrapperCuponPromo->field_coupon_promo_date_end->value());
  $bases_condiciones .= " ".$wrapperCuponPromo->field_coupon_promo_legal->value();

  $cupon->term_conditions = trim($bases_condiciones);

  cache_set('cupon'.$voucher->id_cupon.'_v2',$cupon,'cache',CACHE_PERMANENT);
  return $cupon;
};

function bbva_cuopon_promo_numero_en_uso($promo_numero_tid, $fechas) {
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'coupon_promo')
    ->entityCondition('bundle', 'coupon_promo')
    ->fieldCondition('field_coupon_promo_numero', 'tid', $promo_numero_tid)
    ->fieldCondition('field_coupon_promo_date_end', 'value', $fechas['value'], '>=')
    ->fieldCondition('field_coupon_promo_date_start', 'value', $fechas['value2'], '<=')
    ->fieldCondition('field_coupon_state_shipping', 'value', array(0, 1, 2), 'IN');

  $result = $query->execute();

  return $result;
}

function bbva_cuopon_promo_numero_suip_en_uso($promo_numero_tid, $id_suip) {

  $query = new EntityFieldQuery();

  $query
    ->entityCondition('entity_type', 'coupon_promo')
    ->entityCondition('bundle', 'coupon_promo')
    ->fieldCondition('field_coupon_promo_numero', 'tid', $promo_numero_tid)
    ->fieldCondition('field_coupon_promo_promo_suip', 'value', $id_suip);

  return $query->execute();
}

function bbva_coupon_validar_coupon_promo(&$form, &$form_state) {
  if($form_state['values']['field_coupon_type']['und'][0]['value'] == 0) {
    $promo_numero_tid = $form_state['values']['field_coupon_promo_numero']['und'][0]['tid'];
    $fecha_vigencia = array();

    $date = DateTime::createFromFormat('Y-m-d H:i:s', $form_state['values']['field_coupon_promo_date_start']['und'][0]['value'] . " 00:00:00");
    if(!$date) {
      $date = new DateTime();
      $date->setTimestamp($form_state['values']['field_coupon_promo_date_start']['und'][0]['value']);
    }
    $fecha_vigencia['value'] = $date->getTimestamp();

    $date = DateTime::createFromFormat('Y-m-d H:i:s', $form_state['values']['field_coupon_promo_date_end']['und'][0]['value'] . " 00:00:00");
    if(!$date) {
      $date = new DateTime();
      $date->setTimestamp($form_state['values']['field_coupon_promo_date_end']['und'][0]['value']);
    }
    $fecha_vigencia['value2'] = $date->getTimestamp();

    $cupones_en_uso_con_ese_promo_numero = bbva_cuopon_promo_numero_en_uso($promo_numero_tid, $fecha_vigencia);
    if(count($cupones_en_uso_con_ese_promo_numero) > 0) {
      form_set_error('field_coupon_promo_numero', 'El promo numero ya se encuentra reservado en la fecha de vigencia.');
    }

    $comercio = $form_state["input"]["id_comercio_custom"];
    $validacion_comercio = preg_match("/.+\(([0-9]+)\)$/", $comercio.trim(), $matches);
    if ($validacion_comercio  == 0) {
      form_set_error('id_comercio_custom', 'El comercio cargado no exite ó no esta correctamente cargado.');
    } else if ($validacion_comercio  == 1) {
        $idComercio = $matches[1];
        $comercioBo  = new \Fgo\Bo\ComercioBo($idComercio);
        if ($comercioBo->idComercio == null) {
          form_set_error('id_comercio_custom', 'El comercio cargado no exite');
        }
    }
  }
}

  /**
   * Crea un cupon desde los parametros de SUIP en Drupal
   *
  **/  
  function bbva_coupon_crear_coupon_promo($cuponSuip, $promoNumeroTid, $targetIdShop) {
  
    $fecha_desde = \DateTime::createFromFormat("Y-m-d", $cuponSuip['fechaDesdePromocion']);
    $fecha_hasta = \DateTime::createFromFormat("Y-m-d", $cuponSuip['fechaHastaPromocion']);

    $entity_cupon = entity_create('coupon_promo', array('type' => 'coupon_promo'));
    $entity_cupon->title = "Cupon Suip - " .$cuponSuip['denominacionPromocion'];
    $entity_cupon->field_coupon_promo_numero['und'][0]['tid'] = $promoNumeroTid;
    $entity_cupon->field_coupon_promo_shop['und'][0]['target_id'] = $targetIdShop;

    $entity_cupon->field_coupon_promo_discount['und'][0]['value'] = $cuponSuip['descuentoCliente'];
    $entity_cupon->field_coupon_promo_top['und'][0]['value'] = $cuponSuip['montoTope'];
    $entity_cupon->field_coupon_promo_lquantity['und'][0]['value'] = $cuponSuip['cantidadCupon'];
    $entity_cupon->field_coupon_promo_date_start['und'][0]['value'] =  $fecha_desde->getTimestamp();
    $entity_cupon->field_coupon_promo_date_end['und'][0]['value'] =  $fecha_hasta->getTimestamp();

    $entity_cupon->field_coupon_promo_target['und'][0]['value'] = 1;
    $entity_cupon->field_coupon_promo_segments_user['und'][0]['value'] = 3; // Todos los suscriptos
    $entity_cupon->field_coupon_promo_promo_suip['und'][0]['value'] = $cuponSuip['idPromocion']; //cambio

    $entity_cupon->field_coupon_type['und'][0]['value'] = 0;
    $entity_cupon->field_coupon_promo_benefit_asoc['und'][0]['value'] = 0; // No debe vincular ningun beneficio
    $entity_cupon->field_coupon_state_shipping['und'][0]['value'] = 0; // Pendiente de envio
    $entity_cupon->save();

    return $entity_cupon;
  }

function _get_cupones_suip() {
  $cupones = array();
  $nums_of_attempts = 3;
  $attempts = 0;

  do {
    try {
      $cs = \CuponesServiceSuip::getInstance();
      $cupones = $cs->getPromoCuponAProcesarAlta();
      return $cupones;
    } catch (Exception $e) {
      $attempts++;
      sleep(5);
      continue;
    }

    break;
  
  } while($attempts < $nums_of_attempts);

  return $cupones;
}

function bbva_coupon_autocomplete_comercio($nombre) {
  $matches = array();
  if (module_exists('fgo_core')) {
    if ($nombre && strlen($nombre) >= 3) {
      $bo = new \Fgo\Bo\ComercioBo();
      $comercios = $bo->listarConcidenciasPorNombre($nombre);
      foreach ($comercios as $comercio) {
        $matches[$comercio->nombre . " (" . $comercio->idComercio . ")"] = check_plain($comercio->nombre . " id:" . $comercio->idComercio . " id suip:" . $comercio->idComercioSuip);
      }
    }
  }

  drupal_json_output($matches);
}


/**
 * Funcion que obtiene el tid del codigo de promo numero del cupon
 * Si existe devuelve el existente si no lo crea
*/
function _get_or_create_promo_numero($code = null){

  if(isset($code)){
    $tid = _get_promo_numero_tid($code);
    
    if($tid){
      return $tid;
    }
      
    $term = new stdClass();
    $term->vid = 21;
    $term->name = $code;
    $term->field_coupon_number_promo['und'][0]['value'] = $code;

    taxonomy_term_save($term);

    return $term->tid;
  }

  return null;
}

  /**
  * Crea una comunicacion en base a la entidad promo cupon.
  * @param $promoCupon Entity Wapper de promo_coupon y beneficio asociado
  * @param $id_beneficio_suip Id de suip ficticio
  * @return mixed
  * @throws Exception
  */
function bbva_coupon_crear_comunicacion($promoCupon, $id_beneficio_suip) {
  if (module_exists('fgo_core')) {

    $bo = bbva_coupon_armar_comunicacion_cupon($promoCupon);

    if ($bo->idComunicacion != null) {
      $promoCuponBo = new \Fgo\Bo\PromoCuponBo();
      $promoEncontrada = $promoCuponBo->buscarPorIdCupon($promoCupon->getIdentifier());
      if ($promoEncontrada != null) {
        $promoEncontrada->idComunicacion = $bo->idComunicacion;
        $promoEncontrada->guardar();
      } else {
        throw new Exception("No se encontro la promo " . $promoCupon->getIdentifier());
      }
    }

    if (isset($id_beneficio_suip)) {
      $comunicacion = new \Fgo\Bo\ComunicacionBo($bo->idComunicacion);
      $comunicacion->idBeneficioSuip = $id_beneficio_suip;
      $comunicacion->guardar();
    } else {
      throw new Exception('No se recibio ningun id de suip para la comunicacion de cupon');
    }

    return $bo->idComunicacion;
  }
}

 /**
  * Arma una comunicacion en base al cupon
  * @param $promoCupon
  * @return \Fgo\Bo\ComunicacionBo
  */
function bbva_coupon_armar_comunicacion_cupon($promoCupon) {
  $comunicacionBo = new \Fgo\Bo\ComunicacionBo();

  $comunicacionBo->fechaDesde = $promoCupon->field_coupon_promo_date_start->value();
  $comunicacionBo->fechaHasta = $promoCupon->field_coupon_promo_date_end->value();
  $comunicacionBo->destacado = ($promoCupon->field_coupon_destacado->value() == 1)?1:0;
  $comunicacionBo->terminosCondiciones = $promoCupon->field_coupon_promo_legal->value();
  $comunicacionBo->descripcionCorta = $promoCupon->field_coupon_promo_subtitle->value();
  $comunicacionBo->cft = (float)0;
  $comunicacionBo->publicado = 1;
  $comercio = \Fgo\Bo\PromoCuponBo::buscarComercioPorIdCupon($promoCupon->getIdentifier());
  if ($comercio != null) {
    $comunicacionBo->titulo = $comercio->nombre;
  }

  $comunicacionBo->guardar();

  $beneficioBo = new \Fgo\Bo\BeneficioBo();
  $beneficioBo->idComunicacion = $comunicacionBo->idComunicacion;
  $beneficioBo->tipoBeneficio = \Fgo\Bo\BeneficioBo::TIPO_BENEFICIO_CUPON;
  $beneficioBo->valor = $promoCupon->field_coupon_promo_discount->value();
  $beneficioBo->tope = $promoCupon->field_coupon_promo_top->value();
  $beneficioBo->guardar();

  $ultimaComunicacion = $comunicacionBo->buscarUltimaComunicacionPorComercio($comercio->idComercio);

  if (count($ultimaComunicacion->sucursalesFisicas) > 0) {
    foreach ($ultimaComunicacion->sucursalesFisicas as $sucursal) {
      $comunicacionSucursal = new \Fgo\Bo\ComunicacionSucursalFisicaBo();
      $comunicacionSucursal->idComunicacion = $comunicacionBo->idComunicacion;
      $comunicacionSucursal->idSucursalFisica = $sucursal->idSucursalFisica;
      $comunicacionSucursal->guardar();
    }
  }

  if (count($ultimaComunicacion->sucursalesVirtuales) > 0) {
    foreach ($ultimaComunicacion->sucursalesVirtuales as $sucursal) {
      $comunicacionSucursal = new \Fgo\Bo\ComunicacionSucursalVirtualBo();
      $comunicacionSucursal->idComunicacion = $comunicacionBo->idComunicacion;
      $comunicacionSucursal->idSucursalVirtual = $sucursal->idSucursalVirtual;
      $comunicacionSucursal->guardar();
    }
  }

  return $comunicacionBo;
}

 /**
  * Crea una promociones cupon relacionando con el comercio para el nuevo core.
  * @param $coupon_entity
  * @param $idBeneficioSuip Id de beneficio de SUIP para relacionar
  * @param $idComercioSuip Id de comercio de SUIP para relacionar
  * @throws Exception
  * @return \Fgo\Bo\PromoCuponBo
  */
function bbva_coupon_crear_promo_cupon($coupon_entity, $idComercioSuip ,$idBeneficioSuip) {

  if (module_exists('fgo_core')) {
    try {
      $comercioBo = new \Fgo\Bo\ComercioBo();
      $comercioEncontrado = $comercioBo->buscarPorIdComercioSuip($idComercioSuip);

      if($comercioEncontrado != null && $comercioEncontrado->idComercio != null) {
        $promoCuponBo = new \Fgo\Bo\PromoCuponBo();
        $promoCuponBo->idCupon = $coupon_entity->id;
        $promoCuponBo->idComercio = $comercioEncontrado->idComercio;
        $promoCuponBo->idBeneficioSuip = $idBeneficioSuip;
        $promoCuponBo->guardar();
      } else {
        throw new Exception('No se encontro el comercio para generar la PromoCuponBo');
      }
      return $promoCuponBo;
    } catch (\Fgo\Bo\DatosInvalidosException $e) {
      $errores = $e->getErrores();
      $mensaje = 'NO SE PUDO GUARDAR el beneficio - ID Cupon entity: ' . $coupon_entity->id . ' Error: ' . implode($errores);
      echo "Error " . $mensaje;
    } catch (Exception $e) {
      echo "Error en cupon: " . $e->getMessage() . PHP_EOL;
    }
  }
}

 /***
  * Asigna un beneficio suip a un promo cupon existente del nuevo core.
  * @param $id_coupon_drupal
  * @param $idComunicacionSuip
  * @throws Exception
  */
function bbva_coupon_relacionar_promo_cupon($id_coupon_drupal, $idComunicacionSuip) {

  echoline("bbva_coupon_relacionar_promo_cupon", false, true);
  
  if (module_exists('fgo_core')) {
    $comunicacionBo = new \Fgo\Bo\ComunicacionBo();
    $comunicacionEncontrada = $comunicacionBo->buscarPorIdSuip($idComunicacionSuip);

    if (isset($comunicacionEncontrada) && $comunicacionEncontrada->idComunicacion != null) {
      $cuponPromoBo = new \Fgo\Bo\PromoCuponBo();
      $cuponPromoEncontrado = $cuponPromoBo->buscarPorIdCupon($id_coupon_drupal);

      if (isset($cuponPromoEncontrado) && $cuponPromoEncontrado->idPromoCupon != null) {
        $cuponPromoEncontrado->idComunicacion = $comunicacionEncontrada->idComunicacion;
        $cuponPromoEncontrado->guardar();
      } else {
        throw new Exception('No se encontro la promo con id coupon_promo entity:'. $id_coupon_drupal);
      }
    } else {
      throw new Exception('No se encontro la comunicación para relacionar la PromoCuponBo');
    }
  }
}

 /***
  * Genera un id ficticio de suip
  * @param $id_benefit
  * @return int
  */
function _generar_id_suip_ficticio($id_benefit) {
  $benefit = entity_metadata_wrapper('benefit', $id_benefit);
  $id_beneficio_suip = $benefit->field_benefit_id->value();
  
  if (!isset($id_beneficio_suip)) {
    $id_beneficio_suip = (int) $id_benefit;
    $id_beneficio_suip += 1000000;
    $benefit->field_benefit_id->set($id_beneficio_suip);
    $benefit->save();
  }

  return $id_beneficio_suip;
}
