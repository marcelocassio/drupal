<?php
/**
 * The batch processor.
 */
// bcpnew -> CuponesGeneracion.sh
function bbva_coupon_promotion_process($data, &$context) {
  
  hd('bbva_coupon_promotion_process');

  // Abro los archivos a escribir
  $coupon_promo_visa_file = fopen("/prod/jp00/cupones/enviar/coupon_promo_visa.txt", "wb");
  $vouchers_promo_visa_file = fopen("/prod/jp00/cupones/enviar/vouchers_promo_visa.txt", "wb");
  
  // Agrego los cupones de SUIP
  $cuponesSuip = _get_cupones_suip();
  
  if(count($cuponesSuip) > 0) {
    echo "\n";
    foreach ($cuponesSuip as $cuponSuip) {


      $idComercioSuip = $cuponSuip['idComercio'];
      $idBeneficioSuip = $cuponSuip['idComunicacion'];
      $idPromocionSuip = $cuponSuip['idPromocion'];
      
      echoline(" Se va a procesar:  idComercioSuip:". $idComercioSuip. " - idBeneficioSuip:".$idBeneficioSuip. " - idPromocionSuip:".$idPromocionSuip, true, true);

      if(isset($idBeneficioSuip['@attributes']['xsi_nil'])) {
        echoline(" PROMO NO GENERADA. La comunicacion aun no fue creada para la promo suip ".$idPromocionSuip, true, true);
        continue;
      }

      try {
        $promoNumeroTid = _get_or_create_promo_numero($cuponSuip['codigoPromoCupon']);

        if(!bbva_cuopon_promo_numero_suip_en_uso($promoNumeroTid, $idComercioSuip)) {
          $target_id_shop = _bbva_coupon_get_id($idComercioSuip);
          
          if($target_id_shop == 0) {
            echoline('No se encontro el id de comercio suip: '.$target_id_shop.' cargado en drupal', true, true);
            continue;
          }

          if(_bbva_coupon_no_existe_promo_suip($idPromocionSuip)) {

            $coupon_entity = bbva_coupon_crear_coupon_promo($cuponSuip, $promoNumeroTid, $target_id_shop);

            // Creamos cupon realicionado con comunicaciones del nuevo Core
            bbva_coupon_crear_promo_cupon($coupon_entity, $idComercioSuip, $idBeneficioSuip);
          }
        }
       } catch (Exception $e) {
        echo "Error al procesar PROMOS de SUIP: " . $e->getMessage() . PHP_EOL;
        echo "STACK: " . $e->getTraceAsString() . PHP_EOL;
      }
    }
  }

  //Traigo todas las promos pendiente de enviar a VISA
  $cuponPoromoPending = _get_coupons_promo(CUPON_ESTADO_PENDIENTE);

  // Limpio el cache de cupones
  borrarCache('coupon_promo');
  
  //Recorremos todas las promociones pendientes y realizamos los procesos
  foreach ($cuponPoromoPending as $key => $value) {

    $cupon_promo = entity_metadata_wrapper('coupon_promo', entity_load_single('coupon_promo', $value->id));

    //Extracion de datos necesarios
    $context['message'] = "\n PROCESANDO ... ".$cupon_promo->label() . " ".date('Y-m-d H:i:s');

    $promo_type = $cupon_promo->field_coupon_type->value();
    $shop = $cupon_promo->field_coupon_promo_shop->value();
    $descuento = $cupon_promo->field_coupon_promo_discount->value();
    $tope = $cupon_promo->field_coupon_promo_top->value();
    $fecha_desde = $cupon_promo->field_coupon_promo_date_start->value();
    $fecha_end = $cupon_promo->field_coupon_promo_date_end->value();
    $promo_numero = $cupon_promo->field_coupon_promo_numero->value();
    $segment_users = $cupon_promo->field_coupon_promo_segments_user->value();
    $cant_vocuhers = $cupon_promo->field_coupon_promo_lquantity->value();
    $promo_numero_tid = $cupon_promo->field_coupon_promo_numero->getIdentifier();
    $promo_tid = $cupon_promo->getIdentifier();
    $promo_target = $cupon_promo->field_coupon_promo_target->value();
    $segmentado = false;
    $cupon_voucherFile = $cupon_promo->field_coupon_vochers_file->value();
    $promo_userFile = $cupon_promo->field_coupon_promo_users_file->value();

    $users = array();
    
    // Detecto si es un cupon segmentado o no
    if ($promo_target === '0' || $segment_users[0] != '3') {
      
      $context['message'] = "\n Es cupon por segmento".$cupon_promo->label()." ".date('Y-m-d H:i:s');

      $users = _get_users_cupon($promo_target, $promo_userFile, $segment_users);
      $segmentado = true;

      // Si es segmentado la cantidad de vouchers a generar es igual a la cantidad de usuarios para no generar de mas
      $cant_vocuhers = sizeof($users);
    }
        
    $context['message'] = "\n OBTENGO CODIGOS ".$cupon_promo->label()." ".date('Y-m-d H:i:s');

    // Obtengo los codigos
    $codes_vouchers = _generate_codes_vouchers($cant_vocuhers, $promo_numero, $promo_type, $segmentado, $cupon_voucherFile['uri']);

    // Genero vouchers de la promo
    $context['message'] = "\n PROCESANDO ... ".$cupon_promo->label()." PROCESANDO VOUCHERS ".date('Y-m-d H:i:s');

    // genera el archivo de vouchers si es de VISA
    _process_cupon_promo_vouchers($vouchers_promo_visa_file, $codes_vouchers, $promo_type, $promo_numero_tid, $promo_tid, $fecha_end, $segmentado, $users, $promo_numero->field_coupon_number_promo['und'][0]['value']);

    // veo si es una promocion ya enviada o no dependiendo eso cambia el estado de la promo a enviar a visa  y realizo las actulizaciones necesarias
    if($promo_numero->field_coupon_number_state_send['und'][0]['value'] === '1') {
      $estado_promo_visa = 'UP';
    }
    else{
      $estado_promo_visa = 'IN';
      $promo_numero->field_coupon_number_state_send['und'][0]['value'] = '1';
      taxonomy_term_save($promo_numero);
    }

    // si es por VISA escribo el archivo y lo pongo como enviado
    if ($promo_type === CUPON_PROMO_TYPE_VISA || $promo_type === CUPON_PROMO_TYPE_EVENTO_SIN_ARCHIVO) {
      
       // genera el archivo de CUPONES si es de VISA
      _escribir_archivo_cupon_promo_visa($coupon_promo_visa_file, $estado_promo_visa, $promo_numero->field_coupon_number_promo['und'][0]['value'], $shop->id, $fecha_desde, $fecha_end, $shop->title, $descuento, $tope);
          
      $cupon_promo->field_coupon_state_shipping->set(CUPON_ESTADO_ENVIADO);
    }
    else{
      $cupon_promo->field_coupon_state_shipping->set(CUPON_ESTADO_APROBADO);

      //genero el beneficio
      $id_benefit = _generate_benefit_cupon_promo(
        $promo_tid,
        $fecha_desde,
        $fecha_end,
        $cupon_promo->field_coupon_promo_msg_web_app->value(),
        $cupon_promo->field_coupon_promo_legal->value(),
        $segmentado || $promo_type === CUPON_PROMO_TYPE_EVENTO_CON_ARCHIVO,
        $shop->id,
        $cupon_promo->field_coupon_destacado->value(),
        $cupon_promo->field_coupon_promo_subtitle->value()
      );

      $id_beneficio_suip = _generar_id_suip_ficticio($id_benefit);
      
      try {
        bbva_coupon_crear_comunicacion($cupon_promo, $id_beneficio_suip);
      } 
      catch (Exception $e) {
        hd($e->getMessage());
        echo "Error en cupon: " . $e->getMessage() . PHP_EOL;
      }
      $cupon_promo->field_coupon_promo_benefit->set($id_benefit);
    }

    $cupon_promo->save();
    
  }

  fclose($coupon_promo_visa_file);
  fclose($vouchers_promo_visa_file);
}

function _process_cupon_promo_vouchers($vouchers_promo_visa_file, $codes_vouchers, $promo_type, $promo_numero_tid, $promo_tid, $fecha_end, $segmentado, $users, $promo_numero_code) {

  // Abro el archivo para escribir las promociones de visa
  foreach ($codes_vouchers as $key => $value) {

    $user = null;
    $title = $value['title'];
    $code = $value['code'];

    $voucher = new Voucher();
    $voucher->id_cupon = $promo_tid;
    $voucher->code_promo_visa = $promo_numero_code;
    $voucher->code = $code;

    if($segmentado){

      if (array_key_exists($key, $users)) {
        $user = $users[$key];
        $voucher->name_user = $user;
      }
    }

    switch ($promo_type) {
      case CUPON_PROMO_TYPE_VISA:
      case CUPON_PROMO_TYPE_EVENTO_SIN_ARCHIVO:
        
      $voucher->state = $voucher->getEstadoEnviado();
      $voucher->num_bin_visa = NUM_BIN_VISA;
      $voucher->title = NUM_BIN_VISA." - ".$promo_numero_code." - ".$voucher->code;
      $voucher->create();

      _escribir_archivo_voucher_visa($vouchers_promo_visa_file, $promo_numero_code, $code, $fecha_end);
      break;

      case CUPON_PROMO_TYPE_ARCHIVO:
      case CUPON_PROMO_TYPE_FARMACITY:

      $voucher->state = $voucher->getEstadoDisponible();
      $voucher->num_bin_visa = 0;
      $voucher->title = $voucher->code;
      $voucher->create();

      break;
      case CUPON_PROMO_TYPE_EVENTO_CON_ARCHIVO:

      $voucher->state = ESTADO_POR_EVENTO;
      $voucher->num_bin_visa = 0;
      $voucher->title = $voucher->code;
      $voucher->create();
    }
  }
}

function _generate_codes_vouchers($cant_vouchers, $promo_numero, $promo_type, $segmentado, $voucher_file) {

    // Traigo los cupones dados de alta para la promocion  para verificar la duplicacion
    $codes_vouchers = array();
    $codes_cupon_use = _get_code_voucher_promo_usado($promo_numero->tid);

    // Verifico si vamos a auto-generar los vouchers o lo tenemos que leer desde el archivo
    if ($promo_type === CUPON_PROMO_TYPE_ARCHIVO || $promo_type === CUPON_PROMO_TYPE_EVENTO_CON_ARCHIVO) {

      // traigo los vouchers del archivo
      $vouchers_file = _get_voucher_form_file($voucher_file);
      $codes_cupon_use = array_flip($codes_cupon_use);

      // si no es segmentado tomamos todos los cupones del arvhivo;
      if (!$segmentado) {
        $cant_vouchers = sizeof($vouchers_file);
      }
    }

    // Farmacity (web service)
    if ($promo_type === CUPON_PROMO_TYPE_FARMACITY) {
      try {
        $farmacity = \Services\BBVA\Farmacity::getInstance();
        $codes_vouchers = $farmacity->obtenerVouchers();
        $cant_vouchers = 0; // Evito el bucle
      }
      catch (Exception $e) {
        hd($e->getMessage());
        return array();
      }
    }

    for ($i = 0; $i < $cant_vouchers; $i++) {

      // Me fijo que tipo de promo es
      if ($promo_type === CUPON_PROMO_TYPE_VISA || $promo_type === CUPON_PROMO_TYPE_EVENTO_SIN_ARCHIVO) {

        // Si no es por archivo genero un random hasta encontrar alguno disponible
        $random = getRandomNumber();

        // Genero randoms hasta encontrar uno que no se este usando
        while (array_key_exists($random, $codes_cupon_use)) {
          $random = getRandomNumber();
        }

        $codes_vouchers[] = array(
          'code' => $random, 
          'title' => "505893"." - ".$promo_numero->field_coupon_number_promo['und'][0]['value']." - ".$random
        );
        
        $codes_cupon_use[$random] = $random;
      }
      else {

        // POR archivo obtengo un codigo del archivo reviso que no este usado en alguna promo anterior y lo guardo
        if (!array_key_exists($vouchers_file[$i], $codes_cupon_use)) {
          $codes_vouchers[] = array(
            'code' => $vouchers_file[$i], 
            'title' => $vouchers_file[$i]
          );
          $codes_cupon_use[] = $vouchers_file[$i];
        }
      }
    }

    return $codes_vouchers;
}

function _get_voucher_form_file($uri_file = null) {
  
  ini_set("auto_detect_line_endings", true);
  $vouchers_file = array();
  $handle = fopen($uri_file, 'rb');
  $ivou = 0;
  while (($datos = fgetcsv($handle, 1000, ";")) !== FALSE) {
    if ($ivou != 0) {
      $vouchers_file[] = $datos[0];
    }
    $ivou++;
  }

  return $vouchers_file;
}

function _get_users_cupon($target, $fileUsers, $segmentUser) {

  if ($target === '0') {
    return _get_list_num_altm_form_file($fileUsers['uri']);
  }
  else {
    switch ($segmentUser[0]) {
  
      // Clientes Sin tarjeta
      case '0':
      return _get_user_name(null, '1', '0');
      break;
  
      //1|Clientes con  tarjeta Crédito
      case '1':
      return _get_user_name(null, '1', '1');
      break;
  
      //2|No Clientes
      case '2':
      return _get_user_name(null, '0', '0');
      break;
  
      default:
      return array();
      break;
    }
  }
}

function _alta_voucher($title = null, $state = null, $uid = null, $code = null, $promo_numero_tid = null, $promo_tid = null) {
    $entity = entity_create('voucher', array('type' => 'voucher'));
    $entity->title = $title;
    $entity->field_voucher_state['und']['0']['value'] = $state;
    if (isset($uid)) {
        $entity->field_voucher_user['und']['0']['target_id'] = $uid;
    }
    $entity->field_voucher_coupon_promo['und']['0']['target_id'] = $promo_tid;
    $entity->field_voucher_code['und']['0']['value'] = substr($code, 0, 6);
    $entity->field_voucher_promo_visa['und']['0']['tid'] = $promo_numero_tid;
    entity_save($entity_type, $entity);
}

function _escribir_archivo_voucher_visa($file, $promo_numero_visa, $code, $fecha_hasta) {

    // Generamos lineas para el archivo Voucher
    // NUMCUP: Num Bim Visa + las proximas 4 con el numero de la taxonomia relacionada al cupon_promo + las ultimas 6 con el campo de la entidad voucher generada  + 3 espacios en blanco

    $content = NUM_BIM_VISA;
    $content.= zerofill(substr($promo_numero_visa, 0, 4), 4);
    $content.= $code;
    $content.= '   ';

    //LONG-NUMCUP
    $content.= '  ';

    //ESTADO
    $content.= '20';

    //LIM-COMPRA
    $content.= '000000001';

    //LIM-ADELANTO
    $content.= '000000000';

    //LIM-CUOTAS
    $content.= '000000000';

    //FECHA
    $content.= '         ';

    //CUENTA-ASOC
    $content.= '000000000000000';

    //LIM-PROP
    $content.= ' ';

    //AAMM-VTO: Informar el campo data end de la entity coupon promocion
    $content.= date('ym', $fecha_hasta);

    //FILLER
    $content.= ' ';
    fwrite($file, $content . PHP_EOL);
}

function _escribir_archivo_cupon_promo_visa($file, $state_visa, $promo_numero_visa = null, $id_comercio = null, $fecha_desde = null, $fecha_hasta = null, $nombre_estab = null, $descuento, $tope) {

    $content = $state_visa;

    $content.= '   ';

    // //NUMEST2: Entity relacionado shop  el field id
    $content.= zerofill(substr($id_comercio, 0, 8), 8) . substr($promo_numero_visa, 0, 4);

    // //NOMBRE-ESTAB: Informar el title de la entitiy relacionada shop
    $content.= zerofill_espacio(strtoupper(substr($nombre_estab, 0, 40)), 40);

    // //solo los primeros 40 caracteres
    // //FECHA-DESDE: Informar el campo data start coupon promotion
    $content.= date('ymd', $fecha_desde);

    // //FECHA-HASTA: Informar el campo data end de la entity coupon promotion
    $content.= date('ymd', $fecha_hasta);

    // //FILLER: Tres espacios en blanco
    $content.= '   ';

    // //PAIS: Dos espacios en blanco
    $content.= '  ';

    // //RUBRO-NAC: Cuatro espacios en blanco
    $content.= '    ';

    // //RUBRO-INTER: Cuatro espacios en blanco
    $content.= '    ';

    // //MONEDA: Tres espacios en blanco
    $content.= '   ';

    // //ESTADO: Siempre 30 (Comercio normal)
    $content.= '30';

    // //PROVINCIA: Dos espacios en blanco
    $content.= '  ';

    // //BANCO: Tres espacios en blanco
    $content.= '   ';

    // //SUCURSAL: Tres espacios en blanco
    $content.= '   ';

    // //DESCUENTO: Informar el campo discount de la entity coupon promotion
    if ($descuento == '100') {
      $descuento = '00';
    }

    $content.= zerofill(substr($descuento, 0, 2), 2);

    // //FILLER: Siete espacios en blanco
    $content.= '       ';

    // //MAX-CUOTAS: Dos espacios en blanco
    $content.= '  ';

    // //TIPO-PLAN: Un espacio en blanco
    $content.= ' ';

    // //FILLER: Cuatro espacios en blanco
    $content.= '    ';

    // //GRUPO-CERR: Un espacio en blanco
    $content.= ' ';

    // // //TOPE: Informar el campo tope de la entity coupon promotion
    $content.= zerofill(substr($tope, 0, 5), 5);

    // // //FILLER: Cuatro espacios en blanco
    $content.= '      ';

    // //SALTO DE CARRO
    fwrite($file, $content . PHP_EOL);
}

function _generate_benefit_cupon_promo($id_promo, $fecha_desde, $fecha_hasta, $descripcion_benefit, $legal, $segmentado, $shop_id, $destacado, $subtitle=NULL) {

    $values = array('type' => 'benefit_shop_branch', 'status' => 1, 'comment' => 1, 'promote' => 0,);
    $entityBsb = entity_create('benefit_shop_branch', $values);
    $ewrapperFile = entity_metadata_wrapper('benefit_shop_branch', $entityBsb);

    $ewrapperFile->title->set($id_promo);

    $result_branchs = db_query("
      SELECT bs.entity_id FROM field_data_field_branch_shop bs
      WHERE bs.field_branch_shop_target_id = :shop_id;", array(":shop_id" => $shop_id)
    );

    foreach ($result_branchs as $row_branch) {
      $ewrapperFile->field_bsb_branchs[] = $row_branch->entity_id;
    }

    $ewrapperFile->field_bsb_shop->set($shop_id);
    $ewrapperFile->save();

    // Aqui se guarda los nuevos ids del titulo de comercio
    $vocabulary_tags = taxonomy_vocabulary_machine_name_load('tags');
    $todos_los_terminos_tags = bbva_entity_shop_get_terms($vocabulary_tags->vid); // Se van a guardar todos los nombres del VOCAB Tags

    //Crear Entity benefit

    $entity_type = 'benefit';
    $entityBenefit = entity_create($entity_type, array('type' => 'benefit', 'status' => $stat, 'comment' => 0, 'promote' => 1,));
    $wrapper_benefit = entity_metadata_wrapper($entity_type, $entityBenefit);

    $entity_type = 'shop';
    $shop = entity_load_single($entity_type, $shop_id);
    $shopWrapper = entity_metadata_wrapper($entity_type, $shop);

    // Se agregan los tags
    if (!array_key_exists(trim(mb_strtolower($shop->title)), $todos_los_terminos_tags)) {

       //Si no existe se agrega a la taxonomia
      $palabra_clave = new stdClass();
      $palabra_clave->name = trim($shop->title);
      $palabra_clave->vid = $vocabulary_tags->vid;
      taxonomy_term_save($palabra_clave);
      $wrapper_benefit->field_tags[]=$palabra_clave->tid;
    }else{
      $wrapper_benefit->field_tags[]=$todos_los_terminos_tags[trim(mb_strtolower($shop->title))];
    }

    $wrapper_benefit->title->set($shop->title);
    $wrapper_benefit->field_benefit_description->set($subtitle);
    $wrapper_benefit->field_benefit_description_portal->set($descripcion_benefit);
    $wrapper_benefit->field_benefit_date_start->set($fecha_desde);
    $wrapper_benefit->field_benefit_date_end->set($fecha_hasta);
    $wrapper_benefit->field_benefit_legal->set($legal);
    $wrapper_benefit->field_benefit_shop_branchs[] = $ewrapperFile->getIdentifier();
    if($destacado == 1) {
        $wrapper_benefit->field_benefit_membership_chan[] = 3590;
    }
    $rubros = _get_category();
    $categorycup = $shopWrapper->field_shop_category->value();
    try {
      foreach ((array)$categorycup as $key => $value) {

        $term = taxonomy_term_load($value->tid);
        if (array_key_exists($term->field_category_parent['und'][0]['value'], $rubros)) {
            $wrapper_benefit->field_benefit_category[] = $rubros[$term->field_category_parent['und'][0]['value']]->tid;
        }
      }
    }
    catch(Exception $e) {
    }

    $term_by_suip = _get_term_by_suip_id(991);

    $wrapper_benefit->field_benefit_category[] = $term_by_suip;
    if  ($segmentado) {
      $wrapper_benefit->field_published->set('0');
    }

    $wrapper_benefit->save();

    return $wrapper_benefit->getIdentifier();
}

function bbva_coupon_promotion_finished($success, $results, $operations) {
  if($success) {
    drupal_set_message(t("\n Archivos creados de CUPONES & VOUCHERS - ".date('Y-m-d H:i:s')));
  }
  else{
    $error_operation = reset($operations);
    $message = t(
      'An error occurred while processing %error_operation with arguments: @arguments', 
      array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE)
      )
    );
    drupal_set_message($message, 'error');
  }
}

