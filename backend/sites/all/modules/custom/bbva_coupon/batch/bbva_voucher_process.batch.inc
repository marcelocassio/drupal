<?php
  /**
   * The batch processor.
   */
  // bvpnew => cuponesRecepcion.sh
  function bbva_coupon_voucher_process_visa_process_new($data, &$context) {
    
    $archivo_vouchers = file('/prod/jp00/cupones/vuelta/TJVOUC02-VOUCHER-VISA.txt');

    $cupon_type = array();
    $vouchers_disponibles = array();
    $vouchers_eventos = array();

    foreach($archivo_vouchers as $num_línea => $línea) {
      if($num_línea == 0){
        continue;
      }
      
      // Si es igual a 80 el proceso es correcto tengo que actualizar el estado del voucher
      $members = explode(' ', $línea);

      $data_codigo_random = substr($members[0], -6); // codigo voucher de 6
      $data_code_promo = substr($members[0], -10, 4); // 1021

      if (strlen($línea) == 85) {
        if (!isset($cupon_type[$data_code_promo]["tipo"])) {
          $voucher = voucher_load($data_codigo_random, $data_code_promo);
          
          if (isset($voucher)) {
            $entity = entity_load_single('coupon_promo', $voucher->id_cupon);
            $cupon = entity_metadata_wrapper('coupon_promo', $entity);
            $cupon_type[$data_code_promo]["tipo"] = $cupon->field_coupon_type->value();
            $cupon_type[$data_code_promo]["id_cupon"] = $voucher->id_cupon;
          }
        }
        
        if (isset($cupon_type[$data_code_promo]["tipo"])) {
          if ($cupon_type[$data_code_promo]["tipo"] === '3') {
            $vouchers_eventos[$cupon_type[$data_code_promo]["id_cupon"]][] = $data_codigo_random;
          } 
          else{
            $vouchers_disponibles[$cupon_type[$data_code_promo]["id_cupon"]][] = $data_codigo_random;
          }
        }
      }
      else {
        $voucher = voucher_load($data_codigo_random, $data_code_promo);
        if (isset($voucher)) {
          $voucher->delete();
        }
      }
    }

    if(count($vouchers_disponibles) > 0) {
      foreach ($vouchers_disponibles as $id_cupon => $listado_vouchers) {
        bbva_coupon_actualizacion_masiva_vouchers($id_cupon, $listado_vouchers, 2);
        echoline("Se crearon: ".count($listado_vouchers)." Vouchers Disponibles");
      }
    }

    if(count($vouchers_eventos) > 0) {
      foreach ($vouchers_eventos as $id_cupon => $listado_vouchers_evento) {
        bbva_coupon_actualizacion_masiva_vouchers($id_cupon, $listado_vouchers_evento, 4);
        echoline("Se crearon: ".count($listado_vouchers_evento)." Vouchers Eventos");
      }
    }

    echoline('Fin BVPNEW', true, true);
  }

  /**
   * The batch finish handler.
   */
  function bbva_coupon_voucher_process_finished_new($success, $results, $operations){
      if ($success) {
          drupal_set_message(t('File Voucher is Process Complete!'));
      } else {
          $error_operation = reset($operations);
          $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], true),
      ));
          drupal_set_message($message, 'error');
      }
  }
