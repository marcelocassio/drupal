<?php

  // drush bvrpnew
  function bbva_voucher_process_rendicion_visa_process_new($data, &$context){

    echoline(" === INICIO PROCESO DE RENDICION DE VOUCHER VISA === ", true, true);
    $voucher = new Voucher();
    $archivo_rendicion = file('/prod/jp00/cupones/vuelta/TJVOUC02-RENDINCION-VISA.txt');
    $contador = 0;

    foreach($archivo_rendicion as $num_línea => $línea) {

      $fecha = DateTime::createFromFormat('dmY His', substr($línea, 36, 8)." ".substr($línea, 44, 6));
      $params = array(
        'estado' => $voucher->getEstadoUsado(),
        'monto_compra' => substr($línea, 68, 10).".".substr($línea, 78, 2),
        'monto_descuento' => substr($línea, 80, 10).".".substr($línea, 90, 2),
        'fecha_uso' => $fecha->format('Y-m-d H:i:s')
      );

      $rendicion = $voucher->voucherUpdateStatus(substr($línea, 60, 6), substr($línea, 56, 4), $params);
      if($rendicion > 0){
        $contador ++;
      }
    }

    echoline(" Se rindieron ".$contador." de ".count($archivo_rendicion)." voucher de VISA", true, true);
    echoline(" === FIN PROCESO DE RENDICION DE VOUCHER VISA === ", true, true);

  }

function bbva_voucher_process_rendicion_files_process_new($filename, &$context)
{
  ini_set("auto_detect_line_endings", true);

  //$file_name = drupal_get_path('module', 'batch_coupon') . '/tmp/francesgo.txt'; // Al actualizar el path del archivo, eliminar la carpeta /tmp de /batch_coupon
  $file_name = $filename;
  $lineas = file($file_name);
  $i == 0;
  $context['message'] = 'Start ' . date('Y-m-d H:i:s');

  if (isset($lineas)) {

    foreach ($lineas as $row => $record) {

      $i++;
      $coupon = '';
      $coupon = explode(';', $record);
      $id = trim($coupon[0]);
     
      if (isset($coupon[1])) {
        $fecha = DateTime::createFromFormat('d/m/Y', $coupon[1])->format('Y-m-d H:i:s');

      } else {
        $fecha = date('Y-m-d H:i:s');
      }

      $voucher = voucher_load($id);
      if (isset($voucher)) {

        $voucher->state = 3;
        $voucher->fecha_uso = $fecha;
        $voucher->save();
      }

    }
    if ($key % 200 == 0) {
      $context['message'] = "Update Cupon $key ..." . date('Y-m-d H:i:s');
    }
  }

}

/**
 * The batch finish handler.
 */
function bbva_voucher_process_rendicion_finished_new($success, $results, $operations)
{
  if ($success) {
    drupal_set_message(t('File Rendicion is Process Complete!'));
  } else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], true),
    ));
    drupal_set_message($message, 'error');
  }
}
