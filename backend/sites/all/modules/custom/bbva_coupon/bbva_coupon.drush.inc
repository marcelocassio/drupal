<?php
/**
 * Implements COMMANDFILE_drush_command().
 */
function bbva_coupon_drush_command() {
  $items = array();

  // bcpnew
  $items['bbva-coupon-promotion-new'] = array(
    'description' => 'Drush process that generates coupons and promotions associated vouchers.',
    'aliases' => array('bcpnew'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  // bvpnew
  $items['bbva-coupon-voucher-process-new'] = array(
    'description' => 'Drush process info vouchers process for VISA.',
    'aliases' => array('bvpnew'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  // bvrpnew
  $items['bbva-coupon-voucher-rendicion-process-new'] = array(
    'description' => 'Drush process info vouchers rendicion process for VISA.',
    'aliases' => array('bvrpnew'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  // bvrpfilesnew
  $items['bbva-coupon-voucher-rendicion-files-process-new'] = array(
    'description' => 'Drush process info vouchers rendicion process for Archivos.',
    'aliases' => array('bvrpfilesnew'),

    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
        'arguments' => array(
      'filename' => 'Nombre de archivo',
    )
  );

  return $items;
}

/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_bbva_coupon_promotion_new() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Coupon Promotion Visa'),
    'init_message' => t('Batch Coupon Promotion Visa is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_coupon_promotion_finished',
    'file' => drupal_get_path('module', 'bbva_coupon') . '/batch/bbva_coupon.batch.inc',
  );

  $batch['operations'][] = array('bbva_coupon_promotion_process', array(''));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}



/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_bbva_coupon_voucher_process_new() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Voucher Process Visa'),
    'init_message' => t('Batch Voucher Process Visa is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_coupon_voucher_process_finished_new',
    'file' => drupal_get_path('module', 'bbva_coupon') . '/batch/bbva_voucher_process.batch.inc',
  );

  $batch['operations'][] = array('bbva_coupon_voucher_process_visa_process_new', array(''));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

// bvrpnew
function drush_bbva_coupon_voucher_rendicion_process_new() {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Voucher Rendicion Process Visa'),
    'init_message' => t('Batch Voucher Rendicion new  Process Visa is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_voucher_process_rendicion_finished_new',
    'file' => drupal_get_path('module', 'bbva_coupon') . '/batch/bbva_voucher_rendicion_process.batch.inc',
  );

  $batch['operations'][] = array('bbva_voucher_process_rendicion_visa_process_new', array(''));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}


function drush_bbva_coupon_voucher_rendicion_files_process_new($filename) {
  // Set up the batch job.
  $batch = array(
    'operations' => array(),
    'title' => t('Batch Voucher Rendicion Process Visa'),
    'init_message' => t('Batch Voucher Rendicion new  Process Visa is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_voucher_process_rendicion_finished_new',
    'file' => drupal_get_path('module', 'bbva_coupon') . '/batch/bbva_voucher_rendicion_process.batch.inc',
  );

    $batch['operations'][] = array('bbva_voucher_process_rendicion_files_process_new', array($filename));
  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}