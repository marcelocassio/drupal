<?php

class Voucher
{
  public $id;
  public $id_cupon;
  public $num_bin_visa;
  public $code_promo_visa;
  public $code;
  public $state;
  public $fecha_uso;
  public $fecha_descarga;
  public $monto_compra;
  public $monto_descuento;
  public $name_user;
  public $title;

  const ESTADO_PENDIENTE = 0;
  const ESTADO_ENVIADO = 1;
  const ESTADO_DISPONIBLE = 2;
  const ESTADO_USADO = 3;
  const ESTADO_POR_EVENTO = 4;
  const ESTADO_DESCARGADO = 5;

  public function __construct1($id_cupon, $code_promo_visa, $code){
    $this->id_cupon = $id_cupon;
    $this->code_promo_visa = $code_promo_visa;
    $this->code = $code;
  }

  public function __construct(){
  }

  public function getEstadoPendiente(){
    return self::ESTADO_PENDIENTE;
  }

  public function getEstadoEnviado(){
    return self::ESTADO_ENVIADO;
  }

  public function getEstadoUsado(){
    return self::ESTADO_USADO;
  }

  public function save(){
    $query = db_merge('bbva_vouchers')
      ->key(array(
        'id' => $this->id,
      ))
      ->fields(array(
        'name_user' => $this->name_user,
        'state' => $this->state,
        'num_bin_visa' => $this->num_bin_visa,
        'code_promo_visa' => $this->code_promo_visa,
        'id_cupon' => $this->id_cupon,
        'fecha_uso' => $this->fecha_uso,
        'fecha_descarga' => $this->fecha_descarga,
        'monto_compra' => $this->monto_compra,
        'monto_descuento' => $this->monto_descuento,
        'code' => $this->code,
        'title' => $this->title,
      ))
      ->execute();
    return $query;

  }
  public function delete(){
    $query = db_delete('bbva_vouchers')
      ->condition('id', $this->id)
      ->execute();

  }

  public function create(){
    $query = db_insert('bbva_vouchers')
      ->fields(array(
        'name_user' => $this->name_user,
        'state' => $this->state,
        'num_bin_visa' => $this->num_bin_visa,
        'code_promo_visa' => $this->code_promo_visa,
        'id_cupon' => $this->id_cupon,
        'fecha_uso' => $this->fecha_uso,
        'fecha_descarga' => $this->fecha_descarga,
        'monto_compra' => $this->monto_compra,
        'monto_descuento' => $this->monto_descuento,
        'code' => $this->code,
        'title' => $this->title,
      ))
      ->execute();

  }

  /*
   * Codigo Random = 6 char
   * Codigo promo = ejemplo: 1021
   * params (estado, fechaUso, montoCompra, montoDescuento)
   *
  */
  public function voucherUpdateStatus($codigo_random, $code_promo_visa, $params){

   $updated = db_update('bbva_vouchers')
    ->fields(array(
      'state' => $params['estado'],
      'monto_compra' => $params['monto_compra'],
      'monto_descuento' => $params['monto_descuento'],
      'fecha_uso' => $params['fecha_uso']
    ))
    ->condition('code_promo_visa', $code_promo_visa)
    ->condition('code', $codigo_random)
    ->execute();

    return $updated;
  }
}





function voucher_load($code = null, $code_promo_visa = null, $id = null){

  $query = db_select('bbva_vouchers', 'v')
    ->fields('v', array(
      'id',
      'name_user',
      'state',
      'id_cupon',
      'fecha_uso',
      'fecha_descarga',
      'monto_compra',
      'monto_descuento',
      'title',
      'code',
      'num_bin_visa',
      'code_promo_visa'))
    ->orderBy('id_cupon', 'DESC');
  
  if (isset($id)) {
    $query->condition('v.id', $id, '=');
  } 
  else {
    if(isset($code)) {
      $query->condition('v.code', $code, '=');

    }
    if (isset($code_promo_visa)) {
      $query->condition('v.code_promo_visa', $code_promo_visa, '=');
    }
  }

  $result = $query->execute()->fetchObject();
  
  if (!empty($result)) {
    $voucher = new Voucher();
    $voucher->id = $result->id;
    $voucher->name_user = $result->id;
    $voucher->name_user = $result->name_user;
    $voucher->state = $result->state;
    $voucher->id_cupon = $result->id_cupon;
    $voucher->fecha_uso = $result->fecha_uso;
    $voucher->fecha_descarga = $result->fecha_descarga;
    $voucher->monto_compra = $result->monto_compra;
    $voucher->monto_descuento = $result->monto_descuento;
    $voucher->title = $result->title;
    $voucher->code = $result->code;
    $voucher->num_bin_visa = $result->num_bin_visa;
    $voucher->code_promo_visa = $result->code_promo_visa;
    return $voucher;
  }

  return null;

}

function download_voucher($id_cupon, $name_user)
{

  $query = db_select('bbva_vouchers', 'v')
    ->fields('v', array(
      'id',
      'name_user',
      'state',
      'id_cupon',
      'fecha_uso',
      'fecha_descarga',
      'monto_compra',
      'monto_descuento',
      'title',
      'code',
      'num_bin_visa',
      'code_promo_visa'))
    ->condition('v.id_cupon', $id_cupon, '=')
    ->condition('v.state', 2, '=')
    ->isNull('v.name_user');
  $result = $query->execute()->fetchObject();
  if (!empty($result)) {
    $voucher = new Voucher();
    $voucher->id = $result->id;
    $voucher->id_cupon = $result->id_cupon;
    $voucher->fecha_uso = $result->fecha_uso;
    $voucher->monto_compra = $result->monto_compra;
    $voucher->monto_descuento = $result->monto_descuento;
    $voucher->title = $result->title;
    $voucher->code = $result->code;
    $voucher->num_bin_visa = $result->num_bin_visa;
    $voucher->code_promo_visa = $result->code_promo_visa;

    $voucher->name_user = $name_user;
    $voucher->fecha_descarga = date('Y-m-d H:i:s');
    $voucher->state = 5;
    $voucher->save();
    return $voucher;
  }

  return null;

}

function asignar_voucher($id_cupon, $name_user)
{

  $query = db_select('bbva_vouchers', 'v')
    ->fields('v', array(
      'id',
      'name_user',
      'state',
      'id_cupon',
      'fecha_uso',
      'fecha_descarga',
      'monto_compra',
      'monto_descuento',
      'title',
      'code',
      'num_bin_visa',
      'code_promo_visa'))
    ->condition('v.id_cupon', $id_cupon, '=')
    ->condition('v.state', 4, '=')
    ->condition('v.name_user', $name_user, '=');

  $result = $query->execute()->fetchObject();
  if (empty($result)) {
    $query = db_select('bbva_vouchers', 'v')
      ->fields('v', array(
        'id',
        'name_user',
        'state',
        'id_cupon',
        'fecha_uso',
        'fecha_descarga',
        'monto_compra',
        'monto_descuento',
        'title',
        'code',
        'num_bin_visa',
        'code_promo_visa'))
      ->condition('v.id_cupon', $id_cupon, '=')
      ->condition('v.state', 4, '=')
      ->isNull('v.name_user');

    $result = $query->execute()->fetchObject();
  }

  if (!empty($result)) {
    $voucher = new Voucher();
    $voucher->id = $result->id;
    $voucher->id_cupon = $result->id_cupon;
    $voucher->fecha_uso = $result->fecha_uso;
    $voucher->monto_compra = $result->monto_compra;
    $voucher->monto_descuento = $result->monto_descuento;
    $voucher->title = $result->title;
    $voucher->code = $result->code;
    $voucher->num_bin_visa = $result->num_bin_visa;
    $voucher->code_promo_visa = $result->code_promo_visa;

    $voucher->name_user = $name_user;
    $voucher->fecha_descarga = date('Y-m-d H:i:s');
    $voucher->state = 5;
    $voucher->save();
    return $voucher;
  }

  return null;

}

function load_vouchers_user_state($name_user, $state){

  $hoy = new DateTime('today');
  $vouchers = array();
  $query = db_select('bbva_vouchers', 'v')
    ->fields('v', array(
      'id',
      'name_user',
      'state',
      'id_cupon',
      'fecha_uso',
      'fecha_descarga',
      'monto_compra',
      'monto_descuento',
      'title',
      'code',
      'num_bin_visa',
      'code_promo_visa'));
  $query->condition('v.state', $state, '=');
  $query->condition('v.name_user', $name_user, '=');
  if ($state == 2 || $state == 5) {
    $query->innerJoin('field_data_field_coupon_promo_date_end', 'datend', 'datend.entity_id = v.id_cupon');
    $query->condition('datend.field_coupon_promo_date_end_value', $hoy->getTimestamp(), '>=');
  }
  $result = $query->execute()->fetchAll();

  if (isset($result)) {
    foreach ($result as $key => $value) {
      $voucher = new Voucher();
      $voucher->id = $value->id;
      $voucher->name_user = $value->name_user;
      $voucher->state = $value->state;
      $voucher->id_cupon = $value->id_cupon;
      $voucher->fecha_uso = $value->fecha_uso;
      $voucher->fecha_descarga = $value->fecha_descarga;
      $voucher->monto_compra = $value->monto_compra;
      $voucher->monto_descuento = $value->monto_descuento;
      $voucher->title = $value->title;
      $voucher->code = $value->code;
      $voucher->num_bin_visa = $value->num_bin_visa;
      $voucher->code_promo_visa = $value->code_promo_visa;

      $vouchers[] = $voucher;
    }

    return $vouchers;
  }

  return null;

}

function load_voucher_user($id_voucher, $name_user)
{

  $query = db_select('bbva_vouchers', 'v')
    ->fields('v', array(
      'id',
      'name_user',
      'state',
      'id_cupon',
      'fecha_uso',
      'fecha_descarga',
      'monto_compra',
      'monto_descuento',
      'title',
      'code',
      'num_bin_visa',
      'code_promo_visa'));
  $query->condition('v.id', $id_voucher, '=');
  $query->condition('v.name_user', $name_user, '=');
  $result = $query->execute()->fetchObject();
  if (!empty($result)) {
    $voucher = new Voucher();
    $voucher->id = $result->id;
    $voucher->name_user = $result->name_user;
    $voucher->state = $result->state;
    $voucher->id_cupon = $result->id_cupon;
    $voucher->fecha_uso = $result->fecha_uso;
    $voucher->fecha_descarga = $result->fecha_descarga;
    $voucher->monto_compra = $result->monto_compra;
    $voucher->monto_descuento = $result->monto_descuento;
    $voucher->title = $result->title;
    $voucher->code = $result->code;
    $voucher->num_bin_visa = $result->num_bin_visa;
    $voucher->code_promo_visa = $result->code_promo_visa;
    return $voucher;
  }

  return null;

}

function _voucher_disponible($id_cupon, $name_user)
{

  $query = db_select('bbva_vouchers', 'v')
    ->fields('v', array('id', 'name_user'))
    ->condition('v.id_cupon', $id_cupon, '=')
    ->condition('v.name_user', $name_user, '=');
  $result = $query->execute()->fetchObject();
  if (!empty($result)) {
    return $result->id;
  }

  return null;

}

function _vouchers_con_stock($coupon_id)
{

  $query = db_select('bbva_vouchers', 'v')
    ->fields('v', array('id'))
    ->condition('v.id_cupon', $coupon_id, '=')
    ->condition('v.state', 2, '=')
    ->range(0, 1);
  $query->isNull('v.name_user');

  $result = $query->execute()->fetchAll();

  if (!empty($result)) {

    return 1;
  }

  return 0;

}
