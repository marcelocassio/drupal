<?php

class Cupon{

  public function getCuponesEnviadosVisa($cupon_promo){
   
    $query = db_query('
      SELECT fcpn.entity_id as id,
      fcps.field_coupon_promo_shop_target_id as id_promo_shop,
      cpp.field_coupon_promo_promo_suip_value AS id_promo_suip
      FROM field_data_field_coupon_number_promo fdcnp
      JOIN field_data_field_coupon_promo_numero fcpn on fcpn.field_coupon_promo_numero_tid = fdcnp.entity_id
      JOIN field_data_field_coupon_state_shipping fcss on fcss.entity_id = fcpn.entity_id
      LEFT JOIN field_data_field_coupon_promo_shop fcps ON fcps.entity_id = fcpn.entity_id
      LEFT JOIN field_data_field_coupon_promo_promo_suip cpp ON fcpn.entity_id = cpp.entity_id
      WHERE fdcnp.field_coupon_number_promo_value = :number_promo and fcss.field_coupon_state_shipping_value=:state and (fcps.field_coupon_promo_shop_target_id IS NOT NULL OR cpp.field_coupon_promo_promo_suip_value IS NOT NULL);'
      ,
      array(
        ':number_promo' => (int) $cupon_promo,
        ':state' => CUPON_ESTADO_ENVIADO
      )
    );

    return $query;
  }

  public function getBranchShop($idPromoShop){

    $query = db_query(
      'SELECT bs.entity_id FROM field_data_field_branch_shop bs
      WHERE bs.field_branch_shop_target_id = :shop_id;'
      , 
      array(
        ':shop_id' => $idPromoShop
      )
    );

    return $query;
  }

  public function getCuponesPromoNumero($cupon_promo){
    $query = db_query(
      'SELECT fcpn.entity_id as id FROM field_data_field_coupon_number_promo fdcnp
      JOIN field_data_field_coupon_promo_numero fcpn on fcpn.field_coupon_promo_numero_tid = fdcnp.entity_id
      WHERE fdcnp.field_coupon_number_promo_value = :number_promo;'
      ,
      array(':number_promo' => $cupon_promo)
    );

    return $query;
  }
}