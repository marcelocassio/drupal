<?php

/**
 * @function: _get_coupons_promo()
 * @description: trae los id de los cupon promo
 * @param $state  (in): 0 : ESTADO PENDIENTE
 */

function _get_coupons_promo($state = null) {

    $query = db_select('eck_coupon_promo', 'ecp');
    $query->fields('ecp', array('id'));

    $query->innerJoin('field_data_field_coupon_state_shipping', 'fcss', 'fcss.entity_id = ecp.id');

    if(isset($state)) {
      $query->condition('fcss.field_coupon_state_shipping_value', $state, '=');
    }

    return $query->execute()->fetchAll();
}

/**
 * @function: _get_cupon_whit_benefit()
 * @description: busca el cupon asociado al beneficio
 * @param $id beneficio
 */

function _get_cupon_with_benefit($benefit){

  $query = db_select("field_data_field_coupon_promo_benefit", "fcpb");
  $query = $query->fields('fcpb', array('entity_id'));
  $query = $query->condition('fcpb.field_coupon_promo_benefit_target_id', $benefit);
  $result = $query->execute()->fetchField();

  if (!empty($result)) {
    return $result;
  }else{
    return FALSE;
  }

}

function _get_cupon_con_comunicacion($id_comunicacion) {

  $idPromo = null;
  $promoCuponBo = new \Fgo\Bo\PromoCuponBo();
  $promoEncontrada = $promoCuponBo->buscarPromoCuponPorIdComunicacion($id_comunicacion);
  if ($promoEncontrada != null) {
    $idPromo = $promoEncontrada->idCupon;
  }
  return $idPromo;
}

function _get_cupon_whit_campania($id_campania)
{
    $query = db_select('field_data_field_coupon_promo_campania_id', 'ecpcid');
    $query->innerJoin('field_data_field_coupon_state_shipping', 'fcss', 'fcss.entity_id = ecpcid.entity_id');

    $query = $query->fields('ecpcid', array('entity_id'));
    $query = $query->condition('fcss.field_coupon_state_shipping_value', 2, '=');
    $query = $query->condition('ecpcid.field_coupon_promo_campania_id_value', $id_campania, '=');

    return $result = $query->execute()->fetchObject();
}


/* Funcion que trae todos los vauchers  generados por el modulo de drupal entity
   Para poder exportarlo  al nuevo schema
        SELECT ecv.title,
               ecv.id,
               fvcp.field_voucher_coupon_promo_target_id              AS field_voucher_coupon_promo_target_id,
               fvcode.field_voucher_code_value                        AS field_voucher_code_value,
               fvst.field_voucher_state_value                         AS field_voucher_state_value,
               fvfu.field_voucher_fecha_uso_value                     AS field_voucher_fecha_uso_value,
               fvmtc.field_voucher_monto_compra_value                 AS field_voucher_monto_compra_value,
               fvmtd.field_voucher_monto_descuento_value              AS field_voucher_monto_descuento_value,
               fvu.field_voucher_user_target_id                       AS field_voucher_user_target_id,
               fcnp.field_coupon_number_promo_value                   AS field_coupon_number_promo_value
        FROM eck_voucher  ecv
        INNER JOIN       field_data_field_voucher_coupon_promo     fvcp   ON ecv.id         = fvcp.entity_id
        LEFT OUTER JOIN  field_data_field_voucher_code             fvcode ON ecv.id         = fvcode.entity_id
        LEFT OUTER JOIN  field_data_field_voucher_promo_visa       fvpv   ON ecv.id         = fvpv.entity_id
        LEFT OUTER JOIN  field_data_field_voucher_state            fvst   ON ecv.id         = fvst.entity_id
        LEFT OUTER JOIN  field_data_field_voucher_fecha_uso        fvfu   ON ecv.id         = fvfu.entity_id
        LEFT OUTER JOIN  field_data_field_voucher_monto_compra     fvmtc  ON ecv.id         = fvmtc.entity_id
        LEFT OUTER JOIN  field_data_field_voucher_monto_descuento  fvmtd  ON ecv.id         = fvmtd.entity_id
        LEFT OUTER JOIN  field_data_field_voucher_user             fvu    ON ecv.id         = fvu.entity_id
        LEFT OUTER JOIN  field_data_field_coupon_number_promo      fcnp   ON fcnp.entity_id = fvpv.field_voucher_promo_visa_tid
        WHERE  (fvu.field_voucher_user_target_id IS NOT NULL )

*/
function _voucher_migrate_descargado(&$context) {
    $query = db_select('eck_voucher', 'ecv');

    $query->innerJoin('field_data_field_voucher_coupon_promo'   , 'fvcp'  , 'ecv.id         = fvcp.entity_id');
    $query->leftJoin('field_data_field_voucher_code'            , 'fvcode', 'ecv.id         = fvcode.entity_id');
    $query->leftJoin('field_data_field_voucher_promo_visa'      , 'fvpv'  , 'ecv.id         = fvpv.entity_id');
    $query->leftJoin('field_data_field_voucher_state'           , 'fvst'  , 'ecv.id         = fvst.entity_id');
    $query->leftJoin('field_data_field_voucher_fecha_uso'       , 'fvfu'  , 'ecv.id         = fvfu.entity_id');
    $query->leftJoin('field_data_field_voucher_monto_compra'    , 'fvmtc' , 'ecv.id         = fvmtc.entity_id');
    $query->leftJoin('field_data_field_voucher_monto_descuento' , 'fvmtd' , 'ecv.id         = fvmtd.entity_id');
    $query->leftJoin('field_data_field_voucher_user'            , 'fvu'   , 'ecv.id         = fvu.entity_id');
    $query->leftJoin('users'                                    , 'user'   , 'fvu.field_voucher_user_target_id         = user.uid');


    $query->leftJoin('field_data_field_coupon_number_promo'     , 'fcnp'  , 'fcnp.entity_id = fvpv.field_voucher_promo_visa_tid');


    $query->condition('fvst.field_voucher_state_value',3,'=');


    $query = $query->isNotNull('fvu.field_voucher_user_target_id');

    $query->fields('ecv'      , array('id','title'));
    $query->fields('fvcp'      , array('field_voucher_coupon_promo_target_id'));
    $query->fields('fvcode'    , array('field_voucher_code_value'));
    $query->fields('fvst'      , array('field_voucher_state_value'));
    $query->fields('fvfu'      , array('field_voucher_fecha_uso_value'));
    $query->fields('fvmtc'     , array('field_voucher_monto_compra_value'));
    $query->fields('fvmtd'     , array('field_voucher_monto_descuento_value'));
    $query->fields('fvu'       , array('field_voucher_user_target_id'));
    $query->fields('user'      , array('name'));

    $query->fields('fcnp'      , array('field_coupon_number_promo_value'));
    $result = $query->execute()->fetchAll();

    if (empty($result)) {
        return null;
    }

    $vouchers= array();
    $count=0;
     foreach ($result as $key => $value) {


        $voucher = new Voucher;
        $voucher->id = $value->id;

        $voucher->id_cupon = $value->field_voucher_coupon_promo_target_id;


        $voucher->code_promo_visa = $value->field_coupon_number_promo_value;
        if ($value->field_voucher_code_value==='00000') {
            $voucher->num_bin_visa = 0;
            if (!$value->title) {
                $vouchermalos[]=$value->id;
             }
            $voucher->code = $value->title;
            $voucher->title = $value->title;

        }else {

           $voucher->num_bin_visa = 505893;
           $voucher->code = $value->field_voucher_code_value;
           $voucher->title ="505893"." - ".$voucher->code_promo_visa." - ".$voucher->code;
        }

       if ($value->field_voucher_state_value==2) {
                   $voucher->state =5;

       }else{
         $voucher->state =$value->field_voucher_state_value;
       }
        $voucher->fecha_uso = $value->field_voucher_fecha_uso_value;
        $voucher->monto_compra = $value->field_voucher_monto_compra_value;
        $voucher->monto_descuento = $value->field_voucher_monto_descuento_value;
        if (!empty($value->name)&&$value->name!=null) {
             $voucher->name_user = $value->name;
        }





        $voucher->save();
        if ($count%100) {
          $context['message'] = "Now processing $count...";
        }


     }

    return $vouchermalos;
}


function _voucher_migrate_vigente() {
    $query = db_select('eck_voucher', 'ecv');

    $query->innerJoin('field_data_field_voucher_coupon_promo'   , 'fvcp'  , 'ecv.id         = fvcp.entity_id');
    $query->leftJoin('field_data_field_voucher_code'            , 'fvcode', 'ecv.id         = fvcode.entity_id');
    $query->leftJoin('field_data_field_voucher_promo_visa'      , 'fvpv'  , 'ecv.id         = fvpv.entity_id');
    $query->leftJoin('field_data_field_voucher_state'           , 'fvst'  , 'ecv.id         = fvst.entity_id');
    $query->leftJoin('field_data_field_voucher_fecha_uso'       , 'fvfu'  , 'ecv.id         = fvfu.entity_id');
    $query->leftJoin('field_data_field_voucher_monto_compra'    , 'fvmtc' , 'ecv.id         = fvmtc.entity_id');
    $query->leftJoin('field_data_field_voucher_monto_descuento' , 'fvmtd' , 'ecv.id         = fvmtd.entity_id');
    $query->leftJoin('field_data_field_voucher_user'            , 'fvu'   , 'ecv.id         = fvu.entity_id');
    $query->leftJoin('users'                                    , 'user'   , 'fvu.field_voucher_user_target_id         = user.uid');

    $query->leftJoin('field_data_field_coupon_number_promo'     , 'fcnp'  , 'fcnp.entity_id = fvpv.field_voucher_promo_visa_tid');

    $query->innerJoin('field_data_field_coupon_promo_date_end', 'datend', 'datend.entity_id = fvcp.field_voucher_coupon_promo_target_id');
    $query->condition('datend.field_coupon_promo_date_end_value',time(), '>=');
    $query->condition('fvst.field_voucher_state_value',3,'!=');


    $query->fields('ecv'      , array('id','title'));
    $query->fields('fvcp'      , array('field_voucher_coupon_promo_target_id'));
    $query->fields('fvcode'    , array('field_voucher_code_value'));
    $query->fields('fvst'      , array('field_voucher_state_value'));
    $query->fields('fvfu'      , array('field_voucher_fecha_uso_value'));
    $query->fields('fvmtc'     , array('field_voucher_monto_compra_value'));
    $query->fields('fvmtd'     , array('field_voucher_monto_descuento_value'));
    $query->fields('fvu'       , array('field_voucher_user_target_id'));
        $query->fields('user'       , array('name'));

    $query->fields('fcnp'      , array('field_coupon_number_promo_value'));
    $result = $query->execute()->fetchAll();
    if (empty($result)) {
        return null;
    }

    $vouchers= array();
    $count=0;
     foreach ($result as $key => $value) {


        $voucher = new Voucher;
        $voucher->id = $value->id;

        $voucher->id_cupon = $value->field_voucher_coupon_promo_target_id;


        $voucher->code_promo_visa = $value->field_coupon_number_promo_value;
        if ($value->field_voucher_code_value==='00000') {
            $voucher->num_bin_visa = 0;
            if (!$value->title) {
                $vouchermalos[]=$value->id;
             }
            $voucher->code = $value->title;
            $voucher->title = $value->title;

        }else {

           $voucher->num_bin_visa = 505893;
           $voucher->code = $value->field_voucher_code_value;
           $voucher->title ="505893"." - ".$voucher->code_promo_visa." - ".$voucher->code;
        }

        $voucher->state = $value->field_voucher_state_value;
        $voucher->fecha_uso = $value->field_voucher_fecha_uso_value;
        $voucher->monto_compra = $value->field_voucher_monto_compra_value;
        $voucher->monto_descuento = $value->field_voucher_monto_descuento_value;
        if (!empty($value->name)&&$value->name!=null) {
             $voucher->name_user = $value->name;
             $voucher->state = 5;

        }





        $voucher->save();

     }

    return $vouchermalos;
}

function _get_code_voucher_promo_usado($code_visa){

  $query = db_select('bbva_vouchers', 'bv');
  $query->condition('bv.code_promo_visa',$code_visa , '=');
  $query->fields('bv', array('code','code_promo_visa'));

  $resultT = $query->execute()->fetchAll();
  $numbresVouchers = array();

  foreach ($resultT as $key => $value) {
    $numbresVouchers[$value->code] = $key ;
  }

  return $numbresVouchers;
}

function _get_voucher_view($order, $sort="desc", $filtros=NULL, $limit=NULL){

  if (null === $sort) {
    $sort = "desc";
  }

  $query = db_select('bbva_vouchers', 'bv');
  $query->fields('bv', array('id', 'id_cupon', 'state', 'code','num_bin_visa','code_promo_visa','fecha_activo','fecha_uso','fecha_descarga','monto_compra','monto_descuento','title','name_user'));

  if (is_array($filtros)) {
    if ($filtros["id_cupon"]) {
      $query->condition('bv.id_cupon', $filtros["id_cupon"], '=');
    }
    if ($filtros["state"] != '') {
      $query->condition('bv.state', $filtros["state"], '=');
    }
    if ($filtros["name_user"] != '') {
      $query->condition('bv.name_user', $filtros["name_user"], '=');
    }
  }

  $query->orderBy($order, $sort);

  if ($limit != '') {
    $query = $query->extend('TableSort')->extend('PagerDefault')->limit($limit);
  }

  $results = $query->execute()->fetchAll();
  if ($results) {
    return $results;
  }else{
    return null;
  }
}



/**
 * @function: _get_coupons_promo()
 * @description: trae los id de los cupon promo
 * @param $state  (in): 0 : ESTADO PENDIENTE
 */

function _get_coupon_promo_por_vencer($dias = null){

  $dias = $dias + 1;

  $fecha_vencimiento = new DateTime();
  $fecha_vencimiento->modify('+' . $dias . ' day');

  $query = db_select('eck_coupon_promo', 'ecp');
  $query = $query->fields('ecp', array('id'));

  $query->innerJoin('field_data_field_coupon_state_shipping', 'fcss', 'fcss.entity_id = ecp.id');
  $query->innerJoin('field_data_field_coupon_promo_date_end', 'fcpde', 'fcpde.entity_id = ecp.id');
  $query = $query->condition('fcss.field_coupon_state_shipping_value', 2, '=');
  $query = $query->where("DATE_FORMAT( FROM_UNIXTIME(fcpde.field_coupon_promo_date_end_value ) , '%Y-%m-%d') ='" . $fecha_vencimiento->format('Y-m-d') . "'");

  return $result = $query->execute()->fetchAll();
}

function _bbva_coupon_get_id($idComercioSuip) {

  $query = db_select('eck_shop', 'es');
  $query->fields('es', array('id'));

  $query->leftJoin('field_data_field_shop_id', 'sid', 'sid.entity_id = es.id');
  $query->condition('sid.field_shop_id_value', $idComercioSuip, '=');

  return $query->execute()->fetchField();
}


function _bbva_coupon_no_existe_promo_suip($id_promo_suip) {
  
  $query = db_select('eck_coupon_promo', 'ecp');
  $query->innerJoin('field_data_field_coupon_promo_promo_suip', 'ps', 'ps.entity_id = ecp.id');
  $query = $query->fields('ecp', array('id'));
  $query = $query->condition('ps.field_coupon_promo_promo_suip_value', $id_promo_suip, '=');

  $result = $query->execute()->fetchAll();

  return count($result) <= 0;
}

function _get_promo_numero_tid($code = null) {
  
  $query = db_select('field_data_field_coupon_number_promo', 'fcn');
  $query = $query->fields('fcn', array('entity_id'));
  $query = $query->condition('fcn.field_coupon_number_promo_value', $code, '=');
  $result = $query->execute()->fetchAll();

  return isset($result[0]->entity_id) ? $result[0]->entity_id : false;
}

function bbva_coupon_actualizacion_masiva_vouchers($id_cupon, $vouchers, $estado) {
  $updated = db_update('bbva_vouchers')
    ->fields(array(
      'state' => $estado
    ))
    ->condition('id_cupon', $id_cupon)
    ->condition('code', $vouchers, 'IN')
    ->execute();

  return $updated;
}

function _bbva_coupon_get_coupon_suip_incompleto() {
  $query = db_select('eck_coupon_promo', 'ecp');
  $query->leftJoin('field_data_field_coupon_promo_promo_suip', 'ps', 'ps.entity_id = ecp.id');
  $query->leftJoin('field_data_field_coupon_promo_benefit', 'pb', 'pb.entity_id = ecp.id');
  $query->innerJoin('field_data_field_coupon_state_shipping', 'ss', 'ss.entity_id = ecp.id');
  $query->innerJoin('field_data_field_coupon_promo_id_comunica', 'pic', 'pic.entity_id = ecp.id');

  $query->addField('ecp', 'id', 'id_cupon');
  $query->addField('pic', 'field_coupon_promo_id_comunica_value', 'id_comunicacion_suip');

  $query = $query->isNull('pb.field_coupon_promo_benefit_target_id');
  $query = $query->isNotNull('ps.field_coupon_promo_promo_suip_value');

  $result = $query->execute()->fetchAll();

  return $result;
}
