<?php
function fgo_backend_comercio_guardar_imagen_drupal($fid, $codigoImagen, $id_entidad_padre=NULL) {
  $resultado = null;
  $imagenBoId = null;

  $imagenFid = $fid;

  $imagenBo = new \Fgo\Bo\ComercioImagenBo();
  $imagenes = $imagenBo->buscarImagenesPorIdComercio($id_entidad_padre);

  if (isset($imagenes[0]->datosImagen->fid)){
    $imagenFidBo = $imagenes[0]->datosImagen->fid;
  }else{
    $imagenFidBo = 0;
  }

  if ($imagenFidBo != $fid ){

    $fileimagen = file_load($imagenFid);
    $fileimagen->status = FILE_STATUS_PERMANENT;
    $imagen_data = file_save($fileimagen);

    //La imagen se guarda en BD FGO
    $crearImagen = fgo_backend_guardar_imagen_bo($imagen_data, null, $codigoImagen);
    $imagenFid = $imagen_data->fid;
    $crearImagen->idImagen;
    if ($crearImagen->idImagen != 0){
      /* Guardado de imagenes dentro de la BD de Drupal */
      file_usage_add(file_load($imagenFid), 'fgo_backend', 'imagen_comercio', $imagenFid);
    }

    $resultado = $crearImagen;
  }else{
    $resultado = $imagenes[0];
  }

  return $resultado;
}

function fgo_backend_crear_relacion_comercio_imagen($id, $id_comercio){

    $comercioImagenBo = new \Fgo\Bo\ComercioImagenBo();
    $listadoComercioImagenes = $comercioImagenBo->listarComercioImagenIds();

    $verificar = false;
    if (isset($listadoComercioImagenes[$id_comercio])){
        foreach ($listadoComercioImagenes[$id_comercio] as $imagen_array) {
            if ($imagen_array == $id){
                $verificar = true;
              }
    }
  }

  if (!$verificar){ // Si la relacion no existe, se crea
        $data_para_guardar = new stdClass();
        $data_para_guardar->id_comercio = $id_comercio;
        $data_para_guardar->id_imagen = $id;
        $comercioImagenBo->construir($data_para_guardar);
        $comercioImagenBo->guardar();
      }
}