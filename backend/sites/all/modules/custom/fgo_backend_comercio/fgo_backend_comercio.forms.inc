<?php
function fgo_backend_comercio_editar_comercios_form($form_state, $build) {

  $id = $build["build_info"]["args"][0];

  $comercio = new \Fgo\Bo\ComercioBo($id);

  $fid_imagen = 0;
  $id_imagen = '';

  if (!empty($comercio->imagenes)){
    foreach ($comercio->imagenes as $imagen_temp){
      $fid_imagen = $imagen_temp->datosImagen->fid;
      $id_imagen = $imagen_temp->idImagen;
    }
  }

  $form['#attributes']['enctype'] = "multipart/form-data";

  $form['grupo_imagen_comercio'] = array(
      '#type' => 'fieldset',
      '#title' => t('<strong><i>Imagen Comercio</i></strong>'),
  );

  $form['grupo_imagen_comercio']['idComercio'] = array(
      '#type' => 'hidden',
      '#default_value' => isset($id) ? $id : ''
  );

  $form['grupo_imagen_comercio']['id_imagen'] = array(
      '#value' => $id_imagen,
      '#type' => 'hidden',
  );

  $form['grupo_imagen_comercio']['primer_paso'] = array(
      '#type' => 'fieldset',
  );

  $form['grupo_imagen_comercio']['idImagenFid'] = array(
      '#type' => 'managed_file',
      '#name' => 'custom_content_block_image',
      '#size' => 40,
      '#default_value' => $fid_imagen,
      '#description' => t("Los archivos deben ser menores que <b>2 MB</b>. <br> Tipos de archivo permitidos: <b>png jpg jpeg</b>."),
      '#upload_location' => 'public://',
      '#theme' => 'preview_img',
      '#upload_validators' => array(
          'file_validate_is_image' => array(),
          'file_validate_extensions' => array('png jpg jpeg'),
          'file_validate_size' => array(2 * 1024 * 1024),
      ),
  );

  $form['grupo_imagen_comercio']['guardar'] = array(
      '#type' => 'submit',
      '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
      '#value' => 'Guardar',
      '#submit' => array('fgo_backend_comercio_editar_comercios_form_submit_now')
  );

  return $form;
}

function fgo_backend_comercio_editar_comercios_form_submit_now(&$form, &$form_state){
  $form_state['redirect'] = url('../admin/v2/comercios');
}

function fgo_backend_comercio_editar_comercios_form_validate(&$form, &$form_state){
  $objectComercio = new stdClass();

  $objectComercio->id_comercio = $form_state['values']['idComercio'];
  $idImagenFid = $form_state['values']['idImagenFid'];
  $objectComercio->id_imagen = $form_state['values']['id_imagen'];

  // Si se carga la imagen
  if (!empty($idImagenFid)){
    try {
      $objectReturn = fgo_backend_comercio_guardar_imagen_drupal($idImagenFid, FGO_BACKEND_COD_TIPO_IMAGEN, $objectComercio->id_comercio);
      $objectComercio->id_imagen = $objectReturn->idImagen;


      \Fgo\Bo\ComercioImagenBo::eliminarPorIdComercio($objectComercio->id_comercio);
      $relacionComercioImagen = new \Fgo\Bo\ComercioImagenBo();
      $relacionComercioImagen->construir($objectComercio);

      $relacionComercioImagen->guardar();
    } catch ( \Fgo\Bo\DatosInvalidosException $e) {
      $errores = $e->getErrores();
      foreach ($errores as $error_campo => $error_descripcion){
        form_set_error($error_campo, $error_descripcion);
      }
    }
  } else {
    $relacionComercioImagen = new \Fgo\Bo\ComercioImagenBo();
    $relacionComercioImagen->listarComercioComercioImagen($objectComercio->id_comercio);
    if (!empty($relacionComercioImagen->idComercioImagen)){
      $relacionComercioImagen->eliminar();
      drupal_set_message(t('Se guardo el comercio correctamente.'), 'status');
    } else {
      form_set_error("id_imagen", "Imagen no encontrada");
    }
  }
}
