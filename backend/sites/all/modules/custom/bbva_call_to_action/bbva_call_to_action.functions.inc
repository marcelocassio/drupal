<?php
function _get_call_to_acction_api($id_call_to_action) {

  $call_to_action = entity_load_single('field_collection_item', $id_call_to_action);
  $call_to_action_type = $call_to_action->field_tipo['und'][0]['value'];

  switch ($call_to_action_type) {

    case '0':
      $nav_app = _get_code_nav_app($call_to_action->field_call_to_acction_nav_app['und'][0]['target_id']);
      $code_nav_app = $nav_app->field_nav_code['und'][0]['value'];
      return "nav://menu/" . $code_nav_app;
      break;

    case '1':
      return "nav://experienciaNivel1/id=" . $call_to_action->field_call_to_action_exp_1['und'][0]['tid'];
      break;

    case '2':

      return "nav://experienciaNivel2/id=" . $call_to_action->field_call_to_action_exp_2['und'][0]['tid'];
      break;
    case '3':

      return "nav://experienciaNivel3/id=" . $call_to_action->field_call_to_action_sorteos['und'][0]['target_id'];
      break;

    case '4':
      $id_benefit = $call_to_action->field_call_to_action_benefit['und'][0]['target_id'];
      $id_beneficio_suip = 0;
      if (!empty($id_benefit)) {
        $benefit = entity_metadata_wrapper('benefit', $id_benefit);
        $id_beneficio_suip = (int) $benefit->field_benefit_id->value();
      }

      return "nav://benefit/idSuip=" . $id_beneficio_suip;
      break;

    case '5':

      return "nav://benefitList/rubro=" . $call_to_action->field_call_to_action_rubro['und'][0]['tid'];
      break;

    case '6':
      $id_campania = $call_to_action->field_field_call_to_action_camp['und'][0]['target_id'];
      if (!empty($id_campania)) {
        $campaign = entity_metadata_wrapper('campaign', entity_load_single("campaign", $id_campania));
        $id_campania_suip = $campaign->field_campaign_id->value();
      }
      return "nav://benefitList/campaign=" . $id_campania_suip;
      break;
    case '7':
      return "nav://internalURL/url=" . $call_to_action->field_call_to_acction_link['und'][0]['value'];

    case '8':
      return "nav://wallet";

    default:
      return "";
      break;
  }
}
