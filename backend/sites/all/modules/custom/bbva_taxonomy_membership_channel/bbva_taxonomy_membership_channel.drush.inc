<?php

/*
 * Implements hook_drush_command().
 */
function bbva_taxonomy_membership_channel_drush_command() {

  $items['vuelco-membership_channel'] = array(
    'description' => 'Realiza el vuelco de los canales de pertnencias',
    'aliases' => array('membershipChannelImport'),
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_taxonomy_membership_channel_vuelco_membership_channel() {
  import_membership_channel();
  drupal_set_message("Membber ship vuelco echo", 'status', FALSE);
}
