<?php


function _get_ast_view($order, $sort="desc", $filtros=NULL, $limit=NULL){

  if (null === $sort) {
    $sort = "desc";
  }

  $query = db_select('bbva_ast', 'ba');
  $query->fields('ba', array('id_ast', 'cuit', 'codigo_ast', 'descripcion','descripcion_fantasia','fecha_actualizacion'));

  if (is_array($filtros)) {
    if ($filtros["id_ast"]) {
      $query->condition('ba.id_ast', trim($filtros["id_ast"]));
    }
    if ($filtros["cuit"] != '') {
      $query->condition('ba.cuit', trim($filtros["cuit"]));
    }
    if ($filtros["codigo_ast"] != '') {
      $query->condition('ba.codigo_ast', trim($filtros["codigo_ast"]));
    }
    if ($filtros["descripcion"] != '') {
      $query->condition('descripcion', '%' . db_like(trim($filtros["descripcion"])) . '%', 'LIKE');
    }
    if ($filtros["descripcion_fantasia"] != '') {
      $query->condition('descripcion_fantasia', '%' . db_like(trim($filtros["descripcion_fantasia"])) . '%', 'LIKE');
    }
  }

  $query->orderBy($order, $sort);

  if ($limit != '') {
    $query = $query->extend('TableSort')->extend('PagerDefault')->limit($limit);
  }

  $results = $query->execute()->fetchAll();
  if ($results) {
    return $results;
  }else{
    return null;
  }
}

function _bbva_ast_buscar_datos_ast($id){
    $query = db_select('bbva_ast', 'ba');
    $query = $query->condition('ba.id_ast',$id);
    $query = $query->fields('ba', array('descripcion','descripcion_fantasia'));

    $result = $query->execute()->fetchObject();

    return $result;
}

function _bbva_ast_buscar_datos_ast_by_codigo($codigo) {
    $query = db_select('bbva_ast', 'ba');
    $query = $query->condition('ba.codigo_ast', $codigo);
    $query = $query->fields('ba');

    $result = $query->execute()->fetchObject();

    return $result;
}

function _bbva_ast_cambiar_descripcion_fantasia($id, $descripcion){
  db_update('bbva_ast')
    ->fields(array('descripcion_fantasia' => $descripcion, 'fecha_actualizacion' => date('Y-m-d H:i:s')))
    ->condition('id_ast', $id)
    ->execute();
}

function _bbva_ast_duplicar_tabla_ast_nameuser(){
   if (!db_table_exists('bbva_ast_usuarios_backup')) {
    db_query("CREATE TABLE bbva_ast_usuarios_backup LIKE bbva_ast_usuarios");
  }else{
    db_truncate("bbva_ast_usuarios_backup")->execute();
  }
  db_query("INSERT bbva_ast_usuarios_backup SELECT * FROM bbva_ast_usuarios;");
}

function _bbva_ast_borrar_tabla_ast_nameuser(){
  db_truncate("bbva_ast_usuarios")->execute();
}

function _bbva_ast_insertar_data_ast_nameuser($retorno){
  foreach ($retorno as $value) {

    try {
      $query = db_insert('bbva_ast_usuarios')
        ->fields(array(
          'name_user' => ltrim($value[7],'0'),
          'codigo_ast' => ltrim($value[1],'0')));

      $result = $query->execute();
    }catch (PDOException $e) {
      watchdog('bbva_ast', 'No se pudo guardar el siguiente altamira en ast, %error', array('%error' => htmlspecialchars_decode(print_r("Codigo ast: ".ltrim($value[1],'0'), true))));
    }
  }
}

// Funcion para cache (Seteada en 2x"cache_api.module" y "bbva.module")
function _bbva_ast_get_all_ast_nameuser(){

  $query = db_select('bbva_ast_usuarios', 'bau');
  $query = $query->fields('bau', array('name_user','codigo_ast'));
  $result = $query->execute();

  $asts = array();
  while ($row = $result->fetchObject()){
    $page = (int) $row->name_user % 50;
    $asts[$page][$row->name_user][] = $row->codigo_ast;
  }

  foreach ($asts as $key => $value) {
    cache_set('user_ast_' . $key, $value,'cache_api',CACHE_PERMANENT);
  }
}
