<?php

/*
 * Implements hook_drush_command().
 */
function bbva_ast_drush_command() {

  $items['vuelco-segmento-ast'] = array(
    'description' => 'Realiza el vuelco de las sucursales en suip a drupal',
    'aliases' => array('semento-ast'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  $items['vuelco-ast-nameuser'] = array(
    'description' => 'Realiza el vuelco de las ast en suip relacionadas a los nameuser de drupal',
    'aliases' => array('ast-nameuser'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */

// Imports
function drush_bbva_ast_vuelco_segmento_ast($odate = NULL) {
  drupal_set_message( "Start" . date("ymd"), 'status', FALSE);

  $batch = array(
    'operations' => array(),
    'title' => t('Batch actualizacion de segmentos y ast'),
    'init_message' => t('Batch Segmentos y AST is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_ast_batch_finished',
    'file' => drupal_get_path('module', 'bbva_ast') . '/bbva_ast.batch.inc',
  );


    //Define operaciones
  $batch['operations'][] = array('bbva_ast_batch_segmentos_ast',array($odate));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

// Imports
function drush_bbva_ast_vuelco_ast_nameuser($odate = NULL) {
  drupal_set_message( "Empieza el " . date("ymd"), 'status', FALSE);

  $batch = array(
    'operations' => array(),
    'title' => t('Batch actualizacion de segmentos y ast'),
    'init_message' => t('Batch Segmentos y AST is starting...'),
    'error_message' => t('An error occurred'),
    'finished' => 'bbva_ast_batch_finished',
    'file' => drupal_get_path('module', 'bbva_ast') . '/bbva_ast.batch.inc',
  );

    //Define operaciones
  $batch['operations'][] = array('bbva_ast_batch_ast_nameuser',array($odate));

  // Start the batch job.
  batch_set($batch);
  drush_backend_batch_process();
}

