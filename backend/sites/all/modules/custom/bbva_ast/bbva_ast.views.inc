<?php

/**
 * Views page.
 */

function bbva_ast_view_admin(){

  $sort = $_GET['sort'];

  if ((isset($_GET['filter_cuit'])) && ($_GET['filter_cuit'] != '')) {
    $filtros["cuit"] = $_GET['filter_cuit'];
  }
  if ((isset($_GET['filter_codigo_ast'])) && ($_GET['filter_codigo_ast'] != '')) {
    $filtros["codigo_ast"] = $_GET['filter_codigo_ast'];
  }
  if ((isset($_GET['filter_descripcion'])) && ($_GET['filter_descripcion'] != '')) {
    $filtros["descripcion"] = $_GET['filter_descripcion'];
  }
  if ((isset($_GET['filter_descripcion_fantasia'])) && ($_GET['filter_descripcion_fantasia'] != '')) {
    $filtros["descripcion_fantasia"] = $_GET['filter_descripcion_fantasia'];
  }

  $view_ast = _get_ast_view('id_ast', $sort, $filtros, 100);

  return bbva_ast_create_view($view_ast);
}

function bbva_ast_view_admin_submit($form, &$form_state) {
  $form_state['filters']['cuit'] = $form_state['values']['filter_cuit'];
  $form_state['filters']['codigo_ast'] = $form_state['values']['filter_codigo_ast'];
  $form_state['filters']['descripcion'] = $form_state['values']['filter_descripcion'];
  $form_state['filters']['descripcion_fantasia'] = $form_state['values']['filter_descripcion_fantasia'];
  $form_state['rebuild'] = TRUE;
}

function bbva_ast_create_view($view_ast){

  $header = array(
      array('data' => t('CUIT')),
      array('data' => t('Código AST')),
      array('data' => t('Descripción')),
      array('data' => t('Descripción Fantasía (Editable)')),
      array('data' => t('Fecha Actualización')),
      array('data' => t('Modificar'))
  );

  $form = array();

  $form['#method'] = 'get';

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buscar por:')
  );
  $form['filter']['filter_cuit'] = array(
    '#type' => 'textfield',
    '#title' => t('CUIT: '),
    '#size' => 11,
    '#weight' => '1',
    '#default_value'=> $_GET['filter_cuit'],
  );

  $form['filter']['filter_codigo_ast'] = array(
    '#type' => 'textfield',
    '#title' => t('Código AST: '),
    '#size' => 15,
    '#weight' => '2',
    '#default_value'=> $_GET['filter_codigo_ast'],
  );

  $form['filter']['filter_descripcion'] = array(
    '#type' => 'textfield',
    '#title' => t('Descripción: '),
    '#size' => 20,
    '#weight' => '3',
    '#default_value'=> $_GET['filter_descripcion'],
  );

  $form['filter']['filter_descripcion_fantasia'] = array(
    '#type' => 'textfield',
    '#title' => t('Descripción Fantasía'),
    '#size' => 20,
    '#weight' => '4',
    '#default_value'=> $_GET['filter_descripcion_fantasia'],
  );
  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Buscar'),
    '#weight' => '5'
  );

  // Tomamos los datos
  foreach ($view_ast as $row) {

    if (($row->fecha_actualizacion == 0) || ($row->fecha_actualizacion == '')) {
      $date_sent = "-";
    }else{
      $date_sent = date('d-m-Y', strtotime($row->fecha_actualizacion));
    }

    $data[] = array(
      array('data' => $row->cuit, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->codigo_ast, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->descripcion, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $row->descripcion_fantasia, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => $date_sent, 'align' => 'center', 'style' => 'font-size: 1em'),
      array('data' => array('#type' => 'link', '#title' => t('Editar'), '#href' => 'admin/administracion/ast/editar/'.$row->id_ast))
      );
  }
  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $data,
    '#empty' => t('No hay beneficios disponibles.')
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}
