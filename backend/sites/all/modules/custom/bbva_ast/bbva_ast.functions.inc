<?php

define('BBVA_AST_RUTA_NOMINAS', '/prod/jp00/nominas/');
define('BBVA_AST_ARCH_NOMINAS_NAME', 'fgo_cabecera_conv.txt');
define('BBVA_AST_ARCH_AST_NAMEUSER', 'fgo_ast_clientes.txt');
define('BBVA_AST_ARCH_SEGM_AST_NAME', 'INFORME-TABLA-PARAM-GENERALES-SEG-AST.txt');

// leo archivos de AST
function _actualizar_tabla_ast($ODATE) {
  echo '==Comienzo Import datos Empresas Nominas=='.PHP_EOL;

  $filename = BBVA_AST_RUTA_NOMINAS.BBVA_AST_ARCH_NOMINAS_NAME;


  $dt = new DateTime();
  $date_time_now = $dt->format('Y-m-d H:i:s');

  $nominas_ast_list = array();
  $row=0;
  if (($handle = fopen($filename, "r")) !== FALSE) {

    while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
      $num = count($data);

      $fila = array();
      $row++;

      if ($row == 1) {
        continue;
      }
      for ($c=0; $c < $num; $c++) {
          $fila[$c] = $data[$c];
      }

    	$ast = array('codigoAst' => ltrim($fila[0],'0'), 'descripcion' => trim(utf8_encode($fila[2])), 'cuit' => trim($fila[3]));

        $ast = (object)$ast;
    	if (!is_numeric($ast->cuit)) {
    		echo "Cuit invalido en número de fila " . $row . "Data: ". implode(";", $fila);
    		continue;
    	}
      if (empty($ast->codigoAst)) {
        continue;
      }
      $nominas_ast_list[] = $ast;

    }

    fclose($handle);
  }

  $cantActualizaciones = 0;
  foreach ($nominas_ast_list as $value) {
  	if ($value->codigoAst != '0') {
      try {

      db_merge('bbva_ast')
         ->key(array('cuit' => $value->cuit))
         ->fields(array(
              'cuit' => $value->cuit,
              'descripcion' => $value->descripcion,
              'codigo_ast' => $value->codigoAst,
          ))
       ->execute();
     } catch (PDOException $e) {
        $datos = "Cuit: " . $value->cuit . ', Descripcion: ' .  $value->descripcion . 'CodigoAST: ' . $value->codigoAst;
        $error = "Error al guardar ".$e->getMessage();
        echo $datos;
        throw new Exception($error.'Datos->'.$datos);
     }

  	  $cantActualizaciones++;
  	}
  }
  echo "Cantidad de lineas leidas: ". $row.PHP_EOL;
  echo "Cantidad de nominas actualizadas: ". $cantActualizaciones.PHP_EOL;
}

// leo archivos de AST NAMEUSER
function bbva_ast_read_file_ast_nameuser() {
  $data_picada = array();
  $filename = BBVA_AST_RUTA_NOMINAS.BBVA_AST_ARCH_AST_NAMEUSER;

  $nominas_ast_nameuser_list_delimitadores = array(13,5,50,2,11,20,20,8);
  $handle = fopen($filename, "r");
  if ($handle !== FALSE) {
    while (!feof($handle)) {

      $linea_con_data = batch_messages_parsear_linea_ast($handle);

      $data_picada[] = batch_messages_parsear_linea_array_ast($linea_con_data, $nominas_ast_nameuser_list_delimitadores);
    }
  }

  return $data_picada;
}


/**
* Retorna un listado de objectos segmento ast crudo de archivo
*/
function bbva_ast_leer_listado_segmentos_ast() {

	$filename = BBVA_AST_RUTA_NOMINAS.BBVA_AST_ARCH_SEGM_AST_NAME;
	$delimitador = ";";
	$segmentos_ast_list = array();
	$row=0;
	$formato = 'Y-m-d H:i:s';

	if (($handle = fopen($filename, "r")) !== FALSE) {

	  while (($data = fgetcsv($handle, 0, $delimitador)) !== FALSE) {
	    $num = count($data);
	    $fila = array();
	    $row++;
	    if ($row == 1) {
	    	continue;
	    }
	    for ($c=0; $c < $num; $c++) {
	        $fila[$c] = $data[$c];
	    }
	    $codigoAst = $fila[4];

	    if (isset($codigoAst)) {
            $fechaDesde = DateTime::createFromFormat($formato, trim($fila[18]) . ' 00:00:00');
            $fechaHasta = DateTime::createFromFormat($formato, trim($fila[19]) . ' 00:00:00');
	    	$segmento_ast = array(
              'parametro' => trim($fila[2]),
     		 'codigoAst' => ltrim($fila[4],'0'),
		 'fechaDesde' => $fechaDesde,
		 'fechaHasta' => $fechaHasta,);
	        $segmento_ast = (object)$segmento_ast;
		    $segmentos_ast_list[] = $segmento_ast;
	    }
	  }

	  fclose($handle);
	} else {
	  echo "Hubo un error al abrir el archivo ". $filename.PHP_EOL;
	}

	return $segmentos_ast_list;
}

function batch_messages_parsear_linea_ast($file_listado) {
  $line_of_text_users = fgets($file_listado);
  $line_of_text_users = str_ireplace("\x0D", "", $line_of_text_users);
  $line_of_text_users = eregi_replace("[\n|\r|\n\r]", "", $line_of_text_users);
  $user_array = explode(';', $line_of_text_users);
  return $user_array;
}

function batch_messages_parsear_linea_array_ast($data, $delimitadores) {
  $suma_delimitadores = 0;

  foreach ($delimitadores as $key => $value) {
    $resultado = substr($data[0], $suma_delimitadores, $value);
    if ($resultado) { $total[] = trim($resultado); }
    $suma_delimitadores += $value;
  }
  return $total;
}

/*
* Consulta los segmentos y sus ASTs con vigencia.
* @param Listado de \BBVA\Segmento
*/
function bbva_ast_get_segmentos_ast() {
  $hoy = new \DateTime("midnight");
  $listado_segmento_ast = bbva_ast_leer_listado_segmentos_ast();
  $listado_segmentos = array();
  foreach ($listado_segmento_ast as $fila_segmento_ast) {
    $codigoSegmento = substr($fila_segmento_ast->parametro, 0, 3);
    if ($fila_segmento_ast->fechaHasta >= $hoy) {
      if (!array_key_exists($codigoSegmento, $listado_segmentos)) {
      	$listado_segmentos[$codigoSegmento] = new \BBVA\Segmento($codigoSegmento);
      }

      $segmento = $listado_segmentos[$codigoSegmento];
      $segmentoAst = new \BBVA\SegmentoAst();
      $segmentoAst->build($fila_segmento_ast);
      $segmento->addSegmentoAst($segmentoAst);

      $listado_segmentos[$codigoSegmento] = $segmento;
    }
  }
  return $listado_segmentos;
}

/**
 * Busco los id de beneficios según los AST's del usuario
 */
function bbva_ast_beneficios_segun_ast($aid) {
  //SE DESACTIVA CACHE SET PARA MITIGAR CUELLO DE BOTELLA EN MEMCACHE - Huallpa Nestor 15/11/2018
  return array();

  $id_beneficios_cliente_ast = array();
  $page = (int) $aid % 50;
  $cache_key_user_id = 'user_ast_'.$page;
  $altamira_asts_cache = cache_get($cache_key_user_id, "cache_api");

  if (isset($altamira_asts_cache->data)) {
    if (array_key_exists($aid, $altamira_asts_cache->data)) {
      $asts = $altamira_asts_cache->data[$aid];

      $ast_beneficios_cache = cache_get('jsonAstBeneficio', 'cache_api');
      $ast_beneficios = $ast_beneficios_cache->data;

      foreach ($asts as $codigo_ast) {
        if(array_key_exists($codigo_ast, $ast_beneficios)){
          $id_beneficios_cliente_ast = array_merge($id_beneficios_cliente_ast, $ast_beneficios[$codigo_ast]);
        }
      }
    }
  }

  return $id_beneficios_cliente_ast;

}

