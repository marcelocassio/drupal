<?php

function bbva_ast_batch_segmentos_ast($ODATE) {
  try {
    $retorno = _actualizar_tabla_ast($ODATE);
  } catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
    exit(1);
  }
}

function bbva_ast_batch_ast_nameuser($ODATE) {
  try {
    // leo archivos de AST NAMEUSER
    $retorno = bbva_ast_read_file_ast_nameuser();

    // Crear y copiar tabla
    _bbva_ast_duplicar_tabla_ast_nameuser();

    //Borrar  tabla AST NAMEUSER
    _bbva_ast_borrar_tabla_ast_nameuser();

    //Se inserta la data dentro de la tabla bbca_ast_usuarios
    _bbva_ast_insertar_data_ast_nameuser($retorno);

    //SE DESACTIVA CACHE SET PARA MITIGAR CUELLO DE BOTELLA EN MEMCACHE - Huallpa Nestor 15/11/2018
    //Cache AST NAMEUSER
    //cache_set('jsonAstUsuarios',_bbva_ast_get_all_ast_nameuser(),'cache_api',CACHE_PERMANENT);
    //drupal_set_message(t("Ast-Usuarios Cacheado"), 'status', FALSE);

  } catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
    exit(1);
  }   
}
