<?php
namespace BBVA;

class Segmento {

	public $codigoSegmento;
	public $listadoAst;

	public function __construct($codigoSegmento = NULL) {
		$this->codigoSegmento = $codigoSegmento;
	}

	public function addSegmentoAst($segmentoAst) {
		$this->listadoAst[] = $segmentoAst;
	}
}


class SegmentoAst {
	public $codigoAst;
	public $fechaDesde;
	public $fechaHasta;

	public function build($fila_segmento_ast) {
		$this->codigoAst = trim($fila_segmento_ast->codigoAst);
		$this->fechaDesde = $fila_segmento_ast->fechaDesde;
		$this->fechaHasta = $fila_segmento_ast->fechaHasta;
		return $this;
	}
}

