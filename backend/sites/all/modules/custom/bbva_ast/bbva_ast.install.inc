<?php
/**
* Creacion de tabla bbva_ast para empresas nominas
**/
function bbva_ast_schema() {
  $schema = array();

  $schema['bbva_ast'] = array(
    'description' => 'Datos de empresas AST',
    'fields' => array(
      'id_ast' => array(
        'description' => 'Clave primaria.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'cuit' => array(
        'description' => 'Cuit de la empresa',
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
      ),
      'codigo_ast' => array(
        'description' => 'Código ast',
        'type' => 'varchar',
        'length' => 15,
        'not null' => FALSE,
      ),
      'descripcion' => array(
        'description' => 'Descripción/Nombre de la empresa',
        'type' => 'varchar',
        'length' => 200,
        'not null' => FALSE,
      ),
      'descripcion_fantasia' => array(
        'description' => 'Descripción/Nombre fantasía de la empresa',
        'type' => 'varchar',
        'length' => 200,
        'not null' => FALSE,
      ),
       'fecha_actualizacion' => array(
        'description' => 'Fecha de activacion',
        'type' => 'varchar',
        'mysql_type' => 'datetime',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id_ast'),
    'indexes' => array(
      'index_codigo_ast' => array('codigo_ast'),
    ),
  );


  $schema['bbva_ast_usuarios'] = array(
    'description' => 'Relación de AST con name_user',
    'fields' => array(
       'id' => array(
        'description' => 'Clave primaria.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'name_user' => array(
        'description' => 'Numero de cliente del usuario (Altamira)',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'codigo_ast' => array(
        'description' => 'Código de AST (No ID)',
        'type' => 'varchar',
        'length' => 15,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'index_codigo_ast' => array('codigo_ast'),
      'index_name_user' => array('name_user'),
    ),
     'unique keys' => array(
      'codigo_ast_name_user' => array('codigo_ast', 'name_user'),
    ),
  );

  if (!db_table_exists('bbva_ast')) {
    db_create_table('bbva_ast', $schema['bbva_ast']);
  }
  if (!db_table_exists('bbva_ast_usuarios')) {
    db_create_table('bbva_ast_usuarios', $schema['bbva_ast_usuarios']);
  }
}
