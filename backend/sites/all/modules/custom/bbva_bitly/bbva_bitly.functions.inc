<?php

/**
 * The URI of the bitly OAuth endpoints.
 * @throws Exception
 */
define('bitly_oauth_api', 'https://api-ssl.bitly.com/v3/');

function http_bitly($bitlyconnector) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $bitlyconnector);
  curl_setopt($ch, CURLOPT_HTTPGET, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Accept: application/json'
  ));
  curl_setopt($ch, CURLOPT_PROXY, variable_get('bitly_proxy_ip'));
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec($ch);
  curl_close($ch);

  if (json_decode($response)) {
    $response = json_decode($response);

    if (in_array( $response->status_code, array(200, 304))) {
      //$response = ($response);
    }else if ($response->status_code == HTTP_REQUEST_TIMEOUT){
      watchdog('bitly_error', 'Time out');
      throw new Exception("Time out");
      $response = "Error creando bitly";
    }else {
      $error_text = t('Http resquets Fail [ url: %url | error : %error, %message]', array('%error'=>$response->status_code, '%message' => $response, '%url' => $bitlyconnector));
      watchdog('bitly_error', $error_text);
      throw new Exception($error_text);
    }
  }else{
    $response = "Error creando bitly";
    throw new Exception("Error desconocido con realizar http_bitly");
  }

  return $response;
}
