<?php
/*
Credenciales para Bitly:
Login : bbvaglobal
Key: R_b65b8744e54343cfaff7115a92962285
Se pueden encontrar aqui: http://bit.ly/a/your_api_key
 */

  /** Esto le da al admin permiso para acceder al modulo
   * Implements hook_permission().
  */
function bbva_bitly_permission() {
  return array(
    'permisos_bitly' => array(
    'title' => t('BBVA Bitly'),
    'description' => t('Permisos para acceder al config de bitly'),
    ),
  );
}

/**
 * Admin settings page.
 */
function bbva_bitly_admin($form, $form_state) {

  $form['bitly_keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bit.ly API Access Token'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['bitly_keys']['bitly_access_token'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('bitly_access_token', '2cbc242bf81a1b71c48a107c7ecabdc5f7d3906a'),
    '#description' => t('Este campo es case-sensitive. Se puede obtener desde este link: <a href="https://bitly.com/a/oauth_apps">Link Bitly</a>'),
  );

  /* Dominios */
  $form['bitly_url'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bit.ly Base URL'),
    '#description' => t("Elegir el dominio para las URL's cortas"),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['bitly_url']['bbva_bitly_custom_url'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('bbva_bitly_custom_url', 'bbva.info'),
    '#options' => array('bbva.info' => 'bbva.info', 'bit.ly' => 'bit.ly', 'j.mp' => 'j.mp'),
  );
  /* Proxy */
  $form['bitly_proxy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bit.ly Proxy'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['bitly_proxy']['bitly_proxy_ip'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('bitly_proxy_ip', '172.29.92.203:8082'),
    '#description' => t('Cambiar IP dependiendo del entorno.'),
  );

  return system_settings_form($form);
}
