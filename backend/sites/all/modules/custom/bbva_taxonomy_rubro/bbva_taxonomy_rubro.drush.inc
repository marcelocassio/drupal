<?php

/*
 * Implements hook_drush_command().
 */
function bbva_taxonomy_rubro_drush_command() {

  $items['rubro-import'] = array(
    'description' => 'Realiza el vuelco de los rubros en suip a drupal',
    'aliases' => array('rubrosImport'),
  );
  $items['rubro-delete'] = array(
    'description' => 'Realiza el vuelco de los rubros en suip a drupal',
    'aliases' => array('rubrosDelete'),
  );
  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_bbva_taxonomy_rubro_rubro_import() {
  import_rubro_suip();
  drupal_set_message("Rubros  Import Echo", 'status', FALSE);
}

function drush_bbva_taxonomy_rubro_rubro_delete() {
  _delete_category();
}