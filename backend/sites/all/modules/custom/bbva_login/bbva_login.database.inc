<?php

/*No modificar estos valores porque van para BI*/
define('BBVA_LOGIN_CANAL_NINGUNO','NIN');
define('BBVA_LOGIN_CANAL_WEB','WEB');
define('BBVA_LOGIN_CANAL_APP','APP');


/**
 * Set the atributte canal_logueo to on
 * @param uid The user id on drupal
 * @param canalType It's the canal_logeo 
 */
function _update_user_set_canal_logueo($uid, $canalType) {
  $existingUser = user_load($uid);

  if ($existingUser) {
    
    $canalCod = 0;
    if (BBVA_LOGIN_CANAL_WEB == $canalType) {
      $canalCod = 1;
    } else if (BBVA_LOGIN_CANAL_APP == $canalType) {
      $canalCod = 2;
    }   

    $edit = array();
    $edit['field_profile_canal_logueo']['und'][0]['value'] = $canalCod;

    user_save($existingUser, $edit);
  }
    else {
    throw new Exception(PROFILE_ERR_GENERICO);
  }
}