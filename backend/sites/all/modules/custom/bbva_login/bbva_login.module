<?php


require_once 'bbva_login.functions.inc';
require_once 'bbva_login.class.inc';
require_once 'bbva_login.database.inc';


/**
 * Implementation of hook_init
 */

function bbva_login_init() {
  drupal_add_js(drupal_get_path('module', 'bbva_login') . '/theme/jquery.simplemodal.1.4.4.min.js');
  drupal_add_js(drupal_get_path('module', 'bbva_login') . '/theme/bbva_login.js');
  drupal_add_css(drupal_get_path('module', 'bbva_login') . '/theme/bbva_login.css');
}


/**
 * Implements hook_menu().
 */
function bbva_login_menu() {


  $items['admin/config/bbva/login_setting'] = array(
    'title' => 'BBVA Login Setting',
    'description' => 'Configuration for Login Module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bbva_login_setting_form'),
    'access arguments' => array('administer users'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['push/mensajero'] = array(
    'title' => t('Mensajero Push'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bbva_login_push_mensajero_form'),
	'access callback' => 'user_access',
    'access arguments' => array('access content Marketing'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function bbva_login_user_has_role($user, $roles = array()) {
  foreach ($roles as $role) {
    if (in_array($role, $user->roles)) {
      return TRUE;
    }
  }
  return FALSE;
}

function bbva_login_push_mensajero_form($form, &$form_state) {
  global $user;

  $form['title'] = array (
	'#type' => 'markup',
	'#title' => t('Envio de mensajeria push para usuario Anonimos'),
	'#markup' => '<h3>Mensajero push para usuario Anonimos</h3>',
	'#weight' => 0,
  );

  $form['plataforma'] = array(
   '#type' => 'select',
   '#title' => t('Plataforma'),
   '#options' => array( 0 => t('seleccione'), 1 => t('Android'), 2 => t('IOS'), 3 => t('Ambos'), ),
   '#default_value' => array(0),
   '#required' => TRUE,
   '#description' => t('Plataforma movíl destion'),
  );

  $form['fecha_desde'] = array(
    '#type' => 'date',
    '#title' => 'Fecha desde',
    '#description' => 'Fecha desde registración del usuario en el mensajero push',
    '#required' => TRUE,
  );

  $form['fecha_hasta'] = array(
    '#type' => 'date',
    '#title' => 'Fecha hasta',
    '#description' => 'Fecha hasta registración del usuario en el mensajero push',
    '#required' => TRUE,
  );

  $form['mensaje'] = array(
    '#title' => t('Mensaje'),
    '#type' => 'textfield',
    '#description' => t("Mesaje a enviar"),
    '#required' => TRUE,
    '#size' => 160,
    '#maxlength' => 160,
  );

  $form['budget'] = array(
    '#type' => 'textfield',
    '#title' => t('Budget'),
    '#size' => 5,
    '#maxlength' => 3,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Enviar'),
    );

  $form['#validate'][] = 'bbva_login_push_mensajero_form_validate';

  $form['#submit'][] = 'bbva_login_push_mensajero_form_envio';

  return $form;
}


function bbva_login_push_mensajero_form_validate($form, &$form_state) {
	$plataforma = $form_state['values']['plataforma'];
	$budget = $form_state['values']['budget'];
	if ($plataforma == 0) {
		form_set_error('plataforma', 'Debe seleccionar una plataformas');
	} else if (!is_numeric($budget)) {
		form_set_error('budget', 'El campo budget debe ser numericos');
	}
}

function bbva_login_push_mensajero_form_envio($form, &$form_state) {

	$plataforma_id = $form_state['values']['plataforma'];
	$fecha_desde_control = $form_state['values']['fecha_desde'];
	$fecha_hasta_control = $form_state['values']['fecha_hasta'];
	$mensaje = $form_state['values']['mensaje'];
	$budget = $form_state['values']['budget'];

	$plataforma = "";

	if ($plataforma_id == 1) {
		$plataforma = "ANDROID";
	} else if ($plataforma_id == 2) {
		$plataforma = "IOS";
	} else if ($plataforma_id == 3) {
		$plataforma = "";
	}

	$fecha_desde = $fecha_desde_control['year'] .'/'. $fecha_desde_control['month'] . '/' . $fecha_desde_control['day'] . " 00:00";
	$fecha_hasta = $fecha_hasta_control['year'] .'/'. $fecha_hasta_control['month'] . '/' . $fecha_hasta_control['day'] . " 23:59";

	try {
		$respuesta = bbva_login_ws_push_send_broadcast($plataforma, $fecha_desde, $fecha_hasta, $mensaje, $budget);
		if ($respuesta['codigo_retorno'] == '0') {
			drupal_set_message($respuesta['descripcion']);
		} else {
			form_set_error('', $respuesta['descripcion']);
		}
	} catch (Exception $e) {
		form_set_error('', $e->getMessage());
	}
}


function bbva_login_setting_form($form, &$form_state) {

   $form['group-endpoint'] = array(
	'#type' => 'fieldset',
	'#title' => t('Configuración de endpoint para login y registro.'),
	'#weight' => 1,
    '#collapsible' => TRUE,
  );



  $form['group-endpoint']['fnet_host'] = array(
    '#title' => t('Frances Net Web Host'),
    '#description' => t('Nombre de equipo ó ip donde esta hosteado Francés Net'),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 50,
    '#default_value' => variable_get('fnet.host', 'http://'),
  );

  $form['group-endpoint']['fnet_host_web'] = array(
    '#title' => t('Frances Net Host'),
	'#description' => t('Nombre de equipo ó ip donde esta hosteado Francés Net (Solo para Olvido mi Clave)'),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 50,
    '#default_value' => variable_get('fnet.host.web', 'http://'),
  );

  $form['group-endpoint']['fgo_middleware_host'] = array(
    '#title' => t('Middleware Fgo Host'),
	'#description' => t('Nombre de equipo ó ip donde esta hosteado el middleware de FGO. No anteponer protocolo'),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 50,
    '#default_value' => variable_get('suip.middleware.host.name', ''),
  );

  $form['group-endpoint']['fmovil_host'] = array(
    '#title' => t('Francés Móvil Host'),
	'#description' => t('Nombre de equipo ó ip donde esta hosteado el Frances Móvil '),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 50,
    '#default_value' => variable_get('fmovil.host.ws', ''),
  );

  $form['group-endpoint']['fnet_sitio'] = array(
    '#title' => t('Frances Net URI'),
	'#description' => t('URL de Francés Net '),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 50,
    '#default_value' => variable_get('fnet.sitio', ''),
  );

  $form['group-endpoint']['push_host'] = array(
    '#title' => t('Push Server Host'),
	'#description' => t('Nombre de equipo ó ip del servidor de notificaciones push. No anteponer protocolo'),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 50,
    '#default_value' => variable_get('bbva_push.host', ''),
  );


  $form['group-link-responsive'] = array(
	'#type' => 'fieldset',
	'#description' => t('Link utilizados en la aplicación de frances go.'),
	'#weight' => 2,
	'#title' => t('Links Apps Francés Go.'),
    '#collapsible' => TRUE,
  );

  $form['group-link-responsive']['recover-cliente-uri'] = array(
	'#title' => t('Recupero de usuario y clave digital Cliente'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_login_recover_cliente_url', ''),
  );

  $form['group-link-responsive']['new-cliente-uri'] = array(
    '#title' => t('Generacion de clave digital Cliente'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_login_new_cliente_url', ''),
  );

  $form['group-link-responsive']['fnet-contacto-url'] = array(
	'#title' => t('URL Contacto'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_login_fnet_contacto_url', ''),
  );

  $form['group-link-responsive']['fgo-comercio-url'] = array(
    '#title' => t('URL Comercio'),
    '#type' => 'textfield',
    '#maxlength' => 100,
    '#size' => 100,
    '#default_value' => variable_get('bbva_login_fgo_comercio_url', ''),
  );


  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#weight' => 3,
  );

  return $form;
}

///  Submits

function bbva_login_setting_form_submit(&$form, &$form_state){
  if($form['#form_id'] == "bbva_login_setting_form") {
    // Socials variables
    variable_set('fnet.host', $form_state['values']['fnet_host']);
    variable_set('fnet.host.web',$form_state['values']['fnet_host_web']); // Usada para las llamadas desde JavaScript.
    variable_set('suip.middleware.host.name', $form_state['values']['fgo_middleware_host']);
    variable_set('fmovil.host.ws', $form_state['values']['fmovil_host']);
    variable_set('fnet.sitio', $form_state['values']['fnet_sitio']);
    variable_set('bbva_push.host', $form_state['values']['push_host']);
    variable_set('bbva_login_recover_cliente_url', $form_state['values']['recover-cliente-uri']);
    variable_set('bbva_login_new_cliente_url', $form_state['values']['new-cliente-uri']);
    variable_set('bbva_login_fnet_contacto_url', $form_state['values']['fnet-contacto-url']);
    variable_set('bbva_login_fgo_comercio_url', $form_state['values']['fgo-comercio-url']);
  }

}


function bbva_login_form_alter(&$form, &$form_state, $form_id) {
  switch($form_id){
    case 'user_login_block';

      $documentTypeKeyValue = bbva_login_get_document_long_type_list();

	  $tipo_cliente_option = array(0 => "CLIENTE", 1 => "NO CLIENTE");

      $gender_html = '<div id="gender">';
      $gender_html .= '  <div id="g-masculino" class="gender">';
      $gender_html .= '    <b>M</b>';
      $gender_html .= '  </div>';
      $gender_html .= '  <div id="g-femenino" class="gender">';
      $gender_html .= '    <b>F</b>';
      $gender_html .= '  </div>';
      $gender_html .= '</div>';

	  // FIELDS

	  $form['tipo_cliente'] = array('#type' => 'hidden', '#weight' => -4,);

      $form['type_doc'] = array('#type' => 'select',
      '#title' => t('Tipo y número de documento'),
      '#maxlength' => 15,
      '#weight' => -3,
      '#required' => TRUE,
	  '#prefix' => '<div id="login_form_full"><div id="tipo-number-content">',
      '#options' => $documentTypeKeyValue,
      '#default_value' => 'DNI',
      );

      $form['document_number'] = array('#type' => 'textfield',
      '#maxlength' => 11,
      '#weight' => 1,
      '#required' => TRUE,
      );


      unset($form['document_number']['#title']);



	  $gender = array('M' => t('M'), 'F' => t('F'));
      $form['sex'] = array(
      '#title' => 'Sexo',
	    '#type' => 'radios',
	    '#options' => $gender,
		  '#weight' => 2,
	    '#prefix' => '<div class="form-type-radios bbva-field-sex bbva-field-hide">',
	    '#suffix' => $gender_html . '</div></div>'
      );


	  $form['user_name_digital'] = array('#type' => 'textfield',
        '#title' => t('Usuario Digital'),
        '#maxlength' => 15,
        '#weight' => 3,
        '#maxlength' => 20,
        '#size' => 20,
        '#prefix' => '<div id="bbva-clave-usuario">',
      );


      $form['pass']['#title'] = t('Clave Digital');
      $form['pass']['#weight'] = 4;
      $form['pass']['#maxlength'] = 8;
      $form['pass']['#size'] = 20;
	  $form['pass']['#required'] = FALSE;
	  unset($form['pass']['#attributes']['tabindex']);

      unset($form['pass']['#description']);
      unset($form['name']);

      $form['password']['#markup']  = '<div id="recover_pass_1" class="recover_pass">';
      $form['password']['#markup'] .= '  <a href="#" class="trigger-margin-region-slideshow" onclick="iniciarIdentificacionPositiva(); return false;">¿Olvidaste tu usuario o tu clave digital?</a>';
      $form['password']['#markup'] .= '</div>';
      $form['password']['#weight'] = 5;

      $form['password_new']['#markup'] = '<div id="recover_pass_2" class="recover_pass"><a href="javascript:void(null)" class="trigger-margin-region-slideshow">¿Todavía no tenés tu Clave?</a></div>';
      $form['password_new']['#weight'] = 6;
  	  $form['password_new']['#suffix'] = '</div>';


      $form['pass_nocliente'] = array(
        '#type' => 'password',
          '#title' => t('Contraseña'),
          '#maxlength' => 8,
          '#size' => 17,
  	  '#prefix' => '<div id="bbva-clave-no-cliente">',
  	  '#weight' => 7
      );

  	  $form['password_nocli']['#markup']  = '<div id="recover_pass_nocliente" class="recover_pass">';
      $form['password_nocli']['#markup'] .= '  <a href="#" class="trigger-margin-region-slideshow">¿Olvidaste tu contraseña?</a>';
      $form['password_nocli']['#markup'] .= '</div>';

      $form['password_nocli']['#weight'] = 8;
  	  $form['password_nocli']['#suffix'] = '</div>';

      $form['remember_me']['#title'] = 'Recordarme en este Equipo';
      $form['remember_me']['#weight'] = 9;

      $form['actions']['submit']['#value'] = t('Ingresar');
      $form['actions']['#weight'] = 10;
      $form['#submit'][] = 'bbva_user_login_redirect';

      _bbva_login_add_ajax($form, $form_id);

      $form['login_error'] = array(
        '#markup' => '<div class="login_error"></div>',
        '#weight' => 14,
      );

      $form['#validate'] = array('bbva_login_validate_clave_digital');


      break;

  }
}

/**
 * Implements hook_theme().
 */
function bbva_login_theme() {
  return array(
    'ajax_bbva_login_wrapper' => array(
      'render element' => 'form',
      'file' => 'bbva_login.theme.inc',
    ),
  );
}



function _bbva_login_add_ajax(&$form, $form_id) {

  $form['actions']['submit']['#ajax'] = array(
    'callback' => 'bbva_login_user_login_ajax_callback',
    'wrapper' => 'user-login-form',
    'effect' => 'fade',
  );



}

function bbva_login_user_login_ajax_callback(&$form, &$form_state) {
  if (!form_get_errors()) {
    $commands   = _bbva_login_execute_form($form_state);
    return array('#type' => 'ajax', '#commands' => $commands);
  }

  // Reload form if it didn't pass validation.
  return $form;
}

/**
 * Executes form.
 */
function _bbva_login_execute_form($form_state) {


  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  $commands = array();

  $redirect_url = $form_state['redirect'];

  $commands[] = ctools_ajax_command_redirect($redirect_url);

  return $commands;
}

function bbva_user_login_redirect(&$form, &$form_state) {
  $uid = $form_state['uid'];

  $es_cliente = false;
  if ($form_state['values']['tipo_cliente'] == TIPO_CLIENTE) {
    $es_cliente = true;
  }

  if ($es_cliente) {
    lists_session('BBVA_LOGIN_TIPO_CLIENTE', TIPO_CLIENTE);
  } else {
    lists_session('BBVA_LOGIN_TIPO_CLIENTE', TIPO_NOCLIENTE);
  }

  if (!$es_cliente && _is_require_change_pass($uid)) {
	$form_state['redirect'] = 'user/' . $uid  . '/change-password';
  } else {
	$form_state['redirect'] = drupal_get_destination();
  }
}


/**
*
*/
function bbva_login_validate_clave_digital(&$form, &$form_state) {
  // Mantiene oculto el ingreso de clave no cliente

  $url_suscription = "subscription";
  $error_message = "";
  $es_cliente = false;
  if ($form_state['values']['tipo_cliente'] == TIPO_CLIENTE) {
    $es_cliente = true;
  }

  if ($es_cliente) {
    $form['pass_nocliente']['#prefix'] ='<div id="bbva-clave-no-cliente" style="display:none;">';
  } else {
    $form['user_name_digital']['#prefix'] ='<div id="bbva-clave-usuario" style="display:none;">';
  }

  $document_type = $form_state['values']['type_doc'];
  $document_number = $form_state['values']['document_number'];
  $digital_user = $form_state['values']['user_name_digital'];
  $pass =  $form_state['values']['pass'];
  $pass_nocliente =  $form_state['values']['pass_nocliente'];
  $sex_field =  $form_state['values']['sex'];

  $hay_errores = false;
  if (empty($document_type)) {
    $hay_errores = true;
    $error_message = t("Seleccione el tipo de documento");
  } else if (empty($document_number)){
    $hay_errores = true;
    $error_message = t("Ingrese su Número de documento");
  }
  if ($hay_errores) {
      $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
      return $form;
  }



  if (strlen(trim($document_number)) > 0) {
    if (!is_numeric($document_number)) {
      form_set_error('document_number', 'Solo se permite numeros');
      return $form;
    }
  }
  $bbvaTimerRegister = new BbvaTimerRegister();

  try {
    $bbvaTimerRegister->addTimeMark('Comienzo');

	if ($es_cliente) {
  	 	$login_status = bbva_login_do_login_cliente($document_type, $document_number, $digital_user, $pass, $sex_field);
  	} else {
  	 	$login_status = bbva_login_do_login_no_cliente($document_type, $document_number, $pass_nocliente, $sex_field);
  	}

  	if ($login_status->codigo == LOGIN_OK) {
  		$form_state['uid'] = $login_status->uid;
  		watchdog('user', 'uid %nro Encontrado.', array('%nro'=>$login_status->uid));
      _update_user_set_canal_logueo($login_status->uid, BBVA_LOGIN_CANAL_WEB);
  	} else {
  		if ($login_status->codigo == LOGIN_RET_NO_SUSCRIPTO){
  			$error_message = t(LOGIN_RET_DATOS_INCORRECTO_MSG);
		  } else if ($login_status->codigo == LOGIN_RET_REQUIERE_CONIF){
  			$error_message = t('Para acceder necesita confirmar su registro mandando la palabra OK al 2282.');
  		} else if ($login_status->codigo == LOGIN_RET_DUPLICADO){
  			$form['sex']['#prefix'] = '<div class="form-type-radios bbva-field-sex">';
  			$form['sex']['#required'] = TRUE;
  			$error_message = t('Seleccione su sexo');
  		} else if ($login_status->codigo == LOGIN_RET_SIN_CD){
  			$error_message = t($login_status->descripcion);
  		} else if ($login_status->codigo == LOGIN_RET_DATOS_INCORRECTO){
  			$error_message = $login_status->descripcion;
  			if (empty($error_message)) {
  				$error_message = LOGIN_RET_DATOS_INCORRECTO_MSG;
  			}
      } else if ($login_status->codigo == LOGIN_RET_FLOOD_DETECT){
        $error_message = $login_status->descripcion;
  		} else {
  			$error_message = "Esta transacción no puede ser realizada en este momento.";
  		}
  		form_set_error('login_error', $error_message);
  		$form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
  		return $form;
  	}


  } catch (Exception $e) {
    $bbvaTimerRegister->addTimeMark('Fin');
    $bbvaTimerRegister->logResult();
    $error_message = "Esta transacción no puede ser realizada en este momento.";
    form_set_error('login_error', $error_message);
	  $form['login_error']['#markup']= '<div class="error_login">'.$error_message.'<div>';
    return $form;
  }

}


