<?php

/**
 * @file
 * Theming functions for BBVA_LOGIN module.
 */

/**
 * Returns HTML for a wrapped Ajax Login form.
 */
function theme_ajax_bbva_login_wrapper($variables) {
  $form = $variables['form'];
  $html_id = $form['actions']['submit']['#ajax']['wrapper'];
  return '<div id="' . $html_id . '">' . $form['#children'] . '</div>';
}
