(function($) {
  var MSG_ERROR_GENERICO = 'En este momento no podemos ejecutar la operaci&oacute;n';
  var MSG_ERROR_A_FNET = 'Puede obtener su usuario y clave digital desde www.bbvabancofrances.com.ar';
  var MSG_ERROR_NO_OK = 'Lamentablemente sus respuestas no coinciden con la información que nos proporcionó oportunamente. Aguarde unos minutos para volver a responder las preguntas o consulte en su sucursal por otros medios para obtener su clave digital.';
  var MSG_ERROR_PREGUNTAS_RECUPERO_CODE = '36';
  var MSG_ERROR_PREGUNTAS_BLOQUEADAS_CODE = '2';
  var MSG_ERROR_PREGUNTAS_RECUPERO = 'La respuesta ingresada es incorrecta. Ante reiteradas respuestas incorrectas sus Preguntas de Recuperación quedarán inhabilitadas y deberá usar otro mecanismo para identificarse.';
  var MSG_ERROR_PREGUNTAS_BLOQUEADAS = 'Recupero con preguntas inhabilitado por reiterados intentos de respuesta erróneos. Presione Continuar para recuperar su Usuario y Clave Digital usando otro mecanismo de identificación.';
  var TIPO_CLIENTE = 'CLIENTE';
  var TIPO_NO_CLIENTE = 'NO_CLIENTE';

  var MSG_EXITOSO_ENRROLAMIENTO = 'Tu clave fue cambiada con éxito. Ingresa y seguí disfrutando de los beneficios personalizados de Go.';

  var RECUPERO_CLAVE_CLIENTE_MSG = 'Recuper&aacute; tu usuario y clave digital';
  var GENERACION_CLAVE_CLIENTE_MSG = 'Generá tu usuario y clave digital';

  // Respuestas I+
  var RESPUESTA_OK = 0;
  var RESPUESTA_NO_OK = 1;
  var HAY_PREGUNTAS = 2;

  var MARCA_RECUPERA_CLAVE_DESDE_APP = false;

  /**datos para i+**/
  var postDataRecuperoClaveCliente=0;
  var formURLRecuperoClaveCliente=0;


  function marcar_femenino() {
    $('#tipo-number-content #gender #g-masculino').css('background-color','#b4b4b4');
    $('#tipo-number-content #gender #g-femenino').css('background-color','#0079C1');
    $("#tipo-number-content .form-item-sex input[value=M]").removeAttr('checked');
    $("#tipo-number-content .form-item-sex input[value=F]").attr('checked', 'checked');
  }
  function marcar_masculino() {
    $('#tipo-number-content #gender #g-femenino').css('background-color','#b4b4b4');
    $('#tipo-number-content #gender #g-masculino').css('background-color','#0079C1');
    $("#tipo-number-content .form-item-sex input[value=F]").removeAttr('checked');
    $("#tipo-number-content .form-item-sex input[value=M]").attr('checked', 'checked');
  }

  function marcar_femenino_idem_pos() {
    $('.identificacion-positiva-form #g-masculino').css('background-color','#b4b4b4');
    $('.identificacion-positiva-form #g-femenino').css('background-color','#0079C1');
    $(".identificacion-positiva-form #gender-m").removeAttr('checked');
    $(".identificacion-positiva-form #gender-f").attr('checked', 'checked');
  }
  function marcar_masculino_idem_pos() {
    $('.identificacion-positiva-form #g-femenino').css('background-color','#b4b4b4');
    $('.identificacion-positiva-form #g-masculino').css('background-color','#0079C1');
    $(".identificacion-positiva-form #gender-f").removeAttr('checked');
    $(".identificacion-positiva-form #gender-m").attr('checked', 'checked');
  }

  function manage_gender() {
    // Checkbox Gender Suscripcion
    var check_F = $("#tipo-number-content .form-item-sex input[value=F]").attr('checked');
    var check_M = $("#tipo-number-content .form-item-sex input[value=M]").attr('checked');

    if (check_M == 'checked') {
        marcar_masculino();
    } else if (check_F == 'checked') {
        marcar_femenino();
    }

    $("#tipo-number-content #gender #g-masculino").click(function() { // Masculino
        marcar_masculino();
        return false;
    });
    $("#tipo-number-content #gender #g-femenino").click(function() { // Femenino
        marcar_femenino();
        return false;
    });

	$(".identificacion-positiva-form #g-masculino").click(function() { // Masculino
        marcar_masculino_idem_pos();
        return false;
    });
    $(".identificacion-positiva-form #g-femenino").click(function() { // Femenino
        marcar_femenino_idem_pos();
        return false;
    });
  }

  Drupal.behaviors.bbva_login = {
    attach: function (context, settings) {

		$('#dropdown-login #recover_pass_1 a').click(function() {
			$('.identificacion-positiva-form h3').html(RECUPERO_CLAVE_CLIENTE_MSG);
			iniciarRecuperoClaveDigital();
			return false;
		});
        $('#dropdown-login #recover_pass_2 a').click(function() {
			$('.identificacion-positiva-form h3').html(GENERACION_CLAVE_CLIENTE_MSG);
			iniciarRecuperoClaveDigital();
			return false;
		});
        $('#dropdown-login #recover_pass_nocliente a').click(iniciarRecoverPassNoCliente);
        manage_gender();

		$("#dropdown-login #bbva-profile-pass-recovery-form #recovery-continuar").click(function() {
			bindLoginButonHandler();
			ocultarReseteoPassNoCliente();
			return false;
		});

		$("#dropdown-login #bbva-profile-pass-recovery-form .recuperar-salir").click(function() {
			bindLoginButonHandler();
			ocultarReseteoPassNoCliente();
			return false;
		});

		$(".trigger-margin-region-slideshow").click(function() {
			manageRegionSlideShowMarginTop();
		});

        if (MARCA_RECUPERA_CLAVE_DESDE_APP) {
            adaptarPanelRecuperoParaApp();
        }
    }
  }

  function loginSoyClienteHandler() {
	ocultarIdentificacionPositiva();
	ocultarRecupero();
	ocultarEnrrolamiento();
	ocultarReseteoPassNoCliente();
	abrirLoginClaveDigital();
	$('.pregunta-soy-cliente #soy_login').css('background-color','#09519d');
	$('.pregunta-soy-cliente #nosoy_login').css('background-color','#b4b4b4');
	$('#dropdown-login #user-login-form').css('display','block'); // no iria si corregimos el ajax
	$('#dropdown-login input[name=tipo_cliente]').val(TIPO_CLIENTE);

	hideIdentificacionError();

	return false;
  }
    function manageRegionSlideShowMarginTop(){
    	if (document.documentElement.clientWidth < 961){
			var dropdownContent = document.getElementById('dropdown-content');
			var newMarginTop = 0;
			if (dropdownContent != null) {
				var newMarginTop = dropdownContent.clientHeight;
			}
			$('.region-slideshow').css('margin-top', newMarginTop);
		} else {
			$('.region-slideshow').css('margin-top',0);
		}
    }
  function loginNoSoyClienteHandler() {
	ocultarReseteoPassNoCliente();
	ocultarIdentificacionPositiva();
	ocultarRecupero();
	ocultarEnrrolamiento();

	mostrarLoginForm();
	$('#bbva-clave-usuario').hide();
	$('#bbva-clave-no-cliente').show();
	$('.pregunta-soy-cliente #soy_login').css('background-color','#b4b4b4');
	$('.pregunta-soy-cliente #nosoy_login').css('background-color','#09519d');
	$('#dropdown-login #user-login-form').css('display','block'); // no iria si corregimos el ajax
	$('#dropdown-login input[name=tipo_cliente]').val(TIPO_NO_CLIENTE);

	hideIdentificacionError();
	return false;
  }

  function unbindLoginButonHandler() {
	$(".pregunta-soy-cliente #soy_login").unbind( "click" );
	$(".pregunta-soy-cliente #nosoy_login").unbind( "click" );
  }

  function bindLoginButonHandler() {
	$(".pregunta-soy-cliente #soy_login").click(function(){
        loginSoyClienteHandler();
        manageRegionSlideShowMarginTop();

    });
	$(".pregunta-soy-cliente #nosoy_login").click(function(){
        loginNoSoyClienteHandler();
        manageRegionSlideShowMarginTop();
    });
}

  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
  };

  /**
  * Es necesario pasar una vez por la pantalla que pide el tipo y numero de documento.
  */
  function llamarRecuperarClaveCliente() {

    showWaiting('.identificacion-positiva-form');
	$.ajax({
		method: "POST",
		url: formURLRecuperoClaveCliente,
		async: true,
		dataType: "json",
		data: postDataRecuperoClaveCliente,
		xhrFields: {
		   withCredentials: true
		},
		crossDomain: true,
		success:  function(respuesta){
			if (respuesta.codigo == '0') {
				hideIdentificacionError();
				ocultarRecuperoFinal();
				if (respuesta.usarClaveFrances) {
					showIdentificacionError(MSG_ERROR_A_FNET);
				} else if (respuesta.tipoRespuesta == "T") {
					mostrarPregunta(respuesta);
				} else {
					// COMENZAR CON I+
					if (respuesta.codigoRetorno == HAY_PREGUNTAS) {
						if (respuesta.tipoRespuesta == "S") {
							mostrarRadios(respuesta);
						} else if (respuesta.tipoRespuesta == "M") {
							mostrarChecks(respuesta);
						} else if (respuesta.tipoRespuesta == "C") {
							mostrarCombinado(respuesta);
						}
					}
					if (respuesta.codigoRetorno == HAY_PREGUNTAS &&
						(respuesta.tipoRespuesta == "S" ||
						 respuesta.tipoRespuesta == "M" ||
						 respuesta.tipoRespuesta == "C")) {

						$('.pregunta-recuperacion-form').hide();
						mostrarPanelPreguntas();

					} else {
						showIdentificacionError(respuesta.descripcion);
					}
				}
			} else {
				if (respuesta.dniDuplicado) {
					$('#block-dropdown_login-login_dropdown  .identificacion-positiva-form #gender').show();
					$('#block-dropdown_login-login_dropdown  .identificacion-positiva-form #sexo').show();
					showIdentificacionError('Seleccione su sexo');
				} else {
					showIdentificacionError(respuesta.descripcion);
				}
				if (respuesta.codigoRetorno == 0) {
					$('.pregunta-recuperacion-form-final #recupero-continuar').css('display','none');
					$('.salir-recupero').css('width','317px');
				} else {
					$('.pregunta-recuperacion-form-final #recupero-continuar').css('display','block');
					$('.salir-recupero').css('width','153px');
				}
			}
			hideWaiting('.identificacion-positiva-form');
			hideWaiting('.pregunta-recuperacion-form-final');

		},
		beforeSend:function(){},
			error:function(jqXHR, textStatus, errorThrown){
			showIdentificacionError("En este momento no podemos ejecutar la operacion");
			hideWaiting('.identificacion-positiva-form');
			hideWaiting('.pregunta-recuperacion-form-final');
		}
	});
  }

  $(document).ready(function(){

	  $(".page-benefit .field-header a").attr("onclick", "return false");
		$(".page-benefit-categories .field-header a").attr("onclick", "return false");

	var panelId = getUrlParameter('panel');
	mostrarPanel(panelId);

	$("#dropdown-login .dropdown  #recupero-continuar").click(function() {
		hideIdentificacionError();
		ocultarRecuperoFinal();
		bindLoginButonHandler();
        abrirLoginClaveDigital();
        return false;
    });
	$("#dropdown-login .dropdown .salir-recupero").click(function() {
		ocultarTodoRecupero();
		bindLoginButonHandler();
		hideIdentificacionError();
        return false;
    });

	// Login
	bindLoginButonHandler();

	// Checkbox Gender
	$("#g-masculino").click(function() { // Soy cliente BBVA
		$('#g-femenino').css('background-color','#b4b4b4');
		$('#g-masculino').css('background-color','#09519e');
		return false;
	});
	$("#g-femenino").click(function() { // Soy cliente BBVA
		$('#g-masculino').css('background-color','#b4b4b4');
		$('#g-femenino').css('background-color','#09519d');
		return false;
	});


	$("#edit-submit--2").click(function() { // No Soy cliente BBVA
	var res = document.getElementsByName("pass1")[0].hasAttribute("disabled");
	if (res == true) {
		//$.modal(document.getElementById('message-box'));
	}
	});


    /**             IDENTIFICACION POSITIVA                     **/

    /**  Form Step 1: Se valida el DNI*/
    $('.identificacion-positiva-form #form-step1').submit(function( event ) {
		var doc_number = $('#form-step1 #nrodoc').val();
        var sexo = $('#form-step1 input[name=iden-sex]:checked').val();
		hideIdentificacionError();
		if (!isNumber(doc_number)) {
			showIdentificacionError('Ingrese su Número de documento.');
			event.preventDefault();
			return;
		}

        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");

		if (sexo == 'F') {
			postData[0].value = "DNI_FEM";
        } else if (sexo == 'M') {
			postData[0].value = "DNI_MASC";
		}
		postDataRecuperoClaveCliente = postData;
		formURLRecuperoClaveCliente = formURL;
		llamarRecuperarClaveCliente();

        event.preventDefault();
    });



    /**  Form Step N*/
    $('.identificacion-positiva-form-answer #form-step-n').submit(function( event ) {
		hideIdentificacionError();
        var op_elegidas = "";
        var tipoRespuesta = $('#tipoRespuesta').val();
        if (tipoRespuesta == 'S') {
            op_elegidas = $('input[name=respuesta-simple]:checked', '#form-step-n').val();
        } else if (tipoRespuesta == 'M') {
            op_elegidas = "";
            $('.ident-positiva-opciones input:checked').each(function() {
                op_elegidas = op_elegidas.concat($(this).attr('name').trim());
            });
        } else {
			op_elegidas = "";
            $('.ident-positiva-opciones input:checked').each(function() {
                op_elegidas = op_elegidas.concat($(this).attr('value').trim());
            });
		}
        $('#respuestasId').val(op_elegidas);
        showWaiting('.identificacion-positiva-form-answer');
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");

        $.ajax({
            method: "POST",
            url: formURL,
            async: true,
            dataType: "json",
            data: postData,
            xhrFields: {
               withCredentials: true
            },
            crossDomain: true,
            success:  function(respuesta){

                if (respuesta.codigo == '0') {
                    if (respuesta.codigoRetorno == HAY_PREGUNTAS) {
                        if (respuesta.tipoRespuesta == "S") {
                            mostrarRadios(respuesta);
                        } else if (respuesta.tipoRespuesta == "M") {
                            mostrarChecks(respuesta);
                        } else if (respuesta.tipoRespuesta == "C") {
                            mostrarCombinado(respuesta);
                        }
                    } else if (respuesta.codigoRetorno == RESPUESTA_OK) {
                        mostrarPanelEnrrolar();
                    } else {
						ocultarIdentificacionPositiva();
						ocultarEnrrolamiento();
						showIdentificacionError(MSG_ERROR_NO_OK);
						mostrarRecuperoFinal();
                    }
                } else {
					ocultarIdentificacionPositiva();
					ocultarEnrrolamiento();
					mostrarRecuperoFinal();
					var mensaje = respuesta.descripcion.split("|");
					showIdentificacionError(mensaje[0]);
					if (mensaje[1] != "") {
						$(".mensaje-exitoso").html(mensaje[1]);
					}

					// Agregar logica al boton continuar
					$("#dropdown-login .dropdown  #recupero-continuar").css("display", "block");
					$("#dropdown-login .dropdown  #recupero-continuar").unbind( "click" );
					$("#dropdown-login .dropdown  #recupero-continuar").click(function(){
						showWaiting('.pregunta-recuperacion-form-final');
						llamarRecuperarClaveCliente();
					});
                }
                hideWaiting('.identificacion-positiva-form-answer');
            },
            beforeSend:function(){},
            error:function(jqXHR, textStatus, errorThrown){
				ocultarIdentificacionPositiva();
				ocultarEnrrolamiento();
				mostrarRecuperoFinal();
				showIdentificacionError(MSG_ERROR_GENERICO);
				hideWaiting('.identificacion-positiva-form-answer');
            }
        });
        event.preventDefault();
    });


	/**  Form Step Pregunta Recuperacion*/
    $('.pregunta-recuperacion-form #form-recup').submit(function( event ) {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
		showWaiting('.pregunta-recuperacion-form');
		hideIdentificacionError();
        $.ajax({
            method: "POST",
            url: formURL,
            async: true,
            dataType: "json",
            data: postData,
            xhrFields: {
               withCredentials: true
            },
            crossDomain: true,
            success:  function(respuesta){
				$('.p-recupero').show();
                if (respuesta.codigo == '0') {
					if (!respuesta.identificado) {
						mostrarPregunta(respuesta);
					} else {
						ocultarRecupero();
						mostrarPanelEnrrolar();
					}
				} else if (respuesta.codigo == MSG_ERROR_PREGUNTAS_RECUPERO_CODE) {
					$('.p-recupero').hide();
					showIdentificacionError(MSG_ERROR_PREGUNTAS_RECUPERO);
				} else if (respuesta.codigo == MSG_ERROR_PREGUNTAS_BLOQUEADAS_CODE) {
					ocultarRecupero();
					mostrarRecuperoFinal();
					showIdentificacionError(MSG_ERROR_PREGUNTAS_BLOQUEADAS);
                } else {
					showIdentificacionError(respuesta.descripcion);
                }
				hideWaiting('.pregunta-recuperacion-form');
            },
            beforeSend:function(){},
				error:function(jqXHR, textStatus, errorThrown){
				showIdentificacionError("En este momento no podemos ejecutar la operacion");
				hideWaiting('.pregunta-recuperacion-form');
            }
        });

        event.preventDefault();
    });


    /**  						FORMULARIO ENRROLLAR Ó GENERAR USUARIO Y CLAVE						*/
    $('.identificacion-positiva-form-enrrolar #form-enrrolar').submit(function( event ) {
		hideIdentificacionError();

        var form = $( ".identificacion-positiva-form-enrrolar #form-enrrolar" );
        form.validate();
        if (!form.valid()) {
            return false;
        }

        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
		showWaiting('.identificacion-positiva-form-enrrolar');

        $.ajax({
            method: "POST",
            url: formURL,
            async: true,
            dataType: "json",
            data: postData,
            xhrFields: {
               withCredentials: true
            },
            crossDomain: true,
            success:  function(respuesta){
                if (respuesta.codigo == '0') {
                    $('.identificacion-error').html(respuesta.descripcion);
                    ocultarEnrrolamiento();

					if (MARCA_RECUPERA_CLAVE_DESDE_APP) {
						$('.mensaje-exitoso').html(MSG_EXITOSO_ENRROLAMIENTO);
						$('.pregunta-recuperacion-form-final #recupero-continuar').css('display','none');
						mostrarRecuperoFinal();
					} else {
						abrirLoginClaveDigital();
					}

                } else {
					showIdentificacionError(respuesta.descripcion);
                }
				hideWaiting('.identificacion-positiva-form-enrrolar');
            },
            beforeSend:function(){},
            error:function(jqXHR, textStatus, errorThrown){
				showIdentificacionError(MSG_ERROR_GENERICO);
				hideWaiting('.identificacion-positiva-form-enrrolar');
            }
        });
        event.preventDefault();
    });

    /**  Form Enrrolar :  VALIDACIONES          **/
    if($(".identificacion-positiva-form-enrrolar #form-enrrolar").length > 0) {
    $(".identificacion-positiva-form-enrrolar #form-enrrolar").validate({
        rules: {
            usuario: {
                required: true,
                letters: true,
                minlength: 6
            },
            clave: {
                required: true,
                alphanumeric: true,
                lettersAndNumbers: true,
                minlength: 8
            },
            claveConf: {
                required: true,
                equalToIgnoreCase: "#clave"
            }
        },
        messages: {
            usuario: {
                required:  " Ingrese su Usuario",
                letters:   " No utilice números ni caracteres especiales",
                minlength: " Debe tener entre 6 y 20 letras"
            },
            clave: {
                required:     " Ingrese su Clave Digital",
                alphanumeric: " No utilice caracteres especiales",
                lettersAndNumbers: " Debe tener por lo menos una letra y un número",
                minlength:    " Debe tener 8 dígitos"
            },
            claveConf: {
                required:     " Confirme su Clave Digital",
                equalToIgnoreCase: " Las claves ingresadas no coinciden"
            }
        }
    });
    }


  });




  function iniciarRecoverPassNoCliente() {
    ocultarLoginForm();
	unbindLoginButonHandler();
    $('.recover-pass-nocliente').show();
  }


  /**
  * Funcion para mostrar los paneles de recupero de clave cuando vienen desde el APP
  */
  function mostrarPanel(panelId) {

	var recoverpassNocliente = "recoverpassNocliente";
	var recoverpassCliente = "recoverpassCliente";
	var recoverpassNewCliente = "recoverpassNewCliente";

	if (panelId == recoverpassNocliente || panelId == recoverpassCliente || panelId == recoverpassNewCliente) {
		$('#dropdown-login .login').addClass('active').siblings('.dropdown').show();
        ocultarElementosConRecuperoMovil();
		mostrarLoginForm();
	}

	if (panelId == recoverpassNocliente) {
		loginNoSoyClienteHandler();
		iniciarRecoverPassNoCliente();
	} else if (panelId == recoverpassCliente) {
		$('.identificacion-positiva-form h3').html(RECUPERO_CLAVE_CLIENTE_MSG);
		loginSoyClienteHandler();
		iniciarRecuperoClaveDigital();
	} else if (panelId == recoverpassNewCliente) {
		$('.identificacion-positiva-form h3').html(GENERACION_CLAVE_CLIENTE_MSG);
		loginSoyClienteHandler();
		iniciarRecuperoClaveDigital();
	}
  }

  function ocultarElementosConRecuperoMovil() {
    MARCA_RECUPERA_CLAVE_DESDE_APP = true;
    $('#main #content').css('display', 'none');
    $('.breadcrumb').css('display', 'none');
    $('#pre-footer').css('display', 'none');
    $('#footer').css('display', 'none');
    $('.cabezal-ingresar').css('display', 'none');
    $('.pregunta-soy-cliente').css('display', 'none');
    $('#menu-responsive button').css('display', 'none');

    // ocultar botones Salir
    $('.salir-recupero').css('display', 'none');
    $('#dropdown-login .dropdown .footer-buton .salir-leyenda').css('display', 'none');

    // Adaptar Pantalla de introduccion de I+
    $('.ident-comenzar-intro').addClass('ident-comenzar-intro-movil').removeClass('ident-comenzar-intro');

    // Adaptar Pantalla Pedir DNI en I+
    // Adaptar Preguntas en I+
    // Adaptar Usuario y Clave Digital en I+
    // Adaptar Preguntas Recupero
    $('.siguiente-recupero').addClass('siguiente-recupero-movil').removeClass('siguiente-recupero');

    // Adaptar Recupero No Cliente
    adaptarPanelRecuperoParaApp();

  }
  function adaptarPanelRecuperoParaApp() {
    $('.recover-pass-nocliente .recuperar-salir').css('display', 'none');
    $('.recover-pass-nocliente .form-submit').css('width', '100%');
  }

  function iniciarIdentificacionPositiva() {
    $('.identificacion-positiva').show();
	$('.pregunta-recuperacion-form').hide();

    $('#ident-comenzar').click(function(){
      $('.identificacion-positiva').hide();
      mostrarPanelPreguntas();
    });

	$("#dropdown-login .dropdown  .salir-leyenda").click(function() {
		hideIdentificacionError();
		ocultarTodoRecupero();
		bindLoginButonHandler();
        return false;
    });

  }

  function iniciarRecuperoClaveDigital() {
    ocultarLoginForm();
	unbindLoginButonHandler();

	// 0 - NO MOCK Punto de salida. Pedido de DNI
	$('.identificacion-positiva-form').show();
    $('.identificacion-positiva-form #form-step1 #nrodoc').val("");

	// 1 - MOCK	Primer Pantalla I+ Harcodeada
	//iniciarIdentificacionPositiva();

    // 2 - MOCK Panel de Preguntas y opciones  para I+
    //mostrarPanelPreguntas();

    // 3 - MOCK mostrar Panel Enrrolar
    //mostrarPanelEnrrolar();

    // 4 - MOCK mostrar Panel Preguntas Precargadas
    //$('.pregunta-recuperacion-form').show();

  }

  function initPaneles() {
	$('.identificacion-positiva').hide();
	$('.pregunta-recuperacion-form').hide();
    $('.identificacion-positiva-form').show();

    $('.identificacion-positiva-form #form-step1 #nrodoc').val("");
  }

  function mostrarPanelPreguntas() {
    $('.identificacion-positiva').hide();
    $('.identificacion-positiva-form').hide();
    $('.identificacion-positiva-form-answer').show();

  }
  function mostrarPanelEnrrolar() {
    ocultarIdentificacionPositiva();
    $('.identificacion-positiva-form-enrrolar').show();
    $('.identificacion-positiva-form-enrrolar #usuario').val("");
    $('.identificacion-positiva-form-enrrolar #clave').val("");
    $('.identificacion-positiva-form-enrrolar #claveConf').val("");
  }

  function ocultarLoginForm() {
    $('#dropdown-login #user-login-form').hide();
  }
  function mostrarLoginForm() {
    $('#dropdown-login #user-login-form').show();
  }

  function abrirLoginClaveDigital() {
    mostrarLoginForm();
    $('#bbva-clave-usuario').show();
    $('#bbva-clave-no-cliente').hide();

  }

  function ocultarReseteoPassNoCliente() {
	$('#bbva-profile-pass-recovery-form .messages.error').parent().hide();
	$('.recover-pass-nocliente').hide();
  }

  function ocultarIdentificacionPositiva() {
    $('.identificacion-positiva').hide();
    $('.identificacion-positiva-form').hide();
    $('.identificacion-positiva-form-answer').hide();
    $('.identificacion-positiva-form #form-step1 #nrodoc').val("");
  }

  function ocultarTodoRecupero() {
    $('.identificacion-positiva').hide();
    $('.identificacion-positiva-form').hide();
    $('.identificacion-positiva-form-answer').hide();
    $('.identificacion-positiva-form-enrrolar').hide();
	$('.pregunta-recuperacion-form').hide();
	$('.pregunta-recuperacion-form-final').hide();
  }

  function ocultarEnrrolamiento() {
    $('.identificacion-positiva').hide();
    $('.identificacion-positiva-form-enrrolar').hide();
    $('.identificacion-positiva-form-enrrolar #usuario').val("");
    $('.identificacion-positiva-form-enrrolar #clave').val("");
    $('.identificacion-positiva-form-enrrolar #claveConf').val("");
  }
  function mostrarRecuperoFinal() {
    $('.pregunta-recuperacion-form-final').show();
  }

  function ocultarRecuperoFinal() {
    $('.pregunta-recuperacion-form-final').hide();
  }

  function ocultarRecupero() {
    $('.pregunta-recuperacion-form').hide();
  }

  function prepararDiv(respuesta_iden_positiva) {
    $('.identificacion-positiva-form-answer .ident-positiva-pregunta').empty();
    $('.identificacion-positiva-form-answer .ident-positiva-pregunta').html(respuesta_iden_positiva.pregunta);
    $('.identificacion-positiva-form-answer #estadoConversacion').val(respuesta_iden_positiva.estadoConversacion);
    $('.identificacion-positiva-form-answer #tipoRespuesta').val(respuesta_iden_positiva.tipoRespuesta);
    $('.identificacion-positiva-form-answer .ident-positiva-opciones').empty();
    $('.identificacion-positiva-form-answer .ident-positiva-norecuerdo').empty();
    if (respuesta_iden_positiva.opcionNoLoRecurda == true) {
        var input_norecuerdo = '<span>¿Olvidó la respuesta?</span><a id="no-recuerdo-btn" href="#">Ver otra pregunta</a>'
        $(input_norecuerdo).appendTo('.identificacion-positiva-form-answer .ident-positiva-norecuerdo');
        $(".ident-positiva-norecuerdo #no-recuerdo-btn").click(enviarNoRecuerdo);
    }
    if (respuesta_iden_positiva.tipoRespuesta == 'S') {
        $('.ident-positiva-leyenda').html("Seleccione una de las opciones.");
    } else if (respuesta_iden_positiva.tipoRespuesta == 'M') {
        $('.ident-positiva-leyenda').html("Seleccione una o varias opciones.");
    }
  }

  function mostrarPregunta(respuesta_json) {
	$('.identificacion-positiva-form').hide();
	$('.pregunta-recuperacion-form #respuesta').val("");
	$('.pregunta-recuperacion-form .pregunta-label').html(respuesta_json.pregunta);
	$('.pregunta-recuperacion-form').show();
  }

  function mostrarRadios(respuesta_iden_positiva) {
    prepararDiv(respuesta_iden_positiva);
    $.each(respuesta_iden_positiva.respuestas, function(index, value) {
        var respuesta_item = value;
        if (respuesta_item.grupo == 1) {
            var id = respuesta_item.id;
            var textoRespuesta = respuesta_item.respuesta;
            var radioBtn = $('<input type="radio" name="respuesta-simple" value="'+id+'">'+textoRespuesta+'</input><br/>');
            radioBtn.appendTo('#form-step-n .ident-positiva-opciones');
        }
    });
    $('.ident-positiva-opciones input:radio').last().attr('checked','checked');
  }

  function mostrarChecks(respuesta_iden_positiva) {
    prepararDiv(respuesta_iden_positiva);
    $.each(respuesta_iden_positiva.respuestas, function(index, value) {
        var respuesta_item = value;
        if (respuesta_item.grupo == 1) {
            var id = respuesta_item.id;
            var textoRespuesta = respuesta_item.respuesta;
            var checkbox = $('<input type="checkbox" name="'+id+'">'+textoRespuesta+'</input><br/>');
            $(checkbox).click(desSeleccionarUltimo);
            checkbox.appendTo('#form-step-n .ident-positiva-opciones');
        }
    });
    $('.ident-positiva-opciones input:checkbox').last().attr('checked','checked');
    $('.ident-positiva-opciones input:checkbox').last().unbind( "click" );
    $('.ident-positiva-opciones input:checkbox').last().click(desSeleccionarTodos);
  }

  function mostrarCombinado(respuesta_iden_positiva) {
    prepararDiv(respuesta_iden_positiva);
    var nro_grupo = 0;
	var nro_grupo_last = 0;
    $.each(respuesta_iden_positiva.respuestas, function(index, value) {
        var respuesta_item = value;
		nro_grupo = respuesta_item.grupo;
		if (nro_grupo == 1) {
			nro_grupo_last = nro_grupo;
		} else if ((nro_grupo != nro_grupo_last) ){
			$('<br/>').appendTo('#form-step-n .ident-positiva-opciones');
			nro_grupo_last  = nro_grupo;
		}

		var id = respuesta_item.id;
		var textoRespuesta = respuesta_item.respuesta;
		var radioBtn = $('<input type="radio" name="respuesta-combi-'+nro_grupo+'" value="'+id+'">'+textoRespuesta+'</input><br/>');
		radioBtn.appendTo('#form-step-n .ident-positiva-opciones');
    });
  }

  function desSeleccionarTodos() {
    $('.ident-positiva-opciones input[type=checkbox]').each(function(){
        $(this).removeAttr('checked');
    })
    $('.ident-positiva-opciones input:checkbox').last().attr('checked','checked');
  }

  function desSeleccionarUltimo() {
    $('.ident-positiva-opciones input:checkbox').last().removeAttr('checked');
  }

  function showIdentificacionError(mensaje) {
	$('.identificacion-error').html(mensaje);
	$('.identificacion-error').css('display','block');
	if (MARCA_RECUPERA_CLAVE_DESDE_APP) {
		$('.pregunta-recuperacion-form-final #recupero-continuar').css('display','none');
	}
  }
  function hideIdentificacionError() {
	$('.identificacion-error').html("");
	$('.identificacion-error').css('display','none');
  }

  function enviarNoRecuerdo() {
	showWaiting('.identificacion-positiva-form-answer');
    var formURL = $('.identificacion-positiva-form-answer #form-step-n').attr('action');

    $.ajax({
        method: "POST",
        url: formURL,
        async: true,
        dataType: "json",
        data: {noRecuerdo : 'true'},
        xhrFields: {
           withCredentials: true
        },
        crossDomain: true,
        success:  function(respuesta){

                if (respuesta.codigo == '0') {
                    if (respuesta.codigoRetorno == HAY_PREGUNTAS) {
                        if (respuesta.tipoRespuesta == "S") {
                            mostrarRadios(respuesta);
                        } else if (respuesta.tipoRespuesta == "M") {
                            mostrarChecks(respuesta);
                        } else if (respuesta.tipoRespuesta == "C") {
                            mostrarCombinado(respuesta);
                        }
                    } else if (respuesta.codigoRetorno == RESPUESTA_OK) {
                        mostrarPanelEnrrolar();
                    } else {
						ocultarIdentificacionPositiva();
						ocultarEnrrolamiento();
						showIdentificacionError(MSG_ERROR_NO_OK);
						mostrarRecuperoFinal();
                    }
                } else {
					ocultarIdentificacionPositiva();
					ocultarEnrrolamiento();
					mostrarRecuperoFinal();
					showIdentificacionError(respuesta.descripcion);
                }
                hideWaiting('.identificacion-positiva-form-answer');
            },
            beforeSend:function(){},
            error:function(jqXHR, textStatus, errorThrown){
				ocultarIdentificacionPositiva();
				ocultarEnrrolamiento();
				mostrarRecuperoFinal();
				showIdentificacionError(MSG_ERROR_GENERICO);
				hideWaiting('.identificacion-positiva-form-answer');
            }
        });
  }

  /**
  * Accept class name and id name
  */
  function showWaiting(identificadorPanel) {
	$(''+identificadorPanel+' #waitting').css('display', 'block');
	$(''+identificadorPanel+' #waitting').spin();
  }
  /**
  * Accept class name and id name
  */
  function hideWaiting(identificadorPanel) {
	$(''+identificadorPanel+' #waitting').css('display', 'none');
	$(''+identificadorPanel+' #waitting').spin(false);
  }

  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
})(jQuery);
