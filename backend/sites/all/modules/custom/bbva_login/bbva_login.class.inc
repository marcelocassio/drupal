<?php

class BbvaTimerRegister {
  public $time_table;

  function __construct() {
    $this->time_table = array();
  }

  public function addTimeMark($label) {
    $time = microtime(true);
    $this->time_table[$label] = $time;
  }

  public function logResult() {
    $first_mark = NULL;
    $output = "";
    if (!empty($this->time_table)) {
      $first_value = reset($this->time_table);
      foreach ($this->time_table as $key => $value) {
         $intervalo = $value - $first_value;
         $output .= "Label= " . $key . ", Tiempo = " . $intervalo;
      }  
      watchdog('debug',"Resultado {%salida}", array('%salida' => $output));
    }
  }
} 