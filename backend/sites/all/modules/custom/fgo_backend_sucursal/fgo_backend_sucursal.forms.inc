<?php
/**
 * Metodos para los formularios de edicion y creacion de sucursales
 */

function fgo_backend_sucursal_agregar_form()
{
    return fgo_backend_sucursal_display_form();
}

function fgo_backend_sucursal_editar_form($form_state, $build)
{
    $sucursal_id = $build["build_info"]["args"][0];
    return fgo_backend_sucursal_display_form($sucursal_id);
}

function fgo_backend_sucursal_display_form($sucursal_id = null)
{
    
    $_default = array(
        'cliente' => '',
        'segmento_cod' => '',
        'segmento_desc' => ''

    );

    if (!is_null($sucursal_id)) {
        $sucursalBo = new \Fgo\Bo\SucursalVirtualBo($sucursal_id);
        $_default['idSucursalVirtual'] = $sucursal_id;
        $_default['web'] = $sucursalBo->web;
       
    }

    $form['redirect'] = false;

    $form['#attributes']['autocomplete'] = "off";

    $form['#validate'][] = 'fgo_backend_sucursal_form_validate';

    $form['idSucursalVirtual'] = array(
        '#type' => 'hidden',
        '#default_value' => $_default['idSucursalVirtual']
    );

    $form['grupo_dato'] = array(
        '#type' => 'fieldset',
        '#title' => t('<strong><i>datos</i></strong>')
    );

    $form['grupo_dato']['web'] = array(
        '#title' => t('Web'),
        '#type' => 'textfield',
        '#maxlength' => 230,
        '#size' => 117,
        '#weight' => 1,
        '#default_value' => $_default['web']
    );
   
    

    $form['guardar'] = array(
        '#type' => 'submit',
        '#id' => 'guardar_slide',
        '#attributes' => array('class' => array('boton_fgo', 'boton_positivo')),
        '#value' => 'Guardar',
        '#submit' => array('fgo_backend_sucursal_submit')
    );

    return $form;
}

function fgo_backend_sucursal_form_validate(&$form, &$form_state)
{
    $sucursalBo = new \Fgo\Bo\SucursalVirtualBo($form_state['values']['idSucursalVirtual']);
    $sucursalBo->web = $form_state['values']['web'];
    
    

    try {
        $sucursalBo->guardar();
    } catch (\Fgo\Bo\DatosInvalidosException $e) {
        $errores = $e->getErrores();
        foreach ($errores as $error_campo => $error_descripcion) {
            form_set_error($error_campo, $error_descripcion);
        }
    }
}

function fgo_backend_sucursal_submit(&$form, &$form_state)
{
   // $form_state['redirect'] = url('../admin/v2/sucursal/lista');
}
