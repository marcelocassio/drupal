<?php
namespace Fgo\Bo;

class SucursalFisicaBo extends GeneralBo {
  public $idSucursalFisica;
  public $idSucursalFisicaSuip;
  public $localidad;
  public $provincia;
  public $nombre;
  public $calle;
  public $calleNumero;
  public $latitud;
  public $longitud;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\SucursalFisicaDao::getInstance();
  }

  protected function construirDesdeRegistro($registro){
    $this->idSucursalFisica = $registro->id_sucursal_fisica;
    $this->idSucursalFisicaSuip = $registro->id_sucursal_fisica_suip;
    $this->calle = $registro->calle;
    $this->calleNumero = $registro->calle_numero;
    $localidadBo = new LocalidadBo();
    $this->localidad = $localidadBo->buscarPorId($registro->id_localidad)->nombre;
    $this->provincia = $registro->id_provincia;
    $this->latitud = $registro->latitud;
    $this->longitud = $registro->longitud;
    $this->nombre = $registro->nombre;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {

    $this->idSucursalFisicaSuip = $dto->id_sucursal_fisica_suip;
    $this->localidad = LocalidadBo::buscarLocalidadPorIdSuip($dto->id_localidad);
    $this->provincia = ProvinciaBo::buscarProvinciaPorIdSuip($dto->id_provincia);
    $this->nombre = $dto->nombre;
    $this->calle = $dto->calle;
    $this->calleNumero = $dto->calle_numero;
    $this->latitud = (float)$dto->latitud;
    $this->longitud = (float)$dto->longitud;
  }

  public function listarPorIdComunicacion($id){
    $listadoSucursalesFisicas = array();
    $result = $this->repo->listarPorIdComunicacion($id);
    foreach ($result as $registro) {
      $entidad = new $this;
      $entidad = $entidad->construirDesdeRegistro($registro);
      $listadoSucursalesFisicas[] = $entidad;
      unset($entidad);
    }

    return $listadoSucursalesFisicas;
  }

  public function buscarPorIdSuip($idSuip) {
    $registro = $this->repo->buscarPorIdSuip($idSuip);
    if ($registro) {
      $object = $this->construirDesdeRegistro($registro);
    }
    return $object;
  }

  public function listarPorIdSucursalFisicaSuip() {
    $listadoClaveIdSuip = array();
    $listadoSucursalesFisicas = $this->listar();
    foreach ($listadoSucursalesFisicas as $registro) {
      $idSuip = $registro->idSucursalFisicaSuip;
      if (isset($idSuip)) {
        $listadoClaveIdSuip[$idSuip] = $registro;
      }
    }
    return $listadoClaveIdSuip;
  }

  public static function listarPorListadoIdSuip($listadoIdSucursalesSuip) {
    $listadoSucursalesFisicas = array();
    $registros = \Fgo\Dao\SucursalFisicaDao::getInstance()->filtrarListadoIdSuip($listadoIdSucursalesSuip);
    foreach ($registros as $registro) {
      $entidad = new SucursalFisicaBo();
      $entidad = $entidad->construirDesdeRegistro($registro);
      $listadoSucursalesFisicas[] = $entidad;
      unset($entidad);
    }
    return $listadoSucursalesFisicas;

  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idSucursalFisica) && !is_numeric($this->idSucursalFisica)) {
      $errores['idSucursalFisica'] = 'Por favor revise el ID de la entidad.';
    }
    if ((!empty($this->localidad->idLocalidad) && !is_numeric($this->localidad->idLocalidad)) || (!empty($this->localidad->idLocalidad) && strlen($this->localidad->idLocalidad) > 11)) {
      $errores['localidad'] = 'Por favor revise el ID Localidad.';
    }
    if ((!empty($this->provincia->idProvincia) && !is_numeric($this->provincia->idProvincia)) || (!empty($this->provincia->idProvincia) && strlen($this->provincia->idProvincia) > 11)) {
      $errores['provincia'] = 'Por favor revise el ID Provincia.';
    }
    if ((!empty($this->idSucursalFisicaSuip) && !is_numeric($this->idSucursalFisicaSuip)) || (!empty($this->idSucursalFisicaSuip) && strlen($this->idSucursalFisicaSuip) > 11)) {
      $errores['idSucursalFisicaSuip'] = 'Por favor revise el ID Sucursal Fisica Suip.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 150) {
      $errores['nombre'] = 'Por favor revise el nombre de la Sucursal Fisica.';
    }
    if (!empty($this->calle) && strlen($this->calle) > 150) {
      $errores['calle'] = 'Por favor revise la calle de la Sucursal Fisica.';
    }
    if (!empty($this->calleNumero) && strlen($this->calleNumero) > 45) {
      $errores['calleNumero'] = 'Por favor revise el número de la calle.';
    }
    if (empty($this->latitud) || !is_float($this->latitud)) {
      $errores['latitud'] = 'Por favor revise la latitud.';
    }
    if (empty($this->longitud) || !is_float($this->longitud)) {
      $errores['longitud'] = 'Por favor revise la longitud.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}