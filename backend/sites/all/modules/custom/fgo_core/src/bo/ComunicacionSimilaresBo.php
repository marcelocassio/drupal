<?php
namespace Fgo\Bo;

class ComunicacionSimilaresBo extends GeneralBo {
  public $idComunicacionSimilar;
  public $idComunicacion;
  public $idSimilar;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ComunicacionSimilaresDao::getInstance();
  }

  public function construirDesdeRegistro($registro){
    $this->idComunicacionSimilar = $registro->id_comunicacion_similar;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idSimilar = $registro->id_similar;
    return $this;
  }

  public function construir($comunicacion, $comunicacion_similar) {
    $this->idComunicacion = $comunicacion;
    $this->idSimilar = $comunicacion_similar;
  }

  public static function listarPorIdCombinado() {
    $listado = \Fgo\Dao\ComunicacionSimilaresDao::getInstance()->listar();
    $matrizCombinada = array();
    foreach ($listado as $registro) {
      $comunicacionSimilarBo = new ComunicacionSimilaresBo();
      $comunicacionSimilarBo->construirDesdeRegistro($registro);
      $matrizCombinada[$registro->id_comunicacion][$registro->id_similar] = $comunicacionSimilarBo;
    }
    return $matrizCombinada;
  }

  public function listarSimilaresPorIdComunicacion($idComunicacion) {
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    return $registros;
  }

  public static function borrarComunicacionSimilares($id_comunicacion) {
    \Fgo\Dao\ComunicacionSimilaresDao::getInstance()->borrarComunicacionSimilares($id_comunicacion);
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idComunicacionSimilar) && !is_numeric($this->idComunicacionSimilar)) {
      $errores['idComunicacionSimilar'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComunicacion) || !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if (empty($this->idSimilar) || !is_numeric($this->idSimilar)) {
      $errores['idSimilar'] = 'Por favor revise el ID del similar.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}