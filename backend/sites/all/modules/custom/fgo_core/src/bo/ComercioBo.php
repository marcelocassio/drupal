<?php

namespace Fgo\Bo;

/**
 * Comercio que brinda los beneficios
 */
class ComercioBo extends GeneralBo {
  public $idComercio;
  public $idComercioSuip;
  public $imagenes;
  public $nombre;
  public $avvio;
  public $sucursalFisica;


  function construirDesdeLineaArchivo($dto) {
    $this->idComercioSuip = $dto->id_comercio_suip;
    $this->nombre = $dto->nombre;

    if (isset($dto->id_shopping) && $dto->id_shopping != null) {
      $sucursalFisicaBo = $this->getSucursalFisica();
      if (!isset($sucursalFisicaBo)) {
        $sucursalFisicaBo = new \Fgo\Bo\SucursalFisicaBo();
      }
      $sucursalFisicaBo->construirDesdeLineaArchivo($dto);
      $sucursalFisicaBo->guardar();
      $this->setSucursalFisica($sucursalFisicaBo);
    }
  }

  protected function construirDesdeRegistro($registro)  {
    $this->idComercio = $registro->id_comercio;
    $this->nombre = $registro->nombre;
    $this->avvio = $registro->avvio;
    $this->idComercioSuip = $registro->id_comercio_suip;
    if ($registro->id_sucursal_fisica != null) {
      $this->setSucursalFisica(new SucursalFisicaBo($registro->id_sucursal_fisica));
    }
    $comercioImagen = new ComercioImagenBo();
    //Revisar si el "->imagenes no se tiene q quitar"
    $this->imagenes = $comercioImagen->listarImagenesPorIdComercio($registro->id_comercio)->imagenes;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ComercioDao::getInstance();
  }

  public function buscarPorIdComercioSuip($idComercioSuip) {
    $comercioBo = null;
    $registros = $this->repo->filtrarPorIdComercioSuip($idComercioSuip);
    if (isset($registros[0])) {
      $comercioBo = new ComercioBo();
      $comercioBo->construirDesdeRegistro($registros[0]);
    }
    return $comercioBo;
  }

  public function listarPorIdComercioSuip() {
    
    $comercios = $this->listar();
    $mapaIdComercioSuip = array();
    

    foreach ($comercios as $comercioBo) {
      if ($comercioBo->idComercioSuip != null) {
        $mapaIdComercioSuip[$comercioBo->idComercioSuip] = $comercioBo;
      }
    }

    return $mapaIdComercioSuip;
  }

  /**
   * Devuelve los comercios que coincidan con el nombre del comercio
   * @param $palabra Nombre del comercio.
   * @return array Listado de ComerciosBO que contengan $palabra
   */
  public function listarConcidenciasPorNombre($palabra){
    $comercios = array();
    $registros = $this->repo->filtrarConcidenciasPorNombre($palabra);
    foreach ($registros as $registro) {
      $comercio = new ComercioBo();
      $comercios[] = $comercio->construirDesdeRegistro($registro);
    }
    return $comercios;
  }

  protected function _validar() {

    $errores = array();
    if (!empty($this->idComercio) && !is_numeric($this->idComercio)) {
      $errores['idComercio'] = 'Por favor revise el ID de Comercio.';
    }
    if (!empty($this->idComercioSuip) && !is_numeric($this->idComercioSuip)) {
      $errores['idComercioSuip'] = 'Por favor revise el ID de Comercio SUIP.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 100) {
      $errores['nombre'] = 'Por favor revise el nombre del Comercio.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

  public function tieneSucursalFisica() {
    return ($this->sucursalFisica != null);
  }

  /**
   * @return mixed
   */
  public function getSucursalFisica() {
    return $this->sucursalFisica;
  }

  /**
   * @param mixed $sucursalFisica
   */
  public function setSucursalFisica($sucursalFisica) {
    $this->sucursalFisica = $sucursalFisica;
  }


}