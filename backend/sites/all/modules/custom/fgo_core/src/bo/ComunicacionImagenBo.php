<?php
namespace Fgo\Bo;

class ComunicacionImagenBo extends GeneralBo {

  public $idComunicacionImagen;
  public $idComunicacion;
  public $idImagen;

  public function construirDesdeRegistro($registro)  {
    $this->idComunicacionImagen = $registro->id_comunicacion_imagen;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idImagen = $registro->id_imagen;
    return $this;
  }

  public function construir($registro)  {
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idImagen = $registro->id_imagen;
  }

  protected function obtenerRepo()  {
    $this->repo = \Fgo\Dao\ComunicacionImagenDao::getInstance();
  }

  public function buscarImagenesPorIdComunicacion($idComunicacion) {
    $imagenes = array();

    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    foreach ($registros as $registro) {
      $imagenes[] = new ImagenBo($registro->id_imagen);
    }

    return $imagenes;
  }

  public function listarComunicacionImagenIds() {
    $listado = $this->repo->listarComunicacionImagenIds();
    return $listado;
  }

  public function listarComunicacionComunicacionImagen($idComunicacion) {
    $comunicacionImagen = $this->repo->listarComunicacionComunicacionImagen($idComunicacion);
    $this->construirDesdeRegistro($comunicacionImagen);
    return $this;
  }

  public static function eliminarPorIdComunicacion($idComunicacion) {
    if ($idComunicacion == null || $idComunicacion == "") {
      throw new \Exception("id de comunicacion invalido para eliminar relacion");
    }
    return \Fgo\Dao\ComunicacionImagenDao::getInstance()->eliminarPorIdComunicacion($idComunicacion);
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idComunicacionImagen) && !is_numeric($this->idComunicacionImagen)) {
      $errores['idComunicacionImagen'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComunicacion) || !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if (empty($this->idImagen) || !is_numeric($this->idImagen)) {
      $errores['idImagen'] = 'Por favor revise el ID de la imagen.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}