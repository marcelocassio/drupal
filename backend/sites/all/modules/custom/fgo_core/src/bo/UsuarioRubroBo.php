<?php

namespace Fgo\Bo;

class UsuarioRubroBo extends GeneralBo {
  public $idUsuarioRubro;
  public $idUsuario;
  public $idRubro;
  public $rubro;
  public $rubros = array();

  public function construir($rubroBo, $usuarioBo) {
    $this->idRubro = $rubroBo->idRubro;
    $this->idUsuario = $usuarioBo->idUsuario;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\UsuarioRubroDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idUsuarioRubro = $registro->id_usuario_rubro;
    $this->idUsuario = $registro->uid;
    $this->idRubro = $registro->id_rubro;
    if ($registro->id_rubro != null) {
      $this->setRubro(new \Fgo\Bo\RubroBo($registro->id_rubro));
    }

    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {

    $this->idUsuario = $dto->uid;
    if ($dto->id_rubro != null) {
      $this->setRubro(new \Fgo\Bo\RubroBo($dto->id_rubro));
      $this->idRubro= $dto->id_rubro;
    }
  }


  public function buscarPorIdUsuario($idUsuario) {
    $this->rubros = array();
    $registros = $this->repo->filtrarPorIdUsuario($idUsuario);
    foreach ($registros as $registro) {
      $rubro = new \Fgo\Bo\RubroBo($registro->id_rubro);
      $this->rubros[] = $rubro;
    }

    return $this;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idUsuarioRubro) && !is_numeric($this->idUsuarioRubro)) {
      $errores['idUsuarioRubro'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idUsuario) || !is_numeric($this->idUsuario)) {
      $errores['idUsuario'] = 'Por favor revise el ID del usuario.';
    }
    if (empty($this->idRubro) || !is_numeric($this->idRubro)) {
      $errores['idRubro'] = 'Por favor revise el ID del rubro.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

  
   /**
   * @return mixed
   */
  public function getRubro() {
    return $this->rubro;
  }

  /**
   * @param mixed $rubro
   */
  public function setRubro($rubro) {
    $this->rubro = $rubro;
  }


}