<?php

namespace Fgo\Bo;
use \Fgo\Dao\BeneficioDao;

class BeneficioBo extends GeneralBo {

  const TIPO_BENEFICIO_CARTERA = 1;
  const TIPO_BENEFICIO_GO = 2;
  const TIPO_BENEFICIO_CUPON = 3;
  const TIPO_BENEFICIO_FRANCESGO_SUIP = "105";
  const TIPO_BENEFICIO_CUPON_SUIP = "109";

  const DESCUENTO_PUNTO_VENTA = 1;
  const DESCUENTO_EN_RESUMEN = 2;

  public $idBeneficio;
  public $idComunicacion;
  public $tipoBeneficio;
  public $tipoAplicacionBeneficio;
  public $mediosPago;
  public $cuota;
  public $tope;
  public $valor;
  public $tipoAplicacionDescuesto;
  public $casuistica;

  /**
   * Devuelve un listado de beneficios dependiendo si es una comunicacion con varios beneficios.
   * @param $dto DTO de interfase con archivo.
   * @param $comunicacionBo BO de la comunicacion relacionada.
   * @return array Listado de beneficios
   */
  public function construirBeneficiosDesdeLineaArchivo($dto, $comunicacionBo) {

    $listadoBeneficios = array();
    $beneficio = new BeneficioBo();
    
    if ($dto->id_tipo_beneficio_suip == self::TIPO_BENEFICIO_FRANCESGO_SUIP) {
      $beneficio->construirBeneficio($dto, $comunicacionBo, self::TIPO_BENEFICIO_GO);
    } 
    else if ($dto->id_tipo_beneficio_suip == self::TIPO_BENEFICIO_CUPON_SUIP) {
      $beneficio->construirBeneficio($dto, $comunicacionBo, self::TIPO_BENEFICIO_CUPON);
    } 
    else {
      $beneficio->construirBeneficio($dto, $comunicacionBo, self::TIPO_BENEFICIO_CARTERA);
    }

    $listadoBeneficios[] = $beneficio;

    return $listadoBeneficios;
  }

  /**
   * Devuelve un beneficio.
   * @param $dto DTO que la interfase del beneficio.
   * @param $comunicacionBo BO de la comunicacion relacionada.
   * @param $tipoBeneficio
   */
  private function construirBeneficio($dto, $comunicacionBo, $tipoBeneficio) {
    $this->valor = ($dto->descuento_cartera_general == "0" || $dto->descuento_cartera_general == "")
                    ?null:$dto->descuento_cartera_general;
    $this->cuota = (int) $dto->cuotas;
    $this->idComunicacion = $comunicacionBo->idComunicacion;
    $this->tipoBeneficio = (int)$tipoBeneficio;
    $tipoAplicacionBeneficio = new TipoAplicacionBeneficioBo();
    $this->tipoAplicacionBeneficio = $tipoAplicacionBeneficio->buscarPorIdTipoAplicacionBeneficio($dto->id_aplicacion_beneficio_suip);
    if ($comunicacionBo->tieneMismoBeneficio($this)) {
      $mismoBeneficio = $comunicacionBo->obtenerMismoBeneficio($this);
      $this->idBeneficio = $mismoBeneficio->idBeneficio;
    }
    if (isset($dto->id_casuistica_suip)) {
      $this->casuistica = CasuisticaBo::buscarPorIdCasuisticaSuip($dto->id_casuistica_suip);
    }
  }

  /**
   * Modifica el estado del objeto con los valores del registro de la tabla
   * @param $registro Registro de la tabla del fgo_beneficio
   * @return $this
   */
  protected function construirDesdeRegistro($registro)  {
    $this->idBeneficio = $registro->id_beneficio;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->tipoBeneficio = (int)$registro->tipo_beneficio;
    if (isset($registro->id_tipo_aplicacion_beneficio)) {
      $this->tipoAplicacionBeneficio = new TipoAplicacionBeneficioBo($registro->id_tipo_aplicacion_beneficio);
    }
    $beneficioMedioPago = new BeneficioMedioPagoBo();
    $this->mediosPago = $beneficioMedioPago->buscarMedioPagoPorIdBeneficio($registro->id_beneficio);
    $this->valor = $registro->valor;
    $this->tope = $registro->tope;
    $this->cuota = ($registro->cuota==='')?0:(int)$registro->cuota;
    $this->casuistica = null;
    if ($registro->id_casuistica != null) {
      $this->casuistica = new CasuisticaBo($registro->id_casuistica);
    }
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = BeneficioDao::getInstance();
  }

  public function buscarPorIdComunicacion($idComunicacion) {
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);

    $listadoObjetosNegocio = array();
    foreach ($registros as $registro) {
      $objeto = new $this;
      $listadoObjetosNegocio[] = $objeto->construirDesdeRegistro($registro);
    }

    return $listadoObjetosNegocio;
  }

  public function esIgual($otroBeneficio) {
    return $this->idComunicacion === $otroBeneficio->idComunicacion && $this->tipoBeneficio->idTipoBeneficio === $otroBeneficio->tipoBeneficio->idTipoBeneficio;
  }

  protected function _validar() {

    $errores = array();

    if (!empty($this->idBeneficio) && !is_numeric($this->idBeneficio)) {
      $errores['idBeneficio'] = 'Por favor revise el ID de Beneficio.';
    }
    if (!empty($this->idComunicacion) && !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if ((isset($this->tipoAplicacionBeneficio->idTipoAplicacionBeneficio) && !is_numeric($this->tipoAplicacionBeneficio->idTipoAplicacionBeneficio)) || (isset($this->tipoAplicacionBeneficio->idTipoAplicacionBeneficio) && strlen($this->tipoAplicacionBeneficio->idTipoAplicacionBeneficio) > 11)) {
      $errores['tipoAplicacionBeneficio'] = 'Por favor revise el tipo aplicación beneficio';
    }
    if (!empty($this->valor) && strlen($this->valor) > 45) {
      $errores['valor'] = 'Por favor revise el valor.';
    }
    if (!empty($this->tope) && !is_numeric($this->tope)) {
      $errores['tope'] = 'Por favor revise el tope.';
    }
    if ( (!empty($this->cuota) && !is_numeric($this->cuota)) || (!empty($this->cuota) && strlen($this->cuota) > 11)) {
      $errores['cuota'] = 'Por favor revise la cuota.';
    }
    if (empty($this->tipoBeneficio) || !is_numeric($this->tipoBeneficio) || strlen($this->tipoBeneficio) > 11) {
      $errores['tipoBeneficio'] = 'Por favor revise el tipo de beneficio.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}