<?php

namespace Fgo\Bo;

class ImagenBo extends GeneralBo {

  const TIPO_IMAGEN_PORTAL = 1;
  const TIPO_IMAGEN_LOGO = 2;
  const PATH_IMAGE = '/sites/default/files/';

  public $idImagen;
  public $datosImagen;
  public $codigoTipoImagen;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ImagenDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idImagen = $registro->id_imagen;
    $fidBo = new FidBo($registro->fid);
    $this->datosImagen = $fidBo;
    $this->codigoTipoImagen = intval($registro->codigo_tipo_imagen);
    return $this;
  }

  public function esImagenPortal() {
    return ($this->codigoTipoImagen === ImagenBo::TIPO_IMAGEN_PORTAL);
  }

  /**
   * Devuelve la ruta relativa de la imagen en el servidor. Sino existe devuelve null;
   * @return string
   */
  public function obtenerNombreRuta() {
    $nombreRuta = null;
    if ($this->datosImagen) {
      $nombreRuta = str_replace('public://',self::PATH_IMAGE,$this->datosImagen->uri);
    }
    return $nombreRuta;
  }

  public function buscarPorFid($fid) {
    $object = null;
    $registro = $this->repo->buscarPorFid($fid);
    if ($registro) {
      $object = $this->construirDesdeRegistro($registro);
    }
    return $object;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idImagen) && !is_numeric($this->idImagen)) {
      $errores['idImagen'] = 'Por favor revise el ID de la entidad.';
    }
    if (isset($this->datosImagen->fid) && !is_numeric($this->datosImagen->fid)) {
      $errores['datosImagen'] = 'Por favor revise el fid de la Imagen.';
    }
    if (empty($this->codigoTipoImagen) || !is_numeric($this->codigoTipoImagen)) {
      $errores['codigoTipoImagen'] = 'Por favor revise el código tipo de la imagen.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}