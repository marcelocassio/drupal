<?php

namespace Fgo\Bo;

use Fgo\Dao\CarruselDao;

class CarruselBo extends GeneralBo
{
    public $idCarruselHome;
    public $nombre;
    public $idRubro;
    public $publicado;

    protected function obtenerRepo()
    {
        $this->repo = CarruselDao::getInstance();
    }

    public function construirDesdeRegistro($registro)
    {
        $this->idCarruselHome = $registro->id_carousel_home;
        $this->nombre = $registro->nombre;
        $this->idRubro =$registro->id_rubro;
        $this->publicado =$registro->publicado;

        return $this;
    }

    public static function obtenerCarruseles()
    {
        return CarruselDao::getInstance()->listar();
    }
}
