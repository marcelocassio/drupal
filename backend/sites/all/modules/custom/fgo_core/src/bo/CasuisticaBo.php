<?php

namespace Fgo\Bo;

use Fgo\Dao\CasuisticaDao;

class CasuisticaBo extends GeneralBo {

  public $idCasuistica;
  public $idCasuisticaSuip;
  public $descripcion;

  protected function construirDesdeRegistro($registro) {
    $this->idCasuistica = $registro->id_casuistica;
    $this->idCasuisticaSuip = $registro->id_casuistica_suip;
    $this->descripcion = $registro->descripcion;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idCasuisticaSuip = $dto->id_casuistica_suip;
    $this->descripcion = $dto->descripcion;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\CasuisticaDao::getInstance();
  }

  static public function listarPorIdCasuisticaSuip() {
    $listado = array();
    $registros = \Fgo\Dao\CasuisticaDao::getInstance()->listar();
    foreach ($registros as $registro) {
      $casuistica = new CasuisticaBo();
      $casuistica->construirDesdeRegistro($registro);
      $listado[$casuistica->idCasuisticaSuip] = $casuistica;
    }
    return $listado;
  }

  static public function buscarPorIdCasuisticaSuip($idCasuisticaSuip) {
    $casuisticaBo = null;
    $registros = CasuisticaDao::getInstance()->filtrarPorIdCasuisticaSuip($idCasuisticaSuip);
    if (isset($registros[0])) {
      $casuisticaBo = new CasuisticaBo();
      $casuisticaBo->construirDesdeRegistro($registros[0]);
    }
    return $casuisticaBo;
  }

  protected function _validar() {
    $errores = array();
    if (!empty($this->idCasuistica) && !is_numeric($this->idCasuistica)) {
      $errores['idCasuistica'] = 'Por favor revise el ID de la casuistica.';
    }
    if (!empty($this->idCasuisticaSuip) && !is_numeric($this->idCasuisticaSuip)) {
      $errores['idCasuisticaSuip'] = 'Por favor revise el ID de casuistica SUIP.';
    }
    if (!empty($this->descripcion) && strlen($this->descripcion) > 100) {
      $errores['descripcion'] = 'Por favor revise la descripcion casuistica.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}