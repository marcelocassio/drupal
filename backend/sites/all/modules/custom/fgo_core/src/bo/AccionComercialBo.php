<?php
namespace Fgo\Bo;

class AccionComercialBo extends GeneralBo {

	public $idAccionComercial;
	public $idAccionComercialSuip;
	public $nombre;

	public function construirDesdeLineaArchivo($dto) {
	  $this->idAccionComercialSuip = $dto->id_tipo_accion_comercial_suip;
	  $this->nombre = $dto->nombre;
  }

  protected function construirDesdeRegistro($registro){
    $this->idAccionComercial = $registro->id_accion_comercial;
    $this->idAccionComercialSuip = $registro->id_accion_comercial_suip;
    $this->nombre = $registro->nombre;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\AccionComercialDao::getInstance();
  }

  public function listarPorIdAccionComercialSuip() {
    $tiposAccionComercial = $this->listar();
    $mapaIdComercioSuip = array();
    foreach ($tiposAccionComercial as $accionComercialBo) {
      if ($accionComercialBo->idAccionComercialSuip != null) {
        $mapaIdComercioSuip[$accionComercialBo->idAccionComercialSuip] = $accionComercialBo;
      }
    }
    return $mapaIdComercioSuip;
  }

  protected function _validar() {
    $errores = false;

    if (!empty($this->idAccionComercial) && !is_numeric($this->idAccionComercial)) {
      $errores['idAccionComercial'] = 'Por favor revise el ID de Accion Comercial.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 100) {
      $errores['nombre'] = 'Por favor revise el nombre de la Acción Comercial.';
    }
    if(empty($this->idAccionComercialSuip) || !is_numeric($this->idAccionComercialSuip)) {
      $errores['idAccionComercialSuip'] = "Por favor revise el ID de SUIP.";
    }

    if($errores) {
      throw new DatosInvalidosException($errores);
    }
    return true;
  }
}
