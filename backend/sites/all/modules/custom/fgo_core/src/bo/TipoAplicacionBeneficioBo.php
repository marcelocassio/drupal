<?php

namespace Fgo\Bo;

class TipoAplicacionBeneficioBo extends GeneralBo {

  public $idTipoAplicacionBeneficio;
  public $idTipoAplicacionBeneficioSuip;
  public $nombre;


  public function construirDesdeLineaArchivo($dto) {
    $this->idTipoAplicacionBeneficioSuip = $dto->id_tipo_aplicacion_beneficio_suip;
    $this->nombre = $dto->nombre;
  }

  protected function construirDesdeRegistro($registro) {
    $this->nombre = $registro->nombre;
    $this->idTipoAplicacionBeneficio = $registro->id_tipo_aplicacion_beneficio;
    $this->idTipoAplicacionBeneficioSuip = $registro->id_tipo_aplicacion_beneficio_suip;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\TipoAplicacionBeneficioDao::getInstance();
  }

  public function listarPorIdAplicacionBeneficioSuip() {
    $listadoClaveIdSuip = array();
    $objetos = $this->listar();
    foreach ($objetos as $objeto) {
      if (isset($objeto->idTipoAplicacionBeneficioSuip)) {
        $listadoClaveIdSuip[$objeto->idTipoAplicacionBeneficioSuip] = $objeto;
      }
    }
    return $listadoClaveIdSuip;
  }

  public function buscarPorIdTipoAplicacionBeneficio($idTipoAplicacionBeneficioSuip) {
    $tipoAplicacionBeneficioBo = null;
    $registros = $this->repo->filtrarPorIdTipoAplicacionBeneficioSuip($idTipoAplicacionBeneficioSuip);
    if (isset($registros[0])) {
      $tipoAplicacionBeneficioBo = new TipoAplicacionBeneficioBo();
      $tipoAplicacionBeneficioBo->construirDesdeRegistro($registros[0]);
    }
    return $tipoAplicacionBeneficioBo;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idTipoAplicacionBeneficio) && !is_numeric($this->idTipoAplicacionBeneficio)) {
      $errores['idTipoAplicacionBeneficio'] = 'Por favor revise el ID de la entidad.';
    }
    if (empty($this->idTipoAplicacionBeneficioSuip) || !is_numeric($this->idTipoAplicacionBeneficioSuip) || strlen($this->idTipoAplicacionBeneficioSuip) > 11) {
      $errores['idTipoAplicacionBeneficioSuip'] = 'Por favor revise el ID Tipo Aplicación Suip.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 100) {
      $errores['nombre'] = 'Por favor revise el nombre del Tipo Aplicación.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}