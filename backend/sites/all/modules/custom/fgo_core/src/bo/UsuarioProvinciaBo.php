<?php

namespace Fgo\Bo;

class UsuarioProvinciaBo extends GeneralBo {
  public $idUsuarioProvincia;
  public $idUsuario;
  public $idProvincia;
  public $provincia;
  public $provincias = array();

  public function construir($provinciaBo, $usuarioBo) {
    $this->idProvincia = $provinciaBo->idProvincia;
    $this->idUsuario = $usuarioBo->idUsuario;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\UsuarioProvinciaDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idUsuarioProvincia = $registro->id_usuario_provincia;
    $this->idUsuario = $registro->uid;
    $this->idProvincia = $registro->id_provincia;
    if ($registro->id_provincia != null) {
      $this->setProvincia(new \Fgo\Bo\ProvinciaBo($registro->id_provincia));
    }

    return $this;
  }

  
  public function construirDesdeLineaArchivo($dto) {

    $this->idUsuario = $dto->uid;
    if ($dto->id_provincia != null) {
      $provincia = new \Fgo\Bo\ProvinciaBo($dto->id_provincia);
      $this->setProvincia($provincia);
      $this->idProvincia= $provincia->idProvincia;
    }
  }

  public function buscarPorIdUsuario($idUsuario) {
    $this->provincias = array();
    $registros = $this->repo->filtrarPorIdUsuario($idUsuario);
    foreach ($registros as $registro) {
      $provincia = new \Fgo\Bo\ProvinciaBo($registro->id_provincia);
      $this->provincias[] = $provincia;
    }

    return $this;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idUsuarioProvincia) && !is_numeric($this->idUsuarioProvincia)) {
      $errores['idUsuarioProvincia'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idUsuario) || !is_numeric($this->idUsuario)) {
      $errores['idUsuario'] = 'Por favor revise el ID del usuario.';
    }
    if (empty($this->idProvincia) || !is_numeric($this->idProvincia)) {
      $errores['idProvincia'] = 'Por favor revise el ID de la provincia.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

   /**
   * @return mixed
   */
  public function getProvincia() {
    return $this->provincia;
  }

  /**
   * @param mixed $provincia
   */
  public function setProvincia($provincia) {
    $this->provincia = $provincia;
  }
}