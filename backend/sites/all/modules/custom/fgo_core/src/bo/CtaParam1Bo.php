<?php

namespace Fgo\Bo;

use Fgo\Dao\CtaParam1Dao;

class CtaParam1Bo extends GeneralBo
{
    public $idParam1;
    public $nombre;
    public $callToAction;

    protected function obtenerRepo()
    {
        $this->repo = CtaParam1Dao::getInstance();
    }

    public function construirDesdeRegistro($registro)
    {
        $this->idParam1 = $registro->id_param1;
        $this->nombre = $registro->nombre;
        $this->callToAction = $registro->call_to_action;
        return $this;
    }

    public function obtenerRegistroActual()
    {
        return $this->construirDesdeRegistro($this->repo->obtenerRegistroActual());
    }
}