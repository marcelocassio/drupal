<?php

namespace Fgo\Bo;

class CampaniaBo extends GeneralBo {

  public $idCampania;
  public $idCampaniaSuip;
  public $nombre;
  public $comunicaciones;
  public $fechaDesde;
  public $fechaHasta;
  public $siempreVisible;

  function construirDesdeLineaArchivo($dto) {
    $this->idCampaniaSuip = $dto->id_campania_suip;
    $this->nombre = $dto->nombre;
    $this->fechaDesde = $dto->fecha_desde;
    $this->fechaHasta = $dto->fecha_hasta;
    $this->siempreVisible = 0;
  }

  protected function construirDesdeRegistro($registro) {
    $this->construirDatosBasicos($registro);
    $campaniaComunicacionBo = new CampaniaComunicacionBo();
    $this->comunicaciones = $campaniaComunicacionBo->buscarPorIdCampania($registro->id_campania)->comunicaciones;
    return $this;
  }

  public function construirDatosBasicos($registro) {
    $this->idCampania = $registro->id_campania;
    $this->idCampaniaSuip = $registro->id_campania_suip;
    $this->nombre = $registro->nombre;
    $this->fechaDesde = $registro->fecha_desde;
    $this->fechaHasta = $registro->fecha_hasta;
    $this->siempreVisible = $registro->siempre_visible;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\CampaniaDao::getInstance();
  }

  public function listarBasico($orden = null) {
    $registros = $this->repo->listar($orden);
    foreach ($registros as $registro) {
      $objeto = new CampaniaBo();
      $listadoObjetosNegocio[] = $objeto->construirDatosBasicos($registro);
    }
    return $listadoObjetosNegocio;
  }

  public function listarPorIdCampaniaSuip() {
    $campanias = $this->listarBasico();
    $mapaIdCampaniaSuip = array();

    foreach ($campanias as $campaniaBo) {
      if ($campaniaBo->idCampaniaSuip != null) {
        $mapaIdCampaniaSuip[$campaniaBo->idCampaniaSuip] = $campaniaBo;
      }
    }
    return $mapaIdCampaniaSuip;
  }

  public static function obtenerIdCampaniaPorIdSuip($idCampaniaSuip){
    $registro = \Fgo\Dao\CampaniaDao::getInstance()->buscarPorIdSuip($idCampaniaSuip);
    return isset($registro) ? $registro->id_campania : null;
  }


  public static function buscarPorIdSuip($idCampaniaSuip) {
    $registro = \Fgo\Dao\CampaniaDao::getInstance()->buscarPorIdSuip($idCampaniaSuip);
    $object = null;
    if ($registro) {
      $object = new CampaniaBo();
      $object->construirDesdeRegistro($registro);
    }
    return $object;
  }

  public function recuperarPorId($idCampania) {
    $registro = $this->repo->buscarPorId($idCampania);
    $objet = null;
    if ($registro) {
      $campania = new CampaniaBo();
      $object = $campania->construirDatosBasicos($registro);
    }
    return $object;
  }

  public function esVisible($fecha = false) {
    if($this->siempreVisible == 1) {
      return true;
    }
    if(!$fecha) {
      $fecha = strtotime("today");
    }
    if(date('N',time()) == 6 ) {
      $fechaDesde = $this->fechaDesde - 172800; // Se ve dos dias antes
    } else {
      $fechaDesde = $this->fechaDesde - 86400; // Se ve desde un dia antes
    }
    $fechaHasta = $this->fechaHasta;

    if($fecha >= $fechaDesde && $fecha <= $fechaHasta) {
      return true;
    }

    return false;
  }

  protected function _validar() {

    $errores = array();

    if (!empty($this->idCampania) && !is_numeric($this->idCampania)) {
      $errores['idCampania'] = 'Por favor revise el ID de Campaña.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 100) {
      $errores['nombre'] = 'Por favor revise el nombre de la Campaña.';
    }
    if (!empty($this->idCampaniaSuip) && strlen($this->idCampaniaSuip) > 11) {
      $errores['idCampaniaSuip'] = 'Por favor revise el ID CampaniaSuip.';
    }
    if ($this->fechaHasta < $this->fechaDesde) {
      $errores['fechaHasta'] = 'La fecha hasta no puede ser menor a la fecha desde.';
    }
    if ($this->nombre == null || $this->nombre === '') {
      $errores['nombre'] = 'El nombre de la campaña no puede estar vacio.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

  public function contieneComunicacion($comunicacionBo) {
    $contiene = false;
    if ($this->comunicaciones && $comunicacionBo) {
      foreach ($this->comunicaciones as $itemComunicacion) {
        if ($itemComunicacion->esIgual($comunicacionBo)) {
          $contiene = true;
          break;
        }
      }
    }
    return $contiene;
  }
}
