<?php

namespace Fgo\Bo;

class SlideBo extends GeneralBo {
  public $idSlide;
  public $imagenWeb;
  public $imagenApp;
  public $cta;
  public $fechaDesde;
  public $fechaHasta;
  public $publicado;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\SlideDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idSlide = $registro->id_slide;
    $fidWeb = new FidBo($registro->fid_web);
    $this->imagenWeb = $fidWeb;
    $fidApp = new FidBo($registro->fid_app);
    $this->imagenApp = $fidApp;
    $this->cta = new CtaBo($registro->id_cta);
    $this->fechaDesde = $registro->fecha_desde;
    $this->fechaHasta = $registro->fecha_hasta;
    $this->publicado = $registro->publicado;

    return $this;
  }

  public function guardar() {
    $id = parent::guardar();
    if($id) {
      $slideFechas = new SlideFechaBo();
      $slideFechas->generarMasivos($id, $this->fechaDesde, $this->fechaHasta);
    }
  }

  protected function _validar() {
    $errores = false;
    // Si esta publicado valido que tengan las imagenes cargadas y que tenga fechas cargadas
    if($this->publicado == 1) {
      if(!isset($this->imagenWeb->datosImagen->fid) || !is_numeric($this->imagenWeb->datosImagen->fid) || $this->imagenWeb->datosImagen->fid < 1) {
        $errores['imagenWeb'] = "Por favor revise la imagen web.";
      }
      if(!isset($this->imagenApp->datosImagen->fid) || !is_numeric($this->imagenApp->datosImagen->fid) || $this->imagenApp->datosImagen->fid < 1) {
        $errores['imagenApp'] = "Por favor revise la imagen app.";
      }
      if (empty($this->fechaDesde)){
        $errores['fechaDesde'] = "Slide publicado debe tener Fecha Desde cargada";
      }

      if (empty($this->fechaHasta)){
        $errores['fechaHasta'] = "Slide publicado debe tener Fecha Hasta cargada";
      }

      if ($this->fechaDesde < strtotime(date('d-m-Y'))){
        $errores['fechaDesde'] = "Fecha Desde no puede ser menor que la fecha actual";
      }
    }

    //La fecha hasta no puede ser menor a la fecha Desde
    if (!empty($this->fechaDesde) && !empty($this->fechaHasta)){
      if($this->fechaDesde > $this->fechaHasta){
        $errores['fechaDesde'] = "Fecha Desde no puede ser mayor que la Fecha Hasta";
      }
    }

    // CTA
    if($this->cta->idCta == 0) {
      $errores['callToActionSeccion'] = "Por favor revise la sección del link.";
    }

    // Comunicacion este vigente durante la vigencia del slide
    if($this->cta->param1 == 1) {
      $comunicacion = new ComunicacionBo($this->cta->param2);
      if($comunicacion->idComunicacion) {
        if(!$comunicacion->esVisible($this->fechaDesde) || !$comunicacion->esVisible($this->fechaHasta)) {
          $errores['callToActionDireccion_1'] = "Hay días que la comunicación no es visible dentro de la vigencia del slide.";
        }
      }
    }

    // Campaña este vigente durante la vigencia del slide
    if($this->cta->param1 == 2) {
      $campania = new CampaniaBo();
      $campania->buscarPorId($this->cta->param2);
      if($campania->idCampania) {
        if(!$campania->esVisible($this->fechaDesde) || !$campania->esVisible($this->fechaHasta)) {
          $errores['callToActionDireccion'] = "Hay días que la campaña no es visible dentro de la vigencia del slide.";
        }
      }
    }


    if($errores) {
      throw new DatosInvalidosException($errores);
    }
    return true;
  }
}