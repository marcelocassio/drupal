<?php

namespace Fgo\Bo;

class SucursalVirtualBo extends GeneralBo {
  public $idSucursalVirtual;
  public $idSucursalVirtualSuip;
  public $web;
  public $tlf;


  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\SucursalVirtualDao::getInstance();
  }

  protected function construirDesdeRegistro($registro){
    $this->idSucursalVirtual = $registro->id_sucursal_virtual;
    $this->idSucursalVirtualSuip = $registro->id_sucursal_virtual_suip;
    $this->web = $registro->web;
    $this->tlf = $registro->telefono;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idSucursalVirtualSuip = $dto->id_sucursal_virtual_suip;
    $this->web = $dto->web;
    $this->tlf = $dto->telefono;
  }

  public function listarPorIdComunicacion($id){
    $listadoSucursalesVirtuales = array();
    $result = $this->repo->listarPorIdComunicacion($id);
    foreach ($result as $registro) {
      $entidad = new $this;
      $entidad = $entidad->construirDesdeRegistro($registro);
      $listadoSucursalesVirtuales[] = $entidad;
      unset($entidad);
    }

    return $listadoSucursalesVirtuales;
  }

  public function buscarPorIdSuip($idSuip) {
    $registro = $this->repo->buscarPorIdSuip($idSuip);
    if ($registro) {
      $object = $this->construirDesdeRegistro($registro);
    }
    return $object;
  }

  public function listarPorIdSucursalVirtualSuip() {
    $listadoClaveIdSuip = array();
    $listadoSucursalesVirtuales = $this->listar();
    foreach ($listadoSucursalesVirtuales as $registro) {
      $idSuip = $registro->idSucursalVirtualSuip;
      if (isset($idSuip)) {
        $listadoClaveIdSuip[$idSuip] = $registro;
      }
    }
    return $listadoClaveIdSuip;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idSucursalVirtual) && !is_numeric($this->idSucursalVirtual)) {
      $errores['idSucursalVirtual'] = 'Por favor revise el ID de la entidad.';
    }
    if ((!empty($this->idSucursalVirtualSuip) && !is_numeric($this->idSucursalVirtualSuip)) || (!empty($this->idSucursalVirtualSuip) && strlen($this->idSucursalVirtualSuip) > 11)) {
      $errores['idSucursalVirtualSuip'] = 'Por favor revise el ID Sucursal Virtual Suip.';
    }
    if (!empty($this->web)) {
      $url = parse_url($this->web);
      if (strlen($this->web) > 100 || ($url['scheme'] != 'https' && $url['scheme'] != 'http')){
        if ((strpos($url['path'], 'www') === false)
          && (strpos($url['path'], 'WWW') === false)) {
          $errores['web'] = 'Por favor revise la web de la Sucursal Virtual: ' . $this->web;
        }
      }
    }
    if (!empty($this->tlf) && strlen($this->tlf) > 30) {
      $errores['telefono'] = 'Por favor revise el teléfono de la Sucursal Virtual.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}