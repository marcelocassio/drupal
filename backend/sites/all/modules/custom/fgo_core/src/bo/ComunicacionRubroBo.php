<?php

namespace Fgo\Bo;

class ComunicacionRubroBo extends GeneralBo {
  public $idComunicacionRubro;
  public $idComunicacion;
  public $idRubro;
  public $rubros = array();

  public function construir($rubroBo, $comunicacionBo) {
    $this->idRubro = $rubroBo->idRubro;
    $this->idComunicacion = $comunicacionBo->idComunicacion;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ComunicacionRubroDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idComunicacionRubro = $registro->id_comunicacion_rubro;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idRubro = $registro->id_rubro;
    return $this;
  }

  public function buscarPorIdComunicacion($idComunicacion) {
    $this->rubros = array();
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    foreach ($registros as $registro) {
      $rubro = new RubroBo($registro->id_rubro);
      $this->rubros[] = $rubro;
    }

    return $this;
  }

  public function listarPorIdSuipCombinado() {
    
    $listado = $this->listar();
    
    $matrizCombinada = array();
    foreach ($listado as $comunicacionRubroBo) {
      $matrizCombinada[$comunicacionRubroBo->idComunicacion][$comunicacionRubroBo->idRubro] = $comunicacionRubroBo;
    }
    return $matrizCombinada;
  }

  public static function listarComunicacionRubroOrden($fechaDesde, $fechaHasta) {
    $listado = \Fgo\Dao\ComunicacionRubroDao::getInstance()->listarComunicacionRubroOrden($fechaDesde, $fechaHasta);
    return $listado;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idComunicacionRubro) && !is_numeric($this->idComunicacionRubro)) {
      $errores['idComunicacionRubro'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComunicacion) || !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if (empty($this->idRubro) || !is_numeric($this->idRubro)) {
      $errores['idRubro'] = 'Por favor revise el ID del rubro.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}