<?php

namespace Fgo\Bo;


class PromoCuponBo extends GeneralBo {

  public $idPromoCupon;
  public $idCupon;
  public $idComercio;
  public $idComunicacion;
  public $idBeneficioSuip;

  protected function construirDesdeRegistro($registro) {
    $this->idPromoCupon = $registro->id_promo_cupon;
    $this->idCupon = $registro->id_cupon;
    $this->idComercio = $registro->id_comercio;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idBeneficioSuip = $registro->id_beneficio_suip;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\PromoCuponDao::getInstance();
  }

  public static function buscarComercioPorIdCupon($idCupon) {
    $comercioBo = null;
    $registros = \Fgo\Dao\PromoCuponDao::getInstance()->filtrarPorIdCupon($idCupon);
    if (isset($registros[0])) {
      $comercioBo = new ComercioBo($registros[0]->id_comercio);
    }
    return $comercioBo;
  }

  public function buscarPromoCuponPorIdComunicacion($idComunicacion) {
    $promoCuponBo = null;
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    if (isset($registros[0])) {
      $promoCuponBo = new PromoCuponBo();
      $promoCuponBo->construirDesdeRegistro($registros[0]);
    }
    return $promoCuponBo;
  }

  public function buscarPorIdCupon($idCupon) {
    $promoCuponBo = null;
    $registros = $this->repo->filtrarPorIdCupon($idCupon);

    if (isset($registros[0])) {
      $promoCuponBo = new PromoCuponBo();
      $promoCuponBo->construirDesdeRegistro($registros[0]);
    }
    return $promoCuponBo;
  }


  protected function _validar() {
    $errores = array();

    if (!empty($this->idPromoCupon) && !is_numeric($this->idPromoCupon)) {
      $errores['idPromoCupon'] = 'Por favor revise el ID de la entidad.';
    }
    if (empty($this->idCupon) || strlen($this->idCupon) > 11 || !is_numeric($this->idCupon)) {
      $errores['idCupon'] = 'Por favor revise el ID cupón.';
    }
    if (empty($this->idComercio) || strlen($this->idComercio) > 11 || !is_numeric($this->idComercio)) {
      $errores['idComercio'] = 'Por favor revise el ID del comercio.';
    }
    if ((!empty($this->idComunicacion) && !is_numeric($this->idComunicacion)) || (!empty($this->idComunicacion) && strlen($this->idComunicacion) > 11)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la Comunicación.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

  /**
   * Recupera una PromoCuponBo sin comunicacion suip
   * @return PromoCuponBo|null
   */
  public static function buscarPromoCuponSinComunicacionSuip() {
    $promoCuponBo = null;
    $registros = \Fgo\Dao\PromoCuponDao::getInstance()->filtrarConComunicacionPendiente();
    if (isset($registros[0])) {
      $promoCuponBo = new PromoCuponBo();
      $promoCuponBo->construirDesdeRegistro($registros[0]);
    }
    return $promoCuponBo;

  }

}