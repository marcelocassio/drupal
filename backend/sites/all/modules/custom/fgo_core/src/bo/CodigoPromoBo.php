<?php

namespace Fgo\Bo;

class CodigoPromoBo extends GeneralBo {
  public $idCodigoPromo;
  public $codigoPromo;
  public $enviadoAVisa;

  protected function construirDesdeRegistro($registro)  {
    $this->idCodigoPromo = $registro->id_codigo_promo;
    $this->codigoPromo = $registro->codigo_promo;
    $this->enviadoAVisa = $registro->enviado_a_visa;

    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\CodigoPromoDao::getInstance();
  }

  public function buscarPorCodigoPromo($codigo_promo) {
    $registro = $this->repo->buscarPorCodigoPromo($codigo_promo);
    if ($registro) {
      $this->construirDesdeRegistro($registro);
    }
    return $this;
  }

  protected function _validar() {

    $errores = array();
    if (!empty($this->idCodigoPromo) && !is_numeric($this->idCodigoPromo)) {
      $errores['idCodigoPromo'] = 'Por favor revise el ID del Codigo Promo.';
    }
    if (!isset($this->codigoPromo) || !is_numeric($this->codigoPromo)) {
      $errores['idCodigoPromo'] = 'Por favor revise el codigo promo.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}