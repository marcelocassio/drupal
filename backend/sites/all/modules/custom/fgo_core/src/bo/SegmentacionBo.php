<?php

namespace Fgo\Bo;

define('SEGMENTACION_SOS_CLIENTE', "Cupones y sorteos");
define('SEGMENTACION_NO_SOS_CLIENTE', "No cliente");
define('SEGMENTACION_TIPO_SEAS_O_NO_CLIENTE', "seas_o_no_cliente");

use \Fgo\Dao\SegmentacionDao;

class SegmentacionBo extends GeneralBo {

  public $id_fgo_segmentacion;
  public $cliente;
  public $segmento_cod;
  public $segmento_desc;
  public $es_cliente_actividad;
  public $tiene_ca_ars_en_actividad;
  public $tiene_ca_usd_en_actividad;
  public $tiene_cc_ars_en_actividad;
  public $tiene_cc_usd_en_actividad;
  public $tiene_tc_visa_en_actividad;
  public $tiene_tc_master_en_actividad;
  public $tiene_pp_en_actividad;
  public $es_titular_tc_visa;
  public $es_titular_tc_visa_latam;
  public $es_titular_tc_visa_nolatam;
  public $es_titular_tc_visa_nolatam_inter;
  public $es_titular_tc_master;
  public $es_titular_tc_master_latam;
  public $es_titular_tc_master_nolatam;
  public $es_titular_tc_master_nolatam_inter;
  public $es_adicional_tc_visa;
  public $es_adicional_tc_visa_latam;
  public $es_adicional_tc_visa_nolatam;
  public $es_adicional_tc_visa_nolatam_inter;
  public $es_adicional_tc_master;
  public $es_adicional_tc_master_latam;
  public $es_adicional_tc_master_nolatam;
  public $es_adicional_tc_master_nolatam_inter;
  public $es_ps;
  public $subscription_close_dt;
  public $business_area_cd;
  public $col_xt_2;
  public $col_xt_3;
  public $col_xt_4;

  
  protected function obtenerRepo() {
    $this->repo = SegmentacionDao::getInstance();
  }

  /**
   * Modifica el estado del objeto con los valores del registro de la tabla
   * @param $registro
   * @return $this
   */
  public function construirDesdeRegistro($registro) {
    $this->id_fgo_segmentacion = $registro->id_fgo_segmentacion;
    $this->cliente = $registro->cliente;
    $this->segmento_cod = $registro->segmento_cod;
    $this->segmento_desc = $registro->segmento_desc;
    $this->es_cliente_actividad = $registro->es_cliente_actividad;
    $this->tiene_ca_ars_en_actividad = $registro->tiene_ca_ars_en_actividad;
    $this->tiene_ca_usd_en_actividad = $registro->tiene_ca_usd_en_actividad;
    $this->tiene_cc_ars_en_actividad = $registro->tiene_cc_ars_en_actividad;
    $this->tiene_cc_usd_en_actividad = $registro->tiene_cc_usd_en_actividad;
    $this->tiene_tc_visa_en_actividad = $registro->tiene_tc_visa_en_actividad;
    $this->tiene_tc_master_en_actividad = $registro->tiene_tc_master_en_actividad;
    $this->tiene_pp_en_actividad = $registro->tiene_pp_en_actividad;
    $this->es_titular_tc_visa = $registro->es_titular_tc_visa;
    $this->es_titular_tc_visa_latam = $registro->es_titular_tc_visa_latam;
    $this->es_titular_tc_visa_nolatam = $registro->es_titular_tc_visa_nolatam;
    $this->es_titular_tc_visa_nolatam_inter = $registro->es_titular_tc_visa_nolatam_inter;
    $this->es_titular_tc_master = $registro->es_titular_tc_master;
    $this->es_titular_tc_master_latam = $registro->es_titular_tc_master_latam;
    $this->es_titular_tc_master_nolatam = $registro->es_titular_tc_master_nolatam;
    $this->es_titular_tc_master_nolatam_inter = $registro->es_titular_tc_master_nolatam_inter;
    $this->es_adicional_tc_visa = $registro->es_adicional_tc_visa;
    $this->es_adicional_tc_visa_latam = $registro->es_adicional_tc_visa_latam;
    $this->es_adicional_tc_visa_nolatam = $registro->es_adicional_tc_visa_nolatam;
    $this->es_adicional_tc_visa_nolatam_inter = $registro->es_adicional_tc_visa_nolatam_inter;
    $this->es_adicional_tc_master = $registro->es_adicional_tc_master;
    $this->es_adicional_tc_master_latam = $registro->es_adicional_tc_master_latam;
    $this->es_adicional_tc_master_nolatam = $registro->es_adicional_tc_master_nolatam;
    $this->es_adicional_tc_master_nolatam_inter = $registro->es_adicional_tc_master_nolatam_inter;
    $this->es_ps = $registro->es_ps;
    $this->subscription_close_dt = $registro->subscription_close_dt;
    $this->business_area_cd = $registro->business_area_cd;
    $this->col_xt_2 = $registro->col_xt_2;
    $this->col_xt_3 = $registro->col_xt_3;
    $this->col_xt_4 = $registro->col_xt_4;
      
    return $this;
  }

  static public function obtenerSegmentaciones() {
    $listado = array();
    $registros = SegmentacionDao::getInstance()->obtenerSegmentaciones();
    
    helperDebug('obtenerSegmentaciones');
    foreach ($registros as $registro) {
      $segmentacion = new SegmentacionBo($registro->id_fgo_segmentacion);
      $listado[] = $segmentacion;
    }

    return $listado;
  }

  /**
   * Abstraccion para saber si es cliente
   */
  public function esCliente(){
    $registro = $this->repo->obtenerSegmentacionUsuario();
    if ($registro) {
      $segmentacion = $this->construirDesdeRegistro($registro);
      return $this->repo->esCliente($segmentacion);
    }
    return false;
  }

  /**
   * Abstraccion para obtener las segmentacions del usuario
   */
  public function obtenerSegmentacionUsuario($clienteAltamira = false){
    return $this->repo->obtenerSegmentacionUsuario($clienteAltamira);
  }
  
  public function esLatam($resService = null){
    if(isset($resService->esLatam)){
        return $resService->esLatam == "S";
    }
    $registro = $this->repo->obtenerSegmentacionUsuario();
    if ($registro) {
      $segmentacion = $this->construirDesdeRegistro($registro);
      return $this->repo->esLatam($segmentacion);
    }
    return true;
  }
  
  public function esMaster($resService = null){
    if(isset($resService->esMaster)){
        return $resService->esMaster;
    }
    $registro = $this->repo->obtenerSegmentacionUsuario();
    if ($registro) {
      $segmentacion = $this->construirDesdeRegistro($registro);
      return $this->repo->esMaster($segmentacion);
    }
    return true;
  }
  /**
   * segmentar solo para usuarios determinados, si es "enable", no segmentar
   * @param type $resService
   * @return boolean
   */
  public function esDomoQr($resService = null){
    if(variable_get("fgo_segmentacion_front_qr_enable",false) == "enable"){
        return true;
    }
    $registro = $this->repo->obtenerSegmentacionUsuario();
    if ($registro) {
      $segmentacion = $this->construirDesdeRegistro($registro);
      return $this->repo->esDomoQr($segmentacion);
    }
    return false;
  }
  
  public function esDomoP2p($resService = null){
      if(variable_get("fgo_segmentacion_front_p2p_enable",false) == "enable"){
        return true;
    }
    $registro = $this->repo->obtenerSegmentacionUsuario();
    if ($registro) {
      $segmentacion = $this->construirDesdeRegistro($registro);
      return $this->repo->esDomoP2p($segmentacion);
    }
    return false;
  }
  
  public function listarPage($page, $order = null) {
    $listadoObjetosNegocio = array();
    $registros = $this->repo->listarPage($page, $order);

    foreach ($registros as $registro) {
      $objeto = new $this;
      $listadoObjetosNegocio[] = $objeto->construirDesdeRegistro($registro);
    }
    return $listadoObjetosNegocio;
  }
  
  
  public function buscarPorCliente($id) {
    $registro = $this->repo->buscarPorCliente($id);
    if ($registro) {
        $this->construirDesdeRegistro($registro);
    }
    return $this;
  }
  
  
  /**
   * solo se uso para obtener un promedio de consumo
   * @deprecated since version v2.3.14  
   * @global type $user
   */
    public function invocarServicio() {
        global $user;
        $cliente = $user->name;

        if(variable_get("bbva_segmentacion_service_contador") == "enable"){
          $cache_clientes = cache_get("bi_clientes");
          $bi_clientes = $cache_clientes->data;
          
          if(!array_key_exists($cliente , $bi_clientes)){
            $bi_clientes[$cliente] = date("Y-m-d H:i:s");
            cache_set("bi_clientes", $bi_clientes);

            $cache_consultas = cache_get("bi_consultas");
            $bi_consultas = $cache_consultas->data;
            $bi_consultas[date("Y-m-d H:i")] = $bi_consultas[date("Y-m-d H:i")] + 1;
            cache_set("bi_consultas",$bi_consultas);

            $cache_consultas_seg = cache_get("bi_consultas_seg");
            $bi_consultas_seg = $bi_consultas_seg->data;
            $bi_consultas_seg[date("Y-m-d H:i:s")] = $bi_consultas_seg[date("Y-m-d H:i:s")] + 1;
            cache_set("bi_consultas_seg",$bi_consultas_seg);
          }
        }
    }
    
    public function listarPaginado($form_state)
    {
      return $this->repo->listarPaginado($form_state);
    }
}