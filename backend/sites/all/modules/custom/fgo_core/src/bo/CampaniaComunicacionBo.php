<?php

namespace Fgo\Bo;

class CampaniaComunicacionBo extends GeneralBo {

  public $idCampaniaComunicacion;
  public $idComunicacion;
  public $idCampania;
  public $comunicaciones;

  protected function construirDesdeRegistro($registro) {
    $this->idCampaniaComunicacion = $registro->id_campania_comunicacion;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idCampania = $registro->id_campania;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\CampaniaComunicacionDao::getInstance();
  }

  public function construir($campaniaBo, $comunicacion) {
    $this->idCampania = $campaniaBo->idCampania;
    $this->idComunicacion = $comunicacion->idComunicacion;
    return $this;
  }

  public function campaniaNoContieneComunicacion($idCampania, $idComunicacion){

    $encontrado = true;

    if(!$this->repo->buscarPorIdCampaniaIdComunicacion($idCampania, $idComunicacion)){
      $this->idCampania = $idCampania;
      $this->idComunicacion = $idComunicacion;
      $this->guardar();
      $encontrado = false;
    }

    return $encontrado;
  }

  public function buscarPorIdCampania($idCampania) {
    $this->comunicaciones = array();
    $registros = $this->repo->filtrarPorIdCampania($idCampania);
    foreach ($registros as $registro) {
      $comunicacion = new ComunicacionBo($registro->id_comunicacion);
      $this->comunicaciones[] = $comunicacion;
    }
    return $this;
  }

  public function buscarCampaniaPorIdComunicacion($idComunicacion) {
    $campaniaBo = null;
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    if (!empty($registros)) {
      $campania = new CampaniaBo();
      $campaniaBo = $campania->recuperarPorId($registros[0]->id_campania);
    }
    return $campaniaBo;
  }

  public function listarPorIdCampaniaIdComunicacion() {
    $listado = $this->listar();
    $matrizCampaniaComunicacion = array();
    foreach ($listado as $campaniaComunicacionBo) {
      $matrizCampaniaComunicacion[$campaniaComunicacionBo->idCampania][$campaniaComunicacionBo->idComunicacion] = $campaniaComunicacionBo;
    }
    return $matrizCampaniaComunicacion;
  }

  protected function _validar() {

    $errores = array();
    if (!empty($this->idCampaniaComunicacion) && !is_numeric($this->idCampaniaComunicacion)) {
      $errores['idCampaniaComunicacion'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComunicacion) || !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de Comunicación.';
    }
    if (empty($this->idCampania) || !is_numeric($this->idCampania)) {
      $errores['idCampania'] = 'Por favor revise el ID de Campaña.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}