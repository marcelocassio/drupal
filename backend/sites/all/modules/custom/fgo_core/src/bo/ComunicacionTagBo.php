<?php

namespace Fgo\Bo;


class ComunicacionTagBo extends GeneralBo {

  public $idComunicacionTag;
  public $idComunicacion;
  public $idTag;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ComunicacionTagDao::getInstance();
  }

  protected function construirDesdeRegistro($registro) {
    $this->idComunicacionTag = $registro->id_comunicacion_tag;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idTag = $registro->id_tag;
    return $this;
  }

  public function listarTagPorIdComunicacion($idComunicacion) {
    $tags = array();
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    foreach ($registros as $registro) {
      $tag = new TagBo($registro->id_tag);
      $tags[] = $tag;
    }
    return $tags;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idComunicacionTag) && !is_numeric($this->idComunicacionTag)) {
      $errores['idComunicacionTag'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComunicacion) || !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if (empty($this->idTag) || !is_numeric($this->idTag)) {
      $errores['idTag'] = 'Por favor revise el ID del Tag.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}