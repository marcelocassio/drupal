<?php

namespace Fgo\Bo;

class RubroBo extends GeneralBo {
  const ESTADO_PDM_OCULTO = 3;

  public $idRubro;
  public $idRubroSuip;
  public $nombre;
  public $estadoPdm;
  public $rubroPadre;
  public $imagenes;

  static public function estadosPdm() {
    return array(
      0 => "Visible",
      1 => "Observado",
      2 => "Rechazado",
      3 => "Oculto"
    );
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\RubroDao::getInstance();
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idRubroSuip = $dto->id_rubro_suip;
    $this->nombre = $dto->nombre;
    $this->estadoPdm = ($this->estadoPdm != null)? (int)$this->estadoPdm:0;
  }

  public function construirDesdeRegistro($registro) {
    $this->idRubro = $registro->id_rubro;
    $this->idRubroSuip = $registro->id_rubro_suip;
    $this->nombre = $registro->nombre;
    $this->estadoPdm = ($registro->estado_pdm != null) ? (int) $registro->estado_pdm: 0;
    $rubroPadre = null;
    if ($registro->id_rubro_padre && $registro->id_rubro_padre != $registro->id_rubro) {
      $rubroPadre = new RubroBo($registro->id_rubro_padre);
    }
    $this->rubroPadre = $rubroPadre;
    $rubroImagenBo = new RubroImagenBo();
    $this->imagenes = $rubroImagenBo->buscarImagenesPorIdRubro($registro->id_rubro);
    return $this;
  }

  public function listarPorIdRubroSuip() {
    $listadoClaveIdSuip = array();
    $listadoRubro = $this->listar();
    foreach ($listadoRubro as $rubroBo) {
      $idSuip = $rubroBo->idRubroSuip;
      if (isset($idSuip)) {
        $listadoClaveIdSuip[$idSuip] = $rubroBo;
      }
    }
    return $listadoClaveIdSuip;
  }


  public function buscarPorIdSuip($idSuip) {
    $registros = $this->repo->filtrarPorIdRubroSuip($idSuip);
    $rubro = null;
    if (isset($registros[0])) {
      $rubro = new RubroBo();
      $rubro->construirDesdeRegistro($registros[0]);
    }
    return $rubro;
  }

  public static function buscarRubroPorIdSuip($idSuip) {
    $registros = \Fgo\Dao\RubroDao::getInstance()->filtrarPorIdRubroSuip($idSuip);
    $rubro = null;
    if (isset($registros[0])) {
      $rubro = new RubroBo();
      $rubro->construirDesdeRegistro($registros[0]);
    }
    return $rubro;
  }

  public function guardar() {
    $id = parent::guardar();
    if($id) {
      if(!empty($this->imagenes)) {
        $this->imagenes->guardarRelaciones();
      }
    }
  }

  public function esPadre() {
    return ($this->rubroPadre == null);
  }

  public static function listarRubrosVisiblesEnPdm() {
    $registros = \Fgo\Dao\RubroDao::getInstance()->filtrarPorEstadoPdmNoEn(RubroBo::ESTADO_PDM_OCULTO);
    $listado = array();
    foreach ($registros as $registro) {
      $rubroBo = new RubroBo();
      $rubroBo->construirDesdeRegistro($registro);
      $listado[] = $rubroBo;
    }
    return $listado;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idRubro) && !is_numeric($this->idRubro)) {
      $errores['idRubro'] = 'Por favor revise el ID del rubro.';
    }
    if (!isset($this->nombre) && !strlen($this->nombre) < 1) {
      $errores['nombre'] = 'Por favor revise el nombre del rubro.';
    }
    if (!is_numeric($this->estadoPdm)) {
      $errores['codigoTipoImagen'] = 'Por favor revise el estado para el portal de marcas del rubro.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}
