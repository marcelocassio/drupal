<?php

namespace Fgo\Bo;

class FidBo extends GeneralBo {
  public $fid;
  public $uri;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\FidDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->fid = $registro->fid;
    $this->uri = $registro->uri;
    return $this;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->fid) && !is_numeric($this->fid)) {
      $errores['fid'] = 'Por favor revise el ID principal.';
    }
    if (empty($this->uri) || !is_numeric($this->uri)) {
      $errores['uri'] = 'Por favor revise el ID de la comunicación.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

  public function guardar() {
    // Esta clase no deberia guardar
    return false;
  }
}