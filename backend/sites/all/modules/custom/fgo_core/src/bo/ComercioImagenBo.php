<?php

namespace Fgo\Bo;

class ComercioImagenBo extends GeneralBo {

  public $imagenes;
  public $idComercioImagen;
  public $idComercio;
  public $idImagen;

  protected function construirDesdeRegistro($registro) {
    $this->imagenes[] = new ImagenBo($registro->id_imagen);
    return $this;
  }

  public function construirDesdeRegistroImg($registro)  {
    $this->idComercioImagen = $registro->id_comercio_imagen;
    $this->idComercio = $registro->id_comercio;
    $this->idImagen = $registro->id_imagen;
    return $this;
  }

  public function construir($registro)  {
    $this->idComercio = $registro->id_comercio;
    $this->idImagen = $registro->id_imagen;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ComercioImagenDao::getInstance();
  }

  public function listarImagenesPorIdComercio($idComercio) {
    $listadoObjetosNegocio = array();

    $registros = $this->repo->filtrarPorIdComercio($idComercio);
    foreach ($registros as $registro) {
      $listadoObjetosNegocio[] = $this->construirDesdeRegistro($registro);
    }

    return $this;
  }

  public function buscarImagenesPorIdComercio($idComercio) {
    $imagenes = array();

    $registros = $this->repo->filtrarPorIdComercio($idComercio);
    foreach ($registros as $registro) {
      $imagenes[] = new ImagenBo($registro->id_imagen);
    }

    return $imagenes;
  }

  public function listarComercioComercioImagen($idComercio) {
    $comercioImagen = $this->repo->listarComercioComercioImagen($idComercio);
    $this->construirDesdeRegistroImg($comercioImagen);
    return $this;
  }

  public function listarComercioImagenIds() {
    $listado = $this->repo->listarComercioImagenIds();
    return $listado;
  }

  public static function eliminarPorIdComercio($idComercio) {
    if ($idComercio == null || $idComercio == "") {
      throw new \Exception("id de comercio invalido para eliminar relacion");
    }
    return \Fgo\Dao\ComercioImagenDao::getInstance()->eliminarPorIdComercio($idComercio);
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idComercioImagen) && !is_numeric($this->idComercioImagen)) {
      $errores['idComercioImagen'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComercio) || !is_numeric($this->idComercio)) {
      $errores['idComercio'] = 'Por favor revise el ID de Comercio.';
    }
    if (empty($this->idImagen) || !is_numeric($this->idImagen)) {
      $errores['idImagen'] = 'Por favor revise el ID de la imagen.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}