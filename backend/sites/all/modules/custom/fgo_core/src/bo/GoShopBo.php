<?php

namespace Fgo\Bo;

class GoShopBo extends GeneralBo {

  public $id_goshop;
  public $id_pedido;
  public $dni_cliente;
  public $nombre;
  public $apellido;
  public $email;
  public $referencia_pedido;
  public $estado_pedido;
  public $cuotas;
  public $items;
  public $total_financing;
  public $importe_envio;
  public $importe_productos;
  public $importe_total;
  public $metodo_entrega;
  public $tiempo_entrega;
  public $seller;
  public $seller_email;
  public $seller_phone;
  public $seller_address;
  public $seller_horario;
  public $seller_image;
  public $medio_pago;
  public $direccion_entrega;
  public $direccion_facturacion;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\GoShopDao::getInstance();
  }

  public function construirDesdeRegistro($reg) {

    $items = array();
    foreach ($reg['items'] as $item) {

      $goShopItemBo = new \Fgo\Bo\GoShopItemBo();
      $goShopItemBo->construirDesdeRegistro($item);
      $goShopItemBo->guardar();
      
      $items[] = $goShopItemBo->idGoshopItem;
    }

    $items = json_encode($items);

    $this->id_goshop = null;
    $this->id_pedido = $reg['id_pedido'];
    $this->dni_cliente = $reg['dni_cliente'];
    $this->nombre = $reg['nombre'];
    $this->apellido = $reg['apellido'];
    $this->email = $reg['email'];
    $this->referencia_pedido = $reg['referencia_pedido'];
    $this->estado_pedido = $reg['estado_pedido'];
    $this->cuotas = $reg['cuotas'];
    $this->items = $items;
    $this->total_financing = $reg['total_financing'];
    $this->importe_envio = $reg['total_shipping'];
    $this->importe_productos = $reg['total_products'];
    $this->importe_total = $reg['importe_total'];
    $this->metodo_entrega = $reg['metodo_entrega'];
    $this->tiempo_entrega = $reg['tiempo_entrega'];
    $this->seller = $reg['seller'];
    $this->seller_email = $reg['seller_email'];
    $this->seller_phone = $reg['seller_phone'];
    $this->seller_address = $reg['seller_address'];
    $this->seller_horario = $reg['seller_horario'];
    $this->seller_image = $reg['seller_image'];
    $this->medio_pago = $reg['medio_pago'];
    $this->direccion_entrega = $reg['direccion_entrega'];
    $this->direccion_facturacion = $reg['direccion_facturacion'];
    $this->total_products = $reg['total_products'];
    $this->total_shipping = $reg['total_shipping'];  

    return $this;
  }

  public function buscarPorIdPedido($idPedido){
    return $this->repo->buscarPorIdPedido($idPedido);
  }
}