<?php

namespace Fgo\Bo;

class LocalidadBo extends GeneralBo {
  public $idLocalidad;
  public $idLocalidadSuip;
  public $nombre;
  public $codigoPostal;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\LocalidadDao::getInstance();
  }

  public function construirDesdeRegistro($registro){
    $this->nombre = $registro->nombre;
    $this->idLocalidad = $registro->id_localidad;
    $this->idLocalidadSuip = $registro->id_localidad_suip;
    $this->codigoPostal = $registro->codigo_postal;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idLocalidadSuip = $dto->id_localidad_suip;
    $this->nombre = $dto->nombre;
    $this->codigoPostal = $dto->codigo_postal;
  }

  public function listarPorIdLocalidadSuip() {
    
    $listadoClaveIdSuip = array();
    $listadoLocalidad = $this->listar();
    
    foreach ($listadoLocalidad as $registro) {
      $idSuip = $registro->idLocalidadSuip;
      if (isset($idSuip)) {
        $listadoClaveIdSuip[$idSuip] = $registro;
      }
    }
    
    return $listadoClaveIdSuip;
  }

  public function buscarPorIdSuip($idSuip) {
    $registro = $this->repo->buscarPorIdSuip($idSuip);
    if ($registro) {
      $object = $this->construirDesdeRegistro($registro);
    }
    return $object;
  }

  /***
   * Busca una localidad por su id de suip
   * @param $idSuip
   * @return LocalidadBo|null
   */
  public static function buscarLocalidadPorIdSuip($idSuip) {
    $registro = \Fgo\Dao\LocalidadDao::getInstance()->buscarPorIdSuip($idSuip);
    $localidad = null;
    if ($registro) {
      $localidad = new LocalidadBo();
      $localidad->construirDesdeRegistro($registro);
    }
    return $localidad;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idLocalidad) && !is_numeric($this->idLocalidad)) {
      $errores['idLocalidad'] = 'Por favor revise el ID de la entidad.';
    }
    if (isset($this->idLocalidadSuip->fid) && !is_numeric($this->idLocalidadSuip)) {
      $errores['idLocalidadSuip'] = 'Por favor revise el ID Localidad de SUIP.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 90) {
      $errores['nombre'] = 'Por favor revise el nombre de la Localidad.';
    }
    if ((!empty($this->codigoPostal) && !is_numeric($this->codigoPostal)) || (!empty($this->codigoPostal) && strlen($this->codigoPostal) > 4)) {
      $errores['codigoPostal'] = 'Por favor revise el código postal.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}