<?php
namespace Fgo\Bo;

class ComunicacionSucursalFisicaBo extends GeneralBo {
  public $idComunicacionSucursalFisica;
  public $idComunicacion;
  public $idSucursalFisica;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ComunicacionSucursalFisicaDao::getInstance();
  }

  protected function construirDesdeRegistro($registro){
    $this->idComunicacionSucursalFisica = $registro->id_comunicacion_sucursal_fisica;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idSucursalFisica = $registro->id_sucursal_fisica;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idComunicacion = $dto->id_comunicacion;
    $this->idSucursalFisica = $dto->id_sucursal_fisica;
  }

  public function construir($sucursal, $comunicacion) {
    $this->idComunicacion = $comunicacion->idComunicacion;
    $this->idSucursalFisica = $sucursal->idSucursalFisica;
  }

  public function listarPorIdCombinado() {
    $listado = $this->listar();
    $matrizCombinada = array();
    foreach ($listado as $combinacionBo) {
      $matrizCombinada[$combinacionBo->idComunicacion][$combinacionBo->idSucursalFisica] = $combinacionBo;
    }
    return $matrizCombinada;
  }

  public function listarSucursalesPorIdComunicacion($idComunicacion) {
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    $listadoObjetosNegocio = array();

    foreach ($registros as $registro) {
      $listadoObjetosNegocio[] = new SucursalFisicaBo($registro->id_sucursal_fisica);
    }

    return $listadoObjetosNegocio;
  }

  /***
   * Elimina la relacion encontrada que coincida con el idComunicacion y idSucursalFisica
   * @param $idComunicacion
   * @param $idSucursalFisica
   */
  public static function eliminarPorIdComunicacionIdSucursal($idComunicacion, $idSucursalFisica) {
    $registros = \Fgo\Dao\ComunicacionSucursalFisicaDao::getInstance()->filtrarPorIdComunicacionIdSucursal($idComunicacion, $idSucursalFisica);
    if (isset($registros[0])) {
      $comunicacionSucursalBo = new ComunicacionSucursalFisicaBo();
      $comunicacionSucursalBo->construirDesdeRegistro($registros[0]);
      if ($comunicacionSucursalBo->idComunicacionSucursalFisica != null) {
        $comunicacionSucursalBo->eliminar();
      }
    }
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idComunicacionSucursalFisica) && !is_numeric($this->idComunicacionSucursalFisica)) {
      $errores['idComunicacionSucursalFisica'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComunicacion) || !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if (empty($this->idSucursalFisica) || !is_numeric($this->idSucursalFisica)) {
      $errores['idSucursalFisica'] = 'Por favor revise el ID de la sucursal física.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}