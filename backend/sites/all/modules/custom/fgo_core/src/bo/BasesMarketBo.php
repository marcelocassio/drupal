<?php

namespace Fgo\Bo;

use Fgo\Dao\BasesMarketDao;

class BasesMarketBo extends GeneralBo
{
    public $idBasesMarket;
    public $texto;

    protected function obtenerRepo()
    {
        $this->repo = BasesMarketDao::getInstance();
    }

    public function construirDesdeRegistro($registro)
    {
        $this->idBasesMarket = $registro->id_bases_market;
        $this->texto = $registro->texto;
        return $this;
    }

    public function obtenerRegistroActual()
    {
        return $this->construirDesdeRegistro($this->repo->obtenerRegistroActual());
    }
}