<?php

namespace Fgo\Bo;

class CtaBo extends GeneralBo {
  public $idCta;
  public $param1;
  public $param2;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\CtaDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idCta = $registro->id_cta;
    $this->param1 = $registro->param1;
    $this->param2 = $registro->param2;
    return $this;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idCta) && !is_numeric($this->idCta)) {
      $errores['idCta'] = 'Por favor revise el ID principal.';
    }
    if ($this->param1 == 0 || empty($this->param1) || !is_numeric($this->param1)) {
      $errores['callToActionSeccion'] = 'Por favor revise la sección del Call To Action.';
    }

    // Comunicacion este vigente durante la vigencia del slide
    if($this->param1 == 1) {
      $comunicacion = new ComunicacionBo($this->param2);
      if(empty($comunicacion->idComunicacion)) {
        $errores['callToActionDireccion_1'] = "No se encontró la comunicación.";
      }
    }

    // Campaña este vigente durante la vigencia del slide
    if($this->param1 == 2) {
      $campania = CampaniaBo::buscarPorIdSuip($this->param2);
      if(empty($campania->idCampania)) {
        $errores['callToActionDireccion_2'] = "No se encontró la campaña.";
      }
    }

    // Sorteo
    if($this->param1 == 3) {
      $sorteo_id = _bbva_sorteo_existe_sorteo($this->param2);
      if(!$sorteo_id) {
        $errores['callToActionDireccion_3'] = "No se encontró el sorteo.";
      }
    }

    // Link Interno y Externo
    if($this->param1 == 4 || $this->param1 == 7) {
      if (!empty($this->param2)){
        $url = parse_url($this->param2);
        if(isset($url['scheme']) && $url['scheme'] != 'https'){
          $errores['callToActionDireccion_4'] = "La URL no tiene https.";
        }
      }else{
        $errores['callToActionDireccion_4'] = "Cargar URL.";
      }
    }

    // Secciones Generales
    if($this->param1 == 5) {
      if (empty($this->param2)){
        $errores['callToActionDireccion_5'] = "No se encuentra la sección.";
      }
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}