<?php

namespace Fgo\Bo;

class CtaRelacionBo extends GeneralBo {
  /*
   * TIPO_RELACION_CTA_PUSH = 1;
   * TIPO_RELACION_CTA_PUSH_ON_DEMAND = 2;
   */

  public $idCtaRelacion;
  public $cta;
  public $idEntidad;
  public $tipoEntidad;

  protected function construirDesdeRegistro($registro) {
    $this->idCtaRelacion = $registro->id_cta_relacion;
    $this->cta = new CtaBo($registro->id_cta);
    $this->idEntidad = $registro->id_entidad;
    $this->tipoEntidad = $registro->tipo_entidad;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\CtaRelacionDao::getInstance();
  }

  public function buscarPorIdEntidad($idEntidad, $tipoEntidad) {
    $relacionCtaBo = null;

    $registros = $this->repo->filtrarPorIdEntidad($idEntidad, $tipoEntidad);
    if (isset($registros[0])) {
      $relacionCtaBo = new CtaRelacionBo();
      $relacionCtaBo->construirDesdeRegistro($registros[0]);
    }
    return $relacionCtaBo;
  }
  
  protected function _validar() {
    $errores = array();
    if (!empty($this->idCtaRelacion) && !is_numeric($this->idCtaRelacion)) {
      $errores['idCtaRelacion'] = 'Por favor revise el ID primario de la entidad.';
    }
    // CTA
    if($this->cta->idCta == 0) {
      $errores['callToActionSeccion'] = "Por favor revise la sección del link.";
    }
    if (empty($this->idEntidad) || strlen($this->idEntidad) > 11 || !is_numeric($this->idEntidad)) {
      $errores['idEntidad'] = 'Por favor revise el ID de la entidad asociada.';
    }
    if (empty($this->tipoEntidad) || strlen($this->tipoEntidad) > 11 || !is_numeric($this->tipoEntidad)) {
      $errores['idEntidad'] = 'Por favor revise el tipo de entidad asociada.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}