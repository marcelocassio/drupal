<?php

namespace Fgo\Bo;

use \Fgo\Dao\AccesoDirectoDao;
use \Fgo\Dao\SegmentacionDao;

class AccesoDirectoBo extends GeneralBo {
  public $idAccesoDirecto;
  public $tipo;
  public $nombre;
  public $cta;
  public $orden;
  public $visible;

  protected function obtenerRepo() {
    $this->repo = AccesoDirectoDao::getInstance();
  }

  /**
   * Modifica el estado del objeto con los valores del registro de la tabla
   * @param $registro
   * @return $this
   */
  public function construirDesdeRegistro($registro) {
    $this->idAccesoDirecto = $registro->id_acceso_directo;
    $this->tipo = $registro->tipo;
    $this->nombre = $registro->nombre;
    $this->cta = new CtaBo($registro->id_cta);
    $this->orden = $registro->orden;
    $this->visible = $registro->visible;

    return $this;
  }

  static public function obtenerAccesosDirectos() {
    $listado = array();
    $registros = AccesoDirectoDao::getInstance()->obtenerAccesosDirectos();

    // $es_latam = null;
    // $es_cliente = null;
    // $es_nfc = null;
    
    // $id_altamira = false;

    // $id_altamira = 4589633; // es cliente, no master, no nfc, no latam
    // $id_altamira = 3348743; // es cliente, es master, no nfc, es latam

    // if(isLoggedUser()){
    //   // hd('segmentacion');
    //   $segmentacion = SegmentacionDao::getInstance()->obtenerSegmentacionUsuario($id_altamira);
    //   if($segmentacion){
    //     $segmentacion = $segmentacion[0];

    //     $es_master = SegmentacionDao::getInstance()->esMaster($segmentacion);
    //     $es_latam = SegmentacionDao::getInstance()->esLatam($segmentacion);
    //     $es_cliente = SegmentacionDao::getInstance()->esCliente($segmentacion);
    //     $es_nfc = isAndroid() && $es_master;

    //     hd($es_master);
    //     hd($es_latam);
    //     hd($es_cliente);
    //     hd($es_nfc);
    //   }
    // }

    // hd('obtenerAccesosDirectos');

    foreach ($registros as $registro) {
      $accesoDirecto = new AccesoDirectoBo($registro->id_acceso_directo);

      /*
        se comenta segmentacion en QA

      if($accesoDirecto->tipo == "latam_pass" && !$es_latam){
        continue;
      }

      if($accesoDirecto->tipo == "pagos_nfc" && !$es_nfc){
        continue;
      }

      if($accesoDirecto->tipo == "seas_o_no_cliente"){
        $accesoDirecto->nombre = $es_cliente ? "Sos Cliente" : "No Sos Cliente";
      }

      */
      
      $listado[] = $accesoDirecto;
    }

    return $listado;
  }
}