<?php

namespace Fgo\Bo;

class GoShopUsuarioBo extends GeneralBo {

  public $idGoshopusuario;
  public $user;
  public $pass;
  public $token;
  public $created;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\GoShopUsuarioDao::getInstance();
  }

  public function construirDesdeRegistro($reg) {

    $this->idGoshopusuario = null;
    if($reg->id_goshopusuario){
      $this->idGoshopusuario = $reg->id_goshopusuario;
    }
    $this->user = $reg->user;
    $this->pass = $reg->pass;
    $this->token = $reg->token;
    $this->modified = $reg->modified;
    return $this;
  }

  public function buscarPorUserPass($user, $password){
    $registro =  $this->repo->buscarPorUserPass($user, $password);
    if(isset($registro[0])){
      $objeto = new $this;
      return $objeto->construirDesdeRegistro($registro[0]);  
    }
    return false;
  }

  public function buscarPorToken($tkn, $date){
    // deberia actualizar el token ?
    $registro = $this->repo->buscarPorToken($tkn, $date);
    if(isset($registro[0])){
      $objeto = new $this;
      return $objeto->construirDesdeRegistro($registro[0]);  
    }
    return false;
  }

}