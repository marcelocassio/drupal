<?php

namespace Fgo\Bo;

use \Fgo\Dao\ComunicacionDao;
use \Fgo\Bo\BeneficioBo;


class ComunicacionBo extends GeneralBo {
  public $idComunicacion;
  public $titulo;
  public $comercio;
  public $beneficios;
  public $idBeneficioSuip;
  public $descripcionCorta;
  public $descripcionExtendida;
  public $destacado;
  public $accionComercial;
  public $fechaDesde;
  public $fechaHasta;
  public $terminosCondiciones;
  public $cft;
  public $imagenes;
  public $publicado;
  public $peso;
  public $rubros;
  public $zonasAdheridas;
  public $tags;
  public $sucursalesFisicas;
  public $sucursalesVirtuales;
  public $prioridad;
  public $nombreCampania;
  public $siempreVisible;
  public $comoUso;

  const TIPO_BENEFICIO_ESPECTACULO = 23;
  const TIPO_BENEFICIO_ESPECTACULO2 = 110;

  protected function obtenerRepo() {
    $this->repo = ComunicacionDao::getInstance();
  }

  /**
   * Modifica el estado actual del objeto con los valores del dto.
   * @param $dto DTO de interface con archivo.
   */
  public function construirDesdeLineaArchivo($dto, $nuevo = true ) {
    
    $this->idBeneficioSuip = $dto->id_beneficio_suip;
    if($nuevo){
        $this->publicado = 1;
        if ($dto->id_tipo_beneficio_suip == BeneficioBo::TIPO_BENEFICIO_CUPON_SUIP){
            $this->publicado = 0;
        }
        
        $this->siempreVisible = 0;
        $this->comoUso = null;
        
        if (isset($dto->id_comercio_suip)) {
            $comercio = new ComercioBo();
            $this->comercio = $comercio->buscarPorIdComercioSuip($dto->id_comercio_suip);
            $this->titulo = $this->comercio->nombre;
        } else {
            $this->titulo = $dto->titulo;
        }
        if ($dto->id_tipo_beneficio_suip == self::TIPO_BENEFICIO_ESPECTACULO ||
            $dto->id_tipo_beneficio_suip == self::TIPO_BENEFICIO_ESPECTACULO2) {
            $this->titulo = $dto->denominacion_promocion;
        }
    }
    
    $this->fechaDesde = $dto->fecha_desde;
    $this->fechaHasta = $dto->fecha_hasta;
    $this->terminosCondiciones = "Promoción Valida desde " . date('d/m/Y', $dto->fecha_desde) . " hasta " . date('d/m/Y', $dto->fecha_hasta)." ";
    $this->terminosCondiciones .= $dto->legales . $dto->descripcion_cft;
    $this->cft = (float)$dto->cft;
    $this->descripcionExtendida = $dto->descripcion_extendida;
    $this->descripcionCorta = $dto->descripcion_corta;
    
    $this->zonasAdheridas = $dto->zonas_adheridas;
    $this->prioridad = $dto->id_prioridad;
    $this->destacado = $dto->destacado;
    
    
  }

  /**
   * Modifica el estado del objeto con los valores del registro de la tabla
   * @param $registro
   * @return $this
   */
  public function construirDesdeRegistro($registro) {
    $this->construirDatosBasico($registro);
    $this->construirSimilaresDesdeRegistro($registro);
    $this->construirTagsDesdeRegistro($registro);
    //$this->construirSucursalesDesdeRegistro($registro);
    $this->recuperarSucursales();
    $comunicacionRubroBo = new ComunicacionRubroBo();
    $this->rubros = $comunicacionRubroBo->buscarPorIdComunicacion($registro->id_comunicacion)->rubros;
    return $this;
  }

  /**
   * Modifica el estado del objeto con los valores del registro de la tabla con datos basicos
   * @param $registro
   * @return $this
   */
  public function construirDesdeRegistroBackend($registro) {
    $this->construirDatosBasico($registro);
    $campaniaComunicacionBo = new CampaniaComunicacionBo();
    $campaniaBo = $campaniaComunicacionBo->buscarCampaniaPorIdComunicacion($registro->id_comunicacion);
    $this->nombreCampania = null;
    if ($campaniaBo != null) {
      $this->nombreCampania = $campaniaBo->nombre;
    }
    return $this;
  }

  /**
   * Modifica el estado actual del objeto con los datos del registro
   * @param $registro
   */
  private function construirDatosBasico($registro) {
    $this->idComunicacion = $registro->id_comunicacion;
    $this->titulo = $registro->titulo;
    $this->idBeneficioSuip = $registro->id_beneficio_suip;
    $comunicacionImagenBo = new ComunicacionImagenBo($registro->id_comunicacion);
    $this->imagenes = $comunicacionImagenBo->buscarImagenesPorIdComunicacion($registro->id_comunicacion);
    $this->descripcionCorta = $registro->descripcion_corta;
    $this->fechaDesde = $registro->fecha_desde;
    $this->fechaHasta = $registro->fecha_hasta;
    $this->terminosCondiciones = $registro->terminos_condiciones;
    $this->cft = (float)$registro->cft;
    $this->publicado = $registro->publicado;
    $this->peso = $registro->peso;
    $this->descripcionExtendida = $registro->descripcion_extendida;
    $this->zonasAdheridas = $registro->zonas_adheridas;
    $this->prioridad = $registro->prioridad;
    $this->destacado = $registro->destacado;

    $beneficioBo = new BeneficioBo();
    $this->beneficios = $beneficioBo->buscarPorIdComunicacion($registro->id_comunicacion);
    if ($registro->id_accion_comercial != null) {
      $this->accionComercial = new AccionComercialBo($registro->id_accion_comercial);
    }
    if ($registro->id_comercio != null) {
      $this->comercio = new ComercioBo($registro->id_comercio);
    }
    $this->siempreVisible = $registro->siempre_visible;
    $this->comoUso = $registro->como_uso;
  }

  private function construirTagsDesdeRegistro($registro) {
    $comunicacionTagBo = new ComunicacionTagBo();
    $this->tags = $comunicacionTagBo->listarTagPorIdComunicacion($registro->id_comunicacion);
  }

  private function construirSimilaresDesdeRegistro($registro) {
    $comunicacionSimilaresBo = new ComunicacionSimilaresBo();
    $comunicacionesSimilares = $comunicacionSimilaresBo->listarSimilaresPorIdComunicacion($registro->id_comunicacion);
    $this->comunicacionesSimilares = $this->construirBeneficiosSimilares($comunicacionesSimilares);
  }

  /***
   * @deprecated
   * @param $registro
   */
  private function construirSucursalesDesdeRegistro($registro) {
    $comunicacionSucursalFisicaBo = new ComunicacionSucursalFisicaBo();
    $comunicacionSucursalVirtualBo = new ComunicacionSucursalVirtualBo();
    $this->sucursalesFisicas = $comunicacionSucursalFisicaBo->listarSucursalesPorIdComunicacion($registro->id_comunicacion);
    $this->sucursalesVirtuales = $comunicacionSucursalVirtualBo->listarSucursalesPorIdComunicacion($registro->id_comunicacion);
  }

  /***
   * Recupera las sucursales de fisicas y virtuales asociadas a la comunicacion;
   */
  private function recuperarSucursales() {
    $comunicacionSucursalFisicaBo = new ComunicacionSucursalFisicaBo();
    $comunicacionSucursalVirtualBo = new ComunicacionSucursalVirtualBo();
    $this->sucursalesFisicas = $comunicacionSucursalFisicaBo->listarSucursalesPorIdComunicacion($this->idComunicacion);
    $this->sucursalesVirtuales = $comunicacionSucursalVirtualBo->listarSucursalesPorIdComunicacion($this->idComunicacion);
  }

  public function construirDesdeRegistroSimilar($registro) {
    $this->idComunicacion = $registro->id_comunicacion;
    $this->titulo = $registro->titulo;
    $comunicacionImagenBo = new ComunicacionImagenBo($registro->id_comunicacion);
    $this->imagenes = $comunicacionImagenBo->buscarImagenesPorIdComunicacion($registro->id_comunicacion);

    $beneficioBo = new BeneficioBo();
    $this->beneficios = $beneficioBo->buscarPorIdComunicacion($registro->id_comunicacion);

    if ($registro->id_comercio != null) {
      $this->comercio = new ComercioBo($registro->id_comercio);
    }

    return $this;
  }
  
  /**
   * Devuelve una lista de beneficios similares para la comunicacion;
   **/
  private function construirBeneficiosSimilares($ids_similares) {
    $comunicaciones = array();
    if (!empty($ids_similares)){
      foreach ($ids_similares as $id_similar){
        $objeto = new $this;
        $comunicacion = $this->repo->buscarPorId($id_similar->id_similar);
        if ($comunicacion) {
          $comunicaciones[] = $objeto->construirDesdeRegistroSimilar($comunicacion);
        }
        unset($comunicacion);
        unset($objeto);
      }
    }
    return $comunicaciones;
  }

  /**
   * @deprecated
   * @param $idSuip
   * @param null $basico
   * @return $this
   */
  public function buscarPorIdSuip($idSuip, $basico = null) {
    $registro = $this->repo->buscarPorIdSuip($idSuip);
    if ($registro) {
      if ($basico){
        $this->construirDesdeRegistroBackend($registro);
      }else{
        $this->construirDesdeRegistro($registro);
      }
    }
    return $this;
  }

  public static function obtenerIdComunicacionPorIdSuip($idSuip){
    $registro = ComunicacionDao::getInstance()->buscarPorIdSuip($idSuip);
    return isset($registro) ? $registro->id_comunicacion : null;
  }

  public static function buscarComunicacionPorIdSuip($idSuip, $basico = null) {
    $comunicacion = null;
    $registro = ComunicacionDao::getInstance()->buscarPorIdSuip($idSuip);
    if ($registro != null) {
      $comunicacion = new ComunicacionBo();
      if ($basico){
        $comunicacion->construirDesdeRegistroBackend($registro);
      }
      else{
        $comunicacion->construirDesdeRegistro($registro);
      }
    }
    return $comunicacion;
  }

  public static function buscarComunicacionImagenes($idSuip, $rubroImagenComercio){
   
    $registro = ComunicacionDao::getInstance()->buscarPorIdSuip($idSuip);

    if(isset($registro)){
      if(isset($registro->id_comercio) && isset($registro->id_comunicacion)){
        $comunicacionRubroBo = new ComunicacionRubroBo();
        $comunicacionImagenBo = new ComunicacionImagenBo();
        $rubroImagenComercio->rubros = $comunicacionRubroBo->buscarPorIdComunicacion($registro->id_comunicacion)->rubros;
        $rubroImagenComercio->imagenes = $comunicacionImagenBo->buscarImagenesPorIdComunicacion($registro->id_comunicacion);
        $rubroImagenComercio->comercio = new ComercioBo($registro->id_comercio);
        $rubroImagenComercio->idComunicacion = $registro->id_comunicacion;
      }
    }

    return $rubroImagenComercio;
  }



  public function listarBasico() {
    $listadoObjetosNegocio = array();
    $registros = $this->repo->listar();

    foreach ($registros as $registro) {
      $objeto = new ComunicacionBo();
      $listadoObjetosNegocio[] = $objeto->construirDesdeRegistroBackend($registro);
    }
    return $listadoObjetosNegocio;
  }

  public function listarPorIdBeneficioSuip() {
    $comunicaciones = $this->listarBasico();

    $listado = array();
    foreach ($comunicaciones as $comunicacionBo) {
      if (isset($comunicacionBo->idBeneficioSuip)) {
        $listado[$comunicacionBo->idBeneficioSuip] = $comunicacionBo;
      }
    }
    return $listado;
  }

  public function tieneMismoBeneficio($beneficioBo) {
    $yalotengo = false;
    foreach ($this->beneficios as $miBeneficio) {
      if ($miBeneficio->esIgual($beneficioBo)) {
        $yalotengo = true;
      }
    }
    return $yalotengo;
  }

  public function obtenerMismoBeneficio($beneficioBo) {
    $beneficioEncontrado = false;
    foreach ($this->beneficios as $miBeneficio) {
      if ($miBeneficio->esIgual($beneficioBo)) {
        $beneficioEncontrado = $miBeneficio;
      }
    }
    return $beneficioEncontrado;
  }

  public function esIgual($comunicacionBo) {
    return $comunicacionBo && isset($comunicacionBo->idComunicacion) && $this->idComunicacion == $comunicacionBo->idComunicacion;
  }

  public function monitoreVigentes(){
    $fechaDesde = self::calcularFechaDesdeVigencia();
    $fechaHasta = date('Y-m-d');

    return $this->repo->filtrarVigentesOrdenados($fechaDesde, $fechaHasta);
  }


  public function buscarVigentes($fecha = NULL) {
    $listado = array();

    $fechaDesde = !$fecha ? self::calcularFechaDesdeVigencia() : $fecha;
    $fechaHasta = date('Y-m-d');
    $registros = $this->repo->filtrarVigentesOrdenados($fechaDesde, $fechaHasta);
    foreach ($registros as $registro) {
      $comunicacion = new ComunicacionBo();
      $comunicacion->construirDesdeRegistro($registro);
      $listado[] = $comunicacion;
    }
    return $listado;
  }

  static public function obtenerIdSuipDeComunicacionesNoVencidas() {
    $result = array();
    $registros = ComunicacionDao::getInstance()->obtenerIdSuipDeComunicacionesNoVencidas();
    foreach ($registros as $value) {
      $result[] = $value->id_beneficio_suip;
    }
    return $result;
  }

  public static function buscarDisponiblesDesdeHoy() {
    $fechaHoy = new \DateTime();
    $registros = ComunicacionDao::getInstance()->filtrarFechasHastaMayorQue($fechaHoy->format("Y-m-d"));
    foreach ($registros as $registro) {
      $comunicacion = new ComunicacionBo();
      $comunicacion->construirDesdeRegistroBackend($registro);
      $listado[] = $comunicacion;
    }
    return $listado;
  }

  public static function buscarVigentesBackend($fechaHastaInicial) {
    $fechaHastaFinal = new \DateTime();
    $fechaHastaFinal->modify('+1 year');
    $registros = ComunicacionDao::getInstance()->filtrarFechasHasta($fechaHastaInicial, $fechaHastaFinal->format("Y-m-d"));
    foreach ($registros as $registro) {
      $comunicacion = new ComunicacionBo();
      $comunicacion->construirDesdeRegistroBackend($registro);
      $listado[] = $comunicacion;
    }
    return $listado;
  }

  public static function buscarHistoricoBackend($fechaDesdeInicial) {
    $fechaDesdeFinal = date('Y-m-d');
    $registros = ComunicacionDao::getInstance()->filtrarFechasDesde($fechaDesdeInicial, $fechaDesdeFinal);
    foreach ($registros as $registro) {
      $comunicacion = new ComunicacionBo();
      $comunicacion->construirDesdeRegistroBackend($registro);
      $listado[] = $comunicacion;
    }
    return $listado;
  }


  public function esVisible($fecha = false) {
    if(!$this->publicado) {
      return false;
    }
    if($this->siempreVisible) {
      return true;
    }
    if(!$fecha) {
      $fecha = strtotime("today");
    }
    if(date('N',time()) == 6 ) {
      $fechaDesde = $this->fechaDesde - 172800; // Se ve dos dias antes
    } else {
      $fechaDesde = $this->fechaDesde - 86400; // Se ve desde un dia antes
    }
    $fechaHasta = $this->fechaHasta;

    if($fecha >= $fechaDesde && $fecha <= $fechaHasta) {
      return true;
    }
    return false;
  }

  /**
   * Calcula fecha desde para la visualizacion de comunicaciones en base a la fecha desde
   * @return false|string
   */
  private static function calcularFechaDesdeVigencia() {
    if (date('N', time()) == 6) {
      $fechaDesde = date('Y-m-d', time() + 172800);
    } else {
      $fechaDesde = date('Y-m-d', time() + 86400); // Dia = 24 * 60 * 60 -> 24 horas; 60 minutos; 60 segundos
    }
    return $fechaDesde;
  }


  /**
   * Se busca la ultima comunicación cuyo comercio correponda con el id de comercio.
   * @param $idComercio Id de comercio
   * @return ComunicacionBo|null
   */
  public function buscarUltimaComunicacionPorComercio($idComercio) {
    $registros = $this->repo->filtrarUltimaComunicacionPorIdComercio($idComercio);
    $comunicacion = NULL;

    if (count($registros) == 1) {
      $comunicacion = new ComunicacionBo();
      $comunicacion->construirDesdeRegistro($registros[0]);
    }

    return $comunicacion;
  }

  public static function buscarComunicacionListado($idComunicacion) {
    $registro = ComunicacionDao::getInstance()->buscarPorId($idComunicacion);
    if ($registro != null) {
      $object = new ComunicacionBo();
      $object->construirDesdeRegistroBackend($registro);
    }
    return $object;
  }

  /***
   * Lista los id de comunicaciones vigentes con sus id de rubros.
   */
  public static function listarComunicacionRubroOrden() {
    $fechaDesde = self::calcularFechaDesdeVigencia();
    $fechaHasta = date('Y-m-d');

    $listadoComunicacionesRubros = \Fgo\Bo\ComunicacionRubroBo::listarComunicacionRubroOrden($fechaDesde, $fechaHasta);

    return $listadoComunicacionesRubros;
  }

  public function obtenerEtiquetas() {
    $nombreTags = array();
    $nombreTags[] = $this->titulo;

    if (isset($this->rubros) && count($this->rubros) > 0) {
      foreach ($this->rubros as $rubro) {
        $nombreTags[] = $rubro->nombre;
      }
    }

    if (isset($this->beneficios) && count($this->beneficios) > 0) {
      foreach ($this->beneficios as $beneficio) {
        if ($beneficio->tipoBeneficio === \Fgo\Bo\BeneficioBo::TIPO_BENEFICIO_CUPON) {
          $nombreTags[] = "Cupones";
        }
      }
    }
    return $nombreTags;
  }

  public function getImagenPortal() {
    $imagenEncontrada = null;
    if ($this->imagenes) {
      foreach ($this->imagenes as $imagenBo) {
        if ($imagenBo->esImagenPortal()) {
          $imagenEncontrada = $imagenBo;
        }
      }
    }
    if (!$imagenEncontrada) {
      if ($this->comercio && $this->comercio->imagenes) {
        foreach ($this->comercio->imagenes as $imagenBo) {
          if ($imagenBo->esImagenPortal()) {
            $imagenEncontrada = $imagenBo;
          }
        }
      }
    }
    return $imagenEncontrada;
  }

  /***
   * Reemplaza las sucursales fisicas actuales de la comunicacion por las informadas en el parametro.
   * @param $listadoSucursalesFisicasBo
   */
  public function reemplazarRelacionConSucursales($listadoSucursalesFisicasBo) {
    $nuevasSucursales = 0;
    // Armo mapa para busqueda directa
    $this->recuperarSucursales();
    $mapaSucursalesFisicas = array();
    foreach ( $this->sucursalesFisicas as $sucursalFisicaBo) {
      $mapaSucursalesFisicas[$sucursalFisicaBo->idSucursalFisica] = $sucursalFisicaBo;
    }

    // Buscar sucursales actualizar o crear
    foreach ( $listadoSucursalesFisicasBo as $sucursalFisicaNueva) {
      if (!isset($mapaSucursalesFisicas[$sucursalFisicaNueva->idSucursalFisica])) {
        $comunicacionConSucursal = new \Fgo\Bo\ComunicacionSucursalFisicaBo();
        $comunicacionConSucursal->construir($sucursalFisicaNueva, $this);
        $comunicacionConSucursal->guardar();
        $nuevasSucursales++;
      }
    }

    if ($nuevasSucursales > 0) {
      $this->recuperarSucursales();
    }

    // Consultar sucursales que no estan en el listado y remover relacion.
    $mapaSucursalesNuevas = array();
    foreach ($listadoSucursalesFisicasBo as $sucursalFisicaNueva) {
      $mapaSucursalesNuevas[$sucursalFisicaNueva->idSucursalFisica] = $sucursalFisicaNueva;
    }
    foreach ($this->sucursalesFisicas as $sucursalesFisicaBo) {
      if (!isset($mapaSucursalesNuevas[$sucursalesFisicaBo->idSucursalFisica])) {
        ComunicacionSucursalFisicaBo::eliminarPorIdComunicacionIdSucursal($this->idComunicacion, $sucursalesFisicaBo->idSucursalFisica);
      }
    }

    $this->recuperarSucursales();

  }

  protected function _validar() {

    $errores = array();

    if (!empty($this->idComunicacion) && !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if (!empty($this->idBeneficioSuip) && !is_numeric($this->idBeneficioSuip)) {
      $errores['idBeneficioSuip'] = 'Por favor revise el ID de Beneficio SUIP.';
    }
    if (!empty($this->titulo) && strlen($this->titulo) > 100) {
      $errores['titulo'] = 'Por favor revise el titulo de la comunicación.';
    }
    if (!empty($this->descripcionCorta) && strlen($this->descripcionCorta) > 150) {
      $errores['descripcionCorta'] = 'Por favor revise la descripción corta de la comunicación.';
    }
    if (!empty($this->zonasAdheridas) && strlen($this->zonasAdheridas) > 150) {
      $errores['zonasAdheridas'] = 'Por favor revise las zonas adheridas a la comunicación.';
    }
    if (!empty($this->accionComercial) && !is_numeric($this->accionComercial->idAccionComercial)) {
      $errores['accionComercial'] = 'Por favor revise el ID de accion comercial.';
    }
    if (!empty($this->cft) && !is_float($this->cft)) {
      $errores['cft'] = 'Por favor revise el cft';
    }
    if (!empty($this->publicado) && !is_numeric($this->publicado)) {
      $errores['publicado'] = 'Por favor revise la marca publicado.';
    }
    if ((!empty($this->peso) && !is_numeric($this->peso)) || (!empty($this->peso) && strlen($this->peso) > 11)) {
      $errores['peso'] = 'Por favor revise el peso de la comunicacion.';
    }
    if (!empty($this->fechaDesde) && !is_numeric($this->fechaDesde)) {
      $errores['fechaDesde'] = 'Por favor revise la fecha desde.';
    }
    if (!empty($this->fechaHasta) && !is_numeric($this->fechaHasta)) {
      $errores['fechaHasta'] = 'Por favor revise la fecha hasta.';
    }
    if ((!empty($this->prioridad) && !is_numeric($this->prioridad)) || (!empty($this->prioridad) && strlen($this->prioridad) > 11)) {
      $errores['prioridad'] = 'Por favor revise la prioridad.';
    }
    if (!empty($this->destacado) && !is_numeric($this->destacado)) {
      $errores['destacado'] = 'Por favor revise la opción destacado.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

  public static function filtrarPorIdRubro( $idRubro, $limit = 5 ){
    $fechaDesde = self::calcularFechaDesdeVigencia();
    $fechaHasta = date('Y-m-d');
    return ComunicacionDao::getInstance()->filtrarPorIdRubro($idRubro, $limit, $fechaDesde, $fechaHasta);
  }

}