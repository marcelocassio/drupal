<?php
/**
 * Created by PhpStorm.
 * User: nestor
 * Date: 05/04/18
 * Time: 11:24
 */

namespace Fgo\Bo;


class BeneficioMedioPagoBo extends GeneralBo {

  public $idBeneficioMedioPago;
  public $idBeneficio;
  public $idMedioPago;

  protected function construirDesdeRegistro($registro) {
    $this->idBeneficioMedioPago = $registro->id_beneficio_medio_pago;
    $this->idBeneficio = $registro->id_beneficio;
    $this->idMedioPago = $registro->id_medio_pago;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\BeneficioMedioPagoDao::getInstance();
  }


  public function buscarMedioPagoPorIdBeneficio($idBeneficio) {

    $registros = $this->repo->filtrarPorIdBeneficio($idBeneficio);
    $listadoObjetosNegocio = array();
    $listadoMediosPago = array();
    foreach ($registros as $registro) {
      $objeto = new $this;
      $listadoObjetosNegocio[] = $objeto->construirDesdeRegistro($registro);
    }

    foreach ($listadoObjetosNegocio as $beneficioMedioPagoBo) {
      $listadoMediosPago[] = new MedioPagoBo($beneficioMedioPagoBo->idMedioPago);
    }

    return $listadoMediosPago;
  }

  protected function _validar() {
    $errores = false;

    if (!empty($this->idBeneficioMedioPago) && !is_numeric($this->idBeneficioMedioPago)) {
      $errores['idBeneficioMedioPago'] = "Por favor revise el ID de la relación.";
    }

    if (empty($this->idBeneficio) || !is_numeric($this->idBeneficio)) {
      $errores['idBeneficio'] = "Por favor revise el ID del Beneficio.";
    }

    if (empty($this->idMedioPago) || !is_numeric($this->idMedioPago)) {
      $errores['idMedioPago'] = "Por favor revise el ID del Medio Pago.";
    }

    if($errores) {
      throw new DatosInvalidosException($errores);
    }
    return true;
  }
}