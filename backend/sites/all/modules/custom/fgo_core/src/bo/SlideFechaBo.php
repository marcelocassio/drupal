<?php

namespace Fgo\Bo;

class SlideFechaBo extends GeneralBo {
  public $idSlideFecha;
  public $slide;
  public $orden;
  public $fecha;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\SlideFechaDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idSlideFecha = $registro->id_slide_fecha;
    $this->slide = new SlideBo($registro->id_slide);
    $this->orden = $registro->orden;
    $this->fecha = $registro->fecha;

    return $this;
  }

  public function construirRegistroDefault($slide) {
    $this->idSlideFecha = $slide['idSlideFecha'];
    $this->slide = $slide['slide'];
    $this->orden = $slide['orden'];
    $this->fecha = $slide['fecha'];

    return $this;
  }

  public function defaultSlides($registros){
    $slideFechas = array();
   
    if(!empty($registros)) {
      foreach ($registros as $registro) {
        $slideFecha = new SlideFechaBo($registro->idSlideFecha);
        $slideFechas[] = $slideFecha;
      }
    }

    return $slideFechas;
  }

  public function buscarPorFecha($fecha, $orden = NULL) {
    $slideFechas = array();
    $registros = $this->repo->buscarPorFecha($fecha, $orden);

    if(!empty($registros)) {
      foreach ($registros as $registro) {
        $slideFecha = new SlideFechaBo($registro->id_slide_fecha);
        $slideFechas[] = $slideFecha;
      }
    }
    return $slideFechas;
  }

  public function generarMasivos($idSlide, $desde, $hasta) {
    if($desde != null && $hasta != null) {
      $this->_borrarHasta($idSlide, $desde);
      $this->_borrarDesde($idSlide, $hasta);
      for($dia = $desde; $dia <= $hasta; $dia += 86400) {
        $slide_data = $this->_buscarSlideFecha($idSlide, $dia);
        if(isset($slide_data->id_slide_fecha) && $slide_data->id_slide_fecha) {
          $slide = new SlideFechaBo($slide_data->id_slide_fecha);
        } else {
          $slide = new SlideFechaBo();
          $slide->orden = 1;
        }
        $slide->slide->idSlide = $idSlide;
        $slide->fecha = $dia;
        $slide->guardar();
      }
    } else { // Si no tiene vencimiento las fechas vienen en null
      $this->_borrarNoNull($idSlide);
      $slide_data = $this->_buscarSlideFecha($idSlide, NULL);
      if(isset($slide_data->id_slide_fecha)) {
        $slide = new SlideFechaBo($slide_data->id_slide_fecha);
      } else {
        $slide = new SlideFechaBo();
      }
      $slide->slide->idSlide = $idSlide;
      $slide->fecha = NULL;
      $slide->guardar();
    }
  }

  private function _buscarSlideFecha($idSlide, $fecha) {
    return $this->repo->buscarSlideFecha($idSlide, $fecha);
  }

  private function _borrarHasta($idSlide, $desde) {
    return $this->repo->borrarHasta($idSlide, $desde);
  }

  private function _borrarDesde($idSlide, $hasta) {
    return $this->repo->borrarDesde($idSlide, $hasta);
  }

  private function _borrarNoNull($idSlide) {
    return $this->repo->borrarNoNull($idSlide);
  }
}