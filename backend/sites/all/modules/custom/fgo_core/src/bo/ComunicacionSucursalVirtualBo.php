<?php

namespace Fgo\Bo;

class ComunicacionSucursalVirtualBo extends GeneralBo {
  public $idComunicacionSucursalVirtual;
  public $idComunicacion;
  public $idSucursalVirtual;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ComunicacionSucursalVirtualDao::getInstance();
  }

  protected function construirDesdeRegistro($registro){
    $this->idComunicacionSucursalVirtual = $registro->id_comunicacion_sucursal_virtual;
    $this->idComunicacion = $registro->id_comunicacion;
    $this->idSucursalVirtual = $registro->id_sucursal_virtual;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idComunicacion = $dto->id_comunicacion;
    $this->idSucursalVirtual = $dto->id_sucursal_virtual;
  }

  public function construir($sucursal, $comunicacion) {
    $this->idComunicacion = $comunicacion->idComunicacion;
    $this->idSucursalVirtual = $sucursal->idSucursalVirtual;
  }

  public function listarPorIdCombinado() {
    $listado = $this->listar();
    $matrizCombinada = array();
    foreach ($listado as $combinacionBo) {
      $matrizCombinada[$combinacionBo->idComunicacion][$combinacionBo->idSucursalVirtual] = $combinacionBo;
    }
    return $matrizCombinada;
  }

  public function listarSucursalesPorIdComunicacion($idComunicacion) {
    $registros = $this->repo->filtrarPorIdComunicacion($idComunicacion);
    $listadoObjetosNegocio = array();
    foreach ($registros as $registro) {
      $listadoObjetosNegocio[] = new SucursalVirtualBo($registro->id_sucursal_virtual);
    }

    return $listadoObjetosNegocio;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idComunicacionSucursalVirtual) && !is_numeric($this->idComunicacionSucursalVirtual)) {
      $errores['idComunicacionSucursalVirtual'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idComunicacion) || !is_numeric($this->idComunicacion)) {
      $errores['idComunicacion'] = 'Por favor revise el ID de la comunicación.';
    }
    if (empty($this->idSucursalVirtual) || !is_numeric($this->idSucursalVirtual)) {
      $errores['idSucursalVirtual'] = 'Por favor revise el ID de la sucursal virtual.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}