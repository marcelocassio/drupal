<?php

namespace Fgo\Bo;

class ZonaBo extends GeneralBo {
  public $idZona;
  public $idZonaSuip;
  public $nombre;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ZonaDao::getInstance();
  }

  protected function construirDesdeRegistro($registro){
    $this->idZona = $registro->id_zona;
    $this->idZonaSuip = $registro->id_zona_suip;
    $this->nombre = $registro->nombre;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idZonaSuip = $dto->id_zona_suip;
    $this->nombre = $dto->nombre;
  }

  public function listarPorIdZonaSuip() {
    $listadoClaveIdSuip = array();
    $listadoZona = $this->listar();
    foreach ($listadoZona as $registro) {
      $idSuip = $registro->idZonaSuip;
      if (isset($idSuip)) {
        $listadoClaveIdSuip[$idSuip] = $registro;
      }
    }
    return $listadoClaveIdSuip;
  }

  public function buscarPorIdSuip($idSuip) {
    $registro = $this->repo->buscarPorIdSuip($idSuip);
    if ($registro) {
      $object = $this->construirDesdeRegistro($registro);
    }
    return $object;
  }


  /***
   * Busca una zona por su id de suip
   * @param $idSuip
   * @return ZonaBo|null
   */
  public static function buscarZonaPorIdSuip($idSuip) {
    $registro = \Fgo\Dao\ZonaDao::getInstance()->buscarPorIdSuip($idSuip);
    $zona = null;
    if ($registro) {
      $zona = new ZonaBo();
      $zona->construirDesdeRegistro($registro);
    }
    return $zona;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idZona) && !is_numeric($this->idZona)) {
      $errores['idZona'] = 'Por favor revise el ID de la entidad.';
    }
    if (empty($this->idZonaSuip) || strlen($this->idZonaSuip) > 11 || !is_numeric($this->idZonaSuip)) {
      $errores['idZonaSuip'] = 'Por favor revise el ID Zona SUIP.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 45) {
      $errores['nombre'] = 'Por favor revise el nombre Zona.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}