<?php

namespace Fgo\Bo;

class MedioPagoBo extends GeneralBo {

	public $idMedioPago;
	public $nombre;
	public $tipoMedioPago;
  public $idTarjetaSuip;

  protected function construirDesdeRegistro($registro) {
    $this->idMedioPago = $registro->id_medio_pago;
    $this->nombre = $registro->nombre;
    $this->tipoMedioPago = $registro->tipo_medio_pago;
    $this->idTarjetaSuip = $registro->id_tarjeta_suip;
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\MedioPagoDao::getInstance();
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idMedioPago) && !is_numeric($this->idMedioPago)) {
      $errores['idMedioPago'] = 'Por favor revise el ID de la entidad.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 150) {
      $errores['nombre'] = 'Por favor revise el nombre del Medio de Pago.';
    }
    if (!empty($this->tipoMedioPago) && strlen($this->tipoMedioPago) > 5) {
      $errores['tipoMedioPago'] = 'Por favor revise el tipo de Medio de Pago.';
    }
    if ((!empty($this->idTarjetaSuip) && !is_numeric($this->idTarjetaSuip)) || (!empty($this->idTarjetaSuip) && strlen($this->idTarjetaSuip) > 11)) {
      $errores['idTarjetaSuip'] = 'Por favor revise el ID Tarjeta Suip.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}
