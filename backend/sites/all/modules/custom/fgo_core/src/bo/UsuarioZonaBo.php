<?php

namespace Fgo\Bo;

class UsuarioZonaBo extends GeneralBo {
  public $idUsuarioZona;
  public $idUsuario;
  public $idZona;
  public $zona;
  public $zonas = array();

  public function construir($zonaBo, $usuarioBo) {
    $this->idZona = $zonaBo->idZona;
    $this->idUsuario = $usuarioBo->idUsuario;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\UsuarioZonaDao::getInstance();
  }

  public function construirDesdeRegistro($registro) {
    $this->idUsuarioZona = $registro->id_usuario_zona;
    $this->idUsuario = $registro->uid;
    $this->idZona = $registro->id_zona;
    if ($registro->id_zona != null) {
      $this->setZona(new \Fgo\Bo\ZonaBo($registro->id_zona));
    }
    return $this;
  }

  
  public function construirDesdeLineaArchivo($dto) {

    $this->idUsuario = $dto->uid;
    if ($dto->id_zona != null) {
      $this->setZona(new \Fgo\Bo\ZonaBo($dto->id_zona));
      $this->idZona = $dto->id_zona;
    }
  }

  public function buscarPorIdUsuario($idUsuario) {
    $this->zonas = array();
    $registros = $this->repo->filtrarPorIdUsuario($idUsuario);
    foreach ($registros as $registro) {
      $zona = new \Fgo\Bo\ZonaBo($registro->id_zona);
      $this->zonas[] = $zona;
    }

    return $this;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idUsuarioZona) && !is_numeric($this->idUsuarioZona)) {
      $errores['idUsuarioZona'] = 'Por favor revise el ID de la relación.';
    }
    if (empty($this->idUsuario) || !is_numeric($this->idUsuario)) {
      $errores['idUsuario'] = 'Por favor revise el ID del usuario.';
    }
    if (empty($this->idZona) || !is_numeric($this->idZona)) {
      $errores['idZona'] = 'Por favor revise el ID de la zona.';
    }
    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

      /**
   * @return mixed
   */
  public function getZona() {
    return $this->zona;
  }

  /**
   * @param mixed $zona
   */
  public function setZona($zona) {
    $this->zona = $zona;
  }

}