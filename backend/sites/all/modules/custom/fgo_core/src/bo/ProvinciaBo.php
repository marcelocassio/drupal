<?php

namespace Fgo\Bo;

class ProvinciaBo extends GeneralBo {
  public $idProvincia;
  public $idProvinciaSuip;
  public $nombre;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\ProvinciaDao::getInstance();
  }

  protected function construirDesdeRegistro($registro){
    $this->idProvincia = $registro->id_provincia;
    $this->idProvinciaSuip = $registro->id_provincia_suip;
    $this->nombre = $registro->nombre;
    return $this;
  }

  public function construirDesdeLineaArchivo($dto) {
    $this->idProvinciaSuip = $dto->id_provincia_suip;
    $this->nombre = $dto->nombre;
  }

  public function listarPorIdProvinciaSuip() {
    $listadoClaveIdSuip = array();
    $listadoProvincia = $this->listar();
    foreach ($listadoProvincia as $registro) {
      $idSuip = $registro->idProvinciaSuip;
      if (isset($idSuip)) {
        $listadoClaveIdSuip[$idSuip] = $registro;
      }
    }
    return $listadoClaveIdSuip;
  }

  public function buscarPorIdSuip($idSuip) {
    $registro = $this->repo->buscarPorIdSuip($idSuip);
    if ($registro) {
      $object = $this->construirDesdeRegistro($registro);
    }
    return $object;
  }


  /***
   * Busca una provincia por su id de suip
   * @param $idSuip
   * @return ProvinciaBo|null
   */
  public static function buscarProvinciaPorIdSuip($idSuip) {
    $registro = \Fgo\Dao\ProvinciaDao::getInstance()->buscarPorIdSuip($idSuip);
    $provincia = null;
    if ($registro) {
      $provincia = new ProvinciaBo();
      $provincia->construirDesdeRegistro($registro);
    }
    return $provincia;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idProvincia) && !is_numeric($this->idProvincia)) {
      $errores['idProvincia'] = 'Por favor revise el ID de la entidad.';
    }
    if (empty($this->idProvinciaSuip) || strlen($this->idProvinciaSuip) > 11 || !is_numeric($this->idProvinciaSuip)) {
      $errores['idProvinciaSuip'] = 'Por favor revise el ID Provincia SUIP.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 45) {
      $errores['nombre'] = 'Por favor revise el nombre Provincia.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}