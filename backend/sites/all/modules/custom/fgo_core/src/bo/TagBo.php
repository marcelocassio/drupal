<?php

namespace Fgo\Bo;

class TagBo extends GeneralBo {

  public $idTag;
  public $nombre;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\TagDao::getInstance();
  }

  protected function construirDesdeRegistro($registro) {
    $this->idTag = $registro->id_tag;
    $this->nombre = $registro->nombre;
    return $this;
  }

  public function buscarPorNombre($nombre) {
    $tagBo = null;
    $registros = $this->repo->filtrarPorNombre($nombre);
    if (isset($registros[0])) {
      $tagBo = new TagBo();
      $tagBo->construirDesdeRegistro($registros[0]);
    }
    return $tagBo;
  }

  protected function _validar() {
    $errores = array();

    if (!empty($this->idTag) && !is_numeric($this->idTag)) {
      $errores['idTag'] = 'Por favor revise el ID de la entidad.';
    }
    if (!empty($this->nombre) && strlen($this->nombre) > 100) {
      $errores['nombre'] = 'Por favor revise el nombre Tag.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }
}