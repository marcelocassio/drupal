<?php

namespace Fgo\Bo;

class GoShopItemBo extends GeneralBo {

  public $id_goshop_item;
  public $id_item;
  public $titulo_item;
  public $importe_base_item;
  public $importe_final_item;

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\GoShopItemDao::getInstance();
  }

  public function construirDesdeRegistro($reg) {

    $this->id_goshop_item = null;
    $this->id_item = $reg['id_item'];
    $this->titulo_item = $reg['titulo_item'];
    $this->importe_base_item = $reg['importe_base_item'];
    $this->importe_final_item = $reg['importe_final_item'];

    return $this;
  }
}