<?php

namespace Fgo\Bo;

class CuponBo extends GeneralBo {
  public $idCupon;
  public $codigoPromo;
  public $cuponSuip;
  public $tipoCupon;
  public $comunicacionSuip;
  public $idEstado = 0; // 0 = Pendiente de envio; 1 = Enviado; 2 = Aprobado; 3 = Rechazado; 4 = Cancelado
  public $vouchers;
  public $fechaDesde;
  public $fechaHasta;

  protected function construirDesdeRegistro($registro)  {
    $this->idCupon = $registro->id_cupon;
    $this->codigoPromo = new CodigoPromoBo($registro->id_codigo_promo);
    $this->cuponSuip = $registro->cupon_suip;
    $this->tipoCupon = $registro->tipo_cupon;
    $this->comunicacionSuip = $registro->comunicacion_suip;
    $this->idEstado = $registro->id_estado;
    $this->vouchers = $registro->vouchers;
    $this->fechaDesde = $registro->fecha_desde;
    $this->fechaHasta = $registro->fecha_hasta;

    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\CuponDao::getInstance();

    return $this->repo;
  }

  public function estaCreado($cupon_suip) {
    $registro = $this->repo->estaCreado($cupon_suip);
    if ($registro) {
      return TRUE;
    }
    return FALSE;
  }

  public static function obtenerCupones($estado = NULL) {
    $repo = \Fgo\Dao\CuponDao::getInstance();
    $registro = $repo->obtenerCupones($estado);
    $cupones = array();

    if(!empty($registro)) {
      foreach ($registro as $cupon) {
        $cupones[] = new CuponBo($cupon->id_cupon);
      }
    }
    return $cupones;
  }

  public function generarVouchers($cantidad) {
    $codigos = array();
    for($i = 1; $i <= $cantidad; $i++) {
      $codigo = false;
      while(in_array($codigo, $codigos) || !$codigo) {
        $codigo = rand(100000, 999999);
      }
      $codigos[] = $codigo;
    }
    return $codigos;
  }

  protected function _validar() {

    $errores = array();
    if (!empty($this->idCupon) && !is_numeric($this->idCupon)) {
      $errores['idCupon'] = 'Por favor revise el ID del Cupon.';
    }
    if (!isset($this->codigoPromo) || !isset($this->codigoPromo->idCodigoPromo) || !is_numeric($this->codigoPromo->idCodigoPromo)) {
      $errores['idCodigoPromo'] = 'Por favor revise el codigo promo.';
    }
    if (!isset($this->cuponSuip) || !is_numeric($this->cuponSuip)) {
      $errores['cuponSuip'] = 'Por favor revise el id del cupon SUIP.';
    }
    if (!isset($this->tipoCupon) || !is_numeric($this->tipoCupon)) {
      $errores['tipoCupon'] = 'Por favor revise el tipo de cupon.';
    }
    if (!isset($this->comunicacionSuip) && !is_numeric($this->comunicacionSuip)) {
      $errores['comunicacionSuip'] = 'Por favor revise el id de la Comunicacion SUIP.';
    }
    if (!isset($this->idEstado) && !is_numeric($this->idEstado)) {
      $errores['idEstado'] = 'Por favor revise el estado del cupon.';
    }
    if (!isset($this->vouchers) && !is_numeric($this->vouchers)) {
      $errores['vouchers'] = 'Por favor revise la cantidad de vouchers.';
    }
    if (!isset($this->fechaDesde) && !is_numeric($this->fechaDesde)) {
      $errores['fechaDesde'] = 'Por favor revise la fecha desde.';
    }
    if (!isset($this->fechaHasta) && !is_numeric($this->fechaHasta)) {
      $errores['fechaHasta'] = 'Por favor revise la fecha hasta.';
    }
    if(isset($this->fechaDesde) && isset($this->fechaHasta) && $this->fechaDesde >= $this->fechaHasta) {
      $errores['fechaHasta'] = 'La fecha hasta debe ser posterior a la fecha desde.';
    }

    if(count($errores)>0) {
      throw new DatosInvalidosException($errores);
    }

    return true;
  }

}