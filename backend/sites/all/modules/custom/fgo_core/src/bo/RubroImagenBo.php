<?php
namespace Fgo\Bo;


class RubroImagenBo extends GeneralBo {

  public $idRubro;
  public $data;

  protected function construirDesdeRegistro($registros) {
    $this->data = array();
    foreach ($registros as $registro) {
      $this->data[] = new ImagenBo($registro->id_imagen);
    }
    return $this;
  }

  protected function obtenerRepo() {
    $this->repo = \Fgo\Dao\RubroImagenDao::getInstance();
  }

  public function buscarImagenesPorIdRubro($idRubro) {

    $registros = $this->repo->filtrarPorIdRubro($idRubro);
    
    $this->idRubro = $idRubro;
    return $this->construirDesdeRegistro($registros);
  }

}