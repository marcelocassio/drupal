<?php
/**
 *  Active record para los objetos de negocio
 */

namespace Fgo\Bo;

abstract class GeneralBo {

  protected $repo;

  abstract protected function construirDesdeRegistro($registro);
  abstract protected function obtenerRepo();

  public function __construct($id = null) {
    $this->obtenerRepo();
    if($id) {
      return $this->buscarPorId($id);
    }
    return null;
  }

  public function buscarPorId($id) {
    $object = null;
    $registro = $this->repo->buscarPorId($id);
    if($registro) {
      $object = $this->construirDesdeRegistro($registro);
    }

    return $object;
  }

  public function listar($order = null) {
    $listadoObjetosNegocio = array();
    $registros = $this->repo->listar($order);

    foreach ($registros as $registro) {
      $objeto = new $this;
      $listadoObjetosNegocio[] = $objeto->construirDesdeRegistro($registro);
    }
    return $listadoObjetosNegocio;
  }

  public function guardar() {
    $this->_validar();
    return $this->repo->guardar($this);
  }

  public function guardarRelaciones() {
    return $this->repo->guardarRelaciones($this);
  }

  public function eliminar() {
    $this->repo->eliminar($this);
  }

  public function filtrar($conditions, $orden = null) {
    return $this->repo->filtrar($conditions, $orden);
  }

  protected function _validar() {
    return true;
  }
}

class DatosInvalidosException extends \Exception {

  protected $errores;

  public function __construct($errores) {
    parent::__construct("Datos inválidos");
    $this->errores = $errores;
  }

  public function getErrores() {
    return $this->errores;
  }
}