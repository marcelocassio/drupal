<?php

namespace Fgo\Dao;

class ComunicacionSucursalFisicaDao extends GeneralDao {

  protected function obtenerNombreTabla(){
    return 'fgo_comunicacion_sucursal_fisica';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_comunicacion_sucursal_fisica';
  }

  protected function obtenerNombreCampoClaveComunicacion(){
    return 'id_comunicacion';
  }

  protected function obtenerNombreCampoClaveSucursalF(){
    return 'id_sucursal_fisica';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_comunicacion_sucursal_fisica' => $objetoNegocio->idComunicacionSucursalFisica,
      'id_comunicacion' => $objetoNegocio->idComunicacion,
      'id_sucursal_fisica' => $objetoNegocio->idSucursalFisica
    );
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $condiciones['id_comunicacion'] = array("value" => $idComunicacion, "operator" => "=");
    return $this->filtrar($condiciones);
  }

  public function filtrarPorIdComunicacionIdSucursal($idComunicacion, $idSucursalFisica) {
    $condiciones['id_comunicacion'] = array("value" => $idComunicacion, "operator" => "=");
    $condiciones['id_sucursal_fisica'] = array("value" => $idSucursalFisica, "operator" => "=");
    return $this->filtrar($condiciones);
  }

}