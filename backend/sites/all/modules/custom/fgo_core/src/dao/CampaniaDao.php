<?php

namespace Fgo\Dao;

class CampaniaDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return "fgo_campania";
  }

  protected function obtenerNombreCampoClave() {
    return "id_campania";
  }

  protected function obtenerNombreCampoClaveSuip() {
    return "id_campania_suip";
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array (
      'id_campania' => $objetoNegocio->idCampania,
      'nombre' => $objetoNegocio->nombre,
      'id_campania_suip' => $objetoNegocio->idCampaniaSuip,
      'fecha_desde' => $objetoNegocio->fechaDesde,
      'fecha_hasta' => $objetoNegocio->fechaHasta,
      'siempre_visible' => (int) $objetoNegocio->siempreVisible,
    );
  }

  public function buscarPorIdSuip($id) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');
    $query->condition($this->obtenerNombreCampoClaveSuip(), $id);
    return $query->execute()->fetchObject();
  }

}
