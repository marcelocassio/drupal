<?php

namespace Fgo\Dao;

class CampaniaComunicacionDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return 'fgo_campania_comunicacion';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_campania_comunicacion';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_campania_comunicacion' => $objetoNegocio->idCampaniaComunicacion,
      'id_campania' => $objetoNegocio->idCampania,
      'id_comunicacion' => $objetoNegocio->idComunicacion,
    );
  }

  public function filtrarPorIdCampania($idCampania) {
    $conditions = array("id_campania" => array("value" => $idCampania, "operator" => "="));
    return $this->filtrar($conditions);
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $conditions = array("id_comunicacion" => array("value" => $idComunicacion, "operator" => "="));
    return $this->filtrar($conditions);
  }

  public function buscarPorIdCampaniaIdComunicacion($idCampania, $idComunicacion){
    $conditions = array(
      "id_comunicacion" => array("value" => $idComunicacion, "operator" => "="), 
      "id_campania" => array("value" => $idCampania, "operator" => "=")
    );
    return $this->filtrar($conditions); 
  }
}