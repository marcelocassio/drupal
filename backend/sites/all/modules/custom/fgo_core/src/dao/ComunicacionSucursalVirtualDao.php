<?php

namespace Fgo\Dao;

class ComunicacionSucursalVirtualDao extends GeneralDao {

  protected function obtenerNombreTabla(){
    return 'fgo_comunicacion_sucursal_virtual';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_comunicacion_sucursal_virtual';
  }

  protected function obtenerNombreCampoClaveComunicacion(){
    return 'id_comunicacion';
  }

  protected function obtenerNombreCampoClaveSucursalV(){
    return 'id_sucursal_virtual';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_comunicacion_sucursal_virtual' => $objetoNegocio->idComunicacionSucursalVirtual,
      'id_comunicacion' => $objetoNegocio->idComunicacion,
      'id_sucursal_virtual' => $objetoNegocio->idSucursalVirtual
    );
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $condiciones['id_comunicacion'] = array("value" => $idComunicacion, "operator" => "=");
    return $this->filtrar($condiciones);
  }
}