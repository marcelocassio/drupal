<?php

namespace Fgo\Dao;
use PDO;
class UsuarioProvinciaDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_usuario_provincia';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_usuario_provincia';
  }

  protected function obtenerNombreCampoClaveProvincia() {
    return 'id_provincia';
  }

  protected function obtenerNombreCampoClaveUsuario() {
    return 'uid';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_usuario_provincia' => $objetoNegocio->idUsuarioProvincia,
      'uid' => $objetoNegocio->idUsuario,
      'id_provincia' => ($objetoNegocio->getProvincia() != null) ?
      $objetoNegocio->getProvincia()->idProvincia : null,
    );
  }

  public function filtrarPorIdUsuario($idUsuario) {
    $conditions = array("uid" => array("value" => $idUsuario, "operator" => "="));
    return $this->filtrar($conditions);
  }

}