<?php

namespace Fgo\Dao;

class CuponDao extends GeneralDao {

  protected function obtenerNombreTabla()
  {
    return 'fgo_cupon';
  }

  protected function obtenerNombreCampoClave()
  {
    return 'id_cupon';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array (
      'id_cupon' => $objetoNegocio->idCupon,
      'id_codigo_promo' => ($objetoNegocio->codigoPromo != null) ? $objetoNegocio->codigoPromo->idCodigoPromo: null,
      'cupon_suip' => $objetoNegocio->cuponSuip,
      'tipo_cupon' => $objetoNegocio->tipoCupon,
      'comunicacion_suip' => $objetoNegocio->comunicacionSuip,
      'id_estado' => $objetoNegocio->idEstado,
      'vouchers' => $objetoNegocio->vouchers,
      'fecha_desde' => $objetoNegocio->fechaDesde,
      'fecha_hasta' => $objetoNegocio->fechaHasta,
    );
  }

  public function estaCreado($cupon_suip) {
    $query = db_select($this->obtenerNombreTabla(), 'fc');
    $query->fields('fc');
    $query->condition('fc.cupon_suip', $cupon_suip);

    $result = $query->execute()->fetchObject();
    return $result;
  }

  public function obtenerCupones($estado = NULL) {
    $query = db_select($this->obtenerNombreTabla(), 'fc');
    $query->fields('fc');
    if($estado) {
      $query->condition('fc.id_estado', $estado);
    }

    $result = $query->execute()->fetchAll();
    return $result;
  }
}