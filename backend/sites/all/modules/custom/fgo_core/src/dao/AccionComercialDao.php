<?php
  namespace Fgo\Dao;
  class AccionComercialDao  extends GeneralDao {

    protected function obtenerNombreTabla() {
      return "fgo_accion_comercial";
    }

    protected function obtenerNombreCampoClave() {
      return 'id_accion_comercial';
    }
    protected function obtenerNombreCampoClaveSuip() {
      return 'id_accion_comercial_suip';
    }

    protected function obtenerMapeoCampos($accionComercial) {
      return array(
        'id_accion_comercial' => $accionComercial->idAccionComercial,
        'nombre' => $accionComercial->nombre,
        'id_accion_comercial_suip' => $accionComercial->idAccionComercialSuip,
      );
    }
  }