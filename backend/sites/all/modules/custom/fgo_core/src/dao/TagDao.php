<?php

namespace Fgo\Dao;


class TagDao extends GeneralDao {
  protected function obtenerNombreTabla() {
    return 'fgo_tag';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_tag';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_tag' => $objetoNegocio->idTag,
      'nombre' => $objetoNegocio->nombre,
    );
  }

  public function filtrarPorNombre($nombre) {
    $condicion['nombre'] = array('value' => $nombre, 'operator' => '=');
    return $this->filtrar($condicion);
  }

}