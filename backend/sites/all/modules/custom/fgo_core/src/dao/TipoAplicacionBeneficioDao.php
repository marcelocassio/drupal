<?php

namespace Fgo\Dao;

class TipoAplicacionBeneficioDao extends GeneralDao {

  protected function obtenerNombreTabla()
  {
    return 'fgo_tipo_aplicacion_beneficio';
  }

  protected function obtenerNombreCampoClave()
  {
    return 'id_tipo_aplicacion_beneficio';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_tipo_aplicacion_beneficio' => $objetoNegocio->idTipoAplicacionBeneficio,
      'id_tipo_aplicacion_beneficio_suip' => $objetoNegocio->idTipoAplicacionBeneficioSuip,
      'nombre' => $objetoNegocio->nombre,
    );
  }

  public function filtrarPorIdTipoAplicacionBeneficioSuip($idTipoAplicacionBeneficioSuip) {
    $condicion['id_tipo_aplicacion_beneficio_suip'] = array('value' => $idTipoAplicacionBeneficioSuip, 'operator' => '=');
    return $this->filtrar($condicion);
  }
}