<?php

namespace Fgo\Dao;


class ComunicacionTagDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return "fgo_comunicacion_tag";
  }

  protected function obtenerNombreCampoClave() {
    return "id_comunicacion_tag";
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      "id_comunicacion_tag" => $objetoNegocio->idComunicacionTag,
      "id_comunicacion" => $objetoNegocio->idComunicacion,
      "id_tag" => $objetoNegocio->idTag
    );
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $conditions = array("id_comunicacion" => array("value" => $idComunicacion, "operator" => "="));
    return $this->filtrar($conditions);
  }

}