<?php

namespace Fgo\Dao;

class ProvinciaDao extends GeneralDao {

  protected function obtenerNombreTabla(){
    return 'fgo_provincia';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_provincia';
  }
  protected function obtenerNombreCampoClaveSuip(){
    return 'id_provincia_suip';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_provincia' => $objetoNegocio->idProvincia,
      'id_provincia_suip' => $objetoNegocio->idProvinciaSuip,
      'nombre' => $objetoNegocio->nombre
    );
  }

  public function buscarPorIdSuip($idProvinciaSuip) {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c')->condition($this->obtenerNombreCampoClaveSuip(), $idProvinciaSuip);
    $result = $query->execute()->fetchObject();

    return $result;
  }
}