<?php

namespace Fgo\Dao;

class SlideFechaDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_slide_fecha';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_slide_fecha';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_slide_fecha' => $objetoNegocio->idSlideFecha,
      'id_slide' => ($objetoNegocio->slide) ? $objetoNegocio->slide->idSlide : null,
      'orden' => $objetoNegocio->orden,
      'fecha' => $objetoNegocio->fecha,
    );
  }

  public function buscarPorFecha($fecha, $orden = null) {
    $query = db_select($this->obtenerNombreTabla(), 'slide')->fields('slide');

    if ($orden){
      $query->condition("slide.fecha", $fecha);
      $query->isNotNull('slide.orden');
    }
    else{
      $query->condition(
        db_or()->isNull('slide.fecha')
        ->condition("slide.fecha", $fecha)
      );
    }
    $query->orderBy('slide.orden', 'ASC');
    $query->orderBy('slide.fecha', 'DESC');

    return $query->execute()->fetchAll();
  }

  public function buscarSlideFecha($idSlide, $fecha) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');

    $query->condition("id_slide", $idSlide);
    $query->condition("fecha", $fecha);

    return $query->execute()->fetchObject();
  }

  public function borrarDesde($idSlide, $hasta) {
    $query = db_delete('fgo_slide_fecha')
      ->condition("id_slide", $idSlide)
      ->condition('fecha', $hasta, '>')
      ->execute();

    return $query;
  }

  public function borrarHasta($idSlide, $desde) {
    $query = db_delete('fgo_slide_fecha')
      ->condition("id_slide", $idSlide)
      ->condition('fecha', $desde, '<')
      ->execute();

    return $query;
  }

  public function borrarNoNull($idSlide) {
    $query = db_delete('fgo_slide_fecha')
      ->condition("id_slide", $idSlide)
      ->isNotNull('fecha')
      ->execute();

    return $query;
  }
}