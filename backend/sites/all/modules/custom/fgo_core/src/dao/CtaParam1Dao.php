<?php
namespace Fgo\Dao;

class CtaParam1Dao extends GeneralDao
{
    protected function obtenerNombreTabla()
    {
        return 'fgo_cta_param1';
    }

    protected function obtenerNombreCampoClave()
    {
        return 'id_param1';
    }

    protected function obtenerMapeoCampos($objetoNegocio)
    {
        return array(
            'id_param1' => $objetoNegocio->idParam1,
            'nombre' => $objetoNegocio->nombre,
            'call_to_action' => $objetoNegocio->callToAction
        );
    }
}
