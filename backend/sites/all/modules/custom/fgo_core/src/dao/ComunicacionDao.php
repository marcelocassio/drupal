<?php
namespace Fgo\Dao;

class ComunicacionDao extends GeneralDao {
  
  protected function obtenerNombreTabla() {
    return 'fgo_comunicacion';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_comunicacion';
  }

  protected function obtenerNombreCampoClaveSuip(){
    return 'id_beneficio_suip';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_comunicacion' => $objetoNegocio->idComunicacion,
      'titulo' => $objetoNegocio->titulo,
      'prioridad' => $objetoNegocio->prioridad,
      'id_beneficio_suip' => $objetoNegocio->idBeneficioSuip,
      'descripcion_corta' => $objetoNegocio->descripcionCorta,
      'descripcion_extendida' => $objetoNegocio->descripcionExtendida,
      'id_accion_comercial' => ($objetoNegocio->accionComercial != null) ? $objetoNegocio->accionComercial->idAccionComercial: null,
      'fecha_desde' => $objetoNegocio->fechaDesde,
      'fecha_hasta' => $objetoNegocio->fechaHasta,
      'terminos_condiciones' => $objetoNegocio->terminosCondiciones,
      'cft' => $objetoNegocio->cft,
      'publicado' => ($objetoNegocio->publicado)?1:0,
      'peso' => $objetoNegocio->peso,
      'id_comercio' => ($objetoNegocio->comercio != null) ? $objetoNegocio->comercio->idComercio : null,
      'zonas_adheridas' => $objetoNegocio->zonasAdheridas,
      'destacado' => $objetoNegocio->destacado,
      'siempre_visible' => (int) $objetoNegocio->siempreVisible,
      'como_uso' => ($objetoNegocio->comoUso != null) ? $objetoNegocio->comoUso : null,
    );
  }

  function listarPorIdBeneficioSuip() {
    $listado = array();
    $comunicaciones = $this->listar();

    foreach ($comunicaciones as $comunicacionBo) {
      if (isset($comunicacionBo->idBeneficioSuip)) {
        $listado[$comunicacionBo->idBeneficioSuip] = $comunicacionBo;
      }
    }
    return $listado;
  }

  public function buscarPorIdSuip($id) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');

    $query->condition($this->obtenerNombreCampoClaveSuip(), $id);

    $result = $query->execute()->fetchObject();
    return $result;
  }

  /**
   * Filtra las comunicaciones con
   * fecha desde menor o igual a $fechaDesde y con
   * fecha hasta mayor o igual a $fechaHasta
   * @param $fechaDesde
   * @param $fechaHasta
   * @return Array Listado de registros encontrados
   */
  public function filtrarVigentesOrdenados($fechaDesde, $fechaHasta) {
    $query = db_select($this->obtenerNombreTabla(), 'c');
    $query->fields('c');
    $query->condition(
      db_or()
        ->condition('siempre_visible', 1, '=')
        ->condition('fecha_desde', strtotime($fechaDesde), '<=')
    );
    $query->condition('fecha_hasta', strtotime($fechaHasta), '>=');
    $query->condition('publicado', 1, '=');
    $query = $query->orderBy('peso', 'DESC');

    $result = $query->execute()->fetchAll();
    return $result;
  }

  public function obtenerIdSuipDeComunicacionesNoVencidas() {
    $query = db_select($this->obtenerNombreTabla(), 'c');
    $query->fields('c', array('id_beneficio_suip'));
    $query->condition('fecha_hasta', strtotime("TODAY"), '>=');

    $result = $query->execute()->fetchAll();
    return $result;
  }

  /**
   * Filtra comunicacion en base al campo fecha desde
   * @param $fechaDesdeInicial
   * @param $fechaDesdeFinal
   * @return mixed
   */
  public function filtrarFechasDesde($fechaDesdeInicial, $fechaDesdeFinal) {
    $condicion['fecha_desde'] = array('value' => strtotime($fechaDesdeInicial), 'operator' => '>=');
    $condicion['fecha_desde'] = array('value' => strtotime($fechaDesdeFinal), 'operator' => '<=');
    $orden = array("fecha_desde" => "DESC");
    return $this->filtrar($condicion, $orden);
  }


  public function filtrarFechasHastaMayorQue($fecha) {
    $condicion['fecha_hasta'] = array('value' => strtotime($fecha), 'operator' => '>=');
    $orden = array("fecha_desde" => "DESC");
    return $this->filtrar($condicion, $orden);
  }

  /**
   * Filtra comunicacion en base al campo Fecha Hasta
   * @param $fechaHastaInicial
   * @param $fechaHastaFinal
   * @return mixed
   */
  public function filtrarFechasHasta($fechaHastaInicial, $fechaHastaFinal) {
    $condicion['fecha_hasta'] = array('value' => array(strtotime($fechaHastaInicial), strtotime($fechaHastaFinal)), 'operator' => 'BETWEEN');
    $orden = array("fecha_hasta" => "DESC");
    return $this->filtrar($condicion, $orden);
  }

  public function filtrarUltimaComunicacionPorIdComercio($idComercio) {
    $sq_max = db_select("fgo_comunicacion", "c");
    $sq_max->addExpression('MAX(id_comunicacion)', 'id_comunicacion_max');
    $sq_max->condition("c.id_comercio", $idComercio);

    $query = db_select("fgo_comunicacion", "c1")
      ->fields('c1');
    $query->addJoin("INNER", $sq_max, 'c', 'c.id_comunicacion_max = c1.id_comunicacion');

    $result = $query->execute()->fetchAll();

    return $result;
  }

  public function filtrarPorIdRubro($idRubro, $limit, $fechaDesde, $fechaHasta){
    $query = db_select($this->obtenerNombreTabla(), 'c');
    $query->fields('c');
    $query->innerJoin('fgo_comunicacion_rubro', 'fcr', 'fcr.id_comunicacion = c.id_comunicacion');
    $query->condition('publicado', 1, '=');
    $query->condition('fecha_desde', strtotime($fechaDesde), '<=');
    $query->condition('fecha_hasta', strtotime($fechaHasta), '>=');
    $query->condition('fcr.id_rubro', $idRubro, '=');
    $query->groupBy('c.id_comunicacion');
    $query->range(0,$limit);
    $query->orderBy('peso', 'DESC');
    $query->orderBy('fecha_hasta', 'DESC');

    return $query->execute()->fetchAll();
  }

}
