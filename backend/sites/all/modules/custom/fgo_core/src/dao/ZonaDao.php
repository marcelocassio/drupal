<?php

namespace Fgo\Dao;

class ZonaDao extends GeneralDao {

  protected function obtenerNombreTabla(){
    return 'fgo_zona';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_zona';
  }
  protected function obtenerNombreCampoClaveSuip(){
    return 'id_zona_suip';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_zona' => $objetoNegocio->idZona,
      'id_zona_suip' => $objetoNegocio->idZonaSuip,
      'nombre' => $objetoNegocio->nombre
    );
  }

  public function buscarPorIdSuip($idZonaSuip) {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c')->condition($this->obtenerNombreCampoClaveSuip(), $idZonaSuip);
    $result = $query->execute()->fetchObject();

    return $result;
  }
}