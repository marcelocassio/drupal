<?php

namespace Fgo\Dao;


class PromoCuponDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return "fgo_promo_cupon";
  }

  protected function obtenerNombreCampoClave() {
    return "id_comercio_cupon";
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      "id_comercio_cupon" => $objetoNegocio->idComercioCupon,
      "id_cupon" => $objetoNegocio->idCupon,
      "id_comercio" => $objetoNegocio->idComercio,
      "id_comunicacion" => $objetoNegocio->idComunicacion,
      "id_beneficio_suip" => $objetoNegocio->idBeneficioSuip,
    );
  }

  public function filtrarPorIdCupon($idCupon) {
    $condicion['id_cupon'] = array('value' => $idCupon, 'operator' => '=');
    return $this->filtrar($condicion);
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $condicion['id_comunicacion'] = array('value' => $idComunicacion, 'operator' => '=');
    return $this->filtrar($condicion);
  }


  public function filtrarConComunicacionPendiente() {
    $query = db_select('fgo_promo_cupon', 'c')->fields('c');
    $query->isNotNull('id_beneficio_suip');
    $query->isNull('id_comunicacion');
    return $query->execute()->fetchAll();
  }

}