<?php

namespace Fgo\Dao;

class LocalidadDao extends GeneralDao {

  protected function obtenerNombreTabla(){
    return 'fgo_localidad';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_localidad';
  }

  protected function obtenerNombreCampoClaveSuip(){
    return 'id_localidad_suip';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_localidad' => $objetoNegocio->idLocalidad,
      'id_localidad_suip' => $objetoNegocio->idLocalidadSuip,
      'nombre' => $objetoNegocio->nombre,
      'codigo_postal' => $objetoNegocio->codigoPostal
    );
  }

  public function buscarPorIdSuip($idLocalidadSuip) {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c')->condition($this->obtenerNombreCampoClaveSuip(), $idLocalidadSuip);
    $result = $query->execute()->fetchObject();

    return $result;
  }
}