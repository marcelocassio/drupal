<?php

namespace Fgo\Dao;


class BeneficioMedioPagoDao extends GeneralDao {

  protected function obtenerNombreTabla()
  {
    return "fgo_beneficio_medio_pago";
  }

  protected function obtenerNombreCampoClave()
  {
    return "id_beneficio_medio_pago";
  }

  protected function obtenerMapeoCampos($objetoNegocio)
  {
    return array (
      'id_beneficio_medio_pago' => $objetoNegocio->idBeneficioMedioPago,
      'id_beneficio' => $objetoNegocio->idBeneficio,
      'id_medio_pago' => $objetoNegocio->idMedioPago,
    );
  }

  public function filtrarPorIdBeneficio($idBeneficio) {
    $conditions['id_beneficio'] = array('value' => $idBeneficio, 'operator' => '=');
    return $this->filtrar($conditions);
  }


}