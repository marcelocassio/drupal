<?php
namespace Fgo\Dao;

class ImagenDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_imagen';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_imagen';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_imagen' => $objetoNegocio->idImagen,
      'fid' => ($objetoNegocio->datosImagen != null) ? $objetoNegocio->datosImagen->fid: null,
      'codigo_tipo_imagen' => $objetoNegocio->codigoTipoImagen,
    );
  }

  public function buscarPorFid($fid) {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c')->condition("fid", $fid);
    $result = $query->execute()->fetchObject();

    return $result;
  }
}