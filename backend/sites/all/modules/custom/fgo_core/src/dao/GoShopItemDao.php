<?php

namespace Fgo\Dao;

class GoShopItemDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_goshop_item';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_goshop_item';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {

    return array(
      'id_goshop_item' => $objetoNegocio->id_goshop_item,
      'id_item'=> $objetoNegocio->id_item,
      'titulo_item'=> $objetoNegocio->titulo_item,
      'importe_base_item'=> $objetoNegocio->importe_base_item,   
      'importe_final_item'=> $objetoNegocio->importe_final_item      
    );
  }
}