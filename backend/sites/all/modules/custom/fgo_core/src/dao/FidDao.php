<?php
namespace Fgo\Dao;

class FidDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'file_managed';
  }

  protected function obtenerNombreCampoClave() {
    return 'fid';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'fid' => $objetoNegocio->fid,
      'uri' => $objetoNegocio->uri,
    );
  }
}