<?php

namespace Fgo\Dao;

class SegmentacionDao  extends GeneralDao {
    
    public $segmentacion_user = null;

  protected function obtenerNombreTabla() {
    return "fgo_segmentacion";
  }

  protected function obtenerNombreCampoClave() {
    return 'id_fgo_segmentacion';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_fgo_segmentacion' => $objetoNegocio->id_fgo_segmentacion,
      'cliente' => $objetoNegocio->cliente,
      'segmento_cod' => $objetoNegocio->segmento_cod,
      'segmento_desc' => $objetoNegocio->segmento_desc,
      'es_cliente_actividad' => $objetoNegocio->es_cliente_actividad,
      'tiene_ca_ars_en_actividad' => $objetoNegocio->tiene_ca_ars_en_actividad,
      'tiene_ca_usd_en_actividad' => $objetoNegocio->tiene_ca_usd_en_actividad,
      'tiene_cc_ars_en_actividad' => $objetoNegocio->tiene_cc_ars_en_actividad,
      'tiene_cc_usd_en_actividad' => $objetoNegocio->tiene_cc_usd_en_actividad,
      'tiene_tc_visa_en_actividad' => $objetoNegocio->tiene_tc_visa_en_actividad,
      'tiene_tc_master_en_actividad' => $objetoNegocio->tiene_tc_master_en_actividad,
      'tiene_pp_en_actividad' => $objetoNegocio->tiene_pp_en_actividad,
      'es_titular_tc_visa' => $objetoNegocio->es_titular_tc_visa,
      'es_titular_tc_visa_latam' => $objetoNegocio->es_titular_tc_visa_latam,
      'es_titular_tc_visa_nolatam' => $objetoNegocio->es_titular_tc_visa_nolatam,
      'es_titular_tc_visa_nolatam_inter' => $objetoNegocio->es_titular_tc_visa_nolatam_inter,
      'es_titular_tc_master' => $objetoNegocio->es_titular_tc_master,
      'es_titular_tc_master_latam' => $objetoNegocio->es_titular_tc_master_latam,
      'es_titular_tc_master_nolatam' => $objetoNegocio->es_titular_tc_master_nolatam,
      'es_titular_tc_master_nolatam_inter' => $objetoNegocio->es_titular_tc_master_nolatam_inter,
      'es_adicional_tc_visa' => $objetoNegocio->es_adicional_tc_visa,
      'es_adicional_tc_visa_latam' => $objetoNegocio->es_adicional_tc_visa_latam,
      'es_adicional_tc_visa_nolatam' => $objetoNegocio->es_adicional_tc_visa_nolatam,
      'es_adicional_tc_visa_nolatam_inter' => $objetoNegocio->es_adicional_tc_visa_nolatam_inter,
      'es_adicional_tc_master' => $objetoNegocio->es_adicional_tc_master,
      'es_adicional_tc_master_latam' => $objetoNegocio->es_adicional_tc_master_latam,
      'es_adicional_tc_master_nolatam' => $objetoNegocio->es_adicional_tc_master_nolatam,
      'es_adicional_tc_master_nolatam_inter' => $objetoNegocio->es_adicional_tc_master_nolatam_inter,
      'es_ps' => $objetoNegocio->es_ps,
      'subscription_close_dt' => $objetoNegocio->subscription_close_dt,
      'business_area_cd' => $objetoNegocio->business_area_cd,
      'col_xt_2' => $objetoNegocio->col_xt_2,
      'col_xt_3' => $objetoNegocio->col_xt_3,
      'col_xt_4' => $objetoNegocio->col_xt_4
    );
  }

  public function obtenerSegmentaciones() {

    $query = db_select($this->obtenerNombreTabla(), 'segmentacion');
    $query->fields('segmentacion');
    $query->orderBy('segmentacion.id_fgo_segmentacion', 'ASC');

    $result = $query->execute()->fetchAll();
    return $result;
  }

  public function obtenerSegmentacionUsuario($clienteAltamira = false){

    global $user;
    
    if(!isset($user->name) || !is_numeric($user->name)){
        return false;
    }
    
    if($this->segmentacion_user ){
        return $this->segmentacion_user;
    }
    $numero_altamira = $clienteAltamira ? $clienteAltamira : $user->name;
    $query = db_select($this->obtenerNombreTabla(), 'segmentacion');
    $query->fields('segmentacion');
    $query->condition('cliente', $numero_altamira);
    $result = $query->execute()->fetchObject();
    
    $this->segmentacion_user = $result;
    return $this->segmentacion_user;

  }

  public function esMaster($segmentacion){
    return  $segmentacion->es_titular_tc_master || $segmentacion->es_adicional_tc_master || 
            $segmentacion->tiene_tc_master_en_actividad || $segmentacion->es_titular_tc_master_latam || 
            $segmentacion->es_titular_tc_master_nolatam || $segmentacion->es_titular_tc_master_nolatam_inter || 
            $segmentacion->es_adicional_tc_master_latam || $segmentacion->es_adicional_tc_master_nolatam ||
            $segmentacion->es_adicional_tc_master_nolatam_inter;
  }

  public function esLatam($segmentacion){
    return $segmentacion->es_titular_tc_visa_latam || $segmentacion->es_titular_tc_master_latam || $segmentacion->es_adicional_tc_visa_latam || $segmentacion->es_adicional_tc_master_latam; 
  }
  
  public function esDomoP2p($segmentacion){
    return ($segmentacion->business_area_cd == 80 || $segmentacion->business_area_cd ==  82);
  }

   public function esDomoQr($segmentacion){
    return ($segmentacion->business_area_cd == 80 || $segmentacion->business_area_cd ==  82);
  }
  
  public function esCliente($segmentacion){
    return $segmentacion->es_cliente_actividad;
  }
  
  public function listarPage($page = 0 , $order = null) {
    $query = db_select($this->obtenerNombreTabla(), 'c');
    $query->fields('c');
    if($order) {
      foreach ($order as $column => $value) {
        $query->orderBy($column, $value);
      }
    }
        
    $limit = 50;
    $query->range($page*$limit, $limit);
    $result = $query->execute()->fetchAll();
    return $result;
  }
  
  public function buscarPorCliente($id) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');

    $query->condition("cliente", $id);

    $result = $query->execute()->fetchObject();
    return $result;
  }

  public function listarPaginado( $params ) 
  {
    // Select table
    $query = db_select("fgo_segmentacion", "r");
    // Select fields
    $query->fields("r", array("
      id_fgo_segmentacion", 
      "cliente", 
      "segmento_cod", 
      "segmento_desc",
      "es_cliente_actividad", 
      "tiene_ca_ars_en_actividad", 
      "tiene_ca_usd_en_actividad", 
      "tiene_cc_ars_en_actividad", 
      "tiene_cc_usd_en_actividad", 
      "tiene_tc_visa_en_actividad", 
      "tiene_tc_master_en_actividad", 
      "tiene_pp_en_actividad", 
      "es_titular_tc_visa", 
      "es_titular_tc_visa_latam", 
      "es_titular_tc_visa_nolatam", 
      "es_titular_tc_master", 
      "es_titular_tc_master_latam", 
      "es_titular_tc_master_nolatam",   
      "es_adicional_tc_visa", 
      "es_adicional_tc_visa_latam", 
      "es_adicional_tc_visa_nolatam", 
      "es_adicional_tc_master", 
      "es_adicional_tc_master_latam", 
      "es_adicional_tc_master_nolatam", 
      "es_ps",
      "business_area_cd"
    ));
    
    // For pagination
    $query = $query->extend('TableSort')->extend('PagerDefault')->limit(50);
    
    if (isset($params['cliente']) && $params['cliente'] !== ''){	
      $query->condition('r.cliente', '%'.$params['cliente'].'%', 'LIKE');  
    }
    if (isset($params['cod']) && $params['cod'] !== ''){	
      $query->condition('r.segmento_cod', '%'.$params['cod'].'%', 'LIKE');  
    }
    if (isset($params['business_area_cd']) && $params['business_area_cd'] !== ''){	
      $query->condition('r.business_area_cd', '%'.$params['business_area_cd'].'%', 'LIKE');
    }
    if (isset($params['es_cliente_actividad']) && $params['es_cliente_actividad'] !== ''){	
      $query->condition('r.es_cliente_actividad', $params['es_cliente_actividad']);  
    }
    if (isset($params['tiene_ca_ars_en_actividad']) && $params['tiene_ca_ars_en_actividad'] !== ''){	
      $query->condition('r.tiene_ca_ars_en_actividad', $params['tiene_ca_ars_en_actividad']);  
    } 
    if (isset($params['tiene_ca_usd_en_actividad']) && $params['tiene_ca_usd_en_actividad'] !== ''){	
      $query->condition('r.tiene_ca_usd_en_actividad', $params['tiene_ca_usd_en_actividad']);  
    } 
    if (isset($params['tiene_cc_ars_en_actividad']) && $params['tiene_cc_ars_en_actividad'] !== ''){	
      $query->condition('r.tiene_cc_ars_en_actividad', $params['tiene_cc_ars_en_actividad']);  
    } 
    if (isset($params['tiene_cc_usd_en_actividad']) && $params['tiene_cc_usd_en_actividad'] !== ''){	
      $query->condition('r.tiene_cc_usd_en_actividad', $params['tiene_cc_usd_en_actividad']);  
    } 
    if (isset($params['tiene_tc_visa_en_actividad']) && $params['tiene_tc_visa_en_actividad'] !== ''){	
      $query->condition('r.tiene_tc_visa_en_actividad', $params['tiene_tc_visa_en_actividad']);  
    } 
    if (isset($params['tiene_tc_master_en_actividad']) && $params['tiene_tc_master_en_actividad'] !== ''){	
      $query->condition('r.tiene_tc_master_en_actividad', $params['tiene_tc_master_en_actividad']);  
    } 
    if (isset($params['tiene_pp_en_actividad']) && $params['tiene_pp_en_actividad'] !== ''){	
      $query->condition('r.tiene_pp_en_actividad', $params['tiene_pp_en_actividad']);  
    } 
    if (isset($params['es_titular_tc_visa']) && $params['es_titular_tc_visa'] !== ''){	
      $query->condition('r.es_titular_tc_visa', $params['es_titular_tc_visa']);  
    } 
    if (isset($params['es_titular_tc_visa_latam']) && $params['es_titular_tc_visa_latam'] !== ''){	
      $query->condition('r.es_titular_tc_visa_latam', $params['es_titular_tc_visa_latam']);  
    } 
    if (isset($params['es_titular_tc_visa_nolatam']) && $params['es_titular_tc_visa_nolatam'] !== ''){	
      $query->condition('r.es_titular_tc_visa_nolatam', $params['es_titular_tc_visa_nolatam']);  
    } 
    if (isset($params['es_titular_tc_master']) && $params['es_titular_tc_master'] !== ''){	
      $query->condition('r.es_titular_tc_master', $params['es_titular_tc_master']);  
    } 
    if (isset($params['es_titular_tc_master_latam']) && $params['es_titular_tc_master_latam'] !== ''){	
      $query->condition('r.es_titular_tc_master_latam', $params['es_titular_tc_master_latam']);  
    } 
    if (isset($params['es_titular_tc_master_nolatam']) && $params['es_titular_tc_master_nolatam'] !== ''){	
      $query->condition('r.es_titular_tc_master_nolatam', $params['es_titular_tc_master_nolatam']);  
    }   
    if (isset($params['es_adicional_tc_visa']) && $params['es_adicional_tc_visa'] !== ''){	
      $query->condition('r.es_adicional_tc_visa', $params['es_adicional_tc_visa']);  
    } 
    if (isset($params['es_adicional_tc_visa_latam']) && $params['es_adicional_tc_visa_latam'] !== ''){	
      $query->condition('r.es_adicional_tc_visa_latam', $params['es_adicional_tc_visa_latam']);  
    } 
    if (isset($params['es_adicional_tc_visa_nolatam']) && $params['es_adicional_tc_visa_nolatam'] !== ''){	
      $query->condition('r.es_adicional_tc_visa_nolatam', $params['es_adicional_tc_visa_nolatam']);  
    } 
    if (isset($params['es_adicional_tc_master']) && $params['es_adicional_tc_master'] !== ''){	
      $query->condition('r.es_adicional_tc_master', $params['es_adicional_tc_master']);  
    } 
    if (isset($params['es_adicional_tc_master_latam']) && $params['es_adicional_tc_master_latam'] !== ''){	
      $query->condition('r.es_adicional_tc_master_latam', $params['es_adicional_tc_master_latam']);  
    } 
    if (isset($params['es_adicional_tc_master_nolatam']) && $params['es_adicional_tc_master_nolatam'] !== ''){	
      $query->condition('r.es_adicional_tc_master_nolatam', $params['es_adicional_tc_master_nolatam']);  
    } 
    if (isset($params['es_ps']) && $params['es_ps'] !== ''){	
      $query->condition('r.es_ps', $params['es_ps']);  
    }
  
    // Execute query
    $result = $query->execute();
    
    $rows = array();
    
    while($data = $result->fetchObject()){
      $rows[] = array(
        $data->id_fgo_segmentacion,
        $data->cliente,
        $data->segmento_cod,
        $data->segmento_desc,
        $data->es_cliente_actividad,
        $data->tiene_ca_ars_en_actividad, 
        $data->tiene_ca_usd_en_actividad, 
        $data->tiene_cc_ars_en_actividad, 
        $data->tiene_cc_usd_en_actividad, 
        $data->tiene_tc_visa_en_actividad, 
        $data->tiene_tc_master_en_actividad, 
        $data->tiene_pp_en_actividad, 
        $data->es_titular_tc_visa, 
        $data->es_titular_tc_visa_latam, 
        $data->es_titular_tc_visa_nolatam, 
        $data->es_titular_tc_master, 
        $data->es_titular_tc_master_latam, 
        $data->es_titular_tc_master_nolatam,   
        $data->es_adicional_tc_visa, 
        $data->es_adicional_tc_visa_latam, 
        $data->es_adicional_tc_visa_nolatam, 
        $data->es_adicional_tc_master, 
        $data->es_adicional_tc_master_latam, 
        $data->es_adicional_tc_master_nolatam, 
        $data->es_ps,
        $data->business_area_cd
      );
    }  
  
    return $rows;
  }
}