<?php

namespace Fgo\Dao;

abstract class GeneralDao extends \Singleton {

  abstract protected function obtenerNombreTabla();
  abstract protected function obtenerNombreCampoClave();
  abstract protected function obtenerMapeoCampos($objetoNegocio);

  protected function obtenerMapeoRel($objetoNegocio) {
    return false;
  }

  public function guardarRelaciones($objetoNegocio) {
    // Borro las relaciones
    $query = db_delete($this->obtenerNombreTabla());
    $condiciones = $this->obtenerMapeoRel($objetoNegocio);
    foreach ($condiciones as $key => $value) {
      $query->condition($key, $value);
    }
    $query->execute();

    $fields = $this->obtenerRelaciones($objetoNegocio);
    if(!empty($fields)) {
      $query = db_insert($this->obtenerNombreTabla());
      $query->fields(array_keys($fields[0]));
      foreach ($fields as $field) {
        $query->values(array_values($field));
      }
      $query->execute();
    }

  }

  public function guardar(&$objetoNegocio) {
    $last_id = false;
    $fields = $this->obtenerMapeoCampos($objetoNegocio);

    if(!$fields[$this->obtenerNombreCampoClave()]) {
      unset($fields[$this->obtenerNombreCampoClave()]);
    }

    $field_conditions = array();
    $field_values = array();
    foreach ($fields as $field_name => $field_value) {
      $field_conditions[] = $field_name . " = :" . $field_name;
      $field_values[":" . $field_name] = $field_value;
    }

    $sql = "INSERT INTO " . $this->obtenerNombreTabla() . " SET " . implode(",", $field_conditions) . " ON DUPLICATE KEY UPDATE " . implode(",", $field_conditions);

    $status = db_query($sql, $field_values);
    $last_id = $status->dbh->lastInsertId();

    if($last_id < 1) {
      $last_id = $fields[$this->obtenerNombreCampoClave()];
    }

    // Guardo el id en el objeto
    $id_name = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $this->obtenerNombreCampoClave()))));

    $objetoNegocio->$id_name = $last_id;

    return $last_id;
  }

  public function eliminar($objetoNegocio) {
    $fields = $this->obtenerMapeoCampos($objetoNegocio);
    $id = $fields[$this->obtenerNombreCampoClave()];

    $query = db_delete($this->obtenerNombreTabla());
    $query->condition($this->obtenerNombreCampoClave(), $id);
    $query->execute();
  }

  public function eliminarRelaciones($valor) {
    $query = db_delete($this->obtenerNombreTabla());
    $query->condition($this->obtenerNombreCampoRel(), $id);
    $query->execute();
  }

  public function eliminarPorId($id) {
    $query = db_delete($this->obtenerNombreTabla());
    $query->condition($this->obtenerNombreCampoClave(), $id);
    $query->execute();
  }

  public function listar($order = null) {
    $query = db_select($this->obtenerNombreTabla(), 'c');
    $query->fields('c');
    if($order) {
      foreach ($order as $column => $value) {
        $query->orderBy($column, $value);
      }
    }
    $result = $query->execute()->fetchAll();
    return $result;
  }

  public function buscarPorId($id) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');

    $query->condition($this->obtenerNombreCampoClave(), $id);

    $result = $query->execute()->fetchObject();
    return $result;
  }

  /**
   * Ejemplo
   * $conditions['fecha_desde'] = array('value' => 1521753267, 'operator' => '>')
   * $conditions['fecha_hasta'] = array('value' => 1521763267, 'operator' => '<')
   */
  public function filtrar($conditions, $order = null) {
    $query = db_select($this->obtenerNombreTabla(), 'c');
    $query->fields('c');
    foreach ($conditions as $key => $value) {
      $query->condition($key, $value['value'], $value['operator']);
    }
    if ($order) {
      foreach ($order as $column => $value) {
        $query->orderBy($column, $value);
      }
    }
    $result = $query->execute()->fetchAll();
    return $result;
  }
}
