<?php

namespace Fgo\Dao;
use PDO;
class UsuarioZonaDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_usuario_zona';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_usuario_zona';
  }

  protected function obtenerNombreCampoClaveZona() {
    return 'id_zona';
  }

  protected function obtenerNombreCampoClaveUsuario() {
    return 'uid';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_usuario_zona' => $objetoNegocio->idUsuarioZona,
      'uid' => $objetoNegocio->idUsuario,      
      'id_zona' => ($objetoNegocio->getZona() != null) ?
                  $objetoNegocio->getZona()->idZona : null,
    );
  }

  public function filtrarPorIdUsuario($idUsuario) {
    $conditions = array("uid" => array("value" => $idUsuario, "operator" => "="));
    return $this->filtrar($conditions);
  }

}