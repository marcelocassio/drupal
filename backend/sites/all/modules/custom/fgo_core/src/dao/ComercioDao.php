<?php

namespace Fgo\Dao;

class ComercioDao extends GeneralDao {

  protected function obtenerNombreTabla()
  {
    return 'fgo_comercio';
  }

  protected function obtenerNombreCampoClave()
  {
    return 'id_comercio';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array (
      'id_comercio' => $objetoNegocio->idComercio,
      'id_comercio_suip' => $objetoNegocio->idComercioSuip,
      'nombre' => $objetoNegocio->nombre,
      'avvio' => $objetoNegocio->avvio,
      'id_sucursal_fisica' => ($objetoNegocio->getSucursalFisica() != null) ?
                  $objetoNegocio->getSucursalFisica()->idSucursalFisica : null,
    );
  }

  public function filtrarPorIdComercioSuip($idComercioSuip) {
    $condicion['id_comercio_suip'] = array('value' => $idComercioSuip, 'operator' => '=');
    return $this->filtrar($condicion);
  }

  public function filtrarConcidenciasPorNombre($nombre) {
    $result = db_select('fgo_comercio', 'c')
      ->fields('c')
      ->condition('c.nombre',  '%'. db_like($nombre) . '%', 'LIKE')
      ->execute()->fetchAll();
    return $result;
  }


}
