<?php
namespace Fgo\Dao;

class GoShopUsuarioDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_goshopusuario';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_goshopusuario';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_goshopusuario' => $objetoNegocio->idGoshopusuario,
      'user' => $objetoNegocio->user,
      'pass' => $objetoNegocio->pass,
      'token' => $objetoNegocio->token,
      'modified' => $objetoNegocio->modified
    );
  }

  public function buscarPorUserPass($user, $password){
    $condicion['user'] = array('value' => $user, 'operator' => '=');
    $condicion['pass'] = array('value' => $password, 'operator' => '=');
    return $this->filtrar($condicion);
  } 

  public function buscarPorToken($tkn, $date){
    $condicion['token'] = array('value' => $tkn, 'operator' => '=');
    $condicion['modified'] = array('value' => $date, 'operator' => '>');
    return $this->filtrar($condicion);
  }

}