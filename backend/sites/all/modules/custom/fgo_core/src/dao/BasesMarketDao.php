<?php
namespace Fgo\Dao;

class BasesMarketDao extends GeneralDao
{
    protected function obtenerNombreTabla()
    {
        return 'fgo_bases_market';
    }

    protected function obtenerNombreCampoClave()
    {
        return 'id_bases_market';
    }

    protected function obtenerMapeoCampos($objetoNegocio)
    {
        return array(
            'id_bases_market' => $objetoNegocio->idBasesMarket,
            'texto' => $objetoNegocio->texto
        );
    }

    public function obtenerRegistroActual()
    {
        $query  = db_select($this->obtenerNombreTabla(), 'basesMarket')
                  ->fields('basesMarket');
        $data   = $query->execute()->fetchAll();
        $result = array_shift($data);
        $total_registros = $query->countQuery()->execute()->fetchField();
       
        if ($total_registros == 0) {
            $objetoNegocio = (object) array(
                'texto' => null
            );
            $insert_id = $this->guardar($objetoNegocio);
            $result = $this->buscarPorId($insert_id);
        }

        return $result;
    }
}
