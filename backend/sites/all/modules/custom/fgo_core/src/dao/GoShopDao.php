<?php

namespace Fgo\Dao;

class GoShopDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_goshop';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_goshop';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {

    return array(
      'id_goshop' => $objetoNegocio->id_goshop,
      'id_pedido'=> $objetoNegocio->id_pedido,
      'dni_cliente'=> $objetoNegocio->dni_cliente,
      'nombre'=> $objetoNegocio->nombre,
      'apellido'=> $objetoNegocio->apellido,
      'email'=> $objetoNegocio->email,
      'referencia_pedido'=> $objetoNegocio->referencia_pedido,
      'estado_pedido'=> $objetoNegocio->estado_pedido,
      'cuotas'=> $objetoNegocio->cuotas,
      'items' => $objetoNegocio->items,
      'total_financing' => $objetoNegocio->total_financing,
      'importe_envio'=> $objetoNegocio->importe_envio,
      'importe_productos'=> $objetoNegocio->importe_productos,
      'importe_total'=> $objetoNegocio->importe_total,
      'metodo_entrega'=> $objetoNegocio->metodo_entrega,
      'tiempo_entrega'=> $objetoNegocio->tiempo_entrega,
      'seller'=> $objetoNegocio->seller,
      'seller_email'=> $objetoNegocio->seller_email,
      'seller_phone'=> $objetoNegocio->seller_phone,
      'seller_address'=> $objetoNegocio->seller_address,
      'seller_horario'=> $objetoNegocio->seller_horario,
      'seller_image'=> $objetoNegocio->seller_image,
      'medio_pago'=> $objetoNegocio->medio_pago,
      'direccion_entrega'=> $objetoNegocio->direccion_entrega,
      'direccion_facturacion'=> $objetoNegocio->direccion_facturacion
    );
  }

  public function buscarPorIdPedido($idPedido){
    $condicion['id_pedido'] = array('value' => $idPedido, 'operator' => '=');
    return $this->filtrar($condicion);
  }
}