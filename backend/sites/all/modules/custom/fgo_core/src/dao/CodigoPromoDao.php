<?php

namespace Fgo\Dao;

class CodigoPromoDao extends GeneralDao {

  protected function obtenerNombreTabla()
  {
    return 'fgo_cupon_codigo_promo';
  }

  protected function obtenerNombreCampoClave()
  {
    return 'id_codigo_promo';
  }

  public function buscarPorCodigoPromo($codigo_promo) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');

    $query->condition('codigo_promo', $codigo_promo);

    $result = $query->execute()->fetchObject();
    return $result;
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array (
      'id_codigo_promo' => $objetoNegocio->idCodigoPromo,
      'codigo_promo' => $objetoNegocio->codigoPromo,
      'enviado_a_visa' => $objetoNegocio->enviadoAVisa,
    );
  }

}