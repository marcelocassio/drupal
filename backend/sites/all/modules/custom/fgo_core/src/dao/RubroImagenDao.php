<?php

namespace Fgo\Dao;

class RubroImagenDao extends GeneralDao {

  protected function obtenerNombreTabla()  {
    return "fgo_rubro_imagen";
  }

  protected function obtenerNombreCampoClave() {
    return "id_rubro_imagen";
  }

  protected function obtenerMapeoRel($objetoNegocio) {
    return array("id_rubro" => $objetoNegocio->idRubro);
  }

  protected function obtenerRelaciones($objetoNegocio) {
    $fields = array();
    if(!empty($objetoNegocio->data)) {
      foreach ($objetoNegocio->data as $value) {
        $fields[] = array(
          "id_rubro" => $objetoNegocio->idRubro,
          "id_imagen" => $value->idImagen
        );
      }
    }
    return $fields;
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_rubro_imagen' => $objetoNegocio->idRubroImagen,
      'id_rubro' => $objetoNegocio->idRubro,
      'imagenes' => $objetoNegocio->imagenes,
    );
  }

  public function filtrarPorIdRubro($idRubro) {
    $condicion['id_rubro'] = array('value' => $idRubro, 'operator' => '=');
    return $this->filtrar($condicion);
  }

  public function guardar(&$objetoNegocio) {
     parent::guardarRelaciones();
     return true;
  }

}