<?php

namespace Fgo\Dao;
use PDO;

class ComercioImagenDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return "fgo_comercio_imagen";
  }

  protected function obtenerNombreCampoClave() {
    return "id_comercio_imagen";
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      "id_comercio_imagen" => $objetoNegocio->idComercioImagen,
      "id_comercio" => $objetoNegocio->idComercio,
      "id_imagen" => $objetoNegocio->idImagen,
    );
  }

  public function filtrarPorIdComercio($idComercio) {
    $condiciones["id_comercio"] = array('value' => $idComercio, 'operator' => '=');
    return $this->filtrar($condiciones);
  }

  public function listarComercioImagenIds() {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c', array("id_comercio", "id_imagen"));
    $query = $query->orderBy('c.id_imagen', 'ASC');
    $result = $query->execute()->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_COLUMN);
    return $result;
  }

  public function listarComercioComercioImagen($idComercio) {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c', array("id_comercio_imagen","id_comercio", "id_imagen"));
    $query->condition("id_comercio", $idComercio);
    $result = $query->execute()->fetchObject();
    return $result;
  }


  /***
   * Elimina todas las relaciones en base al id de comercio
   * @param $idComercio
   * @return int cantidad de filas eliminadas
   */
  public function eliminarPorIdComercio($idComercio) {
    $num_deleted = db_delete($this->obtenerNombreTabla())
      ->condition('id_comercio', $idComercio)
      ->execute();
    return $num_deleted;
  }
}