<?php
namespace Fgo\Dao;
use PDO;

class ComunicacionImagenDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return "fgo_comunicacion_imagen";
  }

  protected function obtenerNombreCampoClave() {
    return "id_comunicacion_imagen";
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_comunicacion_imagen' => $objetoNegocio->idComunicacionImagen,
      'id_comunicacion' => $objetoNegocio->idComunicacion,
      'id_imagen' => $objetoNegocio->idImagen,
    );
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $conditions = array("id_comunicacion" => array("value" => $idComunicacion, "operator" => "="));
    return $this->filtrar($conditions);
  }

  public function listarComunicacionImagenIds() {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c', array("id_comunicacion", "id_imagen"));
    $result = $query->execute()->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_COLUMN);
    return $result;
  }

  public function listarComunicacionComunicacionImagen($idComunicacion) {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->fields('c', array("id_comunicacion_imagen","id_comunicacion", "id_imagen"));
    $query->condition("id_comunicacion", $idComunicacion);
    $result = $query->execute()->fetchObject();
    return $result;
  }

  /***
   * Elimina todas las relaciones en base al id de la comunicacion
   * @param $idComunicacion
   * @return int cantidad de filas eliminadas
   */
  public function eliminarPorIdComunicacion($idComunicacion) {
    $num_deleted = db_delete($this->obtenerNombreTabla())
      ->condition('id_comunicacion', $idComunicacion)
      ->execute();
    return $num_deleted;
  }
}