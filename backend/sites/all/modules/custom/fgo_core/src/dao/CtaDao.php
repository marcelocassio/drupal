<?php
namespace Fgo\Dao;

class CtaDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_cta';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_cta';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_cta' => $objetoNegocio->idCta,
      'param1' => $objetoNegocio->param1,
      'param2' => $objetoNegocio->param2,
    );
  }
}