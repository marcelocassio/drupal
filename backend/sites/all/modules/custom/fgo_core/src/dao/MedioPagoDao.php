<?php
namespace Fgo\Dao;

use Fgo\Bo\MedioPagoBo;

class MedioPagoDao extends GeneralDao{

  protected function obtenerNombreTabla() {
    return "fgo_medio_pago";
  }

  protected function obtenerNombreCampoClave() {
    return 'id_medio_pago';
  }

  protected function obtenerMapeoCampos($medioPago) {
    return array(
      'id_medio_pago' => $medioPago->idMedioPago,
      'nombre' => $medioPago->nombre,
      'tipo_medio_pago' => $medioPago->tipoMedioPago,
      'id_tarjeta_suip' => $medioPago->idTarjetaSuip,
    );
  }

  protected function construirObjetoNegocio($registro) {
    $medioPago = new MedioPagoBo();
    $medioPago->contruirDesdeRegistro($registro);
    return $medioPago;
  }

  public function listarPorIdBeneficio($idBeneficio) {

    $query = db_select("fgo_medio_pago", "c");
    $query->innerJoin('fgo_beneficio_medio_pago', 'bmp', 'c.id_medio_pago = bmp.id_medio_pago');
    $query->fields('c')->condition('bmp.id_beneficio', $idBeneficio);
    $result = $query->execute()->fetchAll();

    $listadoMedioPagos = array();
    foreach ($result as $key => $registro) {
      $listadoMedioPagos[] = $this->construirObjetoNegocio($registro);
    }

    return $listadoMedioPagos;
  }

}
