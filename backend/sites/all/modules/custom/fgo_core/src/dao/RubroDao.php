<?php

namespace Fgo\Dao;

class RubroDao extends GeneralDao {
  protected function obtenerNombreTabla() {
    return "fgo_rubro";
  }

  protected function obtenerNombreCampoClave() {
    return "id_rubro";
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    $mapeo = array(
      "id_rubro_suip" => $objetoNegocio->idRubroSuip,
      "nombre" => $objetoNegocio->nombre,
      "id_rubro_padre" => ($objetoNegocio->rubroPadre != null) ? $objetoNegocio->rubroPadre->idRubro: null,
      "estado_pdm" => $objetoNegocio->estadoPdm,
    );

    return $mapeo;
  }


  public function filtrarPorIdRubroSuip($idRubroSuip) {
    $condicion['id_rubro_suip'] = array('value' => $idRubroSuip, 'operator' => '=');
    return $this->filtrar($condicion);
  }

  public function filtrarPorEstadoPdmNoEn($estadoPdm) {
    $condicion['estado_pdm'] = array('value' => array($estadoPdm), 'operator' => 'NOT IN');
    return $this->filtrar($condicion);
  }
}