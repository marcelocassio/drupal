<?php
namespace Fgo\Dao;

class CarruselDao extends GeneralDao
{
    protected function obtenerNombreTabla()
    {
        return 'fgo_carousel_home';
    }

    protected function obtenerNombreCampoClave()
    {
        return 'id_carousel_home';
    }

    protected function obtenerMapeoCampos($objetoNegocio)
    {
        return array(
            'id_carousel_home' => $objetoNegocio->idCarruselHome,
            'nombre' => $objetoNegocio->nombre,
            'id_rubro' => $objetoNegocio->idRubro,
            'publicado' => $objetoNegocio->publicado,
        );
    }
}
