<?php

namespace Fgo\Dao;
use PDO;
class ComunicacionRubroDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_comunicacion_rubro';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_comunicacion_rubro';
  }

  protected function obtenerNombreCampoClaveRubro() {
    return 'id_rubro';
  }

  protected function obtenerNombreCampoClaveComunicacion() {
    return 'id_comunicacion';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_comunicacion_rubro' => $objetoNegocio->idComunicacionRubro,
      'id_comunicacion' => $objetoNegocio->idComunicacion,
      'id_rubro' => $objetoNegocio->idRubro,
    );
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $conditions = array("id_comunicacion" => array("value" => $idComunicacion, "operator" => "="));
    return $this->filtrar($conditions);
  }

  public function listarComunicacionRubroOrden($fechaDesde, $fechaHasta) {
    $query = db_select($this->obtenerNombreTabla(), "c");
    $query->innerJoin('fgo_comunicacion', 'fc', 'fc.id_comunicacion = c.id_comunicacion');
    $query->fields('c', array("id_comunicacion", "id_rubro"));
    $query->condition('fecha_desde', strtotime($fechaDesde), '<=');
    $query->condition('fecha_hasta', strtotime($fechaHasta), '>=');
    $query = $query->orderBy('fc.peso', 'DESC');
    $result = $query->execute()->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_COLUMN);
    return $result;
  }

}