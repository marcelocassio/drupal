<?php
namespace Fgo\Dao;

class CtaRelacionDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_cta_relacion';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_cta_relacion';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_cta_relacion' => $objetoNegocio->idCtaRelacion,
      'id_cta' => $objetoNegocio->cta->idCta,
      'id_entidad' => $objetoNegocio->idEntidad,
      'tipo_entidad' => $objetoNegocio->tipoEntidad
    );
  }

  public function filtrarPorIdEntidad($idEntidad, $tipoEntidad) {
    $condicion['id_entidad'] = array('value' => $idEntidad, 'operator' => '=');
    $condicion['tipo_entidad'] = array('value' => $tipoEntidad, 'operator' => '=');
    return $this->filtrar($condicion);
  }
}