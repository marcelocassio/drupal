<?php

namespace Fgo\Dao;

class SlideDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_slide';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_slide';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    
    if(isset($objetoNegocio)){
      return array(
        'id_slide' => $objetoNegocio->idSlide,
        'fid_web' => $objetoNegocio->imagenWeb != null ? $objetoNegocio->imagenWeb->datosImagen->fid: null,
        'fid_app' => $objetoNegocio->imagenApp != null ? $objetoNegocio->imagenApp->datosImagen->fid: null,
        'id_cta' => $objetoNegocio->cta->idCta != null ? $objetoNegocio->cta->idCta: null,
        'fecha_desde' => $objetoNegocio->fechaDesde,
        'fecha_hasta' => $objetoNegocio->fechaHasta,
        'publicado' => $objetoNegocio->publicado,
      );
    }

    return null;

  }
}