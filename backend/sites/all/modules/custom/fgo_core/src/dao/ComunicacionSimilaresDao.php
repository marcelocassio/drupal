<?php
namespace Fgo\Dao;

class ComunicacionSimilaresDao extends GeneralDao {

  protected function obtenerNombreTabla(){
    return 'fgo_comunicacion_similares';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_comunicacion_similar';
  }

  protected function obtenerNombreCampoClaveComunicacion(){
    return 'id_comunicacion';
  }

  protected function obtenerNombreCampoClaveSimilar(){
    return 'id_similar';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_comunicacion_similar' => $objetoNegocio->idComunicacionSimilar,
      'id_comunicacion' => $objetoNegocio->idComunicacion,
      'id_similar' => $objetoNegocio->idSimilar
    );
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $condiciones['id_comunicacion'] = array("value" => $idComunicacion, "operator" => "=");
    return $this->filtrar($condiciones);
  }

  public function borrarComunicacionSimilares($id_comunicacion) {
    if ($id_comunicacion){
      db_delete($this->obtenerNombreTabla())
        ->condition($this->obtenerNombreCampoClaveComunicacion(), $id_comunicacion)
        ->execute();
    }
  }
}