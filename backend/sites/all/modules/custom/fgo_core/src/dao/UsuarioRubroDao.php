<?php

namespace Fgo\Dao;
use PDO;
class UsuarioRubroDao extends GeneralDao
{
  protected function obtenerNombreTabla() {
    return 'fgo_usuario_rubro';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_usuario_rubro';
  }

  protected function obtenerNombreCampoClaveRubro() {
    return 'id_rubro';
  }

  protected function obtenerNombreCampoClaveUsuario() {
    return 'uid';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_usuario_rubro' => $objetoNegocio->idUsuarioRubro,
      'uid' => $objetoNegocio->idUsuario,
      'id_rubro' => ($objetoNegocio->getRubro() != null) ?
      $objetoNegocio->getRubro()->idRubro : null,
    );
  }

  public function filtrarPorIdUsuario($idUsuario) {
    $conditions = array("uid" => array("value" => $idUsuario, "operator" => "="));
    return $this->filtrar($conditions);
  }

}