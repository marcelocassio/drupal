<?php

namespace Fgo\Dao;


class BeneficioDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return "fgo_beneficio";
  }

  protected function obtenerNombreCampoClave() {
    return "id_beneficio";
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_beneficio' => $objetoNegocio->idBeneficio,
      'id_comunicacion' => $objetoNegocio->idComunicacion,
      'tipo_beneficio' => $objetoNegocio->tipoBeneficio,
      'id_tipo_aplicacion_beneficio' => ($objetoNegocio->tipoAplicacionBeneficio != NULL) ? $objetoNegocio->tipoAplicacionBeneficio->idTipoAplicacionBeneficio : NULL,
      'valor' => $objetoNegocio->valor,
      'cuota' => $objetoNegocio->cuota,
      'tope' => $objetoNegocio->tope,
      'id_casuistica' => ($objetoNegocio->casuistica != null) ? $objetoNegocio->casuistica->idCasuistica : null,
    );
  }

  public function filtrarPorIdComunicacion($idComunicacion) {
    $condiciones['id_comunicacion'] = array('value' => $idComunicacion, 'operator' => '=');
    return $this->filtrar($condiciones);
  }

}