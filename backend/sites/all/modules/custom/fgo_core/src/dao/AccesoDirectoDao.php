<?php

namespace Fgo\Dao;

class AccesoDirectoDao  extends GeneralDao {

  protected function obtenerNombreTabla() {
    return "fgo_acceso_directo";
  }

  protected function obtenerNombreCampoClave() {
    return 'id_acceso_directo';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array(
      'id_acceso_directo' => $objetoNegocio->idAccesoDirecto,
      'tipo' => $objetoNegocio->tipo,
      'nombre' => $objetoNegocio->nombre,
      'id_cta' => ($objetoNegocio->cta != NULL) ? $objetoNegocio->cta->idCta : NULL,
      'orden' => $objetoNegocio->orden,
      'visible' => $objetoNegocio->visible,
    );
  }

  public function obtenerAccesosDirectos() {
    $query = db_select($this->obtenerNombreTabla(), 'c');
    $query->fields('c');
    $query->orderBy('c.orden', 'ASC');

    $result = $query->execute()->fetchAll();
    return $result;
  }
}