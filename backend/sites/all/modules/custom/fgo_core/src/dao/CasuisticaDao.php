<?php

namespace Fgo\Dao;

class CasuisticaDao extends GeneralDao {

  protected function obtenerNombreTabla() {
    return 'fgo_casuistica';
  }

  protected function obtenerNombreCampoClave() {
    return 'id_casuistica';
  }

  protected function obtenerMapeoCampos($objetoNegocio) {
    return array (
      'id_casuistica' => $objetoNegocio->idCasuistica,
      'id_casuistica_suip' => $objetoNegocio->idCasuisticaSuip,
      'descripcion' => $objetoNegocio->descripcion,
    );
  }

  public function filtrarPorIdCasuisticaSuip($idCasuisticaSuip) {
    $condicion['id_casuistica_suip'] = array('value' => $idCasuisticaSuip, 'operator' => '=');
    return $this->filtrar($condicion);
  }

}
