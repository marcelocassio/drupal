<?php
namespace Fgo\Dao;

class SucursalVirtualDao extends GeneralDao {

  protected function obtenerNombreTabla(){
    return 'fgo_sucursal_virtual';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_sucursal_virtual';
  }

  protected function obtenerNombreCampoClaveSuip(){
    return 'id_sucursal_virtual_suip';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_sucursal_virtual' => $objetoNegocio->idSucursalVirtual,
      'id_sucursal_virtual_suip' => $objetoNegocio->idSucursalVirtualSuip,
      'web' => $objetoNegocio->web,
      'telefono' => $objetoNegocio->tlf
    );
  }

  public function listarPorIdComunicacion($idComunicacion) {

    $query = db_select("fgo_sucursal_virtual", "c");
    $query->innerJoin('fgo_comunicacion_sucursal_virtual', 'fcsf', 'c.id_sucursal_virtual = fcsf.id_sucursal_virtual');
    $query->fields('c')->condition('fcsf.id_comunicacion', $idComunicacion);
    $result = $query->execute()->fetchAll();

    return $result;
  }

  public function buscarPorIdSuip($id) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');

    $query->condition($this->obtenerNombreCampoClaveSuip(), $id);

    $result = $query->execute()->fetchObject();
    return $result;
  }
}