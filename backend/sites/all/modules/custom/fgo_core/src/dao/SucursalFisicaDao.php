<?php

namespace Fgo\Dao;

class SucursalFisicaDao extends GeneralDao {


  protected function obtenerNombreTabla(){
    return 'fgo_sucursal_fisica';
  }

  protected function obtenerNombreCampoClave(){
    return 'id_sucursal_fisica';
  }

  protected function obtenerNombreCampoClaveSuip(){
    return 'id_sucursal_fisica_suip';
  }

  protected function obtenerMapeoCampos($objetoNegocio){
    return array(
      'id_sucursal_fisica' => $objetoNegocio->idSucursalFisica,
      'id_sucursal_fisica_suip' => $objetoNegocio->idSucursalFisicaSuip,
      'nombre' => $objetoNegocio->nombre,
      'calle' => $objetoNegocio->calle,
      'calle_numero' => $objetoNegocio->calleNumero,
      'id_localidad' => $objetoNegocio->localidad->idLocalidad,
      'id_provincia' => $objetoNegocio->provincia->idProvincia,
      'latitud' => $objetoNegocio->latitud,
      'longitud' => $objetoNegocio->longitud
    );
  }

  public function listarPorIdComunicacion($idComunicacion) {

    $query = db_select("fgo_sucursal_fisica", "c");
    $query->innerJoin('fgo_comunicacion_sucursal_fisica', 'fcsf', 'c.id_sucursal_fisica = fcsf.id_sucursal_fisica');
    $query->fields('c')->condition('fcsf.id_comunicacion', $idComunicacion);
    $result = $query->execute()->fetchAll();

    return $result;
  }

  /***
   * Recupera los registras en base al listado de id de sucursales suip
   * @param $listadoIdSucursalesSuip array()
   * @return mixed
   */
  public function filtrarListadoIdSuip($listadoIdSucursalesSuip) {
    $condiciones['id_sucursal_fisica_suip'] = array("value" => $listadoIdSucursalesSuip, "operator" => "in");
    return $this->filtrar($condiciones);
  }

  public function buscarPorIdSuip($id) {
    $query = db_select($this->obtenerNombreTabla(), 'c')
      ->fields('c');

    $query->condition($this->obtenerNombreCampoClaveSuip(), $id);

    $result = $query->execute()->fetchObject();
    return $result;
  }
}