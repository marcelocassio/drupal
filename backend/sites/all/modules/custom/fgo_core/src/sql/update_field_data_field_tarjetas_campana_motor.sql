-- AE1-241  fix link solicitud tarjeta
-- SELECT * FROM field_data_field_tarjetas_campana_motor;
UPDATE field_data_field_tarjetas_campana_motor SET field_tarjetas_campana_motor_value = "solicita-tarjeta-xeneize" WHERE field_tarjetas_campana_motor_value = "ofertaBoca";
UPDATE field_data_field_tarjetas_campana_motor SET field_tarjetas_campana_motor_value = "solicita-tarjeta-river" WHERE field_tarjetas_campana_motor_value = "ofertaRiver";
UPDATE field_data_field_tarjetas_campana_motor SET field_tarjetas_campana_motor_value = "solicita-tarjeta" WHERE field_tarjetas_campana_motor_value = "ofertasDigitales";