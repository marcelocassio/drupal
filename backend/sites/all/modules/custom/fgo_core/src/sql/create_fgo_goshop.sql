CREATE TABLE fgo_goshop(
	id_goshop int(10) AUTO_INCREMENT,
	id_pedido varchar(255),
	dni_cliente varchar(255),
	nombre varchar(255),
	apellido varchar(255),
	email varchar(255),
	referencia_pedido varchar(255),
	estado_pedido varchar(255),
	cuotas varchar(255),
	items varchar(255),
	total_financing varchar(255),
	importe_envio varchar(255),
	importe_productos varchar(255),
	importe_total varchar(255),
	metodo_entrega varchar(255),
	tiempo_entrega varchar(255),
	seller varchar(255),
	seller_email varchar(255),
	seller_phone varchar(255),
	seller_address varchar(255),
	seller_horario varchar(255),
	seller_image varchar(255),
	medio_pago varchar(255),
	direccion_entrega blob,
	direccion_facturacion blob,
   	PRIMARY KEY(id_goshop)
);

CREATE TABLE fgo_goshop_item(
	id_goshop_items int(10) AUTO_INCREMENT,
	id_item varchar(255),
	titulo_item varchar(255),
	importe_base_item varchar(255),
	importe_final_item varchar(255),
	PRIMARY KEY(id_goshop_items)
);

