<?php
/*
 * La documentación del componente para crear tablas esta aquí
 * https://github.com/xaksis/vue-good-table
 *
 * Buscar en esta dirección las dependencias que se quieran usar y bajarlas local:
 * https://cdn.jsdelivr.net/npm/vue-particles
 * O
 * https://unpkg.com/vue-particles@1.0.9/src/vue-particles/index.js
 *
 * */
global $base_url;
$path_backend = drupal_get_path('module', 'fgo_backend');
$path_backend_ate = drupal_get_path('module', 'fgo_backend_atencion_cli');

//Principal VueJS
drupal_add_js( $path_backend . '/js/vue.min.js');

// Dependencia para hacer llamadas http //AXIOS
drupal_add_js($path_backend . '/js/axios_0_18_0.min.js');

//Dependencias para tablas
drupal_add_js($path_backend . '/js/vue-good-table.js');
drupal_add_css($path_backend . '/css/vue-good-table.min.css');

// Bootstrap 4
drupal_add_css($path_backend . '/css/bootstrap_4_1_1.min.css');

// Bootstrap Vue
drupal_add_js($path_backend . '/js/bootstrap-vue.js');

// Font Awesome
drupal_add_css($path_backend . '/css/font_awesome_4_7.min.css');

// Depedndencia del modal
drupal_add_js($path_backend . '/js/vuedals.min.js');

//Modals (Pop-Up Estados)
drupal_add_js($path_backend . '/js/modals.js');

//Custom
drupal_add_css($path_backend .'/css/fgo_backend_default.css');
drupal_add_css($path_backend_ate .'/css/fgo_backend_atencion_cli.css');

?>
<div id="container-particles-js">
    <div id="particles-js"></div>
</div>
<div id="admin_container">
    <h1>Administraci&oacute;n de Atenci&oacute;n de Clientes</h1>
    <div v-if="loading" class="spinner_backend" v-cloak>
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
    <vue-good-table v-else
                    :columns="formattedColumns"
                    :rows="communications"
                    style-class="vgt-table striped condensed"
                    :pagination-options="{ nextLabel: 'Siguiente', prevLabel: 'Anterior', enabled: true, perPage: 40, ofLabel: 'de'}"
                    :search-options="{ enabled: true, placeholder: 'Buscar cualquier dato', trigger: 'enter'}">
        <div v-slot:emptystate>
            No hay comunicaciones con ese criterio
        </div>
        <template v-slot:table-row="props">
            <div v-if="props.column.field == 'botones'">
                <b-button v-on:click="openSucursales(props.row.idComunicacion)" variant="primary" class="btn btn-outline-info btn-md">Ver Sucursales</b-button>
            </div>
            <div v-else-if="props.column.field == 'botonLegales'">
                <b-button @click="openLegales(props.row.idComunicacion)" variant="primary" class="btn btn-outline-info btn-md">Ver Legales</b-button>
            </div>
            <div v-else>
                {{props.formattedRow[props.column.field]}}
            </div>
        </template>
    </vue-good-table>
    <vuedals></vuedals>
</div>
<script src="<?=$base_url?>/<?=$path_backend?>/js/particles.min.js"></script>
<script>
    const link = 'https://' + window.location.host + '/fgo/API/v1/backend/comunicaciones?ate=1';
    const linkVerSucursales = 'https://' + window.location.host + '/fgo/API/v1/backend/comunicacion/';

    // Contantes para el modal
    const Bus = Vuedals.Bus;
    const Component = Vuedals.Component;
    const Plugin = Vuedals.default;

    Vue.use(Plugin);

    new Vue({
        el: '#admin_container',
        components: {
            vuedals: Component
        },
        data: {
            communications: [],
            loading: false,
            comunicacionMarcasShopping: null,
            comunicacionZonas: null,
            comunicacionSucursales: null,
            comunicacionWeb: null,
            comunicacionTelefono: null,
            comunicacionResumido: null,
            tituloComunicacion: null,
            columns: [
                {
                    label: 'ID',
                    field: 'idComunicacion',
                    type: 'number',
                },{
                    label: 'ID Suip',
                    field: 'idBeneficioSuip',
                    type: 'number',
                },{
                    label: 'Título',
                    field: 'titulo',
                },{
                    label: 'Cuotas',
                    field: 'cuota'
                },{
                    label: 'Descuento',
                    field: 'descuento',
                },{
                    label: 'Casuistica',
                    field: 'casuistica',
                },{
                    label: 'Fecha Desde',
                    field: 'fechaDesde',
                    type: 'date',
                    dateInputFormat: 'DD-MM-YYYY',
                    dateOutputFormat: 'DD-MM-YYYY',
                    width: '90px',
                },{
                    label: 'Fecha Hasta',
                    field: 'fechaHasta',
                    type: 'date',
                    dateInputFormat: 'DD-MM-YYYY',
                    dateOutputFormat: 'DD-MM-YYYY',
                    width: '90px',
                },{
                    label: 'Sucursales',
                    field: 'botones',
                    globalSearchDisabled: true,
                },{
                    label: 'Legales',
                    field: 'botonLegales',
                    globalSearchDisabled: true,
                }
            ]
        },
        methods:{
            getCommunications(){
                this.loading = true;
                axios
                    .get(link)
                    .then(response => {
                        if (response.status === 200 && response.data.code === 0){
                            this.communications = response.data.data;
                        }else{
                            this.communications = null;
                        }
                    })
                    .catch(error => {
                        this.communications = null;
                    })
                    .finally(() => this.loading = false)
            },
            openSucursales(idComunicacion) {
                axios
                    .get(linkVerSucursales + idComunicacion)
                    .then(response => {
                        if (response.status === 200){
                            if (response.data.data.beneficio){
                                this.comunicacionMarcasShopping = response.data.data.beneficio.canales_venta.marcas_adheridas_shopping;
                                this.comunicacionZonas = response.data.data.beneficio.canales_venta.zonas;
                                this.comunicacionSucursales = response.data.data.beneficio.canales_venta.sucursales;
                                this.comunicacionWeb = response.data.data.beneficio.canales_venta.web;
                                this.comunicacionTelefono = response.data.data.beneficio.canales_venta.telefono;
                                this.tituloComunicacion = response.data.data.beneficio.titulo;

                                Bus.$emit('new', {
                                    title: 'Sucursales de ' + this.tituloComunicacion,
                                    component: {
                                        name: 'inside-modal',
                                        template: ' <div class="container">' +
                                        '               <div class="row">' +
                                        '                   <div class="col">' +
                                        '                   <div v-if="this.$parent.$parent.comunicacionMarcasShopping">' +
                                        '                       <div v-if="this.$parent.$parent.comunicacionMarcasShopping.length">' +
                                        '                       Locales Adheridos:' +
                                        '                           <p class="card-text" v-for="marca in this.$parent.$parent.comunicacionMarcasShopping">' +
                                        '                               <strong>{{ marca | capitalize }}</strong>' +
                                        '                           </p>' +
                                        '                       </div><br>' +
                                        '                    </div>' +
                                        '                       <p class="card-text" v-for="zona in this.$parent.$parent.comunicacionZonas">' +
                                        '                           - <strong>{{ zona | capitalize }}</strong></p>' +
                                        '                       <p class="card-text" v-for="sucursal in this.$parent.$parent.comunicacionSucursales">' +
                                        '                           - <strong>{{ sucursal.direccion | capitalize }}</strong> en {{ sucursal.localidad }}</p>' +
                                        '                       <p class="card-text" v-for="web in this.$parent.$parent.comunicacionWeb">' +
                                        '                           - <strong> Web: </strong> <b-link :href="web.url" target="_blank">{{ web.name | capitalize }}</b-link></p>' +
                                        '                       <p class="card-text" v-if="this.$parent.$parent.comunicacionTelefono">' +
                                        '                           - <strong>Teléfono: </strong>{{ this.$parent.$parent.comunicacionTelefono }}</p>' +
                                        '                   </div>' +
                                        '               </div>' +
                                        '           </div>',
                                        filters: {
                                            capitalize(value) {
                                                if (!value) return ''
                                                value = value.toString()
                                                return value.charAt(0).toUpperCase() + value.slice(1)
                                            }
                                        }
                                    }
                                });
                            }else{
                                Bus.$emit('new', {
                                    title: 'Comunicación no vigente',
                                    component: badModal
                                });
                            }
                        }
                    })
                    .catch(error => {
                        Bus.$emit('new', {
                            title: 'No se pueden mostrar las sucursales adheridas',
                            component: badModal
                        });
                    })
            },
            openLegales(idComunicacion) {
                this.comunicacionResumido = this.communications.filter(obj => { return obj.idComunicacion === idComunicacion });
                Bus.$emit('new', {
                    title: 'Legales de ' + this.comunicacionResumido[0].titulo,
                    component: {
                        name: 'inside-modal',
                        template: ' <div class="container">' +
                        '               <div class="row">' +
                        '                   <div class="col">' +
                        '                       <div>' +
                        '                        {{ this.$parent.$parent.comunicacionResumido[0].legales }}   ' +
                        '                       </div>' +
                        '                   </div>' +
                        '               </h2>' +
                        '           </h2>',
                    }
                });
            },
            renderData(communications) {
                communications.map(item => {
                    item.botones = ' <button onclick="this.openSucursales('+item.idComunicacion+')" class="btn btn-outline-info btn-md">Ver Sucursales</button>';
                })
            }
        },
        computed:{
            formattedColumns(){
                if(this.communications){
                    this.renderData(this.communications);
                    return this.columns;
                }
            }
        },
        mounted() {
            this.getCommunications();
        }
    });
    particlesJS.load('particles-js', '<?=$base_url?>/<?=$path_backend?>/js/particles.json', function() {});
</script>
