<?php


define('BBVA_SUBSCRIPTION_CANAL_WEB_VALUE', 'WBPC');
define('BBVA_SUBSCRIPTION_CANAL_APP_VALUE', 'WBSP');

class SuscripcionRequest {

  public $canalSuscripcion;

  public $tipoDeDocumento;
  public $numeroDeDocumento;
  
  public $nacionalidad;

  public $nombre;
  public $apellido;
  public $sexo;
  public $codigoDeArea;
  public $codigoDeOperador;
  public $fechaNacimiento;
  
  public $fechaNacimiento_dia;
  public $fechaNacimiento_mes;
  public $fechaNacimiento_anio;
  
  public $email;
  public $numeroDeCelular;
  public $numeroDeCliente;

  public $rubrosList;
  public $provinciasList;
  public $zonasList;

  public function __toString()
  {
    $rubrosString = "";
	if (!empty($this->rubrosList)) {
		$rubrosString = implode(',', $this->rubrosList);
	}
    $zonasString = "";
	if (!empty($this->zonasList)) {
		$zonasString = implode(',', $this->zonasList);
	}
	$provinciasString = ""; 
	if (!empty($this->provinciasList)) {
		$provinciasString = implode(',', $this->provinciasList);
	}

  return " tipoDeDocumento=". $this->tipoDeDocumento .
		   "| numeroDeDocumento=" . $this->numeroDeDocumento .
       "| nacionalidad=" . $this->nacionalidad .
		   "| nombre=" . $this->nombre .
		   "| apellido=" . $this->apellido .
		   "| sexo=" . $this->sexo .
		   "| codigoDeArea=" . $this->codigoDeArea .
		   "| codigoDeOperador=" . $this->codigoDeOperador .
		   "| fechaNacimiento=" . $this->fechaNacimiento .
       "| canalSuscripcion=" . $this->canalSuscripcion .
		   "| email=" . $this->email .
		   "| numeroDeCelular=" . $this->numeroDeCelular .
		   "| numeroDeCliente=" . $this->numeroDeCliente .
       "| rubrosString=" . $rubrosString .
	   "| provinciasString=" . $provinciasString .
       "| zonasList=" . $zonasString;
  }

  public function establecer_canal_web() {
    $this->canalSuscripcion = BBVA_SUBSCRIPTION_CANAL_WEB_VALUE;
  }

  public function establecer_canal_app() {
    $this->canalSuscripcion = BBVA_SUBSCRIPTION_CANAL_APP_VALUE;
  }
  public function tieneNumeroCliente() {
    return ($this->numeroDeCliente != '0');
  }


}



class NoClienteResponse {
  public $codigo;
  public $descripcion;
  public $numeroDeCliente;
  
  public function __toString()
  {
    return " codigo=". $this->codigo . 
		   "| descripcion=" . $this->descripcion . 
		   "| numeroDeCliente=" . $this->numeroDeCliente;
  }
}

class SuscripcionResponse {
  public $codigoError;
 
  public $transaccionProcesada;
  
  public $tipoDeDocumento;
  public $numeroDeDocumento;
  
  public $nombre;
  public $apellido;
  public $sexo;
  public $codigoDeArea;
  public $codigoDeOperador;
  public $estadoSuscripcion;
  public $fechaNacimiento;
  public $email;
  public $numeroDeCelular;
  public $numeroDeCliente;

  public $rubrosList;
  public $zonasList;
  public $provinciasList;

  public function esta_suscripto() {
    if (strcasecmp($this->estadoSuscripcion, 'SC') == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function tiene_confirmacion_pendiente() {
    if (strcasecmp($this->estadoSuscripcion, 'SPCT') == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function tiene_carga_datos_pendiente() {
    if (strcasecmp($this->estadoSuscripcion, 'SPID') == 0) {
      return true;
    } else {
      return false;
    }
  }  
  
  public function es_no_registrado() {
    if (strcasecmp($this->estadoSuscripcion, 'NR') == 0) {
      return true;
    } else {
      return false;
    }
  }

  public function es_desconocido() {
    if (strcasecmp($this->estadoSuscripcion, 'NN') == 0) {
      return true;
    } else {
      return false;
    }
  }   
  
  
  public function es_inexistente() {
    if (strcasecmp($this->codigoError, SUIP_SUSCRIPTOR_INEXISTENTE) == 0) {
      return true;
    } else {
      return false;
    }
  }
  
  public function get_zonas_provincias_list() {
	$zones = array();
	$provinces = array();
	if (!empty($this->zonasList)) {
		foreach ($this->zonasList as $value) {
			$zones[] = SUFIJO_ZONA . $value;
		}
	}
	if (!empty($this->provinciasList)) {
		foreach ($this->provinciasList as $value) {
			$provinces[] = SUFIJO_PROV . $value;
		}
	}
	
	return array_merge($zones, $provinces);
  }
  
  public function get_rubros_list() {
	$rubros = array();
	foreach ($this->rubrosList as $value) {
		$rubros[] = $value;
	}
	return $rubros;
  }
  
  public function __toString()
  {
    $rubrosString = "";
	if (!empty($this->rubrosList)) {
		$rubrosString = implode(',', $this->rubrosList);
	}
    $zonasString = "";
	if (!empty($this->zonasList)) {
		$zonasString = implode(',', $this->zonasList);
	}
	$provinciasString = ""; 
	if (!empty($this->provinciasList)) {
		$provinciasString = implode(',', $this->provinciasList);
	}
	
    return " tipoDeDocumento=". $this->tipoDeDocumento . 
		   "| numeroDeDocumento=" . $this->numeroDeDocumento . 
		   "| nombre=" . $this->nombre . 
		   "| apellido=" . $this->apellido . 
		   "| sexo=" . $this->sexo . 
		   "| codigoDeArea=" . $this->codigoDeArea . 
		   "| codigoDeOperador=" . $this->codigoDeOperador . 
		   "| fechaNacimiento=" . $this->fechaNacimiento . 
		   "| email=" . $this->email . 
		   "| numeroDeCelular=" . $this->numeroDeCelular . 
		   "| numeroDeCliente=" . $this->numeroDeCliente . 
		   "| estadoSuscripcion=" . $this->estadoSuscripcion .
       "| rubrosList=" . $rubrosString .
	   "| zonasList=" . $zonasString .
       "| provinciasList=" . $provinciasString;
  }
  
}




class NoClienteRequest {
  public $tipoDeDocumento;
  public $numeroDeDocumento;

  public $nacionalidad;

  public $nombre;
  public $apellido;
  public $sexo;
  public $fechaNacimiento; //DDMMAAAA
  public $email;
  public $prefijoCelular;
  public $caracterCelular;
  public $numeroCelular;
  
  public function __toString()
  {
    return " tipoDeDocumento=". $this->tipoDeDocumento . 
		   "| numeroDeDocumento=" . $this->numeroDeDocumento . 
       "| nacionalidad=" . $this->nacionalidad . 
		   "| nombre=" . $this->nombre . 
		   "| apellido=" . $this->apellido . 
		   "| sexo=" . $this->sexo . 
		   "| fechaNacimiento=" . $this->fechaNacimiento . 
		   "| email=" . $this->email . 
		   "| prefijoCelular=" . $this->prefijoCelular . 
		   "| caracterCelular=" . $this->caracterCelular . 
		   "| numeroCelular=" . $this->numeroCelular;
  }
}


class AltaSMSResponse {
	public $transaccionProcesada;
	public $codigoRetorno;
	public $mensajeRetorno;
	public $idBeneficio;
	public $compania;
	public $area;
	public $numeroCelular;
	
	public function __toString()  {
    return " codigoRetorno=". $this->codigoRetorno . 
		   "| mensajeRetorno=" . $this->mensajeRetorno . 
		   "| idBeneficio=" . $this->idBeneficio . 
		   "| compania=" . $this->compania .  
		   "| area=" . $this->area .
		   "| numeroCelular=" . $this->numeroCelular;
  }
}



/**
 * Definir una clase de excepción personalizada
 */
class DatosIncorrectosException extends Exception
{
   
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    // representación de cadena personalizada del objeto
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}