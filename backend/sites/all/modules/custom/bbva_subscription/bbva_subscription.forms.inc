<?php


function bbva_subscription_form($form, &$form_state) {

  // Variables predefinidas
  $documentTypeKeyValue = bbva_login_get_document_type_list();

  $nationality_list = bbva_subscription_get_nationality_type_list();

  $etiquetas_tipo_cliente  = '<div class="pregunta-soy-cliente">';
  $etiquetas_tipo_cliente .= '  <a id="soy" href="#">SOY CLIENTE BBVA</a>';
  $etiquetas_tipo_cliente .= '  <a id="nosoy" href="#">NO SOY CLIENTE BBVA</a>';
  $etiquetas_tipo_cliente .= '</div>';

  $gender = array('M' => t('M'), 'F' => t('F'));

  $days_list = array('0'=>'DD');
  for ($i = 1; $i<=31; $i++) {
	$days_list[$i] = $i;
  }

  $month_list = array('0'=>'MM');
  for ($i = 1; $i<=12; $i++) {
	$month_list[$i] = $i;
  }

  $hoy = getdate();
  $year = (int)$hoy['year'];
  $early_year_allow = $year - 16;

  $year_list = array('0'=>'AAAA');
  for ($i = $early_year_allow; $i>=1930; $i--) {
	$year_list[$i] = $i;
  }

  $companiaKeyValue = array('0' => t('seleccione'), '0001' => t('Movistar'), '0002' => t('Claro'), '0003' => t('Personal'),'0004' => t('Nextel'),);

  $area_list = get_code_area_type();

  $rubros = _bbva_subscription_options(9);
  $rubros_keys = array_keys($rubros);
  $zonas = array_merge(_bbva_subscription_zones(), _bbva_subscription_provinces());

  // Load dates if second page.
  $fields = drupal_get_query_parameters();

  if (isset($fields)) {
    form_clear_error();
    $nationality = (empty($fields['nationality']))?ARGENTINA_CODE:$fields['nationality'];
  }

  if (isset($form_state['storage']['page_two'])) {
    return bbva_subscription_page($form, $form_state);
  }


  if (!drupal_is_front_page()) {
    $form['group_left_top'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fields'),
      '#collapsible' => TRUE,
      '#weight' => -2,
      '#attributes' => array(
        'class' => array('group-left-top'),
      ),
    );
    $form['group_left_top']['title'] = array(
      '#title' => '',
      '#type' => 'markup',
      '#markup' => '<div id="form-item-title">Registrarse en Go</div>',
    );
  }

  $form['group_right'] = array(
      '#type' => 'fieldset',
      '#title' => t('Group Right'),
      '#collapsible' => TRUE,
  );

  $form['group_left'] = array(
    '#type' => 'fieldset',
    '#title' => t('Group left'),
    '#collapsible' => TRUE,
    '#weight' => -1,
    '#attributes' => array(
        'class' => array('group-left'),
    ),
  );

  $form['group_left']['subtitle'] = array(
    '#title' => t('Facebook'),
    '#type' => 'markup',
    '#markup' => '<div id="form-item-subtitle">y accedé a los beneficios que BBVA tiene para vos, seas o no cliente BBVA.</div>',
  );

  $form['group_left']['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
    '#collapsible' => TRUE,
    '#attributes' => array(
      'class' => array('group-left-fields'),
    ),
  );
  $form['group_left']['fields']['tipo_cliente'] = array('#type' => 'hidden', '#default_value' => 'NO_CLIENTE', '#weight' => -3,);
  if (drupal_is_front_page()) {
	$form['group_left']['fields']['tipo_form'] = array('#type' => 'hidden', '#value' => 'HOME', '#weight' => -3,);
  } else {
	$form['group_left']['fields']['tipo_form'] = array('#type' => 'hidden', '#value' => 'FULL', '#weight' => -3,);
  }

  $form['group_left']['fields']['origen'] = array('#type' => 'hidden', '#default_value' => $fields['origen']);

  $form['group_left']['fields']['client_notclient'] = array(
    '#title' => t('Facebook'),
    '#type' => 'markup',
    '#markup' => $etiquetas_tipo_cliente,
  );



  $form['group_left']['fields']['message_box'] = array(
    '#type' => 'markup',
    '#markup' => '<div style="display:none;"><div id="message-box"><div id="msg-title">Atenci&oacute;n</div><div id="msg-container">Si ya sos cliente BBVA al finalizar la suscripci&oacute;n podr&aacute;s identificarte en FranceGo con el mismo usuario y contraseña para ingresar al Home Banking de BBVA</div><div id="msg-footer"><a href="javascript:void(null);" class="simplemodal-close">ACEPTAR</a><a href="javascript:void(null);" class="simplemodal-close">CANCELAR</a></div></div></div>',
    '#default_value' => '',
    '#attributes' => array(
      'style' => array('display:none;'),
    ),
    '#weight' => -5,
  );

  $form['group_left']['fields']['nombre'] = array(
    '#title' => t('Nombre'),
    '#type' => 'textfield',
    '#maxlength' => 30,
    '#size' => 17,
    '#weight' => 1,
    '#default_value' => $fields['nombre'],
  );

  $form['group_left']['fields']['apellido'] = array(
    '#title' => t('Apellido'),
    '#type' => 'textfield',
    '#maxlength' => 30,
    '#size' => 17,
    '#weight' => 2,
    '#default_value' => $fields['apellido'],
  );


  $form['group_left']['fields']['group_document'] = array(
      '#type' => 'fieldset',
      '#weight' => 3,
      '#attributes' => array(
        'class' => array('group-document'),
      ),
  );

  $form['group_left']['fields']['group_document']['document_type'] = array(
    '#type' => 'select',
    '#maxlength' => 3,
    '#title' => t('Tipo y Nro de documento'),
    '#options' => $documentTypeKeyValue,
    '#default_value' => $fields['document_type'],
  );

  $form['group_left']['fields']['group_document']['document_number'] = array(
    '#type' => 'textfield',
    '#maxlength' => 15,
    '#size' => 17,
    '#default_value' => $fields['document_number'],
  );

  $form['group_left']['fields']['nationality'] = array(
    '#title' => t('Nacionalidad'),
    '#type' => 'select',
    '#weight' => 4,
    '#options' => $nationality_list,
    '#default_value' => $nationality,
  );



  $form['group_left']['fields']['fecha_nac_div'] = array(
    '#prefix' => '<div class="fecha_nacimiento">',
    '#weight' => 5,
  );

  $form['group_left']['fields']['fecha_nac_label'] = array(
    '#prefix' => '<div class="date-field-subscription-label">',
    '#markup' => t('Fecha de Nacimiento'),
    '#weight' => 6,
    '#suffix' => '</div>',
  );

  $form['group_left']['fields']['day'] = array(
    '#type' => 'select',
	  '#prefix' => '<div class="date-field-subscription">',
    '#options' => $days_list,
    '#maxlength' => 2,
    '#weight' => 7,
    '#attributes' => array(
      'placeholder' => array(t('DD')),
    ),

    '#default_value' => array($fields['day']),
  );

  $form['group_left']['fields']['month'] = array(
    '#type' => 'select',
    '#options' => $month_list,
    '#maxlength' => 2,
    '#weight' => 8,
    '#attributes' => array(
      'placeholder' => array(t('MM')),
    ),
    '#default_value' => array($fields['month']),
  );

  $form['group_left']['fields']['year'] = array(
    '#type' => 'select',
    '#options' => $year_list,
    '#maxlength' => 4,
    '#weight' => 9,
	  '#suffix' => '</div></div>',
    '#attributes' => array(
      'placeholder' => array(t('AAAA')),
    ),
    '#default_value' => array($fields['year']),
  );

  $gender_html_div = '<div id="gender_suscripcion">';
  $gender_html_div .= '  <div id="g-masculino" class="gender">';
  $gender_html_div .= '    <b>M</b>';
  $gender_html_div .= '  </div>';
  $gender_html_div .= '  <div id="g-femenino" class="gender">';
  $gender_html_div .= '    <b>F</b>';
  $gender_html_div .= '  </div>';
  $gender_html_div .= '</div>';
  $gender_html_div .= '</div>';
  $form['group_left']['fields']['sex'] = array(
    '#title' => 'Sexo',
    '#type' => 'radios',
    '#options' => $gender,
    '#prefix' => '<div class="bbva-form-item-sexo">',
    '#suffix' => $gender_html_div,
    '#weight' => 11,
    '#default_value' => $fields['sex'],
  );



  $form['group_left']['fields']['tel_cel_div'] = array(
    '#prefix' => '<div class="telefono_celular">',
    '#weight' => 12,
  );

  $form['group_left']['fields']['telefono_celular_label'] = array(
    '#prefix' => '<div class="date-field-celular-label">',
    '#markup' => t('Teléfono Celular <span class="bbva_prefix">(sin 15)</span>'),
    '#weight' => 13,
    '#suffix' => '</div>',
  );

  $form['group_left']['fields']['area'] = array(
    '#type' => 'select',
    '#options' => $area_list,
    '#maxlength' => 4,
    '#weight' => 14,
    '#default_value' => $fields['area'],
  );

  $form['group_left']['fields']['celular'] = array(
    '#type' => 'textfield',
    '#maxlength' => 8,
    '#size' => 17,
    '#weight' => 15,
    '#attributes' => array(
      'title' => array(t("Ingrese Celular sin 15.")),
    ),
    '#default_value' => $fields['celular'],
    '#suffix' => '</div>',
  );

  $form['group_left']['fields']['compania'] = array(
    '#type' => 'select',
    '#title' => t('Compañía'),
    '#weight' => 16,
    '#options' => $companiaKeyValue,
    '#default_value' => $fields['compania'],
  );

    $form['group_left']['fields']['email'] = array(
    '#title' => t('E-mail'),
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => 17,
    '#weight' => 17,
    '#default_value' => $fields['email'],
  );

  $form['group_left']['fields']['group_pass'] = array(
      '#type' => 'fieldset',
      '#weight' => 18,
      '#prefix' => '<div class="group_pass_all"><p>' . t('A continuación generá tu contraseña que te permitirá iniciar sesión') . '</p>',
      '#suffix' => '</div>',
  );

  $form['group_left']['fields']['group_pass']['pass1'] = array(
    '#title' => t('Contraseña'),
    '#type' => 'password',
    '#maxlength' => 8,
    '#size' => 17,
    '#attributes' => array(
      'placeholder' => array(t("Min 6, Max 8 Caracteres")),
    ),
  );

  $form['group_left']['fields']['group_pass']['pass2'] = array(
    '#title' => t('Confirmá tu Contraseña'),
    '#type' => 'password',
    '#maxlength' => 8,
    '#size' => 17,
  );

  if (drupal_is_front_page()) {

    $form['group_left']['zonas'] = array(
      '#type' => 'fieldset',
      '#title' => t('Zonas'),
      '#collapsible' => TRUE,
	    '#attributes' => array(
      'class' => array('bbva-edit-zonas'),
      ),
    );

    $form['group_left']['zonas']['todos_zonas'] = array(
      '#type' => 'checkboxes',
      '#prefix' => '<div class="todas_zonas_div">',
      '#options' => array(t('Todas las Zonas')),
	    '#default_value' => array(0),
	    '#disabled' => TRUE,
      '#attributes' => array(
      'class' => array('todos-zonas'),
      ),
      '#weight' => -2,
    );

    $form['group_left']['zonas']['next'] = array(
      '#type' => 'submit',
      '#value' => '(Editar)',
      '#attributes' => array(
      'class' => array('field-next'),
      ),
      '#weight' => 0,
      '#suffix' => '</div>',
    );

    $form['group_left']['zonas']['todos_rubros'] = array(
      '#type' => 'checkboxes',
      '#prefix' => '<div class="todos_rubros_div">',
      '#options' => array(t('Todos los Rubros')),
	    '#default_value' => array(0),
	    '#disabled' => TRUE,
      '#attributes' => array(
        'class' => array('todos-rubros'),
      ),
      '#weight' => -1,
    );

    $form['group_left']['zonas']['next2'] = array(
      '#type' => 'submit',
      '#value' => '(Editar)',
      '#attributes' => array(
        'class' => array('field-next2'),
      ),
      '#weight' => 0,
      '#suffix' => '</div>',
    );
  } else {
    //***********      Columna Derecha       *****************
    //***********                            *****************
    // Fieldset Group Right

    $form['group_right'] = array(
      '#type' => 'fieldset',
      '#title' => t('Group Right'),
      '#collapsible' => TRUE,
      '#weight' => 30,
      '#attributes' => array(
        'class' => array('group-right'),
      ),
    );

    $form['group_right']['zonas'] = array(
      '#title' => t('Seleccione las Zonas de las que desea recibir alertas de beneficios'),
      '#description' => t('*Debe seleccionar al menos una'),
      '#type' => 'checkboxes',
      '#options' => $zonas,
	  '#default_value' => array('PROV-1'),
    );

    $form['group_right']['zonas_all'] = array(
      '#title' => '',
      '#type' => 'markup',
      '#markup' => '<div id="all"><a id="checkAll">Todos</a> / <a id="checkNone">Ninguno</a></div>',
    );

    $form['group_right']['rubros'] = array(
      '#title' => t('Seleccione los rubros de los que desea recibir alertas de beneficios'),
      '#description' => t('*Debe seleccionar al menos una'),
      '#type' => 'checkboxes',
      '#options' => $rubros,
	  '#default_value' => $rubros_keys,
    );

    $form['group_right']['rubros_all'] = array(
      '#title' => '',
      '#type' => 'markup',
      '#markup' => '<div id="all"><a id="checkAll2">Todos</a> / <a id="checkNone2">Ninguno</a></div>',
    );
  }

  $form['group_left']['terminos'] = array(
    '#title' => t('Acepto Términos y Condiciones'),
    '#type' => 'checkbox',
    '#required' => FALSE,
    '#default_value' =>FALSE,
    '#weight' => 20,
    '#attributes' => array('class'=>'terminos-check'),
    '#attributes' => array(
      'class' => array('terminos-check'),
    ),

  );

  $form['group_left']['next3'] = array(
    '#markup' => '<a href="'.url('terminos-y-condiciones-de-francesgo').'" class="colorbox-node colorbox-subscription-ver-terminos">(Ver)</a>',
    '#weight' => 21,
  );

  // $form['group_left']['bbva_captcha'] = array(
  //     '#type' => 'markup',
  //     '#markup' => '<div id="bbva_captcha_content"></div>',
  //     '#weight' => 22,
  //   );

  $form['#validate'] = array('bbva_subscription_validate_form_home');

  $form['group_left']['group_footer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Group Footer'),
    '#collapsible' => TRUE,
    '#attributes' => array(
      'class' => array('group-footer'),
    ),
	'#weight' => 23,
  );

  $form['group_left']['group_footer']['submit'] = array(
    '#value' => t('Registrarme'),
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'bbva_subscription_mock_ok',
      'wrapper' => 'bbva-subscription-form',
      'effect' => 'fade',
    ),
  );


  $form['group_left']['group_footer']['wrapper'] = array('#markup' => '<div id="test-ajax">'.$messages.'</div>');

  $form['#submit'][] = 'bbva_subscription_action';

  //$form['#after_build'][] = 'bbva_subscription_captcha_after_build';

  return $form;
}


function bbva_subscription_form_callback($form, &$form_state) {
    $messages = theme('status_messages');
    return '<div id="test-ajax">'.$messages.'</div>';
}

function bbva_subscription_form_validate($form, &$form_state) {

}

function bbva_subscription_form_submit($form, &$form_state) {

}


function bbva_subscription_get_nationality_type_list() {
  $nationality_list = array(
   '001' => 'REINO UNIDO',
   '002' => 'ESTADOS UNIDOS',
   '003' => 'ALEMANIA',
   '004' => 'FRANCIA',
   '005' => 'SUIZA',
   '006' => 'BELGICA',
   '007' => 'ITALIA',
   '008' => 'HOLANDA',
   '009' => 'ESPANIA',
   '010' => 'URUGUAY',
   '011' => 'CHILE',
   '012' => 'BRASIL',
   '013' => 'AUSTRIA',
   '014' => 'CHECOSLOVAQUIA',
   '015' => 'DINAMARCA',
   '016' => 'PORTUGAL',
   '017' => 'CANADA',
   '018' => 'ISLANDIA',
   '019' => 'JAPON',
   '020' => 'INDIA',
   '021' => 'AUSTRALIA',
   '022' => 'NUEVA ZELANDIA',
   '023' => 'SUDAFRICA',
   '024' => 'IRLANDA',
   '025' => 'ALEMANIA DEMOCRATICA',
   '026' => 'PERU',
   '027' => 'SUECIA',
   '028' => 'NORUEGA',
   '029' => 'MACEDONIA',
   '030' => 'POLONIA',
   '031' => 'RUMANIA',
   '032' => 'LIBANESA',
   '033' => 'MEXICO',
   '034' => 'HUNGRIA',
   '035' => 'ARABE SIRIA',
   '036' => 'IRAN',
   '037' => 'FINLANDIA',
   '038' => 'GRECIA',
   '039' => 'ISRAEL',
   '040' => 'U.R.S.S.',
   '041' => 'BOLIVIA',
   '042' => 'PAKISTAN',
   '043' => 'LUXEMBURGO',
   '044' => 'COREA',
   '045' => 'BULGARIA',
   '046' => 'TURQUIA',
   '047' => 'ECUADOR',
   '048' => 'BAHAMAS',
   '049' => 'LITUANIA',
   '051' => 'CUBA',
   '052' => 'ALBANIA',
   '053' => 'ARABIA SAUDITA',
   '054' => 'COLOMBIA',
   '055' => 'COSTA RICA',
   '056' => 'TAIWAN',
   '057' => 'EGIPTO',
   '058' => 'EL SALVADOR',
   '059' => 'ETIOPIA SOC',
   '061' => 'GUATEMALA',
   '062' => 'HAITI',
   '063' => 'HONDURAS',
   '064' => 'LIBERIA',
   '065' => 'AFGANISTAN',
   '066' => 'PTO.RICO',
   '067' => 'NICARAGUA',
   '068' => 'PANAMA',
   '069' => 'PARAGUAY',
   '073' => 'BHUTAN',
   '076' => 'DOMINICANA',
   '078' => 'SANTA SEDE',
   '079' => 'VENEZUELA',
   '080' => 'ARGENTINA',
   '083' => 'CHINA',
   '085' => 'FILIPINAS',
   '086' => 'IRAK',
   '088' => 'SRI LANKA',
   '089' => 'MYANMAR',
   '090' => 'INDONESIA',
   '092' => 'GUINEA BISSAU',
   '094' => 'SOMALIA',
   '096' => 'NVAS.HEBRIDAS',
   '097' => 'GUAYANA',
   '100' => 'ALTO VOLTA',
   '101' => 'ANDORRA',
   '102' => 'ARGELIA',
   '104' => 'BURUNDI',
   '105' => 'KAMPUCHEA DEMOC.',
   '106' => 'CAMERUM',
   '107' => 'CENTRO AFRICANA',
   '108' => 'ZAIRE',
   '109' => 'ZAMBIA',
   '110' => 'COSTA DE MARFIL',
   '111' => 'CHAD',
   '112' => 'CHIPRE',
   '113' => 'BENIM',
   '114' => 'GABONESA',
   '115' => 'GHANA',
   '116' => 'GUINEA',
   '117' => 'JAMAICA',
   '118' => 'JORDANIA',
   '119' => 'KENIA',
   '120' => 'KUWAIT',
   '122' => 'LIBIA',
   '123' => 'LIECHTENSTEIN',
   '124' => 'MADAGASCAR',
   '125' => 'MALI',
   '126' => 'MARRUECOS',
   '127' => 'MAURITANIA',
   '128' => 'MONGOLIA',
   '129' => 'MONACO',
   '130' => 'NEPAL',
   '131' => 'NIGER',
   '132' => 'NIGERIA',
   '133' => 'RUANDA',
   '135' => 'SAN MARINO',
   '136' => 'SENEGAL',
   '137' => 'SIERRA LEONA',
   '138' => 'SUDAN',
   '139' => 'SURINAME,REPUBLICA DE',
   '140' => 'THAILANDIA',
   '141' => 'TOGOLEN',
   '142' => 'TRINIDAD Y TOBAGO',
   '143' => 'TUNECINA',
   '144' => 'UGANDA',
   '145' => 'VIETNAM',
   '147' => 'YEMEN,REP.ARABE',
   '148' => 'YEMEN,REP.EMOC.',
   '149' => 'BAHERIN',
   '150' => 'BARBADOS',
   '151' => 'BOTSWANA',
   '152' => 'CONGO',
   '153' => 'EMIRATOS ARABES',
   '154' => 'GAMBIA',
   '155' => 'GUINEA ECUATORIAL',
   '156' => 'MALASIA',
   '157' => 'MALAWI',
   '158' => 'MAURICIO',
   '159' => 'OMAN',
   '160' => 'QATAR',
   '161' => 'TANZANIA',
   '162' => 'GRENADA',
   '163' => 'DOMINICA',
   '164' => 'ANGOLA',
   '165' => 'BANGLADESH',
   '166' => 'LESOTHO',
   '167' => 'MALDIVAS',
   '168' => 'MALTA',
   '169' => 'PAPUA Y NVA.GUINEA',
   '170' => 'SAN VICENTE Y LAS GRANADINAS',
   '171' => 'MALTA (OR.MIL)',
   '172' => 'CABO VERDE',
   '173' => 'SALOMON',
   '174' => 'BRUNEI',
   '175' => 'NUEVA CALEDONIA',
   '176' => 'ZIMBAWE',
   '177' => 'NAURU',
   '178' => 'SANTA LUCIA',
   '179' => 'SANTO TOME Y PRINCIPE',
   '180' => 'SWAZILANDIA',
   '181' => 'TONGA',
   '182' => 'SINGAPUR',
   '183' => 'HONG KONG',
   '184' => 'COMORAS',
   '185' => 'DJIBOUTIA',
   '186' => 'FIJI',
   '187' => 'MOZAMBIQUE',
   '188' => 'SEYCHELLES',
   '199' => 'SURINAME',
   '250' => 'REPUBLICA DE ARMENIA',
   '251' => 'SERBIA',
   '252' => 'MONTENEGRO',
   '253' => 'UCRANIA',
   '254' => 'CROACIA',
  );
  asort($nationality_list);
  return $nationality_list;
}



/**
* Esto no tiene que estar harcodeado.  Meter en una base.
**/
function get_code_area_type() {
$area_list = array(
'11'=>'11',
'220'=>'220',
'221'=>'221',
'223'=>'223',
'230'=>'230',
'236'=>'236',
'237'=>'237',
'249'=>'249',
'260'=>'260',
'261'=>'261',
'263'=>'263',
'264'=>'264',
'266'=>'266',
'280'=>'280',
'291'=>'291',
'294'=>'294',
'297'=>'297',
'298'=>'298',
'299'=>'299',
'336'=>'336',
'341'=>'341',
'342'=>'342',
'343'=>'343',
'345'=>'345',
'348'=>'348',
'351'=>'351',
'353'=>'353',
'358'=>'358',
'362'=>'362',
'364'=>'364',
'370'=>'370',
'376'=>'376',
'379'=>'379',
'380'=>'380',
'381'=>'381',
'383'=>'383',
'385'=>'385',
'387'=>'387',
'388'=>'388',
'2202'=>'2202',
'2221'=>'2221',
'2223'=>'2223',
'2224'=>'2224',
'2225'=>'2225',
'2226'=>'2226',
'2227'=>'2227',
'2229'=>'2229',
'2241'=>'2241',
'2242'=>'2242',
'2243'=>'2243',
'2244'=>'2244',
'2245'=>'2245',
'2246'=>'2246',
'2252'=>'2252',
'2254'=>'2254',
'2255'=>'2255',
'2257'=>'2257',
'2261'=>'2261',
'2262'=>'2262',
'2264'=>'2264',
'2265'=>'2265',
'2266'=>'2266',
'2267'=>'2267',
'2268'=>'2268',
'2271'=>'2271',
'2272'=>'2272',
'2273'=>'2273',
'2274'=>'2274',
'2281'=>'2281',
'2283'=>'2283',
'2284'=>'2284',
'2285'=>'2285',
'2286'=>'2286',
'2291'=>'2291',
'2292'=>'2292',
'2293'=>'2293',
'2296'=>'2296',
'2297'=>'2297',
'2302'=>'2302',
'2314'=>'2314',
'2316'=>'2316',
'2317'=>'2317',
'2320'=>'2320',
'2322'=>'2322',
'2323'=>'2323',
'2324'=>'2324',
'2325'=>'2325',
'2326'=>'2326',
'2331'=>'2331',
'2333'=>'2333',
'2334'=>'2334',
'2335'=>'2335',
'2336'=>'2336',
'2337'=>'2337',
'2338'=>'2338',
'2342'=>'2342',
'2343'=>'2343',
'2344'=>'2344',
'2345'=>'2345',
'2346'=>'2346',
'2352'=>'2352',
'2353'=>'2353',
'2354'=>'2354',
'2355'=>'2355',
'2356'=>'2356',
'2357'=>'2357',
'2358'=>'2358',
'2362'=>'2362',
'2392'=>'2392',
'2393'=>'2393',
'2394'=>'2394',
'2395'=>'2395',
'2396'=>'2396',
'2473'=>'2473',
'2474'=>'2474',
'2475'=>'2475',
'2477'=>'2477',
'2478'=>'2478',
'2622'=>'2622',
'2623'=>'2623',
'2624'=>'2624',
'2625'=>'2625',
'2626'=>'2626',
'2627'=>'2627',
'2646'=>'2646',
'2647'=>'2647',
'2648'=>'2648',
'2651'=>'2651',
'2652'=>'2652',
'2655'=>'2655',
'2656'=>'2656',
'2657'=>'2657',
'2658'=>'2658',
'2901'=>'2901',
'2902'=>'2902',
'2903'=>'2903',
'2920'=>'2920',
'2921'=>'2921',
'2922'=>'2922',
'2923'=>'2923',
'2924'=>'2924',
'2925'=>'2925',
'2926'=>'2926',
'2927'=>'2927',
'2928'=>'2928',
'2929'=>'2929',
'2931'=>'2931',
'2932'=>'2932',
'2933'=>'2933',
'2934'=>'2934',
'2935'=>'2935',
'2936'=>'2936',
'2940'=>'2940',
'2941'=>'2941',
'2942'=>'2942',
'2944'=>'2944',
'2945'=>'2945',
'2946'=>'2946',
'2948'=>'2948',
'2952'=>'2952',
'2953'=>'2953',
'2954'=>'2954',
'2962'=>'2962',
'2963'=>'2963',
'2964'=>'2964',
'2965'=>'2965',
'2966'=>'2966',
'2972'=>'2972',
'2982'=>'2982',
'2983'=>'2983',
'3327'=>'3327',
'3329'=>'3329',
'3382'=>'3382',
'3385'=>'3385',
'3387'=>'3387',
'3388'=>'3388',
'3400'=>'3400',
'3401'=>'3401',
'3402'=>'3402',
'3404'=>'3404',
'3405'=>'3405',
'3406'=>'3406',
'3407'=>'3407',
'3408'=>'3408',
'3409'=>'3409',
'3435'=>'3435',
'3436'=>'3436',
'3437'=>'3437',
'3438'=>'3438',
'3442'=>'3442',
'3444'=>'3444',
'3445'=>'3445',
'3446'=>'3446',
'3447'=>'3447',
'3454'=>'3454',
'3455'=>'3455',
'3456'=>'3456',
'3458'=>'3458',
'3460'=>'3460',
'3461'=>'3461',
'3462'=>'3462',
'3463'=>'3463',
'3464'=>'3464',
'3465'=>'3465',
'3466'=>'3466',
'3467'=>'3467',
'3468'=>'3468',
'3469'=>'3469',
'3471'=>'3471',
'3472'=>'3472',
'3476'=>'3476',
'3482'=>'3482',
'3483'=>'3483',
'3487'=>'3487',
'3488'=>'3488',
'3489'=>'3489',
'3491'=>'3491',
'3492'=>'3492',
'3493'=>'3493',
'3496'=>'3496',
'3497'=>'3497',
'3498'=>'3498',
'3521'=>'3521',
'3522'=>'3522',
'3524'=>'3524',
'3525'=>'3525',
'3532'=>'3532',
'3533'=>'3533',
'3534'=>'3534',
'3537'=>'3537',
'3541'=>'3541',
'3542'=>'3542',
'3543'=>'3543',
'3544'=>'3544',
'3546'=>'3546',
'3547'=>'3547',
'3548'=>'3548',
'3549'=>'3549',
'3562'=>'3562',
'3563'=>'3563',
'3564'=>'3564',
'3571'=>'3571',
'3572'=>'3572',
'3573'=>'3573',
'3574'=>'3574',
'3575'=>'3575',
'3576'=>'3576',
'3582'=>'3582',
'3583'=>'3583',
'3584'=>'3584',
'3585'=>'3585',
'3711'=>'3711',
'3715'=>'3715',
'3716'=>'3716',
'3717'=>'3717',
'3718'=>'3718',
'3721'=>'3721',
'3722'=>'3722',
'3725'=>'3725',
'3731'=>'3731',
'3732'=>'3732',
'3734'=>'3734',
'3735'=>'3735',
'3741'=>'3741',
'3743'=>'3743',
'3751'=>'3751',
'3752'=>'3752',
'3754'=>'3754',
'3755'=>'3755',
'3756'=>'3756',
'3757'=>'3757',
'3758'=>'3758',
'3772'=>'3772',
'3773'=>'3773',
'3774'=>'3774',
'3775'=>'3775',
'3777'=>'3777',
'3781'=>'3781',
'3782'=>'3782',
'3783'=>'3783',
'3786'=>'3786',
'3821'=>'3821',
'3822'=>'3822',
'3825'=>'3825',
'3826'=>'3826',
'3827'=>'3827',
'3832'=>'3832',
'3833'=>'3833',
'3835'=>'3835',
'3837'=>'3837',
'3838'=>'3838',
'3841'=>'3841',
'3843'=>'3843',
'3844'=>'3844',
'3845'=>'3845',
'3846'=>'3846',
'3854'=>'3854',
'3855'=>'3855',
'3856'=>'3856',
'3857'=>'3857',
'3858'=>'3858',
'3861'=>'3861',
'3862'=>'3862',
'3863'=>'3863',
'3865'=>'3865',
'3867'=>'3867',
'3868'=>'3868',
'3869'=>'3869',
'3873'=>'3873',
'3875'=>'3875',
'3876'=>'3876',
'3877'=>'3877',
'3878'=>'3878',
'3884'=>'3884',
'3885'=>'3885',
'3886'=>'3886',
'3887'=>'3887',
'3888'=>'3888',
'3891'=>'3891',
'3892'=>'3892',
'3894'=>'3894');

return $area_list;
}
