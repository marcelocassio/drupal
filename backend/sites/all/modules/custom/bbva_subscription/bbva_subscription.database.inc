<?php

define('BBVA_SUBSCRIPTION_CANAL_WEB', 1);
define('BBVA_SUBSCRIPTION_CANAL_APP', 2);

function _bbva_subscription_nueva_suscripcion_web($name_user) {
  $name_user = intval($name_user);
  return _bbva_subscription_nueva_suscripcion($name_user, BBVA_SUBSCRIPTION_CANAL_WEB);
}
function _bbva_subscription_nueva_suscripcion_app($name_user) {
  $name_user = intval($name_user);
  return _bbva_subscription_nueva_suscripcion($name_user, BBVA_SUBSCRIPTION_CANAL_APP);
}

function _bbva_subscription_nueva_suscripcion($name_user, $cod_canal) {

  $date = date_create();
  $date = date_timestamp_get($date);

  $query = db_insert('bbva_user_subscription')->fields(array(
              'name_user' => $name_user,
              'date_subscription' => $date,
              'channel_subscription' => $cod_canal,
            ));

  $result = $query->execute();

  return $result;
}
