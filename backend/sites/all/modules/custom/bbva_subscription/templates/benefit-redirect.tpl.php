<?php
  if ($vars != null) {
    $options = array('query' => array(
      'field_benefit_campaign_target_id' => $vars['id_campaign_drupal'])
    );
  }
  $link = isset($linkclass) ? $linkclass : null;
?>
<div id="section-benefit-redirect">
	<div class="redirect-content-wrapper">
		<p>En un instante <br>el contenido estará disponible</p>
	</div>
</div>
<div>
<?php if ($vars != null): ?>
  <?php if ($vars['id_campaign_drupal'] != null): ?>
    <span class="benefit_id" style="display:none">
      <?php print t('<a href="@url" class="link-redirect-benefit">LinkBeneficio</a>', array('@url' => url('benefit-experience', $options))) ?>
    </span>
		<?php else: ?>
			<span class="benefit_id" style="display:none">
        <?php print l('Link Beneficio', 'benefit/benefit/' . $vars['id_benefit'], array('attributes' => array('class' => array($link, 'link-redirect-benefit'))))?>
      </span>
		<?php endif ?>
	<?php else: ?>
		<span class="benefit_id" style="display:none">
      <?php print l('Link Beneficio', '404', array('attributes' => array('class' => array($link, 'link-redirect-benefit'))))?>
    </span>
	<?php endif ?>
</div>
<script type="text/javascript">
	(function($) {
		href = $('.link-redirect-benefit').attr('href') +"&app=s"+ '#section-benefit';
		window.location.href = href;
	})(jQuery);
</script>