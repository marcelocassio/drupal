<?php
$codarea = "";
$celular = "";
$compania = "";

$link_susbcription = "";

if ($vars != null) {
	$codarea = $vars['area'];
	$celular = $vars['celular'];
	$compania = $vars['compania'];
	

	$options = array('query' =>
	  array(
		'area' => trim($codarea),
		'celular' => trim($celular),
		'compania' => trim($compania),
	  )
	);
	
	$link_susbcription = 'subscription';
}

?>
<div id="section-alta-sms-redirect">
	<h3>Aguarda un momento por favor...<h3>
    <?php if ($vars != null): ?>
	<span class="alta_sms_id" style="display:none"><?php print t('<a href="@url" class="link-redirect-sms-alta">LinkBeneficio</a>', array('@url' => url('subscription', $options)))?></span>
	<?php else: ?>
	<span class="alta_sms_id" style="display:none"><?php print l('Link Beneficio', '404', array('attributes' => array('class' => array($linkclass, 'link-redirect-sms-alta'))))?></span>
	<?php endif;?>
</div>
<script type="text/javascript">


(function($) {

var href = $('.link-redirect-sms-alta').attr('href');
window.location.href = href;


})(jQuery);
</script>