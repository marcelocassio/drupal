(function($) {
	var TIPO_CLIENTE = 'CLIENTE';
	var TIPO_NO_CLIENTE = 'NO_CLIENTE';

  function adaptarSuscripcionMobile() {
	$('#block-views-solo_por_hoy-block').css('display', 'none');
	$('.region region-slideshow').css('display', 'none');
	$('#slideshow-wrapper').css('display', 'none');
	$('#higlighted-area').css('margin-top', '0px');
	$('#higlighted-area').css('margin-bottom', '0px');
	$('.cabezal-ingresar').css('display', 'none');
	$('#menu-responsive button').css('display', 'none');
	$('#pre-footer').css('display', 'none');
	$('#footer').css('display', 'none');
	$('#footer-copyright').css('display', 'none');
	$('#bbva-subscription-form input[name=origen] ').val("IOS");
  }

  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
  };

  function move_description() {
  	/** Mover Elementos en Subscription **/
    $('.form-item-zonas .description').prependTo('.form-item-zonas');
    $('.form-item-zonas > label').prependTo('.form-item-zonas');
    $('.form-item-rubros .description').prependTo('.form-item-rubros');
    $('.form-item-rubros > label').prependTo('.form-item-rubros');
  }

  function manage_select_all_none() {
  	/** Check all or none checkbox **/
      $('#all #checkAll').click(function() {
        $('.form-item-zonas input[type="checkbox"]').attr('checked', true);
      });

      $('#all #checkNone').click(function() {
        $('.form-item-zonas input[type="checkbox"]').attr('checked', false);
      });

      $('#all #checkAll2').click(function() {
        $('.form-item-rubros input[type="checkbox"]').attr('checked', true);
      });

      $('#all #checkNone2').click(function() {
        $('.form-item-rubros input[type="checkbox"]').attr('checked', false);
      });
  }

  function fix_captcha_position_drupaldesa() { // Si el cache resulta exitoso, borrar esta funcion
/*
	var captcha = $('#block-bbva_subscription-subscription_block .captcha').clone();
	$('#block-bbva_subscription-subscription_block .captcha').remove();
	$(captcha).appendTo('#block-bbva_subscription-subscription_block #bbva_captcha_content');

	var captcha_full = $('.page-subscription #bbva-subscription-form .captcha').clone();
	$('.page-subscription #bbva-subscription-form .captcha').remove();
	$(captcha_full).appendTo('.page-subscription #bbva-subscription-form #bbva_captcha_content');
*/
  }
  function openLoginAndMouseUp() {
	$('#dropdown-login .login').addClass('active').siblings('.dropdown').show();
	$("#user-login-form").hide(); //$("#login_form_full").hide();

	// Animate the scrolling motion.
	$("html, body").animate({
	scrollTop:0
	},"slow");
  }

  function manage_open_login() {
	$('#bbva-subscription-form .item-button').click(function() {
		openLoginAndMouseUp();
		return false;
	});
  }

  function marcar_no_cliente() {
	  document.getElementsByName("pass1")[0].removeAttribute("disabled");
	  document.getElementsByName("pass2")[0].removeAttribute("disabled");
	  $('.form-item-pass1').show();
	  $('.form-item-pass2').show();
	  $('.group_pass_all').show();
	  $('.pregunta-soy-cliente #soy').css('background-color','#b4b4b4');
	  $('.pregunta-soy-cliente #nosoy').css('background-color','#0079C1');
	  $('#bbva-subscription-form input[name=tipo_cliente]').val(TIPO_NO_CLIENTE);
  }

  function marcar_cliente() {
	  $(".form-item-pass1 input[type='password']").attr('disabled','disabled');
	  $(".form-item-pass2 input[type='password']").attr('disabled','disabled');
	  $(".form-item-pass1 input").val('');
	  $(".form-item-pass2 input").val('');
	  $('.form-item-pass1').hide();
	  $('.form-item-pass2').hide();
	  $('.group_pass_all').hide();

	  $('.pregunta-soy-cliente #soy').css('background-color','#0079C1');
	  $('.pregunta-soy-cliente #nosoy').css('background-color','#b4b4b4');
	  $('#bbva-subscription-form input[name=tipo_cliente]').val(TIPO_CLIENTE);

  }

  function manage_client_type() {
	var tipo = $('#bbva-subscription-form input[name=tipo_cliente]').val();
	if (tipo == TIPO_CLIENTE) {
      marcar_cliente();
	}

  	$(".pregunta-soy-cliente #soy").click(function() { // Soy cliente BBVA
	  marcar_cliente();
	  return false;
	});

	$(".pregunta-soy-cliente #nosoy").click(function() { // No Soy cliente BBVA
	  marcar_no_cliente();
	  return false;
	});
  }

  function marcar_femenino() {
	$('#bbva-subscription-form #gender_suscripcion #g-masculino').css('background-color','#b4b4b4');
	$('#bbva-subscription-form #gender_suscripcion #g-femenino').css('background-color','#0079C1');
	$("#bbva-subscription-form .form-item-sex input[value=M]").removeAttr('checked');
	$("#bbva-subscription-form .form-item-sex input[value=F]").attr('checked', 'checked');
  }
  function marcar_masculino() {
	$('#bbva-subscription-form #gender_suscripcion #g-femenino').css('background-color','#b4b4b4');
	$('#bbva-subscription-form #gender_suscripcion #g-masculino').css('background-color','#0079C1');
	$("#bbva-subscription-form .form-item-sex input[value=F]").removeAttr('checked');
	$("#bbva-subscription-form .form-item-sex input[value=M]").attr('checked', 'checked');
  }

  function manage_gender() {
  	// Checkbox Gender Suscripcion
	var check_F = $("#bbva-subscription-form .form-item-sex input[value=F]").attr('checked');
	var check_M = $("#bbva-subscription-form .form-item-sex input[value=M]").attr('checked');

	if (check_M == 'checked') {
		marcar_masculino();
	} else if (check_F == 'checked') {
		marcar_femenino();
	}

	$("#bbva-subscription-form #gender_suscripcion #g-masculino").click(function() { // Masculino
		marcar_masculino();
		return false;
	});
	$("#bbva-subscription-form #gender_suscripcion #g-femenino").click(function() { // Femenino
		marcar_femenino();
		return false;
	});
  }



  Drupal.behaviors.bbva_subscription = {
    attach: function (context, settings) {

    	manage_client_type();

		manage_gender();

		manage_select_all_none();

		//fix_captcha_position_drupaldesa();

		move_description();

		manage_open_login();
    }
  }


  $(document).ready(function(){

	var panelId = getUrlParameter('panel');
	if (panelId == 'suscribirCliente') {
		adaptarSuscripcionMobile();
		marcar_cliente();
	} else if (panelId == 'suscribirNocliente') {
		adaptarSuscripcionMobile();
		marcar_no_cliente()
	}

  });



})(jQuery);
