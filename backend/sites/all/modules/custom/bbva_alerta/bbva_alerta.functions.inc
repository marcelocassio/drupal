<?php
function _send_mail_cupon_promo_env() {

  $promosEnviado = _get_cuopon_promo(1);
  if(!empty($promosEnviado)){


  $message = _get_message_alerta_prom_env($promosEnviado);

  // Send the email.
  $params = array(
    'body' => $message
  );

  if($recipients = _get_recipients()) {
    drupal_mail('bbva_alerta', 'promo_env', $recipients, language_default(), $params, $from = NULL, $send = TRUE);
  }
 }
}

function bbva_alerta_campana_vencimientos() {
  $min_expire = new Datetime("today");
  $min_expire->add(new DateInterval("P" . BBVA_CAMPANIAS_AVISO_VENCIMIENTO . "D"));

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'campana')
    ->entityCondition('bundle', 'campana')
    ->fieldCondition('field_campana_estado', 'value', BBVA_CAMPANIAS_CAMPANA_STATUS_ACTIVE, '=')
    ->fieldCondition('field_campana_con_cupon', 'value', 1, '=')
    ->fieldCondition('field_campana_vigencia', 'value', time(), '>=')
    ->fieldCondition('field_campana_vigencia', 'value2', $min_expire->getTimestamp(), '<');

  $result = $query->execute();

  if(count($result) > 0) {
    foreach ($result as $campana_array) {
      foreach ($campana_array as $campana) {
        $entity = entity_load_single('campana', $campana->id);
        $ewrapper = entity_metadata_wrapper('campana', $entity);
        // Busco otras campañas del mismo tipo
        $query->entityCondition('entity_type', 'campana')
          ->entityCondition('bundle', 'campana')
          ->fieldCondition('field_campana_estado', 'value', BBVA_CAMPANIAS_CAMPANA_STATUS_ACTIVE, '=')
          ->fieldCondition('field_campana_tipo_de_evento', 'value', $ewrapper->field_campana_tipo_de_evento->value(), '=')
          ->fieldCondition('field_campana_con_cupon', 'value', 1, '=')
          ->fieldCondition('field_campana_vigencia', 'value', time(), '>=')
          ->fieldCondition('field_campana_vigencia', 'value2', $min_expire->getTimestamp(), '>=');

        $result2 = $query->execute();
        // Si hay otra oferta vigente que no expira del mismo tipo no envio alerta
        if(count($result2) > 0) {
          break;
        }

        $nombre = $ewrapper->field_campana_titulo->raw() . "(" . $campana->id . ")";
        $message = t("La campaña %title se vencerá próximamente.", array('%title' => $nombre));
        // Send the email.
        $params = array(
          'body' => $message
        );

        if($recipients = _get_recipients()) {
          drupal_mail('bbva_alerta', 'campana_expira', $recipients, language_default(), $params, $from = NULL, $send = TRUE);
        }
      }
    }
  }
}

function _get_message_alerta_prom_env(&$promos_enviadas){

  $message= "ALERTA Los Siguientes cupones han sido enviados y aun no contamos con los archivos de PRISMA<BR/>";
  foreach ($promos_enviadas as $key => $value) {
  	 $message.=$value->title;
  	 $message.=' Fecha de comienzo de campaña :';
  	 $message.=date('Y-m-d',$value->field_coupon_promo_date_start_value);
  	 $message.="<br/>";
  }

  return $message;
}

function check_vouchers_stock() {
  $coupons_promo = _get_coupons_promo_active();


  // Si hay cupones activos y hay seteado al menos un mail donde mandar la alerta
  if(count($coupons_promo) > 0 && $recipients = _get_recipients()) {
    $porcentaje_alarma = (int) variable_get('bbva_alerta_alertaform_porcentaje', 101);
    $params = array(
      'startMessage'=>"Estimados: <br/> Los siguientes cupones están próximos a agotarse:<BR/><BR/>",
      'body' => false,
      'endMessage'=>"<BR/>Este es un mensaje automático, por favor no responder."
    );
    foreach ($coupons_promo as $cp) {
      $total = (int) _get_total_vouchers($cp->id);
      $asignados = (int) _get_total_downloaded_vouchers($cp->id);
      $porcentaje_asignado = 0;
      if($total > 0) {
        $porcentaje_asignado = (int) $asignados * 100 / $total;
      }
      if($total > 0 && $porcentaje_asignado >= $porcentaje_alarma) {
        $disponibles = (int) $total - $asignados;
        $fecha = DateTime::createFromFormat('U', $cp->field_coupon_promo_date_start_value);
        $params['body'] .= $cp->title . "<br/>Fecha de comienzo de campaña: " . $fecha->format('Y-m-d') . "<br/>Cupones disponibles: " . $disponibles . "<br/><br/>";
      }
    }
    if($params['body']) {
      $params['body']=$params['startMessage'].$params['body'].$params['endMessage'];
      drupal_mail('bbva_alerta', 'percent_alert', $recipients, language_default(), $params, $from = NULL, $send = TRUE);
    }
  }
}

function _get_recipients() {
  $mails = variable_get('bbva_alerta_alertaform_emails', false);
  $mails = explode("\n", $mails);
  $mails_array = array();
  if(count($mails) < 1) {
    return false;
  }
  foreach ($mails as $value) {
    $value = trim($value);
    if(valid_email_address($value)) {
      $mails_array[] = $value;
    }
  }
  $mails_string = implode(",", $mails_array);

  return $mails_string;
}

