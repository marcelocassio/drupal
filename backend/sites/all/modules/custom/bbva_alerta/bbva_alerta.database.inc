<?php
function _get_cuopon_promo($state){
             
  $query = db_select('eck_coupon_promo', 'ecp');
  $query->leftJoin('field_data_field_coupon_promo_date_start', 'ecpds', 'ecpds.entity_id = ecp.id');
  $query->leftJoin('field_data_field_coupon_state_shipping', 'ecpss', 'ecpss.entity_id = ecp.id');
  $query = $query->condition('ecpss.field_coupon_state_shipping_value', 1);
  $tomorrow = time()+(86400*2); // Dia = 24 * 60 * 60 -> 24 horas; 60 minutos; 60 segundos

  $query = $query->condition('ecpds.field_coupon_promo_date_start_value', $tomorrow, '<=');

  $query = $query->fields('ecp', array('id', 'title'));
  $query = $query->fields('ecpds', array('field_coupon_promo_date_start_value'));

  $resultCuponPromoEnviado = $query->execute()->fetchAll();

  return $resultCuponPromoEnviado;
}

function _get_coupons_promo_active() {
  $hoy = new DateTime('today');
  $query = db_select('eck_coupon_promo', 'ecp');
  $query->leftJoin('field_data_field_coupon_promo_date_start', 'ds', 'ds.entity_id = ecp.id');
  $query->leftJoin('field_data_field_coupon_promo_date_end', 'de', 'de.entity_id = ecp.id');
  $query->leftJoin('field_data_field_coupon_state_shipping', 'ss', 'ss.entity_id = ecp.id');
  $query->condition('ds.field_coupon_promo_date_start_value', $hoy->getTimestamp(), '<=');
  $query->condition('de.field_coupon_promo_date_end_value', $hoy->getTimestamp(), '>=');
  $query->condition('ss.field_coupon_state_shipping_value', 2);

  $query = $query->fields('ecp', array('id', 'title'));
  $query = $query->fields('ds', array('field_coupon_promo_date_start_value'));

  $result = $query->execute()->fetchAll();

  return $result;
}

function _get_total_vouchers($coupon_promo = 0) {
  if($coupon_promo < 1) {
    return 0;
  }
  $query = db_select('bbva_vouchers', 'ev');
  $query->condition('ev.id_cupon', $coupon_promo, '=');
  $query = $query->fields('ev', array('id'));
  $result = $query->execute()->rowCount();

  return $result;
}

function _get_total_downloaded_vouchers($coupon_promo = 0) {
  if($coupon_promo < 1) {
    return 0;
  }

  $query = db_select('bbva_vouchers', 'ev');
  $query->condition('ev.id_cupon', $coupon_promo, '=');
  $query->isNotNull('ev.name_user');
  $query = $query->fields('ev', array('id'));
  $result = $query->execute()->rowCount();

  return $result;
}