<?php
use \Fgo\Bo\SegmentacionBo;


/**
 * Implement hook_dush
 *
 */
function bbva_segmentacion_drush_command() {
   $items['split'] = array(
     'description' => 'Procesa los elementos de la carpeta split.',
     'aliases' => array('bbva_segmentacion_split'),
     'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL
   );
 
   return $items;
 }
 
 
 
 function drush_bbva_segmentacion_split() {
     bbva_segmentacion_batch_split();
 }
 

