<?php

/**
 * @file
 * PDF template for printing.
 *
 * Available variables are:
 *  - $entity - The entity itself.
 *  - $entity_array - The renderable array of this entity.
 *  - $entity_print_css - An array of stylesheets to be rendered.
 */
$wrapper = entity_metadata_wrapper('voucher', $entity->id);
$wrapperCuponPromo =$wrapper->field_voucher_coupon_promo;
$wrapperShop=$wrapperCuponPromo->field_coupon_promo_shop;
$wrapperBenefit    = $wrapperCuponPromo->field_coupon_promo_benefit;
$cuponImagenes = array();
$cuponImagenBenefit=$wrapperBenefit->field_benfit_image->value();
$cuponImagenBenefitLogo=$wrapperBenefit->field_benefit_logo_img->value();
$cuponImagenShop=$wrapperShop->field_shop_imagen->value();
$cuponImagenShopLogo=$wrapperShop->field_shiop_mark_tip->value(); 
if ($cuponImagenBenefit) {
   $cuponImagenes['imagen']=$cuponImagenBenefit['uri'];
}
if ($cuponImagenBenefitLogo) {
   $cuponImagenes['logo']=$cuponImagenBenefit['uri'];
}
if ($cuponImagenShop) {
   $cuponImagenes['imagen']=$cuponImagenShop['uri'];
}
if ($cuponImagenShopLogo) {
   $cuponImagenes['logo']=$cuponImagenShopLogo['uri'];
}
$cuponImagenes['title']=$wrapperCuponPromo->field_coupon_promo_shop->label();
if (!empty($cuponImagenes['logo'])) {
  $cuponImagenes['logo'] = '<img src="'.image_style_url('soloxhoy_small2', $cuponImagenes['logo']).'">';
} else {
  $cuponImagenes['logo']= $cuponImagenes['title'];
}

if (!empty($cuponImagenes['imagen'])) {
  $cuponImagenes['imagen'] =image_style_url('benefit_full', $cuponImagenes['imagen']);
}
$descuento= $wrapperCuponPromo->field_coupon_promo_discount->value();
$tope=$wrapperCuponPromo->field_coupon_promo_top->value();
$fecha=date('d/m/Y',$wrapperCuponPromo->field_coupon_promo_date_end->value());
$codigo=$wrapper->label();
$legal=$wrapperCuponPromo->field_coupon_promo_legal->value();
$cuponImagenes=$cuponImagenes;
$codigo=$wrapper->label();
?>
<html>
<head>
    <title></title>
    <?php print drupal_get_css($entity_print_css);?>
    <style type="text/css">
    .main{
        background:url(<?php print("'" . $cuponImagenes['imagen']);?>');
    }
    </style>
    <style type="text/css" media="all">
@import url("http://104.131.95.98:8085/sites/default/files/fontyourface/font.css?nridv0");
@import url("http://104.131.95.98:8085/sites/default/files/fontyourface/local_fonts/bbva_light-normal-normal/stylesheet.css?nridv0");
@import url("http://104.131.95.98:8085/sites/default/files/fontyourface/local_fonts/bbva-normal-normal/stylesheet.css?nridv0");
</style>
</head>
<body>
    <div class="main">
       <div class="group-left">
        </div>
        <div class="group-right">
            <div class="discount-area">
                <div class="logo">
               <?php print_r($cuponImagenes['logo']);?>

                </div>
                <div class="discount">
                    <div class="number">
                        <p>- <?php print_r($descuento);?></p>
                    </div>
                    <div class="porcent">
                        <p>%</p>
                    </div>
                </div>
                <div class="total-legend">
                    <div class="totalidad-compra">
                    <p>Sobre la totalidad de la compra.</p><p class="tope">Tope: $<?php print_r($tope);?></p>
                    </div>
                </div>
                <div class="todos-los-medios">
                    <p>Con todos los medios de pagos</p>
                </div>
                <div class="code-area">
                    <p class="code-label">C&oacute;digo</p>
                    <div class="code-container">
                        <p><?php print_r($codigo);?></p>
                    </div>
                </div>
                <div class="vencimiento-box">
                    <p>Vencimiento  <?php print_r($fecha);?></p>
                </div>
            </div>
        </div>
    <div id="footer-voucher">
        <div class="terminos">
        <?php $legal = utf8_decode($legal); ?>
        <?php print str_replace('?','',$legal); ?>
        </div>
    </div>

    </div>
</body>
</html>
