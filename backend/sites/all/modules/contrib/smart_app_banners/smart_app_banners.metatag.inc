<?php

/**
 * @file
 * Metatag module include.
 */

/**
 * Implements hook_metatag_config_default_alter().
 *
 * Check to see if the smartbanner library exists -- if it does,
 * add the key and an empty value to the configs array
 */
function smart_app_banners_metatag_config_default_alter(array &$configs) {
  foreach ($configs as &$config) {
    switch ($config->instance) {
      case 'global':
        $config->config += array('apple-itunes-app' => array('value' => ''));

        $list = libraries_get_libraries();
        if (isset($list['smartbanner'])) {
          $config->config += array('googlePlay-app' => array('value' => ''));
        }
        break;
    }
  }
}

/**
 * Implements hook_metatag_info().
 *
 * Check to see if the smartbanner library exists -- if it does, add the
 * form item to add a googlePlay app id into the METATAGS module
 */
function smart_app_banners_metatag_info() {
  $info['groups']['mobile'] = array(
    'label' => t('Mobile'),
  );

  $info['tags']['apple-itunes-app'] = array(
    'label' => t('iOS Smart App Banner app-id'),
    'description' => t('Site wide Smart App Banner app-id for iOS'),
    'class' => 'DrupalTextMetaTag',
    'form' => array(
      '#title' => t('iOS Smart App Banner') . ' app-id',
      '#description' => t('Apple provides numeric %app_id; visit !itunes_linkmaker_url to get any app id.', array(
        '%app_id' => 'app-id',
        '!itunes_linkmaker_url' => l(ITUNES_LINKMAKER_URL, ITUNES_LINKMAKER_URL),
      )),
      '#maxlength' => 10,
    ),
    'group' => 'mobile',
    'element' => array(
      '#theme' => 'metatag_smart_app_banner',
    ),

  );

  $list = libraries_get_libraries();

  if (isset($list['smartbanner'])) {

    $info['tags']['google-play-app'] = array(
      'label' => t('Android Smart App Banner app-id'),
      'description' => t('Site wide Smart App Banner app-id for Android'),
      'class' => 'DrupalTextMetaTag',
      'form' => array(
        '#title' => t('Android Smart App Banner') . ' app-id',
        '#description' => t('Google Play ID'),
        '#maxlength' => 50,
      ),
      'group' => 'mobile',
      'element' => array(
        '#theme' => 'metatag_smart_app_banner',
      ),

    );
  }

  return $info;
}
