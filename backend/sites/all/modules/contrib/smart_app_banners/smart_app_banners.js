/**
 * @file adding smartbanners.
 */

(function ($) {
  // Add on function if you don't have it.
  if (typeof on == undefined) {
    $.fn.extend({
      on: function(types, data, fn) {
        $(this).live(types, data, fn);
        return this;
      }
    });
  }
  Drupal.behaviors.smart_app_banners = {
    ran: false,
    attach: function (context, settings) {
      // Only run it once.
      if (!this.ran) {
        $.smartbanner(Drupal.settings.smart_app_banners);
        this.ran = true;
      }
    }
  };
})(jQuery);
