 Smart App Banners
 =======================

 Smart App Banners notifies a mobile iOS/Android user when an app is available (or already installed) for a currently visited site.

 Implementation for Android and iOS < 6 requires an additional library, though Smart App Banners does not require it to work in iOS.

 Advanced Additional arguments can be passed through the Smart App Banner upon launch allowing an app to respond with a certain View or action.



 Instructions
 ------------------------
 1. Install and enable the "Smart App Banners" module.

 *  (optional: go to https://github.com/jasny/jquery.smartbanner
               download jquery.smartbanner and place in your libraries folder (e.g. sites/all/libraries)
               rename from jquery.smartbanner to smartbanner.)

 2. Configure the module by adding your App ID via the METATAG module, which
    should have been downloaded and installed when Smart App Banners was enabled.

    Drupal: Configurations -> "Search and Metadata" - Meta Tags -> Click "override" or "edit" next to the GLOBAL heading -> Click on "mobile" -> enter your app ID(s) (to see the app ID(s) that have been entered: on the Meta Tags module screen click on the GLOBAL heading)

    App ID available via http://itunes.apple.com/linkmaker/

 *  (optional: add your Googleplay app ID in the provided slot via the METATAG module.)
    Google Play com.<yourpackage>

 3. Open web site in mobile Safari on an iOS device running iOS 6
    or higher.

 *  (optional: use Android!)


 Creator & Current Maintainer:
 - Chris Charlton - XNTD.US & LA Drupal

 Modified to utilize jquery.smartbanner library by:
 - Jay Schoen, sponsored by NumbersUSA.com
