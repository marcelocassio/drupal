<?php

/**
 * @file
 * create the smart_app_banners_admin_form
 * also: check if smartbanner library exists...
 * otherwise, list instructions on installing it
 */

/**
 * Smart app banner admin settings form.
 */
function smart_app_banners_admin_form($form, &$form_state) {
  $form['smart_app_banners_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('smart_app_banners_title', NULL),
    '#required' => TRUE,
  );

  $form['smart_app_banners_author'] = array(
    '#type' => 'textfield',
    '#title' => t('Author'),
    '#default_value' => variable_get('smart_app_banners_author', NULL),
    '#required' => TRUE,
  );

  $form['smart_app_banners_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#default_value' => variable_get('smart_app_banners_price', t('FREE')),
    '#required' => TRUE,
  );

  $form['smart_app_banners_appstorelanguage'] = array(
    '#type' => 'textfield',
    '#title' => t('App Store Language'),
    '#default_value' => variable_get('smart_app_banners_appstorelanguage', t('us')),
    '#required' => TRUE,
  );

  $form['smart_app_banners_inappstore'] = array(
    '#type' => 'textfield',
    '#title' => t('iOS Text'),
    '#default_value' => variable_get('smart_app_banners_inappstore', t('On the App Store')),
  );

  $form['smart_app_banners_ingoogleplay'] = array(
    '#type' => 'textfield',
    '#title' => t('Android Text'),
    '#default_value' => variable_get('smart_app_banners_ingoogleplay', t('In Google Play')),
  );

  // If there is already an uploaded image, display it.
  if ($image_fid = variable_get('smart_app_banners_icon_image_fid', FALSE)) {
    $image = file_load($image_fid);
    $style = 'thumbnail';
    $form['smart_app_banners_icon_image'] = array(
      '#markup' => theme('smart_app_banners_icon_image', array('image' => $image, 'style' => $style)),
    );
  }

  $form['smart_app_banners_icon_image_fid'] = array(
    '#title' => t('Smart app icon image'),
    '#type' => 'managed_file',
    '#description' => t("If you don't have direct file access to the server, use this field to upload a smart app banner icon image."),
    '#default_value' => variable_get('smart_app_banners_icon_image_fid', ''),
    '#upload_location' => 'public://smart_app_banners_icon_images/',
  );

  $form['smart_app_banners_icongloss'] = array(
    '#type' => 'radios',
    '#title' => t('Icon Gloss'),
    '#default_value' => variable_get('smart_app_banners_icongloss', NULL),
    '#options' => array(
      0 => t("no gloss"),
      1 => t("add gloss"),
    ),
    '#required' => FALSE,
  );

  $form['smart_app_banners_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#default_value' => variable_get('smart_app_banners_button', t('VIEW')),
    '#required' => TRUE,
  );

  $form['smart_app_banners_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Button URL'),
    '#description' => t('The URL you want the button to link to. Leave empty to direct to default app store for iOS or Android.'),
    '#default_value' => variable_get('smart_app_banners_url', NULL),
  );

  $form['smart_app_banners_scale'] = array(
    '#type' => 'textfield',
    '#title' => t('Scale'),
    '#description' => t('1: disabled -- "auto": recommended'),
    '#default_value' => variable_get('smart_app_banners_scale', 'auto'),
    '#required' => TRUE,
  );

  $form['smart_app_banners_speedin'] = array(
    '#type' => 'textfield',
    '#title' => t('"Show" Animation Speed'),
    '#default_value' => variable_get('smart_app_banners_speedin', '300'),
    '#required' => TRUE,
  );

  $form['smart_app_banners_speedout'] = array(
    '#type' => 'textfield',
    '#title' => t('"Hide" Animation Speed'),
    '#default_value' => variable_get('smart_app_banners_speedout', '400'),
    '#required' => TRUE,
  );

  $form['smart_app_banners_dayshidden'] = array(
    '#type' => 'textfield',
    '#title' => t('Days Hidden'),
    '#description' => t('Duration to "hide" the banner after being closed. "0" to always show this banner'),
    '#default_value' => variable_get('smart_app_banners_dayshidden', '15'),
    '#required' => TRUE,
  );

  $form['smart_app_banners_daysreminder'] = array(
    '#type' => 'textfield',
    '#title' => t('Days Reminder'),
    '#description' => t('Duration to "hide" the banner after VIEW has been clicked. "0" to always show this banner'),
    '#default_value' => variable_get('smart_app_banners_daysreminder', '90'),
    '#required' => TRUE,
  );

  $form['smart_app_banners_force'] = array(
    '#type' => 'radios',
    '#title' => t('Force'),
    '#options' => array(
      NULL => t('none'),
      'ios' => t('iOS'),
      'android' => t('Android'),
    ),
    '#description' => t('Choose iOS or Android to skip the browser check and always show this banner'),
    '#default_value' => variable_get('smart_app_banners_force', NULL),
    '#required' => FALSE,
  );

  $form = system_settings_form($form);
  unset($form['#submit']);

  return $form;
}

/**
 * Theme function that displays an image using the specified style.
 *
 * @param array $variables
 *   Array containing the image object and the output style.
 *
 * @return string
 *   Returns HTML for an image using a specific image style.
 */
function theme_smart_app_banners_icon_image($variables) {
  $image = $variables['image'];
  $style = $variables['style'];
  $output = theme('image_style',
    array(
      'style_name' => $style,
      'path' => $image->uri,
      'getsize' => FALSE,
    )
  );
  $output .= '<p>' . t('This image is being displayed using the image style %style_name.', array('%style_name' => $style)) . '</p>';

  return $output;
}

/**
 * Form submission handler.
 *
 * @param array $form
 *   Array containing the individual form elements.
 * @param array $form_state
 *   Array containing form state information.
 */
function smart_app_banners_admin_form_submit($form, &$form_state) {
  // If fid is not 0 we have a valid file.
  if ($form_state['values']['smart_app_banners_icon_image_fid'] != 0) {
    $file = file_load($form_state['values']['smart_app_banners_icon_image_fid']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'smart_app_banners', 'icon_image', $file->fid);
    // Save the fid of the file so that the module can reference it later.
    variable_set('smart_app_banners_icon_image_fid', $file->fid);

  }
  // If the file was removed we need to remove the module's reference to the
  // removed file's fid, and remove the file.
  elseif ($form_state['values']['smart_app_banners_icon_image_fid'] == 0) {
    // Retrieve the old file's id.
    $fid = variable_get('smart_app_banners_icon_image_fid', FALSE);
    $file = $fid ? file_load($fid) : FALSE;
    if ($file) {
      // When a module is managing a file, it must manage the usage count.
      // Here we decrement the usage count with file_usage_delete().
      file_usage_delete($file, 'smart_app_banners', 'icon_image', $file->fid);
      file_delete($file);
    }
    variable_set('smart_app_banners_icon_image_fid', FALSE);
  }

  system_settings_form_submit($form, $form_state);
}
